.class Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;
.super Ljava/lang/Object;
.source "CarrierConfigLoader.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/CarrierConfigLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CarrierServiceConnection"
.end annotation


# instance fields
.field bound:Z

.field eventId:I

.field operatorNumeric:Ljava/lang/String;

.field phoneId:I

.field service:Landroid/os/IBinder;

.field final synthetic this$0:Lcom/android/phone/CarrierConfigLoader;


# direct methods
.method public constructor <init>(Lcom/android/phone/CarrierConfigLoader;IILjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/CarrierConfigLoader;
    .param p2, "phoneId"    # I
    .param p3, "eventId"    # I
    .param p4, "operatorNumeric"    # Ljava/lang/String;

    .prologue
    .line 873
    iput-object p1, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->this$0:Lcom/android/phone/CarrierConfigLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 874
    iput p2, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->phoneId:I

    .line 875
    iput p3, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->eventId:I

    .line 876
    iput-object p4, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->operatorNumeric:Ljava/lang/String;

    .line 877
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 881
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Connected to config app: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap7(Ljava/lang/String;)V

    .line 882
    iput-object p2, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->service:Landroid/os/IBinder;

    .line 884
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->bound:Z

    .line 885
    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->this$0:Lcom/android/phone/CarrierConfigLoader;

    invoke-static {v1}, Lcom/android/phone/CarrierConfigLoader;->-get3(Lcom/android/phone/CarrierConfigLoader;)Landroid/os/Handler;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->eventId:I

    iget v3, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->phoneId:I

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v3, v4, p0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 886
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "operator_number"

    iget-object v3, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->operatorNumeric:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->this$0:Lcom/android/phone/CarrierConfigLoader;

    invoke-static {v1}, Lcom/android/phone/CarrierConfigLoader;->-get3(Lcom/android/phone/CarrierConfigLoader;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 888
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 892
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->service:Landroid/os/IBinder;

    .line 894
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->bound:Z

    .line 895
    return-void
.end method
