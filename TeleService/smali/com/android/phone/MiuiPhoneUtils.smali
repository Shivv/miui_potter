.class public Lcom/android/phone/MiuiPhoneUtils;
.super Ljava/lang/Object;
.source "MiuiPhoneUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/MiuiPhoneUtils$1;
    }
.end annotation


# static fields
.field public static DBG_LEVEL:I

.field public static final DUAL_VOLTE_SUPPORTED:Z

.field public static final PHONE_COUNT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    sput v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    .line 92
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v0

    sput v0, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    .line 93
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->isDualVolteSupported()Z

    move-result v0

    sput-boolean v0, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    .line 103
    new-instance v0, Lcom/android/phone/MiuiPhoneUtils$1;

    invoke-direct {v0}, Lcom/android/phone/MiuiPhoneUtils$1;-><init>()V

    invoke-static {v0}, Lmiui/telephony/PhoneDebug;->addListener(Lmiui/telephony/PhoneDebug$Listener;)Lmiui/telephony/PhoneDebug$Listener;

    .line 72
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static NotifyNetworkSelection(Ljava/lang/String;)V
    .locals 2
    .param p0, "status"    # Ljava/lang/String;

    .prologue
    .line 214
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget-object v0, v0, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    .line 215
    const/4 v1, 0x2

    .line 214
    invoke-virtual {v0, v1, p0}, Lcom/android/phone/NotificationMgr;->postTransientNotification(ILjava/lang/CharSequence;)V

    .line 216
    return-void
.end method

.method public static addWindowLable(Landroid/content/Intent;Ljava/lang/CharSequence;)V
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "charSequence"    # Ljava/lang/CharSequence;

    .prologue
    .line 244
    const-string/jumbo v0, ":miui:starting_window_label"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 245
    return-void
.end method

.method static dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 358
    invoke-static {p0, p1, p2}, Lcom/android/phone/MiuiPhoneUtils;->dumpNetworkSelection(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 359
    invoke-static {p0, p1, p2}, Lcom/android/phone/NetworkModeManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 360
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lmiui/telephony/SubscriptionManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 361
    return-void
.end method

.method private static dumpNetworkSelection(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 8
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 569
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v0

    .line 570
    .local v0, "activeSubInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    if-eqz v0, :cond_0

    .line 571
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v6

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 572
    .local v2, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "subInfo$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/telephony/SubscriptionInfo;

    .line 573
    .local v4, "subInfo":Lmiui/telephony/SubscriptionInfo;
    invoke-virtual {v4}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v3

    .line 574
    .local v3, "subId":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "network_selection_name_key"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, ""

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 575
    .local v1, "networkSelection":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " network_selection_name_key"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 576
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "network_selection_key"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, ""

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 577
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " network_selection_key"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 580
    .end local v1    # "networkSelection":Ljava/lang/String;
    .end local v2    # "sp":Landroid/content/SharedPreferences;
    .end local v3    # "subId":I
    .end local v4    # "subInfo":Lmiui/telephony/SubscriptionInfo;
    .end local v5    # "subInfo$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->flush()V

    .line 581
    return-void
.end method

.method public static getCallManager()Lcom/android/internal/telephony/CallManager;
    .locals 1

    .prologue
    .line 219
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/phone/PhoneGlobals;->getCallManager()Lcom/android/internal/telephony/CallManager;

    move-result-object v0

    return-object v0
.end method

.method public static getCarrierCofigBooleanValue(II)Z
    .locals 6
    .param p0, "subId"    # I
    .param p1, "configId"    # I

    .prologue
    .line 594
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    .line 595
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1, p0}, Landroid/telephony/SubscriptionManager;->getResourcesForSubId(Landroid/content/Context;I)Landroid/content/res/Resources;

    move-result-object v2

    .line 596
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 597
    .local v0, "config":Z
    const-string/jumbo v3, "MiuiPhoneUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getCarrierCofigBooleanValue: configId/value"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " subId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    return v0
.end method

.method public static getCdmaPhone()Lcom/android/internal/telephony/Phone;
    .locals 4

    .prologue
    .line 146
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 147
    .local v1, "phones":[Lcom/android/internal/telephony/Phone;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 148
    aget-object v2, v1, v0

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    aget-object v2, v1, v0

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/internal/telephony/IccCard;->hasIccCard()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    aget-object v2, v1, v0

    return-object v2

    .line 147
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    :cond_1
    const/4 v2, 0x0

    return-object v2
.end method

.method public static getDataPhone()Lcom/android/internal/telephony/Phone;
    .locals 2

    .prologue
    .line 138
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    .line 139
    .local v0, "dataSlotId":I
    invoke-static {v0}, Lmiui/telephony/SubscriptionManager;->isRealSlotId(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 140
    const/4 v1, 0x0

    return-object v1

    .line 142
    :cond_0
    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    return-object v1
.end method

.method public static getDisplayName(Lmiui/telephony/SubscriptionInfo;)Ljava/lang/CharSequence;
    .locals 2
    .param p0, "simInfoRecord"    # Lmiui/telephony/SubscriptionInfo;

    .prologue
    .line 465
    if-nez p0, :cond_0

    .line 466
    const-string/jumbo v0, ""

    return-object v0

    .line 468
    :cond_0
    invoke-static {}, Lmiui/telephony/VirtualSimUtils;->getInstance()Lmiui/telephony/VirtualSimUtils;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/VirtualSimUtils;->getVirtualSimSlotId()I

    move-result v0

    invoke-virtual {p0}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 469
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    invoke-static {v0}, Lmiui/telephony/VirtualSimUtils;->getVirtualSimCarrierName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 471
    :cond_1
    invoke-virtual {p0}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static getEmergencyPhoneForNumber(Ljava/lang/String;)Lcom/android/internal/telephony/Phone;
    .locals 9
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 307
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 308
    return-object v4

    .line 310
    :cond_0
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 311
    .local v2, "phones":[Lcom/android/internal/telephony/Phone;
    const/4 v3, 0x0

    .line 312
    .local v3, "retPhone":Lcom/android/internal/telephony/Phone;
    const/4 v0, 0x0

    .line 313
    .local v0, "matchPhoneCount":I
    const/4 v5, 0x0

    array-length v6, v2

    .end local v3    # "retPhone":Lcom/android/internal/telephony/Phone;
    :goto_0
    if-ge v5, v6, :cond_2

    aget-object v1, v2, v5

    .line 314
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v7

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v8

    invoke-static {v7, v8, p0}, Landroid/telephony/PhoneNumberUtils;->isLocalEmergencyNumber(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 315
    add-int/lit8 v0, v0, 0x1

    .line 316
    move-object v3, v1

    .line 313
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 319
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_2
    const/4 v5, 0x1

    if-ne v0, v5, :cond_3

    :goto_1
    return-object v3

    :cond_3
    move-object v3, v4

    goto :goto_1
.end method

.method public static getFdnUri(I)Landroid/net/Uri;
    .locals 3
    .param p0, "slotId"    # I

    .prologue
    .line 125
    const-string/jumbo v0, "content://icc/fdn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 126
    sget-object v1, Lmiui/telephony/SubscriptionManager;->SLOT_KEY:Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getIccCardCountExcludeSlot(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 323
    const/4 v0, 0x0

    .line 324
    .local v0, "count":I
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v3

    const/4 v2, 0x0

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v1, v3, v2

    .line 325
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v5

    if-eq v5, p0, :cond_0

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v5

    invoke-interface {v5}, Lcom/android/internal/telephony/IccCard;->hasIccCard()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 326
    add-int/lit8 v0, v0, 0x1

    .line 324
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 329
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    return v0
.end method

.method public static getIccRecordsLoaded(I)Z
    .locals 3
    .param p0, "slotId"    # I

    .prologue
    const/4 v1, 0x0

    .line 561
    invoke-static {p0}, Lmiui/telephony/SubscriptionManager;->isRealSlotId(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 562
    return v1

    .line 564
    :cond_0
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 565
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getIccRecordsLoaded()Z

    move-result v1

    :cond_1
    return v1
.end method

.method public static getInsertedSimCount()I
    .locals 6

    .prologue
    .line 223
    const/4 v0, 0x0

    .line 224
    .local v0, "insertedSimCount":I
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 225
    .local v2, "phones":[Lcom/android/internal/telephony/Phone;
    if-eqz v2, :cond_1

    .line 226
    const/4 v3, 0x0

    array-length v4, v2

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v2, v3

    .line 227
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v5

    invoke-interface {v5}, Lcom/android/internal/telephony/IccCard;->hasIccCard()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 228
    add-int/lit8 v0, v0, 0x1

    .line 226
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 232
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    return v0
.end method

.method public static getNetworkQueryService(Landroid/os/IBinder;)Lcom/android/phone/INetworkQueryService;
    .locals 1
    .param p0, "service"    # Landroid/os/IBinder;

    .prologue
    .line 210
    check-cast p0, Lcom/android/phone/NetworkQueryService$LocalBinder;

    .end local p0    # "service":Landroid/os/IBinder;
    invoke-virtual {p0}, Lcom/android/phone/NetworkQueryService$LocalBinder;->getService()Lcom/android/phone/INetworkQueryService;

    move-result-object v0

    return-object v0
.end method

.method public static getPhone(I)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "phoneId"    # I

    .prologue
    .line 130
    invoke-static {p0}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0
.end method

.method public static getPhoneForEmergencyCall(Landroid/telecom/ConnectionRequest;Z)Lcom/android/internal/telephony/Phone;
    .locals 9
    .param p0, "request"    # Landroid/telecom/ConnectionRequest;
    .param p1, "isEmergencyNumber"    # Z

    .prologue
    const/4 v8, 0x0

    .line 271
    if-nez p1, :cond_0

    .line 272
    return-object v8

    .line 275
    :cond_0
    sget v5, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 276
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v5

    return-object v5

    .line 280
    :cond_1
    invoke-virtual {p0}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 281
    .local v0, "accountHandle":Landroid/telecom/PhoneAccountHandle;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 283
    :try_start_0
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v5

    .line 284
    invoke-virtual {v0}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 283
    invoke-virtual {v5, v6}, Lmiui/telephony/SubscriptionManager;->getPhoneIdForSubscription(I)I

    move-result v4

    .line 285
    .local v4, "phoneId":I
    invoke-static {v4}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    .line 286
    .local v3, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/telephony/ServiceState;->getState()I

    move-result v5

    if-eqz v5, :cond_2

    .line 287
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/telephony/ServiceState;->isEmergencyOnly()Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 286
    if-eqz v5, :cond_3

    .line 288
    :cond_2
    return-object v3

    .line 290
    .end local v3    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v4    # "phoneId":I
    :catch_0
    move-exception v1

    .line 291
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v5, "MiuiPhoneUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Could not get subId from account: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    invoke-virtual {p0}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v2

    .line 297
    .local v2, "handle":Landroid/net/Uri;
    if-eqz v2, :cond_4

    .line 298
    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/MiuiPhoneUtils;->getEmergencyPhoneForNumber(Ljava/lang/String;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    return-object v5

    .line 300
    :cond_4
    return-object v8
.end method

.method public static getPhones()[Lcom/android/internal/telephony/Phone;
    .locals 1

    .prologue
    .line 134
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0
.end method

.method public static getPreferenceKey(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "prefKey"    # Ljava/lang/String;
    .param p1, "slotId"    # I

    .prologue
    .line 156
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 159
    :cond_0
    return-object p0
.end method

.method public static getSimIconResId(IZ)I
    .locals 1
    .param p0, "slotId"    # I
    .param p1, "isBig"    # Z

    .prologue
    .line 475
    invoke-static {}, Lmiui/telephony/VirtualSimUtils;->getInstance()Lmiui/telephony/VirtualSimUtils;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/VirtualSimUtils;->getVirtualSimSlotId()I

    move-result v0

    if-ne v0, p0, :cond_1

    .line 476
    if-eqz p1, :cond_0

    const v0, 0x7f02009b

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f02009c

    goto :goto_0

    .line 478
    :cond_1
    const/4 v0, 0x1

    if-ne p0, v0, :cond_3

    .line 479
    if-eqz p1, :cond_2

    const v0, 0x7f020057

    :goto_1
    return v0

    :cond_2
    const v0, 0x7f0200a4

    goto :goto_1

    .line 481
    :cond_3
    if-eqz p1, :cond_4

    const v0, 0x7f020056

    :goto_2
    return v0

    :cond_4
    const v0, 0x7f0200a3

    goto :goto_2
.end method

.method public static getSubscriptionInfoBySlot(Ljava/util/List;I)Lmiui/telephony/SubscriptionInfo;
    .locals 4
    .param p1, "slotId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/telephony/SubscriptionInfo;",
            ">;I)",
            "Lmiui/telephony/SubscriptionInfo;"
        }
    .end annotation

    .prologue
    .local p0, "subInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    const/4 v3, 0x0

    .line 346
    if-nez p0, :cond_0

    .line 347
    return-object v3

    .line 349
    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "subInfo$iterator":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/telephony/SubscriptionInfo;

    .line 350
    .local v0, "subInfo":Lmiui/telephony/SubscriptionInfo;
    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 351
    return-object v0

    .line 354
    .end local v0    # "subInfo":Lmiui/telephony/SubscriptionInfo;
    :cond_2
    return-object v3
.end method

.method public static getVirtualSimCarrierName()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 179
    const/4 v0, 0x0

    .line 181
    .local v0, "b":Landroid/os/Bundle;
    :try_start_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "content://com.miui.virtualsim.provider.virtualsimInfo"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 182
    const-string/jumbo v5, "getCarrierName"

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 181
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 186
    .end local v0    # "b":Landroid/os/Bundle;
    :goto_0
    if-nez v0, :cond_0

    :goto_1
    return-object v2

    .line 183
    .restart local v0    # "b":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 184
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "MiuiPhoneUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getVirtualSimCarrierName e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 186
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const-string/jumbo v2, "carrierName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static getVirtualSimIntent()Landroid/content/Intent;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 190
    const/4 v0, 0x0

    .line 192
    .local v0, "b":Landroid/os/Bundle;
    :try_start_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "content://com.miui.virtualsim.provider.virtualsimInfo"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 193
    const-string/jumbo v5, "getSettingsEntranceIntent"

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 192
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 197
    .end local v0    # "b":Landroid/os/Bundle;
    :goto_0
    if-nez v0, :cond_0

    :goto_1
    return-object v2

    .line 194
    .restart local v0    # "b":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 195
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "MiuiPhoneUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getVirtualSimIntent e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 197
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const-string/jumbo v2, "setting_entrance"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    goto :goto_1
.end method

.method public static getVirtualSimSlot()I
    .locals 1

    .prologue
    .line 172
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$VirtualSim;->isVirtualSimEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$VirtualSim;->getVirtualSimSlotId(Landroid/content/Context;)I

    move-result v0

    return v0

    .line 175
    :cond_0
    sget v0, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    return v0
.end method

.method public static handleUssdMessages(Landroid/content/Context;Lcom/android/internal/telephony/Phone;Ljava/lang/CharSequence;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "ussdMsg"    # Ljava/lang/CharSequence;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 531
    sget-boolean v1, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 532
    if-nez p1, :cond_1

    .line 533
    :cond_0
    return-void

    .line 532
    :cond_1
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 535
    new-instance v1, Lcom/android/phone/MiuiPhoneUtils$2;

    invoke-direct {v1, p1, p2, p0}, Lcom/android/phone/MiuiPhoneUtils$2;-><init>(Lcom/android/internal/telephony/Phone;Ljava/lang/CharSequence;Landroid/content/Context;)V

    .line 557
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    .line 535
    new-array v3, v3, [Ljava/lang/Void;

    .line 557
    check-cast v0, Ljava/lang/Void;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 535
    invoke-virtual {v1, v2, v3}, Lcom/android/phone/MiuiPhoneUtils$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 558
    return-void
.end method

.method public static is4GOnlySim(Ljava/lang/String;)Z
    .locals 1
    .param p0, "numeric"    # Ljava/lang/String;

    .prologue
    .line 409
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isJioSim(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isIraqFastlinkSim(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isCTDualVolteSupported()Z
    .locals 1

    .prologue
    .line 716
    sget-boolean v0, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isCTVolteSupportedByDevice()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCTSimInViceSlot(Lcom/android/internal/telephony/Phone;)Z
    .locals 3
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v1, 0x0

    .line 417
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    .line 418
    .local v0, "ddsSlotId":I
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isChinaOrMacauTelecomSim(Lcom/android/internal/telephony/Phone;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    if-eq v2, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static isCTVoLTESupport(Lcom/android/internal/telephony/Phone;)Z
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 742
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isChinaOrMacauTelecomSim(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isCTVolteSupportedByDevice()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCTVolteSupportedByDevice()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 699
    sget-boolean v4, Lmiui/os/Build;->IS_CT_CUSTOMIZATION_TEST:Z

    if-eqz v4, :cond_0

    .line 700
    return v8

    .line 702
    :cond_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 703
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f07008a

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 704
    .local v3, "devicesSupport":[Ljava/lang/String;
    if-eqz v3, :cond_1

    array-length v4, v3

    if-nez v4, :cond_2

    :cond_1
    return v5

    .line 705
    :cond_2
    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    .line 707
    .local v1, "currentDevice":Ljava/lang/String;
    array-length v6, v3

    move v4, v5

    :goto_0
    if-ge v4, v6, :cond_4

    aget-object v2, v3, v4

    .line 708
    .local v2, "device":Ljava/lang/String;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 709
    return v8

    .line 707
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 712
    .end local v2    # "device":Ljava/lang/String;
    :cond_4
    return v5
.end method

.method public static isCdmaVolteNotInService(Lcom/android/internal/telephony/Phone;)Z
    .locals 4
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v1, 0x0

    .line 650
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isCTVoLTESupport(Lcom/android/internal/telephony/Phone;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 651
    :cond_0
    return v1

    .line 654
    :cond_1
    sget-boolean v2, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSubscriptionId()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 655
    return v1

    .line 658
    :cond_2
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/imsphone/ImsPhone;

    .line 659
    .local v0, "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    if-eqz v0, :cond_3

    .line 660
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getState()I

    move-result v2

    if-eqz v2, :cond_3

    .line 661
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 659
    if-eqz v2, :cond_3

    .line 662
    invoke-static {p0}, Lcom/android/phone/PhoneProxy;->isVolteEnabled(Lcom/android/internal/telephony/Phone;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 659
    if-eqz v2, :cond_3

    .line 663
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isVolteEnabledByPlatform(Lcom/android/internal/telephony/Phone;)Z

    move-result v2

    .line 659
    if-eqz v2, :cond_3

    .line 664
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/services/telephony/ims/ImsAdapter;->getEnhanced4gLteModeSetting(Landroid/content/Context;I)Z

    move-result v1

    .line 659
    :cond_3
    return v1
.end method

.method public static isChinaOrMacauTelecomSim(Lcom/android/internal/telephony/Phone;)Z
    .locals 2
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 440
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    if-nez v1, :cond_1

    .line 441
    :cond_0
    const/4 v1, 0x0

    return v1

    .line 440
    :cond_1
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/internal/telephony/IccCard;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 443
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/internal/telephony/IccCard;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v0

    .line 444
    .local v0, "numeric":Ljava/lang/String;
    invoke-static {v0}, Lmiui/telephony/ServiceProviderUtils;->isChinaTelecom(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 445
    invoke-static {v0}, Lmiui/telephony/ServiceProviderUtils;->isMacauTelecom(Ljava/lang/String;)Z

    move-result v1

    .line 444
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isCmccSim(Lcom/android/internal/telephony/Phone;)Z
    .locals 3
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v2, 0x0

    .line 429
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    if-nez v1, :cond_1

    .line 430
    :cond_0
    return v2

    .line 429
    :cond_1
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/internal/telephony/IccCard;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 432
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/internal/telephony/IccCard;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/IccRecords;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v0

    .line 433
    .local v0, "numeric":Ljava/lang/String;
    invoke-static {v0}, Lmiui/telephony/ServiceProviderUtils;->isChinaMobile(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 434
    const/4 v1, 0x1

    return v1

    .line 436
    :cond_2
    return v2
.end method

.method public static isCmdaCwOperatedByUt(Lcom/android/internal/telephony/Phone;)Z
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->isImsRegistered()Z

    move-result v0

    return v0
.end method

.method public static isDataEnableNeededForUt(Lcom/android/internal/telephony/Phone;I)Z
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "ssType"    # I

    .prologue
    .line 642
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 643
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getDataEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 642
    if-eqz v0, :cond_1

    .line 644
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isSSOperatedByUt(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    .line 642
    if-eqz v0, :cond_1

    .line 645
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isCmccSim(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isChinaOrMacauTelecomSim(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    .line 642
    :goto_0
    return v0

    .line 645
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 642
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDcOnlyVirtualSim(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 163
    invoke-static {p0}, Landroid/provider/MiuiSettings$VirtualSim;->isVirtualSimEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Landroid/provider/MiuiSettings$VirtualSim;->getVirtualSimType(Landroid/content/Context;)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isDualCTSim()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 720
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getInsertedSimCount()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 721
    return v3

    .line 723
    :cond_0
    const/4 v0, 0x0

    .line 724
    .local v0, "ctSimCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v4, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v1, v4, :cond_2

    .line 725
    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 726
    .local v2, "phone":Lcom/android/internal/telephony/Phone;
    invoke-static {v2}, Lcom/android/phone/MiuiPhoneUtils;->isChinaOrMacauTelecomSim(Lcom/android/internal/telephony/Phone;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 727
    add-int/lit8 v0, v0, 0x1

    .line 724
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 730
    .end local v2    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_2
    sget v4, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ne v0, v4, :cond_3

    const/4 v3, 0x1

    :cond_3
    return v3
.end method

.method public static isHMH2xCmForThreeMode()Z
    .locals 2

    .prologue
    .line 333
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_TWOX_CM:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "cm-three"

    const-string/jumbo v1, "ro.product.h2x_cm"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isIccCardActivated(I)Z
    .locals 2
    .param p0, "slotId"    # I

    .prologue
    .line 205
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v0

    .line 206
    .local v0, "si":Lmiui/telephony/SubscriptionInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->isActivated()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isIraqFastlinkSim(I)Z
    .locals 4
    .param p0, "slotId"    # I

    .prologue
    const/4 v3, 0x5

    const/4 v1, 0x0

    .line 401
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v2

    invoke-virtual {v2, p0}, Lmiui/telephony/DefaultSimManager;->getSimImsi(I)Ljava/lang/String;

    move-result-object v0

    .line 402
    .local v0, "imsi":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 403
    return v1

    .line 405
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v3, :cond_1

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->isIraqFastlinkSim(Ljava/lang/String;)Z

    move-result v1

    :cond_1
    return v1
.end method

.method public static isIraqFastlinkSim(Ljava/lang/String;)Z
    .locals 1
    .param p0, "numeric"    # Ljava/lang/String;

    .prologue
    .line 397
    const-string/jumbo v0, "41866"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isJioSim(Ljava/lang/String;)Z
    .locals 7
    .param p0, "numeric"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 384
    const/16 v2, 0x16

    new-array v0, v2, [Ljava/lang/String;

    const-string/jumbo v2, "405840"

    aput-object v2, v0, v3

    const-string/jumbo v2, "405854"

    aput-object v2, v0, v6

    const-string/jumbo v2, "405855"

    const/4 v4, 0x2

    aput-object v2, v0, v4

    const-string/jumbo v2, "405856"

    const/4 v4, 0x3

    aput-object v2, v0, v4

    const-string/jumbo v2, "405857"

    const/4 v4, 0x4

    aput-object v2, v0, v4

    const-string/jumbo v2, "405858"

    const/4 v4, 0x5

    aput-object v2, v0, v4

    const-string/jumbo v2, "405859"

    const/4 v4, 0x6

    aput-object v2, v0, v4

    const-string/jumbo v2, "405860"

    const/4 v4, 0x7

    aput-object v2, v0, v4

    const-string/jumbo v2, "405861"

    const/16 v4, 0x8

    aput-object v2, v0, v4

    const-string/jumbo v2, "405862"

    const/16 v4, 0x9

    aput-object v2, v0, v4

    .line 385
    const-string/jumbo v2, "405863"

    const/16 v4, 0xa

    aput-object v2, v0, v4

    const-string/jumbo v2, "405864"

    const/16 v4, 0xb

    aput-object v2, v0, v4

    const-string/jumbo v2, "405865"

    const/16 v4, 0xc

    aput-object v2, v0, v4

    const-string/jumbo v2, "405866"

    const/16 v4, 0xd

    aput-object v2, v0, v4

    const-string/jumbo v2, "405867"

    const/16 v4, 0xe

    aput-object v2, v0, v4

    const-string/jumbo v2, "405868"

    const/16 v4, 0xf

    aput-object v2, v0, v4

    const-string/jumbo v2, "405869"

    const/16 v4, 0x10

    aput-object v2, v0, v4

    const-string/jumbo v2, "405870"

    const/16 v4, 0x11

    aput-object v2, v0, v4

    const-string/jumbo v2, "405871"

    const/16 v4, 0x12

    aput-object v2, v0, v4

    const-string/jumbo v2, "405872"

    const/16 v4, 0x13

    aput-object v2, v0, v4

    .line 386
    const-string/jumbo v2, "405873"

    const/16 v4, 0x14

    aput-object v2, v0, v4

    const-string/jumbo v2, "405874"

    const/16 v4, 0x15

    aput-object v2, v0, v4

    .line 387
    .local v0, "jioPlmn":[Ljava/lang/String;
    array-length v4, v0

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v1, v0, v2

    .line 388
    .local v1, "plmn":Ljava/lang/String;
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 389
    const-string/jumbo v2, "MiuiPhoneUtils"

    const-string/jumbo v3, "isJioSim"

    invoke-static {v2, v3}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    return v6

    .line 387
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 393
    .end local v1    # "plmn":Ljava/lang/String;
    :cond_1
    return v3
.end method

.method public static isLte(I)Z
    .locals 4
    .param p0, "subId"    # I

    .prologue
    .line 602
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 603
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 605
    .local v1, "telephonymanager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1, p0}, Landroid/telephony/TelephonyManager;->getVoiceNetworkType(I)I

    move-result v2

    invoke-static {v2}, Landroid/telephony/TelephonyManager;->getNetworkClass(I)I

    move-result v2

    .line 604
    const/4 v3, 0x3

    if-ne v3, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isMultiSimEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 342
    sget v1, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isRegionOrDeviceConfiged(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p0, "configValue"    # Ljava/lang/String;
    .param p1, "currentRegion"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 502
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    return v5

    .line 503
    :cond_0
    const-string/jumbo v4, ";"

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 504
    .local v0, "configSplit":[Ljava/lang/String;
    if-eqz v0, :cond_1

    array-length v4, v0

    if-nez v4, :cond_2

    :cond_1
    return v5

    .line 506
    :cond_2
    aget-object v4, v0, v5

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 508
    array-length v4, v0

    if-ne v4, v8, :cond_3

    .line 509
    return v8

    .line 512
    :cond_3
    const-string/jumbo v4, "MiuiPhoneUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "isRegionOrDeviceConfiged: region: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v5

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " devices: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    .line 515
    .local v1, "currentDevice":Ljava/lang/String;
    aget-object v4, v0, v8

    const-string/jumbo v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 516
    .local v3, "devicesConfiged":[Ljava/lang/String;
    if-eqz v3, :cond_4

    array-length v4, v3

    if-nez v4, :cond_5

    .line 517
    :cond_4
    return v5

    .line 519
    :cond_5
    array-length v6, v3

    move v4, v5

    :goto_0
    if-ge v4, v6, :cond_7

    aget-object v2, v3, v4

    .line 520
    .local v2, "device":Ljava/lang/String;
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 521
    return v8

    .line 519
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 526
    .end local v1    # "currentDevice":Ljava/lang/String;
    .end local v2    # "device":Ljava/lang/String;
    .end local v3    # "devicesConfiged":[Ljava/lang/String;
    :cond_7
    return v5
.end method

.method public static isRegionSupportVoLTE(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "region"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 486
    invoke-static {p0}, Lcom/android/phone/CarrierConfigLoaderInjector;->isVolteForceEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 487
    return v6

    .line 490
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f070089

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 491
    .local v1, "regionSupportVoLTE":[Ljava/lang/String;
    if-nez v1, :cond_1

    return v3

    .line 492
    :cond_1
    array-length v4, v1

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v1, v2

    .line 493
    .local v0, "regConfiged":Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/android/phone/MiuiPhoneUtils;->isRegionOrDeviceConfiged(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 494
    return v6

    .line 492
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 498
    .end local v0    # "regConfiged":Ljava/lang/String;
    :cond_3
    return v3
.end method

.method public static isSSOperatedByUt(Lcom/android/internal/telephony/Phone;)Z
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->isImsRegistered()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/android/phone/PhoneAdapter;->isUtEnabled(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isViceSlotOfDualCTSim(I)Z
    .locals 2
    .param p0, "phoneId"    # I

    .prologue
    const/4 v0, 0x0

    .line 735
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isDualCTSim()Z

    move-result v1

    if-nez v1, :cond_0

    .line 736
    return v0

    .line 738
    :cond_0
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v1

    if-eq p0, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static isVideoSuppServiceSupport(Lcom/android/internal/telephony/Phone;)Z
    .locals 4
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 456
    if-nez p0, :cond_0

    .line 457
    const/4 v1, 0x0

    return v1

    .line 459
    :cond_0
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v1

    const v2, 0x7f0e0035

    invoke-static {v1, v2}, Lcom/android/phone/MiuiPhoneUtils;->getCarrierCofigBooleanValue(II)Z

    move-result v0

    .line 460
    .local v0, "config":Z
    const-string/jumbo v1, "MiuiPhoneUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "isVideoSuppServiceSupport: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " /subId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    return v0
.end method

.method public static isVirtualSim(I)Z
    .locals 2
    .param p0, "slotId"    # I

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/MiuiSettings$VirtualSim;->isVirtualSimEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/MiuiSettings$VirtualSim;->getVirtualSimSlotId(Landroid/content/Context;)I

    move-result v1

    if-ne p0, v1, :cond_0

    const/4 v0, 0x1

    .line 167
    :cond_0
    return v0
.end method

.method public static isVolteEnabledByPlatform(Lcom/android/internal/telephony/Phone;)Z
    .locals 3
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 690
    sget-boolean v0, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v0, :cond_0

    .line 691
    const-string/jumbo v0, "MiuiPhoneUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "phoneId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/telephony/TelephonyManager;->isVolteEnabledByPlatform(I)Z

    move-result v0

    return v0

    .line 694
    :cond_0
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->isVolteEnabledByPlatform()Z

    move-result v0

    return v0
.end method

.method public static isVolteRegionFeatureEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 589
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_1

    .line 590
    const-string/jumbo v2, "persist.dbg.volte_avail_ovr"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 589
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 590
    goto :goto_0

    :cond_1
    move v0, v1

    .line 589
    goto :goto_0
.end method

.method public static maybeShowDialogForSuppService(Lcom/android/internal/telephony/Phone;I)Z
    .locals 6
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "ssType"    # I

    .prologue
    .line 610
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 611
    .local v0, "context":Landroid/content/Context;
    const/4 v2, -0x1

    .line 612
    .local v2, "messageId":I
    const/4 v3, 0x0

    .line 613
    .local v3, "showNegative":Z
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isCdmaVolteNotInService(Lcom/android/internal/telephony/Phone;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 614
    const v2, 0x7f0b0731

    .line 615
    const/4 v3, 0x1

    .line 616
    const-string/jumbo v4, "MiuiPhoneUtils"

    const-string/jumbo v5, "showTurnOffVolteDialog"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    :cond_0
    :goto_0
    const/4 v4, -0x1

    if-eq v2, v4, :cond_4

    .line 624
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-direct {v1, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 625
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 626
    const-string/jumbo v4, "dialog_type"

    const/4 v5, 0x3

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 627
    const-string/jumbo v4, "dialog_messaga_id"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 628
    if-eqz v3, :cond_1

    .line 629
    const-string/jumbo v4, "show_negative"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 631
    :cond_1
    sget-boolean v4, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v4, :cond_2

    .line 632
    const-string/jumbo v4, "phone_id"

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 634
    :cond_2
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 635
    const/4 v4, 0x1

    return v4

    .line 618
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-static {p0, p1}, Lcom/android/phone/MiuiPhoneUtils;->isDataEnableNeededForUt(Lcom/android/internal/telephony/Phone;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 619
    const v2, 0x7f0b06e6

    .line 620
    const-string/jumbo v4, "MiuiPhoneUtils"

    const-string/jumbo v5, "showOpenMobileDataDialog"

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 638
    :cond_4
    const/4 v4, 0x0

    return v4
.end method

.method public static maybeShowTurnOffVolteForCall(Lcom/android/internal/telephony/Phone;)Z
    .locals 4
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 668
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isCdmaVolteNotInService(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/android/phone/MiuiPhoneUtils$3;

    invoke-direct {v1, p0}, Lcom/android/phone/MiuiPhoneUtils$3;-><init>(Lcom/android/internal/telephony/Phone;)V

    .line 683
    const-wide/16 v2, 0x1f4

    .line 669
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 684
    const/4 v0, 0x1

    return v0

    .line 686
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static sendEnhanced4GLteModeChangeBroadcast(II)V
    .locals 8
    .param p0, "phoneId"    # I
    .param p1, "mode"    # I

    .prologue
    const/4 v7, -0x1

    .line 746
    invoke-static {p0}, Lmiui/telephony/SubscriptionManager;->isRealSlotId(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 747
    return-void

    .line 750
    :cond_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    .line 751
    .local v1, "context":Landroid/content/Context;
    if-nez p0, :cond_1

    const-string/jumbo v0, "miui.intent.action.ACTION_ENHANCED_4G_LTE_MODE_CHANGE_FOR_SLOT1"

    .line 753
    .local v0, "action":Ljava/lang/String;
    :goto_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 754
    .local v2, "intent":Landroid/content/Intent;
    if-ne p1, v7, :cond_2

    invoke-static {v1, p0}, Lcom/android/services/telephony/ims/ImsAdapter;->getEnhanced4gLteModeSetting(Landroid/content/Context;I)Z

    move-result v3

    .line 755
    :goto_1
    const-string/jumbo v4, "extra_is_enhanced_4g_lte_on"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 756
    invoke-static {v2, p0}, Landroid/telephony/SubscriptionManager;->putPhoneIdAndSubIdExtra(Landroid/content/Intent;I)V

    .line 757
    const-string/jumbo v4, "MiuiPhoneUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "send ACTION_ENHANCED_4G_LTE_MODE_CHANGE volteEnabled:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    const-string/jumbo v4, "android.permission.READ_PHONE_STATE"

    invoke-static {v2, v4, v7}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V

    .line 760
    return-void

    .line 752
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    const-string/jumbo v0, "miui.intent.action.ACTION_ENHANCED_4G_LTE_MODE_CHANGE_FOR_SLOT2"

    .restart local v0    # "action":Ljava/lang/String;
    goto :goto_0

    .line 754
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_2
    const/4 v4, 0x1

    if-ne p1, v4, :cond_3

    const/4 v3, 0x1

    .local v3, "volteEnabled":Z
    goto :goto_1

    .end local v3    # "volteEnabled":Z
    :cond_3
    const/4 v3, 0x0

    .restart local v3    # "volteEnabled":Z
    goto :goto_1
.end method

.method public static setActionBar(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 118
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 119
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 120
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 122
    :cond_0
    return-void
.end method

.method public static setNetworkModeInDb(Ljava/util/List;II)V
    .locals 6
    .param p1, "slotId"    # I
    .param p2, "networkType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmiui/telephony/SubscriptionInfo;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 248
    .local p0, "subInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 250
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string/jumbo v3, "preferred_network_mode"

    .line 249
    invoke-static {v0, v3, p1, p2}, Landroid/telephony/TelephonyManager;->putIntAtIndex(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 251
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-ge v3, v4, :cond_0

    .line 253
    return-void

    .line 256
    :cond_0
    if-nez p0, :cond_1

    .line 257
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v2

    .line 261
    .local v2, "subInfo":Lmiui/telephony/SubscriptionInfo;
    :goto_0
    if-eqz v2, :cond_2

    .line 262
    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v1

    .line 263
    .local v1, "subId":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "preferred_network_mode"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, p2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 264
    const-string/jumbo v3, "MiuiPhoneUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setNetworkModeInDb slot="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " sub="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    return-void

    .line 259
    .end local v1    # "subId":I
    .end local v2    # "subInfo":Lmiui/telephony/SubscriptionInfo;
    :cond_1
    invoke-static {p0, p1}, Lcom/android/phone/MiuiPhoneUtils;->getSubscriptionInfoBySlot(Ljava/util/List;I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v2

    .restart local v2    # "subInfo":Lmiui/telephony/SubscriptionInfo;
    goto :goto_0

    .line 267
    :cond_2
    const-string/jumbo v3, "MiuiPhoneUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setNetworkModeInDb slot="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " but no valid sub : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    return-void
.end method

.method public static shouldEnableVideoSuppService(Lcom/android/internal/telephony/Phone;)Z
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 449
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isSSOperatedByUt(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/android/services/telephony/ims/ImsAdapter;->isVideoEnabled(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 450
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 452
    :cond_1
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isVideoSuppServiceSupport(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    return v0
.end method

.method public static toLogSafePhoneNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    .line 201
    invoke-static {p0}, Lcom/android/phone/PhoneUtils;->toLogSafePhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
