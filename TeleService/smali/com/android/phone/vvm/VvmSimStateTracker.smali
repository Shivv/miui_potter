.class public Lcom/android/phone/vvm/VvmSimStateTracker;
.super Landroid/content/BroadcastReceiver;
.source "VvmSimStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;
    }
.end annotation


# static fields
.field private static sListeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            "Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sPreBootHandles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/android/phone/vvm/VvmSimStateTracker;->sListeners:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic -wrap0(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    invoke-static {p0, p1}, Lcom/android/phone/vvm/VvmSimStateTracker;->getTelephonyManager(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/vvm/VvmSimStateTracker;Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/vvm/VvmSimStateTracker;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VvmSimStateTracker;->sendConnected(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Lcom/android/phone/vvm/VvmSimStateTracker;->sListeners:Ljava/util/Map;

    .line 65
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/phone/vvm/VvmSimStateTracker;->sPreBootHandles:Ljava/util/Set;

    .line 50
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private checkRemovedSim(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 178
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v4

    .line 179
    .local v4, "subscriptionManager":Landroid/telephony/SubscriptionManager;
    invoke-direct {p0}, Lcom/android/phone/vvm/VvmSimStateTracker;->isBootCompleted()Z

    move-result v5

    if-nez v5, :cond_2

    .line 180
    sget-object v5, Lcom/android/phone/vvm/VvmSimStateTracker;->sPreBootHandles:Ljava/util/Set;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "phoneAccountHandle$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 181
    .local v1, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    invoke-static {v4, v1}, Lcom/android/phone/PhoneUtils;->isPhoneAccountActive(Landroid/telephony/SubscriptionManager;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 182
    sget-object v5, Lcom/android/phone/vvm/VvmSimStateTracker;->sPreBootHandles:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 185
    .end local v1    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :cond_1
    return-void

    .line 187
    .end local v2    # "phoneAccountHandle$iterator":Ljava/util/Iterator;
    :cond_2
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    .line 188
    .local v3, "removeList":Ljava/util/Set;, "Ljava/util/Set<Landroid/telecom/PhoneAccountHandle;>;"
    sget-object v5, Lcom/android/phone/vvm/VvmSimStateTracker;->sListeners:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "phoneAccountHandle$iterator":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 189
    .restart local v1    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    invoke-static {v4, v1}, Lcom/android/phone/PhoneUtils;->isPhoneAccountActive(Landroid/telephony/SubscriptionManager;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 190
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 191
    sget-object v5, Lcom/android/phone/vvm/VvmSimStateTracker;->sListeners:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;

    .line 192
    .local v0, "listener":Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;
    if-eqz v0, :cond_4

    .line 193
    invoke-virtual {v0}, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->unlisten()V

    .line 195
    :cond_4
    invoke-direct {p0, p1, v1}, Lcom/android/phone/vvm/VvmSimStateTracker;->sendSimRemoved(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_1

    .line 199
    .end local v0    # "listener":Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;
    .end local v1    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :cond_5
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 200
    .restart local v1    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    sget-object v5, Lcom/android/phone/vvm/VvmSimStateTracker;->sListeners:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 202
    .end local v1    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :cond_6
    return-void
.end method

.method private static getTelephonyManager(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 248
    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    return-object v0
.end method

.method private isBootCompleted()Z
    .locals 2

    .prologue
    .line 205
    const-string/jumbo v0, "sys.boot_completed"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private listenToAccount(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 240
    new-instance v0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;-><init>(Lcom/android/phone/vvm/VvmSimStateTracker;Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 241
    .local v0, "listener":Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;
    invoke-virtual {v0}, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->listen()V

    .line 242
    sget-object v1, Lcom/android/phone/vvm/VvmSimStateTracker;->sListeners:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    return-void
.end method

.method private onBootCompleted(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 157
    sget-object v3, Lcom/android/phone/vvm/VvmSimStateTracker;->sPreBootHandles:Ljava/util/Set;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "phoneAccountHandle$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 158
    .local v0, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    invoke-static {p1, v0}, Lcom/android/phone/vvm/VvmSimStateTracker;->getTelephonyManager(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    .line 159
    .local v2, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 162
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    move-result v3

    if-nez v3, :cond_1

    .line 163
    sget-object v3, Lcom/android/phone/vvm/VvmSimStateTracker;->sListeners:Ljava/util/Map;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    invoke-direct {p0, p1, v0}, Lcom/android/phone/vvm/VvmSimStateTracker;->sendConnected(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0

    .line 166
    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/android/phone/vvm/VvmSimStateTracker;->listenToAccount(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0

    .line 169
    .end local v0    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    .end local v2    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :cond_2
    sget-object v3, Lcom/android/phone/vvm/VvmSimStateTracker;->sPreBootHandles:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 170
    return-void
.end method

.method private onCarrierConfigChanged(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    const/4 v3, 0x0

    .line 214
    invoke-direct {p0}, Lcom/android/phone/vvm/VvmSimStateTracker;->isBootCompleted()Z

    move-result v2

    if-nez v2, :cond_0

    .line 215
    sget-object v2, Lcom/android/phone/vvm/VvmSimStateTracker;->sPreBootHandles:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216
    return-void

    .line 218
    :cond_0
    invoke-static {p1, p2}, Lcom/android/phone/vvm/VvmSimStateTracker;->getTelephonyManager(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 219
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_1

    .line 220
    const-class v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 221
    const-class v3, Landroid/telecom/TelecomManager;

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telecom/TelecomManager;

    invoke-virtual {v3, p2}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v3

    .line 220
    invoke-virtual {v2, v3}, Landroid/telephony/TelephonyManager;->getSubIdForPhoneAccount(Landroid/telecom/PhoneAccount;)I

    move-result v0

    .line 223
    .local v0, "subId":I
    const-string/jumbo v2, "VvmSimStateTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Cannot create TelephonyManager from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", subId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/phone/vvm/VvmLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    return-void

    .line 230
    .end local v0    # "subId":I
    :cond_1
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getState()I

    move-result v2

    if-nez v2, :cond_2

    .line 232
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VvmSimStateTracker;->sendConnected(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 233
    sget-object v2, Lcom/android/phone/vvm/VvmSimStateTracker;->sListeners:Ljava/util/Map;

    invoke-interface {v2, p2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    :goto_0
    return-void

    .line 235
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VvmSimStateTracker;->listenToAccount(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0
.end method

.method private sendConnected(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 173
    const-string/jumbo v0, "VvmSimStateTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Service connected on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/vvm/VvmLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-static {p1, p2}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->startCellServiceConnected(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 175
    return-void
.end method

.method private sendSimRemoved(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 209
    const-string/jumbo v0, "VvmSimStateTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Sim removed on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/vvm/VvmLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-static {p1, p2}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->startSimRemoved(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 211
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 112
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 114
    const-string/jumbo v4, "VvmSimStateTracker"

    const-string/jumbo v5, "Null action for intent."

    invoke-static {v4, v5}, Lcom/android/phone/vvm/VvmLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    return-void

    .line 117
    :cond_0
    const-string/jumbo v4, "VvmSimStateTracker"

    invoke-static {v4, v0}, Lcom/android/phone/vvm/VvmLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const-string/jumbo v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 120
    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VvmSimStateTracker;->onBootCompleted(Landroid/content/Context;)V

    .line 154
    :cond_1
    :goto_0
    return-void

    .line 118
    :cond_2
    const-string/jumbo v4, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 123
    const-string/jumbo v4, "ABSENT"

    .line 124
    const-string/jumbo v5, "ss"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 123
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 129
    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VvmSimStateTracker;->checkRemovedSim(Landroid/content/Context;)V

    goto :goto_0

    .line 118
    :cond_3
    const-string/jumbo v4, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 133
    const-string/jumbo v4, "subscription"

    .line 134
    const/4 v5, -0x1

    .line 133
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 136
    .local v2, "subId":I
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v3

    .line 137
    .local v3, "subscriptionManager":Landroid/telephony/SubscriptionManager;
    invoke-virtual {v3, v2}, Landroid/telephony/SubscriptionManager;->isActiveSubId(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 138
    const-string/jumbo v4, "VvmSimStateTracker"

    const-string/jumbo v5, "Received SIM change for invalid subscription id."

    invoke-static {v4, v5}, Lcom/android/phone/vvm/VvmLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VvmSimStateTracker;->checkRemovedSim(Landroid/content/Context;)V

    .line 140
    return-void

    .line 144
    :cond_4
    invoke-static {v2}, Lcom/android/phone/vvm/PhoneAccountHandleConverter;->fromSubId(I)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 146
    .local v1, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    const-string/jumbo v4, "null"

    invoke-virtual {v1}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 147
    const-string/jumbo v4, "VvmSimStateTracker"

    .line 148
    const-string/jumbo v5, "null phone account handle ID, possible modem crash. Ignoring carrier config changed event"

    .line 147
    invoke-static {v4, v5}, Lcom/android/phone/vvm/VvmLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    return-void

    .line 152
    :cond_5
    invoke-direct {p0, p1, v1}, Lcom/android/phone/vvm/VvmSimStateTracker;->onCarrierConfigChanged(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0
.end method
