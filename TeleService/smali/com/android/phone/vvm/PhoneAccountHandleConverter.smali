.class public Lcom/android/phone/vvm/PhoneAccountHandleConverter;
.super Ljava/lang/Object;
.source "PhoneAccountHandleConverter.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method public static fromSubId(I)Landroid/telecom/PhoneAccountHandle;
    .locals 5
    .param p0, "subId"    # I

    .prologue
    const/4 v4, 0x0

    .line 39
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 40
    const-string/jumbo v1, "PhoneAccountHndCvtr"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "invalid subId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/vvm/VvmLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    return-object v4

    .line 45
    :cond_0
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 46
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v0, :cond_1

    .line 47
    const-string/jumbo v1, "PhoneAccountHndCvtr"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to find Phone for subId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/vvm/VvmLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    return-object v4

    .line 50
    :cond_1
    invoke-static {v0}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    return-object v1
.end method

.method public static toSubId(Landroid/telecom/PhoneAccountHandle;)I
    .locals 1
    .param p0, "handle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 54
    invoke-static {p0}, Lcom/android/phone/PhoneUtils;->getSubIdForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)I

    move-result v0

    return v0
.end method
