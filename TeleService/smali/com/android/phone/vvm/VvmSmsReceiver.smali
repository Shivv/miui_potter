.class public Lcom/android/phone/vvm/VvmSmsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VvmSmsReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 38
    const-string/jumbo v3, "android.provider.extra.VOICEMAIL_SMS"

    .line 37
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telephony/VisualVoicemailSms;

    .line 40
    .local v0, "sms":Landroid/telephony/VisualVoicemailSms;
    invoke-virtual {v0}, Landroid/telephony/VisualVoicemailSms;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    if-nez v2, :cond_0

    .line 42
    const-string/jumbo v2, "VvmSmsReceiver"

    const-string/jumbo v3, "Received message for null phone account"

    invoke-static {v2, v3}, Lcom/android/phone/vvm/VvmLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    return-void

    .line 46
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/VisualVoicemailSms;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/vvm/PhoneAccountHandleConverter;->toSubId(Landroid/telecom/PhoneAccountHandle;)I

    move-result v1

    .line 47
    .local v1, "subId":I
    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 48
    const-string/jumbo v2, "VvmSmsReceiver"

    const-string/jumbo v3, "Received message for invalid subId"

    invoke-static {v2, v3}, Lcom/android/phone/vvm/VvmLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void

    .line 52
    :cond_1
    invoke-static {p1, v1}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->hasRemoteService(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 53
    const-string/jumbo v2, "VvmSmsReceiver"

    const-string/jumbo v3, "Sending SMS received event to remote service"

    invoke-static {v2, v3}, Lcom/android/phone/vvm/VvmLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-static {p1, v0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->startSmsReceived(Landroid/content/Context;Landroid/telephony/VisualVoicemailSms;)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_2
    const-string/jumbo v2, "VvmSmsReceiver"

    const-string/jumbo v3, "Sending SMS received event to remote service"

    invoke-static {v2, v3}, Lcom/android/phone/vvm/VvmLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
