.class Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;
.super Ljava/lang/Object;
.source "VisualVoicemailSmsFilterConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Reader"
.end annotation


# instance fields
.field private final mKeyPrefix:Ljava/lang/String;

.field private final mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static synthetic -wrap0(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;Ljava/lang/String;I)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static synthetic -wrap2(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap3(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/util/List;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->getStringSet(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "subId"    # I

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    invoke-static {p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;->-wrap0(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->mPrefs:Landroid/content/SharedPreferences;

    .line 158
    invoke-static {p2, p3}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;->-wrap1(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->mKeyPrefix:Ljava/lang/String;

    .line 159
    return-void
.end method

.method private getBoolean(Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->mPrefs:Landroid/content/SharedPreferences;

    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->makeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private getInt(Ljava/lang/String;I)I
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->mPrefs:Landroid/content/SharedPreferences;

    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->makeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->mPrefs:Landroid/content/SharedPreferences;

    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->makeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getStringSet(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "defaultValue":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 174
    iget-object v1, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->mPrefs:Landroid/content/SharedPreferences;

    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->makeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 175
    .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v0, :cond_0

    .line 176
    return-object p2

    .line 178
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private makeKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->mKeyPrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
