.class public Lcom/android/phone/NotificationMgr;
.super Ljava/lang/Object;
.source "NotificationMgr.java"


# static fields
.field private static final DBG:Z

.field private static final LOG_TAG:Ljava/lang/String;

.field static final PHONES_PROJECTION:[Ljava/lang/String;

.field private static sInstance:Lcom/android/phone/NotificationMgr;


# instance fields
.field private mApp:Lcom/android/phone/PhoneGlobals;

.field private mContext:Landroid/content/Context;

.field private mMwiVisible:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mPhoneCount:I

.field private mSelectedUnavailableNotify:[Z

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mSubscriptionManager:Landroid/telephony/SubscriptionManager;

.field private mTelecomManager:Landroid/telecom/TelecomManager;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mToast:Landroid/widget/Toast;

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    const-class v0, Lcom/android/phone/NotificationMgr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    .line 85
    sput-boolean v2, Lcom/android/phone/NotificationMgr;->DBG:Z

    .line 175
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 176
    const-string/jumbo v1, "number"

    aput-object v1, v0, v2

    .line 177
    const-string/jumbo v1, "display_name"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 178
    const-string/jumbo v1, "_id"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 175
    sput-object v0, Lcom/android/phone/NotificationMgr;->PHONES_PROJECTION:[Ljava/lang/String;

    .line 83
    return-void
.end method

.method private constructor <init>(Lcom/android/phone/PhoneGlobals;)V
    .locals 3
    .param p1, "app"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    iput-object v1, p0, Lcom/android/phone/NotificationMgr;->mMwiVisible:Landroid/util/ArrayMap;

    .line 135
    iput-object p1, p0, Lcom/android/phone/NotificationMgr;->mApp:Lcom/android/phone/PhoneGlobals;

    .line 136
    iput-object p1, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    .line 138
    const-string/jumbo v1, "notification"

    invoke-virtual {p1, v1}, Lcom/android/phone/PhoneGlobals;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 137
    iput-object v1, p0, Lcom/android/phone/NotificationMgr;->mNotificationManager:Landroid/app/NotificationManager;

    .line 140
    const-string/jumbo v1, "statusbar"

    invoke-virtual {p1, v1}, Lcom/android/phone/PhoneGlobals;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    .line 139
    iput-object v1, p0, Lcom/android/phone/NotificationMgr;->mStatusBarManager:Landroid/app/StatusBarManager;

    .line 141
    const-string/jumbo v1, "user"

    invoke-virtual {p1, v1}, Lcom/android/phone/PhoneGlobals;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    iput-object v1, p0, Lcom/android/phone/NotificationMgr;->mUserManager:Landroid/os/UserManager;

    .line 142
    iget-object v1, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/NotificationMgr;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    .line 143
    iget-object v1, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/NotificationMgr;->mTelecomManager:Landroid/telecom/TelecomManager;

    .line 144
    const-string/jumbo v1, "phone"

    invoke-virtual {p1, v1}, Lcom/android/phone/PhoneGlobals;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/android/phone/NotificationMgr;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 147
    iget-object v1, p0, Lcom/android/phone/NotificationMgr;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v1

    iput v1, p0, Lcom/android/phone/NotificationMgr;->mPhoneCount:I

    .line 148
    iget v1, p0, Lcom/android/phone/NotificationMgr;->mPhoneCount:I

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    .line 149
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/android/phone/NotificationMgr;->mPhoneCount:I

    if-ge v0, v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    :cond_0
    return-void
.end method

.method private cancelNetworkSelection(I)V
    .locals 4
    .param p1, "phoneId"    # I

    .prologue
    .line 749
    sget-boolean v0, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "cancelNetworkSelection()..."

    invoke-direct {p0, v0}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 751
    :cond_0
    iget-object v0, p0, Lcom/android/phone/NotificationMgr;->mNotificationManager:Landroid/app/NotificationManager;

    .line 754
    const/4 v1, 0x6

    invoke-direct {p0, v1, p1}, Lcom/android/phone/NotificationMgr;->getNotificationId(II)I

    move-result v1

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v3, 0x0

    .line 751
    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    .line 755
    return-void
.end method

.method private getNotificationId(II)I
    .locals 1
    .param p1, "notificationId"    # I
    .param p2, "slotId"    # I

    .prologue
    .line 893
    mul-int/lit8 v0, p2, 0x32

    add-int/2addr v0, p1

    return v0
.end method

.method private getShowVoicemailIntentForDefaultDialer(Landroid/os/UserHandle;)Landroid/content/Intent;
    .locals 3
    .param p1, "userHandle"    # Landroid/os/UserHandle;

    .prologue
    .line 482
    iget-object v1, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v2

    .line 481
    invoke-static {v1, v2}, Landroid/telecom/DefaultDialerManager;->getDefaultDialerApplication(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 483
    .local v0, "dialerPackage":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.telephony.action.SHOW_VOICEMAIL_NOTIFICATION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method static init(Lcom/android/phone/PhoneGlobals;)Lcom/android/phone/NotificationMgr;
    .locals 4
    .param p0, "app"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    .line 164
    const-class v1, Lcom/android/phone/NotificationMgr;

    monitor-enter v1

    .line 165
    :try_start_0
    sget-object v0, Lcom/android/phone/NotificationMgr;->sInstance:Lcom/android/phone/NotificationMgr;

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Lcom/android/phone/NotificationMgr;

    invoke-direct {v0, p0}, Lcom/android/phone/NotificationMgr;-><init>(Lcom/android/phone/PhoneGlobals;)V

    sput-object v0, Lcom/android/phone/NotificationMgr;->sInstance:Lcom/android/phone/NotificationMgr;

    .line 170
    :goto_0
    sget-object v0, Lcom/android/phone/NotificationMgr;->sInstance:Lcom/android/phone/NotificationMgr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 168
    :cond_0
    :try_start_1
    sget-object v0, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "init() called multiple times!  sInstance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/phone/NotificationMgr;->sInstance:Lcom/android/phone/NotificationMgr;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isUiccCardProvisioned(I)Z
    .locals 10
    .param p1, "subId"    # I

    .prologue
    const/4 v7, 0x1

    .line 864
    const/4 v1, 0x1

    .line 865
    .local v1, "PROVISIONED":I
    const/4 v0, -0x1

    .line 866
    .local v0, "INVALID_STATE":I
    const/4 v5, -0x1

    .line 868
    .local v5, "provisionStatus":I
    const-string/jumbo v8, "extphone"

    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v8

    .line 867
    invoke-static {v8}, Lorg/codeaurora/internal/IExtTelephony$Stub;->asInterface(Landroid/os/IBinder;)Lorg/codeaurora/internal/IExtTelephony;

    move-result-object v4

    .line 869
    .local v4, "mExtTelephony":Lorg/codeaurora/internal/IExtTelephony;
    invoke-static {}, Lcom/android/internal/telephony/SubscriptionController;->getInstance()Lcom/android/internal/telephony/SubscriptionController;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/android/internal/telephony/SubscriptionController;->getSlotIndex(I)I

    move-result v6

    .line 872
    .local v6, "slotId":I
    :try_start_0
    invoke-interface {v4, v6}, Lorg/codeaurora/internal/IExtTelephony;->getCurrentUiccCardProvisioningStatus(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 880
    :cond_0
    :goto_0
    if-ne v5, v7, :cond_1

    :goto_1
    return v7

    .line 876
    :catch_0
    move-exception v3

    .line 877
    .local v3, "ex":Ljava/lang/NullPointerException;
    const/4 v5, -0x1

    .line 878
    sget-boolean v8, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Failed to get status for slotId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 873
    .end local v3    # "ex":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v2

    .line 874
    .local v2, "ex":Landroid/os/RemoteException;
    const/4 v5, -0x1

    .line 875
    sget-boolean v8, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Failed to get status for slotId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 880
    .end local v2    # "ex":Landroid/os/RemoteException;
    :cond_1
    const/4 v7, 0x0

    goto :goto_1
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 884
    sget-object v0, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    return-void
.end method

.method private logi(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 888
    sget-object v0, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    return-void
.end method

.method private maybeSendVoicemailNotificationUsingDefaultDialer(Lcom/android/internal/telephony/Phone;Ljava/lang/Integer;Ljava/lang/String;Landroid/app/PendingIntent;ZLandroid/os/UserHandle;ZI)Z
    .locals 3
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "count"    # Ljava/lang/Integer;
    .param p3, "number"    # Ljava/lang/String;
    .param p4, "pendingIntent"    # Landroid/app/PendingIntent;
    .param p5, "isSettingsIntent"    # Z
    .param p6, "userHandle"    # Landroid/os/UserHandle;
    .param p7, "isRefresh"    # Z
    .param p8, "subId"    # I

    .prologue
    const/4 v2, 0x0

    .line 446
    invoke-direct {p0, p6}, Lcom/android/phone/NotificationMgr;->shouldManageNotificationThroughDefaultDialer(Landroid/os/UserHandle;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 447
    invoke-direct {p0, p6}, Lcom/android/phone/NotificationMgr;->getShowVoicemailIntentForDefaultDialer(Landroid/os/UserHandle;)Landroid/content/Intent;

    move-result-object v0

    .line 448
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 449
    const-string/jumbo v1, "android.telephony.action.SHOW_VOICEMAIL_NOTIFICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 450
    const-string/jumbo v1, "android.telephony.extra.PHONE_ACCOUNT_HANDLE"

    .line 451
    invoke-static {p1}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 450
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 452
    const-string/jumbo v1, "android.telephony.extra.IS_REFRESH"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 453
    const-string/jumbo v1, "sub_id"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 454
    if-eqz p2, :cond_0

    .line 455
    const-string/jumbo v1, "android.telephony.extra.NOTIFICATION_COUNT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 461
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_3

    .line 462
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 463
    const-string/jumbo v1, "android.telephony.extra.VOICEMAIL_NUMBER"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 466
    :cond_2
    if-eqz p4, :cond_3

    .line 467
    if-eqz p5, :cond_4

    .line 468
    const-string/jumbo v1, "android.telephony.extra.LAUNCH_VOICEMAIL_SETTINGS_INTENT"

    .line 467
    :goto_0
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 473
    :cond_3
    iget-object v1, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "android.permission.READ_PHONE_STATE"

    invoke-virtual {v1, v0, p6, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 474
    const/4 v1, 0x1

    return v1

    .line 469
    :cond_4
    const-string/jumbo v1, "android.telephony.extra.CALL_VOICEMAIL_INTENT"

    goto :goto_0

    .line 477
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    return v2
.end method

.method private shouldManageNotificationThroughDefaultDialer(Landroid/os/UserHandle;)Z
    .locals 4
    .param p1, "userHandle"    # Landroid/os/UserHandle;

    .prologue
    const/4 v2, 0x0

    .line 488
    invoke-direct {p0, p1}, Lcom/android/phone/NotificationMgr;->getShowVoicemailIntentForDefaultDialer(Landroid/os/UserHandle;)Landroid/content/Intent;

    move-result-object v0

    .line 489
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 490
    return v2

    .line 493
    :cond_0
    iget-object v3, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 495
    .local v1, "receivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method private showNetworkSelection(Ljava/lang/String;II)V
    .locals 8
    .param p1, "operator"    # Ljava/lang/String;
    .param p2, "subId"    # I
    .param p3, "phoneId"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 685
    sget-boolean v2, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "showNetworkSelection("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 687
    :cond_0
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 688
    const v3, 0x108008a

    .line 687
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 689
    iget-object v3, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0495

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 687
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 691
    iget-object v3, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v6

    const v5, 0x7f0b0496

    invoke-virtual {v3, v5, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 687
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 694
    const-string/jumbo v3, "alert"

    .line 687
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setChannel(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 697
    .local v0, "builder":Landroid/app/Notification$Builder;
    iget-object v2, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-static {v2, v0, p3}, Lcom/android/phone/MiuiNotificationMgrUtils;->configNetworkSelectionNotification(Landroid/content/Context;Landroid/app/Notification$Builder;I)V

    .line 700
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 701
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 727
    new-instance v2, Landroid/content/ComponentName;

    const-string/jumbo v3, "com.android.phone"

    const-string/jumbo v4, "com.android.phone.settings.NetworkSetting"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 728
    invoke-static {v1, p3, p3, p2}, Lmiui/telephony/SubscriptionManager;->putSlotIdPhoneIdAndSubIdExtra(Landroid/content/Intent;III)V

    .line 733
    iget-object v2, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-static {v2, p3, v1, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 734
    iget-object v2, p0, Lcom/android/phone/NotificationMgr;->mNotificationManager:Landroid/app/NotificationManager;

    .line 738
    const/4 v3, 0x6

    invoke-direct {p0, v3, p3}, Lcom/android/phone/NotificationMgr;->getNotificationId(II)I

    move-result v3

    .line 739
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    .line 740
    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    .line 735
    const/4 v6, 0x0

    .line 734
    invoke-virtual {v2, v6, v3, v4, v5}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    .line 741
    return-void
.end method


# virtual methods
.method hideDataDisconnectedRoaming()V
    .locals 2

    .prologue
    .line 673
    sget-boolean v0, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "hideDataDisconnectedRoaming()..."

    invoke-direct {p0, v0}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/android/phone/NotificationMgr;->mNotificationManager:Landroid/app/NotificationManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 675
    return-void
.end method

.method postTransientNotification(ILjava/lang/CharSequence;)V
    .locals 2
    .param p1, "notifyId"    # I
    .param p2, "msg"    # Ljava/lang/CharSequence;

    .prologue
    .line 855
    iget-object v0, p0, Lcom/android/phone/NotificationMgr;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 856
    iget-object v0, p0, Lcom/android/phone/NotificationMgr;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 859
    :cond_0
    iget-object v0, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/NotificationMgr;->mToast:Landroid/widget/Toast;

    .line 860
    iget-object v0, p0, Lcom/android/phone/NotificationMgr;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 861
    return-void
.end method

.method refreshMwi(I)V
    .locals 6
    .param p1, "subId"    # I

    .prologue
    const/4 v5, 0x1

    .line 191
    const/4 v3, -0x1

    if-ne p1, v3, :cond_1

    .line 192
    iget-object v3, p0, Lcom/android/phone/NotificationMgr;->mMwiVisible:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 193
    iget-object v3, p0, Lcom/android/phone/NotificationMgr;->mMwiVisible:Landroid/util/ArrayMap;

    invoke-virtual {v3}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 194
    .local v1, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 195
    .local v0, "keyIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 196
    return-void

    .line 198
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 201
    .end local v0    # "keyIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v1    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_1
    iget-object v3, p0, Lcom/android/phone/NotificationMgr;->mMwiVisible:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 202
    iget-object v3, p0, Lcom/android/phone/NotificationMgr;->mMwiVisible:Landroid/util/ArrayMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 203
    .local v2, "mwiVisible":Z
    if-eqz v2, :cond_2

    .line 204
    iget-object v3, p0, Lcom/android/phone/NotificationMgr;->mApp:Lcom/android/phone/PhoneGlobals;

    iget-object v3, v3, Lcom/android/phone/PhoneGlobals;->notifier:Lcom/android/phone/CallNotifier;

    invoke-virtual {v3, v5}, Lcom/android/phone/CallNotifier;->updatePhoneStateListeners(Z)V

    .line 207
    .end local v2    # "mwiVisible":Z
    :cond_2
    return-void
.end method

.method showDataDisconnectedRoaming()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 620
    sget-boolean v8, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v8, :cond_0

    const-string/jumbo v8, "showDataDisconnectedRoaming()..."

    invoke-direct {p0, v8}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 625
    :cond_0
    new-instance v5, Landroid/content/Intent;

    iget-object v8, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const-class v9, Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-direct {v5, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 627
    .local v5, "intent":Landroid/content/Intent;
    const/4 v6, 0x0

    .line 629
    .local v6, "isVendorNetworkSettingApkAvailable":Z
    const-string/jumbo v8, "extphone"

    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lorg/codeaurora/internal/IExtTelephony$Stub;->asInterface(Landroid/os/IBinder;)Lorg/codeaurora/internal/IExtTelephony;

    move-result-object v4

    .line 631
    .local v4, "extTelephony":Lorg/codeaurora/internal/IExtTelephony;
    if-eqz v4, :cond_1

    .line 632
    :try_start_0
    const-string/jumbo v8, "com.qualcomm.qti.networksetting"

    invoke-interface {v4, v8}, Lorg/codeaurora/internal/IExtTelephony;->isVendorApkAvailable(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 631
    if-eqz v8, :cond_1

    .line 633
    const/4 v6, 0x1

    .line 640
    :cond_1
    :goto_0
    if-eqz v6, :cond_3

    .line 642
    new-instance v8, Landroid/content/ComponentName;

    const-string/jumbo v9, "com.qualcomm.qti.networksetting"

    .line 643
    const-string/jumbo v10, "com.qualcomm.qti.networksetting.MobileNetworkSettings"

    .line 642
    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 649
    :goto_1
    iget-object v8, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-static {v8, v11, v5, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 651
    .local v1, "contentIntent":Landroid/app/PendingIntent;
    iget-object v8, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const v9, 0x7f0b03ac

    invoke-virtual {v8, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 653
    .local v2, "contentText":Ljava/lang/CharSequence;
    new-instance v8, Landroid/app/Notification$Builder;

    iget-object v9, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 654
    const v9, 0x108008a

    .line 653
    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 655
    iget-object v9, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const v10, 0x7f0b03a9

    invoke-virtual {v9, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    .line 653
    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 656
    iget-object v9, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f08003e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 653
    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    .line 658
    const-string/jumbo v9, "mobileDataAlertNew"

    .line 653
    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setChannel(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 660
    .local v0, "builder":Landroid/app/Notification$Builder;
    if-eqz v6, :cond_2

    .line 661
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 664
    :cond_2
    new-instance v8, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v8, v0}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v8, v2}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v7

    .line 665
    .local v7, "notif":Landroid/app/Notification;
    iget-object v8, p0, Lcom/android/phone/NotificationMgr;->mNotificationManager:Landroid/app/NotificationManager;

    .line 666
    sget-object v9, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v10, 0x5

    .line 665
    invoke-virtual {v8, v12, v10, v7, v9}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    .line 667
    return-void

    .line 635
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    .end local v1    # "contentIntent":Landroid/app/PendingIntent;
    .end local v2    # "contentText":Ljava/lang/CharSequence;
    .end local v7    # "notif":Landroid/app/Notification;
    :catch_0
    move-exception v3

    .line 637
    .local v3, "ex":Landroid/os/RemoteException;
    const-string/jumbo v8, "couldn\'t connect to extphone service, launch the default activity"

    invoke-direct {p0, v8}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 646
    .end local v3    # "ex":Landroid/os/RemoteException;
    :cond_3
    const-string/jumbo v8, "vendor MobileNetworkSettings is not available"

    invoke-direct {p0, v8}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method updateCfi(IZ)V
    .locals 1
    .param p1, "subId"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 504
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/phone/NotificationMgr;->updateCfi(IZZ)V

    .line 505
    return-void
.end method

.method updateCfi(IZZ)V
    .locals 22
    .param p1, "subId"    # I
    .param p2, "visible"    # Z
    .param p3, "isRefresh"    # Z

    .prologue
    .line 513
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "updateCfi: subId= "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ", visible="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    if-eqz p2, :cond_0

    const-string/jumbo v18, "Y"

    :goto_0
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/phone/NotificationMgr;->logi(Ljava/lang/String;)V

    .line 515
    invoke-static/range {p1 .. p1}, Lcom/android/phone/PhoneGlobals;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v11

    .line 516
    .local v11, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v11, :cond_1

    .line 517
    sget-object v18, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v19, "updateCfi: phone is null, returning..."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    return-void

    .line 513
    .end local v11    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_0
    const-string/jumbo v18, "N"

    goto :goto_0

    .line 520
    .restart local v11    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    invoke-virtual {v11}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v12

    .line 523
    .local v12, "phoneId":I
    if-eqz p2, :cond_6

    invoke-direct/range {p0 .. p1}, Lcom/android/phone/NotificationMgr;->isUiccCardProvisioned(I)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v14

    .line 536
    .local v14, "subInfo":Landroid/telephony/SubscriptionInfo;
    if-nez v14, :cond_2

    .line 537
    sget-object v18, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "Found null subscription info for: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    return-void

    .line 542
    :cond_2
    const v13, 0x7f0200a9

    .line 543
    .local v13, "resId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_5

    .line 544
    invoke-static {}, Lcom/android/internal/telephony/SubscriptionController;->getInstance()Lcom/android/internal/telephony/SubscriptionController;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/SubscriptionController;->getSlotIndex(I)I

    move-result v8

    .line 545
    .local v8, "mSlotId":I
    if-nez v8, :cond_4

    const v13, 0x7f0200aa

    .line 549
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f0b05f4

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 556
    .end local v8    # "mSlotId":I
    .local v10, "notificationTitle":Ljava/lang/String;
    :goto_2
    new-instance v18, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v18

    .line 558
    invoke-virtual {v14}, Landroid/telephony/SubscriptionInfo;->getIconTint()I

    move-result v19

    .line 556
    invoke-virtual/range {v18 .. v19}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v18

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v12}, Lcom/android/phone/MiuiNotificationMgrUtils;->getCallForwardNotiContentText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v19

    .line 556
    invoke-virtual/range {v18 .. v19}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v18

    .line 563
    const/16 v19, 0x0

    .line 556
    invoke-virtual/range {v18 .. v19}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v18

    .line 564
    const/16 v19, 0x1

    .line 556
    invoke-virtual/range {v18 .. v19}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v18

    .line 565
    const-string/jumbo v19, "callForward"

    .line 556
    invoke-virtual/range {v18 .. v19}, Landroid/app/Notification$Builder;->setChannel(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v5

    .line 569
    .local v5, "builder":Landroid/app/Notification$Builder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/android/phone/MiuiNotificationMgrUtils;->getCallForwardStatusIcon(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 570
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/content/Intent;

    const-string/jumbo v18, "android.intent.action.MAIN"

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 571
    .local v7, "intent":Landroid/content/Intent;
    const/high16 v18, 0x14000000

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 574
    const-string/jumbo v18, "com.android.phone"

    const-string/jumbo v19, "com.android.phone.settings.CallFeaturesSetting"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 576
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v18

    .line 575
    move-object/from16 v0, v18

    invoke-static {v7, v0}, Lcom/android/phone/SubscriptionInfoHelper;->addExtrasToIntent(Landroid/content/Intent;Landroid/telephony/SubscriptionInfo;)V

    .line 578
    invoke-virtual {v5, v4}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    .line 581
    const/16 v19, 0x0

    .line 580
    move-object/from16 v0, v18

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-static {v0, v1, v7, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 583
    invoke-virtual {v5}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v9

    .line 584
    .local v9, "notification":Landroid/app/Notification;
    iget-object v0, v9, Landroid/app/Notification;->extraNotification:Landroid/app/MiuiNotification;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/app/MiuiNotification;->setCustomizedIcon(Z)Landroid/app/MiuiNotification;

    .line 587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mNotificationManager:Landroid/app/NotificationManager;

    move-object/from16 v18, v0

    .line 588
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v19

    .line 592
    const/16 v20, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/phone/NotificationMgr;->getNotificationId(II)I

    move-result v20

    .line 595
    sget-object v21, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    .line 587
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v9, v3}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    .line 612
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "builder":Landroid/app/Notification$Builder;
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v9    # "notification":Landroid/app/Notification;
    .end local v10    # "notificationTitle":Ljava/lang/String;
    .end local v13    # "resId":I
    .end local v14    # "subInfo":Landroid/telephony/SubscriptionInfo;
    :cond_3
    return-void

    .line 546
    .restart local v8    # "mSlotId":I
    .restart local v13    # "resId":I
    .restart local v14    # "subInfo":Landroid/telephony/SubscriptionInfo;
    :cond_4
    const v13, 0x7f0200ab

    goto/16 :goto_1

    .line 553
    .end local v8    # "mSlotId":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f0b05f4

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "notificationTitle":Ljava/lang/String;
    goto/16 :goto_2

    .line 597
    .end local v10    # "notificationTitle":Ljava/lang/String;
    .end local v13    # "resId":I
    .end local v14    # "subInfo":Landroid/telephony/SubscriptionInfo;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mUserManager:Landroid/os/UserManager;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    move-result-object v17

    .line 598
    .local v17, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_3

    .line 599
    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/pm/UserInfo;

    .line 600
    .local v15, "user":Landroid/content/pm/UserInfo;
    invoke-virtual {v15}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 598
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 603
    :cond_7
    invoke-virtual {v15}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v16

    .line 604
    .local v16, "userHandle":Landroid/os/UserHandle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/NotificationMgr;->mNotificationManager:Landroid/app/NotificationManager;

    move-object/from16 v18, v0

    .line 605
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v19

    .line 608
    const/16 v20, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/phone/NotificationMgr;->getNotificationId(II)I

    move-result v20

    .line 604
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    move-object/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    goto :goto_4
.end method

.method updateMwi(IZ)V
    .locals 1
    .param p1, "subId"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 236
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/phone/NotificationMgr;->updateMwi(IZZ)V

    .line 237
    return-void
.end method

.method updateMwi(IZZ)V
    .locals 32
    .param p1, "subId"    # I
    .param p2, "visible"    # Z
    .param p3, "isRefresh"    # Z

    .prologue
    .line 248
    sget-boolean v2, Lcom/android/phone/PhoneGlobals;->sVoiceCapable:Z

    if-nez v2, :cond_0

    .line 251
    sget-object v2, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v9, "Called updateMwi() on non-voice-capable device! Ignoring..."

    invoke-static {v2, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    return-void

    .line 255
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/android/phone/PhoneGlobals;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    .line 256
    .local v3, "phone":Lcom/android/internal/telephony/Phone;
    sget-object v2, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "updateMwi(): subId "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " update to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mMwiVisible:Landroid/util/ArrayMap;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    if-eqz p2, :cond_c

    invoke-direct/range {p0 .. p1}, Lcom/android/phone/NotificationMgr;->isUiccCardProvisioned(I)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 260
    if-nez v3, :cond_1

    .line 261
    sget-object v2, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Found null phone for: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    return-void

    .line 265
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v28

    .line 266
    .local v28, "subInfo":Landroid/telephony/SubscriptionInfo;
    if-nez v28, :cond_2

    .line 267
    sget-object v2, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Found null subscription info for: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    return-void

    .line 271
    :cond_2
    const v27, 0x108007e

    .line 272
    .local v27, "resId":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v2

    const/4 v9, 0x1

    if-le v2, v9, :cond_3

    .line 273
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    if-nez v2, :cond_6

    const v27, 0x7f0200a5

    .line 287
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const v9, 0x7f0b0491

    invoke-virtual {v2, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 288
    .local v24, "notificationTitle":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getVoiceMailNumber()Ljava/lang/String;

    move-result-object v5

    .line 289
    .local v5, "vmNumber":Ljava/lang/String;
    sget-boolean v2, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "- got vm number: \'"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v9, "\'"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 297
    :cond_4
    if-nez v5, :cond_7

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getIccRecordsLoaded()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_7

    .line 298
    sget-boolean v2, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v2, :cond_5

    const-string/jumbo v2, "- Null vm number: SIM records not loaded (yet)..."

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 299
    :cond_5
    return-void

    .line 274
    .end local v5    # "vmNumber":Ljava/lang/String;
    .end local v24    # "notificationTitle":Ljava/lang/String;
    :cond_6
    const v27, 0x7f0200a6

    goto :goto_0

    .line 302
    .restart local v5    # "vmNumber":Ljava/lang/String;
    .restart local v24    # "notificationTitle":Ljava/lang/String;
    :cond_7
    const/4 v4, 0x0

    .line 304
    .local v4, "vmCount":Ljava/lang/Integer;
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyCapabilities;->supportsVoiceMessageCount(Lcom/android/internal/telephony/Phone;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 305
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getVoiceMessageCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 306
    .local v4, "vmCount":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const v9, 0x7f0b0492

    invoke-virtual {v2, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 307
    .local v29, "titleFormat":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v2, v9

    move-object/from16 v0, v29

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    .line 311
    .end local v4    # "vmCount":Ljava/lang/Integer;
    .end local v29    # "titleFormat":Ljava/lang/String;
    :cond_8
    invoke-static {v3}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v25

    .line 315
    .local v25, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    .line 317
    .local v7, "isSettingsIntent":Z
    if-eqz v7, :cond_a

    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    .line 319
    const v9, 0x7f0b0494

    .line 318
    invoke-virtual {v2, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 323
    .local v23, "notificationText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    .line 324
    const v9, 0x7f0b0494

    .line 323
    invoke-virtual {v2, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 325
    new-instance v21, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.phone.CallFeaturesSetting.ADD_VOICEMAIL"

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 326
    .local v21, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.android.phone.settings.SubscriptionInfoHelper.SubscriptionId"

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 329
    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-static {v0, v1}, Lmiui/telephony/SubscriptionManager;->putSubscriptionIdExtra(Landroid/content/Intent;I)V

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const-class v9, Lcom/android/phone/settings/VoicemailSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 351
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v10

    invoke-static {v9, v10}, Lcom/android/phone/MiuiNotificationMgrUtils;->getSimCardDesp(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const/4 v9, 0x0

    move/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v2, v0, v1, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 356
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    .line 357
    .local v26, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v19

    .line 359
    .local v19, "carrierConfig":Landroid/os/PersistableBundle;
    new-instance v18, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 360
    .local v18, "builder":Landroid/app/Notification$Builder;
    move-object/from16 v0, v18

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 361
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 360
    invoke-virtual {v2, v10, v11}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 362
    invoke-virtual/range {v28 .. v28}, Landroid/telephony/SubscriptionInfo;->getIconTint()I

    move-result v9

    .line 360
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 366
    const v9, 0x7f08003e

    move-object/from16 v0, v26

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 360
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 368
    const/4 v9, 0x1

    .line 360
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 370
    const-string/jumbo v9, "voicemail_notification_persistent_bool"

    .line 369
    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 360
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 371
    const-string/jumbo v9, "voiceMail"

    .line 360
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setChannel(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    .line 375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/phone/MiuiNotificationMgrUtils;->getVoiceMailStatusIcon(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 376
    invoke-virtual/range {v18 .. v18}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v22

    .line 378
    .local v22, "notification":Landroid/app/Notification;
    move-object/from16 v0, v22

    iget-object v2, v0, Landroid/app/Notification;->extraNotification:Landroid/app/MiuiNotification;

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/app/MiuiNotification;->setCustomizedIcon(Z)Landroid/app/MiuiNotification;

    .line 379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mUserManager:Landroid/os/UserManager;

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    move-result-object v31

    .line 380
    .local v31, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_2
    invoke-interface/range {v31 .. v31}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v20

    if-ge v0, v2, :cond_e

    .line 381
    move-object/from16 v0, v31

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Landroid/content/pm/UserInfo;

    .line 383
    .local v30, "user":Landroid/content/pm/UserInfo;
    invoke-static/range {v30 .. v30}, Lmiui/securityspace/XSpaceUserHandle;->isXSpaceUser(Landroid/content/pm/UserInfo;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 380
    :cond_9
    :goto_3
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 339
    .end local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v18    # "builder":Landroid/app/Notification$Builder;
    .end local v19    # "carrierConfig":Landroid/os/PersistableBundle;
    .end local v20    # "i":I
    .end local v21    # "intent":Landroid/content/Intent;
    .end local v22    # "notification":Landroid/app/Notification;
    .end local v23    # "notificationText":Ljava/lang/String;
    .end local v26    # "res":Landroid/content/res/Resources;
    .end local v30    # "user":Landroid/content/pm/UserInfo;
    .end local v31    # "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    const v9, 0x7f0b0493

    invoke-virtual {v2, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 338
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    .line 340
    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    aput-object v10, v9, v11

    .line 338
    invoke-static {v2, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    .line 346
    .restart local v23    # "notificationText":Ljava/lang/String;
    new-instance v21, Landroid/content/Intent;

    .line 347
    const-string/jumbo v2, "android.intent.action.CALL"

    const-string/jumbo v9, "voicemail"

    const/4 v10, 0x0

    invoke-static {v9, v5, v10}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 346
    move-object/from16 v0, v21

    invoke-direct {v0, v2, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 348
    .restart local v21    # "intent":Landroid/content/Intent;
    const-string/jumbo v2, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 387
    .restart local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .restart local v18    # "builder":Landroid/app/Notification$Builder;
    .restart local v19    # "carrierConfig":Landroid/os/PersistableBundle;
    .restart local v20    # "i":I
    .restart local v22    # "notification":Landroid/app/Notification;
    .restart local v26    # "res":Landroid/content/res/Resources;
    .restart local v30    # "user":Landroid/content/pm/UserInfo;
    .restart local v31    # "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :cond_b
    invoke-virtual/range {v30 .. v30}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v8

    .line 388
    .local v8, "userHandle":Landroid/os/UserHandle;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mUserManager:Landroid/os/UserManager;

    .line 389
    const-string/jumbo v9, "no_outgoing_calls"

    .line 388
    invoke-virtual {v2, v9, v8}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 390
    invoke-virtual/range {v30 .. v30}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 388
    if-eqz v2, :cond_9

    move-object/from16 v2, p0

    move/from16 v9, p3

    move/from16 v10, p1

    .line 391
    invoke-direct/range {v2 .. v10}, Lcom/android/phone/NotificationMgr;->maybeSendVoicemailNotificationUsingDefaultDialer(Lcom/android/internal/telephony/Phone;Ljava/lang/Integer;Ljava/lang/String;Landroid/app/PendingIntent;ZLandroid/os/UserHandle;ZI)Z

    move-result v2

    if-nez v2, :cond_9

    .line 393
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mNotificationManager:Landroid/app/NotificationManager;

    .line 394
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    .line 397
    const/4 v10, 0x3

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v10, v1}, Lcom/android/phone/NotificationMgr;->getNotificationId(II)I

    move-result v10

    .line 393
    move-object/from16 v0, v22

    invoke-virtual {v2, v9, v10, v0, v8}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    goto :goto_3

    .line 405
    .end local v5    # "vmNumber":Ljava/lang/String;
    .end local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v7    # "isSettingsIntent":Z
    .end local v8    # "userHandle":Landroid/os/UserHandle;
    .end local v18    # "builder":Landroid/app/Notification$Builder;
    .end local v19    # "carrierConfig":Landroid/os/PersistableBundle;
    .end local v20    # "i":I
    .end local v21    # "intent":Landroid/content/Intent;
    .end local v22    # "notification":Landroid/app/Notification;
    .end local v23    # "notificationText":Ljava/lang/String;
    .end local v24    # "notificationTitle":Ljava/lang/String;
    .end local v25    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    .end local v26    # "res":Landroid/content/res/Resources;
    .end local v27    # "resId":I
    .end local v28    # "subInfo":Landroid/telephony/SubscriptionInfo;
    .end local v30    # "user":Landroid/content/pm/UserInfo;
    .end local v31    # "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mUserManager:Landroid/os/UserManager;

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    move-result-object v31

    .line 406
    .restart local v31    # "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    const/16 v20, 0x0

    .restart local v20    # "i":I
    :goto_4
    invoke-interface/range {v31 .. v31}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v20

    if-ge v0, v2, :cond_e

    .line 407
    move-object/from16 v0, v31

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Landroid/content/pm/UserInfo;

    .line 408
    .restart local v30    # "user":Landroid/content/pm/UserInfo;
    invoke-virtual/range {v30 .. v30}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v8

    .line 409
    .restart local v8    # "userHandle":Landroid/os/UserHandle;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mUserManager:Landroid/os/UserManager;

    .line 410
    const-string/jumbo v9, "no_outgoing_calls"

    .line 409
    invoke-virtual {v2, v9, v8}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 411
    invoke-virtual/range {v30 .. v30}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 409
    if-eqz v2, :cond_d

    .line 412
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 413
    const/4 v14, 0x0

    move-object/from16 v9, p0

    move-object v10, v3

    move-object v15, v8

    move/from16 v16, p3

    move/from16 v17, p1

    .line 412
    invoke-direct/range {v9 .. v17}, Lcom/android/phone/NotificationMgr;->maybeSendVoicemailNotificationUsingDefaultDialer(Lcom/android/internal/telephony/Phone;Ljava/lang/Integer;Ljava/lang/String;Landroid/app/PendingIntent;ZLandroid/os/UserHandle;ZI)Z

    move-result v2

    if-nez v2, :cond_d

    .line 414
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/NotificationMgr;->mNotificationManager:Landroid/app/NotificationManager;

    .line 415
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    .line 418
    const/4 v10, 0x3

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v10, v1}, Lcom/android/phone/NotificationMgr;->getNotificationId(II)I

    move-result v10

    .line 414
    invoke-virtual {v2, v9, v10, v8}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    .line 406
    :cond_d
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    .line 424
    .end local v8    # "userHandle":Landroid/os/UserHandle;
    .end local v30    # "user":Landroid/content/pm/UserInfo;
    :cond_e
    return-void
.end method

.method updateNetworkSelection(III)V
    .locals 10
    .param p1, "serviceState"    # I
    .param p2, "subId"    # I
    .param p3, "phoneId"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 769
    invoke-static {p3}, Landroid/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 770
    const-string/jumbo v5, "invalid phoneId"

    invoke-direct {p0, v5}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 771
    return-void

    .line 773
    :cond_0
    invoke-static {p3}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    .line 774
    .local v3, "phone":Lcom/android/internal/telephony/Phone;
    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/telephony/TelephonyManagerEx;->hasIccCard()Z

    move-result v5

    if-nez v5, :cond_3

    .line 775
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v5, p0, Lcom/android/phone/NotificationMgr;->mPhoneCount:I

    if-ge v0, v5, :cond_2

    .line 776
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    aget-boolean v5, v5, v0

    if-eqz v5, :cond_1

    .line 777
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "no IccCard, cancelNetworkSelection slot = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 778
    invoke-direct {p0, v0}, Lcom/android/phone/NotificationMgr;->cancelNetworkSelection(I)V

    .line 779
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    aput-boolean v8, v5, v0

    .line 775
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 782
    :cond_2
    return-void

    .line 784
    .end local v0    # "i":I
    :cond_3
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    aget-boolean v5, v5, p3

    if-eqz v5, :cond_5

    .line 785
    if-ne p1, v9, :cond_4

    .line 786
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/telephony/ServiceState;->getIsManualSelection()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    .line 784
    if-eqz v5, :cond_5

    .line 787
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "is not Manual Selection, cancelNetworkSelection slot = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 788
    invoke-direct {p0, p3}, Lcom/android/phone/NotificationMgr;->cancelNetworkSelection(I)V

    .line 789
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    aput-boolean v8, v5, p3

    .line 790
    return-void

    .line 793
    :cond_5
    invoke-static {v3}, Lcom/android/internal/telephony/TelephonyCapabilities;->supportsNetworkSelection(Lcom/android/internal/telephony/Phone;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 794
    invoke-static {p2}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 799
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 801
    .local v4, "sp":Landroid/content/SharedPreferences;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "network_selection_name_key"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 802
    .local v2, "networkSelection":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 804
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "network_selection_key"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 807
    :cond_6
    sget-boolean v5, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v5, :cond_7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateNetworkSelection()...state = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 808
    const-string/jumbo v6, " new network "

    .line 807
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    .line 811
    :cond_7
    invoke-direct {p0, p2}, Lcom/android/phone/NotificationMgr;->isUiccCardProvisioned(I)Z

    move-result v1

    .line 812
    .local v1, "isUiccCardProvisioned":Z
    sget-object v5, Lcom/android/phone/NotificationMgr;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateNetworkSelection()...state = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 813
    const-string/jumbo v7, " new network "

    .line 812
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 814
    const-string/jumbo v7, " phoneId "

    .line 812
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 814
    const-string/jumbo v7, " isUiccCardProvisioned "

    .line 812
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    if-ne p1, v9, :cond_9

    .line 818
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    .line 817
    if-eqz v5, :cond_9

    if-eqz v1, :cond_9

    .line 826
    invoke-direct {p0, v2, p2, p3}, Lcom/android/phone/NotificationMgr;->showNetworkSelection(Ljava/lang/String;II)V

    .line 827
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    aput-boolean v9, v5, p3

    .line 852
    .end local v1    # "isUiccCardProvisioned":Z
    .end local v2    # "networkSelection":Ljava/lang/String;
    .end local v4    # "sp":Landroid/content/SharedPreferences;
    :cond_8
    :goto_1
    return-void

    .line 835
    .restart local v1    # "isUiccCardProvisioned":Z
    .restart local v2    # "networkSelection":Ljava/lang/String;
    .restart local v4    # "sp":Landroid/content/SharedPreferences;
    :cond_9
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    aget-boolean v5, v5, p3

    if-eqz v5, :cond_8

    .line 836
    invoke-direct {p0, p3}, Lcom/android/phone/NotificationMgr;->cancelNetworkSelection(I)V

    .line 837
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    aput-boolean v8, v5, p3

    goto :goto_1

    .line 842
    .end local v1    # "isUiccCardProvisioned":Z
    .end local v2    # "networkSelection":Ljava/lang/String;
    .end local v4    # "sp":Landroid/content/SharedPreferences;
    :cond_a
    sget-boolean v5, Lcom/android/phone/NotificationMgr;->DBG:Z

    if-eqz v5, :cond_8

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateNetworkSelection()...state = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 843
    const-string/jumbo v6, " not updating network due to invalid subId "

    .line 842
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/phone/NotificationMgr;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 847
    :cond_b
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    aget-boolean v5, v5, p3

    if-eqz v5, :cond_8

    .line 848
    invoke-direct {p0, p3}, Lcom/android/phone/NotificationMgr;->cancelNetworkSelection(I)V

    .line 849
    iget-object v5, p0, Lcom/android/phone/NotificationMgr;->mSelectedUnavailableNotify:[Z

    aput-boolean v8, v5, p3

    goto :goto_1
.end method
