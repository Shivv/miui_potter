.class public Lcom/android/phone/CallWaitingSwitchPreference;
.super Landroid/preference/SwitchPreference;
.source "CallWaitingSwitchPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;
    }
.end annotation


# instance fields
.field private final DBG:Z

.field private final mHandler:Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;


# direct methods
.method static synthetic -get0(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CallWaitingSwitchPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/phone/TimeConsumingPreferenceListener;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CallWaitingSwitchPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/CallWaitingSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const v0, 0x101036d

    invoke-direct {p0, p1, p2, v0}, Lcom/android/phone/CallWaitingSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->DBG:Z

    .line 20
    new-instance v0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;-><init>(Lcom/android/phone/CallWaitingSwitchPreference;Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;)V

    iput-object v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mHandler:Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;

    .line 26
    return-void
.end method


# virtual methods
.method init(Lcom/android/phone/TimeConsumingPreferenceListener;ZLcom/android/internal/telephony/Phone;)V
    .locals 3
    .param p1, "listener"    # Lcom/android/phone/TimeConsumingPreferenceListener;
    .param p2, "skipReading"    # Z
    .param p3, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v2, 0x0

    .line 38
    iput-object p3, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 39
    iput-object p1, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    .line 41
    if-nez p2, :cond_0

    .line 42
    iget-object v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mHandler:Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;

    invoke-virtual {v1, v2, v2, v2}, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->getCallWaiting(Landroid/os/Message;)V

    .line 44
    iget-object v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/android/phone/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    .line 48
    :cond_0
    return-void
.end method

.method protected onClick()V
    .locals 4

    .prologue
    .line 52
    invoke-super {p0}, Landroid/preference/SwitchPreference;->onClick()V

    .line 54
    iget-object v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {p0}, Lcom/android/phone/CallWaitingSwitchPreference;->isChecked()Z

    move-result v1

    .line 55
    iget-object v2, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mHandler:Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 54
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->setCallWaiting(ZLandroid/os/Message;)V

    .line 56
    iget-object v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/android/phone/CallWaitingSwitchPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/android/phone/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    .line 59
    :cond_0
    return-void
.end method
