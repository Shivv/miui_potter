.class public Lcom/android/phone/CdmaVoicePrivacySwitchPreference;
.super Landroid/preference/SwitchPreference;
.source "CdmaVoicePrivacySwitchPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;
    }
.end annotation


# instance fields
.field private final DBG:Z

.field private mHandler:Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;

.field phone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    const v0, 0x101036d

    invoke-direct {p0, p1, p2, v0}, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    iput-boolean v2, p0, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;->DBG:Z

    .line 34
    new-instance v0, Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;-><init>(Lcom/android/phone/CdmaVoicePrivacySwitchPreference;Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;)V

    iput-object v0, p0, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;->mHandler:Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;

    .line 39
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;->phone:Lcom/android/internal/telephony/Phone;

    .line 40
    iget-object v0, p0, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;->phone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;->mHandler:Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;

    invoke-virtual {v1, v2}, Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->getEnhancedVoicePrivacy(Landroid/os/Message;)V

    .line 41
    return-void
.end method


# virtual methods
.method protected onClick()V
    .locals 4

    .prologue
    .line 54
    invoke-super {p0}, Landroid/preference/SwitchPreference;->onClick()V

    .line 56
    iget-object v0, p0, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;->phone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {p0}, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;->isChecked()Z

    move-result v1

    .line 57
    iget-object v2, p0, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;->mHandler:Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/phone/CdmaVoicePrivacySwitchPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 56
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->enableEnhancedVoicePrivacy(ZLandroid/os/Message;)V

    .line 58
    return-void
.end method
