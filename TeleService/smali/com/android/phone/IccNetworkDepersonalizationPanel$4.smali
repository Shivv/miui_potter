.class Lcom/android/phone/IccNetworkDepersonalizationPanel$4;
.super Ljava/lang/Object;
.source "IccNetworkDepersonalizationPanel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/IccNetworkDepersonalizationPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;


# direct methods
.method constructor <init>(Lcom/android/phone/IccNetworkDepersonalizationPanel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 245
    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    invoke-static {v4}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-get7(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 247
    .local v3, "pin":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 248
    return-void

    .line 251
    :cond_0
    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    invoke-static {v4}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-get4(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->getState()I

    move-result v2

    .line 252
    .local v2, "persoState":I
    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Requesting De-Personalization for subtype "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    invoke-static {v6}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-get5(Lcom/android/phone/IccNetworkDepersonalizationPanel;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 253
    const-string/jumbo v6, " subtype val "

    .line 252
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-wrap2(Lcom/android/phone/IccNetworkDepersonalizationPanel;Ljava/lang/String;)V

    .line 256
    :try_start_0
    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    invoke-static {v4}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-get2(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Lorg/codeaurora/internal/IExtTelephony;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 257
    iget-object v6, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    invoke-static {v6}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-get1(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Lorg/codeaurora/internal/IDepersoResCallback;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    invoke-static {v7}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-get6(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Lcom/android/internal/telephony/Phone;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v7

    .line 256
    invoke-interface {v4, v3, v5, v6, v7}, Lorg/codeaurora/internal/IExtTelephony;->supplyIccDepersonalization(Ljava/lang/String;Ljava/lang/String;Lorg/codeaurora/internal/IDepersoResCallback;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    :goto_0
    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    sget-object v5, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->IN_PROGRESS:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    invoke-virtual {v5}, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-wrap0(Lcom/android/phone/IccNetworkDepersonalizationPanel;Ljava/lang/String;)V

    .line 264
    return-void

    .line 260
    :catch_0
    move-exception v1

    .line 261
    .local v1, "ex":Ljava/lang/NullPointerException;
    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "NullPointerException @supplyIccDepersonalization"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-wrap2(Lcom/android/phone/IccNetworkDepersonalizationPanel;Ljava/lang/String;)V

    goto :goto_0

    .line 258
    .end local v1    # "ex":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 259
    .local v0, "ex":Landroid/os/RemoteException;
    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;->this$0:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "RemoteException @supplyIccDepersonalization"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->-wrap2(Lcom/android/phone/IccNetworkDepersonalizationPanel;Ljava/lang/String;)V

    goto :goto_0
.end method
