.class public Lcom/android/phone/otasp/OtaspActivationService;
.super Landroid/app/Service;
.source "OtaspActivationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/otasp/OtaspActivationService$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sIccId:Ljava/lang/String;

.field private static sOtaspCallRetries:I


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mIsOtaspCallCommitted:Z

.field private mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method static synthetic -wrap0(Ljava/lang/String;)V
    .locals 0
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap1(Ljava/lang/String;)V
    .locals 0
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/phone/otasp/OtaspActivationService;->loge(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/otasp/OtaspActivationService;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/otasp/OtaspActivationService;
    .param p1, "r"    # Landroid/os/AsyncResult;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/otasp/OtaspActivationService;->onCdmaProvisionStatusUpdate(Landroid/os/AsyncResult;)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/phone/otasp/OtaspActivationService;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/otasp/OtaspActivationService;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/otasp/OtaspActivationService;->onOtaspCallStateChanged()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/phone/otasp/OtaspActivationService;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/otasp/OtaspActivationService;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/otasp/OtaspActivationService;->onStartOtaspCall()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/android/phone/otasp/OtaspActivationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/otasp/OtaspActivationService;->TAG:Ljava/lang/String;

    .line 57
    const/4 v0, 0x0

    sput v0, Lcom/android/phone/otasp/OtaspActivationService;->sOtaspCallRetries:I

    .line 67
    const/4 v0, 0x0

    sput-object v0, Lcom/android/phone/otasp/OtaspActivationService;->sIccId:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mIsOtaspCallCommitted:Z

    .line 103
    new-instance v0, Lcom/android/phone/otasp/OtaspActivationService$1;

    invoke-direct {v0, p0}, Lcom/android/phone/otasp/OtaspActivationService$1;-><init>(Lcom/android/phone/otasp/OtaspActivationService;)V

    iput-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    .line 45
    return-void
.end method

.method private static logd(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 219
    sget-object v0, Lcom/android/phone/otasp/OtaspActivationService;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 223
    sget-object v0, Lcom/android/phone/otasp/OtaspActivationService;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    return-void
.end method

.method private onCdmaProvisionStatusUpdate(Landroid/os/AsyncResult;)V
    .locals 4
    .param p1, "r"    # Landroid/os/AsyncResult;

    .prologue
    const/4 v3, 0x0

    .line 169
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v0, [I

    .line 170
    .local v0, "otaStatus":[I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCdmaProvisionStatusUpdate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, v0, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 171
    aget v1, v0, v3

    const/16 v2, 0x8

    if-ne v2, v1, :cond_0

    .line 172
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mIsOtaspCallCommitted:Z

    .line 174
    :cond_0
    return-void
.end method

.method private onComplete()V
    .locals 1

    .prologue
    .line 196
    const-string/jumbo v0, "otasp service onComplete"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 197
    invoke-direct {p0}, Lcom/android/phone/otasp/OtaspActivationService;->unregisterAll()V

    .line 198
    invoke-virtual {p0}, Lcom/android/phone/otasp/OtaspActivationService;->stopSelf()V

    .line 199
    return-void
.end method

.method private onOtaspCallStateChanged()V
    .locals 2

    .prologue
    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onOtaspCallStateChanged: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v0

    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/PhoneConstants$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-boolean v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mIsOtaspCallCommitted:Z

    if-eqz v0, :cond_1

    .line 185
    const-string/jumbo v0, "Otasp activation succeed"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 186
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/phone/otasp/OtaspActivationService;->updateActivationState(Landroid/content/Context;Z)V

    .line 191
    :goto_0
    invoke-direct {p0}, Lcom/android/phone/otasp/OtaspActivationService;->onComplete()V

    .line 193
    :cond_0
    return-void

    .line 188
    :cond_1
    const-string/jumbo v0, "Otasp activation failed"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 189
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/phone/otasp/OtaspActivationService;->updateActivationState(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method private onStartOtaspCall()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 140
    invoke-direct {p0}, Lcom/android/phone/otasp/OtaspActivationService;->unregisterAll()V

    .line 141
    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    const-string/jumbo v1, "OTASP call failure, wait for network available."

    invoke-static {v1}, Lcom/android/phone/otasp/OtaspActivationService;->loge(Ljava/lang/String;)V

    .line 143
    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v5}, Lcom/android/internal/telephony/Phone;->registerForServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 144
    return-void

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3, v5}, Lcom/android/internal/telephony/Phone;->registerForCdmaOtaStatusChange(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 148
    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, v4, v5}, Lcom/android/internal/telephony/Phone;->registerForPreciseCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 149
    const-string/jumbo v1, "startNonInteractiveOtasp: placing call to \'*22899\'..."

    invoke-static {v1}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 151
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 152
    const-string/jumbo v2, "*22899"

    .line 150
    invoke-static {p0, v1, v2, v5, v4}, Lcom/android/phone/PhoneUtils;->placeCall(Landroid/content/Context;Lcom/android/internal/telephony/Phone;Ljava/lang/String;Landroid/net/Uri;Z)I

    move-result v0

    .line 155
    .local v0, "callStatus":I
    if-nez v0, :cond_1

    .line 156
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "  ==> success return from placeCall(): callStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 162
    :goto_0
    return-void

    .line 158
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " ==> failure return from placeCall(): callStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/otasp/OtaspActivationService;->loge(Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    .line 160
    const-wide/16 v2, 0xbb8

    .line 159
    const/4 v4, 0x1

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private unregisterAll()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForCdmaOtaStatusChange(Landroid/os/Handler;)V

    .line 203
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForSubscriptionInfoReady(Landroid/os/Handler;)V

    .line 204
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForServiceStateChanged(Landroid/os/Handler;)V

    .line 205
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForPreciseCallStateChanged(Landroid/os/Handler;)V

    .line 206
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 207
    return-void
.end method

.method public static updateActivationState(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "success"    # Z

    .prologue
    .line 210
    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 211
    .local v0, "mTelephonyMgr":Landroid/telephony/TelephonyManager;
    if-eqz p1, :cond_0

    const/4 v1, 0x2

    .line 213
    .local v1, "state":I
    :goto_0
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultSubscriptionId()I

    move-result v2

    .line 214
    .local v2, "subId":I
    invoke-virtual {v0, v2, v1}, Landroid/telephony/TelephonyManager;->setVoiceActivationState(II)V

    .line 215
    invoke-virtual {v0, v2, v1}, Landroid/telephony/TelephonyManager;->setDataActivationState(II)V

    .line 216
    return-void

    .line 212
    .end local v1    # "state":I
    .end local v2    # "subId":I
    :cond_0
    const/4 v1, 0x3

    .restart local v1    # "state":I
    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    const-string/jumbo v0, "otasp service onCreate"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 76
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 77
    sget-object v0, Lcom/android/phone/otasp/OtaspActivationService;->sIccId:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/phone/otasp/OtaspActivationService;->sIccId:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getIccSerialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getIccSerialNumber()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/otasp/OtaspActivationService;->sIccId:Ljava/lang/String;

    .line 80
    sput v2, Lcom/android/phone/otasp/OtaspActivationService;->sOtaspCallRetries:I

    .line 82
    :cond_1
    sget v0, Lcom/android/phone/otasp/OtaspActivationService;->sOtaspCallRetries:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/phone/otasp/OtaspActivationService;->sOtaspCallRetries:I

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "OTASP call tried "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/android/phone/otasp/OtaspActivationService;->sOtaspCallRetries:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " times"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 84
    sget v0, Lcom/android/phone/otasp/OtaspActivationService;->sOtaspCallRetries:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_2

    .line 85
    const-string/jumbo v0, "OTASP call exceeds max retries => activation failed"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->logd(Ljava/lang/String;)V

    .line 86
    invoke-static {p0, v2}, Lcom/android/phone/otasp/OtaspActivationService;->updateActivationState(Landroid/content/Context;Z)V

    .line 87
    invoke-direct {p0}, Lcom/android/phone/otasp/OtaspActivationService;->onComplete()V

    .line 88
    return-void

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 91
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 95
    const/4 v0, 0x3

    return v0
.end method
