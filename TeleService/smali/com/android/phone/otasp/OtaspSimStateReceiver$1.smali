.class Lcom/android/phone/otasp/OtaspSimStateReceiver$1;
.super Landroid/telephony/PhoneStateListener;
.source "OtaspSimStateReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/otasp/OtaspSimStateReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/otasp/OtaspSimStateReceiver;


# direct methods
.method constructor <init>(Lcom/android/phone/otasp/OtaspSimStateReceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/otasp/OtaspSimStateReceiver;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/otasp/OtaspSimStateReceiver$1;->this$0:Lcom/android/phone/otasp/OtaspSimStateReceiver;

    .line 34
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onOtaspChanged(I)V
    .locals 4
    .param p1, "otaspMode"    # I

    .prologue
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onOtaspChanged: otaspMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspSimStateReceiver;->-wrap0(Ljava/lang/String;)V

    .line 38
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 39
    const-string/jumbo v0, "otasp activation required, start otaspActivationService"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspSimStateReceiver;->-wrap0(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspSimStateReceiver$1;->this$0:Lcom/android/phone/otasp/OtaspSimStateReceiver;

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspSimStateReceiver;->-get0(Lcom/android/phone/otasp/OtaspSimStateReceiver;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/otasp/OtaspSimStateReceiver$1;->this$0:Lcom/android/phone/otasp/OtaspSimStateReceiver;

    invoke-static {v2}, Lcom/android/phone/otasp/OtaspSimStateReceiver;->-get0(Lcom/android/phone/otasp/OtaspSimStateReceiver;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/android/phone/otasp/OtaspActivationService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspSimStateReceiver$1;->this$0:Lcom/android/phone/otasp/OtaspSimStateReceiver;

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspSimStateReceiver;->-get0(Lcom/android/phone/otasp/OtaspSimStateReceiver;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/phone/otasp/OtaspActivationService;->updateActivationState(Landroid/content/Context;Z)V

    goto :goto_0
.end method
