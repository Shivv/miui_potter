.class Lcom/android/phone/otasp/OtaspActivationService$1;
.super Landroid/os/Handler;
.source "OtaspActivationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/otasp/OtaspActivationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/otasp/OtaspActivationService;


# direct methods
.method constructor <init>(Lcom/android/phone/otasp/OtaspActivationService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/otasp/OtaspActivationService;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/otasp/OtaspActivationService$1;->this$0:Lcom/android/phone/otasp/OtaspActivationService;

    .line 103
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 106
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "invalid msg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " not handled."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap1(Ljava/lang/String;)V

    .line 130
    :goto_0
    return-void

    .line 108
    :pswitch_0
    const-string/jumbo v0, "EVENT_SERVICE_STATE_CHANGED"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap0(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService$1;->this$0:Lcom/android/phone/otasp/OtaspActivationService;

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap4(Lcom/android/phone/otasp/OtaspActivationService;)V

    goto :goto_0

    .line 112
    :pswitch_1
    const-string/jumbo v0, "EVENT_START_OTASP_CALL"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap0(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService$1;->this$0:Lcom/android/phone/otasp/OtaspActivationService;

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap4(Lcom/android/phone/otasp/OtaspActivationService;)V

    goto :goto_0

    .line 116
    :pswitch_2
    const-string/jumbo v0, "OTASP_CALL_STATE_CHANGED"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap0(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService$1;->this$0:Lcom/android/phone/otasp/OtaspActivationService;

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap3(Lcom/android/phone/otasp/OtaspActivationService;)V

    goto :goto_0

    .line 120
    :pswitch_3
    const-string/jumbo v0, "OTASP_ACTIVATION_STATUS_UPDATE_EVENT"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap0(Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/android/phone/otasp/OtaspActivationService$1;->this$0:Lcom/android/phone/otasp/OtaspActivationService;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    invoke-static {v1, v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap2(Lcom/android/phone/otasp/OtaspActivationService;Landroid/os/AsyncResult;)V

    goto :goto_0

    .line 124
    :pswitch_4
    const-string/jumbo v0, "EVENT_CDMA_OTASP_CALL_RETRY"

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap0(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspActivationService$1;->this$0:Lcom/android/phone/otasp/OtaspActivationService;

    invoke-static {v0}, Lcom/android/phone/otasp/OtaspActivationService;->-wrap4(Lcom/android/phone/otasp/OtaspActivationService;)V

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
