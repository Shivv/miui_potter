.class public Lcom/android/phone/otasp/OtaspSimStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "OtaspSimStateReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/otasp/OtaspSimStateReceiver$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method static synthetic -get0(Lcom/android/phone/otasp/OtaspSimStateReceiver;)Landroid/content/Context;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/otasp/OtaspSimStateReceiver;

    .prologue
    iget-object v0, p0, Lcom/android/phone/otasp/OtaspSimStateReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -wrap0(Ljava/lang/String;)V
    .locals 0
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/phone/otasp/OtaspSimStateReceiver;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/android/phone/otasp/OtaspSimStateReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/otasp/OtaspSimStateReceiver;->TAG:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 34
    new-instance v0, Lcom/android/phone/otasp/OtaspSimStateReceiver$1;

    invoke-direct {v0, p0}, Lcom/android/phone/otasp/OtaspSimStateReceiver$1;-><init>(Lcom/android/phone/otasp/OtaspSimStateReceiver;)V

    iput-object v0, p0, Lcom/android/phone/otasp/OtaspSimStateReceiver;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 29
    return-void
.end method

.method private static isCarrierSupported()Z
    .locals 5

    .prologue
    .line 52
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    .line 53
    .local v3, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 54
    .local v2, "context":Landroid/content/Context;
    if-eqz v2, :cond_1

    .line 55
    const/4 v0, 0x0

    .line 57
    .local v0, "b":Landroid/os/PersistableBundle;
    const-string/jumbo v4, "carrier_config"

    .line 56
    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/CarrierConfigManager;

    .line 58
    .local v1, "configManager":Landroid/telephony/CarrierConfigManager;
    if-eqz v1, :cond_0

    .line 59
    invoke-virtual {v1}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 61
    .end local v0    # "b":Landroid/os/PersistableBundle;
    :cond_0
    if-eqz v0, :cond_1

    .line 62
    const-string/jumbo v4, "use_otasp_for_provisioning_bool"

    .line 61
    invoke-virtual {v0, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 63
    const/4 v4, 0x1

    return v4

    .line 66
    .end local v1    # "configManager":Landroid/telephony/CarrierConfigManager;
    :cond_1
    const-string/jumbo v4, "otasp activation not needed: no supported carrier"

    invoke-static {v4}, Lcom/android/phone/otasp/OtaspSimStateReceiver;->logd(Ljava/lang/String;)V

    .line 67
    const/4 v4, 0x0

    return v4
.end method

.method private static logd(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 84
    sget-object v0, Lcom/android/phone/otasp/OtaspSimStateReceiver;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/android/phone/otasp/OtaspSimStateReceiver;->mContext:Landroid/content/Context;

    .line 73
    const-string/jumbo v1, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Received intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/otasp/OtaspSimStateReceiver;->logd(Ljava/lang/String;)V

    .line 75
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getIccRecordsLoaded()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/phone/otasp/OtaspSimStateReceiver;->isCarrierSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    invoke-static {p1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 77
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/android/phone/otasp/OtaspSimStateReceiver;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 78
    const/16 v2, 0x200

    .line 77
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 81
    .end local v0    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :cond_0
    return-void
.end method
