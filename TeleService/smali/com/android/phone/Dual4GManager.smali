.class public Lcom/android/phone/Dual4GManager;
.super Ljava/lang/Object;
.source "Dual4GManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/Dual4GManager$CallBack;,
        Lcom/android/phone/Dual4GManager$Dual4GHandler;,
        Lcom/android/phone/Dual4GManager$EnableDual4GParams;,
        Lcom/android/phone/Dual4GManager$Listener;
    }
.end annotation


# static fields
.field private static final DEFAULT_VALUE:I

.field private static LOG_TAG:Ljava/lang/String;

.field private static sInstance:Lcom/android/phone/Dual4GManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDual4GListener:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/phone/Dual4GManager$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/android/phone/Dual4GManager$Dual4GHandler;


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/phone/Dual4GManager;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/Dual4GManager;)Landroid/content/Context;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/Dual4GManager;

    .prologue
    iget-object v0, p0, Lcom/android/phone/Dual4GManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string/jumbo v0, "Dual4GManager"

    sput-object v0, Lcom/android/phone/Dual4GManager;->LOG_TAG:Ljava/lang/String;

    .line 41
    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    sput v0, Lcom/android/phone/Dual4GManager;->DEFAULT_VALUE:I

    .line 27
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/Dual4GManager;->mDual4GListener:Ljava/util/List;

    .line 79
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/Dual4GManager;->mContext:Landroid/content/Context;

    .line 80
    new-instance v0, Lcom/android/phone/Dual4GManager$Dual4GHandler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/phone/Dual4GManager$Dual4GHandler;-><init>(Lcom/android/phone/Dual4GManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/phone/Dual4GManager;->mHandler:Lcom/android/phone/Dual4GManager$Dual4GHandler;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/phone/Dual4GManager;->mDual4GListener:Ljava/util/List;

    .line 82
    return-void
.end method

.method private static getDual4GModeDefaultValue()I
    .locals 1

    .prologue
    .line 149
    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x0

    return v0

    .line 152
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public static getInstance()Lcom/android/phone/Dual4GManager;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/android/phone/Dual4GManager;->sInstance:Lcom/android/phone/Dual4GManager;

    return-object v0
.end method

.method public static init()V
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/android/phone/Dual4GManager;->sInstance:Lcom/android/phone/Dual4GManager;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/android/phone/Dual4GManager;

    invoke-direct {v0}, Lcom/android/phone/Dual4GManager;-><init>()V

    sput-object v0, Lcom/android/phone/Dual4GManager;->sInstance:Lcom/android/phone/Dual4GManager;

    .line 72
    :cond_0
    return-void
.end method

.method public static isDual4GEnabled()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 142
    sget-boolean v2, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-nez v2, :cond_0

    .line 143
    return v1

    .line 145
    :cond_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "dual_4g_mode_enabled"

    invoke-static {}, Lcom/android/phone/Dual4GManager;->getDual4GModeDefaultValue()I

    move-result v4

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public notifyDual4GModeChanged(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 172
    iget-object v3, p0, Lcom/android/phone/Dual4GManager;->mDual4GListener:Ljava/util/List;

    monitor-enter v3

    .line 173
    :try_start_0
    iget-object v2, p0, Lcom/android/phone/Dual4GManager;->mDual4GListener:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "l$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/Dual4GManager$Listener;

    .line 174
    .local v0, "l":Lcom/android/phone/Dual4GManager$Listener;
    invoke-interface {v0, p1}, Lcom/android/phone/Dual4GManager$Listener;->onDual4GModeChanged(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 172
    .end local v0    # "l":Lcom/android/phone/Dual4GManager$Listener;
    .end local v1    # "l$iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .restart local v1    # "l$iterator":Ljava/util/Iterator;
    :cond_0
    monitor-exit v3

    .line 177
    return-void
.end method

.method public setDual4GMode(ZLcom/android/phone/Dual4GManager$CallBack;)V
    .locals 10
    .param p1, "enabled"    # Z
    .param p2, "callback"    # Lcom/android/phone/Dual4GManager$CallBack;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 126
    sget-object v5, Lcom/android/phone/Dual4GManager;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "setDual4GMode enabled="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v5, p0, Lcom/android/phone/Dual4GManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "dual_4g_mode_enabled"

    if-eqz p1, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v8, v9, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 128
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->getInstance()Lcom/android/phone/networkmode/NetworkMode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/phone/networkmode/NetworkMode;->getSupportModes()[[I

    move-result-object v3

    .line 129
    .local v3, "supportModes":[[I
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    .line 130
    .local v0, "defaultDataSlotId":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget v5, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v1, v5, :cond_2

    .line 131
    if-eq v1, v0, :cond_0

    .line 132
    aget-object v5, v3, v1

    aget v4, v5, v7

    .line 133
    .local v4, "viceNetworkMode":I
    sget-object v5, Lcom/android/phone/Dual4GManager;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "viceNetworkMode="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", slotId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-static {v1, v4}, Lmiui/telephony/DefaultSimManager;->setNetworkModeInDb(II)V

    .line 135
    iget-object v5, p0, Lcom/android/phone/Dual4GManager;->mHandler:Lcom/android/phone/Dual4GManager$Dual4GHandler;

    new-instance v8, Lcom/android/phone/Dual4GManager$EnableDual4GParams;

    invoke-direct {v8, p0, p1, p2}, Lcom/android/phone/Dual4GManager$EnableDual4GParams;-><init>(Lcom/android/phone/Dual4GManager;ZLcom/android/phone/Dual4GManager$CallBack;)V

    invoke-virtual {v5, v6, v8}, Lcom/android/phone/Dual4GManager$Dual4GHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 136
    .local v2, "msg":Landroid/os/Message;
    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5, v4, v2}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    .line 130
    .end local v2    # "msg":Landroid/os/Message;
    .end local v4    # "viceNetworkMode":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "defaultDataSlotId":I
    .end local v1    # "i":I
    .end local v3    # "supportModes":[[I
    :cond_1
    move v5, v7

    .line 127
    goto :goto_0

    .line 139
    .restart local v0    # "defaultDataSlotId":I
    .restart local v1    # "i":I
    .restart local v3    # "supportModes":[[I
    :cond_2
    return-void
.end method
