.class Lcom/android/phone/CallNotifier$InCallTonePlayer;
.super Ljava/lang/Thread;
.source "CallNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/CallNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InCallTonePlayer"
.end annotation


# instance fields
.field private mState:I

.field private mToneId:I

.field final synthetic this$0:Lcom/android/phone/CallNotifier;


# direct methods
.method constructor <init>(Lcom/android/phone/CallNotifier;I)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/phone/CallNotifier;
    .param p2, "toneId"    # I

    .prologue
    .line 329
    iput-object p1, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    .line 330
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 331
    iput p2, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mToneId:I

    .line 332
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mState:I

    .line 333
    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 337
    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "InCallTonePlayer.run(toneId = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mToneId:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, ")..."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/phone/CallNotifier;->-wrap1(Lcom/android/phone/CallNotifier;Ljava/lang/String;)V

    .line 339
    const/4 v9, 0x0

    .line 342
    .local v9, "toneType":I
    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    invoke-static {v11}, Lcom/android/phone/CallNotifier;->-get4(Lcom/android/phone/CallNotifier;)Lcom/android/internal/telephony/CallManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/internal/telephony/CallManager;->getFgPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v4

    .line 344
    .local v4, "phoneType":I
    iget v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mToneId:I

    packed-switch v11, :pswitch_data_0

    .line 410
    :pswitch_0
    new-instance v11, Ljava/lang/IllegalArgumentException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Bad toneId: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mToneId:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 346
    :pswitch_1
    const/16 v9, 0x16

    .line 347
    const/16 v10, 0x50

    .line 349
    .local v10, "toneVolume":I
    const v8, 0x7fffffeb

    .line 418
    .local v8, "toneLengthMillis":I
    :goto_0
    :try_start_0
    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    invoke-static {v11}, Lcom/android/phone/CallNotifier;->-get2(Lcom/android/phone/CallNotifier;)Landroid/bluetooth/BluetoothHeadset;

    move-result-object v11

    if-eqz v11, :cond_9

    .line 419
    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    invoke-static {v11}, Lcom/android/phone/CallNotifier;->-get2(Lcom/android/phone/CallNotifier;)Landroid/bluetooth/BluetoothHeadset;

    move-result-object v11

    invoke-virtual {v11}, Landroid/bluetooth/BluetoothHeadset;->isAudioOn()Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v6, 0x6

    .line 424
    .local v6, "stream":I
    :goto_1
    new-instance v7, Landroid/media/ToneGenerator;

    invoke-direct {v7, v6, v10}, Landroid/media/ToneGenerator;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    .end local v6    # "stream":I
    :goto_2
    const/4 v2, 0x1

    .line 447
    .local v2, "needToStopTone":Z
    const/4 v3, 0x0

    .line 449
    .local v3, "okToPlayTone":Z
    if-eqz v7, :cond_3

    .line 450
    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    invoke-static {v11}, Lcom/android/phone/CallNotifier;->-get1(Lcom/android/phone/CallNotifier;)Landroid/media/AudioManager;

    move-result-object v11

    invoke-virtual {v11}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v5

    .line 451
    .local v5, "ringerMode":I
    const/4 v11, 0x2

    if-ne v4, v11, :cond_11

    .line 452
    const/16 v11, 0x5d

    if-ne v9, v11, :cond_a

    .line 453
    if-eqz v5, :cond_1

    .line 454
    const/4 v11, 0x1

    if-eq v5, v11, :cond_1

    .line 455
    invoke-static {}, Lcom/android/phone/CallNotifier;->-get0()Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "- InCallTonePlayer: start playing call tone="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/phone/CallNotifier;->-wrap1(Lcom/android/phone/CallNotifier;Ljava/lang/String;)V

    .line 456
    :cond_0
    const/4 v3, 0x1

    .line 457
    const/4 v2, 0x0

    .line 484
    :cond_1
    :goto_3
    monitor-enter p0

    .line 485
    if-eqz v3, :cond_2

    :try_start_1
    iget v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mState:I

    const/4 v12, 0x2

    if-eq v11, v12, :cond_2

    .line 486
    const/4 v11, 0x1

    iput v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mState:I

    .line 487
    invoke-virtual {v7, v9}, Landroid/media/ToneGenerator;->startTone(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489
    add-int/lit8 v11, v8, 0x14

    int-to-long v12, v11

    :try_start_2
    invoke-virtual {p0, v12, v13}, Lcom/android/phone/CallNotifier$InCallTonePlayer;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 494
    :goto_4
    if-eqz v2, :cond_2

    .line 495
    :try_start_3
    invoke-virtual {v7}, Landroid/media/ToneGenerator;->stopTone()V

    .line 499
    :cond_2
    invoke-virtual {v7}, Landroid/media/ToneGenerator;->release()V

    .line 500
    const/4 v11, 0x0

    iput v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mState:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    .line 517
    .end local v5    # "ringerMode":I
    :cond_3
    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    invoke-static {v11}, Lcom/android/phone/CallNotifier;->-get4(Lcom/android/phone/CallNotifier;)Lcom/android/internal/telephony/CallManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v11

    sget-object v12, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    if-ne v11, v12, :cond_4

    .line 518
    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    invoke-static {v11}, Lcom/android/phone/CallNotifier;->-wrap2(Lcom/android/phone/CallNotifier;)V

    .line 520
    :cond_4
    return-void

    .line 352
    .end local v2    # "needToStopTone":Z
    .end local v3    # "okToPlayTone":Z
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :pswitch_2
    const/4 v11, 0x2

    if-ne v4, v11, :cond_5

    .line 353
    const/16 v9, 0x60

    .line 354
    const/16 v10, 0x32

    .line 355
    .restart local v10    # "toneVolume":I
    const/16 v8, 0x3e8

    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 356
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :cond_5
    const/4 v11, 0x1

    if-eq v4, v11, :cond_6

    .line 357
    const/4 v11, 0x3

    if-ne v4, v11, :cond_7

    .line 360
    :cond_6
    const/16 v9, 0x11

    .line 361
    const/16 v10, 0x50

    .line 362
    .restart local v10    # "toneVolume":I
    const/16 v8, 0xfa0

    .line 359
    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 358
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :cond_7
    const/4 v11, 0x5

    if-eq v4, v11, :cond_6

    .line 359
    const/4 v11, 0x4

    if-eq v4, v11, :cond_6

    .line 364
    new-instance v11, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Unexpected phone type: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 368
    :pswitch_3
    const/16 v9, 0x12

    .line 369
    const/16 v10, 0x50

    .line 370
    .restart local v10    # "toneVolume":I
    const/16 v8, 0xfa0

    .line 371
    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 374
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :pswitch_4
    const/16 v9, 0x1b

    .line 375
    const/16 v10, 0x50

    .line 376
    .restart local v10    # "toneVolume":I
    const/16 v8, 0xc8

    .line 377
    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 379
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :pswitch_5
    const/16 v9, 0x56

    .line 380
    const/16 v10, 0x50

    .line 381
    .restart local v10    # "toneVolume":I
    const/16 v8, 0x1388

    .line 382
    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 384
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :pswitch_6
    const/16 v9, 0x26

    .line 385
    const/16 v10, 0x50

    .line 386
    .restart local v10    # "toneVolume":I
    const/16 v8, 0xfa0

    .line 387
    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 389
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :pswitch_7
    const/16 v9, 0x25

    .line 390
    const/16 v10, 0x32

    .line 391
    .restart local v10    # "toneVolume":I
    const/16 v8, 0x1f4

    .line 392
    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 395
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :pswitch_8
    const/16 v9, 0x5f

    .line 396
    const/16 v10, 0x32

    .line 397
    .restart local v10    # "toneVolume":I
    const/16 v8, 0x177

    .line 398
    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 400
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :pswitch_9
    const/16 v9, 0x57

    .line 401
    const/16 v10, 0x32

    .line 402
    .restart local v10    # "toneVolume":I
    const/16 v8, 0x1388

    .line 403
    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 405
    .end local v8    # "toneLengthMillis":I
    .end local v10    # "toneVolume":I
    :pswitch_a
    const/16 v9, 0x15

    .line 406
    const/16 v10, 0x50

    .line 407
    .restart local v10    # "toneVolume":I
    const/16 v8, 0xfa0

    .line 408
    .restart local v8    # "toneLengthMillis":I
    goto/16 :goto_0

    .line 420
    :cond_8
    const/4 v6, 0x0

    .restart local v6    # "stream":I
    goto/16 :goto_1

    .line 422
    .end local v6    # "stream":I
    :cond_9
    const/4 v6, 0x0

    .restart local v6    # "stream":I
    goto/16 :goto_1

    .line 426
    .end local v6    # "stream":I
    :catch_0
    move-exception v1

    .line 427
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string/jumbo v11, "CallNotifier"

    .line 428
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "InCallTonePlayer: Exception caught while creating ToneGenerator: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 427
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    const/4 v7, 0x0

    .local v7, "toneGenerator":Landroid/media/ToneGenerator;
    goto/16 :goto_2

    .line 459
    .end local v1    # "e":Ljava/lang/RuntimeException;
    .end local v7    # "toneGenerator":Landroid/media/ToneGenerator;
    .restart local v2    # "needToStopTone":Z
    .restart local v3    # "okToPlayTone":Z
    .restart local v5    # "ringerMode":I
    :cond_a
    const/16 v11, 0x60

    if-eq v9, v11, :cond_b

    .line 460
    const/16 v11, 0x26

    if-ne v9, v11, :cond_d

    .line 464
    :cond_b
    if-eqz v5, :cond_1

    .line 465
    invoke-static {}, Lcom/android/phone/CallNotifier;->-get0()Z

    move-result v11

    if-eqz v11, :cond_c

    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "InCallTonePlayer:playing call fail tone:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/phone/CallNotifier;->-wrap1(Lcom/android/phone/CallNotifier;Ljava/lang/String;)V

    .line 466
    :cond_c
    const/4 v3, 0x1

    .line 467
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 461
    :cond_d
    const/16 v11, 0x27

    if-eq v9, v11, :cond_b

    .line 462
    const/16 v11, 0x25

    if-eq v9, v11, :cond_b

    .line 463
    const/16 v11, 0x5f

    if-eq v9, v11, :cond_b

    .line 469
    const/16 v11, 0x57

    if-eq v9, v11, :cond_e

    .line 470
    const/16 v11, 0x56

    if-ne v9, v11, :cond_10

    .line 471
    :cond_e
    if-eqz v5, :cond_1

    .line 472
    const/4 v11, 0x1

    if-eq v5, v11, :cond_1

    .line 473
    invoke-static {}, Lcom/android/phone/CallNotifier;->-get0()Z

    move-result v11

    if-eqz v11, :cond_f

    iget-object v11, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "InCallTonePlayer:playing tone for toneType="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/phone/CallNotifier;->-wrap1(Lcom/android/phone/CallNotifier;Ljava/lang/String;)V

    .line 474
    :cond_f
    const/4 v3, 0x1

    .line 475
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 478
    :cond_10
    const/4 v3, 0x1

    goto/16 :goto_3

    .line 481
    :cond_11
    const/4 v3, 0x1

    goto/16 :goto_3

    .line 490
    :catch_1
    move-exception v0

    .line 491
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_4
    const-string/jumbo v11, "CallNotifier"

    .line 492
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "InCallTonePlayer stopped: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 491
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4

    .line 484
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v11

    monitor-exit p0

    throw v11

    .line 344
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public stopTone()V
    .locals 2

    .prologue
    .line 523
    monitor-enter p0

    .line 524
    :try_start_0
    iget v0, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 525
    invoke-virtual {p0}, Lcom/android/phone/CallNotifier$InCallTonePlayer;->notify()V

    .line 527
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/phone/CallNotifier$InCallTonePlayer;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    .line 529
    return-void

    .line 523
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
