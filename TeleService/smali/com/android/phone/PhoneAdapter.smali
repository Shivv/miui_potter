.class public Lcom/android/phone/PhoneAdapter;
.super Ljava/lang/Object;
.source "PhoneAdapter.java"


# static fields
.field private static mNumRec:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bytesToHexString([B)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # [B

    .prologue
    const/4 v3, 0x0

    .line 201
    if-nez p0, :cond_0

    .line 202
    return-object v3

    .line 204
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v3, p0

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 206
    .local v2, "ret":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_1

    .line 208
    aget-byte v3, p0, v1

    and-int/lit8 v0, v3, 0xf

    .line 209
    .local v0, "b":I
    const-string/jumbo v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 206
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 212
    .end local v0    # "b":I
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static convertPlmnList(ILjava/lang/Object;)Ljava/util/ArrayList;
    .locals 12
    .param p0, "slotId"    # I
    .param p1, "result"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;",
            ">;"
        }
    .end annotation

    .prologue
    move-object v1, p1

    .line 79
    check-cast v1, [B

    .line 80
    .local v1, "data":[B
    array-length v8, v1

    div-int/lit8 v8, v8, 0x5

    sput v8, Lcom/android/phone/PhoneAdapter;->mNumRec:I

    .line 81
    new-instance v6, Ljava/util/ArrayList;

    sget v8, Lcom/android/phone/PhoneAdapter;->mNumRec:I

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 82
    .local v6, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;>;"
    const/4 v8, 0x3

    new-array v3, v8, [B

    .line 83
    .local v3, "mcc":[B
    const/4 v8, 0x3

    new-array v4, v8, [B

    .line 84
    .local v4, "mnc":[B
    const/4 v5, 0x0

    .line 85
    .local v5, "num_mnc_digits":I
    const/4 v0, 0x0

    .line 86
    .local v0, "access_tech":I
    const/4 v7, 0x0

    .line 87
    .local v7, "strOperName":Ljava/lang/String;
    const/4 v2, 0x0

    .end local v7    # "strOperName":Ljava/lang/String;
    .local v2, "i":I
    :goto_0
    sget v8, Lcom/android/phone/PhoneAdapter;->mNumRec:I

    if-ge v2, v8, :cond_8

    .line 88
    const/4 v0, 0x0

    .line 89
    mul-int/lit8 v8, v2, 0x5

    aget-byte v8, v1, v8

    and-int/lit8 v8, v8, 0xf

    int-to-byte v8, v8

    const/4 v9, 0x0

    aput-byte v8, v3, v9

    .line 90
    mul-int/lit8 v8, v2, 0x5

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xf0

    shr-int/lit8 v8, v8, 0x4

    int-to-byte v8, v8

    const/4 v9, 0x1

    aput-byte v8, v3, v9

    .line 91
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x1

    aget-byte v8, v1, v8

    and-int/lit8 v8, v8, 0xf

    int-to-byte v8, v8

    const/4 v9, 0x2

    aput-byte v8, v3, v9

    .line 93
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x2

    aget-byte v8, v1, v8

    and-int/lit8 v8, v8, 0xf

    int-to-byte v8, v8

    const/4 v9, 0x0

    aput-byte v8, v4, v9

    .line 94
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x2

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xf0

    shr-int/lit8 v8, v8, 0x4

    int-to-byte v8, v8

    const/4 v9, 0x1

    aput-byte v8, v4, v9

    .line 95
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x1

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xf0

    int-to-byte v8, v8

    const/16 v9, -0x10

    if-ne v8, v9, :cond_6

    .line 96
    const/4 v5, 0x2

    .line 97
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x1

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xf0

    shr-int/lit8 v8, v8, 0x4

    int-to-byte v8, v8

    const/4 v9, 0x2

    aput-byte v8, v4, v9

    .line 103
    :goto_1
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x3

    aget-byte v8, v1, v8

    and-int/lit8 v8, v8, 0x40

    if-eqz v8, :cond_0

    .line 104
    const/16 v0, 0x8

    .line 106
    :cond_0
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x3

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0x80

    if-eqz v8, :cond_1

    .line 107
    or-int/lit8 v0, v0, 0x4

    .line 109
    :cond_1
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x4

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0x80

    if-eqz v8, :cond_2

    .line 110
    or-int/lit8 v0, v0, 0x1

    .line 113
    :cond_2
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x4

    aget-byte v8, v1, v8

    and-int/lit8 v8, v8, 0x40

    if-eqz v8, :cond_3

    .line 114
    or-int/lit8 v0, v0, 0x2

    .line 117
    :cond_3
    mul-int/lit8 v8, v2, 0x5

    aget-byte v8, v1, v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_5

    .line 118
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x1

    aget-byte v8, v1, v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_5

    .line 119
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x2

    aget-byte v8, v1, v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_5

    .line 120
    const/4 v8, 0x2

    if-ne v5, v8, :cond_7

    .line 121
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Lcom/android/phone/PhoneAdapter;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 122
    invoke-static {v4}, Lcom/android/phone/PhoneAdapter;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 121
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 127
    :cond_4
    :goto_2
    new-instance v8, Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;

    invoke-direct {v8, p0, v7, v0, v2}, Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 99
    :cond_6
    const/4 v5, 0x3

    .line 100
    mul-int/lit8 v8, v2, 0x5

    add-int/lit8 v8, v8, 0x1

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xf0

    shr-int/lit8 v8, v8, 0x4

    int-to-byte v8, v8

    const/4 v9, 0x2

    aput-byte v8, v4, v9

    goto/16 :goto_1

    .line 123
    :cond_7
    const/4 v8, 0x3

    if-ne v5, v8, :cond_4

    .line 124
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Lcom/android/phone/PhoneAdapter;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 125
    invoke-static {v4}, Lcom/android/phone/PhoneAdapter;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v9

    .line 124
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .local v7, "strOperName":Ljava/lang/String;
    goto :goto_2

    .line 131
    .end local v7    # "strOperName":Ljava/lang/String;
    :cond_8
    return-object v6
.end method

.method public static getCallBarringOption(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V
    .locals 0
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "facility"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "onComplete"    # Landroid/os/Message;

    .prologue
    .line 27
    invoke-static {p1, p2, p3, p0}, Lcom/android/phone/CallBarringHelper;->getCallBarringOption(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;Lcom/android/internal/telephony/Phone;)V

    .line 28
    return-void
.end method

.method public static getCallForwardingOption(Lcom/android/internal/telephony/Phone;IILandroid/os/Message;)V
    .locals 0
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "commandInterfaceCFReason"    # I
    .param p2, "commandInterfaceServiceClass"    # I
    .param p3, "onComplete"    # Landroid/os/Message;

    .prologue
    .line 269
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/Phone;->getCallForwardingOption(IILandroid/os/Message;)V

    .line 270
    return-void
.end method

.method public static getPlmnList(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/uicc/IccFileHandler;Landroid/os/Message;)V
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "iccFileHandler"    # Lcom/android/internal/telephony/uicc/IccFileHandler;
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 44
    if-eqz p1, :cond_0

    .line 45
    const/16 v0, 0x6f60

    invoke-static {p1, v0, p2}, Lcom/android/phone/PhoneAdapter;->readEfFromIcc(Lcom/android/internal/telephony/uicc/IccFileHandler;ILandroid/os/Message;)V

    .line 47
    :cond_0
    return-void
.end method

.method public static getRadioTech(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "ni"    # Lcom/android/internal/telephony/OperatorInfo;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/android/internal/telephony/OperatorInfo;->getRadioTech()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVideoCFServiceClass()I
    .locals 1

    .prologue
    .line 262
    const/16 v0, 0x50

    return v0
.end method

.method public static getVoiceRoaming(Landroid/telephony/ServiceState;)Z
    .locals 1
    .param p0, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 254
    invoke-virtual {p0}, Landroid/telephony/ServiceState;->getVoiceRoaming()Z

    move-result v0

    return v0
.end method

.method public static hexCharToInt(C)I
    .locals 3
    .param p0, "c"    # C

    .prologue
    .line 234
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 235
    add-int/lit8 v0, p0, -0x30

    return v0

    .line 236
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 237
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    return v0

    .line 238
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 239
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    return v0

    .line 241
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "invalid hex char \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static hexStringToBytes(Ljava/lang/String;)[B
    .locals 6
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 218
    if-nez p0, :cond_0

    .line 219
    return-object v3

    .line 221
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 223
    .local v2, "sz":I
    new-array v1, v2, [B

    .line 225
    .local v1, "ret":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 226
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/android/phone/PhoneAdapter;->hexCharToInt(C)I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v1, v0

    .line 227
    const-string/jumbo v3, "PhoneAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "hexStringToBytes = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-byte v5, v1, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 230
    :cond_1
    return-object v1
.end method

.method public static isLockNetwork()Z
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    return v0
.end method

.method public static isManualNetSelAllowed(Lcom/android/internal/telephony/Phone;)Z
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public static isUtEnabled(Lcom/android/internal/telephony/Phone;)Z
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v0

    return v0
.end method

.method private static readEfFromIcc(Lcom/android/internal/telephony/uicc/IccFileHandler;ILandroid/os/Message;)V
    .locals 0
    .param p0, "mfh"    # Lcom/android/internal/telephony/uicc/IccFileHandler;
    .param p1, "efid"    # I
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 50
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/telephony/uicc/IccFileHandler;->loadEFTransparent(ILandroid/os/Message;)V

    .line 51
    return-void
.end method

.method public static registerForVoiceRoaming(Landroid/os/Handler;II)V
    .locals 5
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "whatOn"    # I
    .param p2, "whatOff"    # I

    .prologue
    const/4 v4, 0x0

    .line 245
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 246
    .local v1, "phones":[Lcom/android/internal/telephony/Phone;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 247
    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/android/phone/PhoneProxy;->getServiceStateTracker(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v2

    .line 248
    .local v2, "serviceStateTracker":Lcom/android/internal/telephony/ServiceStateTracker;
    invoke-virtual {v2, p0, p1, v4}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForVoiceRoamingOn(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 249
    invoke-virtual {v2, p0, p2, v4}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForVoiceRoamingOff(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 251
    .end local v2    # "serviceStateTracker":Lcom/android/internal/telephony/ServiceStateTracker;
    :cond_0
    return-void
.end method

.method public static setCallBarringOption(Lcom/android/internal/telephony/Phone;Ljava/lang/String;ZLjava/lang/String;Landroid/os/Message;)V
    .locals 0
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "facility"    # Ljava/lang/String;
    .param p2, "lockState"    # Z
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "onComplete"    # Landroid/os/Message;

    .prologue
    .line 32
    invoke-static {p1, p2, p3, p4, p0}, Lcom/android/phone/CallBarringHelper;->setCallBarringOption(Ljava/lang/String;ZLjava/lang/String;Landroid/os/Message;Lcom/android/internal/telephony/Phone;)V

    .line 33
    return-void
.end method

.method public static setCallForwardingOption(Lcom/android/internal/telephony/Phone;IILjava/lang/String;IILandroid/os/Message;)V
    .locals 3
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "commandInterfaceCFAction"    # I
    .param p2, "commandInterfaceCFReason"    # I
    .param p3, "dialingNumber"    # Ljava/lang/String;
    .param p4, "commandInterfaceServiceClass"    # I
    .param p5, "timerSeconds"    # I
    .param p6, "onComplete"    # Landroid/os/Message;

    .prologue
    .line 278
    const-string/jumbo v0, "PhoneAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setCallForwardingOption serviceClass= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    invoke-virtual/range {p0 .. p6}, Lcom/android/internal/telephony/Phone;->setCallForwardingOption(IILjava/lang/String;IILandroid/os/Message;)V

    .line 281
    return-void
.end method

.method public static setPlmnList(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/uicc/IccFileHandler;Ljava/util/ArrayList;Landroid/os/Message;)V
    .locals 12
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "iccFileHandler"    # Lcom/android/internal/telephony/uicc/IccFileHandler;
    .param p3, "response"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/telephony/Phone;",
            "Lcom/android/internal/telephony/uicc/IccFileHandler;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;",
            ">;",
            "Landroid/os/Message;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;>;"
    const/4 v11, 0x2

    const/16 v10, -0x80

    const/4 v9, 0x1

    const/4 v6, -0x1

    const/4 v8, 0x0

    .line 136
    sget v5, Lcom/android/phone/PhoneAdapter;->mNumRec:I

    mul-int/lit8 v5, v5, 0x5

    new-array v0, v5, [B

    .line 137
    .local v0, "data":[B
    const/4 v5, 0x6

    new-array v2, v5, [B

    .line 138
    .local v2, "mccmnc":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v5, Lcom/android/phone/PhoneAdapter;->mNumRec:I

    if-ge v1, v5, :cond_0

    .line 139
    mul-int/lit8 v5, v1, 0x5

    aput-byte v6, v0, v5

    .line 140
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x1

    aput-byte v6, v0, v5

    .line 141
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x2

    aput-byte v6, v0, v5

    .line 143
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x3

    aput-byte v8, v0, v5

    .line 144
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x4

    aput-byte v8, v0, v5

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 146
    :cond_0
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    sget v5, Lcom/android/phone/PhoneAdapter;->mNumRec:I

    if-ge v1, v5, :cond_1

    .line 147
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;

    .line 148
    .local v3, "ni":Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;
    invoke-virtual {v3}, Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v4

    .line 149
    .local v4, "strOperNumeric":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 187
    .end local v3    # "ni":Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;
    .end local v4    # "strOperNumeric":Ljava/lang/String;
    :cond_1
    const-string/jumbo v5, "PhoneAdapter"

    const-string/jumbo v6, "update EFuplmn Start."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    if-eqz p1, :cond_8

    .line 190
    const/16 v5, 0x6f60

    invoke-static {p1, v0, v5, p3}, Lcom/android/phone/PhoneAdapter;->writeEfToIcc(Lcom/android/internal/telephony/uicc/IccFileHandler;[BILandroid/os/Message;)V

    .line 194
    :goto_2
    return-void

    .line 152
    .restart local v3    # "ni":Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;
    .restart local v4    # "strOperNumeric":Ljava/lang/String;
    :cond_2
    const-string/jumbo v5, "PhoneAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "strOperNumeric = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_3

    .line 154
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "F"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 156
    :cond_3
    invoke-static {v4}, Lcom/android/phone/PhoneAdapter;->hexStringToBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 158
    mul-int/lit8 v5, v1, 0x5

    aget-byte v6, v2, v9

    shl-int/lit8 v6, v6, 0x4

    aget-byte v7, v2, v8

    add-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v0, v5

    .line 159
    const-string/jumbo v5, "PhoneAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "mccmnc[0] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, v2, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const-string/jumbo v5, "PhoneAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "mccmnc[1] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, v2, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    const-string/jumbo v5, "PhoneAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "data[i*UPLMN_W_ACT_LEN] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 162
    mul-int/lit8 v7, v1, 0x5

    aget-byte v7, v0, v7

    .line 161
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x1

    const/4 v6, 0x5

    aget-byte v6, v2, v6

    shl-int/lit8 v6, v6, 0x4

    aget-byte v7, v2, v11

    add-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v0, v5

    .line 164
    const-string/jumbo v5, "PhoneAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "data[1] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, v0, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x2

    const/4 v6, 0x4

    aget-byte v6, v2, v6

    shl-int/lit8 v6, v6, 0x4

    const/4 v7, 0x3

    aget-byte v7, v2, v7

    add-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v0, v5

    .line 166
    const-string/jumbo v5, "PhoneAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "data[2] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, v0, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-virtual {v3}, Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;->getNetworMode()I

    move-result v5

    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_6

    .line 168
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x3

    aput-byte v10, v0, v5

    .line 172
    :goto_3
    invoke-virtual {v3}, Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;->getNetworMode()I

    move-result v5

    and-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_4

    .line 173
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x3

    mul-int/lit8 v6, v1, 0x5

    add-int/lit8 v6, v6, 0x3

    aget-byte v6, v0, v6

    or-int/lit8 v6, v6, 0x40

    int-to-byte v6, v6

    aput-byte v6, v0, v5

    .line 176
    :cond_4
    invoke-virtual {v3}, Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;->getNetworMode()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_7

    .line 177
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x4

    aput-byte v10, v0, v5

    .line 182
    :goto_4
    invoke-virtual {v3}, Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;->getNetworMode()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_5

    .line 183
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x4

    mul-int/lit8 v6, v1, 0x5

    add-int/lit8 v6, v6, 0x4

    aget-byte v6, v0, v6

    or-int/lit8 v6, v6, 0x40

    int-to-byte v6, v6

    aput-byte v6, v0, v5

    .line 146
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 170
    :cond_6
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x3

    aput-byte v8, v0, v5

    goto :goto_3

    .line 179
    :cond_7
    mul-int/lit8 v5, v1, 0x5

    add-int/lit8 v5, v5, 0x4

    aput-byte v8, v0, v5

    goto :goto_4

    .line 192
    .end local v3    # "ni":Lcom/android/phone/settings/UserPLMNListPreference$UPLMNInfoWithEf;
    .end local v4    # "strOperNumeric":Ljava/lang/String;
    :cond_8
    const-string/jumbo v5, "PhoneAdapter"

    const-string/jumbo v6, "mIccFileHandler is null."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private static writeEfToIcc(Lcom/android/internal/telephony/uicc/IccFileHandler;[BILandroid/os/Message;)V
    .locals 0
    .param p0, "mfh"    # Lcom/android/internal/telephony/uicc/IccFileHandler;
    .param p1, "efdata"    # [B
    .param p2, "efid"    # I
    .param p3, "response"    # Landroid/os/Message;

    .prologue
    .line 54
    invoke-virtual {p0, p2, p1, p3}, Lcom/android/internal/telephony/uicc/IccFileHandler;->updateEFTransparent(I[BLandroid/os/Message;)V

    .line 55
    return-void
.end method
