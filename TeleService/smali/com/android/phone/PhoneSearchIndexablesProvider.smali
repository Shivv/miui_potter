.class public Lcom/android/phone/PhoneSearchIndexablesProvider;
.super Landroid/provider/SearchIndexablesProvider;
.source "PhoneSearchIndexablesProvider.java"


# static fields
.field private static INDEXABLE_RES:[Landroid/provider/SearchIndexableResource;


# instance fields
.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 58
    new-array v0, v5, [Landroid/provider/SearchIndexableResource;

    .line 59
    new-instance v1, Landroid/provider/SearchIndexableResource;

    .line 60
    const-class v2, Lcom/android/phone/MobileNetworkSettings;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 59
    const v3, 0x7f060028

    .line 61
    const v4, 0x7f030001

    .line 59
    invoke-direct {v1, v5, v3, v2, v4}, Landroid/provider/SearchIndexableResource;-><init>(IILjava/lang/String;I)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 58
    sput-object v0, Lcom/android/phone/PhoneSearchIndexablesProvider;->INDEXABLE_RES:[Landroid/provider/SearchIndexableResource;

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/provider/SearchIndexablesProvider;-><init>()V

    return-void
.end method

.method private createNonIndexableRow(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 164
    sget-object v1, Landroid/provider/SearchIndexablesContract;->NON_INDEXABLES_KEYS_COLUMNS:[Ljava/lang/String;

    array-length v1, v1

    new-array v0, v1, [Ljava/lang/Object;

    .line 165
    .local v0, "ref":[Ljava/lang/Object;
    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 166
    return-object v0
.end method


# virtual methods
.method isEnhanced4gLteHidden()Z
    .locals 3
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/android/phone/PhoneSearchIndexablesProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 160
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {p0}, Lcom/android/phone/PhoneSearchIndexablesProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v2

    .line 159
    invoke-static {v1, v2}, Lcom/android/phone/MobileNetworkSettings;->hideEnhanced4gLteSettings(Landroid/content/Context;Landroid/os/PersistableBundle;)Z

    move-result v1

    return v1
.end method

.method isEuiccSettingsHidden()Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/android/phone/PhoneSearchIndexablesProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/MobileNetworkSettings;->showEuiccSettings(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/android/phone/PhoneSearchIndexablesProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/phone/PhoneSearchIndexablesProvider;->mUserManager:Landroid/os/UserManager;

    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public queryNonIndexableKeys([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 130
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v4, Landroid/provider/SearchIndexablesContract;->NON_INDEXABLES_KEYS_COLUMNS:[Ljava/lang/String;

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 132
    .local v0, "cursor":Landroid/database/MatrixCursor;
    iget-object v4, p0, Lcom/android/phone/PhoneSearchIndexablesProvider;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v4}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v4

    if-nez v4, :cond_0

    .line 133
    const/16 v4, 0xa

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v4, "preferred_network_mode_key"

    aput-object v4, v2, v3

    const-string/jumbo v4, "button_roaming_key"

    const/4 v5, 0x1

    aput-object v4, v2, v5

    .line 134
    const-string/jumbo v4, "cdma_lte_data_service_key"

    const/4 v5, 0x2

    aput-object v4, v2, v5

    const-string/jumbo v4, "enabled_networks_key"

    const/4 v5, 0x3

    aput-object v4, v2, v5

    const-string/jumbo v4, "enhanced_4g_lte"

    const/4 v5, 0x4

    aput-object v4, v2, v5

    .line 135
    const-string/jumbo v4, "button_apn_key"

    const/4 v5, 0x5

    aput-object v4, v2, v5

    const-string/jumbo v4, "button_carrier_sel_key"

    const/4 v5, 0x6

    aput-object v4, v2, v5

    const-string/jumbo v4, "carrier_settings_key"

    const/4 v5, 0x7

    aput-object v4, v2, v5

    .line 136
    const-string/jumbo v4, "cdma_system_select_key"

    const/16 v5, 0x8

    aput-object v4, v2, v5

    const-string/jumbo v4, "esim_list_profile"

    const/16 v5, 0x9

    aput-object v4, v2, v5

    .line 137
    .local v2, "values":[Ljava/lang/String;
    array-length v4, v2

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v2, v3

    .line 138
    .local v1, "nik":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/phone/PhoneSearchIndexablesProvider;->createNonIndexableRow(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 137
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 141
    .end local v1    # "nik":Ljava/lang/String;
    .end local v2    # "values":[Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/PhoneSearchIndexablesProvider;->isEuiccSettingsHidden()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 142
    const-string/jumbo v3, "esim_list_profile"

    invoke-direct {p0, v3}, Lcom/android/phone/PhoneSearchIndexablesProvider;->createNonIndexableRow(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 144
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/PhoneSearchIndexablesProvider;->isEnhanced4gLteHidden()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 145
    const-string/jumbo v3, "enhanced_4g_lte"

    invoke-direct {p0, v3}, Lcom/android/phone/PhoneSearchIndexablesProvider;->createNonIndexableRow(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 148
    :cond_2
    const-string/jumbo v3, "carrier_settings_euicc_key"

    invoke-direct {p0, v3}, Lcom/android/phone/PhoneSearchIndexablesProvider;->createNonIndexableRow(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 149
    return-object v0
.end method

.method public queryRawData([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 108
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v3, Landroid/provider/SearchIndexablesContract;->INDEXABLES_RAW_COLUMNS:[Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 109
    .local v1, "cursor":Landroid/database/MatrixCursor;
    invoke-virtual {p0}, Lcom/android/phone/PhoneSearchIndexablesProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 110
    .local v0, "context":Landroid/content/Context;
    const v3, 0x7f0b03b2

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 111
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 112
    const-string/jumbo v4, "rank"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 111
    invoke-virtual {v3, v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 113
    const-string/jumbo v4, "title"

    .line 111
    invoke-virtual {v3, v4, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 115
    const-string/jumbo v4, "keywords"

    .line 116
    const v5, 0x7f0b03b3

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 111
    invoke-virtual {v3, v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 117
    const-string/jumbo v4, "screenTitle"

    .line 111
    invoke-virtual {v3, v4, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 118
    const-string/jumbo v4, "key"

    const-string/jumbo v5, "esim_list_profile"

    .line 111
    invoke-virtual {v3, v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 120
    const-string/jumbo v4, "intentAction"

    .line 121
    const-string/jumbo v5, "android.telephony.euicc.action.MANAGE_EMBEDDED_SUBSCRIPTIONS"

    .line 111
    invoke-virtual {v3, v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    .line 123
    const-string/jumbo v4, "intentTargetPackage"

    .line 124
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 111
    invoke-virtual {v3, v4, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 125
    return-object v1
.end method

.method public queryXmlResources([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x6

    const/4 v9, 0x5

    .line 72
    const/4 v4, 0x0

    .line 74
    .local v4, "isApkAvailable":Z
    const-string/jumbo v7, "extphone"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lorg/codeaurora/internal/IExtTelephony$Stub;->asInterface(Landroid/os/IBinder;)Lorg/codeaurora/internal/IExtTelephony;

    move-result-object v3

    .line 76
    .local v3, "extTelephony":Lorg/codeaurora/internal/IExtTelephony;
    if-eqz v3, :cond_0

    .line 77
    :try_start_0
    const-string/jumbo v7, "com.qualcomm.qti.networksetting"

    invoke-interface {v3, v7}, Lorg/codeaurora/internal/IExtTelephony;->isVendorApkAvailable(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 76
    if-eqz v7, :cond_0

    .line 78
    const/4 v4, 0x1

    .line 84
    :cond_0
    :goto_0
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v7, Landroid/provider/SearchIndexablesContract;->INDEXABLES_XML_RES_COLUMNS:[Ljava/lang/String;

    invoke-direct {v1, v7}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 85
    .local v1, "cursor":Landroid/database/MatrixCursor;
    sget-object v7, Lcom/android/phone/PhoneSearchIndexablesProvider;->INDEXABLE_RES:[Landroid/provider/SearchIndexableResource;

    array-length v0, v7

    .line 86
    .local v0, "count":I
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_1
    if-ge v5, v0, :cond_2

    .line 87
    const/4 v7, 0x7

    new-array v6, v7, [Ljava/lang/Object;

    .line 88
    .local v6, "ref":[Ljava/lang/Object;
    sget-object v7, Lcom/android/phone/PhoneSearchIndexablesProvider;->INDEXABLE_RES:[Landroid/provider/SearchIndexableResource;

    aget-object v7, v7, v5

    iget v7, v7, Landroid/provider/SearchIndexableResource;->rank:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v6, v8

    .line 89
    sget-object v7, Lcom/android/phone/PhoneSearchIndexablesProvider;->INDEXABLE_RES:[Landroid/provider/SearchIndexableResource;

    aget-object v7, v7, v5

    iget v7, v7, Landroid/provider/SearchIndexableResource;->xmlResId:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x1

    aput-object v7, v6, v8

    .line 90
    const/4 v7, 0x2

    aput-object v11, v6, v7

    .line 91
    sget-object v7, Lcom/android/phone/PhoneSearchIndexablesProvider;->INDEXABLE_RES:[Landroid/provider/SearchIndexableResource;

    aget-object v7, v7, v5

    iget v7, v7, Landroid/provider/SearchIndexableResource;->iconResId:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x3

    aput-object v7, v6, v8

    .line 92
    const-string/jumbo v7, "android.intent.action.MAIN"

    const/4 v8, 0x4

    aput-object v7, v6, v8

    .line 93
    if-eqz v4, :cond_1

    .line 94
    const-string/jumbo v7, "com.qualcomm.qti.networksetting"

    aput-object v7, v6, v9

    .line 96
    const-string/jumbo v7, "com.qualcomm.qti.networksetting.MobileNetworkSettings"

    .line 95
    aput-object v7, v6, v10

    .line 101
    :goto_2
    invoke-virtual {v1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 86
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 80
    .end local v0    # "count":I
    .end local v1    # "cursor":Landroid/database/MatrixCursor;
    .end local v5    # "n":I
    .end local v6    # "ref":[Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 81
    .local v2, "e":Landroid/os/RemoteException;
    const-string/jumbo v7, "PhoneSearchIndexablesProvider"

    const-string/jumbo v8, "couldn\'t connect to extphone service, launch the default activity"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "count":I
    .restart local v1    # "cursor":Landroid/database/MatrixCursor;
    .restart local v5    # "n":I
    .restart local v6    # "ref":[Ljava/lang/Object;
    :cond_1
    const-string/jumbo v7, "com.android.phone"

    aput-object v7, v6, v9

    .line 99
    sget-object v7, Lcom/android/phone/PhoneSearchIndexablesProvider;->INDEXABLE_RES:[Landroid/provider/SearchIndexableResource;

    aget-object v7, v7, v5

    iget-object v7, v7, Landroid/provider/SearchIndexableResource;->className:Ljava/lang/String;

    aput-object v7, v6, v10

    goto :goto_2

    .line 103
    .end local v6    # "ref":[Ljava/lang/Object;
    :cond_2
    return-object v1
.end method
