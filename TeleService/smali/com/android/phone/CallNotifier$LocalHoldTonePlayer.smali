.class public Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;
.super Ljava/lang/Object;
.source "CallNotifier.java"

# interfaces
.implements Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/CallNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LocalHoldTonePlayer"
.end annotation


# instance fields
.field private mLocalHoldTonePlayer:Lcom/android/phone/CallNotifier$InCallTonePlayer;

.field final synthetic this$0:Lcom/android/phone/CallNotifier;


# direct methods
.method public constructor <init>(Lcom/android/phone/CallNotifier;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/phone/CallNotifier;

    .prologue
    .line 863
    iput-object p1, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 862
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->mLocalHoldTonePlayer:Lcom/android/phone/CallNotifier$InCallTonePlayer;

    .line 863
    return-void
.end method


# virtual methods
.method public startPlayLocalHoldTone()V
    .locals 3

    .prologue
    .line 865
    iget-object v0, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->mLocalHoldTonePlayer:Lcom/android/phone/CallNotifier$InCallTonePlayer;

    if-nez v0, :cond_0

    .line 866
    iget-object v0, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    const-string/jumbo v1, "startPlayLocalHoldTone"

    invoke-static {v0, v1}, Lcom/android/phone/CallNotifier;->-wrap1(Lcom/android/phone/CallNotifier;Ljava/lang/String;)V

    .line 867
    new-instance v0, Lcom/android/phone/CallNotifier$InCallTonePlayer;

    iget-object v1, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/android/phone/CallNotifier$InCallTonePlayer;-><init>(Lcom/android/phone/CallNotifier;I)V

    iput-object v0, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->mLocalHoldTonePlayer:Lcom/android/phone/CallNotifier$InCallTonePlayer;

    .line 868
    iget-object v0, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->mLocalHoldTonePlayer:Lcom/android/phone/CallNotifier$InCallTonePlayer;

    invoke-virtual {v0}, Lcom/android/phone/CallNotifier$InCallTonePlayer;->start()V

    .line 870
    :cond_0
    return-void
.end method

.method public stopPlayLocalHoldTone()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 872
    iget-object v0, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->mLocalHoldTonePlayer:Lcom/android/phone/CallNotifier$InCallTonePlayer;

    if-eqz v0, :cond_0

    .line 873
    iget-object v0, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->this$0:Lcom/android/phone/CallNotifier;

    const-string/jumbo v1, "stopPlayLocalHoldTone"

    invoke-static {v0, v1}, Lcom/android/phone/CallNotifier;->-wrap1(Lcom/android/phone/CallNotifier;Ljava/lang/String;)V

    .line 874
    iget-object v0, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->mLocalHoldTonePlayer:Lcom/android/phone/CallNotifier$InCallTonePlayer;

    invoke-virtual {v0}, Lcom/android/phone/CallNotifier$InCallTonePlayer;->stopTone()V

    .line 875
    iput-object v2, p0, Lcom/android/phone/CallNotifier$LocalHoldTonePlayer;->mLocalHoldTonePlayer:Lcom/android/phone/CallNotifier$InCallTonePlayer;

    .line 877
    :cond_0
    return-void
.end method
