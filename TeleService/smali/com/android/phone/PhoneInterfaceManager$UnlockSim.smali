.class Lcom/android/phone/PhoneInterfaceManager$UnlockSim;
.super Ljava/lang/Thread;
.source "PhoneInterfaceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/PhoneInterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UnlockSim"
.end annotation


# instance fields
.field private mDone:Z

.field private mHandler:Landroid/os/Handler;

.field private mResult:I

.field private mRetryCount:I

.field private final mSimCard:Lcom/android/internal/telephony/IccCard;


# direct methods
.method static synthetic -set0(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneInterfaceManager$UnlockSim;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mDone:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneInterfaceManager$UnlockSim;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mResult:I

    return p1
.end method

.method static synthetic -set2(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneInterfaceManager$UnlockSim;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mRetryCount:I

    return p1
.end method

.method public constructor <init>(Lcom/android/internal/telephony/IccCard;)V
    .locals 1
    .param p1, "simCard"    # Lcom/android/internal/telephony/IccCard;

    .prologue
    .line 1428
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1418
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mDone:Z

    .line 1419
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mResult:I

    .line 1420
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mRetryCount:I

    .line 1429
    iput-object p1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mSimCard:Lcom/android/internal/telephony/IccCard;

    .line 1430
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 1434
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 1435
    monitor-enter p0

    .line 1436
    :try_start_0
    new-instance v0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;

    invoke-direct {v0, p0}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;-><init>(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;)V

    iput-object v0, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mHandler:Landroid/os/Handler;

    .line 1463
    invoke-virtual {p0}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    .line 1465
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 1466
    return-void

    .line 1435
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized unlockSim(Ljava/lang/String;Ljava/lang/String;)[I
    .locals 5
    .param p1, "puk"    # Ljava/lang/String;
    .param p2, "pin"    # Ljava/lang/String;

    .prologue
    monitor-enter p0

    .line 1477
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    .line 1479
    :try_start_1
    invoke-virtual {p0}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1480
    :catch_0
    move-exception v1

    .line 1481
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 1484
    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x64

    invoke-static {v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 1486
    .local v0, "callback":Landroid/os/Message;
    if-nez p1, :cond_1

    .line 1487
    iget-object v3, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mSimCard:Lcom/android/internal/telephony/IccCard;

    invoke-interface {v3, p2, v0}, Lcom/android/internal/telephony/IccCard;->supplyPin(Ljava/lang/String;Landroid/os/Message;)V

    .line 1492
    :goto_1
    iget-boolean v3, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mDone:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v3, :cond_2

    .line 1494
    :try_start_4
    const-string/jumbo v3, "PhoneInterfaceManager"

    const-string/jumbo v4, "wait for done"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1495
    invoke-virtual {p0}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1496
    :catch_1
    move-exception v1

    .line 1498
    .restart local v1    # "e":Ljava/lang/InterruptedException;
    :try_start_5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 1489
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v3, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mSimCard:Lcom/android/internal/telephony/IccCard;

    invoke-interface {v3, p1, p2, v0}, Lcom/android/internal/telephony/IccCard;->supplyPuk(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_1

    .line 1501
    :cond_2
    const-string/jumbo v3, "PhoneInterfaceManager"

    const-string/jumbo v4, "done"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1502
    const/4 v3, 0x2

    new-array v2, v3, [I

    .line 1503
    .local v2, "resultArray":[I
    iget v3, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mResult:I

    const/4 v4, 0x0

    aput v3, v2, v4

    .line 1504
    iget v3, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->mRetryCount:I

    const/4 v4, 0x1

    aput v3, v2, v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    .line 1505
    return-object v2
.end method
