.class public Lcom/android/phone/VolteEnableManager;
.super Ljava/lang/Object;
.source "VolteEnableManager.java"

# interfaces
.implements Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;
.implements Lmiui/telephony/DefaultSimManager$DataSlotListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/VolteEnableManager$VolteEnableHandler;
    }
.end annotation


# static fields
.field private static LOG_TAG:Ljava/lang/String;

.field private static sInstance:Lcom/android/phone/VolteEnableManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/android/phone/VolteEnableManager$VolteEnableHandler;

.field private mLastSubInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/telephony/SubscriptionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/phone/VolteEnableManager;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/VolteEnableManager;)Landroid/content/Context;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/VolteEnableManager;

    .prologue
    iget-object v0, p0, Lcom/android/phone/VolteEnableManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/VolteEnableManager;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/VolteEnableManager;
    .param p1, "-value"    # Ljava/util/List;

    .prologue
    iput-object p1, p0, Lcom/android/phone/VolteEnableManager;->mLastSubInfos:Ljava/util/List;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/VolteEnableManager;Lmiui/telephony/SubscriptionInfo;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/VolteEnableManager;
    .param p1, "subInfo"    # Lmiui/telephony/SubscriptionInfo;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/VolteEnableManager;->isNewSubInfo(Lmiui/telephony/SubscriptionInfo;)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/VolteEnableManager;I)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/VolteEnableManager;
    .param p1, "subId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/VolteEnableManager;->updateEnhanced4gLteModeSetting(I)Z

    move-result v0

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "VolteEnableManager"

    sput-object v0, Lcom/android/phone/VolteEnableManager;->LOG_TAG:Ljava/lang/String;

    .line 30
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;-><init>(Lcom/android/phone/VolteEnableManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/phone/VolteEnableManager;->mHandler:Lcom/android/phone/VolteEnableManager$VolteEnableHandler;

    .line 52
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 53
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/telephony/DefaultSimManager;->addDataSlotListener(Lmiui/telephony/DefaultSimManager$DataSlotListener;)V

    .line 54
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/VolteEnableManager;->mContext:Landroid/content/Context;

    .line 55
    return-void
.end method

.method public static clearUserSelectedVolteState(I)V
    .locals 4
    .param p0, "subId"    # I

    .prologue
    .line 188
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 189
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 190
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_selected_volte_state"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 191
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 192
    return-void
.end method

.method public static getUserSelectedVolteState(I)I
    .locals 5
    .param p0, "subId"    # I

    .prologue
    .line 174
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 175
    .local v1, "sp":Landroid/content/SharedPreferences;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_selected_volte_state"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 176
    .local v0, "result":I
    sget-object v2, Lcom/android/phone/VolteEnableManager;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getUserSelectedVolteState subId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    return v0
.end method

.method public static init()V
    .locals 2

    .prologue
    .line 41
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    .line 42
    return-void

    .line 44
    :cond_0
    new-instance v0, Lcom/android/phone/VolteEnableManager;

    invoke-direct {v0}, Lcom/android/phone/VolteEnableManager;-><init>()V

    sput-object v0, Lcom/android/phone/VolteEnableManager;->sInstance:Lcom/android/phone/VolteEnableManager;

    .line 45
    return-void
.end method

.method private isNewSubInfo(Lmiui/telephony/SubscriptionInfo;)Z
    .locals 6
    .param p1, "subInfo"    # Lmiui/telephony/SubscriptionInfo;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 121
    if-nez p1, :cond_0

    .line 122
    return v4

    .line 124
    :cond_0
    iget-object v2, p0, Lcom/android/phone/VolteEnableManager;->mLastSubInfos:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/phone/VolteEnableManager;->mLastSubInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 125
    :cond_1
    return v5

    .line 127
    :cond_2
    iget-object v2, p0, Lcom/android/phone/VolteEnableManager;->mLastSubInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "lastSubInfo$iterator":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/telephony/SubscriptionInfo;

    .line 128
    .local v0, "lastSubInfo":Lmiui/telephony/SubscriptionInfo;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v2

    invoke-virtual {p1}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 129
    return v4

    .line 132
    .end local v0    # "lastSubInfo":Lmiui/telephony/SubscriptionInfo;
    :cond_4
    return v5
.end method

.method public static saveUserSelectedVolteState(IZ)V
    .locals 4
    .param p0, "subId"    # I
    .param p1, "enabled"    # Z

    .prologue
    .line 181
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 182
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 183
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "user_selected_volte_state"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 184
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 185
    return-void

    .line 183
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private updateEnhanced4gLteModeSetting(I)Z
    .locals 7
    .param p1, "subId"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 140
    const/4 v2, 0x0

    .line 141
    .local v2, "result":Z
    iget-object v3, p0, Lcom/android/phone/VolteEnableManager;->mContext:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 142
    sget-object v3, Lcom/android/phone/VolteEnableManager;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v4, "updateEnhanced4gLteModeSetting context == null"

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return v6

    .line 155
    :cond_0
    sget-boolean v3, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-nez v3, :cond_4

    .line 156
    iget-object v3, p0, Lcom/android/phone/VolteEnableManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "volte_vt_enabled"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 157
    .local v0, "enabled":I
    if-ne v0, v5, :cond_1

    .line 158
    const/4 v0, 0x1

    .line 160
    :cond_1
    iget-object v3, p0, Lcom/android/phone/VolteEnableManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "volte_vt_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 161
    const/4 v2, 0x1

    .line 169
    .end local v0    # "enabled":I
    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    sget-object v3, Lcom/android/phone/VolteEnableManager;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateEnhanced4gLteModeSetting subId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", result="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_3
    return v2

    .line 162
    :cond_4
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isVolteRegionFeatureEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 163
    iget-object v3, p0, Lcom/android/phone/VolteEnableManager;->mContext:Landroid/content/Context;

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/MiuiPhoneUtils;->isRegionSupportVoLTE(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 164
    .local v1, "regionSupported":Z
    if-nez v1, :cond_2

    .line 165
    iget-object v3, p0, Lcom/android/phone/VolteEnableManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "volte_vt_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 166
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onDataSlotReady(Z)V
    .locals 3
    .param p1, "isDataSlotChanged"    # Z

    .prologue
    .line 113
    if-eqz p1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/android/phone/VolteEnableManager;->mHandler:Lcom/android/phone/VolteEnableManager$VolteEnableHandler;

    iget-object v1, p0, Lcom/android/phone/VolteEnableManager;->mHandler:Lcom/android/phone/VolteEnableManager$VolteEnableHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->sendMessage(Landroid/os/Message;)Z

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/android/phone/VolteEnableManager;->mHandler:Lcom/android/phone/VolteEnableManager$VolteEnableHandler;

    iget-object v1, p0, Lcom/android/phone/VolteEnableManager;->mHandler:Lcom/android/phone/VolteEnableManager$VolteEnableHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onSubscriptionsChanged()V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/android/phone/VolteEnableManager;->mHandler:Lcom/android/phone/VolteEnableManager$VolteEnableHandler;

    iget-object v1, p0, Lcom/android/phone/VolteEnableManager;->mHandler:Lcom/android/phone/VolteEnableManager$VolteEnableHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->sendMessage(Landroid/os/Message;)Z

    .line 109
    return-void
.end method
