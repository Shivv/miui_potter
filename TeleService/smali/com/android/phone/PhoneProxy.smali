.class public Lcom/android/phone/PhoneProxy;
.super Ljava/lang/Object;
.source "PhoneProxy.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCallForwardingOption(Lcom/android/internal/telephony/Phone;IILandroid/os/Message;)V
    .locals 0
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "commandInterfaceCFReason"    # I
    .param p2, "commandInterfaceServiceClass"    # I
    .param p3, "onComplete"    # Landroid/os/Message;

    .prologue
    .line 76
    invoke-static {p0, p1, p2, p3}, Lcom/android/phone/PhoneAdapter;->getCallForwardingOption(Lcom/android/internal/telephony/Phone;IILandroid/os/Message;)V

    .line 78
    return-void
.end method

.method public static getCellLocation(Lcom/android/internal/telephony/Phone;)Landroid/telephony/CellLocation;
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 30
    instance-of v0, p0, Lcom/android/internal/telephony/GsmCdmaPhone;

    if-eqz v0, :cond_0

    .line 31
    check-cast p0, Lcom/android/internal/telephony/GsmCdmaPhone;

    .end local p0    # "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {p0}, Lcom/android/internal/telephony/GsmCdmaPhone;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    return-object v0

    .line 33
    .restart local p0    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getCommandsInterface(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/CommandsInterface;
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/android/internal/telephony/Phone;->mCi:Lcom/android/internal/telephony/CommandsInterface;

    return-object v0
.end method

.method public static getServiceStateTracker(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/ServiceStateTracker;
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v0

    return-object v0
.end method

.method public static getSimOperator(I)Ljava/lang/String;
    .locals 2
    .param p0, "phoneId"    # I

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 39
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v0, :cond_0

    return-object v1

    .line 40
    :cond_0
    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getVideoCFServiceClass()I
    .locals 1

    .prologue
    .line 70
    invoke-static {}, Lcom/android/phone/PhoneAdapter;->getVideoCFServiceClass()I

    move-result v0

    return v0
.end method

.method public static getVolteCarrierConfigValue(Landroid/os/PersistableBundle;)Z
    .locals 1
    .param p0, "carrierConfig"    # Landroid/os/PersistableBundle;

    .prologue
    .line 106
    const-string/jumbo v0, "carrier_volte_available_bool"

    invoke-virtual {p0, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isCallBarringFacilitySupportedOverImsPhone(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Z
    .locals 7
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "facility"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 48
    if-eqz p0, :cond_4

    .line 49
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 50
    const-string/jumbo v6, "carrier_config"

    .line 49
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    .line 51
    .local v0, "configManager":Landroid/telephony/CarrierConfigManager;
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v3

    .line 52
    .local v3, "pb":Landroid/os/PersistableBundle;
    if-eqz v3, :cond_2

    const-string/jumbo v5, "config_enable_callbarring_over_ims"

    invoke-virtual {v3, v5}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 54
    :goto_0
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/imsphone/ImsPhone;

    .line 55
    .local v1, "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    .line 56
    const-string/jumbo v5, "AI"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 57
    const-string/jumbo v5, "IR"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 55
    if-eqz v5, :cond_1

    .line 58
    :cond_0
    if-eqz v1, :cond_1

    .line 59
    invoke-virtual {v1}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getState()I

    move-result v4

    if-eqz v4, :cond_3

    .line 60
    invoke-static {p0}, Lcom/android/phone/PhoneAdapter;->isUtEnabled(Lcom/android/internal/telephony/Phone;)Z

    move-result v4

    .line 55
    :cond_1
    :goto_1
    return v4

    .line 53
    .end local v1    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    :cond_2
    const/4 v2, 0x0

    .local v2, "isOverIms":Z
    goto :goto_0

    .line 59
    .end local v2    # "isOverIms":Z
    .restart local v1    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    :cond_3
    const/4 v4, 0x1

    goto :goto_1

    .line 62
    .end local v0    # "configManager":Landroid/telephony/CarrierConfigManager;
    .end local v1    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    .end local v3    # "pb":Landroid/os/PersistableBundle;
    :cond_4
    return v4
.end method

.method public static isVolteEnabled(Lcom/android/internal/telephony/Phone;)Z
    .locals 1
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->isVolteEnabled()Z

    move-result v0

    return v0
.end method

.method public static selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;Lcom/android/internal/telephony/Phone;)V
    .locals 1
    .param p0, "network"    # Lcom/android/internal/telephony/OperatorInfo;
    .param p1, "response"    # Landroid/os/Message;
    .param p2, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 66
    const/4 v0, 0x1

    invoke-virtual {p2, p0, v0, p1}, Lcom/android/internal/telephony/Phone;->selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;ZLandroid/os/Message;)V

    .line 67
    return-void
.end method

.method public static setCallForwardingOption(Lcom/android/internal/telephony/Phone;IILjava/lang/String;IILandroid/os/Message;)V
    .locals 0
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p1, "commandInterfaceCFAction"    # I
    .param p2, "commandInterfaceCFReason"    # I
    .param p3, "dialingNumber"    # Ljava/lang/String;
    .param p4, "commandInterfaceServiceClass"    # I
    .param p5, "timerSeconds"    # I
    .param p6, "onComplete"    # Landroid/os/Message;

    .prologue
    .line 86
    invoke-static/range {p0 .. p6}, Lcom/android/phone/PhoneAdapter;->setCallForwardingOption(Lcom/android/internal/telephony/Phone;IILjava/lang/String;IILandroid/os/Message;)V

    .line 88
    return-void
.end method

.method public static setVolteCarrierConfigValue(Landroid/os/PersistableBundle;Z)V
    .locals 1
    .param p0, "carrierConfig"    # Landroid/os/PersistableBundle;
    .param p1, "value"    # Z

    .prologue
    .line 101
    const-string/jumbo v0, "carrier_volte_available_bool"

    invoke-virtual {p0, v0, p1}, Landroid/os/PersistableBundle;->putBoolean(Ljava/lang/String;Z)V

    .line 102
    const-string/jumbo v0, "carrier_vt_available_bool"

    invoke-virtual {p0, v0, p1}, Landroid/os/PersistableBundle;->putBoolean(Ljava/lang/String;Z)V

    .line 103
    return-void
.end method

.method public static updateConfigForPhoneId(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "slotId"    # I
    .param p2, "simState"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget-object v0, v0, Lcom/android/phone/PhoneGlobals;->configLoader:Lcom/android/phone/CarrierConfigLoader;

    if-eqz v0, :cond_0

    .line 96
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget-object v0, v0, Lcom/android/phone/PhoneGlobals;->configLoader:Lcom/android/phone/CarrierConfigLoader;

    invoke-virtual {v0, p1, p2}, Lcom/android/phone/CarrierConfigLoader;->updateConfigForPhoneId(ILjava/lang/String;)V

    .line 98
    :cond_0
    return-void
.end method
