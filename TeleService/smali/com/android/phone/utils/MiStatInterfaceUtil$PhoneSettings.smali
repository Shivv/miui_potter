.class public Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;
.super Ljava/lang/Object;
.source "MiStatInterfaceUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/utils/MiStatInterfaceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhoneSettings"
.end annotation


# static fields
.field private static final ANSWER_STATE_SETTINGS:[Ljava/lang/String;

.field private static final AUTO_IP_SETTINGS:[Ljava/lang/String;

.field private static final CALL_ADVANCED_SETTINGS:[Ljava/lang/String;

.field private static final CALL_RECORD_SETTINGS:[Ljava/lang/String;

.field private static final PARAMS_ANSWER_STATE:[Ljava/lang/String;

.field private static final PARAMS_AUTO_IP:[Ljava/lang/String;

.field private static final PARAMS_CALL_ADVANCED:[Ljava/lang/String;

.field private static final PARAMS_CALL_RECORD:[Ljava/lang/String;

.field private static final PARAMS_TELOCATION:[Ljava/lang/String;

.field private static final TELOCATION_SETTINGS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 156
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    .line 157
    const-string/jumbo v1, "CKEnableAntiStrange"

    aput-object v1, v0, v3

    .line 158
    const-string/jumbo v1, "CKEnableProximitySensor"

    aput-object v1, v0, v4

    .line 159
    const-string/jumbo v1, "CKEnableFlashWhenRing"

    aput-object v1, v0, v5

    .line 160
    const-string/jumbo v1, "CKEnableTurnOverMuteMode"

    aput-object v1, v0, v6

    .line 161
    const-string/jumbo v1, "CKEnableHandonMode"

    aput-object v1, v0, v7

    .line 162
    const-string/jumbo v1, "CKEnableCrescendoMode"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 156
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_ANSWER_STATE:[Ljava/lang/String;

    .line 165
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    .line 166
    const-string/jumbo v1, "button_antispam_strange"

    aput-object v1, v0, v3

    .line 167
    const-string/jumbo v1, "button_enable_proximity"

    aput-object v1, v0, v4

    .line 168
    const-string/jumbo v1, "flash_when_ring_enabled"

    aput-object v1, v0, v5

    .line 169
    const-string/jumbo v1, "button_turn_over_mute"

    aput-object v1, v0, v6

    .line 170
    const-string/jumbo v1, "button_handon_ringer"

    aput-object v1, v0, v7

    .line 171
    const-string/jumbo v1, "button_crescendo_ringer"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 165
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->ANSWER_STATE_SETTINGS:[Ljava/lang/String;

    .line 192
    new-array v0, v7, [Ljava/lang/String;

    .line 193
    const-string/jumbo v1, "CKEnableAutoIp"

    aput-object v1, v0, v3

    .line 194
    const-string/jumbo v1, "CKCurrentAreaCode"

    aput-object v1, v0, v4

    .line 195
    const-string/jumbo v1, "CKSupportLocalNumber"

    aput-object v1, v0, v5

    .line 196
    const-string/jumbo v1, "CKEnableAddZeroPrefix"

    aput-object v1, v0, v6

    .line 192
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_AUTO_IP:[Ljava/lang/String;

    .line 199
    new-array v0, v7, [Ljava/lang/String;

    .line 200
    const-string/jumbo v1, "button_autoip"

    aput-object v1, v0, v3

    .line 201
    const-string/jumbo v1, "current_areacode"

    aput-object v1, v0, v4

    .line 202
    const-string/jumbo v1, "button_auto_ip_support_local_numbers"

    aput-object v1, v0, v5

    .line 203
    const-string/jumbo v1, "button_add_zero_prefix"

    aput-object v1, v0, v6

    .line 199
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->AUTO_IP_SETTINGS:[Ljava/lang/String;

    .line 218
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    .line 219
    const-string/jumbo v1, "CKT9IndexMethod"

    aput-object v1, v0, v3

    .line 220
    const-string/jumbo v1, "CKEnableAutoRedial"

    aput-object v1, v0, v4

    .line 221
    const-string/jumbo v1, "CKCallBackground"

    aput-object v1, v0, v5

    .line 222
    const-string/jumbo v1, "CKMissedCallNotifyTime"

    aput-object v1, v0, v6

    .line 223
    const-string/jumbo v1, "CKEnableConnectDisconnectVibrate"

    aput-object v1, v0, v7

    .line 224
    const-string/jumbo v1, "CKCallWaitingTone"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 225
    const-string/jumbo v1, "CKDTMFToneType"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 218
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_CALL_ADVANCED:[Ljava/lang/String;

    .line 228
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    .line 229
    const-string/jumbo v1, "t9_indexing_key"

    aput-object v1, v0, v3

    .line 230
    const-string/jumbo v1, "button_auto_redial"

    aput-object v1, v0, v4

    .line 231
    const-string/jumbo v1, "incall_background_key"

    aput-object v1, v0, v5

    .line 232
    const-string/jumbo v1, "button_missed_call_notify_times"

    aput-object v1, v0, v6

    .line 233
    const-string/jumbo v1, "button_connect_disconnect_vibrate"

    aput-object v1, v0, v7

    .line 234
    const-string/jumbo v1, "call_waiting_tone"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 235
    const-string/jumbo v1, "dtmf_tone_type"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 236
    const-string/jumbo v1, "button_enable_reject_via_sms"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 228
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->CALL_ADVANCED_SETTINGS:[Ljava/lang/String;

    .line 240
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 241
    const-string/jumbo v1, "CKEnableRecordingNotification"

    aput-object v1, v0, v3

    .line 242
    const-string/jumbo v1, "CKEnableAutoRecord"

    aput-object v1, v0, v4

    .line 243
    const-string/jumbo v1, "CKAutoRecordScenarios"

    aput-object v1, v0, v5

    .line 244
    const-string/jumbo v1, "CKEnableUnknownNumberRecord"

    aput-object v1, v0, v6

    .line 245
    const-string/jumbo v1, "CKEnableYellowPageNumberRecord"

    aput-object v1, v0, v7

    .line 240
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_CALL_RECORD:[Ljava/lang/String;

    .line 248
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    .line 249
    const-string/jumbo v1, "button_call_recording_notification"

    aput-object v1, v0, v3

    .line 250
    const-string/jumbo v1, "button_auto_record_call"

    aput-object v1, v0, v4

    .line 251
    const-string/jumbo v1, "radio_record_scenario"

    aput-object v1, v0, v5

    .line 252
    const-string/jumbo v1, "button_record_unknown_number"

    aput-object v1, v0, v6

    .line 253
    const-string/jumbo v1, "button_record_yellowpage_number"

    aput-object v1, v0, v7

    .line 248
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->CALL_RECORD_SETTINGS:[Ljava/lang/String;

    .line 271
    new-array v0, v6, [Ljava/lang/String;

    .line 272
    const-string/jumbo v1, "CKEnableShowLocation"

    aput-object v1, v0, v3

    .line 273
    const-string/jumbo v1, "CKEnableAutoCountryCode"

    aput-object v1, v0, v4

    .line 274
    const-string/jumbo v1, "CKCountryCode"

    aput-object v1, v0, v5

    .line 271
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_TELOCATION:[Ljava/lang/String;

    .line 277
    new-array v0, v6, [Ljava/lang/String;

    .line 278
    const-string/jumbo v1, "enable_telocation"

    aput-object v1, v0, v3

    .line 279
    const-string/jumbo v1, "auto_country_code"

    aput-object v1, v0, v4

    .line 280
    const-string/jumbo v1, "persist.radio.countrycode"

    aput-object v1, v0, v5

    .line 277
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->TELOCATION_SETTINGS:[Ljava/lang/String;

    .line 110
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getAutoAnswerData(Landroid/content/Context;)Ljava/util/Map;
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 180
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 181
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/android/phone/utils/MiStatInterfaceUtil;->-wrap0(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 182
    .local v1, "sp":Landroid/content/SharedPreferences;
    const-string/jumbo v2, "CKEnableAutoAnswer"

    .line 183
    const-string/jumbo v3, "button_auto_answer"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 182
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    const-string/jumbo v2, "CKAutoAnswerDelay"

    .line 185
    const-string/jumbo v3, "button_auto_answer_delay"

    const-string/jumbo v4, "3"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 184
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    const-string/jumbo v2, "CKAutoAnswerScenario"

    .line 187
    const-string/jumbo v3, "button_auto_answer_scenario"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 186
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    return-object v0
.end method

.method private static getAutoIpData(Landroid/content/Context;)Ljava/util/Map;
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 207
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 208
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 209
    .local v3, "resolver":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 210
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    sget-object v4, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_AUTO_IP:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 211
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_AUTO_IP:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->AUTO_IP_SETTINGS:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/provider/MiuiSettings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 209
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 214
    .end local v2    # "j":I
    :cond_1
    return-object v0
.end method

.method private static getMobileNetworkData(Landroid/content/Context;)Ljava/util/Map;
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 294
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v3, "CKMobileNetworkSystemSelect"

    .line 295
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 296
    const-string/jumbo v5, "roaming_settings"

    const/4 v6, 0x0

    .line 295
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 294
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    sget v3, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    if-ge v2, v3, :cond_0

    .line 299
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "CKMobileNetworkNetworkType_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 300
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 301
    const-string/jumbo v5, "preferred_network_mode"

    .line 300
    invoke-static {v4, v5, v2}, Landroid/telephony/TelephonyManager;->getIntAtIndex(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 299
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 303
    :catch_0
    move-exception v1

    .line 304
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-static {}, Lcom/android/phone/utils/MiStatInterfaceUtil;->-get0()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setting of PREFERRED_NETWORK_MODE is not found "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_0
    const-string/jumbo v3, "CKMobileNetworkDataEnable"

    .line 307
    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getDataEnabled()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    .line 306
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    const-string/jumbo v3, "CKMobileNetwork4gLTE"

    .line 309
    invoke-static {p0}, Lcom/android/ims/ImsManager;->isEnhanced4gLteModeSettingEnabledByUser(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    .line 308
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    const-string/jumbo v3, "CKobileNetworkMmsEnabled"

    .line 311
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 312
    const-string/jumbo v5, "always_enable_mms"

    const/4 v6, 0x1

    .line 311
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 310
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 314
    const-string/jumbo v3, "CKMobileNetworkDefaultData"

    .line 315
    invoke-static {}, Lmiui/telephony/SubscriptionManagerEx;->getDefault()Lmiui/telephony/SubscriptionManagerEx;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionManagerEx;->getDefaultDataSlotId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 314
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    const-string/jumbo v3, "CKMobileNetworkDefaultVoice"

    .line 317
    invoke-static {}, Lmiui/telephony/SubscriptionManagerEx;->getDefault()Lmiui/telephony/SubscriptionManagerEx;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionManagerEx;->getDefaultVoiceSlotId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 316
    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    :cond_1
    return-object v0
.end method

.method private static getSettingData(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Map;
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "params"    # [Ljava/lang/String;
    .param p2, "keys"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 325
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 326
    .local v2, "resolver":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_0

    .line 327
    aget-object v3, p1, v1

    aget-object v4, p2, v1

    invoke-static {v2, v4}, Landroid/provider/MiuiSettings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 329
    :cond_0
    return-object v0
.end method

.method private static getSipData(Landroid/content/Context;)Ljava/util/Map;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 262
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Lcom/android/services/telephony/sip/SipSharedPreferences;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/sip/SipSharedPreferences;-><init>(Landroid/content/Context;)V

    .line 263
    .local v1, "sipSharedPreferences":Lcom/android/services/telephony/sip/SipSharedPreferences;
    const-string/jumbo v2, "CKSipCallOptions"

    .line 264
    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipSharedPreferences;->getSipCallOption()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 263
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    const-string/jumbo v2, "CKEnableSipReceiveCalls"

    .line 266
    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipSharedPreferences;->isReceivingCallsEnabled()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    .line 265
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    return-object v0
.end method

.method public static final recordSettingsEvent(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 128
    const-string/jumbo v0, "event_settings"

    const-string/jumbo v1, "key_answer_state_settings"

    .line 129
    sget-object v2, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_ANSWER_STATE:[Ljava/lang/String;

    sget-object v3, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->ANSWER_STATE_SETTINGS:[Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->getSettingData(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 128
    invoke-static {v0, v1, v2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 130
    const-string/jumbo v0, "event_settings"

    const-string/jumbo v1, "key_auto_answer_settings"

    .line 131
    invoke-static {p0}, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->getAutoAnswerData(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v2

    .line 130
    invoke-static {v0, v1, v2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 132
    const-string/jumbo v0, "event_settings"

    const-string/jumbo v1, "key_auto_ip_settings"

    .line 133
    invoke-static {p0}, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->getAutoIpData(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v2

    .line 132
    invoke-static {v0, v1, v2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 134
    const-string/jumbo v0, "event_settings"

    const-string/jumbo v1, "key_call_advanced_settings"

    .line 135
    sget-object v2, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_CALL_ADVANCED:[Ljava/lang/String;

    sget-object v3, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->CALL_ADVANCED_SETTINGS:[Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->getSettingData(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 134
    invoke-static {v0, v1, v2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 136
    const-string/jumbo v0, "event_settings"

    const-string/jumbo v1, "key_call_record_settings"

    .line 137
    sget-object v2, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_CALL_RECORD:[Ljava/lang/String;

    sget-object v3, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->CALL_RECORD_SETTINGS:[Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->getSettingData(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 136
    invoke-static {v0, v1, v2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 138
    const-string/jumbo v0, "event_settings"

    const-string/jumbo v1, "key_sip_settings"

    .line 139
    invoke-static {p0}, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->getSipData(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v2

    .line 138
    invoke-static {v0, v1, v2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 140
    const-string/jumbo v0, "event_settings"

    const-string/jumbo v1, "key_telocation_settings"

    .line 141
    sget-object v2, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->PARAMS_TELOCATION:[Ljava/lang/String;

    sget-object v3, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->TELOCATION_SETTINGS:[Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->getSettingData(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 140
    invoke-static {v0, v1, v2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 142
    const-string/jumbo v0, "event_settings"

    const-string/jumbo v1, "key_mobile_network_settings"

    .line 143
    invoke-static {p0}, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->getMobileNetworkData(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v2

    .line 142
    invoke-static {v0, v1, v2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 144
    return-void
.end method

.method public static final recordSlotIdInMultiSimInfoEditor(Ljava/lang/String;I)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "slotId"    # I

    .prologue
    .line 150
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 151
    .local v0, "hashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v1, "param_slot_id"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string/jumbo v1, "event_settings"

    invoke-static {v1, p0, v0}, Lcom/android/phone/utils/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 153
    return-void
.end method
