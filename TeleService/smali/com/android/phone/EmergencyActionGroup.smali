.class public Lcom/android/phone/EmergencyActionGroup;
.super Landroid/widget/FrameLayout;
.source "EmergencyActionGroup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/EmergencyActionGroup$1;,
        Lcom/android/phone/EmergencyActionGroup$2;
    }
.end annotation


# instance fields
.field private final mFastOutLinearInInterpolator:Landroid/view/animation/Interpolator;

.field private final mHideRunnable:Ljava/lang/Runnable;

.field private mHiding:Z

.field private mLastRevealed:Landroid/view/View;

.field private mLaunchHint:Landroid/view/View;

.field private mPendingTouchEvent:Landroid/view/MotionEvent;

.field private final mRippleRunnable:Ljava/lang/Runnable;

.field private mRippleView:Landroid/view/View;

.field private mSelectedContainer:Landroid/view/ViewGroup;

.field private mSelectedLabel:Landroid/widget/TextView;


# direct methods
.method static synthetic -get0(Lcom/android/phone/EmergencyActionGroup;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/EmergencyActionGroup;

    .prologue
    iget-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mRippleRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/EmergencyActionGroup;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/EmergencyActionGroup;

    .prologue
    iget-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/EmergencyActionGroup;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/EmergencyActionGroup;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/EmergencyActionGroup;->mHiding:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/EmergencyActionGroup;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/EmergencyActionGroup;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/EmergencyActionGroup;->hideTheButton()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/EmergencyActionGroup;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/EmergencyActionGroup;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/EmergencyActionGroup;->startRipple()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 346
    new-instance v0, Lcom/android/phone/EmergencyActionGroup$1;

    invoke-direct {v0, p0}, Lcom/android/phone/EmergencyActionGroup$1;-><init>(Lcom/android/phone/EmergencyActionGroup;)V

    iput-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mHideRunnable:Ljava/lang/Runnable;

    .line 354
    new-instance v0, Lcom/android/phone/EmergencyActionGroup$2;

    invoke-direct {v0, p0}, Lcom/android/phone/EmergencyActionGroup$2;-><init>(Lcom/android/phone/EmergencyActionGroup;)V

    iput-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mRippleRunnable:Ljava/lang/Runnable;

    .line 68
    const v0, 0x10c000f

    .line 67
    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mFastOutLinearInInterpolator:Landroid/view/animation/Interpolator;

    .line 69
    return-void
.end method

.method private animateHintText(Landroid/view/View;Landroid/view/View;Landroid/animation/Animator;)V
    .locals 6
    .param p1, "selectedView"    # Landroid/view/View;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "reveal"    # Landroid/animation/Animator;

    .prologue
    .line 272
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x5

    int-to-float v0, v0

    .line 271
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 273
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 274
    invoke-virtual {p3}, Landroid/animation/Animator;->getDuration()J

    move-result-wide v2

    const-wide/16 v4, 0x3

    div-long/2addr v2, v4

    .line 273
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 275
    invoke-virtual {p3}, Landroid/animation/Animator;->getDuration()J

    move-result-wide v2

    const-wide/16 v4, 0x5

    div-long/2addr v2, v4

    .line 273
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 276
    const/4 v1, 0x0

    .line 273
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 277
    iget-object v1, p0, Lcom/android/phone/EmergencyActionGroup;->mFastOutLinearInInterpolator:Landroid/view/animation/Interpolator;

    .line 273
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 279
    return-void
.end method

.method private getComponentName(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;
    .locals 3
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;

    .prologue
    const/4 v1, 0x0

    .line 215
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v0, :cond_1

    :cond_0
    return-object v1

    .line 216
    :cond_1
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 217
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 216
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private hideTheButton()V
    .locals 7

    .prologue
    .line 282
    iget-boolean v4, p0, Lcom/android/phone/EmergencyActionGroup;->mHiding:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_1

    .line 283
    :cond_0
    return-void

    .line 286
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/phone/EmergencyActionGroup;->mHiding:Z

    .line 288
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v4}, Lcom/android/phone/EmergencyActionGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 290
    iget-object v3, p0, Lcom/android/phone/EmergencyActionGroup;->mLastRevealed:Landroid/view/View;

    .line 291
    .local v3, "v":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v0, v4, v5

    .line 292
    .local v0, "centerX":I
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v1, v4, v5

    .line 294
    .local v1, "centerY":I
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    .line 297
    iget-object v5, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getWidth()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 298
    iget-object v6, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    sub-int/2addr v6, v1

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 297
    add-int/2addr v5, v6

    int-to-float v5, v5

    .line 299
    const/4 v6, 0x0

    .line 293
    invoke-static {v4, v0, v1, v5, v6}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v2

    .line 300
    .local v2, "reveal":Landroid/animation/Animator;
    new-instance v4, Lcom/android/phone/EmergencyActionGroup$3;

    invoke-direct {v4, p0}, Lcom/android/phone/EmergencyActionGroup$3;-><init>(Lcom/android/phone/EmergencyActionGroup;)V

    invoke-virtual {v2, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 308
    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    .line 311
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 312
    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    .line 314
    :cond_2
    return-void
.end method

.method private isSystemApp(Landroid/content/pm/PackageInfo;)Z
    .locals 2
    .param p1, "info"    # Landroid/content/pm/PackageInfo;

    .prologue
    const/4 v0, 0x0

    .line 210
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 210
    :cond_0
    return v0
.end method

.method private queryAssistActivities()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/android/phone/EmergencyActionGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 198
    const-string/jumbo v4, "emergency_assistance_application"

    .line 196
    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "assistPackage":Ljava/lang/String;
    const/4 v1, 0x0

    .line 201
    .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 202
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "android.telephony.action.EMERGENCY_ASSISTANCE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 204
    .local v2, "queryIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/android/phone/EmergencyActionGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 206
    .end local v1    # "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v2    # "queryIntent":Landroid/content/Intent;
    :cond_0
    return-object v1
.end method

.method private resolveAssistPackageAndQueryActivites()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v12, 0x0

    .line 158
    invoke-direct {p0}, Lcom/android/phone/EmergencyActionGroup;->queryAssistActivities()Ljava/util/List;

    move-result-object v3

    .line 160
    .local v3, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/EmergencyActionGroup;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 162
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v7, Landroid/content/Intent;

    const-string/jumbo v8, "android.telephony.action.EMERGENCY_ASSISTANCE"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    .local v7, "queryIntent":Landroid/content/Intent;
    invoke-virtual {v5, v7, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 165
    const/4 v0, 0x0

    .line 166
    .local v0, "bestMatch":Landroid/content/pm/PackageInfo;
    const/4 v2, 0x0

    .end local v0    # "bestMatch":Landroid/content/pm/PackageInfo;
    .local v2, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_4

    .line 167
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    iget-object v8, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v8, :cond_2

    .line 166
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 168
    :cond_2
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    iget-object v8, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 171
    .local v6, "packageName":Ljava/lang/String;
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v5, v6, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 176
    .local v4, "packageInfo":Landroid/content/pm/PackageInfo;
    invoke-direct {p0, v4}, Lcom/android/phone/EmergencyActionGroup;->isSystemApp(Landroid/content/pm/PackageInfo;)Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz v0, :cond_3

    .line 177
    iget-wide v8, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iget-wide v10, v4, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    cmp-long v8, v8, v10

    if-lez v8, :cond_1

    .line 178
    :cond_3
    move-object v0, v4

    .local v0, "bestMatch":Landroid/content/pm/PackageInfo;
    goto :goto_1

    .line 172
    .end local v0    # "bestMatch":Landroid/content/pm/PackageInfo;
    .end local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_1

    .line 182
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v6    # "packageName":Ljava/lang/String;
    :cond_4
    if-eqz v0, :cond_5

    .line 183
    invoke-virtual {p0}, Lcom/android/phone/EmergencyActionGroup;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 184
    const-string/jumbo v9, "emergency_assistance_application"

    .line 185
    iget-object v10, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 183
    invoke-static {v8, v9, v10}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 186
    invoke-direct {p0}, Lcom/android/phone/EmergencyActionGroup;->queryAssistActivities()Ljava/util/List;

    move-result-object v8

    return-object v8

    .line 188
    :cond_5
    return-object v12

    .line 191
    .end local v2    # "i":I
    .end local v5    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v7    # "queryIntent":Landroid/content/Intent;
    :cond_6
    return-object v3
.end method

.method private revealTheButton(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v8, 0x7f0d000a

    move-object v4, p1

    .line 243
    check-cast v4, Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 244
    .local v0, "buttonText":Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedLabel:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedLabel:Landroid/widget/TextView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setAutoSizeTextTypeWithDefaults(I)V

    .line 246
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 247
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v1, v4, v5

    .line 248
    .local v1, "centerX":I
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v2, v4, v5

    .line 250
    .local v2, "centerY":I
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    .line 253
    const/4 v5, 0x0

    .line 254
    iget-object v6, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    sub-int/2addr v6, v1

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 255
    iget-object v7, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    sub-int/2addr v7, v2

    invoke-static {v2, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 254
    add-int/2addr v6, v7

    int-to-float v6, v6

    .line 249
    invoke-static {v4, v1, v2, v5, v6}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v3

    .line 256
    .local v3, "reveal":Landroid/animation/Animator;
    invoke-virtual {v3}, Landroid/animation/Animator;->start()V

    .line 258
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedLabel:Landroid/widget/TextView;

    invoke-direct {p0, v4, p1, v3}, Lcom/android/phone/EmergencyActionGroup;->animateHintText(Landroid/view/View;Landroid/view/View;Landroid/animation/Animator;)V

    .line 259
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mLaunchHint:Landroid/view/View;

    invoke-direct {p0, v4, p1, v3}, Lcom/android/phone/EmergencyActionGroup;->animateHintText(Landroid/view/View;Landroid/view/View;Landroid/animation/Animator;)V

    .line 261
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {p1, v8}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    .line 262
    iput-object p1, p0, Lcom/android/phone/EmergencyActionGroup;->mLastRevealed:Landroid/view/View;

    .line 263
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mHideRunnable:Ljava/lang/Runnable;

    const-wide/16 v6, 0xbb8

    invoke-virtual {p0, v4, v6, v7}, Lcom/android/phone/EmergencyActionGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 264
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mRippleRunnable:Ljava/lang/Runnable;

    const-wide/16 v6, 0x1f4

    invoke-virtual {p0, v4, v6, v7}, Lcom/android/phone/EmergencyActionGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 267
    iget-object v4, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 268
    return-void
.end method

.method private setupAssistActions()V
    .locals 10

    .prologue
    .line 126
    const v7, 0x7f0d00b0

    const v8, 0x7f0d00b1

    const v9, 0x7f0d00b2

    filled-new-array {v7, v8, v9}, [I

    move-result-object v1

    .line 131
    .local v1, "buttonIds":[I
    invoke-direct {p0}, Lcom/android/phone/EmergencyActionGroup;->resolveAssistPackageAndQueryActivites()Ljava/util/List;

    move-result-object v4

    .line 136
    .local v4, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v7, 0x3

    if-ge v2, v7, :cond_2

    .line 137
    aget v7, v1, v2

    invoke-virtual {p0, v7}, Lcom/android/phone/EmergencyActionGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 138
    .local v0, "button":Landroid/widget/Button;
    const/4 v6, 0x0

    .line 140
    .local v6, "visible":Z
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v2, :cond_0

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 143
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 144
    .local v3, "info":Landroid/content/pm/ResolveInfo;
    invoke-direct {p0, v3}, Lcom/android/phone/EmergencyActionGroup;->getComponentName(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v5

    .line 147
    .local v5, "name":Landroid/content/ComponentName;
    new-instance v7, Landroid/content/Intent;

    const-string/jumbo v8, "android.telephony.action.EMERGENCY_ASSISTANCE"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v7

    .line 146
    const v8, 0x7f0d000a

    invoke-virtual {v0, v8, v7}, Landroid/widget/Button;->setTag(ILjava/lang/Object;)V

    .line 149
    invoke-virtual {p0}, Lcom/android/phone/EmergencyActionGroup;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 150
    const/4 v6, 0x1

    .line 153
    .end local v3    # "info":Landroid/content/pm/ResolveInfo;
    .end local v5    # "name":Landroid/content/ComponentName;
    :cond_0
    if-eqz v6, :cond_1

    const/4 v7, 0x0

    :goto_1
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 136
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 153
    :cond_1
    const/16 v7, 0x8

    goto :goto_1

    .line 155
    .end local v0    # "button":Landroid/widget/Button;
    .end local v6    # "visible":Z
    :cond_2
    return-void
.end method

.method private startRipple()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 317
    iget-object v1, p0, Lcom/android/phone/EmergencyActionGroup;->mRippleView:Landroid/view/View;

    .line 318
    .local v1, "ripple":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 319
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 322
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 323
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 325
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 320
    invoke-static {v1, v2, v3, v5, v4}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    .line 326
    .local v0, "reveal":Landroid/animation/Animator;
    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 327
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 329
    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 330
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 331
    new-instance v3, Lcom/android/phone/EmergencyActionGroup$4;

    invoke-direct {v3, p0, v1}, Lcom/android/phone/EmergencyActionGroup$4;-><init>(Lcom/android/phone/EmergencyActionGroup;Landroid/view/View;)V

    .line 330
    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 344
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 105
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/android/phone/EmergencyActionGroup;->mPendingTouchEvent:Landroid/view/MotionEvent;

    if-ne v1, p1, :cond_0

    if-eqz v0, :cond_0

    .line 106
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/phone/EmergencyActionGroup;->mPendingTouchEvent:Landroid/view/MotionEvent;

    .line 108
    :cond_0
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 222
    const v1, 0x7f0d000a

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 224
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 228
    :pswitch_0
    iget-object v1, p0, Lcom/android/phone/EmergencyActionGroup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 229
    invoke-virtual {p0}, Lcom/android/phone/EmergencyActionGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 231
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/phone/EmergencyActionGroup;->revealTheButton(Landroid/view/View;)V

    goto :goto_0

    .line 235
    :pswitch_1
    iget-boolean v1, p0, Lcom/android/phone/EmergencyActionGroup;->mHiding:Z

    if-nez v1, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/android/phone/EmergencyActionGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d00b0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 80
    const v0, 0x7f0d00b3

    invoke-virtual {p0, v0}, Lcom/android/phone/EmergencyActionGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    .line 81
    iget-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v0, 0x7f0d00b5

    invoke-virtual {p0, v0}, Lcom/android/phone/EmergencyActionGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mSelectedLabel:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0d00b4

    invoke-virtual {p0, v0}, Lcom/android/phone/EmergencyActionGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mRippleView:Landroid/view/View;

    .line 84
    const v0, 0x7f0d00b6

    invoke-virtual {p0, v0}, Lcom/android/phone/EmergencyActionGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/EmergencyActionGroup;->mLaunchHint:Landroid/view/View;

    .line 85
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 74
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 0
    .param p1, "visibility"    # I

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowVisibilityChanged(I)V

    .line 90
    if-nez p1, :cond_0

    .line 91
    invoke-direct {p0}, Lcom/android/phone/EmergencyActionGroup;->setupAssistActions()V

    .line 93
    :cond_0
    return-void
.end method
