.class public Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;
.super Ljava/lang/Object;
.source "NetworkModeHandlerMtk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/networkmode/NetworkModeHandlerMtk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SettingArgs"
.end annotation


# instance fields
.field dataSlot:I

.field mode:I

.field final synthetic this$0:Lcom/android/phone/networkmode/NetworkModeHandlerMtk;

.field times:I


# direct methods
.method public constructor <init>(Lcom/android/phone/networkmode/NetworkModeHandlerMtk;I)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/phone/networkmode/NetworkModeHandlerMtk;
    .param p2, "defaultData"    # I

    .prologue
    .line 35
    iput-object p1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->this$0:Lcom/android/phone/networkmode/NetworkModeHandlerMtk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p2, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->dataSlot:I

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->times:I

    .line 38
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->dataSlot:I

    invoke-static {v0, v1}, Lmiui/telephony/DefaultSimManager;->getPreferredNetworkModeFromDb(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->mode:I

    .line 39
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 29
    const-string/jumbo v1, "SettingArgs: dataSlot="

    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 29
    iget v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->dataSlot:I

    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 30
    const-string/jumbo v1, " times="

    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 30
    iget v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->times:I

    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 31
    const-string/jumbo v1, " mode="

    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 31
    iget v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->mode:I

    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
