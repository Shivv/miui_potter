.class public Lcom/android/phone/networkmode/NMFilterVirtualSim;
.super Lcom/android/phone/networkmode/NMFilter;
.source "NMFilterVirtualSim.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/android/phone/networkmode/NMFilter;-><init>()V

    return-void
.end method

.method public static getSupportedNetworkModesForVirtualSim([I)[I
    .locals 4
    .param p0, "supportModes"    # [I

    .prologue
    .line 26
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string/jumbo v3, "getSupportedNetworkModesForVirtualSim"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 27
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    invoke-static {v2}, Landroid/provider/MiuiSettings$VirtualSim;->isSupport4G(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    const-string/jumbo v2, " support4G"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 29
    return-object p0

    .line 31
    :cond_0
    invoke-static {p0}, Lcom/android/phone/networkmode/NMFilterVirtualSim;->removeLteAndTdd([I)[I

    move-result-object v0

    .line 32
    .local v0, "ret":[I
    const-string/jumbo v2, " networkModes from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 33
    const-string/jumbo v3, " to "

    .line 32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 33
    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    .line 32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 34
    return-object v0
.end method

.method private static removeLteAndTdd([I)[I
    .locals 9
    .param p0, "modesToBeRemoved"    # [I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 45
    const/4 v5, 0x6

    new-array v0, v5, [I

    .local v0, "allLteAndTddModes":[I
    fill-array-data v0, :array_0

    .line 46
    const/4 v1, 0x0

    .line 47
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, p0

    if-ge v2, v5, :cond_1

    .line 48
    aget v5, p0, v2

    invoke-static {v0, v5}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v5

    if-nez v5, :cond_0

    .line 49
    add-int/lit8 v1, v1, 0x1

    .line 47
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 52
    :cond_1
    if-lez v1, :cond_4

    .line 53
    new-array v4, v1, [I

    .line 54
    .local v4, "withoutLteModes":[I
    const/4 v2, 0x0

    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v5, p0

    if-ge v2, v5, :cond_3

    .line 55
    aget v5, p0, v2

    invoke-static {v0, v5}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v5

    if-nez v5, :cond_2

    .line 56
    aget v5, p0, v2

    aput v5, v4, v3

    .line 57
    add-int/lit8 v3, v3, 0x1

    .line 54
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 60
    :cond_3
    return-object v4

    .line 63
    .end local v3    # "j":I
    .end local v4    # "withoutLteModes":[I
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "removeLteAndTdd error args="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 65
    new-array v5, v8, [I

    aput v8, v5, v7

    return-object v5

    .line 45
    nop

    :array_0
    .array-data 4
        0xa
        0x8
        0x14
        0x11
        0x9
        0x10
    .end array-data
.end method


# virtual methods
.method public filter([[I)[[I
    .locals 2
    .param p1, "networkModes"    # [[I

    .prologue
    .line 18
    invoke-static {}, Lmiui/telephony/VirtualSimUtils;->getInstance()Lmiui/telephony/VirtualSimUtils;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/VirtualSimUtils;->getVirtualSimSlotId()I

    move-result v0

    .line 19
    .local v0, "virtualSlot":I
    sget v1, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    if-eq v1, v0, :cond_0

    .line 20
    aget-object v1, p1, v0

    invoke-static {v1}, Lcom/android/phone/networkmode/NMFilterVirtualSim;->getSupportedNetworkModesForVirtualSim([I)[I

    move-result-object v1

    aput-object v1, p1, v0

    .line 22
    :cond_0
    return-object p1
.end method
