.class public Lcom/android/phone/networkmode/NetworkMode;
.super Ljava/lang/Object;
.source "NetworkMode.java"


# static fields
.field public static final INDIA_MCC_ARRAY:[I

.field public static final INDONESIA_MCC_ARRAY:[I

.field public static final VICE_SLOT_2G_DEVICES_GREATER_THAN_M:[Ljava/lang/String;

.field public static sLockNetworkType:I

.field private static sNetworkMode:Lcom/android/phone/networkmode/NetworkMode;


# instance fields
.field private mFilters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/phone/networkmode/NMFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 170
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "land"

    aput-object v1, v0, v3

    const-string/jumbo v1, "prada"

    aput-object v1, v0, v4

    const-string/jumbo v1, "virgo"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string/jumbo v1, "aqua"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string/jumbo v1, "leo"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string/jumbo v1, "libra"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "song"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/phone/networkmode/NetworkMode;->VICE_SLOT_2G_DEVICES_GREATER_THAN_M:[Ljava/lang/String;

    .line 188
    const/16 v0, 0x194

    const/16 v1, 0x195

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/phone/networkmode/NetworkMode;->INDIA_MCC_ARRAY:[I

    .line 189
    new-array v0, v4, [I

    const/16 v1, 0x1fe

    aput v1, v0, v3

    sput-object v0, Lcom/android/phone/networkmode/NetworkMode;->INDONESIA_MCC_ARRAY:[I

    .line 40
    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/phone/networkmode/NetworkMode;->mFilters:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Lcom/android/phone/networkmode/NMFilterVirtualSim;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NMFilterVirtualSim;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/phone/networkmode/NetworkMode;->addFilter(Lcom/android/phone/networkmode/NMFilter;)V

    .line 62
    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->isDeviceLockViceNetworkModeForCmcc()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    new-instance v0, Lcom/android/phone/networkmode/NMFilterCmccViceSlot;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NMFilterCmccViceSlot;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/phone/networkmode/NetworkMode;->addFilter(Lcom/android/phone/networkmode/NMFilter;)V

    .line 67
    :cond_0
    invoke-static {}, Lcom/android/phone/NetworkModeManager;->isRemoveNetworkModeSettings()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    new-instance v0, Lcom/android/phone/networkmode/NMFilterCmccPreferred;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NMFilterCmccPreferred;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/phone/networkmode/NetworkMode;->addFilter(Lcom/android/phone/networkmode/NMFilter;)V

    .line 70
    :cond_1
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/telephony/TelephonyManager;->isDisableLte(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    new-instance v0, Lcom/android/phone/networkmode/NMFilterDisableLte;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NMFilterDisableLte;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/phone/networkmode/NetworkMode;->addFilter(Lcom/android/phone/networkmode/NMFilter;)V

    .line 73
    :cond_2
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->isGlobalDeviceSupportWcdmaVice()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    new-instance v0, Lcom/android/phone/networkmode/NMFilterIndiaVicePref;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NMFilterIndiaVicePref;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/phone/networkmode/NetworkMode;->addFilter(Lcom/android/phone/networkmode/NMFilter;)V

    .line 76
    :cond_3
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_4

    .line 77
    new-instance v0, Lcom/android/phone/networkmode/NMFilterLteOnly;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NMFilterLteOnly;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/phone/networkmode/NetworkMode;->addFilter(Lcom/android/phone/networkmode/NMFilter;)V

    .line 79
    :cond_4
    sget-boolean v0, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v0, :cond_5

    .line 80
    new-instance v0, Lcom/android/phone/networkmode/NMFilterDual4GDisabled;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NMFilterDual4GDisabled;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/phone/networkmode/NetworkMode;->addFilter(Lcom/android/phone/networkmode/NMFilter;)V

    .line 82
    :cond_5
    return-void
.end method

.method public static getInstance()Lcom/android/phone/networkmode/NetworkMode;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/android/phone/networkmode/NetworkMode;->sNetworkMode:Lcom/android/phone/networkmode/NetworkMode;

    return-object v0
.end method

.method public static getPhoneTypesAccordingSimType(I)I
    .locals 2
    .param p0, "slotId"    # I

    .prologue
    .line 105
    invoke-static {p0}, Lcom/android/phone/NetworkModeManager;->getPhoneType(I)I

    move-result v0

    .line 106
    .local v0, "ret":I
    if-nez v0, :cond_0

    .line 107
    invoke-static {p0}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v0

    .line 109
    :cond_0
    return v0
.end method

.method public static getPhoneTypesAccordingSimType()[I
    .locals 4

    .prologue
    .line 113
    sget v3, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v2, v3, [I

    .line 114
    .local v2, "ret":[I
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 115
    .local v1, "phones":[Lcom/android/internal/telephony/Phone;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v3, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v3, :cond_1

    .line 116
    invoke-static {v0}, Lcom/android/phone/NetworkModeManager;->getPhoneType(I)I

    move-result v3

    aput v3, v2, v0

    .line 117
    aget v3, v2, v0

    if-nez v3, :cond_0

    .line 118
    aget-object v3, v1, v0

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v3

    aput v3, v2, v0

    .line 115
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_1
    return-object v2
.end method

.method public static getSupportNetworkMode(I)[I
    .locals 7
    .param p0, "type"    # I

    .prologue
    const/4 v6, 0x7

    const/16 v5, 0xa

    const/16 v4, 0x9

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 240
    const/4 v0, 0x0

    .line 241
    .local v0, "ret":[I
    packed-switch p0, :pswitch_data_0

    .line 278
    new-array v0, v3, [I

    .end local v0    # "ret":[I
    aput v3, v0, v2

    .line 281
    .local v0, "ret":[I
    :goto_0
    return-object v0

    .line 243
    .local v0, "ret":[I
    :pswitch_0
    new-array v0, v3, [I

    .end local v0    # "ret":[I
    aput v3, v0, v2

    .line 244
    .local v0, "ret":[I
    goto :goto_0

    .line 246
    .local v0, "ret":[I
    :pswitch_1
    const/4 v1, 0x2

    filled-new-array {v2, v3, v1}, [I

    move-result-object v0

    .local v0, "ret":[I
    goto :goto_0

    .line 249
    .local v0, "ret":[I
    :pswitch_2
    const/4 v1, 0x4

    filled-new-array {v6, v1}, [I

    move-result-object v0

    .local v0, "ret":[I
    goto :goto_0

    .line 252
    .local v0, "ret":[I
    :pswitch_3
    filled-new-array {v5, v6}, [I

    move-result-object v0

    .local v0, "ret":[I
    goto :goto_0

    .line 255
    .local v0, "ret":[I
    :pswitch_4
    filled-new-array {v4, v2, v3}, [I

    move-result-object v0

    .local v0, "ret":[I
    goto :goto_0

    .line 258
    .local v0, "ret":[I
    :pswitch_5
    const/4 v1, 0x3

    filled-new-array {v4, v1, v3}, [I

    move-result-object v0

    .local v0, "ret":[I
    goto :goto_0

    .line 263
    .local v0, "ret":[I
    :pswitch_6
    const/16 v1, 0x11

    const/16 v2, 0x10

    filled-new-array {v1, v2, v3}, [I

    move-result-object v0

    .local v0, "ret":[I
    goto :goto_0

    .line 267
    .local v0, "ret":[I
    :pswitch_7
    sget-boolean v1, Lmiui/telephony/MiuiTelephony;->IS_QCOM:Z

    if-eqz v1, :cond_0

    .line 268
    const/16 v1, 0x14

    const/16 v2, 0x11

    filled-new-array {v1, v2, v5, v4}, [I

    move-result-object v0

    .local v0, "ret":[I
    goto :goto_0

    .line 270
    .local v0, "ret":[I
    :cond_0
    filled-new-array {v5, v4}, [I

    move-result-object v0

    .local v0, "ret":[I
    goto :goto_0

    .line 275
    .local v0, "ret":[I
    :pswitch_8
    const/16 v1, 0x10

    const/16 v2, 0xd

    filled-new-array {v1, v3, v2}, [I

    move-result-object v0

    .local v0, "ret":[I
    goto :goto_0

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_8
        :pswitch_2
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_7
        :pswitch_5
    .end packed-switch
.end method

.method public static hasCardByMcc([I)Z
    .locals 11
    .param p0, "mccArray"    # [I

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 192
    if-eqz p0, :cond_0

    array-length v6, p0

    if-nez v6, :cond_1

    .line 193
    :cond_0
    return v7

    .line 195
    :cond_1
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v5

    .line 196
    .local v5, "sis":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    if-eqz v5, :cond_4

    .line 197
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "si$iterator":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/telephony/SubscriptionInfo;

    .line 198
    .local v3, "si":Lmiui/telephony/SubscriptionInfo;
    array-length v8, p0

    move v6, v7

    :goto_0
    if-ge v6, v8, :cond_2

    aget v2, p0, v6

    .line 199
    .local v2, "mcc":I
    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getMcc()I

    move-result v9

    if-ne v9, v2, :cond_3

    .line 200
    return v10

    .line 198
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 205
    .end local v2    # "mcc":I
    .end local v3    # "si":Lmiui/telephony/SubscriptionInfo;
    .end local v4    # "si$iterator":Ljava/util/Iterator;
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget v6, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    if-ge v0, v6, :cond_7

    .line 206
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v6

    invoke-virtual {v6, v0}, Lmiui/telephony/DefaultSimManager;->getImsi(I)Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, "imsi":Ljava/lang/String;
    if-eqz v1, :cond_6

    .line 208
    array-length v8, p0

    move v6, v7

    :goto_2
    if-ge v6, v8, :cond_6

    aget v2, p0, v6

    .line 209
    .restart local v2    # "mcc":I
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 210
    return v10

    .line 208
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 205
    .end local v2    # "mcc":I
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 215
    .end local v1    # "imsi":Ljava/lang/String;
    :cond_7
    return v7
.end method

.method public static init()V
    .locals 2

    .prologue
    .line 85
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/phone/PhoneGlobals;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 86
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0f0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/phone/networkmode/NetworkMode;->sLockNetworkType:I

    .line 87
    sget-boolean v1, Lmiui/telephony/MiuiTelephony;->IS_QCOM:Z

    if-eqz v1, :cond_0

    .line 88
    new-instance v1, Lcom/android/phone/networkmode/QcNetworkMode;

    invoke-direct {v1}, Lcom/android/phone/networkmode/QcNetworkMode;-><init>()V

    sput-object v1, Lcom/android/phone/networkmode/NetworkMode;->sNetworkMode:Lcom/android/phone/networkmode/NetworkMode;

    .line 94
    :goto_0
    return-void

    .line 89
    :cond_0
    sget-boolean v1, Lmiui/telephony/MiuiTelephony;->IS_MTK:Z

    if-eqz v1, :cond_1

    .line 90
    new-instance v1, Lcom/android/phone/networkmode/MtkNetworkMode;

    invoke-direct {v1}, Lcom/android/phone/networkmode/MtkNetworkMode;-><init>()V

    sput-object v1, Lcom/android/phone/networkmode/NetworkMode;->sNetworkMode:Lcom/android/phone/networkmode/NetworkMode;

    goto :goto_0

    .line 92
    :cond_1
    new-instance v1, Lcom/android/phone/networkmode/NetworkMode;

    invoke-direct {v1}, Lcom/android/phone/networkmode/NetworkMode;-><init>()V

    sput-object v1, Lcom/android/phone/networkmode/NetworkMode;->sNetworkMode:Lcom/android/phone/networkmode/NetworkMode;

    goto :goto_0
.end method

.method public static isDeviceLockViceNetworkModeForCmcc()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 219
    sget v0, Lcom/android/phone/networkmode/NetworkMode;->sLockNetworkType:I

    if-eq v0, v3, :cond_0

    .line 220
    return v2

    .line 222
    :cond_0
    const-string/jumbo v0, "nikel"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    invoke-static {}, Lcom/android/phone/PhoneAdapter;->isLockNetwork()Z

    move-result v0

    return v0

    .line 224
    :cond_1
    const-string/jumbo v0, "rolex"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    const-string/jumbo v0, "persist.radio.miui_network_lock"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    .line 227
    :cond_2
    return v3
.end method

.method public static isDisableLte()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 141
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/telephony/TelephonyManager;->isDisableLte(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/android/phone/networkmode/NetworkMode;->INDONESIA_MCC_ARRAY:[I

    invoke-static {v0}, Lcom/android/phone/networkmode/NetworkMode;->hasCardByMcc([I)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public static isGlobalDeviceSupportWcdmaVice()Z
    .locals 1

    .prologue
    .line 164
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 165
    const/4 v0, 0x0

    return v0

    .line 167
    :cond_0
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->isViceSlotOnlySupport2G()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static isNetworkModeLteOnly(I)Z
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 145
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isIraqFastlinkSim(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isViceSlotOnlySupport2G()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 174
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 175
    return v6

    .line 177
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-ge v1, v3, :cond_1

    .line 178
    return v6

    .line 180
    :cond_1
    sget-object v3, Lcom/android/phone/networkmode/NetworkMode;->VICE_SLOT_2G_DEVICES_GREATER_THAN_M:[Ljava/lang/String;

    array-length v4, v3

    move v1, v2

    .local v0, "device":Ljava/lang/String;
    :goto_0
    if-ge v1, v4, :cond_3

    aget-object v0, v3, v1

    .line 181
    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 182
    return v6

    .line 180
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 185
    :cond_3
    return v2
.end method

.method public static shouldShowViceNetworkTypePref()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 149
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 150
    const/4 v0, 0x0

    .line 152
    .local v0, "viceSlotId":I
    :goto_0
    invoke-static {}, Lcom/android/phone/Dual4GManager;->isDual4GEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 153
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lmiui/telephony/DefaultSimManager;->getSimImsi(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/telephony/ServiceProviderUtils;->isChinaMobile(Ljava/lang/String;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 152
    if-eqz v3, :cond_1

    .line 154
    invoke-static {v0}, Lcom/android/phone/networkmode/NetworkMode;->getPhoneTypesAccordingSimType(I)I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    .line 155
    return v1

    .line 150
    .end local v0    # "viceSlotId":I
    :cond_0
    const/4 v0, 0x1

    .restart local v0    # "viceSlotId":I
    goto :goto_0

    .line 158
    :cond_1
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->isGlobalDeviceSupportWcdmaVice()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 159
    sget-object v3, Lcom/android/phone/networkmode/NetworkMode;->INDIA_MCC_ARRAY:[I

    invoke-static {v3}, Lcom/android/phone/networkmode/NetworkMode;->hasCardByMcc([I)Z

    move-result v3

    .line 158
    if-eqz v3, :cond_3

    .line 160
    invoke-static {v0}, Lcom/android/phone/networkmode/NetworkMode;->getPhoneTypesAccordingSimType(I)I

    move-result v3

    if-ne v3, v1, :cond_2

    .line 158
    :goto_1
    return v1

    :cond_2
    move v1, v2

    .line 160
    goto :goto_1

    :cond_3
    move v1, v2

    .line 158
    goto :goto_1
.end method


# virtual methods
.method public addFilter(Lcom/android/phone/networkmode/NMFilter;)V
    .locals 1
    .param p1, "networkModeFilter"    # Lcom/android/phone/networkmode/NMFilter;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/phone/networkmode/NetworkMode;->mFilters:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 232
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string/jumbo v4, "NetworkMode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 233
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/android/phone/networkmode/NetworkMode;->mFilters:Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "f$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/networkmode/NMFilter;

    .line 234
    .local v0, "f":Lcom/android/phone/networkmode/NMFilter;
    invoke-virtual {v0}, Lcom/android/phone/networkmode/NMFilter;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 236
    .end local v0    # "f":Lcom/android/phone/networkmode/NMFilter;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method public getDeviceSupportModes()[[I
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSupportModes()[[I
    .locals 4

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkMode;->getDeviceSupportModes()[[I

    move-result-object v2

    .line 126
    .local v2, "ret":[[I
    iget-object v3, p0, Lcom/android/phone/networkmode/NetworkMode;->mFilters:Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "f$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/networkmode/NMFilter;

    .line 127
    .local v0, "f":Lcom/android/phone/networkmode/NMFilter;
    invoke-virtual {v0, v2}, Lcom/android/phone/networkmode/NMFilter;->filter([[I)[[I

    move-result-object v2

    goto :goto_0

    .line 129
    .end local v0    # "f":Lcom/android/phone/networkmode/NMFilter;
    :cond_0
    return-object v2
.end method

.method public isShowCdmaUnvailable()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return v0
.end method
