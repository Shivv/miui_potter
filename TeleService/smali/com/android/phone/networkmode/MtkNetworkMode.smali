.class public Lcom/android/phone/networkmode/MtkNetworkMode;
.super Lcom/android/phone/networkmode/NetworkMode;
.source "MtkNetworkMode.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/android/phone/networkmode/NetworkMode;-><init>()V

    return-void
.end method


# virtual methods
.method public getDeviceSupportModes()[[I
    .locals 8

    .prologue
    const v7, 0x7f070088

    const/4 v6, 0x2

    .line 19
    sget v5, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v4, v5, [[I

    .line 20
    .local v4, "supportModes":[[I
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    .line 21
    .local v0, "defaultSlot":I
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/phone/PhoneGlobals;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 22
    .local v3, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/android/phone/networkmode/MtkNetworkMode;->getPhoneTypesAccordingSimType()[I

    move-result-object v2

    .line 23
    .local v2, "phoneTypes":[I
    aget v5, v2, v0

    if-ne v5, v6, :cond_1

    .line 24
    const v5, 0x7f070085

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v5

    aput-object v5, v4, v0

    .line 25
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v2

    if-ge v1, v5, :cond_4

    .line 26
    if-eq v1, v0, :cond_0

    .line 27
    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v5

    aput-object v5, v4, v1

    .line 25
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 32
    .end local v1    # "i":I
    :cond_1
    const v5, 0x7f070087

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v5

    aput-object v5, v4, v0

    .line 33
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    array-length v5, v2

    if-ge v1, v5, :cond_4

    .line 34
    if-eq v1, v0, :cond_2

    .line 35
    aget v5, v2, v1

    if-ne v5, v6, :cond_3

    .line 36
    const v5, 0x7f070086

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v5

    .line 35
    :goto_2
    aput-object v5, v4, v1

    .line 33
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 36
    :cond_3
    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v5

    goto :goto_2

    .line 40
    :cond_4
    return-object v4
.end method
