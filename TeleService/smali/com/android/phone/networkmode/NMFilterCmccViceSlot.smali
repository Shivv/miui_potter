.class public Lcom/android/phone/networkmode/NMFilterCmccViceSlot;
.super Lcom/android/phone/networkmode/NMFilter;
.source "NMFilterCmccViceSlot.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/android/phone/networkmode/NMFilter;-><init>()V

    return-void
.end method


# virtual methods
.method public filter([[I)[[I
    .locals 6
    .param p1, "networkModes"    # [[I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 17
    sget v2, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    invoke-static {}, Lcom/android/phone/DefaultSlotSelectorImpl;->getDataSlotForCmcc()I

    move-result v3

    if-eq v2, v3, :cond_4

    .line 18
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v2, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v2, :cond_4

    .line 19
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v2

    if-ne v0, v2, :cond_1

    .line 18
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22
    :cond_1
    invoke-static {v0}, Lcom/android/phone/NetworkModeManager;->getPhoneType(I)I

    move-result v1

    .line 23
    .local v1, "simType":I
    if-ne v1, v4, :cond_3

    .line 24
    sget-boolean v2, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->sIsNetworkMccAbroad:Z

    if-eqz v2, :cond_2

    aget-object v2, p1, v0

    invoke-static {v2}, Lcom/android/phone/networkmode/NMFilterCmccViceSlot;->removeLte([I)[I

    move-result-object v2

    :goto_2
    aput-object v2, p1, v0

    goto :goto_1

    :cond_2
    new-array v2, v4, [I

    aput v4, v2, v5

    goto :goto_2

    .line 25
    :cond_3
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 26
    new-array v2, v4, [I

    const/4 v3, 0x7

    aput v3, v2, v5

    aput-object v2, p1, v0

    goto :goto_1

    .line 30
    .end local v0    # "i":I
    .end local v1    # "simType":I
    :cond_4
    return-object p1
.end method
