.class public Lcom/android/phone/networkmode/NMFilterIndiaVicePref;
.super Lcom/android/phone/networkmode/NMFilter;
.source "NMFilterIndiaVicePref.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/android/phone/networkmode/NMFilter;-><init>()V

    return-void
.end method


# virtual methods
.method public filter([[I)[[I
    .locals 2
    .param p1, "networkModes"    # [[I

    .prologue
    .line 9
    sget-object v1, Lcom/android/phone/networkmode/NetworkMode;->INDIA_MCC_ARRAY:[I

    invoke-static {v1}, Lcom/android/phone/networkmode/NetworkMode;->hasCardByMcc([I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 10
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 11
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 10
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_1
    invoke-static {}, Lcom/android/phone/Dual4GManager;->isDual4GEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15
    aget-object v1, p1, v0

    invoke-static {v1}, Lcom/android/phone/networkmode/NMFilterIndiaVicePref;->removeLte([I)[I

    move-result-object v1

    aput-object v1, p1, v0

    goto :goto_1

    .line 19
    .end local v0    # "i":I
    :cond_2
    return-object p1
.end method
