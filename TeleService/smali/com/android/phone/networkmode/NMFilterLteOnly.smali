.class public Lcom/android/phone/networkmode/NMFilterLteOnly;
.super Lcom/android/phone/networkmode/NMFilter;
.source "NMFilterLteOnly.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/android/phone/networkmode/NMFilter;-><init>()V

    return-void
.end method


# virtual methods
.method public filter([[I)[[I
    .locals 5
    .param p1, "networkModes"    # [[I

    .prologue
    .line 12
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    .line 13
    .local v0, "dds":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v2, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v1, v2, :cond_1

    .line 14
    if-ne v1, v0, :cond_0

    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->isIraqFastlinkSim(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 15
    const/4 v2, 0x1

    new-array v2, v2, [I

    const/16 v3, 0xb

    const/4 v4, 0x0

    aput v3, v2, v4

    aput-object v2, p1, v1

    .line 16
    return-object p1

    .line 13
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 19
    :cond_1
    return-object p1
.end method
