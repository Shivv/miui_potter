.class public Lcom/android/phone/networkmode/NetworkModeHandlerQc;
.super Lcom/android/phone/networkmode/NetworkModeHandlerBase;
.source "NetworkModeHandlerQc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;-><init>()V

    .line 57
    return-void
.end method


# virtual methods
.method protected handleSetNetworkTypeDone(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 60
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 61
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    check-cast v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;

    .line 62
    .local v2, "settingArgs":Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v3, :cond_0

    .line 64
    iget-object v3, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    const/4 v4, 0x2

    invoke-virtual {p0, v4, v3}, Lcom/android/phone/networkmode/NetworkModeHandlerQc;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 65
    .local v1, "m":Landroid/os/Message;
    const-wide/16 v4, 0x7d0

    invoke-virtual {p0, v1, v4, v5}, Lcom/android/phone/networkmode/NetworkModeHandlerQc;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 75
    :goto_0
    return-void

    .line 67
    .end local v1    # "m":Landroid/os/Message;
    :cond_0
    iget v3, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->index:I

    iget v4, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->dataSlot:I

    sget v5, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    add-int/2addr v4, v5

    if-ne v3, v4, :cond_1

    .line 68
    return-void

    .line 70
    :cond_1
    iget v3, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->index:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->index:I

    .line 71
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setPreferredNetworkType "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 72
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v2}, Lcom/android/phone/networkmode/NetworkModeHandlerQc;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 73
    .restart local v1    # "m":Landroid/os/Message;
    invoke-virtual {v2}, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->getSlot()I

    move-result v3

    invoke-static {v3}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    iget-object v4, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->modes:[I

    invoke-virtual {v2}, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->getSlot()I

    move-result v5

    aget v4, v4, v5

    invoke-virtual {v3, v4, v1}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    goto :goto_0
.end method

.method protected handleSetNetworkTypeRetry(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 78
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;

    .line 79
    .local v2, "settingArgs":Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "handleSetNetworkTypeRetry "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 80
    iget-object v3, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->times:[I

    invoke-virtual {v2}, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->getSlot()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x3

    if-ge v3, v4, :cond_0

    .line 81
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v3

    iget v4, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->dataSlot:I

    if-eq v3, v4, :cond_1

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerQc;->getPreferredNetworkTypes()V

    .line 83
    return-void

    .line 87
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->modes:[I

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 88
    iget-object v3, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->modes:[I

    aget v3, v3, v0

    invoke-static {v0, v3}, Lmiui/telephony/DefaultSimManager;->setNetworkModeInDb(II)V

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_2
    iget-object v3, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->times:[I

    invoke-virtual {v2}, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->getSlot()I

    move-result v4

    aget v5, v3, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v3, v4

    .line 91
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v2}, Lcom/android/phone/networkmode/NetworkModeHandlerQc;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 92
    .local v1, "m":Landroid/os/Message;
    invoke-virtual {v2}, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->getSlot()I

    move-result v3

    invoke-static {v3}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    iget-object v4, v2, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->modes:[I

    invoke-virtual {v2}, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->getSlot()I

    move-result v5

    aget v4, v4, v5

    invoke-virtual {v3, v4, v1}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    .line 93
    return-void
.end method

.method protected updateNetworkTypeIfNeeded()V
    .locals 2

    return-void
.end method
