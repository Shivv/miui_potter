.class public abstract Lcom/android/phone/networkmode/NMFilter;
.super Ljava/lang/Object;
.source "NMFilter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static removeLte([I)[I
    .locals 9
    .param p0, "modesToBeRemoved"    # [I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 9
    const/4 v5, 0x7

    invoke-static {v5}, Lcom/android/phone/networkmode/NetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v3

    .line 10
    .local v3, "lteModes":[I
    const/4 v0, 0x0

    .line 11
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p0

    if-ge v1, v5, :cond_1

    .line 12
    aget v5, p0, v1

    invoke-static {v3, v5}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v5

    if-nez v5, :cond_0

    .line 13
    add-int/lit8 v0, v0, 0x1

    .line 11
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 16
    :cond_1
    if-lez v0, :cond_4

    .line 17
    new-array v4, v0, [I

    .line 18
    .local v4, "withoutLteModes":[I
    const/4 v1, 0x0

    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v5, p0

    if-ge v1, v5, :cond_3

    .line 19
    aget v5, p0, v1

    invoke-static {v3, v5}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v5

    if-nez v5, :cond_2

    .line 20
    aget v5, p0, v1

    aput v5, v4, v2

    .line 21
    add-int/lit8 v2, v2, 0x1

    .line 18
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 24
    :cond_3
    return-object v4

    .line 26
    .end local v2    # "j":I
    .end local v4    # "withoutLteModes":[I
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "removeLte error args="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 28
    new-array v5, v8, [I

    aput v8, v5, v7

    return-object v5
.end method


# virtual methods
.method public abstract filter([[I)[[I
.end method
