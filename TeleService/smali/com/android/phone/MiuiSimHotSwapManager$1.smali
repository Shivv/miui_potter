.class Lcom/android/phone/MiuiSimHotSwapManager$1;
.super Ljava/lang/Object;
.source "MiuiSimHotSwapManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/MiuiSimHotSwapManager;->promptForRestart(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/MiuiSimHotSwapManager;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/phone/MiuiSimHotSwapManager;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/MiuiSimHotSwapManager;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/MiuiSimHotSwapManager$1;->this$0:Lcom/android/phone/MiuiSimHotSwapManager;

    iput-object p2, p0, Lcom/android/phone/MiuiSimHotSwapManager$1;->val$context:Landroid/content/Context;

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 123
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 124
    invoke-static {}, Lcom/android/phone/MiuiSimHotSwapManager;->-get0()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Reboot due to SIM swap"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v1, p0, Lcom/android/phone/MiuiSimHotSwapManager$1;->val$context:Landroid/content/Context;

    const-string/jumbo v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 126
    .local v0, "pm":Landroid/os/PowerManager;
    const-string/jumbo v1, "SIM is added."

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 130
    .end local v0    # "pm":Landroid/os/PowerManager;
    :goto_0
    return-void

    .line 128
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0
.end method
