.class final Lcom/android/phone/MiuiLog$2;
.super Landroid/content/BroadcastReceiver;
.source "MiuiLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/MiuiLog;->register()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 47
    const-string/jumbo v3, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 48
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "host":Ljava/lang/String;
    const-string/jumbo v3, "74663"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 50
    sget-boolean v3, Lmiui/telephony/PhoneDebug;->VDBG:Z

    if-eqz v3, :cond_1

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "phone_debug_flag"

    invoke-static {}, Lcom/android/phone/MiuiLog;->-get0()I

    move-result v5

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 52
    const-string/jumbo v3, "PhoneGlobals"

    const-string/jumbo v4, "Verbose Debug for telephony is closed"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    const-string/jumbo v3, "Debug for telephony is closed."

    invoke-static {p1, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 77
    .end local v0    # "host":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 55
    .restart local v0    # "host":Ljava/lang/String;
    :cond_1
    const-string/jumbo v3, "PhoneGlobals"

    const-string/jumbo v4, "Verbose Debug for telephony is opened"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const-string/jumbo v3, "Debug for telephony is opened."

    invoke-static {p1, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "phone_debug_flag"

    invoke-static {}, Lcom/android/phone/MiuiLog;->-get1()I

    move-result v5

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 59
    :cond_2
    const-string/jumbo v3, "86583"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 60
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-ge v3, v4, :cond_3

    return-void

    .line 61
    :cond_3
    invoke-static {p1}, Lcom/android/phone/CarrierConfigLoaderInjector;->isVolteForceEnabled(Landroid/content/Context;)Z

    move-result v2

    .line 62
    .local v2, "oldStatus":Z
    xor-int/lit8 v3, v2, 0x1

    invoke-static {p1, v3}, Lcom/android/phone/CarrierConfigLoaderInjector;->setVolteForceEnabled(Landroid/content/Context;Z)V

    .line 63
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget v3, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v1, v3, :cond_5

    .line 64
    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->getIccRecordsLoaded(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 65
    const-string/jumbo v3, "LOADED"

    invoke-static {p1, v1, v3}, Lcom/android/phone/PhoneProxy;->updateConfigForPhoneId(Landroid/content/Context;ILjava/lang/String;)V

    .line 63
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 68
    :cond_5
    if-nez v2, :cond_6

    .line 69
    const-string/jumbo v3, "PhoneGlobals"

    const-string/jumbo v4, "Volte carrier check was disabled."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    const-string/jumbo v3, "Volte carrier check was disabled."

    invoke-static {p1, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 72
    :cond_6
    const-string/jumbo v3, "PhoneGlobals"

    const-string/jumbo v4, "Volte carrier check was enabled."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const-string/jumbo v3, "Volte carrier check was enabled."

    invoke-static {p1, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
