.class public Lcom/android/phone/MiuiSimHotSwapManager;
.super Landroid/os/Handler;
.source "MiuiSimHotSwapManager.java"


# static fields
.field private static LOG_TAG:Ljava/lang/String;

.field private static sInstance:Lcom/android/phone/MiuiSimHotSwapManager;


# instance fields
.field private mHasPromptForRestart:Z

.field private mIsHotSwap:Z

.field private mLastCardState:[Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

.field private mRestartDialog:Lmiui/app/AlertDialog;

.field private mUiccController:Lcom/android/internal/telephony/uicc/UiccController;


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/phone/MiuiSimHotSwapManager;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string/jumbo v0, "MiuiSimHotSwapManager"

    sput-object v0, Lcom/android/phone/MiuiSimHotSwapManager;->LOG_TAG:Ljava/lang/String;

    .line 28
    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 142
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 39
    iput-object v4, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    .line 40
    iput-object v4, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mRestartDialog:Lmiui/app/AlertDialog;

    .line 143
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v2

    array-length v1, v2

    .line 144
    .local v1, "phoneCount":I
    new-array v2, v1, [Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    iput-object v2, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mLastCardState:[Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    .line 145
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 146
    iget-object v2, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mLastCardState:[Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    aput-object v4, v2, v0

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    .line 149
    iget-object v2, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    const/4 v3, 0x0

    invoke-virtual {v2, p0, v3, v4}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 150
    return-void
.end method

.method private getIccRecordsForPhoneId(II)Lcom/android/internal/telephony/uicc/IccRecords;
    .locals 3
    .param p1, "phoneId"    # I
    .param p2, "applicationIndex"    # I

    .prologue
    const/4 v2, 0x0

    .line 112
    iget-object v1, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCard(I)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v0

    .line 113
    .local v0, "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {v0, p2}, Lcom/android/internal/telephony/uicc/UiccCard;->getApplicationIndex(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    move-result-object v1

    return-object v1

    .line 116
    :cond_0
    return-object v2
.end method

.method private handleImsiReady(II)V
    .locals 6
    .param p1, "phoneId"    # I
    .param p2, "applicationIndex"    # I

    .prologue
    .line 100
    sget-object v3, Lcom/android/phone/MiuiSimHotSwapManager;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "received EVENT_IMSI_READY: phoneid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "applicationIndex ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 101
    const-string/jumbo v5, " ,mIsHotSwap"

    .line 100
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 101
    iget-boolean v5, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mIsHotSwap:Z

    .line 100
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 101
    const-string/jumbo v5, ",mHasPromptForRestart ="

    .line 100
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 101
    iget-boolean v5, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mHasPromptForRestart:Z

    .line 100
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-static {p1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 103
    .local v2, "phone":Lcom/android/internal/telephony/Phone;
    invoke-direct {p0, p1, p2}, Lcom/android/phone/MiuiSimHotSwapManager;->getIccRecordsForPhoneId(II)Lcom/android/internal/telephony/uicc/IccRecords;

    move-result-object v0

    .line 104
    .local v0, "iccRecords":Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/internal/telephony/uicc/IccRecords;->getIMSI()Ljava/lang/String;

    move-result-object v1

    .line 105
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "450"

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mIsHotSwap:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mHasPromptForRestart:Z

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 106
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/phone/MiuiSimHotSwapManager;->promptForRestart(Landroid/content/Context;)V

    .line 107
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mHasPromptForRestart:Z

    .line 109
    :cond_0
    return-void

    .line 104
    :cond_1
    const/4 v1, 0x0

    .local v1, "imsi":Ljava/lang/String;
    goto :goto_0
.end method

.method public static init()V
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/android/phone/MiuiSimHotSwapManager;->sInstance:Lcom/android/phone/MiuiSimHotSwapManager;

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Lcom/android/phone/MiuiSimHotSwapManager;

    invoke-direct {v0}, Lcom/android/phone/MiuiSimHotSwapManager;-><init>()V

    sput-object v0, Lcom/android/phone/MiuiSimHotSwapManager;->sInstance:Lcom/android/phone/MiuiSimHotSwapManager;

    .line 156
    :cond_0
    return-void
.end method

.method private promptForRestart(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 120
    new-instance v0, Lcom/android/phone/MiuiSimHotSwapManager$1;

    invoke-direct {v0, p0, p1}, Lcom/android/phone/MiuiSimHotSwapManager$1;-><init>(Lcom/android/phone/MiuiSimHotSwapManager;Landroid/content/Context;)V

    .line 132
    .local v0, "listener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v1, Lmiui/app/AlertDialog$Builder;

    const/4 v2, 0x3

    invoke-direct {v1, p1, v2}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 133
    const v2, 0x10405d7

    .line 132
    invoke-virtual {v1, v2}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    .line 134
    const v2, 0x10405d6

    .line 132
    invoke-virtual {v1, v2}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    .line 135
    const v2, 0x10405db

    .line 132
    invoke-virtual {v1, v2, v0}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    .line 136
    const/high16 v2, 0x1040000

    .line 132
    invoke-virtual {v1, v2, v0}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mRestartDialog:Lmiui/app/AlertDialog;

    .line 138
    iget-object v1, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mRestartDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v1}, Lmiui/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d3

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 139
    iget-object v1, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mRestartDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v1}, Lmiui/app/AlertDialog;->show()V

    .line 140
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 44
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    .line 45
    .local v1, "ar":Landroid/os/AsyncResult;
    const/4 v5, 0x0

    .line 46
    .local v5, "phoneId":I
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    .line 97
    :goto_0
    return-void

    .line 48
    :pswitch_0
    iget-object v7, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v7, :cond_0

    .line 49
    iget-object v7, v1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 54
    sget-object v7, Lcom/android/phone/MiuiSimHotSwapManager;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "received EVENT_ICC_CHANGED,phoneId = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget-object v7, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    invoke-virtual {v7, v5}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCard(I)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v2

    .line 56
    .local v2, "card":Lcom/android/internal/telephony/uicc/UiccCard;
    if-nez v2, :cond_1

    .line 57
    sget-object v7, Lcom/android/phone/MiuiSimHotSwapManager;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v8, "Error: UiccCard is null ,when receivingEVENT_ICC_CHANGED "

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    return-void

    .line 51
    .end local v2    # "card":Lcom/android/internal/telephony/uicc/UiccCard;
    :cond_0
    sget-object v7, Lcom/android/phone/MiuiSimHotSwapManager;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v8, "Error: Invalid card index when receiving EVENT_ICC_CHANGED "

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    return-void

    .line 60
    .restart local v2    # "card":Lcom/android/internal/telephony/uicc/UiccCard;
    :cond_1
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    move-result-object v6

    .line 62
    .local v6, "state":Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;
    sget-object v7, Lcom/android/phone/MiuiSimHotSwapManager;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "mLastCardState: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mLastCardState:[Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    aget-object v9, v9, v5

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " state: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 63
    const-string/jumbo v9, ",mIsHotSwap ="

    .line 62
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 63
    iget-boolean v9, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mIsHotSwap:Z

    .line 62
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v7, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mLastCardState:[Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    aget-object v7, v7, v5

    sget-object v8, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    if-ne v7, v8, :cond_5

    .line 65
    sget-object v7, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    if-eq v6, v7, :cond_5

    .line 66
    iget-object v7, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mUiccController:Lcom/android/internal/telephony/uicc/UiccController;

    invoke-virtual {v7, v5}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCard(I)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/uicc/UiccCard;->getNumApplications()I

    move-result v0

    .line 67
    .local v0, "applicationNum":I
    const/4 v4, 0x0

    .line 68
    .local v4, "iccRecords":Lcom/android/internal/telephony/uicc/IccRecords;
    const/4 v3, 0x0

    .end local v4    # "iccRecords":Lcom/android/internal/telephony/uicc/IccRecords;
    .local v3, "i":I
    :goto_1
    if-ge v3, v0, :cond_3

    .line 69
    invoke-direct {p0, v5, v3}, Lcom/android/phone/MiuiSimHotSwapManager;->getIccRecordsForPhoneId(II)Lcom/android/internal/telephony/uicc/IccRecords;

    move-result-object v4

    .line 70
    .local v4, "iccRecords":Lcom/android/internal/telephony/uicc/IccRecords;
    if-eqz v4, :cond_2

    .line 71
    add-int/lit8 v7, v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, p0, v7, v8}, Lcom/android/internal/telephony/uicc/IccRecords;->registerForImsiReady(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 68
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 74
    .end local v4    # "iccRecords":Lcom/android/internal/telephony/uicc/IccRecords;
    :cond_3
    iput-boolean v11, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mIsHotSwap:Z

    .line 83
    .end local v0    # "applicationNum":I
    .end local v3    # "i":I
    :cond_4
    :goto_2
    iget-object v7, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mLastCardState:[Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    aput-object v6, v7, v5

    goto/16 :goto_0

    .line 75
    :cond_5
    iget-object v7, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mLastCardState:[Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    aget-object v7, v7, v5

    sget-object v8, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    if-eq v7, v8, :cond_4

    .line 76
    sget-object v7, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_ABSENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    if-ne v6, v7, :cond_4

    .line 77
    iput-boolean v10, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mHasPromptForRestart:Z

    .line 78
    iput-boolean v10, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mIsHotSwap:Z

    .line 79
    iget-object v7, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mRestartDialog:Lmiui/app/AlertDialog;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mRestartDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v7}, Lmiui/app/AlertDialog;->isShowing()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 80
    iget-object v7, p0, Lcom/android/phone/MiuiSimHotSwapManager;->mRestartDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v7}, Lmiui/app/AlertDialog;->dismiss()V

    goto :goto_2

    .line 87
    .end local v2    # "card":Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v6    # "state":Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;
    :pswitch_1
    iget-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v7, :cond_6

    .line 88
    sget-object v7, Lcom/android/phone/MiuiSimHotSwapManager;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v8, "Error: Invalid phoneid EVENT_IMSI_READY "

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return-void

    .line 91
    :cond_6
    iget-object v7, v1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 92
    iget v7, p1, Landroid/os/Message;->what:I

    add-int/lit8 v7, v7, -0x1

    invoke-direct {p0, v5, v7}, Lcom/android/phone/MiuiSimHotSwapManager;->handleImsiReady(II)V

    goto/16 :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
