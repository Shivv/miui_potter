.class public Lcom/android/phone/GsmUmtsCallForwardOptions;
.super Lcom/android/phone/TimeConsumingPreferenceActivity;
.source "GsmUmtsCallForwardOptions.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final NUM_PROJECTION:[Ljava/lang/String;


# instance fields
.field builder:Landroid/app/AlertDialog$Builder;

.field private mButtonCFB:Lcom/android/phone/CallForwardEditPreference;

.field private mButtonCFNRc:Lcom/android/phone/CallForwardEditPreference;

.field private mButtonCFNRy:Lcom/android/phone/CallForwardEditPreference;

.field private mButtonCFU:Lcom/android/phone/CallForwardEditPreference;

.field private mCarrierMode:Ljava/lang/String;

.field private mFirstResume:Z

.field private mIcicle:Landroid/os/Bundle;

.field private mInitIndex:I

.field private mIsCMCC:Z

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private final mPreferences:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/phone/CallForwardEditPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mReplaceInvalidCFNumbers:Z

.field private mServiceClass:I

.field private mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

.field private mSubscriptionManager:Landroid/telephony/SubscriptionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 38
    const-string/jumbo v1, "data1"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 37
    sput-object v0, Lcom/android/phone/GsmUmtsCallForwardOptions;->NUM_PROJECTION:[Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Lcom/android/phone/TimeConsumingPreferenceActivity;-><init>()V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mInitIndex:I

    .line 64
    iput-object v2, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 68
    const-string/jumbo v0, "persist.radio.carrier_mode"

    const-string/jumbo v1, "default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mCarrierMode:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mCarrierMode:Ljava/lang/String;

    const-string/jumbo v1, "cmcc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mIsCMCC:Z

    .line 70
    iput-object v2, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->builder:Landroid/app/AlertDialog$Builder;

    .line 33
    return-void
.end method

.method private getActiveNetworkType()I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 216
    const-string/jumbo v2, "connectivity"

    .line 215
    invoke-virtual {p0, v2}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 217
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_2

    .line 218
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 219
    .local v1, "ni":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 220
    :cond_0
    return v3

    .line 222
    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    return v2

    .line 224
    .end local v1    # "ni":Landroid/net/NetworkInfo;
    :cond_2
    return v3
.end method

.method private initCallforwarding()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v2, 0x0

    .line 244
    iget-boolean v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mFirstResume:Z

    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mIcicle:Landroid/os/Bundle;

    if-nez v0, :cond_2

    .line 246
    const-string/jumbo v0, "GsmUmtsCallForwardOptions"

    const-string/jumbo v1, "start to init "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mInitIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/CallForwardEditPreference;

    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-boolean v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mReplaceInvalidCFNumbers:Z

    .line 248
    iget v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mServiceClass:I

    move-object v1, p0

    .line 247
    invoke-virtual/range {v0 .. v5}, Lcom/android/phone/CallForwardEditPreference;->init(Lcom/android/phone/TimeConsumingPreferenceListener;ZLcom/android/internal/telephony/Phone;ZI)V

    .line 262
    :cond_0
    iput-boolean v2, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mFirstResume:Z

    .line 263
    iput-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mIcicle:Landroid/os/Bundle;

    .line 265
    :cond_1
    return-void

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mInitIndex:I

    .line 252
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "pref$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/phone/CallForwardEditPreference;

    .line 253
    .local v3, "pref":Lcom/android/phone/CallForwardEditPreference;
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mIcicle:Landroid/os/Bundle;

    invoke-virtual {v3}, Lcom/android/phone/CallForwardEditPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    .line 254
    .local v9, "bundle":Landroid/os/Bundle;
    const-string/jumbo v0, "toggle"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v3, v0}, Lcom/android/phone/CallForwardEditPreference;->setToggled(Z)Lcom/android/phone/EditPhoneNumberPreference;

    .line 255
    new-instance v10, Lcom/android/internal/telephony/CallForwardInfo;

    invoke-direct {v10}, Lcom/android/internal/telephony/CallForwardInfo;-><init>()V

    .line 256
    .local v10, "cf":Lcom/android/internal/telephony/CallForwardInfo;
    const-string/jumbo v0, "number"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    .line 257
    const-string/jumbo v0, "status"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v10, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    .line 258
    invoke-virtual {v3, v10}, Lcom/android/phone/CallForwardEditPreference;->handleCallForwardResult(Lcom/android/internal/telephony/CallForwardInfo;)V

    .line 259
    iget-object v6, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-boolean v7, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mReplaceInvalidCFNumbers:Z

    iget v8, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mServiceClass:I

    const/4 v5, 0x1

    move-object v4, p0

    invoke-virtual/range {v3 .. v8}, Lcom/android/phone/CallForwardEditPreference;->init(Lcom/android/phone/TimeConsumingPreferenceListener;ZLcom/android/internal/telephony/Phone;ZI)V

    goto :goto_0
.end method

.method private showAlertDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 388
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 391
    const v2, 0x1010355

    .line 388
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 392
    const v2, 0x104000a

    .line 388
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 393
    const/high16 v2, 0x1040000

    .line 388
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 396
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 397
    return-void
.end method

.method private showDataInuseToast()V
    .locals 3

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 269
    const v2, 0x7f0b02e7

    .line 268
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 270
    .local v0, "message":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 271
    return-void
.end method

.method private showSwitchDdsDialog(I)V
    .locals 6
    .param p1, "slotId"    # I

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b02e3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 275
    .local v2, "title":Ljava/lang/String;
    add-int/lit8 v1, p1, 0x1

    .line 276
    .local v1, "simId":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 277
    const v5, 0x7f0b02e6

    .line 276
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 277
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 276
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "message":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->builder:Landroid/app/AlertDialog$Builder;

    if-nez v3, :cond_0

    .line 279
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->builder:Landroid/app/AlertDialog$Builder;

    .line 280
    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 281
    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 282
    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->builder:Landroid/app/AlertDialog$Builder;

    const v4, 0x1010355

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    .line 283
    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->builder:Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/android/phone/GsmUmtsCallForwardOptions$1;

    invoke-direct {v4, p0}, Lcom/android/phone/GsmUmtsCallForwardOptions$1;-><init>(Lcom/android/phone/GsmUmtsCallForwardOptions;)V

    const v5, 0x104000a

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 291
    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->builder:Landroid/app/AlertDialog$Builder;

    .line 292
    new-instance v4, Lcom/android/phone/GsmUmtsCallForwardOptions$2;

    invoke-direct {v4, p0}, Lcom/android/phone/GsmUmtsCallForwardOptions$2;-><init>(Lcom/android/phone/GsmUmtsCallForwardOptions;)V

    .line 291
    const/high16 v5, 0x1040000

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 299
    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 301
    :cond_0
    return-void
.end method


# virtual methods
.method public checkDataStatus()V
    .locals 15

    .prologue
    .line 147
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v12}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v10

    .line 148
    .local v10, "sub":I
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    invoke-static {v10}, Landroid/telephony/SubscriptionManager;->getSlotIndex(I)I

    move-result v9

    .line 149
    .local v9, "slotId":I
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultDataSubscriptionId()I

    move-result v3

    .line 150
    .local v3, "defaultDataSub":I
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v12}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v12

    .line 151
    const-string/jumbo v13, "carrier_config"

    .line 150
    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/CarrierConfigManager;

    .line 152
    .local v2, "configManager":Landroid/telephony/CarrierConfigManager;
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v12}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v12

    invoke-virtual {v2, v12}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v7

    .line 153
    .local v7, "pb":Landroid/os/PersistableBundle;
    const-string/jumbo v12, "check_mobile_data_for_cf"

    invoke-virtual {v7, v12}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 154
    .local v1, "checkData":Z
    const-string/jumbo v12, "GsmUmtsCallForwardOptions"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "isUtEnabled = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v14}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ", checkData= "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v12, :cond_6

    .line 156
    invoke-direct {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getActiveNetworkType()I

    move-result v0

    .line 157
    .local v0, "activeNetworkType":I
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v12}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v12

    invoke-virtual {v12}, Landroid/telephony/ServiceState;->getDataRoaming()Z

    move-result v4

    .line 158
    .local v4, "isDataRoaming":Z
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v12}, Lcom/android/internal/telephony/Phone;->getDataRoamingEnabled()Z

    move-result v5

    .line 159
    .local v5, "isDataRoamingEnabled":Z
    if-eqz v4, :cond_0

    xor-int/lit8 v8, v5, 0x1

    .line 160
    :goto_0
    const-string/jumbo v12, "GsmUmtsCallForwardOptions"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "activeNetworkType = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-direct {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getActiveNetworkType()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ", sub = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 161
    const-string/jumbo v14, ", defaultDataSub = "

    .line 160
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 161
    const-string/jumbo v14, ", isDataRoaming = "

    .line 160
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 162
    const-string/jumbo v14, ", isDataRoamingEnabled= "

    .line 160
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    if-eq v10, v3, :cond_2

    .line 164
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v12}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 165
    const-string/jumbo v12, "GsmUmtsCallForwardOptions"

    const-string/jumbo v13, "Show data in use indication if data sub is not on current sub"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-direct {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->showDataInuseToast()V

    .line 167
    invoke-direct {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->initCallforwarding()V

    .line 168
    return-void

    .line 159
    :cond_0
    const/4 v8, 0x0

    .local v8, "promptForDataRoaming":Z
    goto :goto_0

    .line 170
    .end local v8    # "promptForDataRoaming":Z
    :cond_1
    const-string/jumbo v12, "GsmUmtsCallForwardOptions"

    const-string/jumbo v13, "Show dds switch dialog if data sub is not on current sub"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-direct {p0, v9}, Lcom/android/phone/GsmUmtsCallForwardOptions;->showSwitchDdsDialog(I)V

    .line 172
    return-void

    .line 176
    :cond_2
    iget-object v12, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v12}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v12

    if-eqz v12, :cond_6

    if-eqz v1, :cond_6

    .line 177
    if-nez v0, :cond_3

    .line 178
    if-eq v10, v3, :cond_5

    .line 179
    :cond_3
    const/4 v12, -0x1

    if-ne v0, v12, :cond_4

    move v12, v8

    :goto_1
    xor-int/lit8 v12, v12, 0x1

    .line 177
    if-eqz v12, :cond_5

    .line 181
    const-string/jumbo v12, "GsmUmtsCallForwardOptions"

    .line 182
    const-string/jumbo v13, "Show alert dialog if data sub in not on current sub or WLAN is on"

    .line 181
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0b02e3

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 184
    .local v11, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 185
    const v13, 0x7f0b02e1

    .line 184
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 186
    .local v6, "message":Ljava/lang/String;
    invoke-direct {p0, v11, v6}, Lcom/android/phone/GsmUmtsCallForwardOptions;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    return-void

    .line 179
    .end local v6    # "message":Ljava/lang/String;
    .end local v11    # "title":Ljava/lang/String;
    :cond_4
    const/4 v12, 0x0

    goto :goto_1

    .line 189
    :cond_5
    if-eqz v8, :cond_6

    .line 190
    const-string/jumbo v12, "GsmUmtsCallForwardOptions"

    const-string/jumbo v13, "Show alert dialog if data roaming is disabled"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 192
    const v13, 0x7f0b02e5

    .line 191
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 193
    .restart local v11    # "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 194
    const v13, 0x7f0b02e2

    .line 193
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 195
    .restart local v6    # "message":Ljava/lang/String;
    invoke-direct {p0, v11, v6}, Lcom/android/phone/GsmUmtsCallForwardOptions;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    return-void

    .line 200
    .end local v0    # "activeNetworkType":I
    .end local v4    # "isDataRoaming":Z
    .end local v5    # "isDataRoamingEnabled":Z
    .end local v6    # "message":Ljava/lang/String;
    .end local v11    # "title":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->initCallforwarding()V

    .line 201
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 340
    const-string/jumbo v0, "GsmUmtsCallForwardOptions"

    const-string/jumbo v1, "onActivityResult: done"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 342
    const-string/jumbo v0, "GsmUmtsCallForwardOptions"

    const-string/jumbo v1, "onActivityResult: contact picker result not OK."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    return-void

    .line 345
    :cond_0
    const/4 v6, 0x0

    .line 347
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 348
    sget-object v2, Lcom/android/phone/GsmUmtsCallForwardOptions;->NUM_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 347
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 349
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 350
    :cond_1
    const-string/jumbo v0, "GsmUmtsCallForwardOptions"

    const-string/jumbo v1, "onActivityResult: bad contact data, no results found."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    if-eqz v6, :cond_2

    .line 372
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 351
    :cond_2
    return-void

    .line 354
    :cond_3
    packed-switch p1, :pswitch_data_0

    .line 371
    :goto_0
    if-eqz v6, :cond_4

    .line 372
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 375
    :cond_4
    return-void

    .line 356
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/CallForwardEditPreference;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/CallForwardEditPreference;->onPickActivityResult(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 370
    .end local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    .line 371
    if-eqz v6, :cond_5

    .line 372
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 370
    :cond_5
    throw v0

    .line 359
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/CallForwardEditPreference;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/CallForwardEditPreference;->onPickActivityResult(Ljava/lang/String;)V

    goto :goto_0

    .line 362
    :pswitch_2
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/CallForwardEditPreference;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/CallForwardEditPreference;->onPickActivityResult(Ljava/lang/String;)V

    goto :goto_0

    .line 365
    :pswitch_3
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/CallForwardEditPreference;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/CallForwardEditPreference;->onPickActivityResult(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 354
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 205
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 206
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 207
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 208
    invoke-virtual {p0, v0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->startActivity(Landroid/content/Intent;)V

    .line 210
    .end local v0    # "newIntent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->finish()V

    .line 211
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    .line 75
    invoke-super {p0, p1}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v4, 0x7f06000a

    invoke-virtual {p0, v4}, Lcom/android/phone/GsmUmtsCallForwardOptions;->addPreferencesFromResource(I)V

    .line 79
    new-instance v4, Lcom/android/phone/SubscriptionInfoHelper;

    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/android/phone/SubscriptionInfoHelper;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    .line 80
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    .line 81
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0336

    .line 80
    invoke-virtual {v4, v5, v6, v7}, Lcom/android/phone/SubscriptionInfoHelper;->setActionBarTitle(Landroid/app/ActionBar;Landroid/content/res/Resources;I)V

    .line 82
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    invoke-virtual {v4}, Lcom/android/phone/SubscriptionInfoHelper;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iput-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 85
    const-string/jumbo v4, "carrier_config"

    invoke-virtual {p0, v4}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 84
    check-cast v1, Landroid/telephony/CarrierConfigManager;

    .line 86
    .local v1, "carrierConfig":Landroid/telephony/CarrierConfigManager;
    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {v1}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v4

    .line 88
    const-string/jumbo v5, "call_forwarding_map_non_number_to_voicemail_bool"

    .line 87
    invoke-virtual {v4, v5}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mReplaceInvalidCFNumbers:Z

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 92
    .local v3, "prefSet":Landroid/preference/PreferenceScreen;
    const-string/jumbo v4, "button_cfu_key"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/android/phone/CallForwardEditPreference;

    iput-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/CallForwardEditPreference;

    .line 93
    const-string/jumbo v4, "button_cfb_key"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/android/phone/CallForwardEditPreference;

    iput-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/CallForwardEditPreference;

    .line 94
    const-string/jumbo v4, "button_cfnry_key"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/android/phone/CallForwardEditPreference;

    iput-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/CallForwardEditPreference;

    .line 95
    const-string/jumbo v4, "button_cfnrc_key"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/android/phone/CallForwardEditPreference;

    iput-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/CallForwardEditPreference;

    .line 97
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/CallForwardEditPreference;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/CallForwardEditPreference;

    iget v5, v5, Lcom/android/phone/CallForwardEditPreference;->reason:I

    invoke-virtual {v4, p0, v5}, Lcom/android/phone/CallForwardEditPreference;->setParentActivity(Landroid/app/Activity;I)V

    .line 98
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/CallForwardEditPreference;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/CallForwardEditPreference;

    iget v5, v5, Lcom/android/phone/CallForwardEditPreference;->reason:I

    invoke-virtual {v4, p0, v5}, Lcom/android/phone/CallForwardEditPreference;->setParentActivity(Landroid/app/Activity;I)V

    .line 99
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/CallForwardEditPreference;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/CallForwardEditPreference;

    iget v5, v5, Lcom/android/phone/CallForwardEditPreference;->reason:I

    invoke-virtual {v4, p0, v5}, Lcom/android/phone/CallForwardEditPreference;->setParentActivity(Landroid/app/Activity;I)V

    .line 100
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/CallForwardEditPreference;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/CallForwardEditPreference;

    iget v5, v5, Lcom/android/phone/CallForwardEditPreference;->reason:I

    invoke-virtual {v4, p0, v5}, Lcom/android/phone/CallForwardEditPreference;->setParentActivity(Landroid/app/Activity;I)V

    .line 102
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/CallForwardEditPreference;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/CallForwardEditPreference;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/CallForwardEditPreference;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/CallForwardEditPreference;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 113
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "GsmUmtsCallForwardOptions"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Intent is"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const-string/jumbo v4, "service_class"

    invoke-virtual {v2, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mServiceClass:I

    .line 116
    const-string/jumbo v4, "GsmUmtsCallForwardOptions"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "serviceClass: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mServiceClass:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iput-boolean v8, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mFirstResume:Z

    .line 119
    iput-object p1, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mIcicle:Landroid/os/Bundle;

    .line 121
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 122
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_1

    .line 124
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 126
    :cond_1
    return-void
.end method

.method public onFinished(Landroid/preference/Preference;Z)V
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "reading"    # Z

    .prologue
    .line 329
    iget v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mInitIndex:I

    iget-object v1, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->isFinishing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 330
    iget v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mInitIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mInitIndex:I

    .line 331
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mInitIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/CallForwardEditPreference;

    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-boolean v4, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mReplaceInvalidCFNumbers:Z

    .line 332
    iget v5, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mServiceClass:I

    .line 331
    const/4 v2, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/phone/CallForwardEditPreference;->init(Lcom/android/phone/TimeConsumingPreferenceListener;ZLcom/android/internal/telephony/Phone;ZI)V

    .line 335
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onFinished(Landroid/preference/Preference;Z)V

    .line 336
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 379
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 380
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 381
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->finish()V

    .line 382
    const/4 v1, 0x1

    return v1

    .line 384
    :cond_0
    invoke-super {p0, p1}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 305
    invoke-super {p0}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onPause()V

    .line 306
    iget-boolean v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mIsCMCC:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 310
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 229
    invoke-super {p0}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onResume()V

    .line 231
    iget-boolean v2, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mIsCMCC:Z

    if-eqz v2, :cond_0

    .line 232
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 233
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "android.intent.action.ANY_DATA_STATE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 234
    new-instance v2, Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;-><init>(Lcom/android/phone/GsmUmtsCallForwardOptions;Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;)V

    iput-object v2, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 235
    iget-object v2, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 236
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v1

    .line 237
    .local v1, "mSubscriptionManager":Landroid/telephony/SubscriptionManager;
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->checkDataStatus()V

    .line 241
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    .end local v1    # "mSubscriptionManager":Landroid/telephony/SubscriptionManager;
    :goto_0
    return-void

    .line 239
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/GsmUmtsCallForwardOptions;->initCallforwarding()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 314
    invoke-super {p0, p1}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 316
    iget-object v3, p0, Lcom/android/phone/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "pref$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/phone/CallForwardEditPreference;

    .line 317
    .local v1, "pref":Lcom/android/phone/CallForwardEditPreference;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 318
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "toggle"

    invoke-virtual {v1}, Lcom/android/phone/CallForwardEditPreference;->isToggled()Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 319
    iget-object v3, v1, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    if-eqz v3, :cond_0

    .line 320
    const-string/jumbo v3, "number"

    iget-object v4, v1, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget-object v4, v4, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const-string/jumbo v3, "status"

    iget-object v4, v1, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget v4, v4, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 323
    :cond_0
    invoke-virtual {v1}, Lcom/android/phone/CallForwardEditPreference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 325
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "pref":Lcom/android/phone/CallForwardEditPreference;
    :cond_1
    return-void
.end method
