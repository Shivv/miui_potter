.class public Lcom/android/phone/TelephonyJobService;
.super Landroid/app/job/JobService;
.source "TelephonyJobService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/TelephonyJobService$TelephonyJobHandler;
    }
.end annotation


# static fields
.field private static sTelephonyJobService:Landroid/content/ComponentName;


# instance fields
.field private mJobHandler:Lcom/android/phone/TelephonyJobService$TelephonyJobHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v1, "com.android.phone"

    .line 31
    const-class v2, Lcom/android/phone/TelephonyJobService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 30
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/phone/TelephonyJobService;->sTelephonyJobService:Landroid/content/ComponentName;

    .line 24
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method public static initReportJobSchedule(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const v9, 0x123a8

    .line 85
    const-string/jumbo v6, "jobscheduler"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/job/JobScheduler;

    .line 86
    .local v3, "jobScheduler":Landroid/app/job/JobScheduler;
    if-nez v3, :cond_0

    .line 87
    const-string/jumbo v6, "TelephonyJobService"

    const-string/jumbo v7, "initReportJobSchedule jobScheduler == null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    return-void

    .line 90
    :cond_0
    invoke-virtual {v3}, Landroid/app/job/JobScheduler;->getAllPendingJobs()Ljava/util/List;

    move-result-object v2

    .line 91
    .local v2, "jobInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/job/JobInfo;>;"
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "info$iterator":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobInfo;

    .line 92
    .local v0, "info":Landroid/app/job/JobInfo;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/job/JobInfo;->getId()I

    move-result v6

    if-ne v6, v9, :cond_1

    .line 93
    const-string/jumbo v6, "TelephonyJobService"

    const-string/jumbo v7, "initReportJobSchedule(): job exist, return"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    return-void

    .line 98
    .end local v0    # "info":Landroid/app/job/JobInfo;
    :cond_2
    new-instance v6, Landroid/app/job/JobInfo$Builder;

    new-instance v7, Landroid/content/ComponentName;

    const-class v8, Lcom/android/phone/TelephonyJobService;

    invoke-direct {v7, p0, v8}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v6, v9, v7}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 99
    const-wide/32 v8, 0x5265c00

    .line 98
    invoke-virtual {v6, v8, v9}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v6

    .line 100
    const/4 v7, 0x1

    .line 98
    invoke-virtual {v6, v7}, Landroid/app/job/JobInfo$Builder;->setPersisted(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v4

    .line 102
    .local v4, "reportJob":Landroid/app/job/JobInfo;
    invoke-virtual {v3, v4}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    move-result v5

    .line 103
    .local v5, "result":I
    const-string/jumbo v6, "TelephonyJobService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "initReportJobSchedules jobInfo = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/app/job/JobInfo;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " result="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 37
    invoke-super {p0}, Landroid/app/job/JobService;->onCreate()V

    .line 38
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "TelephonyJobService"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 40
    new-instance v1, Lcom/android/phone/TelephonyJobService$TelephonyJobHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/phone/TelephonyJobService$TelephonyJobHandler;-><init>(Lcom/android/phone/TelephonyJobService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/phone/TelephonyJobService;->mJobHandler:Lcom/android/phone/TelephonyJobService$TelephonyJobHandler;

    .line 41
    const-string/jumbo v1, "TelephonyJobService"

    const-string/jumbo v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    return-void
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 5
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .prologue
    const/4 v4, 0x0

    .line 65
    if-nez p1, :cond_0

    return v4

    .line 66
    :cond_0
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v0

    .line 67
    .local v0, "jobId":I
    const-string/jumbo v1, "TelephonyJobService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onStartJob id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    packed-switch v0, :pswitch_data_0

    .line 75
    :goto_0
    return v4

    .line 70
    :pswitch_0
    iget-object v1, p0, Lcom/android/phone/TelephonyJobService;->mJobHandler:Lcom/android/phone/TelephonyJobService$TelephonyJobHandler;

    iget-object v2, p0, Lcom/android/phone/TelephonyJobService;->mJobHandler:Lcom/android/phone/TelephonyJobService$TelephonyJobHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/phone/TelephonyJobService$TelephonyJobHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/phone/TelephonyJobService$TelephonyJobHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x123a8
        :pswitch_0
    .end packed-switch
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 3
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .prologue
    .line 80
    const-string/jumbo v0, "TelephonyJobService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "OnStopJob id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const/4 v0, 0x0

    return v0
.end method
