.class public Lcom/android/phone/CdmaCallOptions;
.super Lcom/android/phone/TimeConsumingPreferenceActivity;
.source "CdmaCallOptions.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field private final DBG:Z

.field private mButtonVoicePrivacy:Landroid/preference/SwitchPreference;

.field private mCWButton:Lcom/android/phone/CallWaitingSwitchPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/android/phone/TimeConsumingPreferenceActivity;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/CdmaCallOptions;->DBG:Z

    .line 54
    return-void
.end method

.method private static isActivityPresent(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intentName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 70
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 72
    .local v2, "pm":Landroid/content/pm/PackageManager;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v0, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 74
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "resolveInfo$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 75
    .local v3, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    .line 77
    const/4 v5, 0x1

    return v5

    .line 80
    .end local v3    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_1
    return v6
.end method

.method public static isCdmaCallWaitingActivityPresent(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    const-string/jumbo v0, "org.codeaurora.settings.CDMA_CALL_WAITING"

    invoke-static {p0, v0}, Lcom/android/phone/CdmaCallOptions;->isActivityPresent(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isPromptTurnOffEnhance4GLTE(Lcom/android/internal/telephony/Phone;)Z
    .locals 6
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v2, 0x0

    .line 93
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    if-nez v3, :cond_1

    .line 94
    :cond_0
    return v2

    .line 96
    :cond_1
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v3

    invoke-static {p0, v3}, Lcom/android/ims/ImsManager;->getInstance(Landroid/content/Context;I)Lcom/android/ims/ImsManager;

    move-result-object v1

    .line 98
    .local v1, "imsMgr":Lcom/android/ims/ImsManager;
    :try_start_0
    invoke-virtual {v1}, Lcom/android/ims/ImsManager;->getImsServiceStatus()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    .line 99
    const-string/jumbo v3, "CdmaCallOptions"

    const-string/jumbo v4, "ImsServiceStatus is not ready!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/android/ims/ImsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    return v2

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "ex":Lcom/android/ims/ImsException;
    const-string/jumbo v3, "CdmaCallOptions"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception when trying to get ImsServiceStatus: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    .end local v0    # "ex":Lcom/android/ims/ImsException;
    :cond_2
    invoke-virtual {v1}, Lcom/android/ims/ImsManager;->isEnhanced4gLteModeSettingEnabledByUserForSlot()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 106
    invoke-virtual {v1}, Lcom/android/ims/ImsManager;->isNonTtyOrTtyOnVolteEnabledForSlot()Z

    move-result v3

    .line 105
    if-eqz v3, :cond_3

    .line 107
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->isImsRegistered()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 105
    if-eqz v3, :cond_3

    .line 108
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 105
    :cond_3
    return v2
.end method

.method private showAlertDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 112
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 115
    const v2, 0x1010355

    .line 112
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 116
    const v2, 0x104000a

    .line 112
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 117
    const/high16 v2, 0x1040000

    .line 112
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 120
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 121
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/android/phone/CdmaCallOptions;->finish()V

    .line 137
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 125
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 126
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 127
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 128
    invoke-virtual {p0, v0}, Lcom/android/phone/CdmaCallOptions;->startActivity(Landroid/content/Intent;)V

    .line 130
    .end local v0    # "newIntent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/CdmaCallOptions;->finish()V

    .line 131
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x0

    .line 142
    invoke-super {p0, p1}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 144
    const v9, 0x7f06000b

    invoke-virtual {p0, v9}, Lcom/android/phone/CdmaCallOptions;->addPreferencesFromResource(I)V

    .line 146
    new-instance v7, Lcom/android/phone/SubscriptionInfoHelper;

    invoke-virtual {p0}, Lcom/android/phone/CdmaCallOptions;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-direct {v7, p0, v9}, Lcom/android/phone/SubscriptionInfoHelper;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    .line 148
    .local v7, "subInfoHelper":Lcom/android/phone/SubscriptionInfoHelper;
    invoke-virtual {p0}, Lcom/android/phone/CdmaCallOptions;->getActionBar()Landroid/app/ActionBar;

    move-result-object v9

    invoke-virtual {p0}, Lcom/android/phone/CdmaCallOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b030f

    .line 147
    invoke-virtual {v7, v9, v10, v11}, Lcom/android/phone/SubscriptionInfoHelper;->setActionBarTitle(Landroid/app/ActionBar;Landroid/content/res/Resources;I)V

    .line 150
    const-string/jumbo v9, "button_voice_privacy_key"

    invoke-virtual {p0, v9}, Lcom/android/phone/CdmaCallOptions;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Landroid/preference/SwitchPreference;

    iput-object v9, p0, Lcom/android/phone/CdmaCallOptions;->mButtonVoicePrivacy:Landroid/preference/SwitchPreference;

    .line 152
    invoke-virtual {v7}, Lcom/android/phone/SubscriptionInfoHelper;->hasSubId()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 153
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v9

    .line 154
    invoke-virtual {v7}, Lcom/android/phone/SubscriptionInfoHelper;->getSubId()I

    move-result v10

    .line 153
    invoke-virtual {v9, v10}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 159
    .local v0, "carrierConfig":Landroid/os/PersistableBundle;
    :goto_0
    invoke-virtual {v7}, Lcom/android/phone/SubscriptionInfoHelper;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 160
    .local v2, "phone":Lcom/android/internal/telephony/Phone;
    const-string/jumbo v9, "CdmaCallOptions"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "sub id = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/android/phone/SubscriptionInfoHelper;->getSubId()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " phone id = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 161
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v11

    .line 160
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-virtual {p0}, Lcom/android/phone/CdmaCallOptions;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    .line 164
    .local v6, "prefScreen":Landroid/preference/PreferenceScreen;
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v9

    if-ne v9, v13, :cond_0

    .line 165
    const-string/jumbo v9, "voice_privacy_disable_ui_bool"

    invoke-virtual {v0, v9}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 164
    if-eqz v9, :cond_1

    .line 167
    :cond_0
    const-string/jumbo v9, "button_voice_privacy_key"

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 166
    check-cast v5, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;

    .line 168
    .local v5, "prefPri":Lcom/android/phone/CdmaVoicePrivacySwitchPreference;
    if-eqz v5, :cond_1

    .line 169
    invoke-virtual {v5, v12}, Lcom/android/phone/CdmaVoicePrivacySwitchPreference;->setEnabled(Z)V

    .line 173
    .end local v5    # "prefPri":Lcom/android/phone/CdmaVoicePrivacySwitchPreference;
    :cond_1
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v9

    if-ne v9, v13, :cond_2

    .line 174
    invoke-direct {p0, v2}, Lcom/android/phone/CdmaCallOptions;->isPromptTurnOffEnhance4GLTE(Lcom/android/internal/telephony/Phone;)Z

    move-result v9

    .line 173
    if-eqz v9, :cond_2

    .line 175
    const-string/jumbo v9, "cdma_cw_cf_enabled_bool"

    invoke-virtual {v0, v9}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    .line 173
    if-eqz v9, :cond_2

    .line 176
    invoke-virtual {p0}, Lcom/android/phone/CdmaCallOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 177
    const v10, 0x7f0b051b

    .line 176
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 178
    .local v8, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/phone/CdmaCallOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 179
    const v10, 0x7f0b051c

    .line 178
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 180
    .local v1, "msg":Ljava/lang/String;
    invoke-direct {p0, v8, v1}, Lcom/android/phone/CdmaCallOptions;->showAlertDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    .end local v1    # "msg":Ljava/lang/String;
    .end local v8    # "title":Ljava/lang/String;
    :cond_2
    const-string/jumbo v9, "button_cw_ut_key"

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    check-cast v9, Lcom/android/phone/CallWaitingSwitchPreference;

    iput-object v9, p0, Lcom/android/phone/CdmaCallOptions;->mCWButton:Lcom/android/phone/CallWaitingSwitchPreference;

    .line 184
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v9

    if-ne v9, v13, :cond_3

    .line 185
    const-string/jumbo v9, "cdma_cw_cf_enabled_bool"

    invoke-virtual {v0, v9}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    .line 184
    if-nez v9, :cond_3

    .line 186
    invoke-static {p0}, Lcom/android/phone/CdmaCallOptions;->isCdmaCallWaitingActivityPresent(Landroid/content/Context;)Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    .line 184
    if-eqz v9, :cond_8

    .line 187
    :cond_3
    const-string/jumbo v9, "CdmaCallOptions"

    const-string/jumbo v10, "Disabled CW CF"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const-string/jumbo v9, "button_cw_key"

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 188
    check-cast v4, Landroid/preference/PreferenceScreen;

    .line 190
    .local v4, "prefCW":Landroid/preference/PreferenceScreen;
    iget-object v9, p0, Lcom/android/phone/CdmaCallOptions;->mCWButton:Lcom/android/phone/CallWaitingSwitchPreference;

    if-eqz v9, :cond_4

    .line 191
    iget-object v9, p0, Lcom/android/phone/CdmaCallOptions;->mCWButton:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 193
    :cond_4
    if-eqz v4, :cond_5

    .line 194
    invoke-virtual {v4, v12}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 197
    :cond_5
    const-string/jumbo v9, "button_cf_expand_key"

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 196
    check-cast v3, Landroid/preference/PreferenceScreen;

    .line 198
    .local v3, "prefCF":Landroid/preference/PreferenceScreen;
    if-eqz v3, :cond_6

    .line 199
    invoke-virtual {v3, v12}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 245
    :cond_6
    :goto_1
    return-void

    .line 156
    .end local v0    # "carrierConfig":Landroid/os/PersistableBundle;
    .end local v2    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v3    # "prefCF":Landroid/preference/PreferenceScreen;
    .end local v4    # "prefCW":Landroid/preference/PreferenceScreen;
    .end local v6    # "prefScreen":Landroid/preference/PreferenceScreen;
    :cond_7
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/phone/PhoneGlobals;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .restart local v0    # "carrierConfig":Landroid/os/PersistableBundle;
    goto/16 :goto_0

    .line 202
    .restart local v2    # "phone":Lcom/android/internal/telephony/Phone;
    .restart local v6    # "prefScreen":Landroid/preference/PreferenceScreen;
    :cond_8
    const-string/jumbo v9, "CdmaCallOptions"

    const-string/jumbo v10, "Enabled CW CF"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    const-string/jumbo v9, "button_cw_key"

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 203
    check-cast v4, Landroid/preference/PreferenceScreen;

    .line 206
    .restart local v4    # "prefCW":Landroid/preference/PreferenceScreen;
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 207
    invoke-virtual {v6, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 208
    iget-object v9, p0, Lcom/android/phone/CdmaCallOptions;->mCWButton:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-virtual {v9, p0, v12, v2}, Lcom/android/phone/CallWaitingSwitchPreference;->init(Lcom/android/phone/TimeConsumingPreferenceListener;ZLcom/android/internal/telephony/Phone;)V

    .line 228
    :cond_9
    :goto_2
    const-string/jumbo v9, "button_cf_expand_key"

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 227
    check-cast v3, Landroid/preference/PreferenceScreen;

    .line 229
    .restart local v3    # "prefCF":Landroid/preference/PreferenceScreen;
    if-eqz v3, :cond_6

    .line 231
    new-instance v9, Lcom/android/phone/CdmaCallOptions$2;

    invoke-direct {v9, p0, v7}, Lcom/android/phone/CdmaCallOptions$2;-><init>(Lcom/android/phone/CdmaCallOptions;Lcom/android/phone/SubscriptionInfoHelper;)V

    .line 230
    invoke-virtual {v3, v9}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_1

    .line 210
    .end local v3    # "prefCF":Landroid/preference/PreferenceScreen;
    :cond_a
    iget-object v9, p0, Lcom/android/phone/CdmaCallOptions;->mCWButton:Lcom/android/phone/CallWaitingSwitchPreference;

    if-eqz v9, :cond_b

    .line 211
    iget-object v9, p0, Lcom/android/phone/CdmaCallOptions;->mCWButton:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 213
    :cond_b
    if-eqz v4, :cond_9

    .line 215
    new-instance v9, Lcom/android/phone/CdmaCallOptions$1;

    invoke-direct {v9, p0, v2}, Lcom/android/phone/CdmaCallOptions$1;-><init>(Lcom/android/phone/CdmaCallOptions;Lcom/android/internal/telephony/Phone;)V

    .line 214
    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 249
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 250
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/android/phone/CdmaCallOptions;->onBackPressed()V

    .line 252
    const/4 v1, 0x1

    return v1

    .line 254
    :cond_0
    invoke-super {p0, p1}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 259
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "button_voice_privacy_key"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    const/4 v0, 0x1

    return v0

    .line 262
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
