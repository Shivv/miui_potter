.class public Lcom/android/phone/MiuiPhoneReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiuiPhoneReceiver.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static sInstance:Lcom/android/phone/MiuiPhoneReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string/jumbo v0, "PhoneReceiver"

    sput-object v0, Lcom/android/phone/MiuiPhoneReceiver;->TAG:Ljava/lang/String;

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 48
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 49
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-static {p1}, Lcom/android/services/telephony/ims/ImsAdapter;->isVolteSupportedByDevice(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isVolteRegionFeatureEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const-string/jumbo v1, "miui.intent.action.MIUI_REGION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/android/phone/MiuiPhoneReceiver;->checkSystemUpdate()V

    .line 53
    :cond_0
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isCTVolteSupportedByDevice()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54
    const-string/jumbo v1, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 55
    sget-boolean v1, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-nez v1, :cond_1

    .line 56
    const-string/jumbo v1, "miui.intent.action.ACTION_DEFAULT_DATA_SLOT_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 59
    :cond_1
    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 60
    return-void
.end method

.method private checkSystemUpdate()V
    .locals 6

    .prologue
    .line 127
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 129
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 130
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string/jumbo v3, "build_miui_fingerprint"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    .local v1, "lastFingerprint":Ljava/lang/String;
    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 132
    sget-object v3, Lcom/android/phone/MiuiPhoneReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "checkSystemUpdate Build fingerprint changed. old: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 133
    const-string/jumbo v5, " new: "

    .line 132
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 133
    sget-object v5, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    .line 132
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string/jumbo v4, "build_miui_fingerprint"

    sget-object v5, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 135
    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/android/services/telephony/ims/ImsAdapter;->isVolteSupportedByDevice(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 136
    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/android/phone/MiuiPhoneReceiver;->updateVolteConfig(Ljava/lang/String;Z)V

    .line 139
    :cond_0
    return-void
.end method

.method private onIccStatusChanged(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 86
    const-string/jumbo v1, "ss"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "iccState":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/android/phone/MiuiPhoneReceiver;->showChangeDataPreferenceDialogIfNeed(Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method static register(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    sget-object v0, Lcom/android/phone/MiuiPhoneReceiver;->sInstance:Lcom/android/phone/MiuiPhoneReceiver;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/android/phone/MiuiPhoneReceiver;

    invoke-direct {v0, p0}, Lcom/android/phone/MiuiPhoneReceiver;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/phone/MiuiPhoneReceiver;->sInstance:Lcom/android/phone/MiuiPhoneReceiver;

    .line 45
    :cond_0
    return-void
.end method

.method private showChangeDataPreferenceDialogIfNeed(Ljava/lang/String;)V
    .locals 7
    .param p1, "iccState"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 91
    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v3, :cond_0

    .line 92
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 91
    if-nez v3, :cond_0

    .line 93
    const-string/jumbo v3, "LOADED"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 91
    if-nez v3, :cond_0

    .line 94
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/telephony/TelephonyManager;->getIccCardCount()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 95
    :cond_0
    return-void

    .line 98
    :cond_1
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    .line 99
    .local v0, "dataSlotId":I
    sget-object v3, Lcom/android/phone/MiuiPhoneReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getDefaultDataSlotId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v3, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    if-ge v1, v3, :cond_3

    .line 101
    if-eq v1, v0, :cond_6

    .line 102
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmiui/telephony/DefaultSimManager;->getSimInsertStates(I)I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    .line 103
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmiui/telephony/DefaultSimManager;->getSimInsertStates(I)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_6

    .line 104
    :cond_2
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmiui/telephony/TelephonyManager;->getSimOperatorForSlot(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/MiuiPhoneUtils;->is4GOnlySim(Ljava/lang/String;)Z

    move-result v3

    .line 101
    if-eqz v3, :cond_6

    .line 105
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "device_provisioned"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_4

    const/4 v2, 0x1

    .line 106
    .local v2, "provisioned":Z
    :goto_1
    if-nez v2, :cond_5

    .line 107
    sget-object v3, Lcom/android/phone/MiuiPhoneReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Detect 4G only SIM in slot "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " and not provision, set it to default data slot directly"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmiui/telephony/SubscriptionManager;->setDefaultDataSlotId(I)V

    .line 115
    .end local v2    # "provisioned":Z
    :cond_3
    :goto_2
    return-void

    .line 105
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "provisioned":Z
    goto :goto_1

    .line 110
    :cond_5
    invoke-virtual {p0}, Lcom/android/phone/MiuiPhoneReceiver;->showChangeDataPreferenceDialog()V

    goto :goto_2

    .line 100
    .end local v2    # "provisioned":Z
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private updateVolteConfig(Ljava/lang/String;Z)V
    .locals 8
    .param p1, "region"    # Ljava/lang/String;
    .param p2, "systemUpdate"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 142
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 143
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0, p1}, Lcom/android/phone/MiuiPhoneUtils;->isRegionSupportVoLTE(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 145
    .local v3, "isRegionSupportVolte":Z
    if-nez v3, :cond_1

    .line 146
    invoke-static {v0, v4}, Lcom/android/services/telephony/ims/ImsAdapter;->setEnhanced4gLteModeSetting(Landroid/content/Context;Z)V

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    if-eqz p2, :cond_2

    .line 149
    return-void

    .line 151
    :cond_2
    sget-boolean v4, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v4, :cond_4

    .line 152
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    sget v4, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v2, v4, :cond_0

    .line 153
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lmiui/telephony/TelephonyManager;->isVolteEnabledByPlatform(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 154
    invoke-static {v0, v6, v2}, Lcom/android/services/telephony/ims/ImsAdapter;->setEnhanced4gLteModeSetting(Landroid/content/Context;ZI)V

    .line 152
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 159
    .end local v2    # "i":I
    :cond_4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 160
    const-string/jumbo v5, "volte_vt_enabled"

    .line 158
    invoke-static {v4, v5, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 161
    .local v1, "enabled":I
    if-eq v1, v7, :cond_5

    if-nez v1, :cond_0

    .line 163
    :cond_5
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/telephony/TelephonyManager;->isVolteEnabledByPlatform()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 164
    invoke-static {v0, v6}, Lcom/android/services/telephony/ims/ImsAdapter;->setEnhanced4gLteModeSetting(Landroid/content/Context;Z)V

    goto :goto_0

    .line 167
    :cond_6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "volte_vt_enabled"

    .line 166
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, -0x1

    .line 64
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "action":Ljava/lang/String;
    sget-object v3, Lcom/android/phone/MiuiPhoneReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Received action:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const-string/jumbo v3, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 67
    invoke-direct {p0, p2}, Lcom/android/phone/MiuiPhoneReceiver;->onIccStatusChanged(Landroid/content/Intent;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    const-string/jumbo v3, "miui.intent.action.MIUI_REGION_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 69
    const-string/jumbo v3, "region"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 70
    .local v2, "region":Ljava/lang/String;
    sget-object v3, Lcom/android/phone/MiuiPhoneReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Region change to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/android/phone/MiuiPhoneReceiver;->updateVolteConfig(Ljava/lang/String;Z)V

    goto :goto_0

    .line 72
    .end local v2    # "region":Ljava/lang/String;
    :cond_2
    const-string/jumbo v3, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 73
    const-string/jumbo v3, "miui.intent.action.ACTION_DEFAULT_DATA_SLOT_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 72
    if-eqz v3, :cond_0

    .line 74
    :cond_3
    const-string/jumbo v3, "phone"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 75
    .local v1, "phoneId":I
    invoke-static {v1}, Lmiui/telephony/SubscriptionManager;->isRealSlotId(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 76
    return-void

    .line 78
    :cond_4
    sget-object v3, Lcom/android/phone/MiuiPhoneReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ACTION_CARRIER_CONFIG_CHANGED phoneId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/MiuiPhoneUtils;->isVolteEnabledByPlatform(Lcom/android/internal/telephony/Phone;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 80
    invoke-static {v1, v6}, Lcom/android/phone/MiuiPhoneUtils;->sendEnhanced4GLteModeChangeBroadcast(II)V

    goto :goto_0
.end method

.method public showChangeDataPreferenceDialog()V
    .locals 4

    .prologue
    .line 118
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 119
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 120
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 121
    const-string/jumbo v2, "dialog_type"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 122
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 123
    sget-object v2, Lcom/android/phone/MiuiPhoneReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "showChangeDataPreferenceDialog"

    invoke-static {v2, v3}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    return-void
.end method
