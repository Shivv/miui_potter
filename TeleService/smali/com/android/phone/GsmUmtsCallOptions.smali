.class public Lcom/android/phone/GsmUmtsCallOptions;
.super Landroid/preference/PreferenceActivity;
.source "GsmUmtsCallOptions.java"


# instance fields
.field private final DBG:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/GsmUmtsCallOptions;->DBG:Z

    .line 29
    return-void
.end method

.method public static init(Landroid/preference/PreferenceScreen;Lcom/android/phone/SubscriptionInfoHelper;)V
    .locals 6
    .param p0, "prefScreen"    # Landroid/preference/PreferenceScreen;
    .param p1, "subInfoHelper"    # Lcom/android/phone/SubscriptionInfoHelper;

    .prologue
    .line 65
    const-string/jumbo v3, "call_forwarding_key"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 66
    .local v2, "callForwardingPref":Landroid/preference/Preference;
    const-class v3, Lcom/android/phone/CallForwardType;

    invoke-virtual {p1, v3}, Lcom/android/phone/SubscriptionInfoHelper;->getIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 69
    const-string/jumbo v3, "additional_gsm_call_settings_key"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 71
    .local v0, "additionalGsmSettingsPref":Landroid/preference/Preference;
    const-class v3, Lcom/android/phone/GsmUmtsAdditionalCallOptions;

    invoke-virtual {p1, v3}, Lcom/android/phone/SubscriptionInfoHelper;->getIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    .line 70
    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 72
    const-string/jumbo v3, "button_callbarring_expand_key"

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 73
    .local v1, "callBarringPref":Landroid/preference/Preference;
    invoke-virtual {v1}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "subscription"

    .line 74
    invoke-virtual {p1}, Lcom/android/phone/SubscriptionInfoHelper;->getSubId()I

    move-result v5

    .line 73
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 75
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v1, 0x7f060012

    invoke-virtual {p0, v1}, Lcom/android/phone/GsmUmtsCallOptions;->addPreferencesFromResource(I)V

    .line 43
    new-instance v0, Lcom/android/phone/SubscriptionInfoHelper;

    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallOptions;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/phone/SubscriptionInfoHelper;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    .line 45
    .local v0, "subInfoHelper":Lcom/android/phone/SubscriptionInfoHelper;
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallOptions;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallOptions;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b030a

    .line 44
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/phone/SubscriptionInfoHelper;->setActionBarTitle(Landroid/app/ActionBar;Landroid/content/res/Resources;I)V

    .line 46
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallOptions;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/phone/GsmUmtsCallOptions;->init(Landroid/preference/PreferenceScreen;Lcom/android/phone/SubscriptionInfoHelper;)V

    .line 48
    invoke-virtual {v0}, Lcom/android/phone/SubscriptionInfoHelper;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallOptions;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 52
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 56
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 57
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/android/phone/GsmUmtsCallOptions;->onBackPressed()V

    .line 59
    const/4 v1, 0x1

    return v1

    .line 61
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method
