.class Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "CallNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/CallNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallNotifierPhoneStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CallNotifier;


# direct methods
.method public constructor <init>(Lcom/android/phone/CallNotifier;I)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/phone/CallNotifier;
    .param p2, "subId"    # I

    .prologue
    .line 836
    iput-object p1, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->this$0:Lcom/android/phone/CallNotifier;

    .line 837
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/telephony/PhoneStateListener;-><init>(Ljava/lang/Integer;)V

    .line 838
    return-void
.end method


# virtual methods
.method public onCallForwardingIndicatorChanged(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    .line 849
    const-string/jumbo v1, "CallNotifier"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCallForwardingIndicatorChanged(): subId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->mSubId:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 850
    const-string/jumbo v2, ", visible="

    .line 849
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 850
    if-eqz p1, :cond_0

    const-string/jumbo v0, "Y"

    .line 849
    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    iget-object v0, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->this$0:Lcom/android/phone/CallNotifier;

    invoke-static {v0}, Lcom/android/phone/CallNotifier;->-get3(Lcom/android/phone/CallNotifier;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->mSubId:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 852
    iget-object v0, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->this$0:Lcom/android/phone/CallNotifier;

    iget-object v1, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->mSubId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/phone/CallNotifier;->updatePhoneStateListeners(ZII)V

    .line 853
    return-void

    .line 850
    :cond_0
    const-string/jumbo v0, "N"

    goto :goto_0
.end method

.method public onMessageWaitingIndicatorChanged(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    const/4 v3, 0x0

    .line 843
    iget-object v0, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->this$0:Lcom/android/phone/CallNotifier;

    invoke-static {v0}, Lcom/android/phone/CallNotifier;->-get5(Lcom/android/phone/CallNotifier;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->mSubId:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 844
    iget-object v0, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->this$0:Lcom/android/phone/CallNotifier;

    iget-object v1, p0, Lcom/android/phone/CallNotifier$CallNotifierPhoneStateListener;->mSubId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v3, v3, v1}, Lcom/android/phone/CallNotifier;->updatePhoneStateListeners(ZII)V

    .line 845
    return-void
.end method
