.class public Lcom/android/phone/CallNotifierInjector;
.super Ljava/lang/Object;
.source "CallNotifierInjector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;
    }
.end annotation


# static fields
.field private static mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getImsForgroundCall()Lcom/android/ims/ImsCall;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 83
    invoke-static {}, Lcom/android/phone/CallNotifierInjector;->getImsPhoneInCall()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 84
    .local v1, "phoneInCall":Lcom/android/internal/telephony/Phone;
    if-nez v1, :cond_0

    .line 85
    return-object v6

    .line 87
    :cond_0
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    .line 88
    .local v0, "fgCall":Lcom/android/internal/telephony/imsphone/ImsPhoneCall;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v2

    check-cast v2, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    .line 89
    .local v2, "ringCall":Lcom/android/internal/telephony/imsphone/ImsPhoneCall;
    const-string/jumbo v3, "CallNotifierInjector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Ims fgCall="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string/jumbo v3, "CallNotifierInjector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Ims ringCall="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->isIdle()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    .line 93
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->getImsCall()Lcom/android/ims/ImsCall;

    move-result-object v3

    return-object v3

    .line 94
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->isIdle()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    .line 95
    invoke-virtual {v0}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->getImsCall()Lcom/android/ims/ImsCall;

    move-result-object v3

    return-object v3

    .line 97
    :cond_2
    return-object v6
.end method

.method private static getImsPhoneInCall()Lcom/android/internal/telephony/Phone;
    .locals 4

    .prologue
    .line 74
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    iget-object v2, v2, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v2}, Lcom/android/internal/telephony/CallManager;->getAllPhones()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "phone$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/Phone;

    .line 75
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    if-eq v2, v3, :cond_0

    .line 76
    return-object v0

    .line 79
    .end local v0    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    const/4 v2, 0x0

    return-object v2
.end method

.method public static handleMessage(Landroid/os/Message;Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;)V
    .locals 5
    .param p0, "msg"    # Landroid/os/Message;
    .param p1, "tonePlayer"    # Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;

    .prologue
    .line 100
    iget v2, p0, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 113
    :goto_0
    return-void

    .line 102
    :pswitch_0
    iget-object v0, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 103
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 104
    .local v1, "isStart":Z
    const-string/jumbo v2, "CallNotifierInjector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Received PHONE_ONHOLD_TONE event, result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-static {v1, p1}, Lcom/android/phone/CallNotifierInjector;->onPlayLocalHoldTone(ZLcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;)V

    goto :goto_0

    .line 108
    .end local v0    # "ar":Landroid/os/AsyncResult;
    .end local v1    # "isStart":Z
    :pswitch_1
    invoke-static {p1}, Lcom/android/phone/CallNotifierInjector;->updatePlayLocalHoldTone(Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;)V

    goto :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static onPlayLocalHoldTone(ZLcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;)V
    .locals 8
    .param p0, "isStart"    # Z
    .param p1, "tonePlayer"    # Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;

    .prologue
    const/4 v7, 0x0

    .line 30
    const-string/jumbo v4, "CallNotifierInjector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onPlayLocalHoldTone, result="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-static {}, Lcom/android/phone/CallNotifierInjector;->getImsPhoneInCall()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 32
    .local v2, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v2, :cond_0

    .line 33
    return-void

    .line 35
    :cond_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/phone/PhoneGlobals;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v5

    invoke-static {v4, v5}, Landroid/telephony/SubscriptionManager;->getResourcesForSubId(Landroid/content/Context;I)Landroid/content/res/Resources;

    move-result-object v3

    .line 36
    .local v3, "res":Landroid/content/res/Resources;
    const v4, 0x7f0e0034

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 37
    .local v0, "config":Z
    const-string/jumbo v4, "CallNotifierInjector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "config_volte_play_local_hold_tone="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    if-nez v0, :cond_1

    .line 39
    return-void

    .line 41
    :cond_1
    invoke-static {}, Lcom/android/phone/CallNotifierInjector;->getImsForgroundCall()Lcom/android/ims/ImsCall;

    move-result-object v1

    .line 42
    .local v1, "imsForgroundCall":Lcom/android/ims/ImsCall;
    sget-object v4, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    if-nez v4, :cond_3

    if-eqz v1, :cond_3

    if-eqz p0, :cond_3

    .line 43
    sput-object v1, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    .line 44
    const-string/jumbo v4, "CallNotifierInjector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "mPlayingLocalHoldToneCall="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-interface {p1}, Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;->startPlayLocalHoldTone()V

    .line 50
    :cond_2
    :goto_0
    return-void

    .line 46
    :cond_3
    if-nez p0, :cond_2

    .line 47
    invoke-interface {p1}, Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;->stopPlayLocalHoldTone()V

    .line 48
    sput-object v7, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    goto :goto_0
.end method

.method private static updatePlayLocalHoldTone(Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;)V
    .locals 5
    .param p0, "tonePlayer"    # Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;

    .prologue
    const/4 v4, 0x0

    .line 53
    sget-object v1, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    if-nez v1, :cond_0

    .line 54
    return-void

    .line 57
    :cond_0
    invoke-static {}, Lcom/android/phone/CallNotifierInjector;->getImsForgroundCall()Lcom/android/ims/ImsCall;

    move-result-object v0

    .line 58
    .local v0, "imsForgroundCall":Lcom/android/ims/ImsCall;
    const-string/jumbo v1, "CallNotifierInjector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePlayLocalHoldTone imsActiveFgCall="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    sget-object v1, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    if-ne v0, v1, :cond_1

    sget-object v1, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    invoke-virtual {v1}, Lcom/android/ims/ImsCall;->getState()I

    move-result v1

    if-nez v1, :cond_3

    .line 61
    :cond_1
    invoke-interface {p0}, Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;->stopPlayLocalHoldTone()V

    .line 62
    sget-object v1, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    invoke-virtual {v1}, Lcom/android/ims/ImsCall;->getState()I

    move-result v1

    if-nez v1, :cond_2

    .line 63
    const-string/jumbo v1, "CallNotifierInjector"

    const-string/jumbo v2, "Clear mPlayingLocalHoldToneCall"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    sput-object v4, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    .line 70
    :cond_2
    :goto_0
    return-void

    .line 67
    :cond_3
    sget-object v1, Lcom/android/phone/CallNotifierInjector;->mPlayingLocalHoldToneCall:Lcom/android/ims/ImsCall;

    if-ne v0, v1, :cond_2

    .line 68
    invoke-interface {p0}, Lcom/android/phone/CallNotifierInjector$LocalHoldTonePlayer;->startPlayLocalHoldTone()V

    goto :goto_0
.end method
