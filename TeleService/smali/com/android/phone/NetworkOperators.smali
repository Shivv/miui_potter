.class public Lcom/android/phone/NetworkOperators;
.super Landroid/preference/PreferenceCategory;
.source "NetworkOperators.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/NetworkOperators$1;
    }
.end annotation


# instance fields
.field private mAutoSelect:Landroid/preference/TwoStatePreference;

.field private final mHandler:Landroid/os/Handler;

.field private mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

.field mPhoneId:I

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSubId:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/NetworkOperators;)Landroid/preference/TwoStatePreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/NetworkOperators;

    .prologue
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mAutoSelect:Landroid/preference/TwoStatePreference;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/NetworkOperators;)Lcom/android/phone/NetworkSelectListPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/NetworkOperators;

    .prologue
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/NetworkOperators;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/NetworkOperators;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/NetworkOperators;->dismissProgressBar()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/NetworkOperators;Ljava/lang/String;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/NetworkOperators;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/NetworkOperators;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/NetworkOperators;Ljava/lang/String;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/NetworkOperators;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/NetworkOperators;->loge(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/NetworkOperators;->mPhoneId:I

    .line 120
    new-instance v0, Lcom/android/phone/NetworkOperators$1;

    invoke-direct {v0, p0}, Lcom/android/phone/NetworkOperators$1;-><init>(Lcom/android/phone/NetworkOperators;)V

    iput-object v0, p0, Lcom/android/phone/NetworkOperators;->mHandler:Landroid/os/Handler;

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/NetworkOperators;->mPhoneId:I

    .line 120
    new-instance v0, Lcom/android/phone/NetworkOperators$1;

    invoke-direct {v0, p0}, Lcom/android/phone/NetworkOperators$1;-><init>(Lcom/android/phone/NetworkOperators;)V

    iput-object v0, p0, Lcom/android/phone/NetworkOperators;->mHandler:Landroid/os/Handler;

    .line 65
    return-void
.end method

.method private dismissProgressBar()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 235
    :cond_0
    return-void
.end method

.method private logd(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 251
    const-string/jumbo v0, "NetworkOperators"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[NetworksList] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 255
    const-string/jumbo v0, "NetworkOperators"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[NetworksList] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    return-void
.end method

.method private selectNetworkAutomatic(Z)V
    .locals 4
    .param p1, "autoSelect"    # Z

    .prologue
    .line 205
    iget-object v2, p0, Lcom/android/phone/NetworkOperators;->mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    if-eqz v2, :cond_0

    .line 206
    iget-object v2, p0, Lcom/android/phone/NetworkOperators;->mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Lcom/android/phone/NetworkSelectListPreference;->setEnabled(Z)V

    .line 208
    :cond_0
    if-eqz p1, :cond_2

    .line 209
    const-string/jumbo v2, "select network automatically..."

    invoke-direct {p0, v2}, Lcom/android/phone/NetworkOperators;->logd(Ljava/lang/String;)V

    .line 210
    invoke-direct {p0}, Lcom/android/phone/NetworkOperators;->showAutoSelectProgressBar()V

    .line 211
    iget-object v2, p0, Lcom/android/phone/NetworkOperators;->mAutoSelect:Landroid/preference/TwoStatePreference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/TwoStatePreference;->setEnabled(Z)V

    .line 212
    iget-object v2, p0, Lcom/android/phone/NetworkOperators;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 213
    .local v0, "msg":Landroid/os/Message;
    iget v2, p0, Lcom/android/phone/NetworkOperators;->mPhoneId:I

    invoke-static {v2}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 214
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_1

    .line 215
    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/Phone;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    .line 220
    .end local v0    # "msg":Landroid/os/Message;
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    :goto_0
    return-void

    .line 217
    :cond_2
    iget-object v2, p0, Lcom/android/phone/NetworkOperators;->mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    if-eqz v2, :cond_1

    .line 218
    iget-object v2, p0, Lcom/android/phone/NetworkOperators;->mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    invoke-virtual {v2}, Lcom/android/phone/NetworkSelectListPreference;->onClick()V

    goto :goto_0
.end method

.method private showAutoSelectProgressBar()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 238
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 239
    invoke-virtual {p0}, Lcom/android/phone/NetworkOperators;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0389

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 241
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 242
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 243
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 244
    return-void
.end method


# virtual methods
.method protected displayNetworkSelectionFailed(Ljava/lang/Throwable;)V
    .locals 9
    .param p1, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 171
    if-eqz p1, :cond_1

    instance-of v5, p1, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_1

    .line 172
    check-cast p1, Lcom/android/internal/telephony/CommandException;

    .end local p1    # "ex":Ljava/lang/Throwable;
    invoke-virtual {p1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v5

    .line 173
    sget-object v6, Lcom/android/internal/telephony/CommandException$Error;->ILLEGAL_SIM_OR_ME:Lcom/android/internal/telephony/CommandException$Error;

    .line 172
    if-ne v5, v6, :cond_1

    .line 174
    invoke-virtual {p0}, Lcom/android/phone/NetworkOperators;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0384

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 179
    .local v3, "status":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 180
    .local v0, "app":Lcom/android/phone/PhoneGlobals;
    iget-object v5, v0, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    .line 181
    const/4 v6, 0x2

    .line 180
    invoke-virtual {v5, v6, v3}, Lcom/android/phone/NotificationMgr;->postTransientNotification(ILjava/lang/CharSequence;)V

    .line 183
    const-string/jumbo v5, "phone"

    invoke-virtual {v0, v5}, Lcom/android/phone/PhoneGlobals;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 184
    .local v4, "tm":Landroid/telephony/TelephonyManager;
    iget v5, p0, Lcom/android/phone/NetworkOperators;->mPhoneId:I

    invoke-static {v5}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 185
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_0

    .line 186
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/telephony/TelephonyManager;->getServiceStateForSubscriber(I)Landroid/telephony/ServiceState;

    move-result-object v2

    .line 187
    .local v2, "ss":Landroid/telephony/ServiceState;
    if-eqz v2, :cond_0

    .line 190
    iget-object v5, v0, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getState()I

    move-result v6

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v7

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Lcom/android/phone/NotificationMgr;->updateNetworkSelection(III)V

    .line 193
    .end local v2    # "ss":Landroid/telephony/ServiceState;
    :cond_0
    return-void

    .line 176
    .end local v0    # "app":Lcom/android/phone/PhoneGlobals;
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v3    # "status":Ljava/lang/String;
    .end local v4    # "tm":Landroid/telephony/TelephonyManager;
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/NetworkOperators;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0385

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "status":Ljava/lang/String;
    goto :goto_0
.end method

.method protected displayNetworkSelectionSucceeded()V
    .locals 4

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/android/phone/NetworkOperators;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0386

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 199
    .local v1, "status":Ljava/lang/String;
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 200
    .local v0, "app":Lcom/android/phone/PhoneGlobals;
    iget-object v2, v0, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    .line 201
    const/4 v3, 0x2

    .line 200
    invoke-virtual {v2, v3, v1}, Lcom/android/phone/NotificationMgr;->postTransientNotification(ILjava/lang/CharSequence;)V

    .line 202
    return-void
.end method

.method protected getNetworkSelectionMode()V
    .locals 4

    .prologue
    .line 223
    const-string/jumbo v2, "getting network selection mode..."

    invoke-direct {p0, v2}, Lcom/android/phone/NetworkOperators;->logd(Ljava/lang/String;)V

    .line 224
    iget-object v2, p0, Lcom/android/phone/NetworkOperators;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xc8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 225
    .local v0, "msg":Landroid/os/Message;
    iget v2, p0, Lcom/android/phone/NetworkOperators;->mPhoneId:I

    invoke-static {v2}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 226
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_0

    .line 227
    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/Phone;->getNetworkSelectionMode(Landroid/os/Message;)V

    .line 229
    :cond_0
    return-void
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 76
    const-string/jumbo v0, "button_network_select_key"

    invoke-virtual {p0, v0}, Lcom/android/phone/NetworkOperators;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/phone/NetworkSelectListPreference;

    .line 75
    iput-object v0, p0, Lcom/android/phone/NetworkOperators;->mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    .line 78
    const-string/jumbo v0, "button_auto_select_key"

    invoke-virtual {p0, v0}, Lcom/android/phone/NetworkOperators;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/TwoStatePreference;

    .line 77
    iput-object v0, p0, Lcom/android/phone/NetworkOperators;->mAutoSelect:Landroid/preference/TwoStatePreference;

    .line 79
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/android/phone/NetworkOperators;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 80
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 112
    iget-object v1, p0, Lcom/android/phone/NetworkOperators;->mAutoSelect:Landroid/preference/TwoStatePreference;

    if-ne p1, v1, :cond_0

    .line 113
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 114
    .local v0, "autoSelect":Z
    invoke-direct {p0, v0}, Lcom/android/phone/NetworkOperators;->selectNetworkAutomatic(Z)V

    .line 115
    const/4 v1, 0x1

    return v1

    .line 117
    .end local v0    # "autoSelect":Z
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method protected preferenceTreeClick(Landroid/preference/Preference;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v0, 0x1

    .line 247
    iget-object v1, p0, Lcom/android/phone/NetworkOperators;->mAutoSelect:Landroid/preference/TwoStatePreference;

    if-eq p1, v1, :cond_0

    iget-object v1, p0, Lcom/android/phone/NetworkOperators;->mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected update(ILcom/android/phone/INetworkQueryService;)V
    .locals 3
    .param p1, "subId"    # I
    .param p2, "queryService"    # Lcom/android/phone/INetworkQueryService;

    .prologue
    .line 89
    iput p1, p0, Lcom/android/phone/NetworkOperators;->mSubId:I

    .line 90
    iget v0, p0, Lcom/android/phone/NetworkOperators;->mSubId:I

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v0

    iput v0, p0, Lcom/android/phone/NetworkOperators;->mPhoneId:I

    .line 92
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mAutoSelect:Landroid/preference/TwoStatePreference;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mAutoSelect:Landroid/preference/TwoStatePreference;

    invoke-virtual {v0, p0}, Landroid/preference/TwoStatePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/android/phone/NetworkOperators;->mNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    iget v1, p0, Lcom/android/phone/NetworkOperators;->mSubId:I

    iget-object v2, p0, Lcom/android/phone/NetworkOperators;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1, p2, p0, v2}, Lcom/android/phone/NetworkSelectListPreference;->initialize(ILcom/android/phone/INetworkQueryService;Lcom/android/phone/NetworkOperators;Landroid/app/ProgressDialog;)V

    .line 100
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/NetworkOperators;->getNetworkSelectionMode()V

    .line 101
    return-void
.end method
