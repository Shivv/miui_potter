.class Lcom/android/phone/MiuiErrorDialogActivity$4;
.super Ljava/lang/Object;
.source "MiuiErrorDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/MiuiErrorDialogActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/MiuiErrorDialogActivity;

.field final synthetic val$messageId:I


# direct methods
.method constructor <init>(Lcom/android/phone/MiuiErrorDialogActivity;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/MiuiErrorDialogActivity;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->this$0:Lcom/android/phone/MiuiErrorDialogActivity;

    iput p2, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->val$messageId:I

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 96
    iget v4, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->val$messageId:I

    sparse-switch v4, :sswitch_data_0

    .line 121
    iget-object v4, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->this$0:Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-virtual {v4}, Lcom/android/phone/MiuiErrorDialogActivity;->finish()V

    .line 123
    :goto_0
    return-void

    .line 99
    :sswitch_0
    sget-boolean v4, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v4, :cond_2

    .line 100
    const-class v3, Lcom/android/phone/settings/MultiSimInfoEditorActivity;

    .line 101
    .local v3, "volteActivity":Ljava/lang/Class;
    :goto_1
    iget-object v4, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->this$0:Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/MiuiErrorDialogActivity;->-wrap0(Lcom/android/phone/MiuiErrorDialogActivity;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 102
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 103
    .local v1, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->this$0:Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 105
    sget-boolean v4, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v4, :cond_0

    .line 106
    iget-object v4, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->this$0:Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-virtual {v4}, Lcom/android/phone/MiuiErrorDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "phone_id"

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 107
    .local v2, "phoneId":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 108
    .local v0, "extras":Landroid/os/Bundle;
    invoke-static {v0, v2}, Lmiui/telephony/SubscriptionManager;->putSlotId(Landroid/os/Bundle;I)V

    .line 109
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 111
    .end local v0    # "extras":Landroid/os/Bundle;
    .end local v2    # "phoneId":I
    :cond_0
    iget-object v4, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->this$0:Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-virtual {v4, v1}, Lcom/android/phone/MiuiErrorDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 113
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v4, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->this$0:Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-virtual {v4}, Lcom/android/phone/MiuiErrorDialogActivity;->finish()V

    goto :goto_0

    .line 100
    .end local v3    # "volteActivity":Ljava/lang/Class;
    :cond_2
    const-class v3, Lcom/android/phone/settings/MobileNetworkSettings;

    .restart local v3    # "volteActivity":Ljava/lang/Class;
    goto :goto_1

    .line 118
    .end local v3    # "volteActivity":Ljava/lang/Class;
    :sswitch_1
    iget-object v4, p0, Lcom/android/phone/MiuiErrorDialogActivity$4;->this$0:Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-virtual {v4}, Lcom/android/phone/MiuiErrorDialogActivity;->finish()V

    goto :goto_0

    .line 96
    :sswitch_data_0
    .sparse-switch
        0x7f0b06e6 -> :sswitch_1
        0x7f0b072d -> :sswitch_1
        0x7f0b072e -> :sswitch_0
        0x7f0b072f -> :sswitch_1
        0x7f0b0731 -> :sswitch_0
    .end sparse-switch
.end method
