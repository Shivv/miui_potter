.class public Lcom/android/phone/DefaultSlotSelectorImpl;
.super Ljava/lang/Object;
.source "DefaultSlotSelectorImpl.java"

# interfaces
.implements Lmiui/telephony/DefaultSlotSelector;


# static fields
.field private static sInstance:Lcom/android/phone/DefaultSlotSelectorImpl;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/telephony/DefaultSimManager;->setDefaultSlotSelector(Lmiui/telephony/DefaultSlotSelector;)V

    .line 39
    return-void
.end method

.method public static getDataSlotForCmcc()I
    .locals 8

    .prologue
    .line 85
    sget-boolean v6, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v6, :cond_4

    invoke-static {}, Lmiui/telephony/VirtualSimUtils;->getInstance()Lmiui/telephony/VirtualSimUtils;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/telephony/VirtualSimUtils;->isVirtualSimEnabled()Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_4

    .line 86
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v5

    .line 87
    .local v5, "subs":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    if-nez v5, :cond_0

    .line 88
    sget v6, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    return v6

    .line 90
    :cond_0
    const/4 v0, 0x0

    .line 91
    .local v0, "cmccCardNum":I
    sget v1, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    .line 92
    .local v1, "dataSlot":I
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "s$iterator":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/telephony/SubscriptionInfo;

    .line 93
    .local v2, "s":Lmiui/telephony/SubscriptionInfo;
    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v4

    .line 94
    .local v4, "slotId":I
    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getMcc()I

    move-result v6

    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getMnc()I

    move-result v7

    invoke-static {v6, v7}, Lmiui/telephony/ServiceProviderUtils;->isChinaMobile(II)Z

    move-result v6

    if-nez v6, :cond_2

    .line 95
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v6

    invoke-virtual {v6, v4}, Lmiui/telephony/DefaultSimManager;->getSimImsi(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lmiui/telephony/ServiceProviderUtils;->isChinaMobile(Ljava/lang/String;)Z

    move-result v6

    .line 94
    if-eqz v6, :cond_1

    .line 96
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 97
    move v1, v4

    goto :goto_0

    .line 100
    .end local v2    # "s":Lmiui/telephony/SubscriptionInfo;
    .end local v4    # "slotId":I
    :cond_3
    const/4 v6, 0x1

    if-ne v0, v6, :cond_4

    .line 101
    return v1

    .line 104
    :cond_4
    sget v6, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    return v6
.end method

.method private isAnyCardStateChanged([I)Z
    .locals 4
    .param p1, "cardStates"    # [I

    .prologue
    const/4 v3, 0x0

    .line 52
    if-nez p1, :cond_0

    .line 53
    return v3

    .line 55
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_3

    .line 56
    aget v1, p1, v0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    aget v1, p1, v0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 57
    :cond_1
    const/4 v1, 0x1

    return v1

    .line 55
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_3
    return v3
.end method

.method public static isDdsForceSetForCmcc()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    sget v0, Lcom/android/phone/networkmode/NetworkMode;->sLockNetworkType:I

    if-eq v0, v3, :cond_0

    .line 68
    return v2

    .line 70
    :cond_0
    const-string/jumbo v0, "nikel"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    invoke-static {}, Lcom/android/phone/PhoneAdapter;->isLockNetwork()Z

    move-result v0

    return v0

    .line 72
    :cond_1
    const-string/jumbo v0, "rolex"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    const-string/jumbo v0, "persist.radio.miui_network_lock"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    .line 75
    :cond_2
    return v3
.end method

.method public static make()Lcom/android/phone/DefaultSlotSelectorImpl;
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lcom/android/phone/DefaultSlotSelectorImpl;->sInstance:Lcom/android/phone/DefaultSlotSelectorImpl;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/android/phone/DefaultSlotSelectorImpl;

    invoke-direct {v0}, Lcom/android/phone/DefaultSlotSelectorImpl;-><init>()V

    sput-object v0, Lcom/android/phone/DefaultSlotSelectorImpl;->sInstance:Lcom/android/phone/DefaultSlotSelectorImpl;

    .line 27
    sget-object v0, Lcom/android/phone/DefaultSlotSelectorImpl;->sInstance:Lcom/android/phone/DefaultSlotSelectorImpl;

    return-object v0

    .line 25
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "DefaultSlotSelectorImpl.make() should only be called once"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getDefaultDataSlot([II)I
    .locals 2
    .param p1, "cardStates"    # [I
    .param p2, "systemDefault"    # I

    .prologue
    .line 44
    invoke-static {}, Lcom/android/phone/DefaultSlotSelectorImpl;->isDdsForceSetForCmcc()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/phone/DefaultSlotSelectorImpl;->isAnyCardStateChanged([I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 45
    :cond_0
    invoke-static {}, Lcom/android/phone/DefaultSlotSelectorImpl;->getDataSlotForCmcc()I

    move-result v0

    .line 46
    .local v0, "cmccDataSlot":I
    sget v1, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    if-eq v1, v0, :cond_1

    .end local v0    # "cmccDataSlot":I
    :goto_0
    return v0

    .restart local v0    # "cmccDataSlot":I
    :cond_1
    move v0, p2

    goto :goto_0

    .line 48
    .end local v0    # "cmccDataSlot":I
    :cond_2
    return p2
.end method
