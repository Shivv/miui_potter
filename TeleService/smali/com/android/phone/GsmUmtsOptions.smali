.class public Lcom/android/phone/GsmUmtsOptions;
.super Ljava/lang/Object;
.source "GsmUmtsOptions.java"


# instance fields
.field private mButtonAPNExpand:Landroid/preference/Preference;

.field mCarrierSettingPref:Landroid/preference/Preference;

.field private mCategoryAPNExpand:Landroid/preference/Preference;

.field private mNetworkOperator:Lcom/android/phone/NetworkOperators;

.field private mPrefFragment:Landroid/preference/PreferenceFragment;

.field private mPrefScreen:Landroid/preference/PreferenceScreen;


# direct methods
.method static synthetic -get0(Lcom/android/phone/GsmUmtsOptions;)Landroid/preference/PreferenceFragment;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/GsmUmtsOptions;

    .prologue
    iget-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefFragment:Landroid/preference/PreferenceFragment;

    return-object v0
.end method

.method public constructor <init>(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;ILcom/android/phone/INetworkQueryService;)V
    .locals 2
    .param p1, "prefFragment"    # Landroid/preference/PreferenceFragment;
    .param p2, "prefScreen"    # Landroid/preference/PreferenceScreen;
    .param p3, "subId"    # I
    .param p4, "queryService"    # Lcom/android/phone/INetworkQueryService;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefFragment:Landroid/preference/PreferenceFragment;

    .line 53
    iput-object p2, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    .line 54
    iget-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefFragment:Landroid/preference/PreferenceFragment;

    const v1, 0x7f060015

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceFragment;->addPreferencesFromResource(I)V

    .line 55
    iget-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v1, "button_gsm_apn_key"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mButtonAPNExpand:Landroid/preference/Preference;

    .line 56
    iget-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v1, "category_gsm_apn_key"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mCategoryAPNExpand:Landroid/preference/Preference;

    .line 57
    iget-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    .line 58
    const-string/jumbo v1, "network_operators_category_key"

    .line 57
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/phone/NetworkOperators;

    iput-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mNetworkOperator:Lcom/android/phone/NetworkOperators;

    .line 59
    iget-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v1, "carrier_settings_key"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mCarrierSettingPref:Landroid/preference/Preference;

    .line 61
    iget-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mNetworkOperator:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v0}, Lcom/android/phone/NetworkOperators;->initialize()V

    .line 63
    invoke-virtual {p0, p3, p4}, Lcom/android/phone/GsmUmtsOptions;->update(ILcom/android/phone/INetworkQueryService;)V

    .line 64
    return-void
.end method


# virtual methods
.method protected log(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 154
    const-string/jumbo v0, "GsmUmtsOptions"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    return-void
.end method

.method protected preferenceTreeClick(Landroid/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/android/phone/GsmUmtsOptions;->mNetworkOperator:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v0, p1}, Lcom/android/phone/NetworkOperators;->preferenceTreeClick(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method protected update(ILcom/android/phone/INetworkQueryService;)V
    .locals 7
    .param p1, "subId"    # I
    .param p2, "queryService"    # Lcom/android/phone/INetworkQueryService;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 69
    const/4 v0, 0x1

    .line 70
    .local v0, "addAPNExpand":Z
    const/4 v2, 0x1

    .line 71
    .local v2, "addNetworkOperatorsCategory":Z
    const/4 v1, 0x1

    .line 72
    .local v1, "addCarrierSettings":Z
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v4

    if-eq v4, v6, :cond_0

    .line 73
    const-string/jumbo v4, "Not a GSM phone"

    invoke-virtual {p0, v4}, Lcom/android/phone/GsmUmtsOptions;->log(Ljava/lang/String;)V

    .line 74
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mCategoryAPNExpand:Landroid/preference/Preference;

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 75
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mNetworkOperator:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v4, v5}, Lcom/android/phone/NetworkOperators;->setEnabled(Z)V

    .line 113
    .end local v1    # "addCarrierSettings":Z
    :goto_0
    if-eqz v0, :cond_5

    .line 114
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mButtonAPNExpand:Landroid/preference/Preference;

    .line 115
    new-instance v5, Lcom/android/phone/GsmUmtsOptions$1;

    invoke-direct {v5, p0, p1}, Lcom/android/phone/GsmUmtsOptions$1;-><init>(Lcom/android/phone/GsmUmtsOptions;I)V

    .line 114
    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 129
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsOptions;->mCategoryAPNExpand:Landroid/preference/Preference;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 134
    :goto_1
    if-eqz v2, :cond_6

    .line 135
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsOptions;->mNetworkOperator:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 136
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mNetworkOperator:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v4, p1, p2}, Lcom/android/phone/NetworkOperators;->update(ILcom/android/phone/INetworkQueryService;)V

    .line 141
    :goto_2
    if-eqz v1, :cond_7

    .line 142
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsOptions;->mCarrierSettingPref:Landroid/preference/Preference;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 147
    :goto_3
    return-void

    .line 77
    .restart local v1    # "addCarrierSettings":Z
    :cond_0
    const-string/jumbo v4, "Not a CDMA phone"

    invoke-virtual {p0, v4}, Lcom/android/phone/GsmUmtsOptions;->log(Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v3

    .line 86
    .local v3, "carrierConfig":Landroid/os/PersistableBundle;
    const-string/jumbo v4, "apn_expand_bool"

    invoke-virtual {v3, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 87
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mCategoryAPNExpand:Landroid/preference/Preference;

    if-eqz v4, :cond_1

    .line 88
    const/4 v0, 0x0

    .line 91
    :cond_1
    const-string/jumbo v4, "operator_selection_expand_bool"

    .line 90
    invoke-virtual {v3, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 92
    const/4 v2, 0x0

    .line 95
    :cond_2
    const-string/jumbo v4, "csp_enabled_bool"

    invoke-virtual {v3, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 96
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->isCspPlmnEnabled()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 97
    const-string/jumbo v4, "[CSP] Enabling Operator Selection menu."

    invoke-virtual {p0, v4}, Lcom/android/phone/GsmUmtsOptions;->log(Ljava/lang/String;)V

    .line 98
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mNetworkOperator:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v4, v6}, Lcom/android/phone/NetworkOperators;->setEnabled(Z)V

    .line 107
    :cond_3
    :goto_4
    const-string/jumbo v4, "carrier_settings_enable_bool"

    .line 106
    invoke-virtual {v3, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .local v1, "addCarrierSettings":Z
    goto :goto_0

    .line 100
    .local v1, "addCarrierSettings":Z
    :cond_4
    const-string/jumbo v4, "[CSP] Disabling Operator Selection menu."

    invoke-virtual {p0, v4}, Lcom/android/phone/GsmUmtsOptions;->log(Ljava/lang/String;)V

    .line 101
    const/4 v2, 0x0

    goto :goto_4

    .line 131
    .end local v1    # "addCarrierSettings":Z
    .end local v3    # "carrierConfig":Landroid/os/PersistableBundle;
    :cond_5
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsOptions;->mCategoryAPNExpand:Landroid/preference/Preference;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 138
    :cond_6
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsOptions;->mNetworkOperator:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2

    .line 144
    :cond_7
    iget-object v4, p0, Lcom/android/phone/GsmUmtsOptions;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v5, p0, Lcom/android/phone/GsmUmtsOptions;->mCarrierSettingPref:Landroid/preference/Preference;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3
.end method
