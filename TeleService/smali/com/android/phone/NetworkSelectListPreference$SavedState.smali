.class Lcom/android/phone/NetworkSelectListPreference$SavedState;
.super Landroid/preference/Preference$BaseSavedState;
.source "NetworkSelectListPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/NetworkSelectListPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/NetworkSelectListPreference$SavedState$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/phone/NetworkSelectListPreference$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mDialogListEntries:[Ljava/lang/CharSequence;

.field mDialogListEntryValues:[Ljava/lang/CharSequence;

.field mOperatorInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/OperatorInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 497
    new-instance v0, Lcom/android/phone/NetworkSelectListPreference$SavedState$1;

    invoke-direct {v0}, Lcom/android/phone/NetworkSelectListPreference$SavedState$1;-><init>()V

    .line 496
    sput-object v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 471
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 477
    invoke-direct {p0, p1}, Landroid/preference/Preference$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 478
    const-class v1, Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 479
    .local v0, "boot":Ljava/lang/ClassLoader;
    invoke-virtual {p1}, Landroid/os/Parcel;->readCharSequenceArray()[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntries:[Ljava/lang/CharSequence;

    .line 480
    invoke-virtual {p1}, Landroid/os/Parcel;->readCharSequenceArray()[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntryValues:[Ljava/lang/CharSequence;

    .line 481
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mOperatorInfoList:Ljava/util/List;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->readParcelableList(Ljava/util/List;Ljava/lang/ClassLoader;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mOperatorInfoList:Ljava/util/List;

    .line 482
    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 493
    invoke-direct {p0, p1}, Landroid/preference/Preference$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 494
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 486
    invoke-super {p0, p1, p2}, Landroid/preference/Preference$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 487
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntries:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharSequenceArray([Ljava/lang/CharSequence;)V

    .line 488
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntryValues:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharSequenceArray([Ljava/lang/CharSequence;)V

    .line 489
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mOperatorInfoList:Ljava/util/List;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelableList(Ljava/util/List;I)V

    .line 490
    return-void
.end method
