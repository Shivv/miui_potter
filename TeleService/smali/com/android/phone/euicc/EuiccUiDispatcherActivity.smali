.class public Lcom/android/phone/euicc/EuiccUiDispatcherActivity;
.super Landroid/app/Activity;
.source "EuiccUiDispatcherActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method findBestActivity(Landroid/content/Intent;)Landroid/content/pm/ActivityInfo;
    .locals 1
    .param p1, "euiccUiIntent"    # Landroid/content/Intent;

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/internal/telephony/euicc/EuiccConnector;->findBestActivity(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    return-object v0
.end method

.method protected getEuiccUiIntent()Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 90
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 93
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "android.telephony.euicc.action.MANAGE_EMBEDDED_SUBSCRIPTIONS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    const-string/jumbo v2, "android.service.euicc.action.MANAGE_EMBEDDED_SUBSCRIPTIONS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    :goto_0
    return-object v1

    .line 93
    :cond_0
    const-string/jumbo v2, "android.telephony.euicc.action.PROVISION_EMBEDDED_SUBSCRIPTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 98
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->isDeviceProvisioned()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 99
    const-string/jumbo v2, "EuiccUiDispatcher"

    const-string/jumbo v3, "Cannot perform eUICC provisioning once device is provisioned"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    return-object v5

    .line 102
    :cond_1
    const-string/jumbo v2, "android.service.euicc.action.PROVISION_EMBEDDED_SUBSCRIPTION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string/jumbo v2, "android.telephony.euicc.extra.FORCE_PROVISION"

    .line 105
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "android.telephony.euicc.extra.FORCE_PROVISION"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 103
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 108
    :cond_2
    const-string/jumbo v2, "EuiccUiDispatcher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unsupported action: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return-object v5
.end method

.method isDeviceProvisioned()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 117
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 118
    const-string/jumbo v2, "device_provisioned"

    .line 117
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    :try_start_0
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->resolveEuiccUiIntent()Landroid/content/Intent;

    move-result-object v0

    .line 41
    .local v0, "euiccUiIntent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 42
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->setResult(I)V

    .line 43
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->onDispatchFailure()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->finish()V

    .line 44
    return-void

    .line 47
    :cond_0
    const/high16 v1, 0x2000000

    :try_start_1
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 48
    invoke-virtual {p0, v0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 51
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->finish()V

    .line 53
    return-void

    .line 49
    .end local v0    # "euiccUiIntent":Landroid/content/Intent;
    :catchall_0
    move-exception v1

    .line 51
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->finish()V

    .line 49
    throw v1
.end method

.method protected onDispatchFailure()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method resolveEuiccUiIntent()Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 58
    const-string/jumbo v3, "euicc_service"

    invoke-virtual {p0, v3}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/euicc/EuiccManager;

    .line 59
    .local v1, "euiccManager":Landroid/telephony/euicc/EuiccManager;
    invoke-virtual {v1}, Landroid/telephony/euicc/EuiccManager;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 60
    const-string/jumbo v3, "EuiccUiDispatcher"

    const-string/jumbo v4, "eUICC not enabled"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    return-object v6

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->getEuiccUiIntent()Landroid/content/Intent;

    move-result-object v2

    .line 65
    .local v2, "euiccUiIntent":Landroid/content/Intent;
    if-nez v2, :cond_1

    .line 66
    const-string/jumbo v3, "EuiccUiDispatcher"

    const-string/jumbo v4, "Unable to handle intent"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    return-object v6

    .line 70
    :cond_1
    invoke-virtual {p0, v2}, Lcom/android/phone/euicc/EuiccUiDispatcherActivity;->findBestActivity(Landroid/content/Intent;)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 71
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-nez v0, :cond_2

    .line 72
    const-string/jumbo v3, "EuiccUiDispatcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Could not resolve activity for intent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    return-object v6

    .line 76
    :cond_2
    invoke-virtual {v0}, Landroid/content/pm/ActivityInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 77
    return-object v2
.end method
