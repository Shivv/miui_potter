.class public Lcom/android/phone/MiuiCloudDataManager;
.super Ljava/lang/Object;
.source "MiuiCloudDataManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;
    }
.end annotation


# static fields
.field private static LOG_TAG:Ljava/lang/String;

.field private static sInstance:Lcom/android/phone/MiuiCloudDataManager;


# instance fields
.field private mBaseHandler:Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;

.field private mCloudVolteCarrierConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

.field private mContext:Landroid/content/Context;

.field private mLock:Ljava/lang/Object;


# direct methods
.method static synthetic -wrap0(Lcom/android/phone/MiuiCloudDataManager;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/MiuiCloudDataManager;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/MiuiCloudDataManager;->syncVolteCloudCarrierConfig()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/MiuiCloudDataManager;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MiuiCloudDataManager;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/MiuiCloudDataManager;->initVolteCloudCarrierConfig()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-string/jumbo v0, "MiuiCloudDataManager"

    sput-object v0, Lcom/android/phone/MiuiCloudDataManager;->LOG_TAG:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/android/phone/MiuiCloudDataManager;->sInstance:Lcom/android/phone/MiuiCloudDataManager;

    .line 37
    return-void
.end method

.method private constructor <init>([Landroid/os/PersistableBundle;)V
    .locals 3
    .param p1, "configFromDefaultApp"    # [Landroid/os/PersistableBundle;

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v1, p0, Lcom/android/phone/MiuiCloudDataManager;->mContext:Landroid/content/Context;

    .line 41
    iput-object v1, p0, Lcom/android/phone/MiuiCloudDataManager;->mBaseHandler:Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;

    .line 43
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/phone/MiuiCloudDataManager;->mCloudVolteCarrierConfig:Ljava/util/HashMap;

    .line 46
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/phone/MiuiCloudDataManager;->mLock:Ljava/lang/Object;

    .line 74
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/MiuiCloudDataManager;->mContext:Landroid/content/Context;

    .line 75
    new-instance v0, Landroid/os/HandlerThread;

    sget-object v1, Lcom/android/phone/MiuiCloudDataManager;->LOG_TAG:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 76
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 77
    new-instance v1, Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;-><init>(Lcom/android/phone/MiuiCloudDataManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/phone/MiuiCloudDataManager;->mBaseHandler:Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;

    .line 78
    iput-object p1, p0, Lcom/android/phone/MiuiCloudDataManager;->mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

    .line 79
    iget-object v1, p0, Lcom/android/phone/MiuiCloudDataManager;->mBaseHandler:Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;->sendEmptyMessage(I)Z

    .line 80
    sget-object v1, Lcom/android/phone/MiuiCloudDataManager;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "init done"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return-void
.end method

.method public static getInstance()Lcom/android/phone/MiuiCloudDataManager;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/android/phone/MiuiCloudDataManager;->sInstance:Lcom/android/phone/MiuiCloudDataManager;

    return-object v0
.end method

.method public static init([Landroid/os/PersistableBundle;)Lcom/android/phone/MiuiCloudDataManager;
    .locals 1
    .param p0, "configFromDefaultApp"    # [Landroid/os/PersistableBundle;

    .prologue
    .line 55
    sget-object v0, Lcom/android/phone/MiuiCloudDataManager;->sInstance:Lcom/android/phone/MiuiCloudDataManager;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiCloudDataManager;->supportVolteClouldConfig()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Lcom/android/phone/MiuiCloudDataManager;

    invoke-direct {v0, p0}, Lcom/android/phone/MiuiCloudDataManager;-><init>([Landroid/os/PersistableBundle;)V

    sput-object v0, Lcom/android/phone/MiuiCloudDataManager;->sInstance:Lcom/android/phone/MiuiCloudDataManager;

    .line 58
    :cond_0
    sget-object v0, Lcom/android/phone/MiuiCloudDataManager;->sInstance:Lcom/android/phone/MiuiCloudDataManager;

    return-object v0
.end method

.method private initVolteCloudCarrierConfig()V
    .locals 9

    .prologue
    .line 111
    iget-object v6, p0, Lcom/android/phone/MiuiCloudDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 112
    :try_start_0
    iget-object v5, p0, Lcom/android/phone/MiuiCloudDataManager;->mContext:Landroid/content/Context;

    const-string/jumbo v7, "cloud_data"

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 113
    .local v4, "sp":Landroid/content/SharedPreferences;
    const-string/jumbo v5, "volte_enabled_plmn_list"

    const/4 v7, 0x0

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    .line 114
    .local v1, "clouldEnabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 115
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "plmn$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 116
    .local v2, "plmn":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/phone/MiuiCloudDataManager;->mCloudVolteCarrierConfig:Ljava/util/HashMap;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 111
    .end local v1    # "clouldEnabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v2    # "plmn":Ljava/lang/String;
    .end local v3    # "plmn$iterator":Ljava/util/Iterator;
    .end local v4    # "sp":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5

    .line 120
    .restart local v1    # "clouldEnabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v4    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    :try_start_1
    const-string/jumbo v5, "volte_disabled_plmn_list"

    const/4 v7, 0x0

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 121
    .local v0, "clouldDisabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 122
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "plmn$iterator":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 123
    .restart local v2    # "plmn":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/phone/MiuiCloudDataManager;->mCloudVolteCarrierConfig:Ljava/util/HashMap;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .end local v2    # "plmn":Ljava/lang/String;
    .end local v3    # "plmn$iterator":Ljava/util/Iterator;
    :cond_1
    monitor-exit v6

    .line 127
    return-void
.end method

.method private isValidVolteConfigValue(I)Z
    .locals 2
    .param p1, "value"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 210
    if-eq p1, v1, :cond_0

    .line 211
    if-nez p1, :cond_1

    .line 212
    :cond_0
    return v1

    .line 214
    :cond_1
    return v0
.end method

.method private static supportVolteClouldConfig()Z
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x0

    .line 63
    .local v0, "result":Z
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v1, v2, :cond_0

    .line 64
    const/4 v0, 0x1

    .line 66
    :cond_0
    return v0
.end method

.method private syncVolteCloudCarrierConfig()Z
    .locals 24

    .prologue
    .line 131
    :try_start_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const-string/jumbo v21, "volte_carrier_config"

    invoke-static/range {v20 .. v21}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 132
    .local v10, "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    if-eqz v10, :cond_0

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v20

    if-nez v20, :cond_1

    .line 133
    :cond_0
    const/16 v20, 0x1

    return v20

    .line 136
    :cond_1
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 137
    .local v6, "clouldEnabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 138
    .local v5, "clouldDisabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MiuiCloudDataManager;->mLock:Ljava/lang/Object;

    move-object/from16 v21, v0

    monitor-enter v21
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MiuiCloudDataManager;->mCloudVolteCarrierConfig:Ljava/util/HashMap;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->clear()V

    .line 141
    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "data$iterator":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    .line 142
    .local v8, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    invoke-virtual {v8}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;

    move-result-object v15

    .line 143
    .local v15, "json":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_2

    .line 146
    new-instance v16, Lorg/json/JSONObject;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 147
    .local v16, "jsonObject":Lorg/json/JSONObject;
    const-string/jumbo v20, "plmn"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 148
    .local v17, "plmn":Ljava/lang/String;
    const-string/jumbo v20, "value"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v19

    .line 149
    .local v19, "value":I
    sget-object v20, Lcom/android/phone/MiuiCloudDataManager;->LOG_TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "clould volte carrier config: plmn="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string/jumbo v23, ", value="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_2

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/phone/MiuiCloudDataManager;->isValidVolteConfigValue(I)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 151
    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 152
    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 156
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MiuiCloudDataManager;->mCloudVolteCarrierConfig:Ljava/util/HashMap;

    move-object/from16 v20, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 138
    .end local v8    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v9    # "data$iterator":Ljava/util/Iterator;
    .end local v15    # "json":Ljava/lang/String;
    .end local v16    # "jsonObject":Lorg/json/JSONObject;
    .end local v17    # "plmn":Ljava/lang/String;
    .end local v19    # "value":I
    :catchall_0
    move-exception v20

    :try_start_2
    monitor-exit v21

    throw v20
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 187
    .end local v5    # "clouldDisabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v6    # "clouldEnabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v10    # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    :catch_0
    move-exception v11

    .line 188
    .local v11, "e":Ljava/lang/Exception;
    const/16 v20, 0x0

    return v20

    .line 154
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v5    # "clouldDisabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v6    # "clouldEnabledVoltePlmnList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v8    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .restart local v9    # "data$iterator":Ljava/util/Iterator;
    .restart local v10    # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .restart local v15    # "json":Ljava/lang/String;
    .restart local v16    # "jsonObject":Lorg/json/JSONObject;
    .restart local v17    # "plmn":Ljava/lang/String;
    .restart local v19    # "value":I
    :cond_3
    :try_start_3
    move-object/from16 v0, v17

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .end local v8    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v15    # "json":Ljava/lang/String;
    .end local v16    # "jsonObject":Lorg/json/JSONObject;
    .end local v17    # "plmn":Ljava/lang/String;
    .end local v19    # "value":I
    :cond_4
    :try_start_4
    monitor-exit v21

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MiuiCloudDataManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string/jumbo v21, "cloud_data"

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v18

    .line 161
    .local v18, "sp":Landroid/content/SharedPreferences;
    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    .line 162
    .local v12, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v20, "volte_enabled_plmn_list"

    move-object/from16 v0, v20

    invoke-interface {v12, v0, v6}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 163
    const-string/jumbo v20, "volte_disabled_plmn_list"

    move-object/from16 v0, v20

    invoke-interface {v12, v0, v5}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 164
    invoke-interface {v12}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 166
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    sget v20, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    move/from16 v0, v20

    if-ge v13, v0, :cond_9

    .line 167
    invoke-static {v13}, Lcom/android/phone/PhoneProxy;->getSimOperator(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/phone/MiuiCloudDataManager;->getCloudVolteConfig(Ljava/lang/String;)I

    move-result v7

    .line 168
    .local v7, "config":I
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/phone/MiuiCloudDataManager;->isValidVolteConfigValue(I)Z

    move-result v20

    if-nez v20, :cond_6

    .line 166
    :cond_5
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 171
    :cond_6
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v7, v0, :cond_7

    const/4 v4, 0x1

    .line 172
    .local v4, "cloudValue":Z
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MiuiCloudDataManager;->mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

    move-object/from16 v20, v0

    aget-object v20, v20, v13

    invoke-static/range {v20 .. v20}, Lcom/android/phone/PhoneProxy;->getVolteCarrierConfigValue(Landroid/os/PersistableBundle;)Z

    move-result v3

    .line 173
    .local v3, "baseValue":Z
    if-eq v4, v3, :cond_5

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MiuiCloudDataManager;->mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

    move-object/from16 v20, v0

    aget-object v20, v20, v13

    move-object/from16 v0, v20

    invoke-static {v0, v4}, Lcom/android/phone/PhoneProxy;->setVolteCarrierConfigValue(Landroid/os/PersistableBundle;Z)V

    .line 175
    invoke-static {v13}, Lcom/android/phone/MiuiPhoneUtils;->getIccRecordsLoaded(I)Z

    move-result v14

    .line 176
    .local v14, "isSimReady":Z
    sget-boolean v20, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v20, :cond_8

    .line 177
    if-eqz v14, :cond_5

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MiuiCloudDataManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string/jumbo v21, "LOADED"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v13, v1}, Lcom/android/phone/PhoneProxy;->updateConfigForPhoneId(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_3

    .line 171
    .end local v3    # "baseValue":Z
    .end local v4    # "cloudValue":Z
    .end local v14    # "isSimReady":Z
    :cond_7
    const/4 v4, 0x0

    .restart local v4    # "cloudValue":Z
    goto :goto_4

    .line 181
    .restart local v3    # "baseValue":Z
    .restart local v14    # "isSimReady":Z
    :cond_8
    if-eqz v14, :cond_5

    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v20

    move/from16 v0, v20

    if-ne v13, v0, :cond_5

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MiuiCloudDataManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string/jumbo v21, "LOADED"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v13, v1}, Lcom/android/phone/PhoneProxy;->updateConfigForPhoneId(Landroid/content/Context;ILjava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    .line 190
    .end local v3    # "baseValue":Z
    .end local v4    # "cloudValue":Z
    .end local v7    # "config":I
    .end local v14    # "isSimReady":Z
    :cond_9
    const/16 v20, 0x1

    return v20
.end method


# virtual methods
.method public getCloudVolteConfig(Ljava/lang/String;)I
    .locals 5
    .param p1, "operatorNumeric"    # Ljava/lang/String;

    .prologue
    .line 198
    iget-object v2, p0, Lcom/android/phone/MiuiCloudDataManager;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 199
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/MiuiCloudDataManager;->mCloudVolteCarrierConfig:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    .local v0, "value":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 202
    const/4 v1, -0x1

    monitor-exit v2

    return v1

    .line 204
    :cond_0
    :try_start_1
    sget-object v1, Lcom/android/phone/MiuiCloudDataManager;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getCloudVolteConfig: plmn="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", value="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    monitor-exit v2

    return v1

    .line 198
    .end local v0    # "value":Ljava/lang/Integer;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
