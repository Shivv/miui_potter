.class Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;
.super Ljava/lang/Object;
.source "AutoRecordWhiteListSetting.java"

# interfaces
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/AutoRecordWhiteListSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;


# direct methods
.method private constructor <init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    .line 369
    iput-object p1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    .param p2, "-this1"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;-><init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V

    return-void
.end method

.method private delete(Landroid/view/ActionMode;)V
    .locals 9
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f0b0668

    const/4 v6, 0x0

    .line 407
    iget-object v1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    invoke-static {v1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get1(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Lmiui/widget/EditableListView;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/widget/EditableListView;->getCheckedItemIds()[J

    move-result-object v0

    .line 408
    .local v0, "checkedIds":[J
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 409
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 411
    const v2, 0x1010355

    .line 409
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 412
    iget-object v2, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    invoke-virtual {v2}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    array-length v3, v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    array-length v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const v5, 0x7f100003

    invoke-virtual {v2, v5, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 409
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 413
    new-instance v2, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback$1;-><init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;[JLandroid/view/ActionMode;)V

    .line 409
    invoke-virtual {v1, v7, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 436
    const/high16 v2, 0x1040000

    .line 409
    invoke-virtual {v1, v2, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 439
    :cond_0
    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 386
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 393
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 388
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;->delete(Landroid/view/ActionMode;)V

    goto :goto_0

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 5
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 373
    iget-object v1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    sget v2, Lmiui/R$attr;->actionBarDeleteIcon:I

    invoke-static {v1, v2}, Lmiui/util/AttributeResolver;->resolveDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 374
    .local v0, "delDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x2

    const v2, 0x7f0b0668

    invoke-interface {p2, v3, v1, v3, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 376
    return v4
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 0
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 398
    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 0
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "position"    # I
    .param p3, "id"    # J
    .param p5, "checked"    # Z

    .prologue
    .line 403
    invoke-virtual {p1}, Landroid/view/ActionMode;->invalidate()V

    .line 404
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 381
    const/4 v0, 0x1

    return v0
.end method
