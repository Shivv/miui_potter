.class final Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;
.super Landroid/preference/CheckBoxPreference;
.source "MiuiPhoneAccountSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AccountSwitchPreference"
.end annotation


# instance fields
.field private final mAccount:Landroid/telecom/PhoneAccount;

.field final synthetic this$0:Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;Landroid/content/Context;Landroid/telecom/PhoneAccount;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "account"    # Landroid/telecom/PhoneAccount;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->this$0:Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;

    .line 120
    invoke-direct {p0, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 121
    iput-object p3, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->mAccount:Landroid/telecom/PhoneAccount;

    .line 123
    invoke-virtual {p3}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 124
    invoke-virtual {p3}, Landroid/telecom/PhoneAccount;->getShortDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 125
    invoke-virtual {p3}, Landroid/telecom/PhoneAccount;->getIcon()Landroid/graphics/drawable/Icon;

    move-result-object v0

    .line 126
    .local v0, "icon":Landroid/graphics/drawable/Icon;
    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Icon;->loadDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 129
    :cond_0
    invoke-virtual {p3}, Landroid/telecom/PhoneAccount;->isEnabled()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->setChecked(Z)V

    .line 130
    return-void
.end method


# virtual methods
.method protected onClick()V
    .locals 3

    .prologue
    .line 134
    invoke-super {p0}, Landroid/preference/CheckBoxPreference;->onClick()V

    .line 136
    iget-object v0, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->this$0:Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;

    invoke-static {v0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->-get0(Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;)Landroid/telecom/TelecomManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->mAccount:Landroid/telecom/PhoneAccount;

    invoke-virtual {v1}, Landroid/telecom/PhoneAccount;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/telecom/TelecomManager;->enablePhoneAccount(Landroid/telecom/PhoneAccountHandle;Z)V

    .line 137
    iget-object v0, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;->this$0:Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;

    invoke-static {v0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->-wrap0(Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;)V

    .line 138
    return-void
.end method
