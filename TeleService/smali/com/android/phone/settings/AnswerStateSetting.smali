.class public Lcom/android/phone/settings/AnswerStateSetting;
.super Lmiui/preference/PreferenceActivity;
.source "AnswerStateSetting.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private DBG:Z

.field private mAntiStrange:Landroid/preference/CheckBoxPreference;

.field private mCrescendoMode:Landroid/preference/CheckBoxPreference;

.field private mFlashWhenRing:Landroid/preference/CheckBoxPreference;

.field private mHandonMode:Landroid/preference/CheckBoxPreference;

.field private mProximitySensor:Landroid/preference/CheckBoxPreference;

.field private mTurnOverMuteMode:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 24
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/AnswerStateSetting;->DBG:Z

    .line 21
    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    iget-boolean v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->DBG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "AnswerStateSetting"

    const-string/jumbo v2, "Creating activity"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 39
    .local v0, "bar":Landroid/app/ActionBar;
    if-eqz v0, :cond_1

    .line 40
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 43
    :cond_1
    const v1, 0x7f060001

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AnswerStateSetting;->addPreferencesFromResource(I)V

    .line 44
    const-string/jumbo v1, "button_enable_antispam_strange"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AnswerStateSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mAntiStrange:Landroid/preference/CheckBoxPreference;

    .line 45
    iget-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mAntiStrange:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 47
    const-string/jumbo v1, "button_enable_proximity"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AnswerStateSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mProximitySensor:Landroid/preference/CheckBoxPreference;

    .line 48
    iget-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mProximitySensor:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 50
    const-string/jumbo v1, "button_flash_when_ring"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AnswerStateSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mFlashWhenRing:Landroid/preference/CheckBoxPreference;

    .line 51
    iget-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mFlashWhenRing:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 53
    const-string/jumbo v1, "button_turnover_mute"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AnswerStateSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mTurnOverMuteMode:Landroid/preference/CheckBoxPreference;

    .line 54
    iget-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mTurnOverMuteMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 56
    const-string/jumbo v1, "button_handon_ringer"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AnswerStateSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mHandonMode:Landroid/preference/CheckBoxPreference;

    .line 57
    iget-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mHandonMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 59
    const-string/jumbo v1, "button_crescendo_ringer"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AnswerStateSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mCrescendoMode:Landroid/preference/CheckBoxPreference;

    .line 60
    iget-object v1, p0, Lcom/android/phone/settings/AnswerStateSetting;->mCrescendoMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 61
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 110
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 111
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->finish()V

    .line 113
    const/4 v1, 0x1

    return v1

    .line 115
    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/phone/settings/AnswerStateSetting;->mProximitySensor:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 92
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 91
    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Telephony;->setProximitySensorEnable(Landroid/content/ContentResolver;Z)V

    .line 105
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 93
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/AnswerStateSetting;->mFlashWhenRing:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_2

    .line 94
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "flash_when_ring_enabled"

    .line 95
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 94
    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto :goto_0

    .line 96
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_2
    iget-object v0, p0, Lcom/android/phone/settings/AnswerStateSetting;->mTurnOverMuteMode:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_3

    .line 97
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Telephony;->setTurnOverMuteEnabled(Landroid/content/ContentResolver;Z)V

    goto :goto_0

    .line 98
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_3
    iget-object v0, p0, Lcom/android/phone/settings/AnswerStateSetting;->mHandonMode:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_4

    .line 99
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Telephony;->setHandonRingerEnabled(Landroid/content/ContentResolver;Z)V

    goto :goto_0

    .line 100
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_4
    iget-object v0, p0, Lcom/android/phone/settings/AnswerStateSetting;->mCrescendoMode:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_5

    .line 101
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Telephony;->setCrescendoRingerEnable(Landroid/content/ContentResolver;Z)V

    goto :goto_0

    .line 102
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_5
    iget-object v0, p0, Lcom/android/phone/settings/AnswerStateSetting;->mAntiStrange:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Telephony;->setAntispamStangerEnabled(Landroid/content/ContentResolver;Z)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 9

    .prologue
    .line 65
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 68
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 67
    invoke-static {v6}, Landroid/provider/MiuiSettings$Telephony;->isProximitySensorEnable(Landroid/content/ContentResolver;)Z

    move-result v4

    .line 69
    .local v4, "isProximitySensor":Z
    iget-object v6, p0, Lcom/android/phone/settings/AnswerStateSetting;->mProximitySensor:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 71
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 72
    const-string/jumbo v7, "flash_when_ring_enabled"

    const/4 v8, 0x0

    .line 71
    invoke-static {v6, v7, v8}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    .line 73
    .local v2, "isFlashWhenRingEnable":Z
    iget-object v6, p0, Lcom/android/phone/settings/AnswerStateSetting;->mFlashWhenRing:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 75
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6}, Landroid/provider/MiuiSettings$Telephony;->isTurnOverMuteEnabled(Landroid/content/ContentResolver;)Z

    move-result v5

    .line 76
    .local v5, "isTurnOverMuteMode":Z
    iget-object v6, p0, Lcom/android/phone/settings/AnswerStateSetting;->mTurnOverMuteMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 78
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6}, Landroid/provider/MiuiSettings$Telephony;->isHandonRingerEnabled(Landroid/content/ContentResolver;)Z

    move-result v3

    .line 79
    .local v3, "isHandonMode":Z
    iget-object v6, p0, Lcom/android/phone/settings/AnswerStateSetting;->mHandonMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 81
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6}, Landroid/provider/MiuiSettings$Telephony;->isCrescendoRingerEnable(Landroid/content/ContentResolver;)Z

    move-result v1

    .line 82
    .local v1, "isCrescendoMode":Z
    iget-object v6, p0, Lcom/android/phone/settings/AnswerStateSetting;->mCrescendoMode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 84
    invoke-virtual {p0}, Lcom/android/phone/settings/AnswerStateSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6}, Landroid/provider/MiuiSettings$Telephony;->isAntispamStangerEnabled(Landroid/content/ContentResolver;)Z

    move-result v0

    .line 85
    .local v0, "isAntispam":Z
    iget-object v6, p0, Lcom/android/phone/settings/AnswerStateSetting;->mAntiStrange:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 86
    return-void
.end method
