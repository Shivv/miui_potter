.class Lcom/android/phone/settings/VoicemailSetting$4;
.super Landroid/os/Handler;
.source "VoicemailSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/VoicemailSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/VoicemailSetting;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/VoicemailSetting;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/VoicemailSetting;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    .line 1011
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1014
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    .line 1015
    .local v1, "result":Landroid/os/AsyncResult;
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 1034
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    iget-boolean v2, v2, Lcom/android/phone/settings/VoicemailSetting;->mVMChangeCompletedSuccesfully:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    iget-object v2, v2, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    if-eqz v2, :cond_7

    .line 1035
    :cond_1
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    iget-boolean v2, v2, Lcom/android/phone/settings/VoicemailSetting;->mFwdChangesRequireRollback:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-wrap0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v0

    .line 1036
    :goto_1
    if-eqz v0, :cond_3

    .line 1037
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v2, "All VM reverts done"

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    .line 1038
    :cond_2
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    const/16 v3, 0x643

    invoke-static {v2, v3}, Lcom/android/phone/settings/VoicemailSetting;->-wrap3(Lcom/android/phone/settings/VoicemailSetting;I)V

    .line 1039
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-virtual {v2}, Lcom/android/phone/settings/VoicemailSetting;->onRevertDone()V

    .line 1041
    :cond_3
    return-void

    .line 1017
    :pswitch_0
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    iput-object v1, v2, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    .line 1018
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "VM revert complete msg"

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    goto :goto_0

    .line 1021
    :pswitch_1
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-get1(Lcom/android/phone/settings/VoicemailSetting;)Ljava/util/Map;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1022
    iget-object v2, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_5

    .line 1023
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error in reverting fwd# "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1024
    iget-object v3, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 1023
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    .line 1028
    :cond_4
    :goto_2
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "FWD revert complete msg "

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1026
    :cond_5
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$4;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Success in reverting fwd# "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    goto :goto_2

    .line 1035
    :cond_6
    const/4 v0, 0x1

    .local v0, "done":Z
    goto/16 :goto_1

    .line 1034
    .end local v0    # "done":Z
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "done":Z
    goto/16 :goto_1

    .line 1015
    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
