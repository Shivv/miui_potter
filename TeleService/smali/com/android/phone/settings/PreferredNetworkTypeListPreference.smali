.class public Lcom/android/phone/settings/PreferredNetworkTypeListPreference;
.super Lmiui/preference/PreferenceActivity;
.source "PreferredNetworkTypeListPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/PreferredNetworkTypeListPreference$1;,
        Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;
    }
.end annotation


# instance fields
.field private mCurrentNetworkType:I

.field private mHandler:Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lmiui/preference/RadioButtonPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mSimStateChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mSlotId:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mCurrentNetworkType:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mHandler:Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mSlotId:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mCurrentNetworkType:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->isMtkPlatform()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;Ljava/lang/String;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->checkRadioPreference(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;Landroid/preference/Preference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;
    .param p1, "pref"    # Landroid/preference/Preference;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->getAllRadioPreference(Landroid/preference/Preference;)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;
    .param p1, "networkType"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->handleClick(I)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;Landroid/preference/PreferenceScreen;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;
    .param p1, "prefScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->updatePreferredNetworkTypes(Landroid/preference/PreferenceScreen;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 325
    new-instance v0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$1;-><init>(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)V

    iput-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mSimStateChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 46
    return-void
.end method

.method private checkRadioPreference(Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 139
    iget-object v2, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mItems:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "pref$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreference;

    .line 140
    .local v0, "pref":Lmiui/preference/RadioButtonPreference;
    invoke-virtual {v0}, Lmiui/preference/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lmiui/preference/RadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    .line 142
    .end local v0    # "pref":Lmiui/preference/RadioButtonPreference;
    :cond_0
    return-void
.end method

.method private getAllRadioPreference(Landroid/preference/Preference;)V
    .locals 4
    .param p1, "pref"    # Landroid/preference/Preference;

    .prologue
    .line 125
    if-eqz p1, :cond_0

    .line 126
    instance-of v2, p1, Lmiui/preference/RadioButtonPreference;

    if-eqz v2, :cond_1

    .line 127
    iget-object v3, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mItems:Ljava/util/ArrayList;

    move-object v2, p1

    check-cast v2, Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 136
    :cond_0
    return-void

    .line 129
    :cond_1
    instance-of v2, p1, Landroid/preference/PreferenceGroup;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 130
    check-cast v1, Landroid/preference/PreferenceGroup;

    .line 131
    .local v1, "pg":Landroid/preference/PreferenceGroup;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 132
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->getAllRadioPreference(Landroid/preference/Preference;)V

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private handleClick(I)V
    .locals 4
    .param p1, "networkType"    # I

    .prologue
    .line 201
    const-string/jumbo v0, "PreferedNetworkTypeListPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "handleClick networkType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iput p1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mCurrentNetworkType:I

    .line 203
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->checkRadioPreference(Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    iget v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mCurrentNetworkType:I

    .line 205
    iget-object v2, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mHandler:Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 204
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    .line 206
    return-void
.end method

.method private isH3LTEGlobal()Z
    .locals 2

    .prologue
    .line 352
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_THREE_LTE:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "LTEGLOBAL"

    const-string/jumbo v1, "ro.boot.modem"

    invoke-static {v1}, Lmiui/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isMtkPlatform()Z
    .locals 2

    .prologue
    .line 356
    const-string/jumbo v0, "mediatek"

    const-string/jumbo v1, "vendor"

    invoke-static {v1}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isPcPlatform()Z
    .locals 2

    .prologue
    .line 377
    const-string/jumbo v0, "pinecone"

    const-string/jumbo v1, "vendor"

    invoke-static {v1}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private showEvdoNetworkModes()Z
    .locals 1

    .prologue
    .line 395
    sget-boolean v0, Lmiui/os/Build;->IS_MITHREE_CDMA:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_MIFOUR_CDMA:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_MITWO_CDMA:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private showFddlteNetworkModes()Z
    .locals 1

    .prologue
    .line 381
    sget-boolean v0, Lmiui/os/Build;->IS_MIFOUR_LTE_CU:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_MIFOUR_LTE_CT:Z

    if-nez v0, :cond_0

    .line 382
    sget-boolean v0, Lmiui/os/Build;->IS_MIFOUR_LTE_INDIA:Z

    .line 381
    if-nez v0, :cond_0

    .line 382
    sget-boolean v0, Lmiui/os/Build;->IS_MIFOUR_LTE_SEASA:Z

    .line 381
    if-nez v0, :cond_0

    .line 383
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_THREE_LTE_CU:Z

    .line 381
    if-nez v0, :cond_0

    .line 383
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->isH3LTEGlobal()Z

    move-result v0

    .line 381
    if-nez v0, :cond_0

    .line 384
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_TWOX_CU:Z

    .line 381
    if-nez v0, :cond_0

    .line 384
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_TWOX_CT:Z

    .line 381
    if-nez v0, :cond_0

    .line 385
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_TWOX_SA:Z

    .line 381
    if-nez v0, :cond_0

    .line 385
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_TWOX_BR:Z

    .line 381
    if-nez v0, :cond_0

    .line 386
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_TWOX_IN:Z

    .line 381
    if-nez v0, :cond_0

    .line 387
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_THREEX_CU:Z

    .line 381
    if-nez v0, :cond_0

    .line 387
    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_THREEX_CT:Z

    .line 381
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private showHeamerheadNetworkModes()Z
    .locals 2

    .prologue
    .line 391
    const-string/jumbo v0, "hammerhead"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private showTdlteNetworkModes()Z
    .locals 1

    .prologue
    .line 373
    sget-boolean v0, Lmiui/os/Build;->IS_MIFOUR_LTE_CM:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_HONGMI_THREE_LTE_CM:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isHMH2xCmForThreeMode()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private showTdscdmaNetworkModes()Z
    .locals 1

    .prologue
    .line 369
    sget-boolean v0, Lmiui/os/Build;->IS_MITHREE_TDSCDMA:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_MITWO_TDSCDMA:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private showWcdmaNetworkModes()Z
    .locals 3

    .prologue
    .line 364
    const-string/jumbo v0, "WCDMA"

    const-string/jumbo v1, "persist.radio.modem"

    const-string/jumbo v2, ""

    invoke-static {v1, v2}, Lmiui/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_MIFOUR_CDMA:Z

    if-nez v0, :cond_0

    .line 365
    sget-boolean v0, Lmiui/os/Build;->IS_MITHREE_CDMA:Z

    .line 364
    if-nez v0, :cond_0

    .line 365
    sget-boolean v0, Lmiui/os/Build;->IS_MITWO:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_MITWO_TDSCDMA:Z

    xor-int/lit8 v0, v0, 0x1

    .line 364
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 365
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updatePreferredNetworkTypes(Landroid/preference/PreferenceScreen;)V
    .locals 5
    .param p1, "prefScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    .line 309
    if-eqz p1, :cond_2

    .line 311
    invoke-static {}, Lcom/android/phone/NetworkModeManager;->isDisableLte()Z

    move-result v3

    if-nez v3, :cond_0

    .line 312
    iget v3, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mSlotId:I

    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v4

    if-eq v3, v4, :cond_2

    invoke-static {}, Lcom/android/phone/Dual4GManager;->isDual4GEnabled()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    .line 313
    invoke-static {}, Lcom/android/phone/NetworkModeManager;->shouldShowViceNetworkTypePref()Z

    move-result v3

    .line 311
    if-eqz v3, :cond_2

    .line 314
    :cond_0
    const/4 v3, 0x7

    invoke-static {v3}, Lcom/android/phone/networkmode/NetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v1

    .line 315
    .local v1, "network4g":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_2

    .line 316
    aget v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 317
    .local v2, "preference4GPrefer":Landroid/preference/Preference;
    if-eqz v2, :cond_1

    .line 318
    invoke-virtual {p1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 315
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 323
    .end local v0    # "i":I
    .end local v1    # "network4g":[I
    .end local v2    # "preference4GPrefer":Landroid/preference/Preference;
    :cond_2
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 58
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v5

    invoke-static {v4, v5}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v4

    iput v4, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mSlotId:I

    .line 60
    iget v4, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mSlotId:I

    invoke-static {v4}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iput-object v4, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 61
    new-instance v4, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;

    invoke-direct {v4, p0, v6}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;-><init>(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;)V

    iput-object v4, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mHandler:Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;

    .line 62
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mItems:Ljava/util/ArrayList;

    .line 63
    iget v4, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mSlotId:I

    invoke-static {v4}, Lcom/android/phone/NetworkModeManager;->getPhoneType(I)I

    move-result v2

    .line 64
    .local v2, "phoneType":I
    const-string/jumbo v4, "PreferedNetworkTypeListPreference"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "PreferredNetworkTypeListPreference: phoneType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    .line 66
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->showEvdoNetworkModes()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 68
    const v4, 0x7f06002d

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    .line 109
    :goto_0
    invoke-virtual {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 110
    invoke-virtual {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 111
    .local v3, "prefScreen":Landroid/preference/PreferenceScreen;
    invoke-direct {p0, v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->updatePreferredNetworkTypes(Landroid/preference/PreferenceScreen;)V

    .line 113
    invoke-direct {p0, v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->getAllRadioPreference(Landroid/preference/Preference;)V

    .line 115
    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v4, "android.intent.action.SIM_STATE_CHANGED"

    invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 116
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mSimStateChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 118
    invoke-virtual {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 119
    .local v0, "bar":Lmiui/app/ActionBar;
    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {v0, v7}, Lmiui/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 122
    :cond_0
    return-void

    .line 71
    .end local v0    # "bar":Lmiui/app/ActionBar;
    .end local v1    # "intentFilter":Landroid/content/IntentFilter;
    .end local v3    # "prefScreen":Landroid/preference/PreferenceScreen;
    :cond_1
    const v4, 0x7f060031

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto :goto_0

    .line 73
    :cond_2
    if-ne v2, v7, :cond_c

    .line 74
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->showTdscdmaNetworkModes()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 76
    const v4, 0x7f06002c

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto :goto_0

    .line 77
    :cond_3
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->showWcdmaNetworkModes()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 79
    const v4, 0x7f06002b

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto :goto_0

    .line 80
    :cond_4
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->showTdlteNetworkModes()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 82
    const v4, 0x7f060030

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto :goto_0

    .line 83
    :cond_5
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->showFddlteNetworkModes()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 85
    const v4, 0x7f060032

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto :goto_0

    .line 86
    :cond_6
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->showHeamerheadNetworkModes()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 88
    const v4, 0x7f060033

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto :goto_0

    .line 89
    :cond_7
    sget-boolean v4, Lmiui/os/Build;->IS_HONGMI_TWOX_LC:Z

    if-eqz v4, :cond_8

    .line 91
    const v4, 0x7f06002e

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto :goto_0

    .line 92
    :cond_8
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->isMtkPlatform()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 94
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x17

    if-lt v4, v5, :cond_9

    .line 95
    const v4, 0x7f060035

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto/16 :goto_0

    .line 97
    :cond_9
    const v4, 0x7f060034

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto/16 :goto_0

    .line 99
    :cond_a
    invoke-direct {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->isPcPlatform()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 101
    const v4, 0x7f060036

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto/16 :goto_0

    .line 104
    :cond_b
    const v4, 0x7f06002f

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->addPreferencesFromResource(I)V

    goto/16 :goto_0

    .line 107
    :cond_c
    invoke-virtual {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->finish()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 347
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    .line 348
    iget-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mSimStateChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 349
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 301
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 302
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->finish()V

    .line 305
    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v7, 0x1

    .line 162
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 163
    .local v2, "phones":[Lcom/android/internal/telephony/Phone;
    const/4 v3, 0x0

    array-length v4, v2

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v2, v3

    .line 164
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v5

    sget-object v6, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    if-eq v5, v6, :cond_0

    .line 166
    const v3, 0x7f0b0694

    .line 165
    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 168
    iget v3, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mCurrentNetworkType:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->checkRadioPreference(Ljava/lang/String;)V

    .line 169
    return v7

    .line 163
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 173
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 174
    .local v0, "networkType":I
    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    .line 176
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b058e

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 177
    const v4, 0x7f0b0599

    .line 176
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 178
    new-instance v4, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$2;

    invoke-direct {v4, p0, v0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$2;-><init>(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;I)V

    const v5, 0x104000a

    .line 176
    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 183
    new-instance v4, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$3;

    invoke-direct {v4, p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$3;-><init>(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)V

    const/high16 v5, 0x1040000

    .line 176
    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 188
    new-instance v4, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$4;

    invoke-direct {v4, p0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$4;-><init>(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)V

    .line 176
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 197
    :goto_1
    return v7

    .line 195
    :cond_2
    invoke-direct {p0, v0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->handleClick(I)V

    goto :goto_1
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 155
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 156
    iget-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 157
    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->mHandler:Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 156
    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->getPreferredNetworkType(Landroid/os/Message;)V

    .line 158
    return-void
.end method
