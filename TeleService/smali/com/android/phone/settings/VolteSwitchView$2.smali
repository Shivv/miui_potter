.class Lcom/android/phone/settings/VolteSwitchView$2;
.super Landroid/os/Handler;
.source "VolteSwitchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/VolteSwitchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/VolteSwitchView;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/VolteSwitchView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/VolteSwitchView$2;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    .line 164
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 167
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 175
    :goto_0
    return-void

    .line 169
    :pswitch_0
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView$2;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-virtual {v0}, Lcom/android/phone/settings/VolteSwitchView;->updateVolteButtonUI()V

    goto :goto_0

    .line 172
    :pswitch_1
    invoke-static {}, Lcom/android/phone/settings/VolteSwitchView;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView$2;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v0}, Lcom/android/phone/settings/VolteSwitchView;->-get3(Lcom/android/phone/settings/VolteSwitchView;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->maybeShowTurnOffVolteForCall(Lcom/android/internal/telephony/Phone;)Z

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getDataPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    goto :goto_1

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
