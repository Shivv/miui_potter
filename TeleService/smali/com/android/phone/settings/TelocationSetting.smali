.class public Lcom/android/phone/settings/TelocationSetting;
.super Lmiui/preference/PreferenceActivity;
.source "TelocationSetting.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private DBG:Z

.field private mAutoCountryCode:Landroid/preference/CheckBoxPreference;

.field private mCountryCode:Ljava/lang/String;

.field private mIsAutoAddCountryCode:Z

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mShowLocationEnable:Landroid/preference/CheckBoxPreference;

.field private mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/TelocationSetting;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/TelocationSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mAutoCountryCode:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/TelocationSetting;)Lcom/android/phone/settings/LTREditTextPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/TelocationSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/TelocationSetting;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/TelocationSetting;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/settings/TelocationSetting;->mIsAutoAddCountryCode:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/TelocationSetting;Ljava/lang/String;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/TelocationSetting;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/TelocationSetting;->setCountryCode(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/TelocationSetting;Landroid/preference/Preference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/TelocationSetting;
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/TelocationSetting;->simulatePreferenceClick(Landroid/preference/Preference;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 47
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/TelocationSetting;->DBG:Z

    .line 54
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mCountryCode:Ljava/lang/String;

    .line 44
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getDefaultCountryCode()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 189
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isDcOnlyVirtualSim(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 190
    invoke-static {p0}, Landroid/provider/MiuiSettings$VirtualSim;->getVirtualSimSlotId(Landroid/content/Context;)I

    move-result v2

    .line 191
    .local v2, "slotId":I
    if-nez v2, :cond_0

    const/4 v3, 0x1

    .line 192
    .local v3, "targetSlotId":I
    :goto_0
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lmiui/telephony/TelephonyManager;->getSimOperatorForSlot(I)Ljava/lang/String;

    move-result-object v1

    .line 193
    .local v1, "simOperator":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v6, :cond_1

    .line 194
    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lmiui/telephony/phonenumber/CountryCodeConverter;->getCountryCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 201
    .end local v1    # "simOperator":Ljava/lang/String;
    .end local v2    # "slotId":I
    .end local v3    # "targetSlotId":I
    .local v0, "countryCode":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 191
    .end local v0    # "countryCode":Ljava/lang/String;
    .restart local v2    # "slotId":I
    :cond_0
    const/4 v3, 0x0

    .restart local v3    # "targetSlotId":I
    goto :goto_0

    .line 196
    .restart local v1    # "simOperator":Ljava/lang/String;
    :cond_1
    const-string/jumbo v0, ""

    .restart local v0    # "countryCode":Ljava/lang/String;
    goto :goto_1

    .line 199
    .end local v0    # "countryCode":Ljava/lang/String;
    .end local v1    # "simOperator":Ljava/lang/String;
    .end local v2    # "slotId":I
    .end local v3    # "targetSlotId":I
    :cond_2
    invoke-static {}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getDefaultCountryCode()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "countryCode":Ljava/lang/String;
    goto :goto_1
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 224
    const-string/jumbo v0, "TelocationSetting"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    return-void
.end method

.method private setCountryCode(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 228
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    invoke-direct {p0}, Lcom/android/phone/settings/TelocationSetting;->getDefaultCountryCode()Ljava/lang/String;

    move-result-object p1

    .line 231
    :cond_0
    iput-object p1, p0, Lcom/android/phone/settings/TelocationSetting;->mCountryCode:Ljava/lang/String;

    .line 232
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/provider/MiuiSettings$Telephony;->setContactCountrycode(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 233
    const-string/jumbo v0, "persist.radio.countrycode"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    invoke-virtual {v0, p1}, Lcom/android/phone/settings/LTREditTextPreference;->setText(Ljava/lang/String;)V

    .line 235
    invoke-direct {p0}, Lcom/android/phone/settings/TelocationSetting;->setCountryCodeSummary()V

    .line 236
    iget-boolean v0, p0, Lcom/android/phone/settings/TelocationSetting;->mIsAutoAddCountryCode:Z

    if-eqz v0, :cond_1

    .line 237
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Telephony;->setAutoCountryCodeEnable(Landroid/content/ContentResolver;Z)V

    .line 238
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mAutoCountryCode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/TelocationSetting;->mIsAutoAddCountryCode:Z

    .line 241
    :cond_1
    return-void
.end method

.method private setCountryCodeSummary()V
    .locals 6

    .prologue
    const v5, 0x7f0b0671

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 244
    invoke-static {}, Lcom/android/phone/utils/Utils;->isLanguageOfRTL()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    new-array v1, v1, [Ljava/lang/Object;

    .line 246
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/phone/settings/TelocationSetting;->mCountryCode:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 245
    invoke-virtual {p0, v5, v1}, Lcom/android/phone/settings/TelocationSetting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/LTREditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 251
    :goto_0
    return-void

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    new-array v1, v1, [Ljava/lang/Object;

    .line 249
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/TelocationSetting;->mCountryCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 248
    invoke-virtual {p0, v5, v1}, Lcom/android/phone/settings/TelocationSetting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/LTREditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private simulatePreferenceClick(Landroid/preference/Preference;)V
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    .line 263
    .local v6, "adapter":Landroid/widget/ListAdapter;
    const/4 v3, 0x0

    .local v3, "idx":I
    :goto_0
    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 264
    invoke-interface {v6, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 265
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 266
    invoke-interface {v6, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    const/4 v2, 0x0

    .line 265
    invoke-virtual/range {v0 .. v5}, Landroid/preference/PreferenceScreen;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 270
    :cond_0
    return-void

    .line 263
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 132
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 133
    iget-boolean v3, p0, Lcom/android/phone/settings/TelocationSetting;->DBG:Z

    if-eqz v3, :cond_0

    .line 134
    const-string/jumbo v3, "Creating activity"

    invoke-static {v3}, Lcom/android/phone/settings/TelocationSetting;->log(Ljava/lang/String;)V

    .line 137
    :cond_0
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    iput-object v3, p0, Lcom/android/phone/settings/TelocationSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 138
    const v3, 0x7f060017

    invoke-virtual {p0, v3}, Lcom/android/phone/settings/TelocationSetting;->addPreferencesFromResource(I)V

    .line 140
    const-string/jumbo v3, "button_enable_telocation"

    invoke-virtual {p0, v3}, Lcom/android/phone/settings/TelocationSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/phone/settings/TelocationSetting;->mShowLocationEnable:Landroid/preference/CheckBoxPreference;

    .line 141
    const-string/jumbo v3, "button_auto_country_code"

    invoke-virtual {p0, v3}, Lcom/android/phone/settings/TelocationSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/phone/settings/TelocationSetting;->mAutoCountryCode:Landroid/preference/CheckBoxPreference;

    .line 142
    const-string/jumbo v3, "button_text_telocation_contacts_countrycode"

    invoke-virtual {p0, v3}, Lcom/android/phone/settings/TelocationSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/android/phone/settings/LTREditTextPreference;

    iput-object v3, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    .line 143
    iget-object v3, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    invoke-virtual {v3, v5}, Lcom/android/phone/settings/LTREditTextPreference;->setSummaryLTR(Z)V

    .line 145
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 146
    .local v0, "bar":Lmiui/app/ActionBar;
    if-eqz v0, :cond_1

    .line 147
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lmiui/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 151
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 152
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_3

    .line 154
    const-string/jumbo v3, "automatic_add_country_code"

    .line 153
    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 155
    .local v2, "isAutoAddCountryCode":Z
    if-eqz v2, :cond_2

    .line 157
    const-string/jumbo v3, "voice_roaming_reminder_interval"

    .line 156
    invoke-virtual {p0, v3, v5}, Lcom/android/phone/settings/TelocationSetting;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 159
    const-string/jumbo v4, "has_notification"

    .line 156
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 161
    :cond_2
    if-eqz v2, :cond_3

    .line 162
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3}, Landroid/provider/MiuiSettings$Telephony;->isAutoCountryCodeEnable(Landroid/content/ContentResolver;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 161
    if-eqz v3, :cond_3

    .line 163
    const/16 v3, 0x64

    invoke-virtual {p0, v3}, Lcom/android/phone/settings/TelocationSetting;->showDialog(I)V

    .line 166
    .end local v2    # "isAutoAddCountryCode":Z
    :cond_3
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 284
    packed-switch p1, :pswitch_data_0

    .line 308
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    return-object v1

    .line 286
    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 287
    const v2, 0x7f0b0683

    .line 286
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 288
    const v2, 0x7f0b0684

    .line 286
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 290
    new-instance v2, Lcom/android/phone/settings/TelocationSetting$4;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/TelocationSetting$4;-><init>(Lcom/android/phone/settings/TelocationSetting;)V

    .line 289
    const v3, 0x104000a

    .line 286
    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 304
    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    .line 286
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 305
    .local v0, "dialog":Landroid/app/AlertDialog;
    return-object v0

    .line 284
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 215
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 220
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 217
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->finish()V

    .line 218
    const/4 v0, 0x1

    return v0

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    const v7, 0x7f0b0675

    const v6, 0x104000a

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 73
    iget-object v5, p0, Lcom/android/phone/settings/TelocationSetting;->mShowLocationEnable:Landroid/preference/CheckBoxPreference;

    if-ne p1, v5, :cond_2

    .line 74
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    .line 76
    .local v1, "enable":I
    :goto_0
    iget-object v3, p0, Lcom/android/phone/settings/TelocationSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 77
    const-string/jumbo v5, "enable_telocation"

    .line 75
    invoke-static {v3, v5, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 127
    .end local v1    # "enable":I
    :cond_0
    :goto_1
    return v4

    .line 74
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "enable":I
    goto :goto_0

    .line 79
    .end local v1    # "enable":I
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_2
    iget-object v5, p0, Lcom/android/phone/settings/TelocationSetting;->mAutoCountryCode:Landroid/preference/CheckBoxPreference;

    if-ne p1, v5, :cond_5

    .line 80
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v1, 0x1

    .line 81
    .restart local v1    # "enable":I
    :goto_2
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    if-ne v1, v4, :cond_3

    move v3, v4

    :cond_3
    invoke-static {v5, v3}, Landroid/provider/MiuiSettings$Telephony;->setAutoCountryCodeEnable(Landroid/content/ContentResolver;Z)V

    goto :goto_1

    .line 80
    .end local v1    # "enable":I
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "enable":I
    goto :goto_2

    .line 82
    .end local v1    # "enable":I
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_5
    iget-object v3, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    if-ne p1, v3, :cond_0

    move-object v2, p2

    .line 83
    check-cast v2, Ljava/lang/String;

    .line 84
    .local v2, "inCountryCode":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_a

    .line 85
    const-string/jumbo v3, "+"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 86
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 90
    :cond_6
    :goto_3
    move-object v0, v2

    .line 92
    .local v0, "countryCode":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 93
    invoke-static {v0}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->isValidCountryCode(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 94
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 96
    const v5, 0x7f0b0674

    .line 94
    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 97
    new-instance v5, Lcom/android/phone/settings/TelocationSetting$1;

    invoke-direct {v5, p0, v0}, Lcom/android/phone/settings/TelocationSetting$1;-><init>(Lcom/android/phone/settings/TelocationSetting;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v3, v6, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 103
    new-instance v5, Lcom/android/phone/settings/TelocationSetting$2;

    invoke-direct {v5, p0}, Lcom/android/phone/settings/TelocationSetting$2;-><init>(Lcom/android/phone/settings/TelocationSetting;)V

    const/high16 v6, 0x1040000

    .line 94
    invoke-virtual {v3, v6, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    .line 87
    .end local v0    # "countryCode":Ljava/lang/String;
    :cond_7
    const-string/jumbo v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 88
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 110
    .restart local v0    # "countryCode":Ljava/lang/String;
    :cond_8
    invoke-direct {p0, v0}, Lcom/android/phone/settings/TelocationSetting;->setCountryCode(Ljava/lang/String;)V

    goto :goto_1

    .line 113
    :cond_9
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 115
    const v5, 0x7f0b0676

    .line 113
    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 116
    new-instance v5, Lcom/android/phone/settings/TelocationSetting$3;

    invoke-direct {v5, p0}, Lcom/android/phone/settings/TelocationSetting$3;-><init>(Lcom/android/phone/settings/TelocationSetting;)V

    .line 113
    invoke-virtual {v3, v6, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 124
    .end local v0    # "countryCode":Ljava/lang/String;
    :cond_a
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/android/phone/settings/TelocationSetting;->setCountryCode(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 274
    iget-object v1, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    if-ne p1, v1, :cond_0

    .line 275
    iget-object v1, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/LTREditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 276
    .local v0, "et":Landroid/widget/EditText;
    iget-object v1, p0, Lcom/android/phone/settings/TelocationSetting;->mCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 277
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 279
    .end local v0    # "et":Landroid/widget/EditText;
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 170
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 171
    iget-object v2, p0, Lcom/android/phone/settings/TelocationSetting;->mShowLocationEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 172
    iget-object v2, p0, Lcom/android/phone/settings/TelocationSetting;->mShowLocationEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 173
    const-string/jumbo v4, "enable_telocation"

    .line 172
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 175
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mAutoCountryCode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 176
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mAutoCountryCode:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/MiuiSettings$Telephony;->isAutoCountryCodeEnable(Landroid/content/ContentResolver;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 178
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    invoke-virtual {v0, p0}, Lcom/android/phone/settings/LTREditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 179
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mTextCountryCode:Lcom/android/phone/settings/LTREditTextPreference;

    invoke-virtual {v0, p0}, Lcom/android/phone/settings/LTREditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 180
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Telephony;->getContactCountrycode(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mCountryCode:Ljava/lang/String;

    .line 181
    iget-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mCountryCode:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    invoke-direct {p0}, Lcom/android/phone/settings/TelocationSetting;->getDefaultCountryCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/TelocationSetting;->mCountryCode:Ljava/lang/String;

    .line 184
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/settings/TelocationSetting;->setCountryCodeSummary()V

    .line 185
    return-void

    :cond_1
    move v0, v1

    .line 172
    goto :goto_0
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 206
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onStop()V

    .line 207
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 208
    const-string/jumbo v1, "automatic_add_country_code"

    const/4 v2, 0x0

    .line 207
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/android/phone/settings/TelocationSetting;->finish()V

    .line 211
    :cond_0
    return-void
.end method
