.class public Lcom/android/phone/settings/SimPickerPreference;
.super Lmiui/preference/PreferenceActivity;
.source "SimPickerPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/SimPickerPreference$1;
    }
.end annotation


# static fields
.field private static final BIG_SIM_SLOT_ICON:[I


# instance fields
.field private mForwardIntent:Landroid/content/Intent;

.field private mSubChangedListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;


# direct methods
.method static synthetic -wrap0(Lcom/android/phone/settings/SimPickerPreference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/SimPickerPreference;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/SimPickerPreference;->updateScreen()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const v0, 0x7f020056

    const v1, 0x7f020057

    .line 29
    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/SimPickerPreference;->BIG_SIM_SLOT_ICON:[I

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 35
    new-instance v0, Lcom/android/phone/settings/SimPickerPreference$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/SimPickerPreference$1;-><init>(Lcom/android/phone/settings/SimPickerPreference;)V

    iput-object v0, p0, Lcom/android/phone/settings/SimPickerPreference;->mSubChangedListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    .line 26
    return-void
.end method

.method private createSimInfoPreference(Lmiui/telephony/SubscriptionInfo;)Landroid/preference/Preference;
    .locals 4
    .param p1, "simInfo"    # Lmiui/telephony/SubscriptionInfo;

    .prologue
    const/4 v3, 0x1

    .line 113
    new-instance v1, Lcom/android/phone/settings/LTRValuePreference;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/LTRValuePreference;-><init>(Landroid/content/Context;)V

    .line 114
    .local v1, "simInfoPref":Lmiui/preference/ValuePreference;
    invoke-static {p1}, Lcom/android/phone/MiuiPhoneUtils;->getDisplayName(Lmiui/telephony/SubscriptionInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/preference/ValuePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 115
    invoke-virtual {p1}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/preference/ValuePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {p1}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v2

    invoke-static {v2}, Lcom/android/phone/MiuiPhoneUtils;->isIccCardActivated(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Lmiui/preference/ValuePreference;->setEnabled(Z)V

    .line 117
    invoke-virtual {p1}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v2

    invoke-static {v2, v3}, Lcom/android/phone/MiuiPhoneUtils;->getSimIconResId(IZ)I

    move-result v2

    invoke-virtual {v1, v2}, Lmiui/preference/ValuePreference;->setIcon(I)V

    .line 118
    invoke-virtual {v1, v3}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    .line 119
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/settings/SimPickerPreference;->mForwardIntent:Landroid/content/Intent;

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 120
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x20000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 121
    invoke-virtual {p1}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v2

    invoke-static {v0, v2}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    .line 122
    invoke-virtual {v1, v0}, Lmiui/preference/ValuePreference;->setIntent(Landroid/content/Intent;)V

    .line 124
    return-object v1
.end method

.method public static showSimPicker(Landroid/app/Activity;)Z
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x1

    .line 128
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v1

    .line 129
    .local v1, "simInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v5, :cond_0

    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isDcOnlyVirtualSim(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 130
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 131
    sget v4, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    .line 130
    invoke-static {v3, v4}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v2

    .line 132
    .local v2, "slotId":I
    sget v3, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    if-ne v2, v3, :cond_1

    .line 133
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/android/phone/settings/SimPickerPreference;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "EXTRA_INTENT"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 135
    const-string/jumbo v3, "EXTRA_TITLE"

    invoke-virtual {p0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/phone/MiuiPhoneUtils;->addWindowLable(Landroid/content/Intent;Ljava/lang/CharSequence;)V

    .line 137
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 138
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 139
    return v5

    .line 142
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "slotId":I
    :cond_1
    const/4 v3, 0x0

    return v3
.end method

.method private updateScreen()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 77
    invoke-virtual {p0}, Lcom/android/phone/settings/SimPickerPreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    invoke-virtual {v6}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 78
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v6

    invoke-virtual {v6}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v3

    .line 80
    .local v3, "simInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 81
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/SimPickerPreference;->finish()V

    .line 82
    return-void

    .line 85
    :cond_1
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isDcOnlyVirtualSim(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 86
    invoke-static {p0}, Landroid/provider/MiuiSettings$VirtualSim;->getVirtualSimSlotId(Landroid/content/Context;)I

    move-result v4

    .line 87
    .local v4, "slotId":I
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 88
    .local v2, "simCount":I
    if-ne v2, v7, :cond_3

    .line 96
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/phone/settings/SimPickerPreference;->finish()V

    .line 97
    return-void

    .line 90
    :cond_3
    const/4 v6, 0x2

    if-ne v2, v6, :cond_2

    .line 91
    if-nez v4, :cond_4

    const/4 v5, 0x1

    .line 92
    .local v5, "targetSlotId":I
    :goto_1
    iget-object v6, p0, Lcom/android/phone/settings/SimPickerPreference;->mForwardIntent:Landroid/content/Intent;

    invoke-static {v6, v5}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    .line 93
    iget-object v6, p0, Lcom/android/phone/settings/SimPickerPreference;->mForwardIntent:Landroid/content/Intent;

    const/high16 v7, 0x20000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 94
    iget-object v6, p0, Lcom/android/phone/settings/SimPickerPreference;->mForwardIntent:Landroid/content/Intent;

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/SimPickerPreference;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 91
    .end local v5    # "targetSlotId":I
    :cond_4
    const/4 v5, 0x0

    .restart local v5    # "targetSlotId":I
    goto :goto_1

    .line 100
    .end local v2    # "simCount":I
    .end local v4    # "slotId":I
    .end local v5    # "targetSlotId":I
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v7, :cond_6

    .line 101
    iget-object v7, p0, Lcom/android/phone/settings/SimPickerPreference;->mForwardIntent:Landroid/content/Intent;

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lmiui/telephony/SubscriptionInfo;

    invoke-virtual {v6}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v6

    invoke-static {v7, v6}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    .line 102
    iget-object v6, p0, Lcom/android/phone/settings/SimPickerPreference;->mForwardIntent:Landroid/content/Intent;

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/SimPickerPreference;->startActivity(Landroid/content/Intent;)V

    .line 103
    invoke-virtual {p0}, Lcom/android/phone/settings/SimPickerPreference;->finish()V

    .line 104
    return-void

    .line 107
    :cond_6
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "SubscriptionInfo$iterator":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/telephony/SubscriptionInfo;

    .line 108
    .local v0, "SubscriptionInfo":Lmiui/telephony/SubscriptionInfo;
    invoke-virtual {p0}, Lcom/android/phone/settings/SimPickerPreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    invoke-direct {p0, v0}, Lcom/android/phone/settings/SimPickerPreference;->createSimInfoPreference(Lmiui/telephony/SubscriptionInfo;)Landroid/preference/Preference;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_2

    .line 110
    .end local v0    # "SubscriptionInfo":Lmiui/telephony/SubscriptionInfo;
    :cond_7
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 44
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v2, 0x7f060037

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/SimPickerPreference;->addPreferencesFromResource(I)V

    .line 47
    invoke-virtual {p0}, Lcom/android/phone/settings/SimPickerPreference;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "EXTRA_TITLE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "title":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 49
    invoke-virtual {p0, v1}, Lcom/android/phone/settings/SimPickerPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/SimPickerPreference;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "EXTRA_INTENT"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    iput-object v2, p0, Lcom/android/phone/settings/SimPickerPreference;->mForwardIntent:Landroid/content/Intent;

    .line 52
    iget-object v2, p0, Lcom/android/phone/settings/SimPickerPreference;->mForwardIntent:Landroid/content/Intent;

    if-nez v2, :cond_1

    .line 53
    invoke-virtual {p0}, Lcom/android/phone/settings/SimPickerPreference;->finish()V

    .line 54
    return-void

    .line 57
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/SimPickerPreference;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 58
    .local v0, "actionBar":Lmiui/app/ActionBar;
    if-eqz v0, :cond_2

    .line 59
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmiui/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 61
    :cond_2
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onPause()V

    .line 73
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/SimPickerPreference;->mSubChangedListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Lmiui/telephony/SubscriptionManager;->removeOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 74
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 66
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/SimPickerPreference;->mSubChangedListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Lmiui/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 67
    invoke-direct {p0}, Lcom/android/phone/settings/SimPickerPreference;->updateScreen()V

    .line 68
    return-void
.end method
