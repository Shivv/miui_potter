.class public Lcom/android/phone/settings/NetworkSetting;
.super Lmiui/preference/PreferenceActivity;
.source "NetworkSetting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/NetworkSetting$1;,
        Lcom/android/phone/settings/NetworkSetting$2;,
        Lcom/android/phone/settings/NetworkSetting$3;,
        Lcom/android/phone/settings/NetworkSetting$4;,
        Lcom/android/phone/settings/NetworkSetting$5;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAlertDialogListener:Landroid/content/DialogInterface$OnClickListener;

.field private mAutoSelect:Landroid/preference/CheckBoxPreference;

.field private mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

.field private final mHandler:Landroid/os/Handler;

.field protected mIsForeground:Z

.field private mNeedDataReEnable:Z

.field private mNetworkList:Landroid/preference/PreferenceGroup;

.field private mNetworkMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/preference/Preference;",
            "Lcom/android/internal/telephony/OperatorInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkQueryFailedDialog:Landroid/app/AlertDialog;

.field private mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

.field private final mNetworkQueryServiceConnection:Landroid/content/ServiceConnection;

.field mNetworkSelectMsg:Ljava/lang/String;

.field mPhone:Lcom/android/internal/telephony/Phone;

.field private mSearchButton:Landroid/preference/Preference;

.field private mSlotId:I

.field private mTryAutoSelectNetwork:Z

.field private mTurnOffAutoSelectConfirmDialog:Landroid/app/AlertDialog;

.field private mTurnOffAutoSelectWarningDialog:Landroid/app/AlertDialog;


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/phone/settings/NetworkSetting;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/NetworkSetting;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/NetworkSetting;)Landroid/os/Handler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/NetworkSetting;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/NetworkSetting;->mSlotId:I

    return v0
.end method

.method static synthetic -get4(Lcom/android/phone/settings/NetworkSetting;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mTryAutoSelectNetwork:Z

    return v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/NetworkSetting;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/settings/NetworkSetting;->mNeedDataReEnable:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/phone/settings/NetworkSetting;Lcom/android/phone/INetworkQueryService;)Lcom/android/phone/INetworkQueryService;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;
    .param p1, "-value"    # Lcom/android/phone/INetworkQueryService;

    .prologue
    iput-object p1, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    return-object p1
.end method

.method static synthetic -set2(Lcom/android/phone/settings/NetworkSetting;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/settings/NetworkSetting;->mTryAutoSelectNetwork:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/NetworkSetting;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;
    .param p1, "ex"    # Ljava/lang/Throwable;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/NetworkSetting;->displayNetworkSelectionFailed(Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/NetworkSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->displayNetworkSelectionSucceeded()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/NetworkSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->doDataReEnable()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/phone/settings/NetworkSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->loadNetworksList()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/phone/settings/NetworkSetting;Ljava/util/List;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;
    .param p1, "result"    # Ljava/util/List;
    .param p2, "status"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/settings/NetworkSetting;->networksListLoaded(Ljava/util/List;I)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/phone/settings/NetworkSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->selectNetworkAutomatic()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/phone/settings/NetworkSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->startNetworkQuery()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    const-class v0, Lcom/android/phone/settings/NetworkSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/NetworkSetting;->LOG_TAG:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 134
    iput-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mIsForeground:Z

    .line 135
    iput-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNeedDataReEnable:Z

    .line 146
    new-instance v0, Lcom/android/phone/settings/NetworkSetting$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/NetworkSetting$1;-><init>(Lcom/android/phone/settings/NetworkSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mHandler:Landroid/os/Handler;

    .line 227
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    .line 230
    new-instance v0, Lcom/android/phone/settings/NetworkSetting$2;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/NetworkSetting$2;-><init>(Lcom/android/phone/settings/NetworkSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryServiceConnection:Landroid/content/ServiceConnection;

    .line 251
    new-instance v0, Lcom/android/phone/settings/NetworkSetting$3;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/NetworkSetting$3;-><init>(Lcom/android/phone/settings/NetworkSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    .line 538
    new-instance v0, Lcom/android/phone/settings/NetworkSetting$4;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/NetworkSetting$4;-><init>(Lcom/android/phone/settings/NetworkSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 634
    new-instance v0, Lcom/android/phone/settings/NetworkSetting$5;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/NetworkSetting$5;-><init>(Lcom/android/phone/settings/NetworkSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mAlertDialogListener:Landroid/content/DialogInterface$OnClickListener;

    .line 87
    return-void
.end method

.method private checkDataConnection()V
    .locals 3

    .prologue
    .line 598
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v1

    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    if-ne v1, v2, :cond_1

    .line 599
    const-string/jumbo v1, "connectivity"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/NetworkSetting;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 600
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 601
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->loadNetworksList()V

    .line 608
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    :goto_0
    return-void

    .line 603
    .restart local v0    # "cm":Landroid/net/ConnectivityManager;
    :cond_0
    const/16 v1, 0x320

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/NetworkSetting;->showDialog(I)V

    goto :goto_0

    .line 606
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    :cond_1
    const/16 v1, 0x2bc

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/NetworkSetting;->showDialog(I)V

    goto :goto_0
.end method

.method private clearList()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 675
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkList:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 677
    iput-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    .line 680
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 681
    return-void
.end method

.method private displayEmptyNetworkList(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 652
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkList:Landroid/preference/PreferenceGroup;

    if-eqz p1, :cond_0

    const v0, 0x7f0b0381

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->setTitle(I)V

    .line 653
    return-void

    .line 652
    :cond_0
    const v0, 0x7f0b037f

    goto :goto_0
.end method

.method private displayNetworkQueryFailed(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 671
    const/16 v0, 0x258

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->showDialogSafely(I)V

    .line 672
    return-void
.end method

.method private displayNetworkSelectionFailed(Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "ex"    # Ljava/lang/Throwable;

    .prologue
    const/4 v3, 0x0

    .line 686
    if-eqz p1, :cond_1

    instance-of v1, p1, Lcom/android/internal/telephony/CommandException;

    if-eqz v1, :cond_1

    .line 687
    check-cast p1, Lcom/android/internal/telephony/CommandException;

    .end local p1    # "ex":Ljava/lang/Throwable;
    invoke-virtual {p1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v1

    .line 688
    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->ILLEGAL_SIM_OR_ME:Lcom/android/internal/telephony/CommandException$Error;

    .line 687
    if-ne v1, v2, :cond_1

    .line 689
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0384

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 694
    .local v0, "status":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->NotifyNetworkSelection(Ljava/lang/String;)V

    .line 696
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    if-eqz v1, :cond_0

    .line 697
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v1, v3}, Lmiui/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroid/preference/Preference;)V

    .line 699
    :cond_0
    return-void

    .line 691
    .end local v0    # "status":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0385

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "status":Ljava/lang/String;
    goto :goto_0
.end method

.method private displayNetworkSelectionSucceeded()V
    .locals 3

    .prologue
    .line 702
    iget-boolean v1, p0, Lcom/android/phone/settings/NetworkSetting;->mTryAutoSelectNetwork:Z

    if-nez v1, :cond_0

    .line 703
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0386

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 704
    .local v0, "status":Ljava/lang/String;
    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->NotifyNetworkSelection(Ljava/lang/String;)V

    .line 706
    .end local v0    # "status":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private displayNetworkSeletionInProgress(Ljava/lang/String;)V
    .locals 3
    .param p1, "networkStr"    # Ljava/lang/String;

    .prologue
    .line 657
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const v2, 0x7f0b0383

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkSelectMsg:Ljava/lang/String;

    .line 659
    iget-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mIsForeground:Z

    if-eqz v0, :cond_0

    .line 660
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->showDialog(I)V

    .line 662
    :cond_0
    return-void
.end method

.method private doDataReEnable()V
    .locals 2

    .prologue
    .line 611
    iget-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNeedDataReEnable:Z

    if-eqz v0, :cond_0

    .line 612
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 613
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNeedDataReEnable:Z

    .line 615
    :cond_0
    return-void
.end method

.method private getNetworkTitle(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;
    .locals 4
    .param p1, "ni"    # Lcom/android/internal/telephony/OperatorInfo;

    .prologue
    .line 852
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v0

    .line 854
    .local v0, "longName":Ljava/lang/String;
    const-string/jumbo v2, "46605"

    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "466 05"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 855
    const-string/jumbo v2, "Gt"

    return-object v2

    .line 858
    :cond_0
    const-string/jumbo v2, "46697"

    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v2, "GT 4G"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "GT 4G R"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 859
    :cond_1
    const-string/jumbo v2, "Gt"

    return-object v2

    .line 861
    :cond_2
    const-string/jumbo v1, ""

    .line 862
    .local v1, "networkTitle":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 863
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v1

    .line 870
    :goto_0
    return-object v1

    .line 864
    :cond_3
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaShort()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 865
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaShort()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 867
    :cond_4
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getRadioTechText(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "radioTech"    # Ljava/lang/String;

    .prologue
    .line 874
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 875
    const-string/jumbo v3, ""

    return-object v3

    .line 878
    :cond_0
    const-string/jumbo v2, " 2G"

    .line 880
    .local v2, "radioText":Ljava/lang/String;
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 881
    .local v1, "radio":I
    const/4 v3, 0x2

    if-le v1, v3, :cond_1

    .line 882
    const/16 v3, 0xe

    if-ne v1, v3, :cond_3

    .line 883
    const-string/jumbo v3, "PL"

    invoke-static {v3}, Lmiui/os/Build;->checkRegion(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 884
    const-string/jumbo v2, " LTE"

    .line 895
    .end local v1    # "radio":I
    :cond_1
    :goto_0
    return-object v2

    .line 886
    .restart local v1    # "radio":I
    :cond_2
    const-string/jumbo v2, " 4G"

    goto :goto_0

    .line 889
    :cond_3
    const-string/jumbo v2, " 3G"
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 892
    .end local v1    # "radio":I
    :catch_0
    move-exception v0

    .line 893
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "parse radio tech error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/phone/settings/NetworkSetting;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private loadNetworksList()V
    .locals 4

    .prologue
    .line 711
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/telephony/IccCard;->hasIccCard()Z

    move-result v0

    if-nez v0, :cond_0

    .line 712
    sget-object v0, Lcom/android/phone/settings/NetworkSetting;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "loadNetworksList: SIM card absent, return."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->displayEmptyNetworkList(Z)V

    .line 714
    return-void

    .line 717
    :cond_0
    iget-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mIsForeground:Z

    if-eqz v0, :cond_1

    .line 718
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->showDialog(I)V

    .line 721
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    const/16 v1, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 723
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->displayEmptyNetworkList(Z)V

    .line 724
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 914
    sget-object v0, Lcom/android/phone/settings/NetworkSetting;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[NetworksList] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    return-void
.end method

.method private networksListLoaded(Ljava/util/List;I)V
    .locals 5
    .param p2, "status"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/OperatorInfo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 746
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/OperatorInfo;>;"
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    const/4 v2, 0x1

    .line 749
    .local v2, "resultValid":Z
    :goto_0
    if-nez p2, :cond_0

    if-eqz v2, :cond_1

    .line 750
    :cond_0
    const/16 v3, 0xc8

    :try_start_0
    invoke-virtual {p0, v3}, Lcom/android/phone/settings/NetworkSetting;->removeDialog(I)V

    .line 754
    :cond_1
    iget-object v3, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    invoke-interface {v3, v4}, Lcom/android/phone/INetworkQueryService;->stopNetworkQuery(Lcom/android/phone/INetworkQueryServiceCallback;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 761
    :goto_1
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->doDataReEnable()V

    .line 763
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 764
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->clearList()V

    .line 766
    if-eqz p2, :cond_3

    .line 768
    invoke-direct {p0, p2}, Lcom/android/phone/settings/NetworkSetting;->displayNetworkQueryFailed(I)V

    .line 776
    :goto_2
    return-void

    .line 746
    .end local v2    # "resultValid":Z
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "resultValid":Z
    goto :goto_0

    .line 770
    :cond_3
    if-eqz v2, :cond_4

    .line 771
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/NetworkSetting;->createList(Ljava/util/List;)V

    goto :goto_2

    .line 773
    :cond_4
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->loadNetworksList()V

    goto :goto_2

    .line 755
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_1

    .line 757
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .local v0, "e":Landroid/os/RemoteException;
    goto :goto_1
.end method

.method private selectNetworkAutomatic()V
    .locals 5

    .prologue
    const/16 v2, 0x12c

    const/4 v4, 0x1

    .line 900
    iget-boolean v1, p0, Lcom/android/phone/settings/NetworkSetting;->mIsForeground:Z

    if-eqz v1, :cond_0

    .line 901
    invoke-virtual {p0, v2}, Lcom/android/phone/settings/NetworkSetting;->showDialog(I)V

    .line 904
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 905
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/Phone;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    .line 907
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->clearList()V

    .line 908
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/NetworkSetting;->getAutoSelectionSummary(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setSummaryOn(Ljava/lang/CharSequence;)V

    .line 909
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 910
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "button_auto_select_key"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/phone/settings/NetworkSetting;->mSlotId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 911
    return-void
.end method

.method private startNetworkQuery()V
    .locals 4

    .prologue
    .line 729
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    iget-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    iget v3, p0, Lcom/android/phone/settings/NetworkSetting;->mSlotId:I

    invoke-static {v1, v2, v3}, Lcom/android/phone/NetworkQueryServiceAdapter;->startNetworkQuery(Lcom/android/phone/INetworkQueryService;Lcom/android/phone/INetworkQueryServiceCallback;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 733
    :goto_0
    return-void

    .line 730
    :catch_0
    move-exception v0

    .line 731
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/android/phone/settings/NetworkSetting;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startNetworkQuery error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected createList(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/OperatorInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/OperatorInfo;>;"
    const v10, 0x7f0b058a

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 779
    new-instance v6, Lmiui/preference/RadioButtonPreferenceCategory;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lmiui/preference/RadioButtonPreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v6, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    .line 780
    iget-object v6, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    const v7, 0x7f0b037f

    invoke-virtual {v6, v7}, Lmiui/preference/RadioButtonPreferenceCategory;->setTitle(I)V

    .line 781
    iget-object v6, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkList:Landroid/preference/PreferenceGroup;

    iget-object v7, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 783
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "ni$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/telephony/OperatorInfo;

    .line 785
    .local v2, "ni":Lcom/android/internal/telephony/OperatorInfo;
    invoke-virtual {v2}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "46020"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 788
    new-instance v0, Lmiui/preference/RadioButtonPreference;

    invoke-direct {v0, p0}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    .line 790
    .local v0, "carrier":Lmiui/preference/RadioButtonPreference;
    invoke-virtual {v2}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/android/phone/settings/NetworkSetting;->getNetworkSpn(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 791
    .local v4, "spn":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/phone/settings/NetworkSetting;->getNetworkTitle(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;

    move-result-object v1

    .line 792
    .local v1, "networkTitle":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/android/internal/telephony/OperatorInfo;->getState()Lcom/android/internal/telephony/OperatorInfo$State;

    move-result-object v5

    .line 794
    .local v5, "state":Lcom/android/internal/telephony/OperatorInfo$State;
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/settings/NetworkSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v7

    invoke-virtual {v6, v7}, Lmiui/telephony/TelephonyManager;->getSimOperatorForSlot(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lmiui/telephony/ServiceProviderUtils;->isChinaTelecom(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 795
    invoke-virtual {v2}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lmiui/telephony/ServiceProviderUtils;->isChinaTelecom(Ljava/lang/String;)Z

    move-result v6

    .line 794
    if-eqz v6, :cond_1

    .line 796
    sget-object v5, Lcom/android/internal/telephony/OperatorInfo$State;->FORBIDDEN:Lcom/android/internal/telephony/OperatorInfo$State;

    .line 798
    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 799
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2}, Lcom/android/phone/PhoneAdapter;->getRadioTech(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/phone/settings/NetworkSetting;->getRadioTechText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lmiui/preference/RadioButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 800
    sget-object v6, Lcom/android/internal/telephony/OperatorInfo$State;->FORBIDDEN:Lcom/android/internal/telephony/OperatorInfo$State;

    if-ne v5, v6, :cond_2

    .line 801
    new-array v6, v9, [Ljava/lang/Object;

    aput-object v1, v6, v8

    invoke-virtual {p0, v10, v6}, Lcom/android/phone/settings/NetworkSetting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 803
    :cond_2
    invoke-virtual {v0, v1}, Lmiui/preference/RadioButtonPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 811
    :goto_1
    sget-boolean v6, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-eqz v6, :cond_3

    sget-object v6, Lcom/android/internal/telephony/OperatorInfo$State;->FORBIDDEN:Lcom/android/internal/telephony/OperatorInfo$State;

    if-ne v5, v6, :cond_3

    .line 812
    invoke-virtual {v0, v8}, Lmiui/preference/RadioButtonPreference;->setEnabled(Z)V

    .line 814
    :cond_3
    invoke-virtual {v0, v8}, Lmiui/preference/RadioButtonPreference;->setPersistent(Z)V

    .line 816
    invoke-virtual {v2}, Lcom/android/internal/telephony/OperatorInfo;->getState()Lcom/android/internal/telephony/OperatorInfo$State;

    move-result-object v6

    sget-object v7, Lcom/android/internal/telephony/OperatorInfo$State;->CURRENT:Lcom/android/internal/telephony/OperatorInfo$State;

    if-ne v6, v7, :cond_4

    .line 817
    invoke-virtual {v0, v9}, Lmiui/preference/RadioButtonPreference;->setChecked(Z)V

    .line 820
    :cond_4
    iget-object v6, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v6, v0}, Lmiui/preference/RadioButtonPreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 821
    iget-object v6, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {v6, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 805
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2}, Lcom/android/phone/PhoneAdapter;->getRadioTech(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/phone/settings/NetworkSetting;->getRadioTechText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 806
    sget-object v6, Lcom/android/internal/telephony/OperatorInfo$State;->FORBIDDEN:Lcom/android/internal/telephony/OperatorInfo$State;

    if-ne v5, v6, :cond_6

    .line 807
    new-array v6, v9, [Ljava/lang/Object;

    aput-object v1, v6, v8

    invoke-virtual {p0, v10, v6}, Lcom/android/phone/settings/NetworkSetting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 809
    :cond_6
    invoke-virtual {v0, v1}, Lmiui/preference/RadioButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 826
    .end local v0    # "carrier":Lmiui/preference/RadioButtonPreference;
    .end local v1    # "networkTitle":Ljava/lang/String;
    .end local v2    # "ni":Lcom/android/internal/telephony/OperatorInfo;
    .end local v4    # "spn":Ljava/lang/String;
    .end local v5    # "state":Lcom/android/internal/telephony/OperatorInfo$State;
    :cond_7
    return-void
.end method

.method protected getAutoSelectionSummary(Z)Ljava/lang/String;
    .locals 5
    .param p1, "flag"    # Z

    .prologue
    const v4, 0x7f0b072a

    const/4 v3, 0x0

    .line 361
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 362
    .local v1, "resource":Landroid/content/res/Resources;
    if-eqz p1, :cond_2

    .line 363
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    .line 364
    .local v0, "operatorName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 365
    iget-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getState()I

    move-result v2

    if-eqz v2, :cond_1

    .line 366
    :cond_0
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 368
    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    const v3, 0x7f0b0587

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 370
    .end local v0    # "operatorName":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected getNetworkAutoSelectionMode()Z
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getIsManualSelection()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected getNetworkOperatorName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 375
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    iget v1, p0, Lcom/android/phone/settings/NetworkSetting;->mSlotId:I

    invoke-virtual {v0, v1}, Lmiui/telephony/TelephonyManager;->getNetworkOperatorNameForSlot(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNetworkSpn(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "operatorNumberic"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 829
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 830
    const-string/jumbo v1, ""

    return-object v1

    .line 832
    :cond_0
    const/16 v1, 0x2c

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 833
    .local v0, "end":I
    if-lez v0, :cond_1

    .line 834
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 837
    :cond_1
    const-string/jumbo v1, "46697"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "GT 4G"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "GT 4G R"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 838
    :cond_2
    const-string/jumbo v1, "Gt"

    return-object v1

    .line 840
    :cond_3
    invoke-static {p1, p2}, Lmiui/telephony/ServiceProviderUtils;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method isNetworkListChecked()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 411
    sget-object v3, Lcom/android/phone/settings/NetworkSetting;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "isNetworkListChecked: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    iget-object v3, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    if-nez v3, :cond_0

    .line 413
    return v6

    .line 414
    :cond_0
    iget-object v3, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v3}, Lmiui/preference/RadioButtonPreferenceCategory;->getPreferenceCount()I

    move-result v0

    .line 415
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 416
    iget-object v3, p0, Lcom/android/phone/settings/NetworkSetting;->mAvailableNetworkList:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v3, v1}, Lmiui/preference/RadioButtonPreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lmiui/preference/RadioButtonPreference;

    .line 417
    .local v2, "pref":Lmiui/preference/RadioButtonPreference;
    sget-object v3, Lcom/android/phone/settings/NetworkSetting;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "isChecked():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lmiui/preference/RadioButtonPreference;->isChecked()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-virtual {v2}, Lmiui/preference/RadioButtonPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    return v6

    .line 415
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 420
    .end local v2    # "pref":Lmiui/preference/RadioButtonPreference;
    :cond_2
    const/4 v3, 0x0

    return v3
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 402
    sget-object v0, Lcom/android/phone/settings/NetworkSetting;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "onBackPressed()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->isNetworkListChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 404
    const/16 v0, 0x384

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->showDialogSafely(I)V

    .line 405
    return-void

    .line 407
    :cond_0
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onBackPressed()V

    .line 408
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 296
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    iget-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    invoke-interface {v1, v2}, Lcom/android/phone/INetworkQueryService;->stopNetworkQuery(Lcom/android/phone/INetworkQueryServiceCallback;)V

    .line 297
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->doDataReEnable()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->finish()V

    .line 302
    return-void

    .line 298
    :catch_0
    move-exception v0

    .line 299
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 619
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mTurnOffAutoSelectWarningDialog:Landroid/app/AlertDialog;

    if-ne p1, v0, :cond_1

    .line 620
    const/16 v0, 0x190

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->dismissDialog(I)V

    .line 621
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->showDialog(I)V

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mTurnOffAutoSelectConfirmDialog:Landroid/app/AlertDialog;

    if-ne p1, v0, :cond_2

    .line 623
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 624
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->checkDataConnection()V

    goto :goto_0

    .line 625
    :cond_2
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryFailedDialog:Landroid/app/AlertDialog;

    if-ne p1, v0, :cond_0

    .line 626
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 627
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->checkDataConnection()V

    goto :goto_0

    .line 629
    :cond_3
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 313
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 314
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 315
    const v4, 0x7f06001a

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/NetworkSetting;->addPreferencesFromResource(I)V

    .line 317
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 318
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v5

    .line 317
    invoke-static {v4, v5}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v4

    iput v4, p0, Lcom/android/phone/settings/NetworkSetting;->mSlotId:I

    .line 319
    iget v4, p0, Lcom/android/phone/settings/NetworkSetting;->mSlotId:I

    invoke-static {v4}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iput-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 321
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/android/phone/NetworkQueryService;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 322
    .local v2, "intent":Landroid/content/Intent;
    iget v4, p0, Lcom/android/phone/settings/NetworkSetting;->mSlotId:I

    invoke-static {v2, v4}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    .line 323
    const-string/jumbo v4, "subscription"

    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 325
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const-string/jumbo v5, "list_networks_key"

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/PreferenceGroup;

    iput-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkList:Landroid/preference/PreferenceGroup;

    .line 326
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkMap:Ljava/util/HashMap;

    .line 328
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const-string/jumbo v5, "button_srch_netwrks_key"

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    iput-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mSearchButton:Landroid/preference/Preference;

    .line 329
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const-string/jumbo v5, "button_auto_select_key"

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    .line 336
    invoke-virtual {p0, v2}, Lcom/android/phone/settings/NetworkSetting;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 339
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "restore_to_automatic_mode"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    .line 340
    .local v1, "automatic":Z
    if-eqz v1, :cond_0

    .line 341
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x258

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 344
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getNetworkAutoSelectionMode()Z

    move-result v4

    if-nez v4, :cond_1

    move v0, v1

    .line 345
    :goto_0
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 346
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/NetworkSetting;->getAutoSelectionSummary(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setSummaryOn(Ljava/lang/CharSequence;)V

    .line 347
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/android/phone/settings/NetworkSetting;->getAutoSelectionSummary(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setSummaryOff(Ljava/lang/CharSequence;)V

    .line 349
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/android/phone/NetworkQueryService;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v5, "com.android.phone.intent.action.LOCAL_BINDER"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 350
    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryServiceConnection:Landroid/content/ServiceConnection;

    .line 349
    invoke-virtual {p0, v4, v5, v6}, Lcom/android/phone/settings/NetworkSetting;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 352
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 353
    .local v3, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "android.intent.action.SERVICE_STATE"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 354
    const-string/jumbo v4, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 355
    const-string/jumbo v4, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 356
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v3}, Lcom/android/phone/settings/NetworkSetting;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 357
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->setActionBar(Landroid/app/Activity;)V

    .line 358
    return-void

    .line 344
    .end local v3    # "intentFilter":Landroid/content/IntentFilter;
    :cond_1
    const/4 v0, 0x1

    .local v0, "autoSelect":Z
    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .param p1, "id"    # I

    .prologue
    const v7, 0x7f0b057f

    const v3, 0x1010355

    const/4 v6, 0x0

    const/high16 v5, 0x1040000

    const/4 v4, 0x1

    .line 440
    sparse-switch p1, :sswitch_data_0

    .line 528
    new-instance v1, Lmiui/app/ProgressDialog;

    invoke-direct {v1, p0}, Lmiui/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 529
    .local v1, "dialog":Lmiui/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0380

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 530
    invoke-virtual {v1, v6}, Lmiui/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 531
    invoke-virtual {v1, p0}, Lmiui/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 532
    return-object v1

    .line 447
    .end local v1    # "dialog":Lmiui/app/ProgressDialog;
    :sswitch_0
    new-instance v1, Lmiui/app/ProgressDialog;

    invoke-direct {v1, p0}, Lmiui/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 448
    .restart local v1    # "dialog":Lmiui/app/ProgressDialog;
    iget-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkSelectMsg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lmiui/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 449
    invoke-virtual {v1, v6}, Lmiui/app/ProgressDialog;->setCancelable(Z)V

    .line 450
    invoke-virtual {v1, v4}, Lmiui/app/ProgressDialog;->setIndeterminate(Z)V

    .line 451
    return-object v1

    .line 454
    .end local v1    # "dialog":Lmiui/app/ProgressDialog;
    :sswitch_1
    new-instance v1, Lmiui/app/ProgressDialog;

    invoke-direct {v1, p0}, Lmiui/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 455
    .restart local v1    # "dialog":Lmiui/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0389

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 456
    invoke-virtual {v1, v6}, Lmiui/app/ProgressDialog;->setCancelable(Z)V

    .line 457
    invoke-virtual {v1, v4}, Lmiui/app/ProgressDialog;->setIndeterminate(Z)V

    .line 458
    return-object v1

    .line 461
    .end local v1    # "dialog":Lmiui/app/ProgressDialog;
    :sswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 462
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b057e

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 466
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 467
    const/4 v2, 0x0

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    .line 462
    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 469
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0580

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 462
    invoke-virtual {v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 471
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mTurnOffAutoSelectWarningDialog:Landroid/app/AlertDialog;

    .line 472
    iget-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mTurnOffAutoSelectWarningDialog:Landroid/app/AlertDialog;

    return-object v2

    .line 475
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :sswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 476
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b0581

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 480
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 481
    const/4 v2, 0x0

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    .line 476
    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 483
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x104000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 476
    invoke-virtual {v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 484
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mTurnOffAutoSelectConfirmDialog:Landroid/app/AlertDialog;

    .line 485
    iget-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mTurnOffAutoSelectConfirmDialog:Landroid/app/AlertDialog;

    return-object v2

    .line 488
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :sswitch_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 489
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b0582

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 493
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 489
    invoke-virtual {v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 495
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0583

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 489
    invoke-virtual {v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 496
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryFailedDialog:Landroid/app/AlertDialog;

    .line 497
    iget-object v2, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryFailedDialog:Landroid/app/AlertDialog;

    return-object v2

    .line 500
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :sswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 501
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0584

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 503
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0585

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 501
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 504
    iget-object v3, p0, Lcom/android/phone/settings/NetworkSetting;->mAlertDialogListener:Landroid/content/DialogInterface$OnClickListener;

    const v4, 0x7f0b02f6

    .line 501
    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 505
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 508
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :sswitch_6
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 509
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0584

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 511
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0586

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 509
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 512
    iget-object v3, p0, Lcom/android/phone/settings/NetworkSetting;->mAlertDialogListener:Landroid/content/DialogInterface$OnClickListener;

    .line 509
    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 513
    iget-object v3, p0, Lcom/android/phone/settings/NetworkSetting;->mAlertDialogListener:Landroid/content/DialogInterface$OnClickListener;

    const v4, 0x7f0b02f6

    .line 509
    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 514
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 517
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :sswitch_7
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 518
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b057d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 520
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b071d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 518
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 521
    iget-object v3, p0, Lcom/android/phone/settings/NetworkSetting;->mAlertDialogListener:Landroid/content/DialogInterface$OnClickListener;

    const v4, 0x7f0b04e4

    .line 518
    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 522
    const v3, 0x7f0b04e3

    const/4 v4, 0x0

    .line 518
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 523
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 440
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x12c -> :sswitch_1
        0x190 -> :sswitch_2
        0x1f4 -> :sswitch_3
        0x258 -> :sswitch_4
        0x2bc -> :sswitch_5
        0x320 -> :sswitch_6
        0x384 -> :sswitch_7
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->doDataReEnable()V

    .line 431
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkQueryServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->unbindService(Landroid/content/ServiceConnection;)V

    .line 432
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/NetworkSetting;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 433
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 434
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    .line 435
    return-void
.end method

.method onNetworkQueryServiceConnected()V
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 589
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->checkDataConnection()V

    .line 591
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 392
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 393
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 394
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->onBackPressed()V

    .line 395
    const/4 v1, 0x1

    return v1

    .line 397
    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 386
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onPause()V

    .line 387
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mIsForeground:Z

    .line 388
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 7
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 264
    .local v0, "handled":Z
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mSearchButton:Landroid/preference/Preference;

    if-ne p2, v4, :cond_0

    .line 265
    invoke-direct {p0}, Lcom/android/phone/settings/NetworkSetting;->loadNetworksList()V

    .line 266
    const/4 v0, 0x1

    .line 289
    :goto_0
    return v0

    .line 267
    :cond_0
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    if-ne p2, v4, :cond_2

    .line 268
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-nez v4, :cond_1

    .line 269
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mAutoSelect:Landroid/preference/CheckBoxPreference;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 270
    const/16 v4, 0x190

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/NetworkSetting;->showDialog(I)V

    .line 274
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 272
    :cond_1
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x258

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 276
    :cond_2
    move-object v3, p2

    .line 277
    .local v3, "selectedCarrier":Landroid/preference/Preference;
    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 280
    .local v2, "networkStr":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mHandler:Landroid/os/Handler;

    const/16 v5, 0xc8

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 281
    .local v1, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/android/phone/settings/NetworkSetting;->mNetworkMap:Ljava/util/HashMap;

    invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/internal/telephony/OperatorInfo;

    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-static {v4, v1, v5}, Lcom/android/phone/PhoneProxy;->selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;Landroid/os/Message;Lcom/android/internal/telephony/Phone;)V

    .line 283
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "button_auto_select_key"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/phone/settings/NetworkSetting;->mSlotId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 285
    invoke-direct {p0, v2}, Lcom/android/phone/settings/NetworkSetting;->displayNetworkSeletionInProgress(Ljava/lang/String;)V

    .line 286
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 579
    const/16 v0, 0x64

    if-eq p1, v0, :cond_0

    const/16 v0, 0xc8

    if-ne p1, v0, :cond_2

    .line 583
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 585
    :cond_1
    return-void

    .line 580
    :cond_2
    const/16 v0, 0x12c

    if-ne p1, v0, :cond_1

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 380
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 381
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mIsForeground:Z

    .line 382
    return-void
.end method

.method protected showDialogSafely(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 665
    iget-boolean v0, p0, Lcom/android/phone/settings/NetworkSetting;->mIsForeground:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/settings/NetworkSetting;->isFinishing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 666
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/NetworkSetting;->showDialog(I)V

    .line 668
    :cond_0
    return-void
.end method
