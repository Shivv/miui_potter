.class public Lcom/android/phone/settings/CallForwardTypeActivity;
.super Lmiui/preference/PreferenceActivity;
.source "CallForwardTypeActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/CallForwardTypeActivity$1;
    }
.end annotation


# instance fields
.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mSubChangedListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

.field private mVideoPreference:Landroid/preference/Preference;

.field private mVoicePreference:Landroid/preference/Preference;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/CallForwardTypeActivity;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallForwardTypeActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardTypeActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/CallForwardTypeActivity;II)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallForwardTypeActivity;
    .param p1, "serviceClass"    # I
    .param p2, "slotId"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/settings/CallForwardTypeActivity;->startForwardActivity(II)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 64
    new-instance v0, Lcom/android/phone/settings/CallForwardTypeActivity$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/CallForwardTypeActivity$1;-><init>(Lcom/android/phone/settings/CallForwardTypeActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/CallForwardTypeActivity;->mSubChangedListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    .line 53
    return-void
.end method

.method private startForwardActivity(II)V
    .locals 4
    .param p1, "serviceClass"    # I
    .param p2, "slotId"    # I

    .prologue
    .line 114
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "CallForwardType"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "serviceClass: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const-string/jumbo v1, "service_class"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 117
    invoke-static {v0, p2}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    .line 118
    invoke-virtual {p0, v0}, Lcom/android/phone/settings/CallForwardTypeActivity;->startActivity(Landroid/content/Intent;)V

    .line 119
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v2, 0x7f060007

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallForwardTypeActivity;->addPreferencesFromResource(I)V

    .line 78
    invoke-virtual {p0}, Lcom/android/phone/settings/CallForwardTypeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 79
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v3

    .line 78
    invoke-static {v2, v3}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v1

    .line 80
    .local v1, "slotId":I
    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/CallForwardTypeActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 82
    invoke-virtual {p0}, Lcom/android/phone/settings/CallForwardTypeActivity;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 83
    .local v0, "actionBar":Lmiui/app/ActionBar;
    if-eqz v0, :cond_0

    .line 84
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmiui/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 88
    :cond_0
    const-string/jumbo v2, "button_cf_key_voice"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallForwardTypeActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/CallForwardTypeActivity;->mVoicePreference:Landroid/preference/Preference;

    .line 89
    iget-object v2, p0, Lcom/android/phone/settings/CallForwardTypeActivity;->mVoicePreference:Landroid/preference/Preference;

    new-instance v3, Lcom/android/phone/settings/CallForwardTypeActivity$2;

    invoke-direct {v3, p0, v1}, Lcom/android/phone/settings/CallForwardTypeActivity$2;-><init>(Lcom/android/phone/settings/CallForwardTypeActivity;I)V

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 99
    const-string/jumbo v2, "button_cf_key_video"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallForwardTypeActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/CallForwardTypeActivity;->mVideoPreference:Landroid/preference/Preference;

    .line 100
    iget-object v2, p0, Lcom/android/phone/settings/CallForwardTypeActivity;->mVideoPreference:Landroid/preference/Preference;

    new-instance v3, Lcom/android/phone/settings/CallForwardTypeActivity$3;

    invoke-direct {v3, p0, v1}, Lcom/android/phone/settings/CallForwardTypeActivity$3;-><init>(Lcom/android/phone/settings/CallForwardTypeActivity;I)V

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 109
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/CallForwardTypeActivity;->mSubChangedListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v2, v3}, Lmiui/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 111
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    .line 124
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/CallForwardTypeActivity;->mSubChangedListener:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Lmiui/telephony/SubscriptionManager;->removeOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 125
    return-void
.end method
