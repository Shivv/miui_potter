.class public Lcom/android/phone/settings/CallWaitingCheckBoxPreference;
.super Landroid/preference/CheckBoxPreference;
.source "CallWaitingCheckBoxPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;
    }
.end annotation


# instance fields
.field private DBG:Z

.field private isOpen:Z

.field private final mHandler:Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mTcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

.field public openComfirm:Landroid/app/Dialog;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->DBG:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->isOpen:Z

    return v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mHandler:Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mTcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->isOpen:Z

    return p1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    const v0, 0x101008f

    invoke-direct {p0, p1, p2, v0}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->DBG:Z

    .line 27
    new-instance v0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;-><init>(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;)V

    iput-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mHandler:Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

    .line 31
    iput-boolean v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->isOpen:Z

    .line 35
    return-void

    :cond_0
    move v0, v1

    .line 25
    goto :goto_0
.end method


# virtual methods
.method public init(Lcom/android/phone/settings/TimeConsumingPreferenceListener;ZI)V
    .locals 4
    .param p1, "listener"    # Lcom/android/phone/settings/TimeConsumingPreferenceListener;
    .param p2, "skipReading"    # Z
    .param p3, "subscription"    # I

    .prologue
    const/4 v3, 0x0

    .line 48
    iget-boolean v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->DBG:Z

    if-eqz v0, :cond_0

    .line 49
    const-string/jumbo v0, "CallWaitingCheckBoxPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "CallWaitingCheckBoxPreference init, subscription :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    :cond_0
    invoke-static {p3}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 52
    iput-object p1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mTcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    .line 54
    if-nez p2, :cond_1

    .line 55
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mHandler:Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

    invoke-virtual {v1, v3, v3, v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->getCallWaiting(Landroid/os/Message;)V

    .line 57
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mTcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mTcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    .line 61
    :cond_1
    return-void
.end method

.method protected onClick()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 65
    invoke-super {p0}, Landroid/preference/CheckBoxPreference;->onClick()V

    .line 67
    invoke-virtual {p0}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    iput-boolean v3, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->isOpen:Z

    .line 69
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->openComfirm:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 71
    const v1, 0x7f0b0692

    .line 70
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 73
    new-instance v1, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;-><init>(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)V

    .line 72
    const v2, 0x7f0b0693

    .line 70
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 84
    const v1, 0x7f0b0578

    .line 70
    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->openComfirm:Landroid/app/Dialog;

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->openComfirm:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 89
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->openComfirm:Landroid/app/Dialog;

    new-instance v1, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$2;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$2;-><init>(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 105
    :cond_1
    :goto_0
    return-void

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 100
    iget-object v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mHandler:Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 99
    invoke-virtual {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->setCallWaiting(ZLandroid/os/Message;)V

    .line 101
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mTcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->mTcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    invoke-interface {v0, p0, v3}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    goto :goto_0
.end method
