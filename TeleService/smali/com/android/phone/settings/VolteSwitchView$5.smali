.class Lcom/android/phone/settings/VolteSwitchView$5;
.super Ljava/lang/Object;
.source "VolteSwitchView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/VolteSwitchView;->showNoServiceTipsWhenTurnCdmaVolteOn(Lcom/android/internal/telephony/Phone;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/VolteSwitchView;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/VolteSwitchView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/VolteSwitchView$5;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    .line 346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 349
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView$5;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v1}, Lcom/android/phone/settings/VolteSwitchView;->-wrap2(Lcom/android/phone/settings/VolteSwitchView;)V

    .line 351
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView$5;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v1}, Lcom/android/phone/settings/VolteSwitchView;->-wrap1(Lcom/android/phone/settings/VolteSwitchView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :goto_0
    return-void

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setEnhanced4gLteModeSetting exception="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView$5;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v1}, Lcom/android/phone/settings/VolteSwitchView;->-get4(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/PreferenceActivity;

    move-result-object v2

    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView$5;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v1}, Lcom/android/phone/settings/VolteSwitchView;->-get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0b06e4

    .line 355
    :goto_1
    const/4 v3, 0x0

    .line 354
    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 356
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView$5;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v1}, Lcom/android/phone/settings/VolteSwitchView;->-get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$5;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 354
    :cond_0
    const v1, 0x7f0b06e5

    goto :goto_1
.end method
