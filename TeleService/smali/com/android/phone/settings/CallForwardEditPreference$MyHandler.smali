.class Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;
.super Landroid/os/Handler;
.source "CallForwardEditPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/CallForwardEditPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/CallForwardEditPreference;


# direct methods
.method private constructor <init>(Lcom/android/phone/settings/CallForwardEditPreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/CallForwardEditPreference;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/settings/CallForwardEditPreference;Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/CallForwardEditPreference;
    .param p2, "-this1"    # Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;-><init>(Lcom/android/phone/settings/CallForwardEditPreference;)V

    return-void
.end method

.method private handleGetCFResponse(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 252
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->-get0(Lcom/android/phone/settings/CallForwardEditPreference;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string/jumbo v8, "CallForwardEditPreference"

    const-string/jumbo v9, "handleGetCFResponse: done"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_0
    iget v8, p1, Landroid/os/Message;->arg2:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_3

    .line 255
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v8, v8, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    iget-object v9, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    .line 260
    :goto_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 262
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    const/4 v9, 0x0

    iput-object v9, v8, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    .line 263
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v8, :cond_4

    .line 264
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->-get0(Lcom/android/phone/settings/CallForwardEditPreference;)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string/jumbo v8, "CallForwardEditPreference"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "handleGetCFResponse: ar.exception="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_1
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v9, v8, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    iget-object v10, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    .line 266
    iget-object v8, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v8, Lcom/android/internal/telephony/CommandException;

    .line 265
    invoke-interface {v9, v10, v8}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onException(Landroid/preference/Preference;Lcom/android/internal/telephony/CommandException;)V

    .line 333
    :cond_2
    :goto_1
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->-wrap0(Lcom/android/phone/settings/CallForwardEditPreference;)V

    .line 334
    return-void

    .line 257
    .end local v0    # "ar":Landroid/os/AsyncResult;
    :cond_3
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v8, v8, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    iget-object v9, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    goto :goto_0

    .line 268
    .restart local v0    # "ar":Landroid/os/AsyncResult;
    :cond_4
    iget-object v8, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    instance-of v8, v8, Ljava/lang/Throwable;

    if-eqz v8, :cond_5

    .line 269
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v8, v8, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    iget-object v9, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    const/16 v10, 0x190

    invoke-interface {v8, v9, v10}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onError(Landroid/preference/Preference;I)V

    .line 271
    :cond_5
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v2, [Lcom/android/internal/telephony/CallForwardInfo;

    .line 272
    .local v2, "cfInfoArray":[Lcom/android/internal/telephony/CallForwardInfo;
    array-length v8, v2

    if-nez v8, :cond_7

    .line 273
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->-get0(Lcom/android/phone/settings/CallForwardEditPreference;)Z

    move-result v8

    if-eqz v8, :cond_6

    const-string/jumbo v8, "CallForwardEditPreference"

    const-string/jumbo v9, "handleGetCFResponse: cfInfoArray.length==0"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_6
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/phone/settings/CallForwardEditPreference;->setEnabled(Z)V

    .line 275
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v8, v8, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    iget-object v9, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    const/16 v10, 0x190

    invoke-interface {v8, v9, v10}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onError(Landroid/preference/Preference;I)V

    goto :goto_1

    .line 277
    :cond_7
    const/4 v4, 0x0

    .local v4, "i":I
    array-length v6, v2

    .local v6, "length":I
    :goto_2
    if-ge v4, v6, :cond_2

    .line 278
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->-get0(Lcom/android/phone/settings/CallForwardEditPreference;)Z

    move-result v8

    if-eqz v8, :cond_8

    const-string/jumbo v8, "CallForwardEditPreference"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "handleGetCFResponse, cfInfoArray["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "]="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 279
    aget-object v10, v2, v4

    .line 278
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_8
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->-get3(Lcom/android/phone/settings/CallForwardEditPreference;)I

    move-result v8

    aget-object v9, v2, v4

    iget v9, v9, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    and-int/2addr v8, v9

    if-eqz v8, :cond_b

    .line 282
    aget-object v5, v2, v4

    .line 283
    .local v5, "info":Lcom/android/internal/telephony/CallForwardInfo;
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v8, v5}, Lcom/android/phone/settings/CallForwardEditPreference;->handleCallForwardResult(Lcom/android/internal/telephony/CallForwardInfo;)V

    .line 285
    sget-boolean v8, Lmiui/os/Build;->IS_CTA_BUILD:Z

    if-eqz v8, :cond_9

    .line 286
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v8, v8, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    if-nez v8, :cond_9

    .line 288
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 287
    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 289
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v8, "unconditional_call_forwarding"

    .line 290
    iget v9, v5, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    .line 289
    invoke-interface {v3, v8, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 291
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 299
    .end local v3    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_9
    iget v8, p1, Landroid/os/Message;->arg2:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_b

    .line 300
    iget v8, p1, Landroid/os/Message;->arg1:I

    if-nez v8, :cond_b

    .line 301
    iget v8, v5, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_b

    .line 303
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v8, v8, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    packed-switch v8, :pswitch_data_0

    .line 311
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0b0358

    invoke-virtual {v8, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    .line 313
    .local v7, "s":Ljava/lang/CharSequence;
    :goto_3
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    instance-of v8, v8, Landroid/app/Activity;

    if-eqz v8, :cond_a

    .line 314
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->isFinishing()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 315
    return-void

    .line 305
    .end local v7    # "s":Ljava/lang/CharSequence;
    :pswitch_0
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0b034e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    .restart local v7    # "s":Ljava/lang/CharSequence;
    goto :goto_3

    .line 308
    .end local v7    # "s":Ljava/lang/CharSequence;
    :pswitch_1
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0b0353

    invoke-virtual {v8, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    .restart local v7    # "s":Ljava/lang/CharSequence;
    goto :goto_3

    .line 318
    :cond_a
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v1, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 319
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v8, 0x7f0b0368

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 320
    iget-object v8, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0b035c

    invoke-virtual {v8, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 321
    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 322
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 323
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    .line 277
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v5    # "info":Lcom/android/internal/telephony/CallForwardInfo;
    .end local v7    # "s":Ljava/lang/CharSequence;
    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleSetCFResponse(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 337
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 339
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    .line 340
    iget-object v1, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v1}, Lcom/android/phone/settings/CallForwardEditPreference;->-get0(Lcom/android/phone/settings/CallForwardEditPreference;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handleSetCFResponse: ar.exception="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v1}, Lcom/android/phone/settings/CallForwardEditPreference;->-get0(Lcom/android/phone/settings/CallForwardEditPreference;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handleSetCFResponse: re get mServiceClass: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallForwardEditPreference;->-get3(Lcom/android/phone/settings/CallForwardEditPreference;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v1, v1, Lcom/android/phone/settings/CallForwardEditPreference;->phone:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v2, v2, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    iget-object v3, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallForwardEditPreference;->-get3(Lcom/android/phone/settings/CallForwardEditPreference;)I

    move-result v3

    .line 345
    iget v4, p1, Landroid/os/Message;->arg1:I

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-virtual {p0, v6, v4, v7, v5}, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 344
    invoke-static {v1, v2, v3, v4}, Lcom/android/phone/PhoneProxy;->getCallForwardingOption(Lcom/android/internal/telephony/Phone;IILandroid/os/Message;)V

    .line 346
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x2

    .line 211
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 249
    :goto_0
    return-void

    .line 215
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v8, :cond_1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v0, v0, Lcom/android/phone/settings/CallForwardEditPreference;->phone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v0

    if-ne v0, v8, :cond_0

    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v0, v0, Lcom/android/phone/settings/CallForwardEditPreference;->phone:Lcom/android/internal/telephony/Phone;

    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->isSSOperatedByUt(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 218
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 219
    .local v7, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v0, v0, Lcom/android/phone/settings/CallForwardEditPreference;->phone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v0

    invoke-static {v7, v0}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    .line 220
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v0, v0, Lcom/android/phone/settings/CallForwardEditPreference;->mParentActivity:Landroid/app/Activity;

    const-class v1, Lcom/android/phone/settings/CdmaCallForwardOptions;

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 221
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v0, v0, Lcom/android/phone/settings/CallForwardEditPreference;->mParentActivity:Landroid/app/Activity;

    invoke-virtual {v0, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 222
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v0, v0, Lcom/android/phone/settings/CallForwardEditPreference;->mParentActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 223
    return-void

    .line 225
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v0, v0, Lcom/android/phone/settings/CallForwardEditPreference;->phone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v1, v1, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    iget-object v2, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v2}, Lcom/android/phone/settings/CallForwardEditPreference;->-get3(Lcom/android/phone/settings/CallForwardEditPreference;)I

    move-result v2

    iget-object v3, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallForwardEditPreference;->-get2(Lcom/android/phone/settings/CallForwardEditPreference;)Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;

    move-result-object v3

    .line 226
    iget v4, p1, Landroid/os/Message;->what:I

    add-int/lit8 v4, v4, 0x1

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 225
    invoke-virtual {v3, v4, v5, v6, v8}, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/phone/PhoneProxy;->getCallForwardingOption(Lcom/android/internal/telephony/Phone;IILandroid/os/Message;)V

    goto :goto_0

    .line 228
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->handleGetCFResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 234
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_5

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget-object v0, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v0, :cond_5

    .line 235
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v0, v0, Lcom/android/phone/settings/CallForwardEditPreference;->phone:Lcom/android/internal/telephony/Phone;

    .line 236
    iget-object v1, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/CallForwardEditPreference;->isToggled()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v1}, Lcom/android/phone/settings/CallForwardEditPreference;->-get1(Lcom/android/phone/settings/CallForwardEditPreference;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 237
    :cond_2
    const/4 v1, 0x3

    .line 239
    :goto_1
    iget-object v2, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v2, v2, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    iget-object v3, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v3}, Lcom/android/phone/settings/CallForwardEditPreference;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 240
    iget-object v4, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v4}, Lcom/android/phone/settings/CallForwardEditPreference;->-get3(Lcom/android/phone/settings/CallForwardEditPreference;)I

    move-result v4

    .line 241
    iget-object v6, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v6, v6, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    if-eq v6, v8, :cond_4

    .line 242
    :goto_2
    iget-object v6, p0, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-static {v6}, Lcom/android/phone/settings/CallForwardEditPreference;->-get2(Lcom/android/phone/settings/CallForwardEditPreference;)Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;

    move-result-object v6

    iget v8, p1, Landroid/os/Message;->what:I

    add-int/lit8 v8, v8, 0x1

    iget v9, p1, Landroid/os/Message;->arg1:I

    iget v10, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v6, v8, v9, v10}, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v6

    .line 235
    invoke-static/range {v0 .. v6}, Lcom/android/phone/PhoneProxy;->setCallForwardingOption(Lcom/android/internal/telephony/Phone;IILjava/lang/String;IILandroid/os/Message;)V

    goto/16 :goto_0

    :cond_3
    move v1, v5

    .line 238
    goto :goto_1

    .line 241
    :cond_4
    const/16 v5, 0x14

    goto :goto_2

    .line 245
    :cond_5
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->handleSetCFResponse(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 211
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
