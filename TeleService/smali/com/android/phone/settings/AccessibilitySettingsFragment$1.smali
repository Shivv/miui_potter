.class Lcom/android/phone/settings/AccessibilitySettingsFragment$1;
.super Landroid/telephony/PhoneStateListener;
.source "AccessibilitySettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/AccessibilitySettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/AccessibilitySettingsFragment;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/AccessibilitySettingsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/AccessibilitySettingsFragment;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment$1;->this$0:Lcom/android/phone/settings/AccessibilitySettingsFragment;

    .line 45
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 6
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 55
    iget-object v4, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment$1;->this$0:Lcom/android/phone/settings/AccessibilitySettingsFragment;

    invoke-virtual {v4}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const-string/jumbo v5, "button_tty_mode_key"

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 56
    .local v1, "pref":Landroid/preference/Preference;
    if-eqz v1, :cond_2

    .line 57
    iget-object v4, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment$1;->this$0:Lcom/android/phone/settings/AccessibilitySettingsFragment;

    invoke-static {v4}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->-get0(Lcom/android/phone/settings/AccessibilitySettingsFragment;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/ims/ImsManager;->isVolteEnabledByPlatform(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 58
    iget-object v4, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment$1;->this$0:Lcom/android/phone/settings/AccessibilitySettingsFragment;

    invoke-static {v4}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->-wrap0(Lcom/android/phone/settings/AccessibilitySettingsFragment;)Z

    move-result v0

    .line 59
    :goto_0
    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment$1;->this$0:Lcom/android/phone/settings/AccessibilitySettingsFragment;

    invoke-static {v4}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->-wrap1(Lcom/android/phone/settings/AccessibilitySettingsFragment;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_1

    .line 60
    :cond_0
    if-nez p1, :cond_4

    .line 59
    :cond_1
    :goto_1
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 62
    :cond_2
    return-void

    .line 57
    :cond_3
    const/4 v0, 0x0

    .local v0, "isVolteTtySupported":Z
    goto :goto_0

    .end local v0    # "isVolteTtySupported":Z
    :cond_4
    move v2, v3

    .line 60
    goto :goto_1
.end method
