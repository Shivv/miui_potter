.class public Lcom/android/phone/settings/TimeConsumingPreferenceActivity;
.super Lmiui/preference/PreferenceActivity;
.source "TimeConsumingPreferenceActivity.java"

# interfaces
.implements Lcom/android/phone/settings/TimeConsumingPreferenceListener;
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/TimeConsumingPreferenceActivity$1;,
        Lcom/android/phone/settings/TimeConsumingPreferenceActivity$DismissAndFinishOnClickListener;,
        Lcom/android/phone/settings/TimeConsumingPreferenceActivity$DismissOnClickListener;
    }
.end annotation


# instance fields
.field private DBG:Z

.field private final mBusyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mDismiss:Landroid/content/DialogInterface$OnClickListener;

.field public final mDismissAndFinish:Landroid/content/DialogInterface$OnClickListener;

.field private mHandler:Landroid/os/Handler;

.field protected mIsForeground:Z

.field private startedTimeMillis:J


# direct methods
.method static synthetic -wrap0(Lcom/android/phone/settings/TimeConsumingPreferenceActivity;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/TimeConsumingPreferenceActivity;
    .param p1, "reading"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->dismissDialogSafely(Z)V

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 24
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->DBG:Z

    .line 39
    new-instance v0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity$DismissOnClickListener;

    invoke-direct {v0, p0, v3}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity$DismissOnClickListener;-><init>(Lcom/android/phone/settings/TimeConsumingPreferenceActivity;Lcom/android/phone/settings/TimeConsumingPreferenceActivity$DismissOnClickListener;)V

    iput-object v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    .line 41
    new-instance v0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity$DismissAndFinishOnClickListener;

    invoke-direct {v0, p0, v3}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity$DismissAndFinishOnClickListener;-><init>(Lcom/android/phone/settings/TimeConsumingPreferenceActivity;Lcom/android/phone/settings/TimeConsumingPreferenceActivity$DismissAndFinishOnClickListener;)V

    .line 40
    iput-object v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismissAndFinish:Landroid/content/DialogInterface$OnClickListener;

    .line 43
    new-instance v0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity$1;-><init>(Lcom/android/phone/settings/TimeConsumingPreferenceActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mHandler:Landroid/os/Handler;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mBusyList:Ljava/util/ArrayList;

    .line 71
    iput-boolean v1, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mIsForeground:Z

    .line 20
    return-void

    :cond_0
    move v0, v1

    .line 24
    goto :goto_0
.end method

.method private dismissDialogSafely(Z)V
    .locals 3
    .param p1, "reading"    # Z

    .prologue
    .line 225
    iget-object v1, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 227
    if-eqz p1, :cond_0

    .line 228
    const/16 v1, 0x64

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->dismissDialog(I)V

    .line 237
    :goto_0
    return-void

    .line 230
    :cond_0
    const/16 v1, 0xc8

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 232
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_0
.end method


# virtual methods
.method dumpState()V
    .locals 5

    .prologue
    .line 240
    const-string/jumbo v2, "TimeConsumingPreferenceActivity"

    const-string/jumbo v3, "dumpState begin"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v2, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mBusyList:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "key$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 242
    .local v0, "key":Ljava/lang/String;
    const-string/jumbo v2, "TimeConsumingPreferenceActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mBusyList: key="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 244
    .end local v0    # "key":Ljava/lang/String;
    :cond_0
    const-string/jumbo v2, "TimeConsumingPreferenceActivity"

    const-string/jumbo v3, "dumpState end"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->DBG:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->dumpState()V

    .line 221
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->finish()V

    .line 222
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 10
    .param p1, "id"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, 0x7f0b0368

    .line 76
    const/16 v5, 0x64

    if-eq p1, v5, :cond_0

    const/16 v5, 0xc8

    if-ne p1, v5, :cond_1

    .line 77
    :cond_0
    new-instance v2, Lmiui/app/ProgressDialog;

    invoke-direct {v2, p0}, Lmiui/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 78
    .local v2, "dialog":Lmiui/app/ProgressDialog;
    const v5, 0x7f0b0359

    invoke-virtual {p0, v5}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Lmiui/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {v2, v8}, Lmiui/app/ProgressDialog;->setIndeterminate(Z)V

    .line 81
    sparse-switch p1, :sswitch_data_0

    .line 92
    return-object v9

    .line 83
    :sswitch_0
    invoke-virtual {v2, v8}, Lmiui/app/ProgressDialog;->setCancelable(Z)V

    .line 84
    invoke-virtual {v2, p0}, Lmiui/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 85
    const v5, 0x7f0b035d

    invoke-virtual {p0, v5}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Lmiui/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 86
    return-object v2

    .line 88
    :sswitch_1
    invoke-virtual {v2, v7}, Lmiui/app/ProgressDialog;->setCancelable(Z)V

    .line 89
    const v5, 0x7f0b035e

    invoke-virtual {p0, v5}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Lmiui/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 90
    return-object v2

    .line 95
    .end local v2    # "dialog":Lmiui/app/ProgressDialog;
    :cond_1
    const/16 v5, 0x190

    if-eq p1, v5, :cond_2

    const/16 v5, 0x1f4

    if-ne p1, v5, :cond_3

    .line 98
    :cond_2
    new-instance v0, Lmiui/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 101
    .local v0, "builder":Lmiui/app/AlertDialog$Builder;
    const v4, 0x7f0b035c

    .line 103
    .local v4, "titleId":I
    sparse-switch p1, :sswitch_data_1

    .line 134
    const v3, 0x7f0b0361

    .line 136
    .local v3, "msgId":I
    iget-object v5, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v6, v5}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    .line 140
    :goto_0
    invoke-virtual {p0, v4}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0, v5}, Lmiui/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    .line 141
    invoke-virtual {p0, v3}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0, v5}, Lmiui/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    .line 142
    invoke-virtual {v0, v7}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    .line 143
    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v1

    .line 145
    .local v1, "dialog":Lmiui/app/AlertDialog;
    return-object v1

    .line 95
    .end local v0    # "builder":Lmiui/app/AlertDialog$Builder;
    .end local v1    # "dialog":Lmiui/app/AlertDialog;
    .end local v3    # "msgId":I
    .end local v4    # "titleId":I
    :cond_3
    const/16 v5, 0x12c

    if-eq p1, v5, :cond_2

    .line 96
    const/16 v5, 0x258

    if-eq p1, v5, :cond_2

    const/16 v5, 0x2bc

    if-eq p1, v5, :cond_2

    .line 97
    const/16 v5, 0x320

    if-eq p1, v5, :cond_2

    const/16 v5, 0x384

    if-eq p1, v5, :cond_2

    .line 147
    return-object v9

    .line 105
    .restart local v0    # "builder":Lmiui/app/AlertDialog$Builder;
    .restart local v4    # "titleId":I
    :sswitch_2
    const v3, 0x7f0b0360

    .line 106
    .restart local v3    # "msgId":I
    iget-object v5, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v6, v5}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    goto :goto_0

    .line 109
    .end local v3    # "msgId":I
    :sswitch_3
    const v3, 0x7f0b0367

    .line 111
    .restart local v3    # "msgId":I
    iget-object v5, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismissAndFinish:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v6, v5}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    goto :goto_0

    .line 114
    .end local v3    # "msgId":I
    :sswitch_4
    const v3, 0x7f0b0366

    .line 115
    .restart local v3    # "msgId":I
    iget-object v5, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v6, v5}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    goto :goto_0

    .line 118
    .end local v3    # "msgId":I
    :sswitch_5
    const v3, 0x7f0b0362

    .line 119
    .restart local v3    # "msgId":I
    iget-object v5, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v6, v5}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    goto :goto_0

    .line 122
    .end local v3    # "msgId":I
    :sswitch_6
    const v3, 0x7f0b0363

    .line 123
    .restart local v3    # "msgId":I
    iget-object v5, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v6, v5}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    goto :goto_0

    .line 126
    .end local v3    # "msgId":I
    :sswitch_7
    const v3, 0x7f0b0364

    .line 127
    .restart local v3    # "msgId":I
    iget-object v5, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v6, v5}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    goto :goto_0

    .line 130
    .end local v3    # "msgId":I
    :sswitch_8
    const v3, 0x7f0b0361

    .line 131
    .restart local v3    # "msgId":I
    iget-object v5, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v6, v5}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    goto :goto_0

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch

    .line 103
    :sswitch_data_1
    .sparse-switch
        0x12c -> :sswitch_8
        0x190 -> :sswitch_2
        0x1f4 -> :sswitch_3
        0x258 -> :sswitch_4
        0x2bc -> :sswitch_5
        0x320 -> :sswitch_6
        0x384 -> :sswitch_7
    .end sparse-switch
.end method

.method public onError(Landroid/preference/Preference;I)V
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "error"    # I

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->DBG:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->dumpState()V

    .line 200
    :cond_0
    iget-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->DBG:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "TimeConsumingPreferenceActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onError, preference="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", error="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_1
    iget-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mIsForeground:Z

    if-eqz v0, :cond_2

    .line 203
    invoke-virtual {p0, p2}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->showDialog(I)V

    .line 205
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 206
    return-void
.end method

.method public onException(Landroid/preference/Preference;Lcom/android/internal/telephony/CommandException;)V
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "exception"    # Lcom/android/internal/telephony/CommandException;

    .prologue
    .line 210
    invoke-virtual {p2}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v0

    sget-object v1, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v0, v1, :cond_0

    .line 211
    const/16 v0, 0x258

    invoke-virtual {p0, p1, v0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    .line 216
    :goto_0
    return-void

    .line 213
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 214
    const/16 v0, 0x12c

    invoke-virtual {p0, p1, v0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    goto :goto_0
.end method

.method public onFinished(Landroid/preference/Preference;Z)V
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "reading"    # Z

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v4, 0x1

    .line 182
    iget-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->DBG:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->dumpState()V

    .line 183
    :cond_0
    iget-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->DBG:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "TimeConsumingPreferenceActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onFinished, preference="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 184
    const-string/jumbo v2, ", reading="

    .line 183
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mBusyList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 187
    iget-object v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mBusyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->startedTimeMillis:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-gez v0, :cond_3

    .line 189
    iget-object v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mHandler:Landroid/os/Handler;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 194
    :cond_2
    :goto_0
    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 195
    return-void

    .line 191
    :cond_3
    invoke-direct {p0, p2}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->dismissDialogSafely(Z)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onPause()V

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mIsForeground:Z

    .line 160
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 152
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mIsForeground:Z

    .line 154
    return-void
.end method

.method public onStarted(Landroid/preference/Preference;Z)V
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "reading"    # Z

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->DBG:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->dumpState()V

    .line 165
    :cond_0
    iget-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->DBG:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "TimeConsumingPreferenceActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStarted, preference="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 166
    const-string/jumbo v2, ", reading="

    .line 165
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mBusyList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    iget-boolean v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->mIsForeground:Z

    if-eqz v0, :cond_2

    .line 170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->startedTimeMillis:J

    .line 171
    if-eqz p2, :cond_3

    .line 172
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->showDialog(I)V

    .line 178
    :cond_2
    :goto_0
    return-void

    .line 174
    :cond_3
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->showDialog(I)V

    goto :goto_0
.end method
