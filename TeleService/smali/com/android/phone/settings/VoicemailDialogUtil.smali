.class public Lcom/android/phone/settings/VoicemailDialogUtil;
.super Ljava/lang/Object;
.source "VoicemailDialogUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDialog(Lcom/android/phone/settings/VoicemailSettingsActivity;I)Landroid/app/Dialog;
    .locals 11
    .param p0, "parent"    # Lcom/android/phone/settings/VoicemailSettingsActivity;
    .param p1, "id"    # I

    .prologue
    const/16 v10, 0x25b

    const/16 v9, 0x259

    const/4 v8, 0x0

    const v7, 0x7f0b0368

    .line 40
    const/16 v6, 0x1f4

    if-eq p1, v6, :cond_0

    const/16 v6, 0x190

    if-ne p1, v6, :cond_1

    .line 44
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 47
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f0b035c

    .line 48
    .local v5, "titleId":I
    sparse-switch p1, :sswitch_data_0

    .line 85
    const v4, 0x7f0b0361

    .line 88
    .local v4, "msgId":I
    invoke-virtual {v0, v7, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 92
    :goto_0
    invoke-virtual {p0, v5}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 93
    invoke-virtual {p0, v4}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 94
    .local v3, "message":Ljava/lang/String;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 95
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 96
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 99
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/view/Window;->addFlags(I)V

    .line 101
    return-object v1

    .line 41
    .end local v0    # "b":Landroid/app/AlertDialog$Builder;
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    .end local v3    # "message":Ljava/lang/String;
    .end local v4    # "msgId":I
    .end local v5    # "titleId":I
    :cond_1
    const/16 v6, 0x1f5

    if-eq p1, v6, :cond_0

    const/16 v6, 0x1f6

    if-eq p1, v6, :cond_0

    .line 42
    const/16 v6, 0x258

    if-eq p1, v6, :cond_0

    const/16 v6, 0x320

    if-eq p1, v6, :cond_0

    .line 102
    if-eq p1, v9, :cond_2

    const/16 v6, 0x25a

    if-ne p1, v6, :cond_3

    .line 104
    :cond_2
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 105
    .local v2, "dialog":Landroid/app/ProgressDialog;
    const v6, 0x7f0b0326

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 106
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 107
    invoke-virtual {v2, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 109
    if-ne p1, v9, :cond_4

    const v6, 0x7f0b035e

    .line 108
    :goto_1
    invoke-virtual {p0, v6}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 112
    return-object v2

    .line 50
    .end local v2    # "dialog":Landroid/app/ProgressDialog;
    .restart local v0    # "b":Landroid/app/AlertDialog$Builder;
    .restart local v5    # "titleId":I
    :sswitch_0
    const v4, 0x7f0b036c

    .line 51
    .restart local v4    # "msgId":I
    const v5, 0x7f0b0320

    .line 53
    invoke-virtual {v0, v7, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 58
    .end local v4    # "msgId":I
    :sswitch_1
    const v4, 0x7f0b0370

    .line 59
    .restart local v4    # "msgId":I
    const v5, 0x7f0b0320

    .line 61
    invoke-virtual {v0, v7, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 64
    .end local v4    # "msgId":I
    :sswitch_2
    const v4, 0x7f0b036d

    .line 66
    .restart local v4    # "msgId":I
    invoke-virtual {v0, v7, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 69
    .end local v4    # "msgId":I
    :sswitch_3
    const v4, 0x7f0b036e

    .line 71
    .restart local v4    # "msgId":I
    invoke-virtual {v0, v7, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 74
    .end local v4    # "msgId":I
    :sswitch_4
    const v4, 0x7f0b036f

    .line 75
    .restart local v4    # "msgId":I
    const v6, 0x7f0b04e3

    invoke-virtual {v0, v6, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 76
    const v6, 0x7f0b04e4

    invoke-virtual {v0, v6, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    .line 79
    .end local v4    # "msgId":I
    :sswitch_5
    const v5, 0x7f0b0431

    .line 80
    const v4, 0x7f0b0435

    .line 81
    .restart local v4    # "msgId":I
    const v6, 0x1010355

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    .line 82
    const v6, 0x7f0b02f6

    invoke-virtual {v0, v6, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    .line 103
    .end local v0    # "b":Landroid/app/AlertDialog$Builder;
    .end local v4    # "msgId":I
    .end local v5    # "titleId":I
    :cond_3
    if-eq p1, v10, :cond_2

    .line 115
    const/4 v6, 0x0

    return-object v6

    .line 110
    .restart local v2    # "dialog":Landroid/app/ProgressDialog;
    :cond_4
    if-ne p1, v10, :cond_5

    const v6, 0x7f0b035f

    goto :goto_1

    .line 111
    :cond_5
    const v6, 0x7f0b035d

    goto :goto_1

    .line 48
    nop

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_1
        0x1f4 -> :sswitch_2
        0x1f5 -> :sswitch_3
        0x1f6 -> :sswitch_4
        0x258 -> :sswitch_0
        0x320 -> :sswitch_5
    .end sparse-switch
.end method
