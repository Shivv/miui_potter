.class public Lcom/android/phone/settings/VoicemailSettingsActivity;
.super Lmiui/preference/PreferenceActivity;
.source "VoicemailSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;
.implements Lcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/VoicemailSettingsActivity$1;,
        Lcom/android/phone/settings/VoicemailSettingsActivity$2;,
        Lcom/android/phone/settings/VoicemailSettingsActivity$3;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mChangingVMorFwdDueToProviderChange:Z

.field private mCurrentDialogId:I

.field private mExpectedChangeResultReasons:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mForeground:Z

.field private mForwardingChangeResults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/AsyncResult;",
            ">;"
        }
    .end annotation
.end field

.field private mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

.field private mFwdChangesRequireRollback:Z

.field private final mGetOptionComplete:Landroid/os/Handler;

.field private mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

.field private mNewVMNumber:Ljava/lang/String;

.field private mOldVmNumber:Ljava/lang/String;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mPreviousVMProviderKey:Ljava/lang/String;

.field private final mRevertOptionComplete:Landroid/os/Handler;

.field private final mSetOptionComplete:Landroid/os/Handler;

.field private mShowVoicemailPreference:Z

.field private mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

.field private mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

.field private mVMChangeCompletedSuccessfully:Z

.field private mVMOrFwdSetError:I

.field private mVMProviderSettingsForced:Z

.field private mVoicemailChangeResult:Landroid/os/AsyncResult;

.field private mVoicemailNotificationPreference:Landroid/preference/Preference;

.field private mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

.field private mVoicemailSettings:Landroid/preference/PreferenceScreen;


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/phone/settings/VoicemailSettingsActivity;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/VoicemailSettingsActivity;)Ljava/util/Map;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingChangeResults:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/VoicemailSettingsActivity;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mFwdChangesRequireRollback:Z

    return v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/VoicemailSettingsActivity;)Lcom/android/phone/SubscriptionInfoHelper;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/phone/settings/VoicemailSettingsActivity;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMChangeCompletedSuccessfully:Z

    return v0
.end method

.method static synthetic -get5(Lcom/android/phone/settings/VoicemailSettingsActivity;)Landroid/os/AsyncResult;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/VoicemailSettingsActivity;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mFwdChangesRequireRollback:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/phone/settings/VoicemailSettingsActivity;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMChangeCompletedSuccessfully:Z

    return p1
.end method

.method static synthetic -set2(Lcom/android/phone/settings/VoicemailSettingsActivity;Landroid/os/AsyncResult;)Landroid/os/AsyncResult;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;
    .param p1, "-value"    # Landroid/os/AsyncResult;

    .prologue
    iput-object p1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/VoicemailSettingsActivity;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->isForwardingCompleted()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/VoicemailSettingsActivity;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->isFwdChangeSuccess()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/VoicemailSettingsActivity;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->isVmChangeSuccess()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap3(Lcom/android/phone/settings/VoicemailSettingsActivity;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;
    .param p1, "id"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->dismissDialogSafely(I)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/phone/settings/VoicemailSettingsActivity;Landroid/os/AsyncResult;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;
    .param p1, "ar"    # Landroid/os/AsyncResult;
    .param p2, "idx"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->handleForwardingSettingsReadResult(Landroid/os/AsyncResult;I)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/phone/settings/VoicemailSettingsActivity;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->handleSetVmOrFwdMessage()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/phone/settings/VoicemailSettingsActivity;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->onRevertDone()V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/phone/settings/VoicemailSettingsActivity;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSettingsActivity;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->setVoicemailNumberWithCarrier()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/android/phone/settings/VoicemailSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/VoicemailSettingsActivity;->LOG_TAG:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 122
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 128
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingChangeResults:Ljava/util/Map;

    .line 135
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mExpectedChangeResultReasons:Ljava/util/Collection;

    .line 140
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    .line 145
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    .line 150
    iput v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mCurrentDialogId:I

    .line 156
    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMProviderSettingsForced:Z

    .line 162
    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mChangingVMorFwdDueToProviderChange:Z

    .line 168
    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMChangeCompletedSuccessfully:Z

    .line 174
    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mFwdChangesRequireRollback:Z

    .line 180
    iput v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMOrFwdSetError:I

    .line 194
    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mShowVoicemailPreference:Z

    .line 200
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    .line 719
    new-instance v0, Lcom/android/phone/settings/VoicemailSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/VoicemailSettingsActivity$1;-><init>(Lcom/android/phone/settings/VoicemailSettingsActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mGetOptionComplete:Landroid/os/Handler;

    .line 819
    new-instance v0, Lcom/android/phone/settings/VoicemailSettingsActivity$2;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/VoicemailSettingsActivity$2;-><init>(Lcom/android/phone/settings/VoicemailSettingsActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSetOptionComplete:Landroid/os/Handler;

    .line 881
    new-instance v0, Lcom/android/phone/settings/VoicemailSettingsActivity$3;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/VoicemailSettingsActivity$3;-><init>(Lcom/android/phone/settings/VoicemailSettingsActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mRevertOptionComplete:Landroid/os/Handler;

    .line 60
    return-void
.end method

.method private dismissDialogSafely(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 623
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 629
    :goto_0
    return-void

    .line 624
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_0
.end method

.method private handleForwardingSettingsReadResult(Landroid/os/AsyncResult;I)V
    .locals 8
    .param p1, "ar"    # Landroid/os/AsyncResult;
    .param p2, "idx"    # I

    .prologue
    const/16 v6, 0x25a

    const/4 v4, 0x0

    .line 734
    const/4 v1, 0x0

    .line 735
    .local v1, "error":Ljava/lang/Throwable;
    iget-object v3, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v3, :cond_0

    .line 736
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 739
    .end local v1    # "error":Ljava/lang/Throwable;
    :cond_0
    iget-object v3, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    instance-of v3, v3, Ljava/lang/Throwable;

    if-eqz v3, :cond_1

    .line 740
    iget-object v1, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Throwable;

    .line 745
    :cond_1
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    if-nez v3, :cond_2

    .line 747
    return-void

    .line 751
    :cond_2
    if-eqz v1, :cond_3

    .line 753
    iput-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 754
    invoke-direct {p0, v6}, Lcom/android/phone/settings/VoicemailSettingsActivity;->dismissDialogSafely(I)V

    .line 755
    const/16 v3, 0x1f6

    invoke-direct {p0, v3}, Lcom/android/phone/settings/VoicemailSettingsActivity;->showDialogIfForeground(I)V

    .line 756
    return-void

    .line 760
    :cond_3
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 761
    iget-object v3, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v3, [Lcom/android/internal/telephony/CallForwardInfo;

    .line 762
    sget-object v5, Lcom/android/phone/settings/VoicemailProviderSettings;->FORWARDING_SETTINGS_REASONS:[I

    aget v5, v5, p2

    .line 760
    invoke-static {v3, v5}, Lcom/android/phone/settings/CallForwardInfoUtil;->getCallForwardInfo([Lcom/android/internal/telephony/CallForwardInfo;I)Lcom/android/internal/telephony/CallForwardInfo;

    move-result-object v3

    aput-object v3, v4, p2

    .line 765
    const/4 v0, 0x1

    .line 766
    .local v0, "done":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    array-length v3, v3

    if-ge v2, v3, :cond_4

    .line 767
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    aget-object v3, v3, v2

    if-nez v3, :cond_7

    .line 768
    const/4 v0, 0x0

    .line 773
    :cond_4
    if-eqz v0, :cond_6

    .line 775
    invoke-direct {p0, v6}, Lcom/android/phone/settings/VoicemailSettingsActivity;->dismissDialogSafely(I)V

    .line 777
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 778
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 779
    const-string/jumbo v4, ""

    .line 780
    new-instance v5, Lcom/android/phone/settings/VoicemailProviderSettings;

    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mOldVmNumber:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    invoke-direct {v5, v6, v7}, Lcom/android/phone/settings/VoicemailProviderSettings;-><init>(Ljava/lang/String;[Lcom/android/internal/telephony/CallForwardInfo;)V

    .line 778
    invoke-static {v3, v4, v5}, Lcom/android/phone/settings/VoicemailProviderSettingsUtil;->save(Landroid/content/Context;Ljava/lang/String;Lcom/android/phone/settings/VoicemailProviderSettings;)V

    .line 782
    :cond_5
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->saveVoiceMailAndForwardingNumberStage2()V

    .line 784
    :cond_6
    return-void

    .line 766
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private handleSetVmOrFwdMessage()V
    .locals 1

    .prologue
    .line 1034
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->isFwdChangeSuccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1035
    const/16 v0, 0x1f5

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->handleVmOrFwdSetError(I)V

    .line 1041
    :goto_0
    return-void

    .line 1036
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->isVmChangeSuccess()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1037
    const/16 v0, 0x1f4

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->handleVmOrFwdSetError(I)V

    goto :goto_0

    .line 1039
    :cond_1
    const/16 v0, 0x258

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->handleVmAndFwdSetSuccess(I)V

    goto :goto_0
.end method

.method private handleVmAndFwdSetSuccess(I)V
    .locals 1
    .param p1, "dialogId"    # I

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v0}, Lcom/android/phone/settings/VoicemailProviderListPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    .line 1070
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mChangingVMorFwdDueToProviderChange:Z

    .line 1071
    invoke-direct {p0, p1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->showDialogIfForeground(I)V

    .line 1072
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->updateVoiceNumberField()V

    .line 1073
    return-void
.end method

.method private handleVmOrFwdSetError(I)V
    .locals 2
    .param p1, "dialogId"    # I

    .prologue
    const/4 v1, 0x0

    .line 1051
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mChangingVMorFwdDueToProviderChange:Z

    if-eqz v0, :cond_0

    .line 1052
    iput p1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMOrFwdSetError:I

    .line 1053
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mChangingVMorFwdDueToProviderChange:Z

    .line 1054
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->switchToPreviousVoicemailProvider()V

    .line 1055
    return-void

    .line 1057
    :cond_0
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mChangingVMorFwdDueToProviderChange:Z

    .line 1058
    invoke-direct {p0, p1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->showDialogIfForeground(I)V

    .line 1059
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->updateVoiceNumberField()V

    .line 1060
    return-void
.end method

.method private isForwardingCompleted()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1095
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingChangeResults:Ljava/util/Map;

    if-nez v2, :cond_0

    .line 1096
    return v3

    .line 1099
    :cond_0
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mExpectedChangeResultReasons:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "reason$iterator":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1100
    .local v0, "reason":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingChangeResults:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1101
    const/4 v2, 0x0

    return v2

    .line 1105
    .end local v0    # "reason":Ljava/lang/Integer;
    :cond_2
    return v3
.end method

.method private isFwdChangeSuccess()Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1109
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingChangeResults:Ljava/util/Map;

    if-nez v4, :cond_0

    .line 1110
    return v5

    .line 1113
    :cond_0
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingChangeResults:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "result$iterator":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/AsyncResult;

    .line 1114
    .local v2, "result":Landroid/os/AsyncResult;
    iget-object v0, v2, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 1115
    .local v0, "exception":Ljava/lang/Throwable;
    if-eqz v0, :cond_1

    .line 1116
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 1117
    .local v1, "msg":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 1118
    :goto_0
    sget-object v4, Lcom/android/phone/settings/VoicemailSettingsActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Failed to change forwarding setting. Reason: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1119
    const/4 v4, 0x0

    return v4

    .line 1117
    :cond_2
    const-string/jumbo v1, ""

    goto :goto_0

    .line 1122
    .end local v0    # "exception":Ljava/lang/Throwable;
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "result":Landroid/os/AsyncResult;
    :cond_3
    return v5
.end method

.method private isVmChangeSuccess()Z
    .locals 4

    .prologue
    .line 1126
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    iget-object v1, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_1

    .line 1127
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    iget-object v1, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 1128
    .local v0, "msg":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1129
    :goto_0
    sget-object v1, Lcom/android/phone/settings/VoicemailSettingsActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Failed to change voicemail. Reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1130
    const/4 v1, 0x0

    return v1

    .line 1128
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    .line 1132
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x1

    return v1
.end method

.method private maybeHidePublicSettings()V
    .locals 4

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "android.telephony.extra.HIDE_PUBLIC_SETTINGS"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 297
    return-void

    .line 302
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 303
    .local v0, "preferenceScreen":Landroid/preference/PreferenceScreen;
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailNotificationPreference:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 304
    return-void
.end method

.method private onRevertDone()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1078
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->updateVMPreferenceWidgets(Ljava/lang/String;)V

    .line 1079
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->updateVoiceNumberField()V

    .line 1080
    iget v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMOrFwdSetError:I

    if-eqz v0, :cond_0

    .line 1081
    iget v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMOrFwdSetError:I

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->showDialogIfForeground(I)V

    .line 1082
    iput v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMOrFwdSetError:I

    .line 1084
    :cond_0
    return-void
.end method

.method private resetForwardingChangeState()V
    .locals 1

    .prologue
    .line 787
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingChangeResults:Ljava/util/Map;

    .line 788
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mExpectedChangeResultReasons:Ljava/util/Collection;

    .line 789
    return-void
.end method

.method private saveVoiceMailAndForwardingNumber(Ljava/lang/String;Lcom/android/phone/settings/VoicemailProviderSettings;)V
    .locals 7
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "newSettings"    # Lcom/android/phone/settings/VoicemailProviderSettings;

    .prologue
    const/4 v6, 0x0

    .line 680
    invoke-virtual {p2}, Lcom/android/phone/settings/VoicemailProviderSettings;->getVoicemailNumber()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewVMNumber:Ljava/lang/String;

    .line 681
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewVMNumber:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string/jumbo v2, ""

    :goto_0
    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewVMNumber:Ljava/lang/String;

    .line 682
    invoke-virtual {p2}, Lcom/android/phone/settings/VoicemailProviderSettings;->getForwardingSettings()[Lcom/android/internal/telephony/CallForwardInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 685
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 687
    sget-object v2, Lcom/android/phone/settings/VoicemailProviderSettings;->NO_FORWARDING:[Lcom/android/internal/telephony/CallForwardInfo;

    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 691
    :cond_0
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewVMNumber:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mOldVmNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 692
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    sget-object v3, Lcom/android/phone/settings/VoicemailProviderSettings;->NO_FORWARDING:[Lcom/android/internal/telephony/CallForwardInfo;

    if-ne v2, v3, :cond_2

    .line 693
    const/16 v2, 0x190

    invoke-direct {p0, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->showDialogIfForeground(I)V

    .line 694
    return-void

    .line 681
    :cond_1
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewVMNumber:Ljava/lang/String;

    goto :goto_0

    .line 697
    :cond_2
    invoke-static {p0, p1, p2}, Lcom/android/phone/settings/VoicemailProviderSettingsUtil;->save(Landroid/content/Context;Ljava/lang/String;Lcom/android/phone/settings/VoicemailProviderSettings;)V

    .line 698
    iput-boolean v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMChangeCompletedSuccessfully:Z

    .line 699
    iput-boolean v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mFwdChangesRequireRollback:Z

    .line 700
    iput v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMOrFwdSetError:I

    .line 702
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    sget-object v3, Lcom/android/phone/settings/VoicemailProviderSettings;->NO_FORWARDING:[Lcom/android/internal/telephony/CallForwardInfo;

    if-eq v2, v3, :cond_3

    .line 703
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 702
    if-eqz v2, :cond_4

    .line 705
    :cond_3
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->setVoicemailNumberWithCarrier()V

    .line 717
    :goto_1
    return-void

    .line 708
    :cond_4
    sget-object v2, Lcom/android/phone/settings/VoicemailProviderSettings;->FORWARDING_SETTINGS_REASONS:[I

    array-length v1, v2

    .line 709
    .local v1, "numSettingsReasons":I
    new-array v2, v1, [Lcom/android/internal/telephony/CallForwardInfo;

    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 710
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 711
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 712
    sget-object v3, Lcom/android/phone/settings/VoicemailProviderSettings;->FORWARDING_SETTINGS_REASONS:[I

    aget v3, v3, v0

    .line 713
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mGetOptionComplete:Landroid/os/Handler;

    const/16 v5, 0x1f6

    invoke-virtual {v4, v5, v0, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    .line 711
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/telephony/Phone;->getCallForwardingOption(ILandroid/os/Message;)V

    .line 710
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 715
    :cond_5
    const/16 v2, 0x25a

    invoke-direct {p0, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->showDialogIfForeground(I)V

    goto :goto_1
.end method

.method private saveVoiceMailAndForwardingNumberStage2()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 793
    iput-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingChangeResults:Ljava/util/Map;

    .line 794
    iput-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    .line 796
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->resetForwardingChangeState()V

    .line 797
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    .line 798
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    aget-object v1, v4, v3

    .line 800
    .local v1, "fi":Lcom/android/internal/telephony/CallForwardInfo;
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    iget v5, v1, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    invoke-static {v4, v5}, Lcom/android/phone/settings/CallForwardInfoUtil;->infoForReason([Lcom/android/internal/telephony/CallForwardInfo;I)Lcom/android/internal/telephony/CallForwardInfo;

    move-result-object v2

    .line 801
    .local v2, "fiForReason":Lcom/android/internal/telephony/CallForwardInfo;
    invoke-static {v2, v1}, Lcom/android/phone/settings/CallForwardInfoUtil;->isUpdateRequired(Lcom/android/internal/telephony/CallForwardInfo;Lcom/android/internal/telephony/CallForwardInfo;)Z

    move-result v0

    .line 803
    .local v0, "doUpdate":Z
    if-eqz v0, :cond_0

    .line 805
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mExpectedChangeResultReasons:Ljava/util/Collection;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 807
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 808
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSetOptionComplete:Landroid/os/Handler;

    .line 809
    iget v6, v1, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    const/16 v7, 0x1f5

    const/4 v8, 0x0

    .line 808
    invoke-virtual {v5, v7, v6, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    .line 807
    invoke-static {v4, v1, v5}, Lcom/android/phone/settings/CallForwardInfoUtil;->setCallForwardingOption(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/CallForwardInfo;Landroid/os/Message;)V

    .line 797
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 812
    .end local v0    # "doUpdate":Z
    .end local v1    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    .end local v2    # "fiForReason":Lcom/android/internal/telephony/CallForwardInfo;
    :cond_1
    const/16 v4, 0x259

    invoke-direct {p0, v4}, Lcom/android/phone/settings/VoicemailSettingsActivity;->showDialogIfForeground(I)V

    .line 813
    return-void
.end method

.method private setVoicemailNumberWithCarrier()V
    .locals 5

    .prologue
    .line 917
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    .line 918
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 919
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getVoiceMailAlphaTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 920
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewVMNumber:Ljava/lang/String;

    .line 921
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSetOptionComplete:Landroid/os/Handler;

    const/16 v4, 0x1f4

    invoke-static {v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    .line 918
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/Phone;->setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 922
    return-void
.end method

.method private showDialogIfForeground(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 616
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForeground:Z

    if-eqz v0, :cond_0

    .line 617
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->showDialog(I)V

    .line 619
    :cond_0
    return-void
.end method

.method private simulatePreferenceClick(Landroid/preference/Preference;)V
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 545
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    .line 546
    .local v6, "adapter":Landroid/widget/ListAdapter;
    const/4 v3, 0x0

    .local v3, "idx":I
    :goto_0
    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 547
    invoke-interface {v6, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 548
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 549
    invoke-interface {v6, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    const/4 v2, 0x0

    .line 548
    invoke-virtual/range {v0 .. v5}, Landroid/preference/PreferenceScreen;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 553
    :cond_0
    return-void

    .line 546
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private switchToPreviousVoicemailProvider()V
    .locals 11

    .prologue
    .line 927
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    if-nez v6, :cond_0

    .line 928
    return-void

    .line 931
    :cond_0
    iget-boolean v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMChangeCompletedSuccessfully:Z

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mFwdChangesRequireRollback:Z

    if-eqz v6, :cond_5

    .line 932
    :cond_1
    const/16 v6, 0x25b

    invoke-direct {p0, v6}, Lcom/android/phone/settings/VoicemailSettingsActivity;->showDialogIfForeground(I)V

    .line 934
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-static {p0, v6}, Lcom/android/phone/settings/VoicemailProviderSettingsUtil;->load(Landroid/content/Context;Ljava/lang/String;)Lcom/android/phone/settings/VoicemailProviderSettings;

    move-result-object v3

    .line 935
    .local v3, "prevSettings":Lcom/android/phone/settings/VoicemailProviderSettings;
    if-nez v3, :cond_2

    .line 936
    sget-object v6, Lcom/android/phone/settings/VoicemailSettingsActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VoicemailProviderSettings for the key \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 937
    iget-object v8, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    .line 936
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 937
    const-string/jumbo v8, "\" is null but should be loaded."

    .line 936
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    return-void

    .line 941
    :cond_2
    iget-boolean v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMChangeCompletedSuccessfully:Z

    if-eqz v6, :cond_3

    .line 942
    invoke-virtual {v3}, Lcom/android/phone/settings/VoicemailProviderSettings;->getVoicemailNumber()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewVMNumber:Ljava/lang/String;

    .line 943
    sget-object v6, Lcom/android/phone/settings/VoicemailSettingsActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VM change is already completed successfully.Have to revert VM back to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 944
    iget-object v8, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewVMNumber:Ljava/lang/String;

    .line 943
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 944
    const-string/jumbo v8, " again."

    .line 943
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 946
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getVoiceMailAlphaTag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    .line 947
    iget-object v8, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mNewVMNumber:Ljava/lang/String;

    .line 948
    iget-object v9, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mRevertOptionComplete:Landroid/os/Handler;

    const/16 v10, 0x1f4

    invoke-static {v9, v10}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v9

    .line 945
    invoke-virtual {v6, v7, v8, v9}, Lcom/android/internal/telephony/Phone;->setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 951
    :cond_3
    iget-boolean v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mFwdChangesRequireRollback:Z

    if-eqz v6, :cond_6

    .line 952
    sget-object v6, Lcom/android/phone/settings/VoicemailSettingsActivity;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v7, "Requested to rollback forwarding changes."

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    invoke-virtual {v3}, Lcom/android/phone/settings/VoicemailProviderSettings;->getForwardingSettings()[Lcom/android/internal/telephony/CallForwardInfo;

    move-result-object v2

    .line 955
    .local v2, "prevFwdSettings":[Lcom/android/internal/telephony/CallForwardInfo;
    if-eqz v2, :cond_6

    .line 956
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForwardingChangeResults:Ljava/util/Map;

    .line 957
    .local v5, "results":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->resetForwardingChangeState()V

    .line 958
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v2

    if-ge v1, v6, :cond_6

    .line 959
    aget-object v0, v2, v1

    .line 962
    .local v0, "fi":Lcom/android/internal/telephony/CallForwardInfo;
    iget v6, v0, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/AsyncResult;

    .line 963
    .local v4, "result":Landroid/os/AsyncResult;
    if-eqz v4, :cond_4

    iget-object v6, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v6, :cond_4

    .line 964
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mExpectedChangeResultReasons:Ljava/util/Collection;

    iget v7, v0, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 965
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 966
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mRevertOptionComplete:Landroid/os/Handler;

    .line 967
    const/16 v8, 0x1f5

    const/4 v9, 0x0

    .line 966
    invoke-virtual {v7, v8, v1, v9}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v7

    .line 965
    invoke-static {v6, v0, v7}, Lcom/android/phone/settings/CallForwardInfoUtil;->setCallForwardingOption(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/CallForwardInfo;Landroid/os/Message;)V

    .line 958
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 974
    .end local v0    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    .end local v1    # "i":I
    .end local v2    # "prevFwdSettings":[Lcom/android/internal/telephony/CallForwardInfo;
    .end local v3    # "prevSettings":Lcom/android/phone/settings/VoicemailProviderSettings;
    .end local v4    # "result":Landroid/os/AsyncResult;
    .end local v5    # "results":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    :cond_5
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->onRevertDone()V

    .line 976
    :cond_6
    return-void
.end method

.method private updateVMPreferenceWidgets(Ljava/lang/String;)V
    .locals 6
    .param p1, "currentProviderSetting"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 990
    move-object v0, p1

    .line 992
    .local v0, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v3, p1}, Lcom/android/phone/settings/VoicemailProviderListPreference;->getVoicemailProvider(Ljava/lang/String;)Lcom/android/phone/settings/VoicemailProviderListPreference$VoicemailProvider;

    move-result-object v1

    .line 998
    .local v1, "provider":Lcom/android/phone/settings/VoicemailProviderListPreference$VoicemailProvider;
    if-nez v1, :cond_0

    .line 1001
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    const v4, 0x7f0b0371

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/VoicemailProviderListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1002
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailSettings:Landroid/preference/PreferenceScreen;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 1003
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailSettings:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->setIntent(Landroid/content/Intent;)V

    .line 1012
    :goto_0
    return-void

    .line 1007
    :cond_0
    iget-object v2, v1, Lcom/android/phone/settings/VoicemailProviderListPreference$VoicemailProvider;->name:Ljava/lang/String;

    .line 1008
    .local v2, "providerName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v3, v2}, Lcom/android/phone/settings/VoicemailProviderListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1009
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailSettings:Landroid/preference/PreferenceScreen;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 1010
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailSettings:Landroid/preference/PreferenceScreen;

    iget-object v4, v1, Lcom/android/phone/settings/VoicemailProviderListPreference$VoicemailProvider;->intent:Landroid/content/Intent;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private updateVoiceNumberField()V
    .locals 4

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getVoiceMailNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mOldVmNumber:Ljava/lang/String;

    .line 1021
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mOldVmNumber:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1022
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/android/phone/EditPhoneNumberPreference;->setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;

    .line 1023
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    const v1, 0x7f0b04e8

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/EditPhoneNumberPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1029
    :goto_0
    return-void

    .line 1025
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mOldVmNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/phone/EditPhoneNumberPreference;->setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;

    .line 1026
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v1

    .line 1027
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mOldVmNumber:Ljava/lang/String;

    sget-object v3, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 1026
    invoke-virtual {v1, v2, v3}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/EditPhoneNumberPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 15
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 446
    const/4 v1, 0x2

    move/from16 v0, p1

    if-ne v0, v1, :cond_8

    .line 447
    const/4 v8, 0x0

    .line 451
    .local v8, "failure":Z
    iget-boolean v12, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMProviderSettingsForced:Z

    .line 452
    .local v12, "isVMProviderSettingsForced":Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMProviderSettingsForced:Z

    .line 454
    const/4 v14, 0x0

    .line 455
    .local v14, "vmNum":Ljava/lang/String;
    const/4 v1, -0x1

    move/from16 v0, p2

    if-eq v0, v1, :cond_2

    .line 457
    const/4 v8, 0x1

    .line 485
    .end local v14    # "vmNum":Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v8, :cond_7

    .line 487
    if-eqz v12, :cond_1

    .line 488
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->switchToPreviousVoicemailProvider()V

    .line 491
    :cond_1
    return-void

    .line 459
    .restart local v14    # "vmNum":Ljava/lang/String;
    :cond_2
    if-nez p3, :cond_3

    .line 461
    const/4 v8, 0x1

    goto :goto_0

    .line 463
    :cond_3
    const-string/jumbo v1, "com.android.phone.Signout"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 465
    if-eqz v12, :cond_4

    .line 467
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->switchToPreviousVoicemailProvider()V

    .line 476
    :goto_1
    return-void

    .line 469
    :cond_4
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/VoicemailProviderListPreference;->getKey()Ljava/lang/String;

    move-result-object v13

    .line 471
    .local v13, "victim":Ljava/lang/String;
    new-instance v11, Landroid/content/Intent;

    const-string/jumbo v1, "com.android.phone.CallFeaturesSetting.ADD_VOICEMAIL"

    invoke-direct {v11, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 472
    .local v11, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.android.phone.ProviderToIgnore"

    invoke-virtual {v11, v1, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 473
    const/high16 v1, 0x4000000

    invoke-virtual {v11, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 474
    invoke-virtual {p0, v11}, Lcom/android/phone/settings/VoicemailSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 478
    .end local v11    # "i":Landroid/content/Intent;
    .end local v13    # "victim":Ljava/lang/String;
    :cond_5
    const-string/jumbo v1, "com.android.phone.VoicemailNumber"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 479
    .local v14, "vmNum":Ljava/lang/String;
    if-eqz v14, :cond_6

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 481
    :cond_6
    const/4 v8, 0x1

    goto :goto_0

    .line 493
    .end local v14    # "vmNum":Ljava/lang/String;
    :cond_7
    iput-boolean v12, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mChangingVMorFwdDueToProviderChange:Z

    .line 494
    const-string/jumbo v1, "com.android.phone.ForwardingNumber"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 498
    .local v9, "fwdNum":Ljava/lang/String;
    const-string/jumbo v1, "com.android.phone.ForwardingNumberTime"

    const/16 v2, 0x14

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 501
    .local v10, "fwdNumTime":I
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/VoicemailProviderListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 502
    new-instance v2, Lcom/android/phone/settings/VoicemailProviderSettings;

    invoke-direct {v2, v14, v9, v10}, Lcom/android/phone/settings/VoicemailProviderSettings;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 501
    invoke-direct {p0, v1, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->saveVoiceMailAndForwardingNumber(Ljava/lang/String;Lcom/android/phone/settings/VoicemailProviderSettings;)V

    .line 503
    return-void

    .line 506
    .end local v8    # "failure":Z
    .end local v9    # "fwdNum":Ljava/lang/String;
    .end local v10    # "fwdNumTime":I
    .end local v12    # "isVMProviderSettingsForced":Z
    :cond_8
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_10

    .line 507
    const/4 v1, -0x1

    move/from16 v0, p2

    if-eq v0, v1, :cond_9

    .line 509
    return-void

    .line 512
    :cond_9
    const/4 v7, 0x0

    .line 514
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 515
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "data1"

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 514
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 516
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_a

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_c

    .line 527
    :cond_a
    if-eqz v7, :cond_b

    .line 528
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 518
    :cond_b
    return-void

    .line 520
    :cond_c
    :try_start_1
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    if-eqz v1, :cond_e

    .line 521
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/phone/EditPhoneNumberPreference;->onPickActivityResult(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527
    :goto_2
    if-eqz v7, :cond_d

    .line 528
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 525
    :cond_d
    return-void

    .line 523
    :cond_e
    :try_start_2
    sget-object v1, Lcom/android/phone/settings/VoicemailSettingsActivity;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "VoicemailSettingsActivity destroyed while setting contacts."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 526
    .end local v7    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    .line 527
    if-eqz v7, :cond_f

    .line 528
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 526
    :cond_f
    throw v1

    .line 533
    :cond_10
    invoke-super/range {p0 .. p3}, Lmiui/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 534
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/16 v2, 0x1f6

    .line 638
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 639
    packed-switch p2, :pswitch_data_0

    .line 663
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 664
    :goto_1
    const-string/jumbo v1, "com.android.phone.CallFeaturesSetting.ADD_VOICEMAIL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 665
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->finish()V

    .line 667
    :cond_1
    return-void

    .line 641
    :pswitch_0
    iget v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mCurrentDialogId:I

    if-ne v1, v2, :cond_0

    .line 644
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->switchToPreviousVoicemailProvider()V

    goto :goto_0

    .line 648
    :pswitch_1
    iget v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mCurrentDialogId:I

    if-ne v1, v2, :cond_2

    .line 651
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->setVoicemailNumberWithCarrier()V

    .line 655
    :goto_2
    return-void

    .line 653
    :cond_2
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->finish()V

    goto :goto_2

    .line 663
    :cond_3
    const/4 v0, 0x0

    .local v0, "action":Ljava/lang/String;
    goto :goto_1

    .line 639
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 211
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 213
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Landroid/os/UserManager;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    .line 214
    .local v1, "userManager":Landroid/os/UserManager;
    invoke-virtual {v1}, Landroid/os/UserManager;->isPrimaryUser()Z

    move-result v3

    if-nez v3, :cond_0

    .line 215
    const v3, 0x7f0b0304

    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 217
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->finish()V

    .line 218
    return-void

    .line 222
    :cond_0
    if-nez p1, :cond_1

    .line 223
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.android.phone.CallFeaturesSetting.ADD_VOICEMAIL"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    .line 222
    :cond_1
    iput-boolean v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mShowVoicemailPreference:Z

    .line 225
    new-instance v2, Lcom/android/phone/SubscriptionInfoHelper;

    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/phone/SubscriptionInfoHelper;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    .line 226
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    .line 227
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0321

    .line 226
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/phone/SubscriptionInfoHelper;->setActionBarTitle(Landroid/app/ActionBar;Landroid/content/res/Resources;I)V

    .line 228
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    invoke-virtual {v2}, Lcom/android/phone/SubscriptionInfoHelper;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 229
    const v2, 0x7f060040

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->addPreferencesFromResource(I)V

    .line 232
    const v2, 0x7f0b0323

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 231
    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailNotificationPreference:Landroid/preference/Preference;

    .line 233
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.settings.CHANNEL_NOTIFICATION_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 234
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "android.provider.extra.CHANNEL_ID"

    .line 235
    const-string/jumbo v3, "voiceMail"

    .line 234
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    const-string/jumbo v2, "android.provider.extra.APP_PACKAGE"

    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailNotificationPreference:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 238
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1, "dialogId"    # I

    .prologue
    .line 569
    invoke-static {p0, p1}, Lcom/android/phone/settings/VoicemailDialogUtil;->getDialog(Lcom/android/phone/settings/VoicemailSettingsActivity;I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onDialogClosed(Lcom/android/phone/EditPhoneNumberPreference;I)V
    .locals 3
    .param p1, "preference"    # Lcom/android/phone/EditPhoneNumberPreference;
    .param p2, "buttonClicked"    # I

    .prologue
    .line 576
    const/4 v1, -0x2

    if-ne p2, v1, :cond_0

    .line 577
    return-void

    .line 580
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    if-ne p1, v1, :cond_1

    .line 581
    new-instance v0, Lcom/android/phone/settings/VoicemailProviderSettings;

    .line 582
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    invoke-virtual {v1}, Lcom/android/phone/EditPhoneNumberPreference;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    .line 583
    sget-object v2, Lcom/android/phone/settings/VoicemailProviderSettings;->NO_FORWARDING:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 581
    invoke-direct {v0, v1, v2}, Lcom/android/phone/settings/VoicemailProviderSettings;-><init>(Ljava/lang/String;[Lcom/android/internal/telephony/CallForwardInfo;)V

    .line 584
    .local v0, "newSettings":Lcom/android/phone/settings/VoicemailProviderSettings;
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/VoicemailProviderListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->saveVoiceMailAndForwardingNumber(Ljava/lang/String;Lcom/android/phone/settings/VoicemailProviderSettings;)V

    .line 586
    .end local v0    # "newSettings":Lcom/android/phone/settings/VoicemailProviderSettings;
    :cond_1
    return-void
.end method

.method public onGetDefaultNumber(Lcom/android/phone/EditPhoneNumberPreference;)Ljava/lang/String;
    .locals 3
    .param p1, "preference"    # Lcom/android/phone/EditPhoneNumberPreference;

    .prologue
    const/4 v2, 0x0

    .line 416
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    if-ne p1, v1, :cond_0

    .line 420
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->updateVoiceNumberField()V

    .line 421
    return-object v2

    .line 424
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getVoiceMailNumber()Ljava/lang/String;

    move-result-object v0

    .line 425
    .local v0, "vmDisplay":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 428
    return-object v2

    .line 433
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b0322

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 314
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 315
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->onBackPressed()V

    .line 316
    const/4 v0, 0x1

    return v0

    .line 318
    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 308
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onPause()V

    .line 309
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForeground:Z

    .line 310
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    .line 377
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    if-ne p1, v2, :cond_1

    move-object v0, p2

    .line 378
    check-cast v0, Ljava/lang/String;

    .line 381
    .local v0, "newProviderKey":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 383
    return v4

    .line 385
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->updateVMPreferenceWidgets(Ljava/lang/String;)V

    .line 388
    invoke-static {p0, v0}, Lcom/android/phone/settings/VoicemailProviderSettingsUtil;->load(Landroid/content/Context;Ljava/lang/String;)Lcom/android/phone/settings/VoicemailProviderSettings;

    move-result-object v1

    .line 393
    .local v1, "newProviderSettings":Lcom/android/phone/settings/VoicemailProviderSettings;
    if-nez v1, :cond_2

    .line 395
    sget-object v2, Lcom/android/phone/settings/VoicemailSettingsActivity;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "Saved preferences not found - invoking config"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    iput-boolean v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMProviderSettingsForced:Z

    .line 397
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailSettings:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->simulatePreferenceClick(Landroid/preference/Preference;)V

    .line 406
    .end local v0    # "newProviderKey":Ljava/lang/String;
    .end local v1    # "newProviderSettings":Lcom/android/phone/settings/VoicemailProviderSettings;
    :cond_1
    :goto_0
    return v4

    .line 401
    .restart local v0    # "newProviderKey":Ljava/lang/String;
    .restart local v1    # "newProviderSettings":Lcom/android/phone/settings/VoicemailProviderSettings;
    :cond_2
    iput-boolean v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mChangingVMorFwdDueToProviderChange:Z

    .line 402
    invoke-direct {p0, v0, v1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->saveVoiceMailAndForwardingNumber(Ljava/lang/String;Lcom/android/phone/settings/VoicemailProviderSettings;)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 323
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    if-ne p2, v1, :cond_0

    .line 324
    return v4

    .line 325
    :cond_0
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailSettings:Landroid/preference/PreferenceScreen;

    invoke-virtual {v2}, Landroid/preference/PreferenceScreen;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, p2

    .line 331
    check-cast v1, Landroid/preference/PreferenceScreen;

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 332
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_1

    .line 335
    invoke-virtual {v0}, Landroid/app/Dialog;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 336
    invoke-virtual {v0}, Landroid/app/Dialog;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 342
    :cond_1
    const-string/jumbo v1, "button_voicemail_key"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/EditPhoneNumberPreference;

    .line 341
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    .line 343
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    invoke-virtual {v1, p0, v4, p0}, Lcom/android/phone/EditPhoneNumberPreference;->setParentActivity(Landroid/app/Activity;ILcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;)V

    .line 344
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    invoke-virtual {v1, p0}, Lcom/android/phone/EditPhoneNumberPreference;->setDialogOnClosedListener(Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;)V

    .line 345
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    const v2, 0x7f0b0475

    invoke-virtual {v1, v2}, Lcom/android/phone/EditPhoneNumberPreference;->setDialogTitle(I)V

    .line 346
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->updateVoiceNumberField()V

    .line 348
    invoke-virtual {p2}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 352
    invoke-virtual {p2}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 353
    return v4

    .line 358
    :cond_2
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    .line 359
    iput-boolean v3, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMProviderSettingsForced:Z

    .line 360
    return v3

    .line 363
    .end local v0    # "dialog":Landroid/app/Dialog;
    :cond_3
    return v3
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 562
    invoke-super {p0, p1, p2}, Lmiui/preference/PreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 563
    iput p1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mCurrentDialogId:I

    .line 564
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 242
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 243
    iput-boolean v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mForeground:Z

    .line 245
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 247
    .local v0, "prefSet":Landroid/preference/PreferenceScreen;
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    if-nez v1, :cond_0

    .line 249
    const-string/jumbo v1, "button_voicemail_key"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/EditPhoneNumberPreference;

    .line 248
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    .line 251
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    if-eqz v1, :cond_1

    .line 252
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    invoke-virtual {v1, p0, v2, p0}, Lcom/android/phone/EditPhoneNumberPreference;->setParentActivity(Landroid/app/Activity;ILcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;)V

    .line 253
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    invoke-virtual {v1, p0}, Lcom/android/phone/EditPhoneNumberPreference;->setDialogOnClosedListener(Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;)V

    .line 254
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mSubMenuVoicemailSettings:Lcom/android/phone/EditPhoneNumberPreference;

    const v2, 0x7f0b0475

    invoke-virtual {v1, v2}, Lcom/android/phone/EditPhoneNumberPreference;->setDialogTitle(I)V

    .line 258
    :cond_1
    const-string/jumbo v1, "button_voicemail_provider_key"

    .line 257
    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/VoicemailProviderListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    .line 259
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/phone/settings/VoicemailProviderListPreference;->init(Lcom/android/internal/telephony/Phone;Landroid/content/Intent;)V

    .line 260
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v1, p0}, Lcom/android/phone/settings/VoicemailProviderListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 261
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/VoicemailProviderListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mPreviousVMProviderKey:Ljava/lang/String;

    .line 263
    const-string/jumbo v1, "button_voicemail_setting_key"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailSettings:Landroid/preference/PreferenceScreen;

    .line 265
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->maybeHidePublicSettings()V

    .line 267
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/VoicemailProviderListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->updateVMPreferenceWidgets(Ljava/lang/String;)V

    .line 274
    iget-boolean v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mShowVoicemailPreference:Z

    if-eqz v1, :cond_2

    .line 276
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/VoicemailProviderListPreference;->hasMoreThanOneVoicemailProvider()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 278
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    invoke-direct {p0, v1}, Lcom/android/phone/settings/VoicemailSettingsActivity;->simulatePreferenceClick(Landroid/preference/Preference;)V

    .line 283
    :goto_0
    iput-boolean v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mShowVoicemailPreference:Z

    .line 286
    :cond_2
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSettingsActivity;->updateVoiceNumberField()V

    .line 287
    iput-boolean v4, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVMProviderSettingsForced:Z

    .line 288
    return-void

    .line 280
    :cond_3
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    const-string/jumbo v2, ""

    invoke-virtual {p0, v1, v2}, Lcom/android/phone/settings/VoicemailSettingsActivity;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    .line 281
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSettingsActivity;->mVoicemailProviders:Lcom/android/phone/settings/VoicemailProviderListPreference;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/VoicemailProviderListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method
