.class Lcom/android/phone/settings/VoicemailSetting$3;
.super Landroid/os/Handler;
.source "VoicemailSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/VoicemailSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/VoicemailSetting;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/VoicemailSetting;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/VoicemailSetting;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    .line 951
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 954
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/os/AsyncResult;

    .line 955
    .local v4, "result":Landroid/os/AsyncResult;
    const/4 v1, 0x0

    .line 956
    .local v1, "done":Z
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    .line 998
    :cond_0
    :goto_0
    if-eqz v1, :cond_3

    .line 999
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string/jumbo v5, "All VM provider related changes done"

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    .line 1000
    :cond_1
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-get1(Lcom/android/phone/settings/VoicemailSetting;)Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1001
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    const/16 v6, 0x641

    invoke-static {v5, v6}, Lcom/android/phone/settings/VoicemailSetting;->-wrap3(Lcom/android/phone/settings/VoicemailSetting;I)V

    .line 1003
    :cond_2
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-wrap4(Lcom/android/phone/settings/VoicemailSetting;)V

    .line 1005
    :cond_3
    return-void

    .line 958
    :pswitch_0
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    iput-object v4, v7, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    .line 959
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    iget-object v8, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v8}, Lcom/android/phone/settings/VoicemailSetting;->-wrap2(Lcom/android/phone/settings/VoicemailSetting;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_4

    move v5, v6

    :cond_4
    iput-boolean v5, v7, Lcom/android/phone/settings/VoicemailSetting;->mVMChangeCompletedSuccesfully:Z

    .line 960
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "VM change complete msg, VM change done = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 961
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    iget-boolean v6, v6, Lcom/android/phone/settings/VoicemailSetting;->mVMChangeCompletedSuccesfully:Z

    invoke-static {v6}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    .line 960
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    .line 962
    :cond_5
    const/4 v1, 0x1

    .line 963
    goto :goto_0

    .line 965
    :pswitch_1
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->-get1(Lcom/android/phone/settings/VoicemailSetting;)Ljava/util/Map;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 966
    iget-object v7, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v7, :cond_8

    .line 967
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v7

    if-eqz v7, :cond_6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Error in setting fwd# "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 968
    iget-object v8, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v8}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    .line 967
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    .line 972
    :cond_6
    :goto_1
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->-wrap0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v0

    .line 973
    .local v0, "completed":Z
    if-eqz v0, :cond_0

    .line 974
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->-wrap1(Lcom/android/phone/settings/VoicemailSetting;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_9

    .line 975
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v5

    if-eqz v5, :cond_7

    const-string/jumbo v5, "Overall fwd changes completed ok, starting vm change"

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    .line 976
    :cond_7
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-virtual {v5}, Lcom/android/phone/settings/VoicemailSetting;->setVMNumberWithCarrier()V

    goto/16 :goto_0

    .line 970
    .end local v0    # "completed":Z
    :cond_8
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v7

    if-eqz v7, :cond_6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Success in setting fwd# "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    goto :goto_1

    .line 978
    .restart local v0    # "completed":Z
    :cond_9
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v7

    if-eqz v7, :cond_a

    const-string/jumbo v7, "Overall fwd changes completed, failure"

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    .line 979
    :cond_a
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    iput-boolean v5, v7, Lcom/android/phone/settings/VoicemailSetting;->mFwdChangesRequireRollback:Z

    .line 981
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-get1(Lcom/android/phone/settings/VoicemailSetting;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 982
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/os/AsyncResult;>;>;"
    :cond_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 983
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 984
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/AsyncResult;

    iget-object v5, v5, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_b

    .line 986
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-get0(Lcom/android/phone/settings/VoicemailSetting;)Z

    move-result v5

    if-eqz v5, :cond_c

    const-string/jumbo v5, "Rollback will be required"

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->-wrap5(Ljava/lang/String;)V

    .line 987
    :cond_c
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting$3;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    iput-boolean v6, v5, Lcom/android/phone/settings/VoicemailSetting;->mFwdChangesRequireRollback:Z

    .line 991
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    :cond_d
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 956
    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
