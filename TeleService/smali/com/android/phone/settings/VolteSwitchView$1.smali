.class Lcom/android/phone/settings/VolteSwitchView$1;
.super Landroid/content/BroadcastReceiver;
.source "VolteSwitchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/VolteSwitchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/VolteSwitchView;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/VolteSwitchView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/VolteSwitchView$1;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    .line 144
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 147
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "action":Ljava/lang/String;
    sget-object v2, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onReceive action="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const-string/jumbo v2, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 150
    const-string/jumbo v2, "phone"

    sget v3, Lmiui/telephony/SubscriptionManager;->INVALID_PHONE_ID:I

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 151
    .local v1, "phoneId":I
    invoke-static {v1}, Lmiui/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/android/phone/settings/VolteSwitchView;->-get0()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 152
    :cond_0
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$1;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-virtual {v2}, Lcom/android/phone/settings/VolteSwitchView;->updateVolteButtonUI()V

    .line 157
    .end local v1    # "phoneId":I
    :cond_1
    :goto_0
    return-void

    .line 154
    :cond_2
    const-string/jumbo v2, "com.android.ims.IMS_SERVICE_UP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 155
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$1;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get2(Lcom/android/phone/settings/VolteSwitchView;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/VolteSwitchView$1;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v3}, Lcom/android/phone/settings/VolteSwitchView;->-get2(Lcom/android/phone/settings/VolteSwitchView;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x5dc

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method
