.class public Lcom/android/phone/settings/GsmUmtsCallForwardOptions;
.super Lcom/android/phone/settings/TimeConsumingPreferenceActivity;
.source "GsmUmtsCallForwardOptions.java"


# static fields
.field private static final NUM_PROJECTION:[Ljava/lang/String;


# instance fields
.field private DBG:Z

.field private mButtonCFB:Lcom/android/phone/settings/CallForwardEditPreference;

.field private mButtonCFNRc:Lcom/android/phone/settings/CallForwardEditPreference;

.field private mButtonCFNRy:Lcom/android/phone/settings/CallForwardEditPreference;

.field private mButtonCFU:Lcom/android/phone/settings/CallForwardEditPreference;

.field private mFirstResume:Z

.field private mHasShowSuppServiceDialog:Z

.field private mIcicle:Landroid/os/Bundle;

.field private mInitIndex:I

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private final mPreferences:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/phone/settings/CallForwardEditPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceClass:I

.field private mSubscription:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "data1"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->NUM_PROJECTION:[Ljava/lang/String;

    .line 34
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;-><init>()V

    .line 37
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->DBG:Z

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    .line 57
    iput v1, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mInitIndex:I

    .line 60
    iput-boolean v1, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mHasShowSuppServiceDialog:Z

    .line 62
    iput v1, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mSubscription:I

    .line 34
    return-void

    :cond_0
    move v0, v1

    .line 37
    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->DBG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "GsmUmtsCallForwardOptions"

    const-string/jumbo v1, "onActivityResult: done"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 195
    const-string/jumbo v0, "GsmUmtsCallForwardOptions"

    const-string/jumbo v1, "onActivityResult: contact picker result not OK."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return-void

    .line 198
    :cond_1
    const/4 v6, 0x0

    .line 200
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v10

    .line 201
    .local v10, "uri":Landroid/net/Uri;
    const-string/jumbo v9, ""

    .line 202
    .local v9, "number":Ljava/lang/String;
    const-string/jumbo v0, "GsmUmtsCallForwardOptions"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "uri:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-virtual {v10}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "tel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 204
    invoke-virtual {v10}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 215
    .end local v6    # "cursor":Landroid/database/Cursor;
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 236
    :goto_1
    if-eqz v6, :cond_2

    .line 237
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 240
    .end local v9    # "number":Ljava/lang/String;
    .end local v10    # "uri":Landroid/net/Uri;
    :cond_2
    :goto_2
    return-void

    .line 206
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v9    # "number":Ljava/lang/String;
    .restart local v10    # "uri":Landroid/net/Uri;
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 207
    sget-object v2, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->NUM_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 206
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 208
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    .line 209
    :cond_4
    const-string/jumbo v0, "GsmUmtsCallForwardOptions"

    const-string/jumbo v1, "onActivityResult: bad contact data, no results found."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    if-eqz v6, :cond_5

    .line 237
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 210
    :cond_5
    return-void

    .line 212
    :cond_6
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 217
    .end local v6    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v0, v9}, Lcom/android/phone/settings/CallForwardEditPreference;->onPickActivityResult(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 231
    .end local v9    # "number":Ljava/lang/String;
    .end local v10    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v8

    .line 236
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    if-eqz v6, :cond_2

    .line 237
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 220
    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v9    # "number":Ljava/lang/String;
    .restart local v10    # "uri":Landroid/net/Uri;
    :pswitch_1
    :try_start_3
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v0, v9}, Lcom/android/phone/settings/CallForwardEditPreference;->onPickActivityResult(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 233
    .end local v9    # "number":Ljava/lang/String;
    .end local v10    # "uri":Landroid/net/Uri;
    :catch_1
    move-exception v7

    .line 236
    .local v7, "e":Ljava/lang/Exception;
    if-eqz v6, :cond_2

    .line 237
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 223
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v9    # "number":Ljava/lang/String;
    .restart local v10    # "uri":Landroid/net/Uri;
    :pswitch_2
    :try_start_4
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v0, v9}, Lcom/android/phone/settings/CallForwardEditPreference;->onPickActivityResult(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 235
    .end local v9    # "number":Ljava/lang/String;
    .end local v10    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    .line 236
    if-eqz v6, :cond_7

    .line 237
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 235
    :cond_7
    throw v0

    .line 226
    .restart local v9    # "number":Ljava/lang/String;
    .restart local v10    # "uri":Landroid/net/Uri;
    :pswitch_3
    :try_start_5
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v0, v9}, Lcom/android/phone/settings/CallForwardEditPreference;->onPickActivityResult(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 70
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    invoke-static {p0}, Lcom/android/phone/settings/SimPickerPreference;->showSimPicker(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    return-void

    .line 76
    :cond_0
    const v3, 0x7f060019

    invoke-virtual {p0, v3}, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->addPreferencesFromResource(I)V

    .line 79
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 80
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v4

    .line 79
    invoke-static {v3, v4}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v3

    iput v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mSubscription:I

    .line 81
    iget v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mSubscription:I

    invoke-static {v3}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    iput-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 83
    const-string/jumbo v3, "GsmUmtsCallForwardOptions"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Call Forwarding options, subscription ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mSubscription:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 87
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "service_class"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mServiceClass:I

    .line 89
    iget-boolean v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->DBG:Z

    if-eqz v3, :cond_1

    const-string/jumbo v3, "GsmUmtsCallForwardOptions"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "serviceClass: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mServiceClass:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    .line 92
    .local v2, "prefSet":Landroid/preference/PreferenceScreen;
    const-string/jumbo v3, "button_cfu_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/android/phone/settings/CallForwardEditPreference;

    iput-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/settings/CallForwardEditPreference;

    .line 93
    const-string/jumbo v3, "button_cfb_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/android/phone/settings/CallForwardEditPreference;

    iput-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/settings/CallForwardEditPreference;

    .line 94
    const-string/jumbo v3, "button_cfnry_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/android/phone/settings/CallForwardEditPreference;

    iput-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/settings/CallForwardEditPreference;

    .line 95
    const-string/jumbo v3, "button_cfnrc_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/android/phone/settings/CallForwardEditPreference;

    iput-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/settings/CallForwardEditPreference;

    .line 97
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v4, v4, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    invoke-virtual {v3, p0, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->setParentActivity(Landroid/app/Activity;I)V

    .line 98
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v4, v4, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    invoke-virtual {v3, p0, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->setParentActivity(Landroid/app/Activity;I)V

    .line 99
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v4, v4, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    invoke-virtual {v3, p0, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->setParentActivity(Landroid/app/Activity;I)V

    .line 100
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/settings/CallForwardEditPreference;

    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v4, v4, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    invoke-virtual {v3, p0, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->setParentActivity(Landroid/app/Activity;I)V

    .line 102
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/settings/CallForwardEditPreference;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFU:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mServiceClass:I

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->setServiceClass(I)V

    .line 108
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFB:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mServiceClass:I

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->setServiceClass(I)V

    .line 109
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRy:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mServiceClass:I

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->setServiceClass(I)V

    .line 110
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mButtonCFNRc:Lcom/android/phone/settings/CallForwardEditPreference;

    iget v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mServiceClass:I

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->setServiceClass(I)V

    .line 116
    iput-boolean v6, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mFirstResume:Z

    .line 117
    iput-object p1, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mIcicle:Landroid/os/Bundle;

    .line 119
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 120
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_2

    .line 122
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 124
    :cond_2
    return-void
.end method

.method public onError(Landroid/preference/Preference;I)V
    .locals 1
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "error"    # I

    .prologue
    .line 254
    invoke-super {p0, p1, p2}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    .line 255
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 256
    return-void
.end method

.method public onException(Landroid/preference/Preference;Lcom/android/internal/telephony/CommandException;)V
    .locals 1
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "exception"    # Lcom/android/internal/telephony/CommandException;

    .prologue
    .line 260
    invoke-super {p0, p1, p2}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onException(Landroid/preference/Preference;Lcom/android/internal/telephony/CommandException;)V

    .line 261
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 262
    return-void
.end method

.method public onFinished(Landroid/preference/Preference;Z)V
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "reading"    # Z

    .prologue
    .line 183
    iget v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mInitIndex:I

    iget-object v1, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->isFinishing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 184
    iget v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mInitIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mInitIndex:I

    .line 185
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mInitIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/settings/CallForwardEditPreference;

    iget v1, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mSubscription:I

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2, v1}, Lcom/android/phone/settings/CallForwardEditPreference;->init(Lcom/android/phone/settings/TimeConsumingPreferenceListener;ZI)V

    .line 188
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onFinished(Landroid/preference/Preference;Z)V

    .line 189
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 244
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 245
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 246
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->finish()V

    .line 247
    const/4 v1, 0x1

    return v1

    .line 249
    :cond_0
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 128
    invoke-super {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onResume()V

    .line 130
    iget-boolean v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mFirstResume:Z

    if-eqz v4, :cond_4

    .line 131
    iget-boolean v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mHasShowSuppServiceDialog:Z

    if-eqz v4, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->finish()V

    .line 133
    return-void

    .line 134
    :cond_0
    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-static {v4, v6}, Lcom/android/phone/MiuiPhoneUtils;->maybeShowDialogForSuppService(Lcom/android/internal/telephony/Phone;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 136
    iput-boolean v6, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mHasShowSuppServiceDialog:Z

    .line 137
    return-void

    .line 140
    :cond_1
    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mIcicle:Landroid/os/Bundle;

    if-nez v4, :cond_5

    .line 141
    iget-boolean v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->DBG:Z

    if-eqz v4, :cond_2

    const-string/jumbo v4, "GsmUmtsCallForwardOptions"

    const-string/jumbo v5, "start to init "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_2
    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/internal/telephony/IccCard;->getIccFdnEnabled()Z

    move-result v4

    if-nez v4, :cond_3

    .line 144
    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    iget v5, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mInitIndex:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/phone/settings/CallForwardEditPreference;

    iget v5, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mSubscription:I

    invoke-virtual {v4, p0, v7, v5}, Lcom/android/phone/settings/CallForwardEditPreference;->init(Lcom/android/phone/settings/TimeConsumingPreferenceListener;ZI)V

    .line 160
    :cond_3
    iput-boolean v7, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mFirstResume:Z

    .line 161
    iput-object v8, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mIcicle:Landroid/os/Bundle;

    .line 163
    :cond_4
    return-void

    .line 147
    :cond_5
    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iput v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mInitIndex:I

    .line 149
    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "pref$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/phone/settings/CallForwardEditPreference;

    .line 150
    .local v2, "pref":Lcom/android/phone/settings/CallForwardEditPreference;
    iget-object v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mIcicle:Landroid/os/Bundle;

    invoke-virtual {v2}, Lcom/android/phone/settings/CallForwardEditPreference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 151
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v4, "toggle"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->setToggled(Z)Lcom/android/phone/settings/EditPhoneNumberPreference;

    .line 152
    new-instance v1, Lcom/android/internal/telephony/CallForwardInfo;

    invoke-direct {v1}, Lcom/android/internal/telephony/CallForwardInfo;-><init>()V

    .line 153
    .local v1, "cf":Lcom/android/internal/telephony/CallForwardInfo;
    const-string/jumbo v4, "number"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    .line 154
    const-string/jumbo v4, "status"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    .line 155
    invoke-virtual {v2, v1}, Lcom/android/phone/settings/CallForwardEditPreference;->handleCallForwardResult(Lcom/android/internal/telephony/CallForwardInfo;)V

    .line 156
    const-string/jumbo v4, "enabled"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v2, v1, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->handleCallForwardResult(Lcom/android/internal/telephony/CallForwardInfo;Z)V

    .line 157
    iget v4, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mSubscription:I

    invoke-virtual {v2, p0, v6, v4}, Lcom/android/phone/settings/CallForwardEditPreference;->init(Lcom/android/phone/settings/TimeConsumingPreferenceListener;ZI)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 167
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 169
    iget-object v3, p0, Lcom/android/phone/settings/GsmUmtsCallForwardOptions;->mPreferences:Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "pref$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/CallForwardEditPreference;

    .line 170
    .local v1, "pref":Lcom/android/phone/settings/CallForwardEditPreference;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 171
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "toggle"

    invoke-virtual {v1}, Lcom/android/phone/settings/CallForwardEditPreference;->isToggled()Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 172
    iget-object v3, v1, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    if-eqz v3, :cond_0

    .line 173
    const-string/jumbo v3, "number"

    iget-object v4, v1, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget-object v4, v4, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string/jumbo v3, "status"

    iget-object v4, v1, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget v4, v4, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 175
    const-string/jumbo v3, "enabled"

    invoke-virtual {v1}, Lcom/android/phone/settings/CallForwardEditPreference;->isEnabled()Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 177
    :cond_0
    invoke-virtual {v1}, Lcom/android/phone/settings/CallForwardEditPreference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 179
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "pref":Lcom/android/phone/settings/CallForwardEditPreference;
    :cond_1
    return-void
.end method
