.class Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;
.super Lcom/android/phone/UIDataLoadTask;
.source "AutoRecordWhiteListSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/AutoRecordWhiteListSetting;->processPickResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/phone/UIDataLoadTask",
        "<",
        "Lcom/android/phone/settings/AutoRecordWhiteListSetting;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    .param p2, "$anonymous0"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .line 210
    invoke-direct {p0, p2}, Lcom/android/phone/UIDataLoadTask;-><init>(Ljava/lang/Object;)V

    .line 1
    return-void
.end method


# virtual methods
.method protected doPrepare()V
    .locals 3

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;->getReferenceObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .line 214
    .local v0, "autoRecordUI":Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get3(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0b059b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 215
    return-void
.end method

.method protected doTask([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 219
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;->getReferenceObject()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .local v6, "autoRecordUI":Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    move-object v13, p1

    .line 220
    check-cast v13, [Landroid/os/Parcelable;

    .line 221
    .local v13, "uris":[Landroid/os/Parcelable;
    array-length v2, v13

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v11, v13, v1

    .local v11, "p":Landroid/os/Parcelable;
    move-object v12, v11

    .line 222
    check-cast v12, Landroid/net/Uri;

    .line 223
    .local v12, "uri":Landroid/net/Uri;
    const-string/jumbo v3, "tel"

    invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 224
    invoke-virtual {v12}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v10

    .line 225
    .local v10, "number":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 226
    invoke-static {v6}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get2(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v5, Landroid/util/Pair;

    invoke-direct {v5, v10, v10}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v3, v5}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap1(Ljava/util/HashMap;Landroid/util/Pair;)V

    .line 221
    .end local v10    # "number":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 230
    .end local v11    # "p":Landroid/os/Parcelable;
    .end local v12    # "uri":Landroid/net/Uri;
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 231
    .local v9, "idSetBuilder":Ljava/lang/StringBuilder;
    const/4 v8, 0x1

    .line 232
    .local v8, "first":Z
    array-length v1, v13

    :goto_1
    if-ge v0, v1, :cond_4

    aget-object v11, v13, v0

    .restart local v11    # "p":Landroid/os/Parcelable;
    move-object v12, v11

    .line 233
    check-cast v12, Landroid/net/Uri;

    .line 234
    .restart local v12    # "uri":Landroid/net/Uri;
    const-string/jumbo v2, "content"

    invoke-virtual {v12}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 235
    if-eqz v8, :cond_3

    .line 236
    const/4 v8, 0x0

    .line 237
    invoke-virtual {v12}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 239
    :cond_3
    const/16 v2, 0x2c

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v12}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 243
    .end local v11    # "p":Landroid/os/Parcelable;
    .end local v12    # "uri":Landroid/net/Uri;
    :cond_4
    if-nez v8, :cond_7

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 244
    const/4 v7, 0x0

    .line 245
    .local v7, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    invoke-virtual {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get0()[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "_id IN ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, ")"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 246
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_7

    .line 248
    :cond_5
    :goto_3
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 249
    if-eqz v6, :cond_5

    .line 250
    invoke-static {v6}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get2(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Ljava/util/HashMap;

    move-result-object v0

    new-instance v1, Landroid/util/Pair;

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 251
    const/4 v3, 0x1

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 250
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap1(Ljava/util/HashMap;Landroid/util/Pair;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 254
    :catchall_0
    move-exception v0

    .line 255
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 254
    throw v0

    .line 255
    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 259
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_7
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap2(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V

    .line 260
    return-object v4
.end method

.method protected doUI(Ljava/lang/Object;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;->getReferenceObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .line 266
    .local v0, "autoRecordUI":Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap4(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V

    .line 267
    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap6(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V

    .line 268
    return-void
.end method
