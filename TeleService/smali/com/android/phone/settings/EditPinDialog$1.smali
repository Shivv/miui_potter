.class Lcom/android/phone/settings/EditPinDialog$1;
.super Landroid/os/Handler;
.source "EditPinDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/EditPinDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/EditPinDialog;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/EditPinDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/EditPinDialog;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    .line 430
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/high16 v7, 0x1040000

    const v6, 0x1010355

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 433
    const/4 v0, 0x0

    .line 435
    .local v0, "ar":Landroid/os/AsyncResult;
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 525
    .end local v0    # "ar":Landroid/os/AsyncResult;
    :goto_0
    return-void

    .line 437
    .restart local v0    # "ar":Landroid/os/AsyncResult;
    :pswitch_0
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get3(Lcom/android/phone/settings/EditPinDialog;)Lmiui/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/app/ProgressDialog;->show()V

    .line 438
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-wrap1(Lcom/android/phone/settings/EditPinDialog;)V

    goto :goto_0

    .line 441
    :pswitch_1
    iget-object v2, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/android/phone/settings/EditPinDialog;->-set0(Lcom/android/phone/settings/EditPinDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 442
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-virtual {v1}, Lcom/android/phone/settings/EditPinDialog;->show()V

    .line 443
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get3(Lcom/android/phone/settings/EditPinDialog;)Lmiui/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 446
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "ar":Landroid/os/AsyncResult;
    check-cast v0, Landroid/os/AsyncResult;

    .line 447
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    .line 448
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get0(Lcom/android/phone/settings/EditPinDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0696

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/android/phone/settings/EditPinDialog$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 450
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1, v3}, Lcom/android/phone/settings/EditPinDialog;->-wrap0(Lcom/android/phone/settings/EditPinDialog;Z)V

    goto :goto_0

    .line 454
    .local v0, "ar":Landroid/os/AsyncResult;
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "ar":Landroid/os/AsyncResult;
    check-cast v0, Landroid/os/AsyncResult;

    .line 455
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get2(Lcom/android/phone/settings/EditPinDialog;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->updatePin2State(Landroid/os/AsyncResult;I)V

    .line 456
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_1

    .line 457
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get0(Lcom/android/phone/settings/EditPinDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b06b1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/android/phone/settings/EditPinDialog$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 459
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1, v3}, Lcom/android/phone/settings/EditPinDialog;->-wrap0(Lcom/android/phone/settings/EditPinDialog;Z)V

    goto :goto_0

    .line 463
    .local v0, "ar":Landroid/os/AsyncResult;
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "ar":Landroid/os/AsyncResult;
    check-cast v0, Landroid/os/AsyncResult;

    .line 464
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_3

    .line 465
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get2(Lcom/android/phone/settings/EditPinDialog;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->isPin1Locked(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 466
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get0(Lcom/android/phone/settings/EditPinDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0696

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/android/phone/settings/EditPinDialog$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 468
    :cond_2
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1, v3}, Lcom/android/phone/settings/EditPinDialog;->-wrap0(Lcom/android/phone/settings/EditPinDialog;Z)V

    .line 469
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v2}, Lcom/android/phone/settings/EditPinDialog;->-get0(Lcom/android/phone/settings/EditPinDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 470
    const v2, 0x7f0b045f

    .line 469
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 471
    new-instance v2, Lcom/android/phone/settings/EditPinDialog$1$1;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/EditPinDialog$1$1;-><init>(Lcom/android/phone/settings/EditPinDialog$1;)V

    const v3, 0x104000a

    .line 469
    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v7, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 479
    :cond_3
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    const v2, 0x7f0b045e

    invoke-static {v1, v2}, Lcom/android/phone/settings/EditPinDialog;->-wrap2(Lcom/android/phone/settings/EditPinDialog;I)V

    .line 480
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1, v3}, Lcom/android/phone/settings/EditPinDialog;->-wrap0(Lcom/android/phone/settings/EditPinDialog;Z)V

    goto/16 :goto_0

    .line 484
    .local v0, "ar":Landroid/os/AsyncResult;
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "ar":Landroid/os/AsyncResult;
    check-cast v0, Landroid/os/AsyncResult;

    .line 485
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get2(Lcom/android/phone/settings/EditPinDialog;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->updatePin2State(Landroid/os/AsyncResult;I)V

    .line 486
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_5

    .line 487
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get2(Lcom/android/phone/settings/EditPinDialog;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->isPin2Locked(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 488
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get0(Lcom/android/phone/settings/EditPinDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b06b2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/android/phone/settings/EditPinDialog$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 490
    :cond_4
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1, v3}, Lcom/android/phone/settings/EditPinDialog;->-wrap0(Lcom/android/phone/settings/EditPinDialog;Z)V

    .line 491
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v2}, Lcom/android/phone/settings/EditPinDialog;->-get0(Lcom/android/phone/settings/EditPinDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 492
    const v2, 0x7f0b046b

    .line 491
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 493
    new-instance v2, Lcom/android/phone/settings/EditPinDialog$1$2;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/EditPinDialog$1$2;-><init>(Lcom/android/phone/settings/EditPinDialog$1;)V

    const v3, 0x104000a

    .line 491
    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v7, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 501
    :cond_5
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    const v2, 0x7f0b0469

    invoke-static {v1, v2}, Lcom/android/phone/settings/EditPinDialog;->-wrap2(Lcom/android/phone/settings/EditPinDialog;I)V

    .line 502
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1, v3}, Lcom/android/phone/settings/EditPinDialog;->-wrap0(Lcom/android/phone/settings/EditPinDialog;Z)V

    goto/16 :goto_0

    .line 506
    .local v0, "ar":Landroid/os/AsyncResult;
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "ar":Landroid/os/AsyncResult;
    check-cast v0, Landroid/os/AsyncResult;

    .line 507
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_6

    .line 508
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get0(Lcom/android/phone/settings/EditPinDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0697

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/android/phone/settings/EditPinDialog$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 510
    :cond_6
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    const v2, 0x7f0b045e

    invoke-static {v1, v2}, Lcom/android/phone/settings/EditPinDialog;->-wrap2(Lcom/android/phone/settings/EditPinDialog;I)V

    .line 511
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1, v3}, Lcom/android/phone/settings/EditPinDialog;->-wrap0(Lcom/android/phone/settings/EditPinDialog;Z)V

    goto/16 :goto_0

    .line 515
    .local v0, "ar":Landroid/os/AsyncResult;
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "ar":Landroid/os/AsyncResult;
    check-cast v0, Landroid/os/AsyncResult;

    .line 516
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get2(Lcom/android/phone/settings/EditPinDialog;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->updatePin2State(Landroid/os/AsyncResult;I)V

    .line 517
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_7

    .line 518
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog;->-get0(Lcom/android/phone/settings/EditPinDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b06b0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/android/phone/settings/EditPinDialog$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 520
    :cond_7
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    const v2, 0x7f0b0469

    invoke-static {v1, v2}, Lcom/android/phone/settings/EditPinDialog;->-wrap2(Lcom/android/phone/settings/EditPinDialog;I)V

    .line 521
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog$1;->this$0:Lcom/android/phone/settings/EditPinDialog;

    invoke-static {v1, v3}, Lcom/android/phone/settings/EditPinDialog;->-wrap0(Lcom/android/phone/settings/EditPinDialog;Z)V

    goto/16 :goto_0

    .line 435
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
