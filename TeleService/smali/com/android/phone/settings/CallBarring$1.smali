.class Lcom/android/phone/settings/CallBarring$1;
.super Landroid/content/BroadcastReceiver;
.source "CallBarring.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/CallBarring;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/CallBarring;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/CallBarring;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/CallBarring;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/CallBarring$1;->this$0:Lcom/android/phone/settings/CallBarring;

    .line 76
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 79
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v4, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 81
    iget-object v4, p0, Lcom/android/phone/settings/CallBarring$1;->this$0:Lcom/android/phone/settings/CallBarring;

    invoke-static {v4}, Lcom/android/phone/settings/CallBarring;->-get1(Lcom/android/phone/settings/CallBarring;)I

    move-result v4

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v5

    invoke-static {p2, v5}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 82
    const-string/jumbo v4, "ss"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, "simState":Ljava/lang/String;
    const-string/jumbo v4, "ABSENT"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/android/phone/settings/CallBarring$1;->this$0:Lcom/android/phone/settings/CallBarring;

    .line 86
    const-class v5, Lcom/android/phone/settings/CallFeaturesSetting;

    .line 85
    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    .local v2, "parent":Landroid/content/Intent;
    const-string/jumbo v4, "android.intent.action.MAIN"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    const/high16 v4, 0x20000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 89
    iget-object v4, p0, Lcom/android/phone/settings/CallBarring$1;->this$0:Lcom/android/phone/settings/CallBarring;

    invoke-virtual {v4, v2}, Lcom/android/phone/settings/CallBarring;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    iget-object v4, p0, Lcom/android/phone/settings/CallBarring$1;->this$0:Lcom/android/phone/settings/CallBarring;

    invoke-virtual {v4}, Lcom/android/phone/settings/CallBarring;->finish()V

    .line 97
    .end local v2    # "parent":Landroid/content/Intent;
    .end local v3    # "simState":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 90
    .restart local v3    # "simState":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    :try_start_1
    invoke-static {}, Lcom/android/phone/settings/CallBarring;->-get0()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "can not go back to parent "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    iget-object v4, p0, Lcom/android/phone/settings/CallBarring$1;->this$0:Lcom/android/phone/settings/CallBarring;

    invoke-virtual {v4}, Lcom/android/phone/settings/CallBarring;->finish()V

    goto :goto_0

    .line 92
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catchall_0
    move-exception v4

    .line 93
    iget-object v5, p0, Lcom/android/phone/settings/CallBarring$1;->this$0:Lcom/android/phone/settings/CallBarring;

    invoke-virtual {v5}, Lcom/android/phone/settings/CallBarring;->finish()V

    .line 92
    throw v4
.end method
