.class public Lcom/android/phone/settings/VoicemailSetting;
.super Lcom/android/phone/settings/TimeConsumingPreferenceActivity;
.source "VoicemailSetting.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/phone/settings/EditPhoneNumberPreference$OnDialogClosedListener;
.implements Lcom/android/phone/settings/EditPhoneNumberPreference$GetDefaultNumberListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/VoicemailSetting$1;,
        Lcom/android/phone/settings/VoicemailSetting$2;,
        Lcom/android/phone/settings/VoicemailSetting$3;,
        Lcom/android/phone/settings/VoicemailSetting$4;,
        Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;,
        Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    }
.end annotation


# static fields
.field static final FORWARDING_SETTINGS_REASONS:[I

.field public static final FWD_SETTINGS_DONT_TOUCH:[Lcom/android/internal/telephony/CallForwardInfo;

.field private static final NUM_PROJECTION:[Ljava/lang/String;


# instance fields
.field private DBG:Z

.field mChangingVMorFwdDueToProviderChange:Z

.field mCurrentDialogId:I

.field private mExpectedChangeResultReasons:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mForeground:Z

.field private mForwardingChangeResults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/AsyncResult;",
            ">;"
        }
    .end annotation
.end field

.field mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

.field mFwdChangesRequireRollback:Z

.field private final mGetOptionComplete:Landroid/os/Handler;

.field private mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

.field mNewVMNumber:Ljava/lang/String;

.field private mOldVmNumber:Ljava/lang/String;

.field mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field mPreviousVMProviderKey:Ljava/lang/String;

.field private mReadingSettingsForDefaultProvider:Z

.field private final mRevertOptionComplete:Landroid/os/Handler;

.field private final mRingtoneLookupComplete:Landroid/os/Handler;

.field private mRingtoneLookupRunnable:Ljava/lang/Runnable;

.field private final mSetOptionComplete:Landroid/os/Handler;

.field private mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

.field mVMChangeCompletedSuccesfully:Z

.field mVMOrFwdSetError:I

.field mVMProviderSettingsForced:Z

.field private final mVMProvidersData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;",
            ">;"
        }
    .end annotation
.end field

.field mVoicemailChangeResult:Landroid/os/AsyncResult;

.field private mVoicemailNotificationRingtone:Landroid/preference/Preference;

.field private mVoicemailNotificationVibrate:Landroid/preference/CheckBoxPreference;

.field private mVoicemailProviders:Landroid/preference/ListPreference;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/VoicemailSetting;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSetting;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/VoicemailSetting;)Ljava/util/Map;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingChangeResults:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/VoicemailSetting;)Landroid/preference/Preference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailNotificationRingtone:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/VoicemailSetting;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->checkForwardingCompleted()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/VoicemailSetting;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->checkFwdChangeSuccess()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/VoicemailSetting;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->checkVMChangeSuccess()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap3(Lcom/android/phone/settings/VoicemailSetting;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSetting;
    .param p1, "id"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/VoicemailSetting;->dismissDialogSafely(I)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/phone/settings/VoicemailSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->handleSetVMOrFwdMessage()V

    return-void
.end method

.method static synthetic -wrap5(Ljava/lang/String;)V
    .locals 0
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/phone/settings/VoicemailSetting;ILandroid/preference/Preference;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VoicemailSetting;
    .param p1, "type"    # I
    .param p2, "preference"    # Landroid/preference/Preference;
    .param p3, "msg"    # I

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lcom/android/phone/settings/VoicemailSetting;->updateRingtoneName(ILandroid/preference/Preference;I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 125
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "data1"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/phone/settings/VoicemailSetting;->NUM_PROJECTION:[Ljava/lang/String;

    .line 203
    const/4 v0, 0x2

    .line 204
    const/4 v1, 0x3

    .line 200
    filled-new-array {v2, v3, v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/VoicemailSetting;->FORWARDING_SETTINGS_REASONS:[I

    .line 75
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;-><init>()V

    .line 122
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    .line 173
    new-instance v0, Lcom/android/phone/settings/VoicemailSetting$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/VoicemailSetting$1;-><init>(Lcom/android/phone/settings/VoicemailSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mRingtoneLookupComplete:Landroid/os/Handler;

    .line 286
    iput-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 292
    iput-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingChangeResults:Ljava/util/Map;

    .line 299
    iput-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mExpectedChangeResultReasons:Ljava/util/Collection;

    .line 304
    iput-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    .line 309
    iput-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    .line 314
    iput v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mCurrentDialogId:I

    .line 320
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMProviderSettingsForced:Z

    .line 326
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mChangingVMorFwdDueToProviderChange:Z

    .line 332
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMChangeCompletedSuccesfully:Z

    .line 338
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mFwdChangesRequireRollback:Z

    .line 344
    iput v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMOrFwdSetError:I

    .line 355
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 354
    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMProvidersData:Ljava/util/Map;

    .line 380
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mReadingSettingsForDefaultProvider:Z

    .line 782
    new-instance v0, Lcom/android/phone/settings/VoicemailSetting$2;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/VoicemailSetting$2;-><init>(Lcom/android/phone/settings/VoicemailSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mGetOptionComplete:Landroid/os/Handler;

    .line 951
    new-instance v0, Lcom/android/phone/settings/VoicemailSetting$3;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/VoicemailSetting$3;-><init>(Lcom/android/phone/settings/VoicemailSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mSetOptionComplete:Landroid/os/Handler;

    .line 1011
    new-instance v0, Lcom/android/phone/settings/VoicemailSetting$4;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/VoicemailSetting$4;-><init>(Lcom/android/phone/settings/VoicemailSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mRevertOptionComplete:Landroid/os/Handler;

    .line 75
    return-void

    :cond_0
    move v0, v1

    .line 122
    goto :goto_0
.end method

.method private checkForwardingCompleted()Z
    .locals 4

    .prologue
    .line 1049
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingChangeResults:Ljava/util/Map;

    if-nez v3, :cond_1

    .line 1050
    const/4 v2, 0x1

    .line 1062
    .local v2, "result":Z
    :cond_0
    :goto_0
    return v2

    .line 1054
    .end local v2    # "result":Z
    :cond_1
    const/4 v2, 0x1

    .line 1055
    .restart local v2    # "result":Z
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mExpectedChangeResultReasons:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "reason$iterator":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1056
    .local v0, "reason":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingChangeResults:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    .line 1057
    const/4 v2, 0x0

    .line 1058
    goto :goto_0
.end method

.method private checkFwdChangeSuccess()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1068
    const/4 v3, 0x0

    .line 1070
    .local v3, "result":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingChangeResults:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1071
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/os/AsyncResult;>;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1072
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1073
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/AsyncResult;

    iget-object v1, v4, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 1074
    .local v1, "exception":Ljava/lang/Throwable;
    if-eqz v1, :cond_0

    .line 1075
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 1076
    .local v3, "result":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 1077
    const-string/jumbo v3, ""

    .line 1082
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    .end local v1    # "exception":Ljava/lang/Throwable;
    .end local v3    # "result":Ljava/lang/String;
    :cond_1
    return-object v3
.end method

.method private checkVMChangeSuccess()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1089
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    iget-object v1, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_1

    .line 1090
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    iget-object v1, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 1091
    .local v0, "msg":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1092
    const-string/jumbo v1, ""

    return-object v1

    .line 1094
    :cond_0
    return-object v0

    .line 1096
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method private deleteSettingsForVoicemailProvider(Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1676
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Deleting settings for"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1677
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    if-nez v0, :cond_1

    .line 1678
    return-void

    .line 1680
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1681
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "#VMNumber"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1680
    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1682
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "#FWDSettings"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "#Length"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 1680
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1684
    return-void
.end method

.method private dismissDialogSafely(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 724
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/VoicemailSetting;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 730
    :goto_0
    return-void

    .line 725
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_0
.end method

.method private getCurrentVoicemailProviderKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1687
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1688
    .local v0, "key":Ljava/lang/String;
    if-eqz v0, :cond_0

    .end local v0    # "key":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "key":Ljava/lang/String;
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method private handleSetVMOrFwdMessage()V
    .locals 5

    .prologue
    .line 1100
    iget-boolean v3, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v3, :cond_0

    .line 1101
    const-string/jumbo v3, "handleSetVMMessage: set VM request complete"

    invoke-static {v3}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1103
    :cond_0
    const/4 v2, 0x1

    .line 1104
    .local v2, "success":Z
    const/4 v1, 0x0

    .line 1105
    .local v1, "fwdFailure":Z
    const-string/jumbo v0, ""

    .line 1106
    .local v0, "exceptionMessage":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingChangeResults:Ljava/util/Map;

    if-eqz v3, :cond_1

    .line 1107
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->checkFwdChangeSuccess()Ljava/lang/String;

    move-result-object v0

    .line 1108
    if-eqz v0, :cond_1

    .line 1109
    const/4 v2, 0x0

    .line 1110
    const/4 v1, 0x1

    .line 1113
    :cond_1
    if-eqz v2, :cond_2

    .line 1114
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->checkVMChangeSuccess()Ljava/lang/String;

    move-result-object v0

    .line 1115
    if-eqz v0, :cond_2

    .line 1116
    const/4 v2, 0x0

    .line 1119
    :cond_2
    if-eqz v2, :cond_4

    .line 1120
    iget-boolean v3, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v3, :cond_3

    const-string/jumbo v3, "change VM success!"

    invoke-static {v3}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1121
    :cond_3
    const/16 v3, 0x258

    invoke-direct {p0, v3}, Lcom/android/phone/settings/VoicemailSetting;->handleVMAndFwdSetSuccess(I)V

    .line 1122
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->updateVoiceNumberField()V

    .line 1132
    :goto_0
    return-void

    .line 1124
    :cond_4
    if-eqz v1, :cond_5

    .line 1125
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "change FW failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1126
    const/16 v3, 0x191

    invoke-direct {p0, v3}, Lcom/android/phone/settings/VoicemailSetting;->handleVMOrFwdSetError(I)V

    goto :goto_0

    .line 1128
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "change VM failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1129
    const/16 v3, 0x190

    invoke-direct {p0, v3}, Lcom/android/phone/settings/VoicemailSetting;->handleVMOrFwdSetError(I)V

    goto :goto_0
.end method

.method private handleVMAndFwdSetSuccess(I)V
    .locals 1
    .param p1, "msgId"    # I

    .prologue
    .line 1147
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mChangingVMorFwdDueToProviderChange:Z

    .line 1148
    invoke-direct {p0, p1}, Lcom/android/phone/settings/VoicemailSetting;->showVMDialog(I)V

    .line 1149
    return-void
.end method

.method private handleVMBtnClickRequest()V
    .locals 4

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->getCurrentVoicemailProviderKey()Ljava/lang/String;

    move-result-object v0

    .line 688
    new-instance v1, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    invoke-virtual {v2}, Lcom/android/phone/settings/EditPhoneNumberPreference;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 689
    sget-object v3, Lcom/android/phone/settings/VoicemailSetting;->FWD_SETTINGS_DONT_TOUCH:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 688
    invoke-direct {v1, p0, v2, v3}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;-><init>(Lcom/android/phone/settings/VoicemailSetting;Ljava/lang/String;[Lcom/android/internal/telephony/CallForwardInfo;)V

    .line 686
    invoke-direct {p0, v0, v1}, Lcom/android/phone/settings/VoicemailSetting;->saveVoiceMailAndForwardingNumber(Ljava/lang/String;Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;)V

    .line 690
    return-void
.end method

.method private handleVMOrFwdSetError(I)V
    .locals 2
    .param p1, "msgId"    # I

    .prologue
    const/4 v1, 0x0

    .line 1135
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mChangingVMorFwdDueToProviderChange:Z

    if-eqz v0, :cond_0

    .line 1136
    iput p1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMOrFwdSetError:I

    .line 1137
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mChangingVMorFwdDueToProviderChange:Z

    .line 1138
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->switchToPreviousVoicemailProvider()V

    .line 1139
    return-void

    .line 1141
    :cond_0
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mChangingVMorFwdDueToProviderChange:Z

    .line 1142
    invoke-direct {p0, p1}, Lcom/android/phone/settings/VoicemailSetting;->showVMDialog(I)V

    .line 1143
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->updateVoiceNumberField()V

    .line 1144
    return-void
.end method

.method private infoForReason([Lcom/android/internal/telephony/CallForwardInfo;I)Lcom/android/internal/telephony/CallForwardInfo;
    .locals 5
    .param p1, "infos"    # [Lcom/android/internal/telephony/CallForwardInfo;
    .param p2, "reason"    # I

    .prologue
    .line 875
    const/4 v1, 0x0

    .line 876
    .local v1, "result":Lcom/android/internal/telephony/CallForwardInfo;
    if-eqz p1, :cond_0

    .line 877
    const/4 v2, 0x0

    array-length v3, p1

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, p1, v2

    .line 878
    .local v0, "info":Lcom/android/internal/telephony/CallForwardInfo;
    iget v4, v0, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    if-ne v4, p2, :cond_1

    .line 879
    move-object v1, v0

    .line 884
    .end local v0    # "info":Lcom/android/internal/telephony/CallForwardInfo;
    .end local v1    # "result":Lcom/android/internal/telephony/CallForwardInfo;
    :cond_0
    return-object v1

    .line 877
    .restart local v0    # "info":Lcom/android/internal/telephony/CallForwardInfo;
    .restart local v1    # "result":Lcom/android/internal/telephony/CallForwardInfo;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private initVoiceMailProviders()V
    .locals 22

    .prologue
    .line 1493
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    .line 1494
    const-string/jumbo v19, "vm_numbers"

    .line 1495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v20

    .line 1494
    invoke-static/range {v19 .. v20}, Lcom/android/phone/MiuiPhoneUtils;->getPreferenceKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v19

    .line 1495
    const/16 v20, 0x0

    .line 1493
    invoke-virtual/range {v18 .. v20}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v18

    .line 1492
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/phone/settings/VoicemailSetting;->mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

    .line 1497
    const/4 v14, 0x0

    .line 1498
    .local v14, "providerToIgnore":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->getIntent()Landroid/content/Intent;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "com.android.phone.CallFeaturesSetting.ADD_VOICEMAIL"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1499
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    move/from16 v18, v0

    if-eqz v18, :cond_0

    const-string/jumbo v18, "ACTION_ADD_VOICEMAIL"

    invoke-static/range {v18 .. v18}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1500
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->getIntent()Landroid/content/Intent;

    move-result-object v18

    const-string/jumbo v19, "com.android.phone.ProviderToIgnore"

    invoke-virtual/range {v18 .. v19}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1501
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->getIntent()Landroid/content/Intent;

    move-result-object v18

    const-string/jumbo v19, "com.android.phone.ProviderToIgnore"

    invoke-virtual/range {v18 .. v19}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1503
    .end local v14    # "providerToIgnore":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "providerToIgnore="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1504
    :cond_2
    if-eqz v14, :cond_3

    .line 1505
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/phone/settings/VoicemailSetting;->deleteSettingsForVoicemailProvider(Ljava/lang/String;)V

    .line 1509
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting;->mVMProvidersData:Ljava/util/Map;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->clear()V

    .line 1512
    const v18, 0x7f0b0372

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/VoicemailSetting;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1513
    .local v10, "myCarrier":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting;->mVMProvidersData:Ljava/util/Map;

    move-object/from16 v18, v0

    const-string/jumbo v19, ""

    new-instance v20, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v10, v2}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;-><init>(Lcom/android/phone/settings/VoicemailSetting;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-interface/range {v18 .. v20}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1516
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    .line 1517
    .local v12, "pm":Landroid/content/pm/PackageManager;
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 1518
    .local v7, "intent":Landroid/content/Intent;
    const-string/jumbo v18, "com.android.phone.CallFeaturesSetting.CONFIGURE_VOICEMAIL"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1519
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v12, v7, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v15

    .line 1520
    .local v15, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    add-int/lit8 v9, v18, 0x1

    .line 1524
    .local v9, "len":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_7

    .line 1525
    invoke-interface {v15, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/ResolveInfo;

    .line 1526
    .local v16, "ri":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v16

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 1527
    .local v3, "currentActivityInfo":Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/phone/settings/VoicemailSetting;->makeKeyForActivity(Landroid/content/pm/ActivityInfo;)Ljava/lang/String;

    move-result-object v8

    .line 1528
    .local v8, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Loading "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1529
    :cond_4
    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 1530
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    move/from16 v18, v0

    if-eqz v18, :cond_5

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Ignoring "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1531
    :cond_5
    add-int/lit8 v9, v9, -0x1

    .line 1524
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1534
    :cond_6
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1535
    .local v11, "nameForDisplay":Ljava/lang/String;
    new-instance v13, Landroid/content/Intent;

    invoke-direct {v13}, Landroid/content/Intent;-><init>()V

    .line 1536
    .local v13, "providerIntent":Landroid/content/Intent;
    const-string/jumbo v18, "com.android.phone.CallFeaturesSetting.CONFIGURE_VOICEMAIL"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1537
    iget-object v0, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 1538
    iget-object v0, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 1537
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting;->mVMProvidersData:Ljava/util/Map;

    move-object/from16 v18, v0

    .line 1541
    new-instance v19, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11, v13}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;-><init>(Lcom/android/phone/settings/VoicemailSetting;Ljava/lang/String;Landroid/content/Intent;)V

    .line 1539
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1547
    .end local v3    # "currentActivityInfo":Landroid/content/pm/ActivityInfo;
    .end local v8    # "key":Ljava/lang/String;
    .end local v11    # "nameForDisplay":Ljava/lang/String;
    .end local v13    # "providerIntent":Landroid/content/Intent;
    .end local v16    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_7
    new-array v4, v9, [Ljava/lang/String;

    .line 1548
    .local v4, "entries":[Ljava/lang/String;
    new-array v0, v9, [Ljava/lang/String;

    move-object/from16 v17, v0

    .line 1549
    .local v17, "values":[Ljava/lang/String;
    const/16 v18, 0x0

    aput-object v10, v4, v18

    .line 1550
    const-string/jumbo v18, ""

    const/16 v19, 0x0

    aput-object v18, v17, v19

    .line 1551
    const/4 v5, 0x1

    .line 1552
    .local v5, "entryIdx":I
    const/4 v6, 0x0

    :goto_2
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_9

    .line 1553
    invoke-interface {v15, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/phone/settings/VoicemailSetting;->makeKeyForActivity(Landroid/content/pm/ActivityInfo;)Ljava/lang/String;

    move-result-object v8

    .line 1554
    .restart local v8    # "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting;->mVMProvidersData:Ljava/util/Map;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_8

    .line 1552
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1557
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting;->mVMProvidersData:Ljava/util/Map;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v4, v5

    .line 1558
    aput-object v8, v17, v5

    .line 1559
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 1562
    .end local v8    # "key":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 1563
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1565
    invoke-direct/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->getCurrentVoicemailProviderKey()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    .line 1566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/phone/settings/VoicemailSetting;->updateVMPreferenceWidgets(Ljava/lang/String;)V

    .line 1567
    return-void
.end method

.method private isUpdateRequired(Lcom/android/internal/telephony/CallForwardInfo;Lcom/android/internal/telephony/CallForwardInfo;)Z
    .locals 2
    .param p1, "oldInfo"    # Lcom/android/internal/telephony/CallForwardInfo;
    .param p2, "newInfo"    # Lcom/android/internal/telephony/CallForwardInfo;

    .prologue
    .line 889
    const/4 v0, 0x1

    .line 890
    .local v0, "result":Z
    iget v1, p2, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    if-nez v1, :cond_0

    .line 893
    if-eqz p1, :cond_0

    iget v1, p1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    if-nez v1, :cond_0

    .line 894
    const/4 v0, 0x0

    .line 897
    :cond_0
    return v0
.end method

.method private loadSettingsForVoiceMailProvider(Ljava/lang/String;)Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    .locals 12
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 1638
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting;->mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "#VMNumber"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1640
    .local v6, "vmNumberSetting":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 1641
    iget-boolean v7, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v7, :cond_0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Settings for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " not found"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1642
    :cond_0
    return-object v10

    .line 1645
    :cond_1
    sget-object v0, Lcom/android/phone/settings/VoicemailSetting;->FWD_SETTINGS_DONT_TOUCH:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 1646
    .local v0, "cfi":[Lcom/android/internal/telephony/CallForwardInfo;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#FWDSettings"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1647
    .local v1, "fwdKey":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting;->mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "#Length"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1648
    .local v2, "fwdLen":I
    if-lez v2, :cond_2

    .line 1649
    new-array v0, v2, [Lcom/android/internal/telephony/CallForwardInfo;

    .line 1650
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v7, v0

    if-ge v3, v7, :cond_2

    .line 1651
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#Setting"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1652
    .local v4, "settingKey":Ljava/lang/String;
    new-instance v7, Lcom/android/internal/telephony/CallForwardInfo;

    invoke-direct {v7}, Lcom/android/internal/telephony/CallForwardInfo;-><init>()V

    aput-object v7, v0, v3

    .line 1653
    aget-object v7, v0, v3

    iget-object v8, p0, Lcom/android/phone/settings/VoicemailSetting;->mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

    .line 1654
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "#Status"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1653
    invoke-interface {v8, v9, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, v7, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    .line 1655
    aget-object v7, v0, v3

    iget-object v8, p0, Lcom/android/phone/settings/VoicemailSetting;->mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

    .line 1656
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "#Reason"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1657
    const/4 v10, 0x5

    .line 1655
    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, v7, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    .line 1658
    aget-object v7, v0, v3

    const/4 v8, 0x1

    iput v8, v7, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    .line 1659
    aget-object v7, v0, v3

    const/16 v8, 0x91

    iput v8, v7, Lcom/android/internal/telephony/CallForwardInfo;->toa:I

    .line 1660
    aget-object v7, v0, v3

    iget-object v8, p0, Lcom/android/phone/settings/VoicemailSetting;->mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

    .line 1661
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "#Number"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, ""

    .line 1660
    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    .line 1662
    aget-object v7, v0, v3

    iget-object v8, p0, Lcom/android/phone/settings/VoicemailSetting;->mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

    .line 1663
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "#Time"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x14

    .line 1662
    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, v7, Lcom/android/internal/telephony/CallForwardInfo;->timeSeconds:I

    .line 1650
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 1667
    .end local v3    # "i":I
    .end local v4    # "settingKey":Ljava/lang/String;
    :cond_2
    new-instance v5, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    invoke-direct {v5, p0, v6, v0}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;-><init>(Lcom/android/phone/settings/VoicemailSetting;Ljava/lang/String;[Lcom/android/internal/telephony/CallForwardInfo;)V

    .line 1668
    .local v5, "settings":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    iget-boolean v7, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v7, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Loaded settings for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1669
    :cond_3
    return-object v5
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 1438
    const-string/jumbo v0, "VoicemailSetting"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1439
    return-void
.end method

.method private lookupRingtoneName()V
    .locals 2

    .prologue
    .line 1434
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mRingtoneLookupRunnable:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1435
    return-void
.end method

.method private makeKeyForActivity(Landroid/content/pm/ActivityInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "ai"    # Landroid/content/pm/ActivityInfo;

    .prologue
    .line 1570
    iget-object v0, p1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method private maybeSaveSettingsForVoicemailProvider(Ljava/lang/String;Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;)V
    .locals 9
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "newSettings"    # Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    .prologue
    .line 1600
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    if-nez v7, :cond_0

    .line 1601
    return-void

    .line 1603
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/phone/settings/VoicemailSetting;->loadSettingsForVoiceMailProvider(Ljava/lang/String;)Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    move-result-object v0

    .line 1604
    .local v0, "curSettings":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    invoke-virtual {p2, v0}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1605
    iget-boolean v7, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v7, :cond_1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Not saving setting for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " since they have not changed"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1606
    :cond_1
    return-void

    .line 1608
    :cond_2
    iget-boolean v7, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v7, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Saving settings for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1609
    :cond_3
    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting;->mPerProviderSavedVMNumbers:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1610
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#VMNumber"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p2, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1611
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#FWDSettings"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1612
    .local v3, "fwdKey":Ljava/lang/String;
    iget-object v5, p2, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 1613
    .local v5, "s":[Lcom/android/internal/telephony/CallForwardInfo;
    sget-object v7, Lcom/android/phone/settings/VoicemailSetting;->FWD_SETTINGS_DONT_TOUCH:[Lcom/android/internal/telephony/CallForwardInfo;

    if-eq v5, v7, :cond_4

    .line 1614
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#Length"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    array-length v8, v5

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1615
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v7, v5

    if-ge v4, v7, :cond_5

    .line 1616
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#Setting"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1617
    .local v6, "settingKey":Ljava/lang/String;
    aget-object v2, v5, v4

    .line 1618
    .local v2, "fi":Lcom/android/internal/telephony/CallForwardInfo;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#Status"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget v8, v2, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1619
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#Reason"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget v8, v2, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1620
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#Number"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v2, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1621
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#Time"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget v8, v2, Lcom/android/internal/telephony/CallForwardInfo;->timeSeconds:I

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1615
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1624
    .end local v2    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    .end local v4    # "i":I
    .end local v6    # "settingKey":Ljava/lang/String;
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "#Length"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1626
    :cond_5
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1627
    return-void
.end method

.method public static migrateVoicemailVibrationSettingsIfNeeded(Landroid/content/SharedPreferences;I)Z
    .locals 6
    .param p0, "prefs"    # Landroid/content/SharedPreferences;
    .param p1, "slotId"    # I

    .prologue
    .line 1414
    const-string/jumbo v4, "button_voicemail_notification_vibrate_key"

    .line 1413
    invoke-static {v4, p1}, Lcom/android/phone/MiuiPhoneUtils;->getPreferenceKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 1415
    .local v2, "vmVibrateKey":Ljava/lang/String;
    invoke-interface {p0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1417
    const-string/jumbo v4, "button_voicemail_notification_vibrate_when_key"

    const-string/jumbo v5, "never"

    .line 1416
    invoke-interface {p0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1421
    .local v1, "vibrateWhen":Ljava/lang/String;
    const-string/jumbo v4, "always"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 1422
    .local v3, "voicemailVibrate":Z
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1423
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1424
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1425
    const/4 v4, 0x1

    return v4

    .line 1427
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "vibrateWhen":Ljava/lang/String;
    .end local v3    # "voicemailVibrate":Z
    :cond_0
    const/4 v4, 0x0

    return v4
.end method

.method private resetForwardingChangeState()V
    .locals 1

    .prologue
    .line 901
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingChangeResults:Ljava/util/Map;

    .line 902
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mExpectedChangeResultReasons:Ljava/util/Collection;

    .line 903
    return-void
.end method

.method private saveVoiceMailAndForwardingNumber(Ljava/lang/String;Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;)V
    .locals 7
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "newSettings"    # Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    .prologue
    const/16 v4, 0x2bc

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 734
    iget-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "saveVoiceMailAndForwardingNumber: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/MiuiPhoneUtils;->toLogSafePhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 735
    :cond_0
    iget-object v1, p2, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    .line 737
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 738
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    .line 741
    :cond_1
    iget-object v1, p2, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 742
    iget-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "newFwdNumber "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 743
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    array-length v1, v1

    :goto_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->toLogSafePhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 742
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 744
    const-string/jumbo v3, " settings"

    .line 742
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 747
    :cond_2
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_5

    .line 748
    iget-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "ignoring forwarding setting since this is CDMA phone"

    invoke-static {v1}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 749
    :cond_3
    sget-object v1, Lcom/android/phone/settings/VoicemailSetting;->FWD_SETTINGS_DONT_TOUCH:[Lcom/android/internal/telephony/CallForwardInfo;

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 750
    const-string/jumbo v1, "*86"

    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mOldVmNumber:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 751
    invoke-direct {p0, v4}, Lcom/android/phone/settings/VoicemailSetting;->showVMDialog(I)V

    .line 752
    return-void

    :cond_4
    move v1, v2

    .line 743
    goto :goto_0

    .line 757
    :cond_5
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mOldVmNumber:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    sget-object v3, Lcom/android/phone/settings/VoicemailSetting;->FWD_SETTINGS_DONT_TOUCH:[Lcom/android/internal/telephony/CallForwardInfo;

    if-ne v1, v3, :cond_6

    .line 758
    invoke-direct {p0, v4}, Lcom/android/phone/settings/VoicemailSetting;->showVMDialog(I)V

    .line 759
    return-void

    .line 762
    :cond_6
    invoke-direct {p0, p1, p2}, Lcom/android/phone/settings/VoicemailSetting;->maybeSaveSettingsForVoicemailProvider(Ljava/lang/String;Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;)V

    .line 763
    iput-boolean v2, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMChangeCompletedSuccesfully:Z

    .line 764
    iput-boolean v2, p0, Lcom/android/phone/settings/VoicemailSetting;->mFwdChangesRequireRollback:Z

    .line 765
    iput v2, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMOrFwdSetError:I

    .line 766
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 768
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    const-string/jumbo v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 767
    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mReadingSettingsForDefaultProvider:Z

    .line 769
    iget-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v1, :cond_7

    const-string/jumbo v1, "Reading current forwarding settings"

    invoke-static {v1}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 770
    :cond_7
    sget-object v1, Lcom/android/phone/settings/VoicemailSetting;->FORWARDING_SETTINGS_REASONS:[I

    array-length v1, v1

    new-array v1, v1, [Lcom/android/internal/telephony/CallForwardInfo;

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 771
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v1, Lcom/android/phone/settings/VoicemailSetting;->FORWARDING_SETTINGS_REASONS:[I

    array-length v1, v1

    if-ge v0, v1, :cond_8

    .line 772
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    aput-object v6, v1, v0

    .line 773
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    sget-object v3, Lcom/android/phone/settings/VoicemailSetting;->FORWARDING_SETTINGS_REASONS:[I

    aget v3, v3, v0

    .line 774
    iget-object v4, p0, Lcom/android/phone/settings/VoicemailSetting;->mGetOptionComplete:Landroid/os/Handler;

    const/16 v5, 0x1f6

    invoke-virtual {v4, v5, v0, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    .line 773
    invoke-virtual {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getCallForwardingOption(ILandroid/os/Message;)V

    .line 771
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 776
    :cond_8
    const/16 v1, 0x642

    invoke-direct {p0, v1}, Lcom/android/phone/settings/VoicemailSetting;->showDialogIfForeground(I)V

    .line 780
    .end local v0    # "i":I
    :goto_2
    return-void

    .line 778
    :cond_9
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->saveVoiceMailAndForwardingNumberStage2()V

    goto :goto_2
.end method

.method private saveVoiceMailAndForwardingNumberStage2()V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 908
    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingChangeResults:Ljava/util/Map;

    .line 909
    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    .line 910
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    sget-object v1, Lcom/android/phone/settings/VoicemailSetting;->FWD_SETTINGS_DONT_TOUCH:[Lcom/android/internal/telephony/CallForwardInfo;

    if-eq v0, v1, :cond_4

    .line 911
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->resetForwardingChangeState()V

    .line 912
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    array-length v0, v0

    if-ge v8, v0, :cond_3

    .line 913
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewFwdSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    aget-object v7, v0, v8

    .line 916
    .local v7, "fi":Lcom/android/internal/telephony/CallForwardInfo;
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    iget v1, v7, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    .line 915
    invoke-direct {p0, v0, v1}, Lcom/android/phone/settings/VoicemailSetting;->infoForReason([Lcom/android/internal/telephony/CallForwardInfo;I)Lcom/android/internal/telephony/CallForwardInfo;

    move-result-object v0

    invoke-direct {p0, v0, v7}, Lcom/android/phone/settings/VoicemailSetting;->isUpdateRequired(Lcom/android/internal/telephony/CallForwardInfo;Lcom/android/internal/telephony/CallForwardInfo;)Z

    move-result v6

    .line 918
    .local v6, "doUpdate":Z
    if-eqz v6, :cond_1

    .line 919
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Setting fwd #: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/android/internal/telephony/CallForwardInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 920
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mExpectedChangeResultReasons:Ljava/util/Collection;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 922
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 923
    iget v1, v7, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 924
    const/4 v1, 0x3

    .line 926
    :goto_1
    iget v2, v7, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    .line 927
    iget-object v3, v7, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    .line 928
    iget v4, v7, Lcom/android/internal/telephony/CallForwardInfo;->timeSeconds:I

    .line 929
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mSetOptionComplete:Landroid/os/Handler;

    .line 930
    iget v10, v7, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    const/16 v11, 0x1f5

    .line 929
    invoke-virtual {v5, v11, v10, v9}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    .line 922
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/Phone;->setCallForwardingOption(IILjava/lang/String;ILandroid/os/Message;)V

    .line 912
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_2
    move v1, v9

    .line 925
    goto :goto_1

    .line 933
    .end local v6    # "doUpdate":Z
    .end local v7    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    :cond_3
    const/16 v0, 0x641

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->showDialogIfForeground(I)V

    .line 938
    .end local v8    # "i":I
    :goto_2
    return-void

    .line 935
    :cond_4
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_5

    const-string/jumbo v0, "Not touching fwd #"

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 936
    :cond_5
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSetting;->setVMNumberWithCarrier()V

    goto :goto_2
.end method

.method private showDialogIfForeground(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 717
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mForeground:Z

    if-eqz v0, :cond_0

    .line 718
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/VoicemailSetting;->showDialog(I)V

    .line 720
    :cond_0
    return-void
.end method

.method private showVMDialog(I)V
    .locals 1
    .param p1, "msgStatus"    # I

    .prologue
    .line 1301
    sparse-switch p1, :sswitch_data_0

    .line 1323
    :goto_0
    return-void

    .line 1305
    :sswitch_0
    const/16 v0, 0x5dc

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->showDialogIfForeground(I)V

    goto :goto_0

    .line 1308
    :sswitch_1
    const/16 v0, 0x5dd

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->showDialogIfForeground(I)V

    goto :goto_0

    .line 1311
    :sswitch_2
    const/16 v0, 0x5de

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->showDialogIfForeground(I)V

    goto :goto_0

    .line 1314
    :sswitch_3
    const/16 v0, 0x578

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->showDialogIfForeground(I)V

    goto :goto_0

    .line 1317
    :sswitch_4
    const/16 v0, 0x640

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->showDialogIfForeground(I)V

    goto :goto_0

    .line 1301
    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_0
        0x191 -> :sswitch_1
        0x192 -> :sswitch_2
        0x258 -> :sswitch_4
        0x2bc -> :sswitch_3
    .end sparse-switch
.end method

.method private simulatePreferenceClick(Landroid/preference/Preference;)V
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 1582
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    .line 1583
    .local v6, "adapter":Landroid/widget/ListAdapter;
    const/4 v3, 0x0

    .local v3, "idx":I
    :goto_0
    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 1584
    invoke-interface {v6, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 1585
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSetting;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 1586
    invoke-interface {v6, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    const/4 v2, 0x0

    .line 1585
    invoke-virtual/range {v0 .. v5}, Landroid/preference/PreferenceScreen;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 1590
    :cond_0
    return-void

    .line 1583
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private switchToPreviousVoicemailProvider()V
    .locals 14

    .prologue
    const/4 v12, 0x0

    .line 509
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "switchToPreviousVoicemailProvider "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 511
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMChangeCompletedSuccesfully:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mFwdChangesRequireRollback:Z

    if-eqz v0, :cond_9

    .line 513
    :cond_1
    const/16 v0, 0x643

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->showDialogIfForeground(I)V

    .line 515
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->loadSettingsForVoiceMailProvider(Ljava/lang/String;)Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    move-result-object v9

    .line 516
    .local v9, "prevSettings":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMChangeCompletedSuccesfully:Z

    if-eqz v0, :cond_3

    .line 517
    iget-object v0, v9, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    .line 518
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "have to revert VM to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->toLogSafePhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 519
    :cond_2
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 520
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getVoiceMailAlphaTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 521
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    .line 522
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mRevertOptionComplete:Landroid/os/Handler;

    const/16 v4, 0x1f4

    invoke-static {v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    .line 519
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/Phone;->setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 524
    :cond_3
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mFwdChangesRequireRollback:Z

    if-eqz v0, :cond_b

    .line 525
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_4

    const-string/jumbo v0, "have to revert fwd"

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 526
    :cond_4
    if-nez v9, :cond_7

    const/4 v8, 0x0

    .line 528
    :goto_0
    if-eqz v8, :cond_b

    .line 529
    iget-object v11, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingChangeResults:Ljava/util/Map;

    .line 531
    .local v11, "results":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->resetForwardingChangeState()V

    .line 532
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v0, v8

    if-ge v7, v0, :cond_b

    .line 533
    aget-object v6, v8, v7

    .line 534
    .local v6, "fi":Lcom/android/internal/telephony/CallForwardInfo;
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Reverting fwd #: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/android/internal/telephony/CallForwardInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 537
    :cond_5
    iget v0, v6, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/os/AsyncResult;

    .line 538
    .local v10, "result":Landroid/os/AsyncResult;
    if-eqz v10, :cond_6

    iget-object v0, v10, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v0, :cond_6

    .line 539
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mExpectedChangeResultReasons:Ljava/util/Collection;

    iget v1, v6, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 540
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 541
    iget v1, v6, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_8

    .line 542
    const/4 v1, 0x3

    .line 544
    :goto_2
    iget v2, v6, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    .line 545
    iget-object v3, v6, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    .line 546
    iget v4, v6, Lcom/android/internal/telephony/CallForwardInfo;->timeSeconds:I

    .line 547
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mRevertOptionComplete:Landroid/os/Handler;

    .line 548
    const/16 v13, 0x1f5

    .line 547
    invoke-virtual {v5, v13, v7, v12}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    .line 540
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/Phone;->setCallForwardingOption(IILjava/lang/String;ILandroid/os/Message;)V

    .line 532
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 527
    .end local v6    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    .end local v7    # "i":I
    .end local v10    # "result":Landroid/os/AsyncResult;
    .end local v11    # "results":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    :cond_7
    iget-object v8, v9, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .local v8, "prevFwdSettings":[Lcom/android/internal/telephony/CallForwardInfo;
    goto :goto_0

    .end local v8    # "prevFwdSettings":[Lcom/android/internal/telephony/CallForwardInfo;
    .restart local v6    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    .restart local v7    # "i":I
    .restart local v10    # "result":Landroid/os/AsyncResult;
    .restart local v11    # "results":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    :cond_8
    move v1, v12

    .line 543
    goto :goto_2

    .line 554
    .end local v6    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    .end local v7    # "i":I
    .end local v9    # "prevSettings":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    .end local v10    # "result":Landroid/os/AsyncResult;
    .end local v11    # "results":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/os/AsyncResult;>;"
    :cond_9
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_a

    const-string/jumbo v0, "No need to revert"

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 555
    :cond_a
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSetting;->onRevertDone()V

    .line 558
    :cond_b
    return-void
.end method

.method private updateRingtoneName(ILandroid/preference/Preference;I)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "preference"    # Landroid/preference/Preference;
    .param p3, "msg"    # I

    .prologue
    .line 1447
    if-nez p2, :cond_0

    return-void

    .line 1448
    :cond_0
    invoke-static {p0, p1}, Landroid/media/ExtraRingtoneManager;->getRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    .line 1449
    .local v0, "ringtoneUri":Landroid/net/Uri;
    const/4 v2, 0x1

    invoke-static {p0, v0, v2}, Landroid/media/ExtraRingtone;->getRingtoneTitle(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v1

    .line 1450
    .local v1, "summary":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting;->mRingtoneLookupComplete:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mRingtoneLookupComplete:Landroid/os/Handler;

    invoke-virtual {v3, p3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1451
    return-void
.end method

.method private updateVMPreferenceWidgets(Ljava/lang/String;)V
    .locals 8
    .param p1, "currentProviderSetting"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1459
    move-object v0, p1

    .line 1460
    .local v0, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMProvidersData:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;

    .line 1466
    .local v1, "provider":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;
    if-nez v1, :cond_0

    .line 1467
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    const v4, 0x7f0b0371

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/VoicemailSetting;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1468
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    invoke-virtual {v3, v5}, Lcom/android/phone/settings/EditPhoneNumberPreference;->setEnabled(Z)V

    .line 1469
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    invoke-virtual {v3, v7}, Lcom/android/phone/settings/EditPhoneNumberPreference;->setIntent(Landroid/content/Intent;)V

    .line 1471
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailNotificationVibrate:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 1480
    :goto_0
    return-void

    .line 1473
    :cond_0
    iget-object v2, v1, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;->name:Ljava/lang/String;

    .line 1474
    .local v2, "providerName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    invoke-virtual {v3, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1475
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    invoke-virtual {v3, v6}, Lcom/android/phone/settings/EditPhoneNumberPreference;->setEnabled(Z)V

    .line 1476
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    iget-object v4, v1, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProvider;->intent:Landroid/content/Intent;

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/EditPhoneNumberPreference;->setIntent(Landroid/content/Intent;)V

    .line 1478
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailNotificationVibrate:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateVoiceNumberField()V
    .locals 3

    .prologue
    .line 1156
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    if-nez v1, :cond_0

    .line 1157
    return-void

    .line 1160
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getVoiceMailNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mOldVmNumber:Ljava/lang/String;

    .line 1161
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mOldVmNumber:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1162
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mOldVmNumber:Ljava/lang/String;

    .line 1164
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting;->mOldVmNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/EditPhoneNumberPreference;->setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/settings/EditPhoneNumberPreference;

    .line 1165
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mOldVmNumber:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mOldVmNumber:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1167
    .local v0, "summary":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/EditPhoneNumberPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1168
    return-void

    .line 1166
    .end local v0    # "summary":Ljava/lang/String;
    :cond_2
    const v1, 0x7f0b04e8

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "summary":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method handleForwardingSettingsReadResult(Landroid/os/AsyncResult;I)V
    .locals 11
    .param p1, "ar"    # Landroid/os/AsyncResult;
    .param p2, "idx"    # I

    .prologue
    const/16 v10, 0x642

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 795
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_0

    const-string/jumbo v5, "VoicemailSetting"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "handleForwardingSettingsReadResult: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    :cond_0
    const/4 v2, 0x0

    .line 797
    .local v2, "error":Ljava/lang/Throwable;
    iget-object v5, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_2

    .line 798
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_1

    const-string/jumbo v5, "VoicemailSetting"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "FwdRead: ar.exception="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 799
    iget-object v7, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v7}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    .line 798
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    :cond_1
    iget-object v2, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 802
    .end local v2    # "error":Ljava/lang/Throwable;
    :cond_2
    iget-object v5, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    instance-of v5, v5, Ljava/lang/Throwable;

    if-eqz v5, :cond_4

    .line 803
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_3

    const-string/jumbo v6, "VoicemailSetting"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "FwdRead: userObj="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 804
    iget-object v5, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Throwable;

    invoke-virtual {v5}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 803
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    :cond_3
    iget-object v2, p1, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Throwable;

    .line 809
    :cond_4
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    if-nez v5, :cond_6

    .line 810
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_5

    const-string/jumbo v5, "VoicemailSetting"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "ignoring fwd reading result: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    :cond_5
    return-void

    .line 815
    :cond_6
    if-eqz v2, :cond_8

    .line 816
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_7

    const-string/jumbo v5, "VoicemailSetting"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Error discovered for fwd read : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    :cond_7
    iput-object v8, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 818
    invoke-direct {p0, v10}, Lcom/android/phone/settings/VoicemailSetting;->dismissDialogSafely(I)V

    .line 819
    const/16 v5, 0x192

    invoke-direct {p0, v5}, Lcom/android/phone/settings/VoicemailSetting;->showVMDialog(I)V

    .line 820
    return-void

    .line 824
    :cond_8
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v0, [Lcom/android/internal/telephony/CallForwardInfo;

    .line 825
    .local v0, "cfInfoArray":[Lcom/android/internal/telephony/CallForwardInfo;
    const/4 v3, 0x0

    .line 826
    .local v3, "fi":Lcom/android/internal/telephony/CallForwardInfo;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v0

    if-ge v4, v5, :cond_9

    .line 827
    aget-object v5, v0, v4

    iget v5, v5, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_10

    .line 828
    aget-object v3, v0, v4

    .line 832
    .end local v3    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    :cond_9
    if-nez v3, :cond_11

    .line 836
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_a

    const-string/jumbo v5, "VoicemailSetting"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Creating default info for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    :cond_a
    new-instance v3, Lcom/android/internal/telephony/CallForwardInfo;

    invoke-direct {v3}, Lcom/android/internal/telephony/CallForwardInfo;-><init>()V

    .line 838
    .local v3, "fi":Lcom/android/internal/telephony/CallForwardInfo;
    iput v9, v3, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    .line 839
    sget-object v5, Lcom/android/phone/settings/VoicemailSetting;->FORWARDING_SETTINGS_REASONS:[I

    aget v5, v5, p2

    iput v5, v3, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    .line 840
    const/4 v5, 0x1

    iput v5, v3, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    .line 849
    .end local v3    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    :cond_b
    :goto_1
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    aput-object v3, v5, p2

    .line 852
    const/4 v1, 0x1

    .line 853
    .local v1, "done":Z
    const/4 v4, 0x0

    :goto_2
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    array-length v5, v5

    if-ge v4, v5, :cond_c

    .line 854
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    aget-object v5, v5, v4

    if-nez v5, :cond_14

    .line 855
    const/4 v1, 0x0

    .line 859
    :cond_c
    if-eqz v1, :cond_15

    .line 860
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_d

    const-string/jumbo v5, "VoicemailSetting"

    const-string/jumbo v6, "Done receiving fwd info"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    :cond_d
    invoke-direct {p0, v10}, Lcom/android/phone/settings/VoicemailSetting;->dismissDialogSafely(I)V

    .line 862
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mReadingSettingsForDefaultProvider:Z

    if-eqz v5, :cond_e

    .line 863
    const-string/jumbo v5, ""

    .line 864
    new-instance v6, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    iget-object v7, p0, Lcom/android/phone/settings/VoicemailSetting;->mOldVmNumber:Ljava/lang/String;

    .line 865
    iget-object v8, p0, Lcom/android/phone/settings/VoicemailSetting;->mForwardingReadResults:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 864
    invoke-direct {v6, p0, v7, v8}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;-><init>(Lcom/android/phone/settings/VoicemailSetting;Ljava/lang/String;[Lcom/android/internal/telephony/CallForwardInfo;)V

    .line 863
    invoke-direct {p0, v5, v6}, Lcom/android/phone/settings/VoicemailSetting;->maybeSaveSettingsForVoicemailProvider(Ljava/lang/String;Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;)V

    .line 866
    iput-boolean v9, p0, Lcom/android/phone/settings/VoicemailSetting;->mReadingSettingsForDefaultProvider:Z

    .line 868
    :cond_e
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->saveVoiceMailAndForwardingNumberStage2()V

    .line 872
    :cond_f
    :goto_3
    return-void

    .line 826
    .end local v1    # "done":Z
    .local v3, "fi":Lcom/android/internal/telephony/CallForwardInfo;
    :cond_10
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 843
    .end local v3    # "fi":Lcom/android/internal/telephony/CallForwardInfo;
    :cond_11
    iget-object v5, v3, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    if-eqz v5, :cond_12

    iget-object v5, v3, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_13

    .line 844
    :cond_12
    iput v9, v3, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    .line 847
    :cond_13
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_b

    const-string/jumbo v5, "VoicemailSetting"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Got  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/android/internal/telephony/CallForwardInfo;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 853
    .restart local v1    # "done":Z
    :cond_14
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 870
    :cond_15
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_f

    const-string/jumbo v5, "VoicemailSetting"

    const-string/jumbo v6, "Not done receiving fwd info"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 20
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 577
    const/4 v2, 0x2

    move/from16 v0, p1

    if-ne v0, v2, :cond_13

    .line 578
    const/4 v11, 0x0

    .line 581
    .local v11, "failure":Z
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mVMProviderSettingsForced: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/phone/settings/VoicemailSetting;->mVMProviderSettingsForced:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 582
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/phone/settings/VoicemailSetting;->mVMProviderSettingsForced:Z

    .line 583
    .local v15, "isVMProviderSettingsForced":Z
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->mVMProviderSettingsForced:Z

    .line 585
    const/16 v19, 0x0

    .line 586
    .local v19, "vmNum":Ljava/lang/String;
    const/4 v2, -0x1

    move/from16 v0, p2

    if-eq v0, v2, :cond_5

    .line 587
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_1

    const-string/jumbo v2, "onActivityResult: vm provider cfg result not OK."

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 588
    :cond_1
    const/4 v11, 0x1

    .line 616
    .end local v19    # "vmNum":Ljava/lang/String;
    :cond_2
    :goto_0
    if-eqz v11, :cond_10

    .line 617
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_3

    const-string/jumbo v2, "Failure in return from voicemail provider"

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 618
    :cond_3
    if-eqz v15, :cond_f

    .line 619
    invoke-direct/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->switchToPreviousVoicemailProvider()V

    .line 623
    :cond_4
    :goto_1
    return-void

    .line 590
    .restart local v19    # "vmNum":Ljava/lang/String;
    :cond_5
    if-nez p3, :cond_7

    .line 591
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_6

    const-string/jumbo v2, "onActivityResult: vm provider cfg result has no data"

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 592
    :cond_6
    const/4 v11, 0x1

    goto :goto_0

    .line 594
    :cond_7
    const-string/jumbo v2, "com.android.phone.Signout"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 595
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_8

    const-string/jumbo v2, "Provider requested signout"

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 596
    :cond_8
    if-eqz v15, :cond_a

    .line 597
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_9

    const-string/jumbo v2, "Going back to previous provider on signout"

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 598
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->switchToPreviousVoicemailProvider()V

    .line 607
    :goto_2
    return-void

    .line 600
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->getCurrentVoicemailProviderKey()Ljava/lang/String;

    move-result-object v18

    .line 601
    .local v18, "victim":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Relaunching activity and ignoring "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 602
    :cond_b
    new-instance v14, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.phone.CallFeaturesSetting.ADD_VOICEMAIL"

    invoke-direct {v14, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 603
    .local v14, "i":Landroid/content/Intent;
    const-string/jumbo v2, "com.android.phone.ProviderToIgnore"

    move-object/from16 v0, v18

    invoke-virtual {v14, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 604
    const/high16 v2, 0x4000000

    invoke-virtual {v14, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 605
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/phone/settings/VoicemailSetting;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    .line 609
    .end local v14    # "i":Landroid/content/Intent;
    .end local v18    # "victim":Ljava/lang/String;
    :cond_c
    const-string/jumbo v2, "com.android.phone.VoicemailNumber"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 610
    .local v19, "vmNum":Ljava/lang/String;
    if-eqz v19, :cond_d

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 611
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_e

    const-string/jumbo v2, "onActivityResult: vm provider cfg result has no vmnum"

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 612
    :cond_e
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 621
    .end local v19    # "vmNum":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_4

    const-string/jumbo v2, "Not switching back the provider since this is not forced config"

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 625
    :cond_10
    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/phone/settings/VoicemailSetting;->mChangingVMorFwdDueToProviderChange:Z

    .line 626
    const-string/jumbo v2, "com.android.phone.ForwardingNumber"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 630
    .local v12, "fwdNum":Ljava/lang/String;
    const-string/jumbo v2, "com.android.phone.ForwardingNumberTime"

    const/16 v3, 0x14

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 632
    .local v13, "fwdNumTime":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_11

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onActivityResult: vm provider cfg result "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 633
    if-eqz v12, :cond_12

    const-string/jumbo v2, "has"

    .line 632
    :goto_3
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 633
    const-string/jumbo v3, " forwarding number"

    .line 632
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 634
    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->getCurrentVoicemailProviderKey()Ljava/lang/String;

    move-result-object v2

    .line 635
    new-instance v3, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v3, v0, v1, v12, v13}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;-><init>(Lcom/android/phone/settings/VoicemailSetting;Ljava/lang/String;Ljava/lang/String;I)V

    .line 634
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/phone/settings/VoicemailSetting;->saveVoiceMailAndForwardingNumber(Ljava/lang/String;Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;)V

    .line 636
    return-void

    .line 633
    :cond_12
    const-string/jumbo v2, " does not have"

    goto :goto_3

    .line 639
    .end local v11    # "failure":Z
    .end local v12    # "fwdNum":Ljava/lang/String;
    .end local v13    # "fwdNumTime":I
    .end local v15    # "isVMProviderSettingsForced":Z
    :cond_13
    const/4 v2, -0x1

    move/from16 v0, p2

    if-eq v0, v2, :cond_15

    .line 640
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v2, :cond_14

    const-string/jumbo v2, "onActivityResult: contact picker result not OK."

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 641
    :cond_14
    return-void

    .line 644
    :cond_15
    const/4 v8, 0x0

    .line 645
    .local v8, "cursor":Landroid/database/Cursor;
    const-string/jumbo v16, ""

    .line 647
    .local v16, "result":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v17

    .line 648
    .local v17, "uri":Landroid/net/Uri;
    const-string/jumbo v2, "VoicemailSetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "uri:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tel"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 650
    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v16

    .line 665
    .end local v8    # "cursor":Landroid/database/Cursor;
    :goto_4
    if-eqz v8, :cond_16

    .line 666
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 670
    .end local v17    # "uri":Landroid/net/Uri;
    :cond_16
    :goto_5
    packed-switch p1, :pswitch_data_0

    .line 677
    :goto_6
    return-void

    .line 652
    .restart local v8    # "cursor":Landroid/database/Cursor;
    .restart local v17    # "uri":Landroid/net/Uri;
    :cond_17
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/VoicemailSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 653
    sget-object v4, Lcom/android/phone/settings/VoicemailSetting;->NUM_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 652
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 654
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_18

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1a

    .line 655
    :cond_18
    const-string/jumbo v2, "onActivityResult: bad contact data, no results found."

    invoke-static {v2}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 665
    if-eqz v8, :cond_19

    .line 666
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 656
    :cond_19
    return-void

    .line 658
    :cond_1a
    const/4 v2, 0x0

    :try_start_2
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v16

    goto :goto_4

    .line 662
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v17    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v9

    .line 665
    .local v9, "e":Ljava/lang/Exception;
    if-eqz v8, :cond_16

    .line 666
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 660
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v10

    .line 665
    .local v10, "e":Ljava/lang/IllegalArgumentException;
    if-eqz v8, :cond_16

    .line 666
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 664
    .end local v10    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v2

    .line 665
    if-eqz v8, :cond_1b

    .line 666
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 664
    :cond_1b
    throw v2

    .line 672
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/android/phone/settings/EditPhoneNumberPreference;->onPickActivityResult(Ljava/lang/String;)V

    goto :goto_6

    .line 670
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 1331
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 1332
    iget-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "Creating activity"

    invoke-static {v1}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 1334
    :cond_0
    invoke-static {p0}, Lcom/android/phone/settings/SimPickerPreference;->showSimPicker(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1335
    return-void

    .line 1337
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSetting;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1338
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v2

    .line 1337
    invoke-static {v1, v2}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v0

    .line 1339
    .local v0, "slotId":I
    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 1341
    const v1, 0x7f06003f

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSetting;->addPreferencesFromResource(I)V

    .line 1344
    const-string/jumbo v1, "button_voicemail"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/EditPhoneNumberPreference;

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    .line 1345
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    if-eqz v1, :cond_2

    .line 1346
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    invoke-virtual {v1, p0, v3, p0}, Lcom/android/phone/settings/EditPhoneNumberPreference;->setParentActivity(Landroid/app/Activity;ILcom/android/phone/settings/EditPhoneNumberPreference$GetDefaultNumberListener;)V

    .line 1347
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    invoke-virtual {v1, p0}, Lcom/android/phone/settings/EditPhoneNumberPreference;->setDialogOnClosedListener(Lcom/android/phone/settings/EditPhoneNumberPreference$OnDialogClosedListener;)V

    .line 1348
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    const v2, 0x7f0b0475

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/EditPhoneNumberPreference;->setDialogTitle(I)V

    .line 1351
    :cond_2
    const-string/jumbo v1, "button_voicemail_provider"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    .line 1352
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    if-eqz v1, :cond_3

    .line 1353
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1355
    const-string/jumbo v1, "button_voicemail_notification_ringtone_key"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1354
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailNotificationRingtone:Landroid/preference/Preference;

    .line 1357
    const-string/jumbo v1, "button_voicemail_notification_vibrate_key"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/VoicemailSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 1356
    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailNotificationVibrate:Landroid/preference/CheckBoxPreference;

    .line 1358
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailNotificationVibrate:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1359
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->initVoiceMailProviders()V

    .line 1368
    :cond_3
    if-nez p1, :cond_4

    .line 1369
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSetting;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.android.phone.CallFeaturesSetting.ADD_VOICEMAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1370
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    if-eqz v1, :cond_4

    .line 1371
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMProvidersData:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-le v1, v3, :cond_5

    .line 1372
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    invoke-direct {p0, v1}, Lcom/android/phone/settings/VoicemailSetting;->simulatePreferenceClick(Landroid/preference/Preference;)V

    .line 1379
    :cond_4
    :goto_0
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->updateVoiceNumberField()V

    .line 1380
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMProviderSettingsForced:Z

    .line 1382
    new-instance v1, Lcom/android/phone/settings/VoicemailSetting$5;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/VoicemailSetting$5;-><init>(Lcom/android/phone/settings/VoicemailSetting;)V

    iput-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mRingtoneLookupRunnable:Ljava/lang/Runnable;

    .line 1392
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->setActionBar(Landroid/app/Activity;)V

    .line 1393
    return-void

    .line 1374
    :cond_5
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    const-string/jumbo v2, ""

    invoke-virtual {p0, v1, v2}, Lcom/android/phone/settings/VoicemailSetting;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    .line 1375
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 11
    .param p1, "id"    # I

    .prologue
    const/16 v10, 0x643

    const/16 v9, 0x641

    const/4 v8, 0x0

    const v7, 0x7f0b0368

    .line 1188
    const/16 v6, 0x5dc

    if-eq p1, v6, :cond_0

    const/16 v6, 0x578

    if-ne p1, v6, :cond_1

    .line 1192
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1195
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f0b035c

    .line 1196
    .local v5, "titleId":I
    sparse-switch p1, :sswitch_data_0

    .line 1227
    const v4, 0x7f0b0361

    .line 1230
    .local v4, "msgId":I
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting;->mDismissAndFinish:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v7, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1234
    :goto_0
    invoke-virtual {p0, v5}, Lcom/android/phone/settings/VoicemailSetting;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1235
    invoke-virtual {p0, v4}, Lcom/android/phone/settings/VoicemailSetting;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1236
    .local v3, "message":Ljava/lang/String;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1237
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1238
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 1241
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/view/Window;->addFlags(I)V

    .line 1243
    return-object v1

    .line 1189
    .end local v0    # "b":Landroid/app/AlertDialog$Builder;
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    .end local v3    # "message":Ljava/lang/String;
    .end local v4    # "msgId":I
    .end local v5    # "titleId":I
    :cond_1
    const/16 v6, 0x5dd

    if-eq p1, v6, :cond_0

    const/16 v6, 0x5de

    if-eq p1, v6, :cond_0

    .line 1190
    const/16 v6, 0x640

    if-eq p1, v6, :cond_0

    .line 1244
    if-eq p1, v9, :cond_2

    const/16 v6, 0x642

    if-ne p1, v6, :cond_3

    .line 1246
    :cond_2
    new-instance v2, Lmiui/app/ProgressDialog;

    invoke-direct {v2, p0}, Lmiui/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 1247
    .local v2, "dialog":Lmiui/app/ProgressDialog;
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Lmiui/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1248
    invoke-virtual {v2, v8}, Lmiui/app/ProgressDialog;->setCancelable(Z)V

    .line 1250
    if-ne p1, v9, :cond_4

    const v6, 0x7f0b035e

    .line 1249
    :goto_1
    invoke-virtual {p0, v6}, Lcom/android/phone/settings/VoicemailSetting;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v2, v6}, Lmiui/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1253
    return-object v2

    .line 1198
    .end local v2    # "dialog":Lmiui/app/ProgressDialog;
    .restart local v0    # "b":Landroid/app/AlertDialog$Builder;
    .restart local v5    # "titleId":I
    :sswitch_0
    const v4, 0x7f0b036c

    .line 1199
    .restart local v4    # "msgId":I
    const v5, 0x7f0b0320

    .line 1201
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v7, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 1206
    .end local v4    # "msgId":I
    :sswitch_1
    const v4, 0x7f0b0370

    .line 1207
    .restart local v4    # "msgId":I
    const v5, 0x7f0b0320

    .line 1209
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v7, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 1212
    .end local v4    # "msgId":I
    :sswitch_2
    const v4, 0x7f0b036d

    .line 1214
    .restart local v4    # "msgId":I
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v7, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 1217
    .end local v4    # "msgId":I
    :sswitch_3
    const v4, 0x7f0b036e

    .line 1219
    .restart local v4    # "msgId":I
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v7, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 1222
    .end local v4    # "msgId":I
    :sswitch_4
    const v4, 0x7f0b036f

    .line 1223
    .restart local v4    # "msgId":I
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    const v7, 0x7f0b04e3

    invoke-virtual {v0, v7, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1224
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    const v7, 0x7f0b04e4

    invoke-virtual {v0, v7, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    .line 1245
    .end local v0    # "b":Landroid/app/AlertDialog$Builder;
    .end local v4    # "msgId":I
    .end local v5    # "titleId":I
    :cond_3
    if-eq p1, v10, :cond_2

    .line 1257
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v6

    return-object v6

    .line 1251
    .restart local v2    # "dialog":Lmiui/app/ProgressDialog;
    :cond_4
    if-ne p1, v10, :cond_5

    const v6, 0x7f0b035f

    goto :goto_1

    .line 1252
    :cond_5
    const v6, 0x7f0b035d

    goto :goto_1

    .line 1196
    :sswitch_data_0
    .sparse-switch
        0x578 -> :sswitch_1
        0x5dc -> :sswitch_2
        0x5dd -> :sswitch_3
        0x5de -> :sswitch_4
        0x640 -> :sswitch_0
    .end sparse-switch
.end method

.method public onDialogClosed(Lcom/android/phone/settings/EditPhoneNumberPreference;I)V
    .locals 3
    .param p1, "preference"    # Lcom/android/phone/settings/EditPhoneNumberPreference;
    .param p2, "buttonClicked"    # I

    .prologue
    .line 454
    iget-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onPreferenceClick: request preference click on dialog close: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 456
    :cond_0
    const/4 v1, -0x2

    if-ne p2, v1, :cond_1

    .line 457
    return-void

    .line 459
    :cond_1
    instance-of v1, p1, Lcom/android/phone/settings/EditPhoneNumberPreference;

    if-eqz v1, :cond_2

    .line 460
    move-object v0, p1

    .line 462
    .local v0, "epn":Lcom/android/phone/settings/EditPhoneNumberPreference;
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    if-ne p1, v1, :cond_2

    .line 463
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->handleVMBtnClickRequest()V

    .line 466
    .end local v0    # "epn":Lcom/android/phone/settings/EditPhoneNumberPreference;
    :cond_2
    return-void
.end method

.method public onGetDefaultNumber(Lcom/android/phone/settings/EditPhoneNumberPreference;)Ljava/lang/String;
    .locals 3
    .param p1, "preference"    # Lcom/android/phone/settings/EditPhoneNumberPreference;

    .prologue
    const/4 v2, 0x0

    .line 474
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    if-ne p1, v1, :cond_1

    .line 477
    iget-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "updating default for voicemail dialog"

    invoke-static {v1}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 478
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->updateVoiceNumberField()V

    .line 479
    return-object v2

    .line 482
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getVoiceMailNumber()Ljava/lang/String;

    move-result-object v0

    .line 483
    .local v0, "vmDisplay":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 486
    return-object v2

    .line 490
    :cond_2
    iget-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "updating default for call forwarding dialogs"

    invoke-static {v1}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 491
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0b0322

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/VoicemailSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1693
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1694
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 1695
    invoke-virtual {p0}, Lcom/android/phone/settings/VoicemailSetting;->finish()V

    .line 1696
    const/4 v1, 0x1

    return v1

    .line 1698
    :cond_0
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 370
    invoke-super {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onPause()V

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mForeground:Z

    .line 372
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    .line 406
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    if-ne p1, v5, :cond_7

    .line 407
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->getCurrentVoicemailProviderKey()Ljava/lang/String;

    move-result-object v0

    .local v0, "currentProviderKey":Ljava/lang/String;
    move-object v2, p2

    .line 408
    check-cast v2, Ljava/lang/String;

    .line 409
    .local v2, "newProviderKey":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "VM provider changes to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 410
    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    .line 409
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 411
    :cond_0
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 412
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_1

    const-string/jumbo v5, "No change "

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 413
    :cond_1
    return v7

    .line 415
    :cond_2
    invoke-direct {p0, v2}, Lcom/android/phone/settings/VoicemailSetting;->updateVMPreferenceWidgets(Ljava/lang/String;)V

    .line 417
    iput-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    .line 420
    invoke-direct {p0, v2}, Lcom/android/phone/settings/VoicemailSetting;->loadSettingsForVoiceMailProvider(Ljava/lang/String;)Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    move-result-object v3

    .line 428
    .local v3, "newProviderSettings":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    if-nez v3, :cond_5

    .line 430
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_3

    const-string/jumbo v5, "Saved preferences not found - invoking config"

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 431
    :cond_3
    iput-boolean v7, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMProviderSettingsForced:Z

    .line 432
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    invoke-direct {p0, v5}, Lcom/android/phone/settings/VoicemailSetting;->simulatePreferenceClick(Landroid/preference/Preference;)V

    .line 449
    .end local v0    # "currentProviderKey":Ljava/lang/String;
    .end local v2    # "newProviderKey":Ljava/lang/String;
    .end local v3    # "newProviderSettings":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    .end local p2    # "objValue":Ljava/lang/Object;
    :cond_4
    :goto_0
    return v7

    .line 434
    .restart local v0    # "currentProviderKey":Ljava/lang/String;
    .restart local v2    # "newProviderKey":Ljava/lang/String;
    .restart local v3    # "newProviderSettings":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_5
    iget-boolean v5, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v5, :cond_6

    const-string/jumbo v5, "Saved preferences found - switching to them"

    invoke-static {v5}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 436
    :cond_6
    iput-boolean v7, p0, Lcom/android/phone/settings/VoicemailSetting;->mChangingVMorFwdDueToProviderChange:Z

    .line 437
    invoke-direct {p0, v2, v3}, Lcom/android/phone/settings/VoicemailSetting;->saveVoiceMailAndForwardingNumber(Ljava/lang/String;Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;)V

    goto :goto_0

    .line 439
    .end local v0    # "currentProviderKey":Ljava/lang/String;
    .end local v2    # "newProviderKey":Ljava/lang/String;
    .end local v3    # "newProviderSettings":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    :cond_7
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailNotificationVibrate:Landroid/preference/CheckBoxPreference;

    if-ne p1, v5, :cond_4

    .line 441
    const-string/jumbo v5, "button_voicemail_notification_vibrate_key"

    iget-object v6, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v6

    .line 440
    invoke-static {v5, v6}, Lcom/android/phone/MiuiPhoneUtils;->getPreferenceKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 443
    .local v4, "vmVibrateKey":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 442
    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 444
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 445
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mSubMenuVoicemailSettings:Lcom/android/phone/settings/EditPhoneNumberPreference;

    if-ne p2, v0, :cond_1

    invoke-virtual {p2}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 390
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Invoking cfg intent "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 391
    :cond_0
    invoke-virtual {p2}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/phone/settings/VoicemailSetting;->startActivityForResult(Landroid/content/Intent;I)V

    .line 392
    const/4 v0, 0x1

    return v0

    .line 394
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 1181
    invoke-super {p0, p1, p2, p3}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 1182
    iput p1, p0, Lcom/android/phone/settings/VoicemailSetting;->mCurrentDialogId:I

    .line 1183
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 1397
    invoke-super {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onResume()V

    .line 1398
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mForeground:Z

    .line 1401
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1400
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1402
    .local v0, "prefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/VoicemailSetting;->migrateVoicemailVibrationSettingsIfNeeded(Landroid/content/SharedPreferences;I)Z

    .line 1403
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailNotificationVibrate:Landroid/preference/CheckBoxPreference;

    .line 1404
    const-string/jumbo v2, "button_voicemail_notification_vibrate_key"

    .line 1405
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v3

    .line 1404
    invoke-static {v2, v3}, Lcom/android/phone/MiuiPhoneUtils;->getPreferenceKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 1405
    const/4 v3, 0x0

    .line 1403
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1406
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->lookupRingtoneName()V

    .line 1407
    return-void
.end method

.method onRevertDone()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 561
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Flipping provider key back to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mVoicemailProviders:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mPreviousVMProviderKey:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->updateVMPreferenceWidgets(Ljava/lang/String;)V

    .line 564
    invoke-direct {p0}, Lcom/android/phone/settings/VoicemailSetting;->updateVoiceNumberField()V

    .line 565
    iget v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMOrFwdSetError:I

    if-eqz v0, :cond_1

    .line 566
    iget v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMOrFwdSetError:I

    invoke-direct {p0, v0}, Lcom/android/phone/settings/VoicemailSetting;->showVMDialog(I)V

    .line 567
    iput v2, p0, Lcom/android/phone/settings/VoicemailSetting;->mVMOrFwdSetError:I

    .line 569
    :cond_1
    return-void
.end method

.method setVMNumberWithCarrier()V
    .locals 5

    .prologue
    .line 941
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "save voicemail #: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->toLogSafePhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 942
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 943
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getVoiceMailAlphaTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 944
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting;->mNewVMNumber:Ljava/lang/String;

    .line 945
    iget-object v3, p0, Lcom/android/phone/settings/VoicemailSetting;->mSetOptionComplete:Landroid/os/Handler;

    const/16 v4, 0x1f4

    invoke-static {v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    .line 942
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/Phone;->setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 946
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 498
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 500
    invoke-super {p0, p1, p2}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 501
    return-void

    .line 504
    :cond_0
    iget-boolean v0, p0, Lcom/android/phone/settings/VoicemailSetting;->DBG:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "startSubActivity: starting requested subactivity"

    invoke-static {v0}, Lcom/android/phone/settings/VoicemailSetting;->log(Ljava/lang/String;)V

    .line 505
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 506
    return-void
.end method
