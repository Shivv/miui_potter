.class public Lcom/android/phone/settings/AccessibilitySettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "AccessibilitySettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/AccessibilitySettingsFragment$1;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mButtonHac:Landroid/preference/SwitchPreference;

.field private mButtonTty:Lcom/android/phone/settings/TtyModeListPreference;

.field private mContext:Landroid/content/Context;

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/AccessibilitySettingsFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/AccessibilitySettingsFragment;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/AccessibilitySettingsFragment;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/AccessibilitySettingsFragment;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->getVolteTtySupported()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/AccessibilitySettingsFragment;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/AccessibilitySettingsFragment;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->isVideoCallOrConferenceInProgress()Z

    move-result v0

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/android/phone/settings/AccessibilitySettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->LOG_TAG:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 45
    new-instance v0, Lcom/android/phone/settings/AccessibilitySettingsFragment$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/AccessibilitySettingsFragment$1;-><init>(Lcom/android/phone/settings/AccessibilitySettingsFragment;)V

    iput-object v0, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 38
    return-void
.end method

.method private getVolteTtySupported()Z
    .locals 3

    .prologue
    .line 138
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "carrier_config"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    .line 139
    .local v0, "configManager":Landroid/telephony/CarrierConfigManager;
    invoke-virtual {v0}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v1

    .line 140
    const-string/jumbo v2, "carrier_volte_tty_supported_bool"

    .line 139
    invoke-virtual {v1, v2}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private isVideoCallOrConferenceInProgress()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 144
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 145
    .local v1, "phones":[Lcom/android/internal/telephony/Phone;
    if-nez v1, :cond_0

    .line 147
    return v3

    .line 150
    :cond_0
    array-length v4, v1

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v1, v2

    .line 151
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->isImsVideoCallOrConferencePresent()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 152
    const/4 v2, 0x1

    return v2

    .line 150
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 155
    .end local v0    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_2
    return v3
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 73
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mContext:Landroid/content/Context;

    .line 76
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "audio"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mAudioManager:Landroid/media/AudioManager;

    .line 78
    const/high16 v1, 0x7f060000

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->addPreferencesFromResource(I)V

    .line 81
    invoke-virtual {p0}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0503

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 80
    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/TtyModeListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonTty:Lcom/android/phone/settings/TtyModeListPreference;

    .line 82
    const-string/jumbo v1, "button_hac_key"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/SwitchPreference;

    iput-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonHac:Landroid/preference/SwitchPreference;

    .line 84
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v1, v1, Lcom/android/phone/PhoneGlobals;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    invoke-virtual {v1}, Lcom/android/phone/PhoneInterfaceManager;->isTtyModeSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonTty:Lcom/android/phone/settings/TtyModeListPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/TtyModeListPreference;->init()V

    .line 91
    :goto_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v1, v1, Lcom/android/phone/PhoneGlobals;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    invoke-virtual {v1}, Lcom/android/phone/PhoneInterfaceManager;->isHearingAidCompatibilitySupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 92
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 93
    const-string/jumbo v4, "hearing_aid"

    .line 92
    invoke-static {v1, v4, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 94
    .local v0, "hac":I
    iget-object v4, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonHac:Landroid/preference/SwitchPreference;

    if-ne v0, v2, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 99
    .end local v0    # "hac":I
    :goto_2
    return-void

    .line 87
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v4, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonTty:Lcom/android/phone/settings/TtyModeListPreference;

    invoke-virtual {v1, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 88
    iput-object v5, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonTty:Lcom/android/phone/settings/TtyModeListPreference;

    goto :goto_0

    .restart local v0    # "hac":I
    :cond_1
    move v1, v3

    .line 94
    goto :goto_1

    .line 96
    .end local v0    # "hac":I
    :cond_2
    invoke-virtual {p0}, Lcom/android/phone/settings/AccessibilitySettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonHac:Landroid/preference/SwitchPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 97
    iput-object v5, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonHac:Landroid/preference/SwitchPreference;

    goto :goto_2
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 111
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 113
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 114
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 115
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v4, 0x1

    .line 119
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonTty:Lcom/android/phone/settings/TtyModeListPreference;

    if-ne p2, v1, :cond_0

    .line 120
    return v4

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonHac:Landroid/preference/SwitchPreference;

    if-ne p2, v1, :cond_3

    .line 122
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mButtonHac:Landroid/preference/SwitchPreference;

    invoke-virtual {v1}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 123
    const/4 v0, 0x1

    .line 125
    .local v0, "hac":I
    :goto_0
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "hearing_aid"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 128
    iget-object v2, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mAudioManager:Landroid/media/AudioManager;

    const-string/jumbo v3, "HACSetting"

    .line 129
    if-ne v0, v4, :cond_2

    .line 130
    const-string/jumbo v1, "ON"

    .line 128
    :goto_1
    invoke-virtual {v2, v3, v1}, Landroid/media/AudioManager;->setParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    return v4

    .line 123
    .end local v0    # "hac":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "hac":I
    goto :goto_0

    .line 130
    :cond_2
    const-string/jumbo v1, "OFF"

    goto :goto_1

    .line 133
    .end local v0    # "hac":I
    :cond_3
    const/4 v1, 0x0

    return v1
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 103
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 105
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 106
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/android/phone/settings/AccessibilitySettingsFragment;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 107
    return-void
.end method
