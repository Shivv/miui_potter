.class public Lcom/android/phone/settings/CallBarringChangePasswordDialog;
.super Ljava/lang/Object;
.source "CallBarringChangePasswordDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private DBG:Z

.field private mCangePasswordPrompt:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mErrorMessage:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mNewPassword:Landroid/widget/EditText;

.field private mNewPasswordConfirm:Landroid/widget/EditText;

.field private mOldPassword:Landroid/widget/EditText;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mProgressDialog:Lmiui/app/ProgressDialog;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->DBG:Z

    return v0
.end method

.method static synthetic -get1()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)Lmiui/app/ProgressDialog;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/CallBarringChangePasswordDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallBarringChangePasswordDialog;
    .param p1, "-value"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mErrorMessage:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->dismiss()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->process()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/CallBarringChangePasswordDialog;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallBarringChangePasswordDialog;
    .param p1, "errorid"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->showErrDialog(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->LOG_TAG:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "slotId"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v5, 0x5

    const/4 v2, 0x1

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    sget v1, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    if-lt v1, v6, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->DBG:Z

    .line 166
    new-instance v1, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;-><init>(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)V

    iput-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mHandler:Landroid/os/Handler;

    .line 64
    iput-object p1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    .line 65
    iput-object v7, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mErrorMessage:Ljava/lang/String;

    .line 67
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x104000a

    invoke-virtual {v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 68
    const/high16 v4, 0x1040000

    .line 67
    invoke-virtual {v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mDialog:Landroid/app/AlertDialog;

    .line 69
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v4, 0x80000

    invoke-virtual {v1, v4}, Landroid/view/Window;->addFlags(I)V

    .line 71
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f040020

    invoke-virtual {v1, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 75
    const v1, 0x7f0d006c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mCangePasswordPrompt:Landroid/widget/TextView;

    .line 77
    const v1, 0x7f0d006d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    .line 78
    const v1, 0x7f0d006e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    .line 79
    const v1, 0x7f0d006f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    .line 81
    invoke-static {p2}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 83
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 84
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 85
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 86
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setTextAlignment(I)V

    .line 87
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 88
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 89
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 90
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setTextAlignment(I)V

    .line 91
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 92
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 93
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 94
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setTextAlignment(I)V

    .line 96
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 98
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 100
    new-instance v1, Lmiui/app/ProgressDialog;

    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Lmiui/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    .line 101
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v1, v3}, Lmiui/app/ProgressDialog;->setCancelable(Z)V

    .line 102
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v1, v2}, Lmiui/app/ProgressDialog;->setIndeterminate(Z)V

    .line 103
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v1}, Lmiui/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d8

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 104
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v1}, Lmiui/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/Window;->addFlags(I)V

    .line 105
    return-void

    .end local v0    # "view":Landroid/view/View;
    :cond_0
    move v1, v3

    .line 40
    goto/16 :goto_0
.end method

.method private dismiss()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v0}, Lmiui/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v0}, Lmiui/app/ProgressDialog;->dismiss()V

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 253
    :cond_1
    return-void
.end method

.method private doChangePassword(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "oldPassword"    # Ljava/lang/String;
    .param p2, "newPassword"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x2

    .line 128
    iget-boolean v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->DBG:Z

    if-eqz v1, :cond_0

    .line 129
    sget-object v1, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "doChangePassword() is called with oldPassword is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 130
    const-string/jumbo v3, "newPassword is "

    .line 129
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v4, v2, v4, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 134
    .local v0, "m":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 135
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-static {v1}, Lcom/android/phone/PhoneProxy;->getCommandsInterface(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/CommandsInterface;

    move-result-object v1

    .line 136
    const-string/jumbo v2, "AB"

    .line 135
    invoke-interface {v1, v2, p1, p2, v0}, Lcom/android/internal/telephony/CommandsInterface;->changeBarringPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 138
    :cond_1
    return-void
.end method

.method private process()V
    .locals 7

    .prologue
    const v6, 0x7f0b060f

    .line 145
    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 146
    .local v3, "oldPassword":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 147
    .local v2, "newPassword":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "confirmNewPassword":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 150
    .local v1, "msg":Landroid/os/Message;
    invoke-static {v3}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->validatePassword(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 151
    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 152
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 164
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 154
    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0611

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 155
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 156
    :cond_1
    invoke-static {v2}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->validatePassword(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 157
    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 158
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 160
    :cond_2
    const/4 v4, 0x2

    iput v4, v1, Landroid/os/Message;->what:I

    .line 161
    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v4}, Lmiui/app/ProgressDialog;->show()V

    .line 162
    invoke-direct {p0, v3, v2}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->doChangePassword(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showErrDialog(I)V
    .locals 6
    .param p1, "errorid"    # I

    .prologue
    .line 214
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 216
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0b035c

    .line 217
    .local v3, "titleId":I
    sparse-switch p1, :sswitch_data_0

    .line 229
    const v2, 0x7f0b0361

    .line 232
    .local v2, "msgId":I
    :goto_0
    new-instance v4, Lcom/android/phone/settings/CallBarringChangePasswordDialog$2;

    invoke-direct {v4, p0}, Lcom/android/phone/settings/CallBarringChangePasswordDialog$2;-><init>(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)V

    const v5, 0x7f0b0368

    invoke-virtual {v0, v5, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 238
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 239
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 240
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 241
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 242
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 243
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 244
    return-void

    .line 219
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    .end local v2    # "msgId":I
    :sswitch_0
    const v2, 0x7f0b0614

    .line 220
    .restart local v2    # "msgId":I
    goto :goto_0

    .line 222
    .end local v2    # "msgId":I
    :sswitch_1
    const v2, 0x7f0b062e

    .line 223
    .restart local v2    # "msgId":I
    goto :goto_0

    .line 225
    .end local v2    # "msgId":I
    :sswitch_2
    const v2, 0x7f0b0360

    .line 226
    .restart local v2    # "msgId":I
    goto :goto_0

    .line 217
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x190 -> :sswitch_2
    .end sparse-switch
.end method

.method private static validatePassword(Ljava/lang/String;)Z
    .locals 3
    .param p0, "password"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 141
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 265
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 266
    const/4 v0, 0x1

    .line 267
    .local v0, "enablePositiveButton":Z
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 268
    const/4 v0, 0x0

    .line 270
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 271
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 272
    const/4 v0, 0x0

    .line 274
    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 275
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 276
    const/4 v0, 0x0

    .line 279
    :cond_2
    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 281
    .end local v0    # "enablePositiveButton":Z
    :cond_3
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 257
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 285
    const/4 v0, -0x2

    if-ne p2, v0, :cond_1

    .line 286
    invoke-direct {p0}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->dismiss()V

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 261
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mErrorMessage:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mCangePasswordPrompt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mCangePasswordPrompt:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114
    :goto_0
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mDialog:Landroid/app/AlertDialog;

    const v1, 0x7f0b0609

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 115
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    const v1, 0x7f0b060d

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 116
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mOldPassword:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 118
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    const v1, 0x7f0b060e

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 119
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPassword:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    const v1, 0x7f0b0610

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 121
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mNewPasswordConfirm:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f0b035e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 124
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->afterTextChanged(Landroid/text/Editable;)V

    .line 125
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->mCangePasswordPrompt:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
