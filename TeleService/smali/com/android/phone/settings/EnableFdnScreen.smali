.class public Lcom/android/phone/settings/EnableFdnScreen;
.super Landroid/app/Activity;
.source "EnableFdnScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/EnableFdnScreen$1;,
        Lcom/android/phone/settings/EnableFdnScreen$2;
    }
.end annotation


# instance fields
.field private mClicked:Landroid/view/View$OnClickListener;

.field private mEnable:Z

.field private mHandler:Landroid/os/Handler;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mPin2Field:Landroid/widget/EditText;

.field private mPinFieldContainer:Landroid/widget/LinearLayout;

.field private mStatusField:Landroid/widget/TextView;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/EnableFdnScreen;)Landroid/widget/EditText;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/EnableFdnScreen;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPin2Field:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/EnableFdnScreen;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/EnableFdnScreen;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/EnableFdnScreen;->enableFdn()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/EnableFdnScreen;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/EnableFdnScreen;
    .param p1, "ar"    # Landroid/os/AsyncResult;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/EnableFdnScreen;->handleResult(Landroid/os/AsyncResult;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/EnableFdnScreen;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/EnableFdnScreen;
    .param p1, "statusMsg"    # Ljava/lang/CharSequence;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/EnableFdnScreen;->showStatus(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 55
    new-instance v0, Lcom/android/phone/settings/EnableFdnScreen$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/EnableFdnScreen$1;-><init>(Lcom/android/phone/settings/EnableFdnScreen;)V

    iput-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mHandler:Landroid/os/Handler;

    .line 149
    new-instance v0, Lcom/android/phone/settings/EnableFdnScreen$2;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/EnableFdnScreen$2;-><init>(Lcom/android/phone/settings/EnableFdnScreen;)V

    iput-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mClicked:Landroid/view/View$OnClickListener;

    .line 43
    return-void
.end method

.method private enableFdn()V
    .locals 4

    .prologue
    .line 124
    iget-object v1, p0, Lcom/android/phone/settings/EnableFdnScreen;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 125
    .local v0, "callback":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/phone/settings/EnableFdnScreen;->mEnable:Z

    invoke-direct {p0}, Lcom/android/phone/settings/EnableFdnScreen;->getPin2()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/android/internal/telephony/IccCard;->setIccFdnEnabled(ZLjava/lang/String;Landroid/os/Message;)V

    .line 127
    return-void
.end method

.method private getPin2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPin2Field:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleResult(Landroid/os/AsyncResult;)V
    .locals 4
    .param p1, "ar"    # Landroid/os/AsyncResult;

    .prologue
    .line 130
    iget-object v0, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v0, :cond_2

    .line 132
    invoke-virtual {p0}, Lcom/android/phone/settings/EnableFdnScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mEnable:Z

    if-eqz v0, :cond_1

    .line 133
    const v0, 0x7f0b042a

    .line 132
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/phone/settings/EnableFdnScreen;->showStatus(Ljava/lang/CharSequence;)V

    .line 142
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/phone/settings/EnableFdnScreen$3;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/EnableFdnScreen$3;-><init>(Lcom/android/phone/settings/EnableFdnScreen;)V

    .line 146
    const-wide/16 v2, 0xbb8

    .line 142
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 147
    return-void

    .line 133
    :cond_1
    const v0, 0x7f0b042b

    goto :goto_0

    .line 134
    :cond_2
    iget-object v0, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v0, v0, Lcom/android/internal/telephony/CommandException;

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/android/phone/settings/EnableFdnScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 139
    const v1, 0x7f0b045d

    .line 138
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/phone/settings/EnableFdnScreen;->showStatus(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private setupView()V
    .locals 2

    .prologue
    .line 99
    const v0, 0x7f0d00bf

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/EnableFdnScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPin2Field:Landroid/widget/EditText;

    .line 100
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPin2Field:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 101
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPin2Field:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 102
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPin2Field:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/phone/settings/EnableFdnScreen;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    const v0, 0x7f0d00a8

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/EnableFdnScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPinFieldContainer:Landroid/widget/LinearLayout;

    .line 105
    const v0, 0x7f0d00c0

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/EnableFdnScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mStatusField:Landroid/widget/TextView;

    .line 106
    return-void
.end method

.method private showStatus(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "statusMsg"    # Ljava/lang/CharSequence;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 109
    if-eqz p1, :cond_0

    .line 110
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mStatusField:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mStatusField:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPinFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 117
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPinFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mStatusField:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    const v0, 0x7f040035

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/EnableFdnScreen;->setContentView(I)V

    .line 73
    invoke-direct {p0}, Lcom/android/phone/settings/EnableFdnScreen;->setupView()V

    .line 74
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/phone/settings/EnableFdnScreen;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 95
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 96
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 78
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 80
    invoke-static {p0}, Lcom/android/phone/settings/SimPickerPreference;->showSimPicker(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    return-void

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/EnableFdnScreen;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 84
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v3

    .line 83
    invoke-static {v2, v3}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v1

    .line 85
    .local v1, "slotId":I
    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 86
    iget-object v2, p0, Lcom/android/phone/settings/EnableFdnScreen;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/internal/telephony/IccCard;->getIccFdnEnabled()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    iput-boolean v2, p0, Lcom/android/phone/settings/EnableFdnScreen;->mEnable:Z

    .line 88
    iget-boolean v2, p0, Lcom/android/phone/settings/EnableFdnScreen;->mEnable:Z

    if-eqz v2, :cond_1

    const v0, 0x7f0b0427

    .line 89
    .local v0, "id":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/phone/settings/EnableFdnScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/EnableFdnScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 90
    return-void

    .line 88
    .end local v0    # "id":I
    :cond_1
    const v0, 0x7f0b0428

    .restart local v0    # "id":I
    goto :goto_0
.end method
