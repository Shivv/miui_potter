.class Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;
.super Ljava/lang/Object;
.source "PhoneAccountSettingsFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/PhoneAccountSettingsFragment;->initAccountList(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/telecom/PhoneAccount;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/PhoneAccountSettingsFragment;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/PhoneAccountSettingsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;->this$0:Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public compare(Landroid/telecom/PhoneAccount;Landroid/telecom/PhoneAccount;)I
    .locals 11
    .param p1, "account1"    # Landroid/telecom/PhoneAccount;
    .param p2, "account2"    # Landroid/telecom/PhoneAccount;

    .prologue
    const/4 v9, 0x4

    const/4 v10, -0x1

    .line 329
    const/4 v6, 0x0

    .line 332
    .local v6, "retval":I
    invoke-virtual {p1, v9}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v0

    .line 333
    .local v0, "isSim1":Z
    invoke-virtual {p2, v9}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v1

    .line 334
    .local v1, "isSim2":Z
    if-eq v0, v1, :cond_0

    .line 335
    if-eqz v0, :cond_5

    const/4 v6, -0x1

    .line 338
    :cond_0
    :goto_0
    iget-object v9, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;->this$0:Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    invoke-static {v9}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->-get1(Lcom/android/phone/settings/PhoneAccountSettingsFragment;)Landroid/telephony/TelephonyManager;

    move-result-object v9

    invoke-virtual {v9, p1}, Landroid/telephony/TelephonyManager;->getSubIdForPhoneAccount(Landroid/telecom/PhoneAccount;)I

    move-result v7

    .line 339
    .local v7, "subId1":I
    iget-object v9, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;->this$0:Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    invoke-static {v9}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->-get1(Lcom/android/phone/settings/PhoneAccountSettingsFragment;)Landroid/telephony/TelephonyManager;

    move-result-object v9

    invoke-virtual {v9, p2}, Landroid/telephony/TelephonyManager;->getSubIdForPhoneAccount(Landroid/telecom/PhoneAccount;)I

    move-result v8

    .line 340
    .local v8, "subId2":I
    if-eq v7, v10, :cond_1

    .line 341
    if-eq v8, v10, :cond_1

    .line 342
    iget-object v9, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;->this$0:Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    invoke-static {v9}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->-get0(Lcom/android/phone/settings/PhoneAccountSettingsFragment;)Landroid/telephony/SubscriptionManager;

    invoke-static {v7}, Landroid/telephony/SubscriptionManager;->getSlotIndex(I)I

    move-result v9

    .line 343
    iget-object v10, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;->this$0:Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    invoke-static {v10}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->-get0(Lcom/android/phone/settings/PhoneAccountSettingsFragment;)Landroid/telephony/SubscriptionManager;

    invoke-static {v8}, Landroid/telephony/SubscriptionManager;->getSlotIndex(I)I

    move-result v10

    .line 342
    if-ge v9, v10, :cond_6

    .line 343
    const/4 v6, -0x1

    .line 347
    :cond_1
    :goto_1
    if-nez v6, :cond_2

    .line 348
    invoke-virtual {p1}, Landroid/telecom/PhoneAccount;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v9

    invoke-virtual {v9}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 349
    .local v4, "pkg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/telecom/PhoneAccount;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v9

    invoke-virtual {v9}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 350
    .local v5, "pkg2":Ljava/lang/String;
    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    .line 354
    .end local v4    # "pkg1":Ljava/lang/String;
    .end local v5    # "pkg2":Ljava/lang/String;
    :cond_2
    if-nez v6, :cond_3

    .line 355
    iget-object v9, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;->this$0:Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    invoke-virtual {p1}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->-wrap0(Lcom/android/phone/settings/PhoneAccountSettingsFragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 356
    .local v2, "label1":Ljava/lang/String;
    iget-object v9, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;->this$0:Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    invoke-virtual {p2}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->-wrap0(Lcom/android/phone/settings/PhoneAccountSettingsFragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 357
    .local v3, "label2":Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    .line 361
    .end local v2    # "label1":Ljava/lang/String;
    .end local v3    # "label2":Ljava/lang/String;
    :cond_3
    if-nez v6, :cond_4

    .line 362
    invoke-virtual {p1}, Landroid/telecom/PhoneAccount;->hashCode()I

    move-result v9

    invoke-virtual {p2}, Landroid/telecom/PhoneAccount;->hashCode()I

    move-result v10

    sub-int v6, v9, v10

    .line 364
    :cond_4
    return v6

    .line 335
    .end local v7    # "subId1":I
    .end local v8    # "subId2":I
    :cond_5
    const/4 v6, 0x1

    goto :goto_0

    .line 343
    .restart local v7    # "subId1":I
    .restart local v8    # "subId2":I
    :cond_6
    const/4 v6, 0x1

    goto :goto_1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 327
    check-cast p1, Landroid/telecom/PhoneAccount;

    check-cast p2, Landroid/telecom/PhoneAccount;

    invoke-virtual {p0, p1, p2}, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;->compare(Landroid/telecom/PhoneAccount;Landroid/telecom/PhoneAccount;)I

    move-result v0

    return v0
.end method
