.class Lcom/android/phone/settings/SmscAddressSettingActivity$3;
.super Ljava/lang/Object;
.source "SmscAddressSettingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/SmscAddressSettingActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/SmscAddressSettingActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/SmscAddressSettingActivity;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$3;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 306
    iget-object v1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$3;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v1}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get1(Lcom/android/phone/settings/SmscAddressSettingActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 307
    .local v0, "newAddress":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$3;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v1}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get4(Lcom/android/phone/settings/SmscAddressSettingActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 309
    iget-object v1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$3;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v1}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get3(Lcom/android/phone/settings/SmscAddressSettingActivity;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$3;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v2, v0}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-wrap1(Lcom/android/phone/settings/SmscAddressSettingActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 310
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$3;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v3}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get2(Lcom/android/phone/settings/SmscAddressSettingActivity;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x3ea

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 309
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/Phone;->setSmscAddress(Ljava/lang/String;Landroid/os/Message;)V

    .line 311
    iget-object v1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$3;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v1}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get2(Lcom/android/phone/settings/SmscAddressSettingActivity;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x1388

    const/16 v4, 0x3ec

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 315
    :goto_0
    return-void

    .line 313
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$3;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-virtual {v1}, Lcom/android/phone/settings/SmscAddressSettingActivity;->finish()V

    goto :goto_0
.end method
