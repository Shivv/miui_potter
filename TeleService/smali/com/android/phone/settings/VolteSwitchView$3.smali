.class Lcom/android/phone/settings/VolteSwitchView$3;
.super Ljava/lang/Object;
.source "VolteSwitchView.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/VolteSwitchView;-><init>(Landroid/preference/PreferenceActivity;Landroid/preference/PreferenceGroup;Lcom/android/internal/telephony/Phone;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/VolteSwitchView;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/VolteSwitchView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v5, 0x0

    .line 78
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get4(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/PreferenceActivity;

    move-result-object v2

    if-nez v2, :cond_0

    .line 79
    sget-object v2, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "mPrefActivity == null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    return v5

    .line 82
    :cond_0
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-wrap0(Lcom/android/phone/settings/VolteSwitchView;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get4(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/PreferenceActivity;

    move-result-object v2

    const v3, 0x7f0b0654

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 85
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v3}, Lcom/android/phone/settings/VolteSwitchView;->-get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 86
    return v5

    .line 89
    :cond_1
    invoke-static {}, Lcom/android/phone/settings/VolteSwitchView;->-get0()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get3(Lcom/android/phone/settings/VolteSwitchView;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 90
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    :goto_0
    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->isCTVoLTESupport(Lcom/android/internal/telephony/Phone;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 91
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    .line 90
    if-eqz v2, :cond_3

    .line 92
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2, v1}, Lcom/android/phone/settings/VolteSwitchView;->-wrap3(Lcom/android/phone/settings/VolteSwitchView;Lcom/android/internal/telephony/Phone;)V

    .line 104
    :goto_1
    const/4 v2, 0x1

    return v2

    .line 89
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_2
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getDataPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .restart local v1    # "phone":Lcom/android/internal/telephony/Phone;
    goto :goto_0

    .line 94
    :cond_3
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-wrap2(Lcom/android/phone/settings/VolteSwitchView;)V

    .line 96
    :try_start_0
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-wrap1(Lcom/android/phone/settings/VolteSwitchView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v2, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setEnhanced4gLteModeSetting exception="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get4(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/PreferenceActivity;

    move-result-object v3

    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f0b06e4

    :goto_2
    invoke-static {v3, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 101
    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v2}, Lcom/android/phone/settings/VolteSwitchView;->-get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/VolteSwitchView$3;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v3}, Lcom/android/phone/settings/VolteSwitchView;->-get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    .line 99
    :cond_4
    const v2, 0x7f0b06e5

    goto :goto_2
.end method
