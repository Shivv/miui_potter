.class Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;
.super Ljava/lang/Object;
.source "CallWaitingCheckBoxPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->onClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x1

    .line 76
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v0, v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-set0(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;Z)Z

    .line 77
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v0}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get3(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get2(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v3, v1}, Lcom/android/internal/telephony/Phone;->setCallWaiting(ZLandroid/os/Message;)V

    .line 79
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v0}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v0}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$1;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    .line 82
    :cond_0
    return-void
.end method
