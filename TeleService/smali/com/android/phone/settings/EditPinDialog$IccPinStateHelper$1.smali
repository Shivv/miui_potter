.class final Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper$1;
.super Ljava/lang/Thread;
.source "EditPinDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->updateIccCardStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 610
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 613
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 614
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 615
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v2, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    if-ge v0, v2, :cond_0

    .line 616
    move v1, v0

    .line 617
    .local v1, "slotId":I
    invoke-static {}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->-get0()[Z

    move-result-object v2

    aput-boolean v4, v2, v1

    .line 618
    invoke-static {}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->-get2()[Z

    move-result-object v2

    aput-boolean v4, v2, v1

    .line 619
    invoke-static {}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->-get1()[Ljava/lang/Object;

    move-result-object v2

    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    aput-object v3, v2, v1

    .line 621
    invoke-static {v1}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->-wrap0(I)Lcom/android/internal/telephony/CommandsInterface;

    move-result-object v2

    new-instance v3, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper$1$1;

    invoke-direct {v3, p0, v1}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper$1$1;-><init>(Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper$1;I)V

    invoke-virtual {v3}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper$1$1;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/android/internal/telephony/CommandsInterface;->getIccCardStatus(Landroid/os/Message;)V

    .line 615
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 629
    .end local v1    # "slotId":I
    :cond_0
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 630
    return-void
.end method
