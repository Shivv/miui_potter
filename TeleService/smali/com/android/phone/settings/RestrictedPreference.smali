.class public Lcom/android/phone/settings/RestrictedPreference;
.super Landroid/preference/Preference;
.source "RestrictedPreference.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDisabledByAdmin:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/settings/RestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    const v0, 0x101008e

    invoke-direct {p0, p1, p2, v0}, Lcom/android/phone/settings/RestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/phone/settings/RestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 33
    iput-object p1, p0, Lcom/android/phone/settings/RestrictedPreference;->mContext:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 39
    iget-boolean v1, p0, Lcom/android/phone/settings/RestrictedPreference;->mDisabledByAdmin:Z

    if-eqz v1, :cond_0

    .line 40
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 42
    :cond_0
    const v1, 0x1020010

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 43
    .local v0, "summaryView":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/android/phone/settings/RestrictedPreference;->mDisabledByAdmin:Z

    if-eqz v1, :cond_1

    .line 45
    invoke-virtual {p0}, Lcom/android/phone/settings/RestrictedPreference;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0b0173

    .line 44
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 46
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 48
    :cond_1
    return-void

    .line 45
    :cond_2
    const v1, 0x7f0b0174

    goto :goto_0
.end method

.method public performClick(Landroid/preference/PreferenceScreen;)V
    .locals 1
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/android/phone/settings/RestrictedPreference;->mDisabledByAdmin:Z

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/android/phone/settings/RestrictedPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/services/telephony/SimpleFeatures;->ShowAdminSupportDetailsIntent(Landroid/content/Context;)V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/Preference;->performClick(Landroid/preference/PreferenceScreen;)V

    goto :goto_0
.end method

.method public setDisabledByAdmin(Z)V
    .locals 1
    .param p1, "disabled"    # Z

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/android/phone/settings/RestrictedPreference;->mDisabledByAdmin:Z

    if-eq v0, p1, :cond_0

    .line 61
    iput-boolean p1, p0, Lcom/android/phone/settings/RestrictedPreference;->mDisabledByAdmin:Z

    .line 62
    xor-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/RestrictedPreference;->setEnabled(Z)V

    .line 64
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 52
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/phone/settings/RestrictedPreference;->mDisabledByAdmin:Z

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/RestrictedPreference;->setDisabledByAdmin(Z)V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0
.end method
