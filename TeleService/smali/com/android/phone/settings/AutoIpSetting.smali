.class public Lcom/android/phone/settings/AutoIpSetting;
.super Lmiui/preference/PreferenceActivity;
.source "AutoIpSetting.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/AutoIpSetting$1;
    }
.end annotation


# instance fields
.field private mAddZeroPrefix:Landroid/preference/CheckBoxPreference;

.field private mAutoIpEnable:Landroid/preference/CheckBoxPreference;

.field private mAutoIpExceptions:Landroid/preference/Preference;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSlotId:I

.field private mSupportLocalNumbers:Landroid/preference/CheckBoxPreference;

.field private mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

.field private mTextIpPrefix:Landroid/preference/EditTextPreference;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/AutoIpSetting;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/AutoIpSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/AutoIpSetting;->mAutoIpEnable:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/AutoIpSetting;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/AutoIpSetting;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    return v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/AutoIpSetting;)Landroid/preference/EditTextPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/AutoIpSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/AutoIpSetting;Landroid/preference/Preference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/AutoIpSetting;
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/AutoIpSetting;->simulatePreferenceClick(Landroid/preference/Preference;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/AutoIpSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/AutoIpSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/AutoIpSetting;->updateTextIpPrefix()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    .line 135
    new-instance v0, Lcom/android/phone/settings/AutoIpSetting$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/AutoIpSetting$1;-><init>(Lcom/android/phone/settings/AutoIpSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/AutoIpSetting;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 42
    return-void
.end method

.method public static addIpPrefix(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "slotId"    # I

    .prologue
    .line 291
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 292
    invoke-static {p0, p2}, Lmiui/telephony/PhoneNumberUtils;->getDefaultIpBySim(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 291
    invoke-static {v3, v4, p2}, Landroid/provider/MiuiSettings$Telephony;->getAutoIpPrefix(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 293
    .local v0, "ipPrefix":Ljava/lang/String;
    move-object v1, p1

    .line 294
    .local v1, "number":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 295
    invoke-static {p1}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->parse(Ljava/lang/CharSequence;)Lmiui/telephony/PhoneNumberUtils$PhoneNumber;

    move-result-object v2

    .line 297
    .local v2, "pn":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 298
    const-string/jumbo v3, "+86"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 299
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 305
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 306
    invoke-virtual {v2}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->recycle()V

    .line 309
    .end local v2    # "pn":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    :cond_0
    return-object v1

    .line 300
    .restart local v2    # "pn":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    :cond_1
    const-string/jumbo v3, "0086"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 301
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 303
    :cond_2
    const-string/jumbo v3, "+"

    const-string/jumbo v4, "00"

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private alertToInputCurrentAreaCode()V
    .locals 3

    .prologue
    .line 219
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b05bf

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 220
    const v1, 0x1010355

    .line 219
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 221
    const/4 v1, 0x0

    .line 219
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 222
    new-instance v1, Lcom/android/phone/settings/AutoIpSetting$3;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/AutoIpSetting$3;-><init>(Lcom/android/phone/settings/AutoIpSetting;)V

    const v2, 0x7f0b05c0

    .line 219
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 228
    return-void
.end method

.method private closeAreaCodeDialog()V
    .locals 2

    .prologue
    .line 244
    iget-object v1, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 245
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 246
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 248
    :cond_0
    return-void
.end method

.method public static formatNumberWithIp(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "slotId"    # I

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    .line 326
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8, p2}, Landroid/provider/MiuiSettings$Telephony;->getCurrentAeraCode(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v2

    .line 327
    .local v2, "currentAreaCode":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-ge v8, v9, :cond_1

    .line 329
    :cond_0
    return-object p1

    .line 333
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 334
    invoke-static {p0, p2}, Lmiui/telephony/PhoneNumberUtils;->getDefaultIpBySim(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 333
    invoke-static {v8, v9, p2}, Landroid/provider/MiuiSettings$Telephony;->getAutoIpPrefix(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 335
    .local v4, "ipPrefix":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 336
    return-object p1

    .line 338
    :cond_2
    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 340
    return-object p1

    .line 343
    :cond_3
    invoke-static {p1}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->parse(Ljava/lang/CharSequence;)Lmiui/telephony/PhoneNumberUtils$PhoneNumber;

    move-result-object v7

    .line 344
    .local v7, "pn":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    invoke-virtual {v7}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getPrefix()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-virtual {v7}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getPrefix()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_7

    .line 345
    invoke-virtual {v7}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    .line 346
    .local v1, "countryCode":Ljava/lang/String;
    invoke-virtual {v7, p0}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getLocationAreaCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 347
    .local v0, "areaCode":Ljava/lang/String;
    const-string/jumbo v8, "0"

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 350
    .local v3, "id":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8, p2}, Landroid/provider/MiuiSettings$Telephony;->isAutoAddZeroPrefix(Landroid/content/ContentResolver;I)Z

    move-result v5

    .line 352
    .local v5, "isAddZeroPrefixEnabled":Z
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8, p2}, Landroid/provider/MiuiSettings$Telephony;->isAutoIpSupportLocalNum(Landroid/content/ContentResolver;I)Z

    move-result v6

    .line 356
    .local v6, "isSupportLocalNumbers":Z
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    if-nez v6, :cond_5

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    xor-int/lit8 v8, v8, 0x1

    if-nez v8, :cond_5

    .line 357
    :cond_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    const-string/jumbo v8, "86"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    xor-int/lit8 v8, v8, 0x1

    .line 356
    if-eqz v8, :cond_7

    .line 358
    :cond_5
    const-string/jumbo v8, "+86"

    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 360
    invoke-virtual {v7}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getAreaCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-gtz v8, :cond_6

    invoke-virtual {v7}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->isNormalMobileNumber()Z

    move-result v8

    if-eqz v8, :cond_9

    if-eqz v5, :cond_9

    .line 361
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "0"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 382
    .end local v0    # "areaCode":Ljava/lang/String;
    .end local v1    # "countryCode":Ljava/lang/String;
    .end local v3    # "id":Ljava/lang/String;
    .end local v5    # "isAddZeroPrefixEnabled":Z
    .end local v6    # "isSupportLocalNumbers":Z
    :cond_7
    :goto_1
    invoke-virtual {v7}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->recycle()V

    .line 384
    return-object p1

    .line 348
    .restart local v0    # "areaCode":Ljava/lang/String;
    .restart local v1    # "countryCode":Ljava/lang/String;
    :cond_8
    move-object v3, v2

    .restart local v3    # "id":Ljava/lang/String;
    goto :goto_0

    .line 363
    .restart local v5    # "isAddZeroPrefixEnabled":Z
    .restart local v6    # "isSupportLocalNumbers":Z
    :cond_9
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 365
    :cond_a
    const-string/jumbo v8, "0086"

    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 367
    invoke-virtual {v7}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getAreaCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-gtz v8, :cond_b

    invoke-virtual {v7}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->isNormalMobileNumber()Z

    move-result v8

    if-eqz v8, :cond_c

    if-eqz v5, :cond_c

    .line 368
    :cond_b
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "0"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 370
    :cond_c
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 373
    :cond_d
    invoke-virtual {v7}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->isNormalMobileNumber()Z

    move-result v8

    if-eqz v8, :cond_e

    if-eqz v5, :cond_e

    .line 374
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "0"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1

    .line 377
    :cond_e
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "+"

    const-string/jumbo v10, "00"

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1
.end method

.method public static handleIpCall(Landroid/net/Uri;Lcom/android/internal/telephony/Phone;Ljava/lang/String;Landroid/os/Bundle;)Landroid/net/Uri;
    .locals 5
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 267
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 268
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/android/phone/settings/AutoIpSetting;->updateAutoIpSettings(Landroid/content/Context;)V

    .line 269
    const-string/jumbo v3, "tel"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 270
    return-object p0

    .line 272
    :cond_0
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    .line 273
    .local v2, "slotId":I
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    .line 274
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lmiui/telephony/TelephonyManager;->getSimOperatorForSlot(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lmiui/telephony/PhoneNumberUtils;->isChineseOperator(Ljava/lang/String;)Z

    move-result v3

    .line 273
    if-eqz v3, :cond_3

    .line 275
    move-object v1, p2

    .line 276
    .local v1, "dialNumber":Ljava/lang/String;
    const-string/jumbo v3, "com.android.phone.IS_IPCALL"

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 277
    invoke-static {v0, p2, v2}, Lcom/android/phone/settings/AutoIpSetting;->addIpPrefix(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 279
    :cond_1
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/provider/MiuiSettings$Telephony;->isAutoIpEnable(Landroid/content/ContentResolver;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 280
    invoke-static {v0, v1, v2}, Lcom/android/phone/settings/AutoIpExceptionSetting;->contains(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 279
    if-eqz v3, :cond_2

    .line 281
    invoke-static {v0, v1, v2}, Lcom/android/phone/settings/AutoIpSetting;->formatNumberWithIp(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 283
    :cond_2
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 284
    const-string/jumbo v3, "tel"

    const/4 v4, 0x0

    invoke-static {v3, v1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 287
    .end local v1    # "dialNumber":Ljava/lang/String;
    :cond_3
    return-object p0
.end method

.method private simulatePreferenceClick(Landroid/preference/Preference;)V
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    .line 234
    .local v6, "adapter":Landroid/widget/ListAdapter;
    const/4 v3, 0x0

    .local v3, "idx":I
    :goto_0
    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 235
    invoke-interface {v6, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 236
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 237
    invoke-interface {v6, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    const/4 v2, 0x0

    .line 236
    invoke-virtual/range {v0 .. v5}, Landroid/preference/PreferenceScreen;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 241
    :cond_0
    return-void

    .line 234
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static updateAutoIpSettings(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, -0x1

    .line 401
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 402
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-static {v0, v7}, Landroid/provider/MiuiSettings$Telephony;->isAutoIpEnable(Landroid/content/ContentResolver;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 403
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v3

    .line 404
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "info$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/telephony/SubscriptionInfo;

    .line 405
    .local v1, "info":Lmiui/telephony/SubscriptionInfo;
    invoke-virtual {v1}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v4

    .line 407
    .local v4, "slotId":I
    invoke-static {p0, v4}, Lmiui/telephony/PhoneNumberUtils;->getDefaultIpBySim(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v6

    .line 406
    invoke-static {v0, v5, v6}, Landroid/provider/MiuiSettings$Telephony;->getAutoIpPrefix(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoIpPrefix(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 408
    const/4 v5, 0x1

    invoke-static {v0, v5, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoIpEnable(Landroid/content/ContentResolver;ZI)V

    .line 410
    invoke-static {v0, v7}, Landroid/provider/MiuiSettings$Telephony;->isAutoAddZeroPrefix(Landroid/content/ContentResolver;I)Z

    move-result v5

    .line 409
    invoke-static {v0, v5, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoAddZeroPrefixEnable(Landroid/content/ContentResolver;ZI)V

    .line 412
    invoke-static {v0, v7}, Landroid/provider/MiuiSettings$Telephony;->getCurrentAeraCode(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v5

    .line 411
    invoke-static {v0, v5, v4}, Landroid/provider/MiuiSettings$Telephony;->setCurrentAeraCode(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 414
    invoke-static {v0, v7}, Landroid/provider/MiuiSettings$Telephony;->isAutoIpSupportLocalNum(Landroid/content/ContentResolver;I)Z

    move-result v5

    .line 413
    invoke-static {v0, v5, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoIpSupportLocalNumEnable(Landroid/content/ContentResolver;ZI)V

    .line 416
    invoke-static {p0, v7}, Lcom/android/phone/settings/AutoIpExceptionSetting;->loadNumbers(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v5

    .line 415
    invoke-static {p0, v4, v5}, Lcom/android/phone/settings/AutoIpExceptionSetting;->saveNumbers(Landroid/content/Context;ILjava/util/HashMap;)V

    goto :goto_0

    .line 418
    .end local v1    # "info":Lmiui/telephony/SubscriptionInfo;
    .end local v4    # "slotId":I
    :cond_0
    const/4 v5, 0x0

    invoke-static {v0, v5, v7}, Landroid/provider/MiuiSettings$Telephony;->setAutoIpEnable(Landroid/content/ContentResolver;ZI)V

    .line 420
    .end local v2    # "info$iterator":Ljava/util/Iterator;
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    :cond_1
    return-void
.end method

.method private updateTextIpPrefix()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 251
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-virtual {v3, v4}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v2

    .line 252
    .local v2, "subId":I
    if-gez v2, :cond_0

    .line 253
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->finish()V

    .line 255
    :cond_0
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    if-nez v3, :cond_1

    .line 256
    return-void

    .line 258
    :cond_1
    iget v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {p0, v3}, Lmiui/telephony/PhoneNumberUtils;->getDefaultIpBySim(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "defaultIpPrefix":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    .line 259
    invoke-static {v3, v0, v4}, Landroid/provider/MiuiSettings$Telephony;->getAutoIpPrefix(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 261
    .local v1, "ipPrefix":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    invoke-virtual {v3, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 262
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 263
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    const v5, 0x7f0b05b3

    invoke-virtual {p0, v5, v4}, Lcom/android/phone/settings/AutoIpSetting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 264
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 146
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 148
    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v4, "android.intent.action.ACTION_SUBINFO_RECORD_UPDATED"

    invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 149
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/android/phone/settings/AutoIpSetting;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 150
    invoke-static {p0}, Lcom/android/phone/settings/SimPickerPreference;->showSimPicker(Landroid/app/Activity;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 151
    return-void

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v5

    invoke-static {v4, v5}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v4

    iput v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    .line 155
    invoke-static {p0}, Lcom/android/phone/settings/AutoIpSetting;->updateAutoIpSettings(Landroid/content/Context;)V

    .line 157
    const v4, 0x7f060003

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/AutoIpSetting;->addPreferencesFromResource(I)V

    .line 159
    const-string/jumbo v4, "button_autoip"

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/AutoIpSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mAutoIpEnable:Landroid/preference/CheckBoxPreference;

    .line 160
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mAutoIpEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget v6, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v5, v6}, Landroid/provider/MiuiSettings$Telephony;->isAutoIpEnable(Landroid/content/ContentResolver;I)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 161
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mAutoIpEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 163
    const-string/jumbo v4, "button_text_autoip_prefix"

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/AutoIpSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/EditTextPreference;

    iput-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    .line 164
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    invoke-virtual {v4, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 165
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    invoke-virtual {v4, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 167
    const-string/jumbo v4, "button_text_autoip_current_areacode"

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/AutoIpSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/EditTextPreference;

    iput-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    .line 168
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    invoke-virtual {v4, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 169
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    invoke-virtual {v4, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 171
    const-string/jumbo v4, "button_autoip_add_zero_prefix"

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/AutoIpSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mAddZeroPrefix:Landroid/preference/CheckBoxPreference;

    .line 172
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget v5, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v4, v5}, Landroid/provider/MiuiSettings$Telephony;->isAutoAddZeroPrefix(Landroid/content/ContentResolver;I)Z

    move-result v2

    .line 173
    .local v2, "isAddZeroPrefixEnabled":Z
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mAddZeroPrefix:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 174
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mAddZeroPrefix:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 176
    const-string/jumbo v4, "button_autoip_support_local_numbers"

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/AutoIpSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSupportLocalNumbers:Landroid/preference/CheckBoxPreference;

    .line 177
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget v5, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v4, v5}, Landroid/provider/MiuiSettings$Telephony;->isAutoIpSupportLocalNum(Landroid/content/ContentResolver;I)Z

    move-result v3

    .line 178
    .local v3, "isSupportLocalNumbers":Z
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSupportLocalNumbers:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 179
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSupportLocalNumbers:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 181
    const-string/jumbo v4, "button_autoip_exceptions"

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/AutoIpSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    iput-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mAutoIpExceptions:Landroid/preference/Preference;

    .line 182
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 183
    .local v0, "bar":Lmiui/app/ActionBar;
    if-eqz v0, :cond_1

    .line 184
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lmiui/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 186
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 204
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    .line 205
    iget-object v0, p0, Lcom/android/phone/settings/AutoIpSetting;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/AutoIpSetting;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 206
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 210
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 211
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->finish()V

    .line 213
    const/4 v1, 0x1

    return v1

    .line 215
    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 69
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    if-ne p1, v3, :cond_2

    move-object v2, p2

    .line 70
    check-cast v2, Ljava/lang/String;

    .line 71
    .local v2, "ipPrefix":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 72
    iget v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {p0, v3}, Lmiui/telephony/PhoneNumberUtils;->getDefaultIpBySim(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 74
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v3, v2, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoIpPrefix(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 75
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v2, v4, v5

    const v5, 0x7f0b05b3

    invoke-virtual {p0, v5, v4}, Lcom/android/phone/settings/AutoIpSetting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 102
    .end local v2    # "ipPrefix":Ljava/lang/String;
    .end local p2    # "objValue":Ljava/lang/Object;
    :cond_1
    :goto_0
    return v6

    .line 76
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_2
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mAutoIpEnable:Landroid/preference/CheckBoxPreference;

    if-ne p1, v3, :cond_4

    .line 77
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 78
    .local v1, "flag":Z
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v3, v4}, Landroid/provider/MiuiSettings$Telephony;->getCurrentAeraCode(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 79
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mAutoIpEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 80
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    invoke-direct {p0, v3}, Lcom/android/phone/settings/AutoIpSetting;->simulatePreferenceClick(Landroid/preference/Preference;)V

    goto :goto_0

    .line 82
    :cond_3
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v3, v1, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoIpEnable(Landroid/content/ContentResolver;ZI)V

    .line 83
    invoke-direct {p0}, Lcom/android/phone/settings/AutoIpSetting;->closeAreaCodeDialog()V

    goto :goto_0

    .line 85
    .end local v1    # "flag":Z
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_4
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    if-ne p1, v3, :cond_6

    move-object v0, p2

    .line 86
    check-cast v0, Ljava/lang/String;

    .line 87
    .local v0, "currentAreaCode":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v3, v0, v4}, Landroid/provider/MiuiSettings$Telephony;->setCurrentAeraCode(Landroid/content/ContentResolver;Ljava/lang/String;I)V

    .line 88
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 89
    invoke-direct {p0}, Lcom/android/phone/settings/AutoIpSetting;->alertToInputCurrentAreaCode()V

    .line 90
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v3, v5, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoIpEnable(Landroid/content/ContentResolver;ZI)V

    goto :goto_0

    .line 92
    :cond_5
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    const v5, 0x7f0b05b4

    invoke-virtual {p0, v5, v4}, Lcom/android/phone/settings/AutoIpSetting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 93
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v3, v6, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoIpEnable(Landroid/content/ContentResolver;ZI)V

    goto :goto_0

    .line 95
    .end local v0    # "currentAreaCode":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mAddZeroPrefix:Landroid/preference/CheckBoxPreference;

    if-ne p1, v3, :cond_7

    .line 96
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 97
    .restart local v1    # "flag":Z
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v3, v1, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoAddZeroPrefixEnable(Landroid/content/ContentResolver;ZI)V

    goto/16 :goto_0

    .line 98
    .end local v1    # "flag":Z
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_7
    iget-object v3, p0, Lcom/android/phone/settings/AutoIpSetting;->mSupportLocalNumbers:Landroid/preference/CheckBoxPreference;

    if-ne p1, v3, :cond_1

    .line 99
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 100
    .restart local v1    # "flag":Z
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v3, v1, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoIpSupportLocalNumEnable(Landroid/content/ContentResolver;ZI)V

    goto/16 :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 389
    iget-object v1, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextIpPrefix:Landroid/preference/EditTextPreference;

    if-eq p1, v1, :cond_0

    iget-object v1, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    if-ne p1, v1, :cond_1

    .line 390
    :cond_0
    check-cast p1, Landroid/preference/EditTextPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 391
    .local v0, "et":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 393
    .end local v0    # "et":Landroid/widget/EditText;
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v5, 0x0

    .line 107
    invoke-super {p0, p1, p2}, Lmiui/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v3

    .line 108
    .local v3, "preferenceClick":Z
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    if-ne p2, v4, :cond_1

    .line 109
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    .line 110
    .local v2, "popupDialog":Landroid/app/Dialog;
    instance-of v4, v2, Landroid/app/AlertDialog;

    if-eqz v4, :cond_0

    move-object v0, v2

    .line 111
    check-cast v0, Landroid/app/AlertDialog;

    .line 112
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 113
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 114
    const/4 v4, -0x2

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v4

    new-instance v5, Lcom/android/phone/settings/AutoIpSetting$2;

    invoke-direct {v5, p0, v0}, Lcom/android/phone/settings/AutoIpSetting$2;-><init>(Lcom/android/phone/settings/AutoIpSetting;Landroid/app/AlertDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    .end local v2    # "popupDialog":Landroid/app/Dialog;
    :cond_0
    :goto_0
    return v3

    .line 126
    :cond_1
    iget-object v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mAutoIpExceptions:Landroid/preference/Preference;

    if-ne p2, v4, :cond_0

    .line 127
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.MAIN"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 128
    .local v1, "intent":Landroid/content/Intent;
    const-class v4, Lcom/android/phone/settings/AutoIpExceptionSetting;

    invoke-virtual {v1, p0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 129
    iget v4, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v1, v4}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    .line 130
    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AutoIpSetting;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 190
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 191
    invoke-direct {p0}, Lcom/android/phone/settings/AutoIpSetting;->updateTextIpPrefix()V

    .line 192
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoIpSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {v1, v2}, Landroid/provider/MiuiSettings$Telephony;->getCurrentAeraCode(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, "currentAreaCode":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    const v2, 0x7f0b05b8

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setSummary(I)V

    .line 199
    :goto_0
    iget-object v1, p0, Lcom/android/phone/settings/AutoIpSetting;->mAutoIpExceptions:Landroid/preference/Preference;

    iget v2, p0, Lcom/android/phone/settings/AutoIpSetting;->mSlotId:I

    invoke-static {p0, v2}, Lcom/android/phone/settings/AutoIpExceptionSetting;->getSummary(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 200
    return-void

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const v3, 0x7f0b05b4

    invoke-virtual {p0, v3, v2}, Lcom/android/phone/settings/AutoIpSetting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v1, p0, Lcom/android/phone/settings/AutoIpSetting;->mTextCurrentAreaCode:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method
