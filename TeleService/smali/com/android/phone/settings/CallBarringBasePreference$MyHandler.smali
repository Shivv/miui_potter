.class Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;
.super Landroid/os/Handler;
.source "CallBarringBasePreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/CallBarringBasePreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/CallBarringBasePreference;


# direct methods
.method private constructor <init>(Lcom/android/phone/settings/CallBarringBasePreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/CallBarringBasePreference;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/settings/CallBarringBasePreference;Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/CallBarringBasePreference;
    .param p2, "-this1"    # Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;-><init>(Lcom/android/phone/settings/CallBarringBasePreference;)V

    return-void
.end method

.method private handleGetCallBarringResponse(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 278
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 279
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v7, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v7, :cond_4

    .line 280
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7, v10}, Lcom/android/phone/settings/CallBarringBasePreference;->-set0(Lcom/android/phone/settings/CallBarringBasePreference;Z)Z

    .line 281
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get0(Lcom/android/phone/settings/CallBarringBasePreference;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 282
    invoke-static {}, Lcom/android/phone/settings/CallBarringBasePreference;->-get1()Ljava/lang/String;

    move-result-object v7

    .line 283
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "handleGetCallBarringResponse: ar.exception="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 282
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_0
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v1, Lcom/android/internal/telephony/CommandException;

    .line 286
    .local v1, "ce":Lcom/android/internal/telephony/CommandException;
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v7

    sget-object v8, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v7, v8, :cond_2

    .line 287
    const/16 v3, 0x2bc

    .line 293
    .local v3, "errorid":I
    :goto_0
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get2(Lcom/android/phone/settings/CallBarringBasePreference;)Lcom/android/phone/settings/CallBarringInterface;

    move-result-object v7

    invoke-interface {v7, v3}, Lcom/android/phone/settings/CallBarringInterface;->setErrorState(I)V

    .line 296
    :try_start_0
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get7(Lcom/android/phone/settings/CallBarringBasePreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v7

    iget-object v8, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-interface {v7, v8, v3}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onError(Landroid/preference/Preference;I)V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    .end local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    .end local v3    # "errorid":I
    :cond_1
    :goto_1
    iget v7, p1, Landroid/os/Message;->arg2:I

    if-nez v7, :cond_9

    .line 342
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get7(Lcom/android/phone/settings/CallBarringBasePreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v7

    iget-object v8, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-interface {v7, v8, v11}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    .line 346
    :goto_2
    return-void

    .line 288
    .restart local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    :cond_2
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v7

    sget-object v8, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v7, v8, :cond_3

    .line 289
    const/16 v3, 0x320

    .restart local v3    # "errorid":I
    goto :goto_0

    .line 291
    .end local v3    # "errorid":I
    :cond_3
    const/16 v3, 0x12c

    .restart local v3    # "errorid":I
    goto :goto_0

    .line 297
    :catch_0
    move-exception v2

    .line 298
    .local v2, "e":Landroid/view/WindowManager$BadTokenException;
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get0(Lcom/android/phone/settings/CallBarringBasePreference;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 299
    invoke-static {}, Lcom/android/phone/settings/CallBarringBasePreference;->-get1()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "BadTokenException"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 304
    .end local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    .end local v2    # "e":Landroid/view/WindowManager$BadTokenException;
    .end local v3    # "errorid":I
    :cond_4
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get0(Lcom/android/phone/settings/CallBarringBasePreference;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 305
    invoke-static {}, Lcom/android/phone/settings/CallBarringBasePreference;->-get1()Ljava/lang/String;

    move-result-object v7

    .line 306
    const-string/jumbo v8, "handleGetCallBarringResponse is called without exception"

    .line 305
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_5
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v7, v11}, Lcom/android/phone/settings/CallBarringBasePreference;->setEnabled(Z)V

    .line 309
    iget-object v4, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v4, [I

    .line 310
    .local v4, "ints":[I
    if-eqz v4, :cond_8

    array-length v7, v4

    if-lez v7, :cond_8

    .line 311
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7, v11}, Lcom/android/phone/settings/CallBarringBasePreference;->-set0(Lcom/android/phone/settings/CallBarringBasePreference;Z)Z

    .line 312
    aget v6, v4, v10

    .line 314
    .local v6, "value":I
    invoke-static {}, Lcom/android/phone/settings/CallBarringBasePreference;->-get1()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Current value = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "  Current serviceClass = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 315
    iget-object v9, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v9}, Lcom/android/phone/settings/CallBarringBasePreference;->-get6(Lcom/android/phone/settings/CallBarringBasePreference;)I

    move-result v9

    .line 314
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get6(Lcom/android/phone/settings/CallBarringBasePreference;)I

    move-result v7

    and-int/2addr v6, v7

    .line 318
    invoke-static {}, Lcom/android/phone/settings/CallBarringBasePreference;->-get1()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "After value & mServiceClass = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    const/4 v5, 0x0

    .line 320
    .local v5, "summary":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get0(Lcom/android/phone/settings/CallBarringBasePreference;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 321
    invoke-static {}, Lcom/android/phone/settings/CallBarringBasePreference;->-get1()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Value is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_6
    if-nez v6, :cond_7

    .line 324
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get3(Lcom/android/phone/settings/CallBarringBasePreference;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b060b

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 325
    .local v5, "summary":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v7, v10}, Lcom/android/phone/settings/CallBarringBasePreference;->setChecked(Z)V

    .line 331
    :goto_3
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v7, v5}, Lcom/android/phone/settings/CallBarringBasePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 327
    .local v5, "summary":Ljava/lang/String;
    :cond_7
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get3(Lcom/android/phone/settings/CallBarringBasePreference;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0b060c

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 328
    .local v5, "summary":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v7, v11}, Lcom/android/phone/settings/CallBarringBasePreference;->setChecked(Z)V

    goto :goto_3

    .line 333
    .end local v5    # "summary":Ljava/lang/String;
    .end local v6    # "value":I
    :cond_8
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7, v10}, Lcom/android/phone/settings/CallBarringBasePreference;->-set0(Lcom/android/phone/settings/CallBarringBasePreference;Z)Z

    .line 334
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get0(Lcom/android/phone/settings/CallBarringBasePreference;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 335
    invoke-static {}, Lcom/android/phone/settings/CallBarringBasePreference;->-get1()Ljava/lang/String;

    move-result-object v7

    .line 336
    const-string/jumbo v8, "handleGetCallBarringResponse ar.result get error"

    .line 335
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 344
    .end local v4    # "ints":[I
    :cond_9
    iget-object v7, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v7}, Lcom/android/phone/settings/CallBarringBasePreference;->-get7(Lcom/android/phone/settings/CallBarringBasePreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v7

    iget-object v8, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-interface {v7, v8, v10}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    goto/16 :goto_2
.end method

.method private handleSetCallBarringResponse(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    .line 249
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 250
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v3, :cond_3

    .line 251
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringBasePreference;->-get0(Lcom/android/phone/settings/CallBarringBasePreference;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 252
    invoke-static {}, Lcom/android/phone/settings/CallBarringBasePreference;->-get1()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "handleSetCallBarringResponse: ar.exception="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 253
    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 252
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_0
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v1, Lcom/android/internal/telephony/CommandException;

    .line 256
    .local v1, "ce":Lcom/android/internal/telephony/CommandException;
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v3, v4, :cond_1

    .line 257
    const/16 v2, 0x2bc

    .line 263
    .local v2, "errorid":I
    :goto_0
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringBasePreference;->-get2(Lcom/android/phone/settings/CallBarringBasePreference;)Lcom/android/phone/settings/CallBarringInterface;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/android/phone/settings/CallBarringInterface;->setErrorState(I)V

    .line 264
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringBasePreference;->-get7(Lcom/android/phone/settings/CallBarringBasePreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-interface {v3, v4, v6}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    .line 265
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringBasePreference;->-get7(Lcom/android/phone/settings/CallBarringBasePreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-interface {v3, v4, v2}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onError(Landroid/preference/Preference;I)V

    .line 274
    .end local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    .end local v2    # "errorid":I
    :goto_1
    return-void

    .line 258
    .restart local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    :cond_1
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v3, v4, :cond_2

    .line 259
    const/16 v2, 0x320

    .restart local v2    # "errorid":I
    goto :goto_0

    .line 261
    .end local v2    # "errorid":I
    :cond_2
    const/16 v2, 0x12c

    .restart local v2    # "errorid":I
    goto :goto_0

    .line 268
    .end local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    .end local v2    # "errorid":I
    :cond_3
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringBasePreference;->-get0(Lcom/android/phone/settings/CallBarringBasePreference;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 269
    invoke-static {}, Lcom/android/phone/settings/CallBarringBasePreference;->-get1()Ljava/lang/String;

    move-result-object v3

    .line 270
    const-string/jumbo v4, "handleSetCallBarringResponse is called without exception"

    .line 269
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_4
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    iget-object v4, p0, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->this$0:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-static {v4}, Lcom/android/phone/settings/CallBarringBasePreference;->-get5(Lcom/android/phone/settings/CallBarringBasePreference;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-static {v3, v4, v5, v6}, Lcom/android/phone/settings/CallBarringBasePreference;->-wrap1(Lcom/android/phone/settings/CallBarringBasePreference;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 235
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 245
    :goto_0
    return-void

    .line 237
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->handleGetCallBarringResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 240
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallBarringBasePreference$MyHandler;->handleSetCallBarringResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 235
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
