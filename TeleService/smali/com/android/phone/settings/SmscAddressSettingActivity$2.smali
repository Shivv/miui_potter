.class Lcom/android/phone/settings/SmscAddressSettingActivity$2;
.super Landroid/os/Handler;
.source "SmscAddressSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/SmscAddressSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/SmscAddressSettingActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/SmscAddressSettingActivity;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    .line 80
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v7, 0x7f0b0628

    const v6, 0x7f0b02f6

    const v5, 0x1010355

    const/4 v4, 0x0

    .line 82
    iget-object v2, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v2}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get0(Lcom/android/phone/settings/SmscAddressSettingActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 83
    return-void

    .line 91
    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 169
    :cond_1
    :goto_0
    return-void

    .line 93
    :pswitch_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 96
    const v3, 0x7f0b0625

    .line 93
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 98
    .local v1, "dlg":Landroid/app/AlertDialog;
    new-instance v2, Lcom/android/phone/settings/SmscAddressSettingActivity$2$1;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/SmscAddressSettingActivity$2$1;-><init>(Lcom/android/phone/settings/SmscAddressSettingActivity$2;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 104
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 107
    .end local v1    # "dlg":Landroid/app/AlertDialog;
    :pswitch_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 110
    const v3, 0x7f0b0627

    .line 107
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 112
    .restart local v1    # "dlg":Landroid/app/AlertDialog;
    new-instance v2, Lcom/android/phone/settings/SmscAddressSettingActivity$2$2;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/SmscAddressSettingActivity$2$2;-><init>(Lcom/android/phone/settings/SmscAddressSettingActivity$2;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 118
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 121
    .end local v1    # "dlg":Landroid/app/AlertDialog;
    :pswitch_2
    const/16 v2, 0x3eb

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->removeMessages(I)V

    .line 122
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 123
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_2

    .line 124
    new-instance v2, Landroid/app/AlertDialog$Builder;

    .line 125
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    .line 124
    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 128
    const v3, 0x7f0b0624

    .line 124
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 131
    .restart local v1    # "dlg":Landroid/app/AlertDialog;
    new-instance v2, Lcom/android/phone/settings/SmscAddressSettingActivity$2$3;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/SmscAddressSettingActivity$2$3;-><init>(Lcom/android/phone/settings/SmscAddressSettingActivity$2;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 137
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 139
    .end local v1    # "dlg":Landroid/app/AlertDialog;
    :cond_2
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    iget-object v4, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-wrap0(Lcom/android/phone/settings/SmscAddressSettingActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-set0(Lcom/android/phone/settings/SmscAddressSettingActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 140
    iget-object v2, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v2}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get1(Lcom/android/phone/settings/SmscAddressSettingActivity;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v3}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get4(Lcom/android/phone/settings/SmscAddressSettingActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v2, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v2}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get4(Lcom/android/phone/settings/SmscAddressSettingActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 142
    iget-object v2, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v2}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get1(Lcom/android/phone/settings/SmscAddressSettingActivity;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-static {v3}, Lcom/android/phone/settings/SmscAddressSettingActivity;->-get4(Lcom/android/phone/settings/SmscAddressSettingActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 147
    .end local v0    # "ar":Landroid/os/AsyncResult;
    :pswitch_3
    const/16 v2, 0x3ec

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->removeMessages(I)V

    .line 148
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 149
    .restart local v0    # "ar":Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_3

    .line 150
    new-instance v2, Landroid/app/AlertDialog$Builder;

    .line 151
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    .line 150
    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 154
    const v3, 0x7f0b0626

    .line 150
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 157
    .restart local v1    # "dlg":Landroid/app/AlertDialog;
    new-instance v2, Lcom/android/phone/settings/SmscAddressSettingActivity$2$4;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/SmscAddressSettingActivity$2$4;-><init>(Lcom/android/phone/settings/SmscAddressSettingActivity$2;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 163
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 165
    .end local v1    # "dlg":Landroid/app/AlertDialog;
    :cond_3
    iget-object v2, p0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;->this$0:Lcom/android/phone/settings/SmscAddressSettingActivity;

    invoke-virtual {v2}, Lcom/android/phone/settings/SmscAddressSettingActivity;->finish()V

    goto/16 :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
