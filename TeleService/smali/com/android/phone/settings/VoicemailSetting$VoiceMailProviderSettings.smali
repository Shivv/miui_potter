.class Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
.super Ljava/lang/Object;
.source "VoicemailSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/VoicemailSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VoiceMailProviderSettings"
.end annotation


# instance fields
.field public forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

.field final synthetic this$0:Lcom/android/phone/settings/VoicemailSetting;

.field public voicemailNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/phone/settings/VoicemailSetting;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1, "this$0"    # Lcom/android/phone/settings/VoicemailSetting;
    .param p2, "voicemailNumber"    # Ljava/lang/String;
    .param p3, "forwardingNumber"    # Ljava/lang/String;
    .param p4, "timeSeconds"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 208
    iput-object p1, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    iput-object p2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    .line 214
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 215
    :cond_0
    sget-object v2, Lcom/android/phone/settings/VoicemailSetting;->FWD_SETTINGS_DONT_TOUCH:[Lcom/android/internal/telephony/CallForwardInfo;

    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 229
    :cond_1
    return-void

    .line 217
    :cond_2
    sget-object v2, Lcom/android/phone/settings/VoicemailSetting;->FORWARDING_SETTINGS_REASONS:[I

    array-length v2, v2

    new-array v2, v2, [Lcom/android/internal/telephony/CallForwardInfo;

    iput-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 218
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 219
    new-instance v0, Lcom/android/internal/telephony/CallForwardInfo;

    invoke-direct {v0}, Lcom/android/internal/telephony/CallForwardInfo;-><init>()V

    .line 220
    .local v0, "fi":Lcom/android/internal/telephony/CallForwardInfo;
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    aput-object v0, v2, v1

    .line 221
    sget-object v2, Lcom/android/phone/settings/VoicemailSetting;->FORWARDING_SETTINGS_REASONS:[I

    aget v2, v2, v1

    iput v2, v0, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    .line 222
    iget v2, v0, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    iput v2, v0, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    .line 223
    iput v4, v0, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    .line 224
    const/16 v2, 0x91

    iput v2, v0, Lcom/android/internal/telephony/CallForwardInfo;->toa:I

    .line 225
    iput-object p3, v0, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    .line 226
    iput p4, v0, Lcom/android/internal/telephony/CallForwardInfo;->timeSeconds:I

    .line 218
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    move v2, v4

    .line 222
    goto :goto_1
.end method

.method public constructor <init>(Lcom/android/phone/settings/VoicemailSetting;Ljava/lang/String;[Lcom/android/internal/telephony/CallForwardInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/VoicemailSetting;
    .param p2, "voicemailNumber"    # Ljava/lang/String;
    .param p3, "infos"    # [Lcom/android/internal/telephony/CallForwardInfo;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->this$0:Lcom/android/phone/settings/VoicemailSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput-object p2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    .line 233
    iput-object p3, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 234
    return-void
.end method

.method private forwardingSettingsEqual([Lcom/android/internal/telephony/CallForwardInfo;[Lcom/android/internal/telephony/CallForwardInfo;)Z
    .locals 7
    .param p1, "infos1"    # [Lcom/android/internal/telephony/CallForwardInfo;
    .param p2, "infos2"    # [Lcom/android/internal/telephony/CallForwardInfo;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 253
    if-ne p1, p2, :cond_0

    return v6

    .line 254
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    return v5

    .line 255
    :cond_2
    array-length v3, p1

    array-length v4, p2

    if-eq v3, v4, :cond_3

    return v5

    .line 256
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_6

    .line 257
    aget-object v1, p1, v0

    .line 258
    .local v1, "i1":Lcom/android/internal/telephony/CallForwardInfo;
    aget-object v2, p2, v0

    .line 259
    .local v2, "i2":Lcom/android/internal/telephony/CallForwardInfo;
    iget v3, v1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    iget v4, v2, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    if-ne v3, v4, :cond_4

    .line 260
    iget v3, v1, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    iget v4, v2, Lcom/android/internal/telephony/CallForwardInfo;->reason:I

    if-eq v3, v4, :cond_5

    .line 265
    :cond_4
    return v5

    .line 261
    :cond_5
    iget v3, v1, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    iget v4, v2, Lcom/android/internal/telephony/CallForwardInfo;->serviceClass:I

    if-ne v3, v4, :cond_4

    .line 262
    iget v3, v1, Lcom/android/internal/telephony/CallForwardInfo;->toa:I

    iget v4, v2, Lcom/android/internal/telephony/CallForwardInfo;->toa:I

    if-ne v3, v4, :cond_4

    .line 263
    iget-object v3, v1, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    if-ne v3, v4, :cond_4

    .line 264
    iget v3, v1, Lcom/android/internal/telephony/CallForwardInfo;->timeSeconds:I

    iget v4, v2, Lcom/android/internal/telephony/CallForwardInfo;->timeSeconds:I

    if-ne v3, v4, :cond_4

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 268
    .end local v1    # "i1":Lcom/android/internal/telephony/CallForwardInfo;
    .end local v2    # "i2":Lcom/android/internal/telephony/CallForwardInfo;
    :cond_6
    return v6
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 238
    if-nez p1, :cond_0

    return v1

    .line 239
    :cond_0
    instance-of v2, p1, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    if-nez v2, :cond_1

    return v1

    :cond_1
    move-object v0, p1

    .line 240
    check-cast v0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;

    .line 242
    .local v0, "v":Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 243
    iget-object v2, v0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 247
    :goto_0
    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 248
    iget-object v2, v0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    .line 247
    invoke-direct {p0, v1, v2}, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettingsEqual([Lcom/android/internal/telephony/CallForwardInfo;[Lcom/android/internal/telephony/CallForwardInfo;)Z

    move-result v1

    .line 242
    :cond_2
    return v1

    .line 244
    :cond_3
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 245
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    iget-object v3, v0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 242
    if-eqz v2, :cond_2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->voicemailNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 274
    iget-object v2, p0, Lcom/android/phone/settings/VoicemailSetting$VoiceMailProviderSettings;->forwardingSettings:[Lcom/android/internal/telephony/CallForwardInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 273
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 274
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method
