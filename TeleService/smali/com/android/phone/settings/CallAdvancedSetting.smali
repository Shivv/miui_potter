.class public Lcom/android/phone/settings/CallAdvancedSetting;
.super Lmiui/preference/PreferenceActivity;
.source "CallAdvancedSetting.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/CallAdvancedSetting$1;,
        Lcom/android/phone/settings/CallAdvancedSetting$2;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private DBG:Z

.field private mAutoIP:Lmiui/preference/ValuePreference;

.field private mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

.field private mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

.field private mButtonDTMF:Landroid/preference/ListPreference;

.field private mButtonHAC:Landroid/preference/CheckBoxPreference;

.field private mButtonTTY:Landroid/preference/ListPreference;

.field private mCallBackground:Lcom/android/phone/settings/ValueListPreference;

.field private mCallBarring:Landroid/preference/PreferenceScreen;

.field private mCallWaitingTone:Lcom/android/phone/settings/ValueListPreference;

.field private mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

.field private mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

.field private mDialPadTouchTone:Lmiui/preference/ValuePreference;

.field private mFirstInit:Z

.field mHandler:Landroid/os/Handler;

.field private mIccCardCount:I

.field private mIsReceiverRegistered:Z

.field private mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRespondViaSms:Lmiui/preference/ValuePreference;

.field private mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/CallAdvancedSetting;)Lcom/android/phone/settings/CheckBoxTitleIconPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallAdvancedSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/CallAdvancedSetting;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallAdvancedSetting;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mFirstInit:Z

    return v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/CallAdvancedSetting;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallAdvancedSetting;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIccCardCount:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/CallAdvancedSetting;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallAdvancedSetting;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mFirstInit:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/phone/settings/CallAdvancedSetting;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallAdvancedSetting;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIccCardCount:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/CallAdvancedSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallAdvancedSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->initScreen()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/CallAdvancedSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallAdvancedSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->updateScreen()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/android/phone/settings/CallAdvancedSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/CallAdvancedSetting;->LOG_TAG:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 47
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v3, 0x2

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->DBG:Z

    .line 109
    iput-boolean v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mFirstInit:Z

    .line 110
    iput-boolean v2, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIsReceiverRegistered:Z

    .line 115
    new-instance v0, Lcom/android/phone/settings/CallAdvancedSetting$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/CallAdvancedSetting$1;-><init>(Lcom/android/phone/settings/CallAdvancedSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 147
    new-instance v0, Lcom/android/phone/settings/CallAdvancedSetting$2;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/CallAdvancedSetting$2;-><init>(Lcom/android/phone/settings/CallAdvancedSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mHandler:Landroid/os/Handler;

    .line 43
    return-void

    :cond_0
    move v0, v2

    .line 47
    goto :goto_0
.end method

.method private handleTTYChange(Landroid/preference/Preference;Ljava/lang/Object;)V
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    .line 685
    check-cast p2, Ljava/lang/String;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 687
    .local v0, "buttonTtyMode":I
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 688
    const-string/jumbo v3, "preferred_tty_mode"

    const/4 v4, 0x0

    .line 686
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 689
    .local v1, "settingsTtyMode":I
    iget-boolean v2, p0, Lcom/android/phone/settings/CallAdvancedSetting;->DBG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handleTTYChange: requesting set TTY mode enable (TTY) to"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 690
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 689
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/settings/CallAdvancedSetting;->log(Ljava/lang/String;)V

    .line 692
    :cond_0
    if-eq v0, v1, :cond_1

    .line 693
    packed-switch v0, :pswitch_data_0

    .line 702
    const/4 v0, 0x0

    .line 705
    :goto_0
    iget-object v2, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 706
    invoke-direct {p0, v0}, Lcom/android/phone/settings/CallAdvancedSetting;->updatePreferredTtyModeSummary(I)V

    .line 708
    :cond_1
    return-void

    .line 698
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 699
    const-string/jumbo v3, "preferred_tty_mode"

    .line 698
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 693
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private initForPhoneType()V
    .locals 4

    .prologue
    .line 614
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 615
    .local v1, "prefScreen":Landroid/preference/PreferenceScreen;
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getCdmaPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 617
    .local v0, "hasAnyCdmaPhone":Z
    :goto_0
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/telephony/TelephonyManager;->getIccCardCount()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_2

    if-eqz v0, :cond_2

    .line 619
    const-string/jumbo v2, "button_caller_id"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 620
    const-string/jumbo v2, "button_auto_redial"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 627
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->initSimIcon()V

    .line 628
    return-void

    .line 615
    .end local v0    # "hasAnyCdmaPhone":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "hasAnyCdmaPhone":Z
    goto :goto_0

    .line 621
    :cond_2
    if-nez v0, :cond_0

    .line 623
    const-string/jumbo v2, "button_display_precise_call_state"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 624
    const-string/jumbo v2, "button_voice_privacy"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    goto :goto_1
.end method

.method private initScreen()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 208
    const v1, 0x7f060004

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->addPreferencesFromResource(I)V

    .line 210
    const-string/jumbo v1, "pref_key_dial_pad_touch_tone"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lmiui/preference/ValuePreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mDialPadTouchTone:Lmiui/preference/ValuePreference;

    .line 211
    const-string/jumbo v1, "button_t9_index_method_settings"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/ValueListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    .line 212
    const-string/jumbo v1, "button_auto_redial"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    .line 214
    const-string/jumbo v1, "button_call_background"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/ValueListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBackground:Lcom/android/phone/settings/ValueListPreference;

    .line 215
    const-string/jumbo v1, "pref_key_auto_ip"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lmiui/preference/ValuePreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoIP:Lmiui/preference/ValuePreference;

    .line 216
    const-string/jumbo v1, "button_missed_call_notify_times"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/ValueListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    .line 217
    const-string/jumbo v1, "button_connect_disconnect_vibrate"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/ValueListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

    .line 219
    const-string/jumbo v1, "button_display_precise_call_state"

    .line 218
    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    .line 220
    const-string/jumbo v1, "button_call_waiting_tone"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/settings/ValueListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallWaitingTone:Lcom/android/phone/settings/ValueListPreference;

    .line 221
    const-string/jumbo v1, "button_respond_via_sms_key"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lmiui/preference/ValuePreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mRespondViaSms:Lmiui/preference/ValuePreference;

    .line 223
    const-string/jumbo v1, "button_call_barring"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBarring:Landroid/preference/PreferenceScreen;

    .line 224
    const-string/jumbo v1, "button_auto_retry"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

    .line 225
    const-string/jumbo v1, "button_dtmf_settings"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonDTMF:Landroid/preference/ListPreference;

    .line 226
    const-string/jumbo v1, "button_hac"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonHAC:Landroid/preference/CheckBoxPreference;

    .line 227
    const-string/jumbo v1, "button_tty_mode"

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    .line 229
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 230
    .local v0, "prefScreen":Landroid/preference/PreferenceScreen;
    invoke-direct {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->initForPhoneType()V

    .line 232
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mDialPadTouchTone:Lmiui/preference/ValuePreference;

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mDialPadTouchTone:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, p0}, Lmiui/preference/ValuePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 234
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mDialPadTouchTone:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    if-eqz v1, :cond_1

    .line 238
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    invoke-virtual {v1, p0}, Lcom/android/phone/settings/CheckBoxTitleIconPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 241
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoIP:Lmiui/preference/ValuePreference;

    if-eqz v1, :cond_2

    .line 242
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoIP:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    .line 243
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_2

    .line 244
    const-string/jumbo v1, "pref_key_auto_ip"

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 248
    :cond_2
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mRespondViaSms:Lmiui/preference/ValuePreference;

    if-eqz v1, :cond_3

    .line 249
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mRespondViaSms:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    .line 252
    :cond_3
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    if-eqz v1, :cond_4

    .line 253
    invoke-direct {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->isAllowShowT9Index()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 254
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {v1, p0}, Lcom/android/phone/settings/ValueListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 260
    :cond_4
    :goto_0
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    if-eqz v1, :cond_5

    .line 261
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {v1, p0}, Lcom/android/phone/settings/ValueListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 264
    :cond_5
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    if-eqz v1, :cond_6

    .line 265
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    invoke-virtual {v1, p0}, Lcom/android/phone/settings/CheckBoxTitleIconPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 268
    :cond_6
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBarring:Landroid/preference/PreferenceScreen;

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e002c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_7

    .line 269
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/lit8 v1, v1, 0x1

    .line 268
    if-eqz v1, :cond_7

    .line 270
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBarring:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 271
    iput-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBarring:Landroid/preference/PreferenceScreen;

    .line 274
    :cond_7
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_8

    .line 275
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 276
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 283
    :cond_8
    :goto_1
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonDTMF:Landroid/preference/ListPreference;

    if-eqz v1, :cond_9

    .line 284
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 285
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonDTMF:Landroid/preference/ListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 292
    :cond_9
    :goto_2
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonHAC:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_a

    .line 293
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 294
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonHAC:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 301
    :cond_a
    :goto_3
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    if-eqz v1, :cond_b

    .line 302
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 303
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 310
    :cond_b
    :goto_4
    sget-boolean v1, Lmiui/os/Build;->IS_CTA_BUILD:Z

    if-eqz v1, :cond_c

    .line 311
    const-string/jumbo v1, "button_auto_redial"

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 312
    const-string/jumbo v1, "button_sip_settings"

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 315
    :cond_c
    invoke-static {p0}, Landroid/net/sip/SipManager;->isApiSupported(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string/jumbo v1, "ZA"

    invoke-static {v1}, Lmiui/os/Build;->checkRegion(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 316
    :cond_d
    const-string/jumbo v1, "button_sip_settings"

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 318
    :cond_e
    return-void

    .line 256
    :cond_f
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/ValueListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 278
    :cond_10
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 279
    iput-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

    goto :goto_1

    .line 287
    :cond_11
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonDTMF:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 288
    iput-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonDTMF:Landroid/preference/ListPreference;

    goto :goto_2

    .line 296
    :cond_12
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonHAC:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 297
    iput-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonHAC:Landroid/preference/CheckBoxPreference;

    goto :goto_3

    .line 305
    :cond_13
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 306
    iput-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    goto :goto_4
.end method

.method private initSimIcon()V
    .locals 8

    .prologue
    .line 634
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getCdmaPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 636
    .local v1, "cdmaPhone":Lcom/android/internal/telephony/Phone;
    const-string/jumbo v6, "button_voice_privacy"

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Lcom/android/phone/settings/CdmaVoicePrivacyCheckBoxPreference;

    .line 638
    .local v5, "voicePrivacy":Lcom/android/phone/settings/CdmaVoicePrivacyCheckBoxPreference;
    const-string/jumbo v6, "button_caller_id"

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/CallAdvancedSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/phone/settings/TitleIconPreference;

    .line 640
    .local v0, "callerId":Lcom/android/phone/settings/TitleIconPreference;
    if-eqz v5, :cond_0

    if-eqz v1, :cond_0

    .line 641
    invoke-virtual {v5, v1}, Lcom/android/phone/settings/CdmaVoicePrivacyCheckBoxPreference;->init(Lcom/android/internal/telephony/Phone;)V

    .line 644
    :cond_0
    const/4 v2, 0x0

    .line 645
    .local v2, "cdmaSimIconResId":I
    iget v6, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIccCardCount:I

    const/4 v7, 0x1

    if-le v6, v7, :cond_1

    if-eqz v1, :cond_1

    .line 646
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v6

    if-nez v6, :cond_7

    .line 647
    const v2, 0x7f0200a3

    .line 650
    :cond_1
    :goto_0
    if-eqz v5, :cond_2

    .line 651
    invoke-virtual {v5, v2}, Lcom/android/phone/settings/CdmaVoicePrivacyCheckBoxPreference;->setTitleIconResource(I)V

    .line 654
    :cond_2
    iget-object v6, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    if-eqz v6, :cond_3

    .line 655
    iget-object v6, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    invoke-virtual {v6, v2}, Lcom/android/phone/settings/CheckBoxTitleIconPreference;->setTitleIconResource(I)V

    .line 658
    :cond_3
    const/4 v3, 0x0

    .line 659
    .local v3, "gsmSimIconResId":I
    if-eqz v2, :cond_4

    .line 660
    const v6, 0x7f0200a3

    if-ne v2, v6, :cond_8

    .line 661
    const v3, 0x7f0200a4

    .line 663
    :cond_4
    :goto_1
    iget-object v6, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    if-eqz v6, :cond_5

    .line 664
    iget-object v6, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    invoke-virtual {v6, v3}, Lcom/android/phone/settings/CheckBoxTitleIconPreference;->setTitleIconResource(I)V

    .line 666
    :cond_5
    if-eqz v0, :cond_6

    .line 667
    invoke-virtual {v0, v3}, Lcom/android/phone/settings/TitleIconPreference;->setTitleIconResource(I)V

    .line 668
    if-eqz v1, :cond_6

    .line 669
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v6

    if-nez v6, :cond_9

    .line 670
    const/4 v4, 0x1

    .line 671
    .local v4, "gsmSlotId":I
    :goto_2
    invoke-virtual {v0}, Lcom/android/phone/settings/TitleIconPreference;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-static {v6, v4}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    .line 674
    .end local v4    # "gsmSlotId":I
    :cond_6
    return-void

    .line 647
    .end local v3    # "gsmSimIconResId":I
    :cond_7
    const v2, 0x7f0200a4

    goto :goto_0

    .line 661
    .restart local v3    # "gsmSimIconResId":I
    :cond_8
    const v3, 0x7f0200a3

    goto :goto_1

    .line 670
    :cond_9
    const/4 v4, 0x0

    .restart local v4    # "gsmSlotId":I
    goto :goto_2
.end method

.method private isAllowShowT9Index()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 321
    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "region":Ljava/lang/String;
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_1

    .line 323
    const-string/jumbo v1, "HK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "TW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 324
    const-string/jumbo v1, "ID"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 323
    :goto_0
    xor-int/lit8 v1, v1, 0x1

    .line 322
    if-eqz v1, :cond_1

    .line 325
    const/4 v1, 0x0

    return v1

    :cond_0
    move v1, v2

    .line 323
    goto :goto_0

    .line 327
    :cond_1
    return v2
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 726
    sget-object v0, Lcom/android/phone/settings/CallAdvancedSetting;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    return-void
.end method

.method private setEnabledForKey(ZLjava/lang/String;)V
    .locals 2
    .param p1, "enabled"    # Z
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 677
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 678
    .local v0, "pref":Landroid/preference/Preference;
    if-eqz v0, :cond_0

    .line 679
    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 681
    :cond_0
    return-void
.end method

.method private showRebuildT9IndexDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 600
    new-instance v0, Lcom/android/phone/settings/CallAdvancedSetting$4;

    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/phone/settings/CallAdvancedSetting$4;-><init>(Lcom/android/phone/settings/CallAdvancedSetting;Landroid/app/FragmentManager;)V

    .line 608
    const v1, 0x7f0b05c8

    .line 600
    invoke-virtual {v0, v1}, Lcom/android/phone/settings/CallAdvancedSetting$4;->setMessage(I)Lmiui/os/AsyncTaskWithProgress;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiui/os/AsyncTaskWithProgress;->setCancelable(Z)Lmiui/os/AsyncTaskWithProgress;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/os/AsyncTaskWithProgress;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 611
    return-void
.end method

.method private updatePreferredTtyModeSummary(I)V
    .locals 4
    .param p1, "TtyMode"    # I

    .prologue
    const/4 v3, 0x0

    .line 711
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070069

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 712
    .local v0, "txts":[Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 720
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 721
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    aget-object v2, v0, v3

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 723
    :goto_0
    return-void

    .line 717
    :pswitch_0
    iget-object v1, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    aget-object v2, v0, p1

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 712
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateScreen()V
    .locals 30

    .prologue
    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mDialPadTouchTone:Lmiui/preference/ValuePreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_0

    .line 334
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 335
    const-string/jumbo v28, "dtmf_tone"

    const/16 v29, 0x0

    .line 334
    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v27

    .line 335
    const/16 v28, 0x1

    .line 334
    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_10

    .line 336
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 337
    const-string/jumbo v28, "dial_pad_touch_tone"

    .line 338
    const/16 v29, -0x1

    .line 336
    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v24

    .line 339
    .local v24, "value":I
    const/16 v22, -0x1

    .line 340
    .local v22, "summaryId":I
    packed-switch v24, :pswitch_data_0

    .line 345
    const v22, 0x7f0b0640

    .line 348
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mDialPadTouchTone:Lmiui/preference/ValuePreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    .line 354
    .end local v22    # "summaryId":I
    .end local v24    # "value":I
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_1

    .line 355
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 356
    const-string/jumbo v28, "t9_indexing_key"

    .line 357
    invoke-static {}, Landroid/provider/MiuiSettings$System;->getT9IndexingKeyDefault()I

    move-result v29

    .line 355
    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v16

    .line 358
    .local v16, "method":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/ValueListPreference;->setValueIndex(I)V

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    .line 360
    const v29, 0x7f07006f

    .line 359
    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v28

    aget-object v28, v28, v16

    invoke-virtual/range {v27 .. v28}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 363
    .end local v16    # "method":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_2

    .line 365
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 364
    invoke-static/range {v27 .. v27}, Landroid/provider/MiuiSettings$Telephony;->getEnabledAutoRedial(Landroid/content/ContentResolver;)Z

    move-result v15

    .line 366
    .local v15, "isAutoRedial":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Lcom/android/phone/settings/CheckBoxTitleIconPreference;->setChecked(Z)V

    .line 369
    .end local v15    # "isAutoRedial":Z
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoIP:Lmiui/preference/ValuePreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_3

    .line 370
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoIP:Lmiui/preference/ValuePreference;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mIccCardCount:I

    move/from16 v27, v0

    if-lez v27, :cond_11

    const/16 v27, 0x1

    :goto_2
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setEnabled(Z)V

    .line 373
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_4

    .line 374
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Landroid/provider/MiuiSettings$Telephony;->getMissedCallNotifyTimes(Landroid/content/ContentResolver;)I

    move-result v23

    .line 375
    .local v23, "times":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/android/phone/settings/ValueListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v13

    .line 376
    .local v13, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    .line 377
    const v29, 0x7f070073

    .line 376
    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v28

    aget-object v28, v28, v13

    invoke-virtual/range {v27 .. v28}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Lcom/android/phone/settings/ValueListPreference;->setValueIndex(I)V

    .line 381
    .end local v13    # "index":I
    .end local v23    # "times":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/ValueListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 383
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    .line 384
    const v28, 0x7f070075

    .line 383
    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v25

    .line 385
    .local v25, "vibrateSummary":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    .line 386
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Landroid/provider/MiuiSettings$Telephony;->getVibrateKey(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v28

    .line 385
    invoke-virtual/range {v27 .. v28}, Lcom/android/phone/settings/ValueListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v13

    .line 387
    .restart local v13    # "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    aget-object v28, v25, v13

    invoke-virtual/range {v27 .. v28}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Lcom/android/phone/settings/ValueListPreference;->setValueIndex(I)V

    .line 391
    .end local v13    # "index":I
    .end local v25    # "vibrateSummary":[Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_6

    .line 392
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 393
    const-string/jumbo v28, "cdma_precise_answer_state"

    .line 394
    const/16 v29, 0x1

    .line 392
    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v18

    .line 395
    .local v18, "preciseCallState":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    move-object/from16 v28, v0

    .line 396
    const/16 v27, 0x1

    move/from16 v0, v18

    move/from16 v1, v27

    if-ne v0, v1, :cond_12

    const/16 v27, 0x1

    .line 395
    :goto_3
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/CheckBoxTitleIconPreference;->setChecked(Z)V

    .line 399
    .end local v18    # "preciseCallState":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBackground:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 400
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    .line 401
    const v28, 0x7f070071

    .line 400
    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 402
    .local v4, "callBackgroundSummary":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Landroid/provider/MiuiSettings$Telephony;->getCallBackgroundType(Landroid/content/ContentResolver;)I

    move-result v13

    .line 403
    .restart local v13    # "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBackground:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    aget-object v28, v4, v13

    invoke-virtual/range {v27 .. v28}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBackground:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Lcom/android/phone/settings/ValueListPreference;->setValueIndex(I)V

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBackground:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/ValueListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 408
    .end local v4    # "callBackgroundSummary":[Ljava/lang/String;
    .end local v13    # "index":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallWaitingTone:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_8

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallWaitingTone:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/ValueListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 410
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    .line 411
    const v28, 0x7f070077

    .line 410
    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 412
    .local v5, "callWaitToneSummary":[Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Landroid/provider/MiuiSettings$Telephony;->getCallWaitingTone(Landroid/content/Context;)I

    move-result v13

    .line 413
    .restart local v13    # "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallWaitingTone:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    aget-object v28, v5, v13

    invoke-virtual/range {v27 .. v28}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallWaitingTone:Lcom/android/phone/settings/ValueListPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Lcom/android/phone/settings/ValueListPreference;->setValueIndex(I)V

    .line 417
    .end local v5    # "callWaitToneSummary":[Ljava/lang/String;
    .end local v13    # "index":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mRespondViaSms:Lmiui/preference/ValuePreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_9

    .line 418
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Landroid/provider/MiuiSettings$Telephony;->isRejectViaSmsEnable(Landroid/content/ContentResolver;)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mRespondViaSms:Lmiui/preference/ValuePreference;

    move-object/from16 v27, v0

    const v28, 0x7f0b05ae

    invoke-virtual/range {v27 .. v28}, Lmiui/preference/ValuePreference;->setValue(I)V

    .line 425
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonDTMF:Landroid/preference/ListPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_a

    .line 426
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 427
    const-string/jumbo v28, "dtmf_tone_type"

    const/16 v29, 0x0

    .line 426
    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    .line 428
    .local v7, "dtmf":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonDTMF:Landroid/preference/ListPreference;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 430
    .end local v7    # "dtmf":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_b

    .line 431
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 432
    const-string/jumbo v28, "call_auto_retry"

    const/16 v29, 0x0

    .line 431
    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 433
    .local v3, "autoretry":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

    move-object/from16 v28, v0

    if-eqz v3, :cond_14

    const/16 v27, 0x1

    :goto_5
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 436
    .end local v3    # "autoretry":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonHAC:Landroid/preference/CheckBoxPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_c

    .line 437
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    const-string/jumbo v28, "hearing_aid"

    const/16 v29, 0x0

    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    .line 438
    .local v9, "hac":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonHAC:Landroid/preference/CheckBoxPreference;

    move-object/from16 v28, v0

    if-eqz v9, :cond_15

    const/16 v27, 0x1

    :goto_6
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 440
    .end local v9    # "hac":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    move-object/from16 v27, v0

    if-eqz v27, :cond_d

    .line 441
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 442
    const-string/jumbo v28, "preferred_tty_mode"

    .line 443
    const/16 v29, 0x0

    .line 441
    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v20

    .line 444
    .local v20, "settingsTtyMode":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    move-object/from16 v27, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 445
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->updatePreferredTtyModeSummary(I)V

    .line 448
    .end local v20    # "settingsTtyMode":I
    :cond_d
    invoke-static/range {p0 .. p0}, Lcom/android/services/telephony/SimpleFeatures;->isHideVoicemailSettings(Landroid/content/Context;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 449
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v19

    .line 450
    .local v19, "prefScreen":Landroid/preference/PreferenceScreen;
    const-string/jumbo v27, "button_voicemail"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 453
    .end local v19    # "prefScreen":Landroid/preference/PreferenceScreen;
    :cond_e
    invoke-static/range {p0 .. p0}, Lcom/android/services/telephony/SimpleFeatures;->isHidePhoneAccountSettings(Landroid/content/Context;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 454
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v19

    .line 455
    .restart local v19    # "prefScreen":Landroid/preference/PreferenceScreen;
    const-string/jumbo v27, "button_phone_account"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 459
    .end local v19    # "prefScreen":Landroid/preference/PreferenceScreen;
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 460
    const-string/jumbo v28, "airplane_mode_on"

    const/16 v29, 0x0

    .line 459
    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v27

    if-lez v27, :cond_16

    const/4 v14, 0x1

    .line 461
    .local v14, "isAirPlane":Z
    :goto_7
    const/4 v8, 0x0

    .line 462
    .local v8, "fdnDisabled":Z
    const/4 v10, 0x0

    .line 463
    .local v10, "hasICC":Z
    if-nez v14, :cond_19

    .line 464
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v21

    .line 465
    .local v21, "slotCount":I
    const/16 v27, 0x1

    const/16 v28, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-static {v0, v1, v2}, Lmiui/telephony/VirtualSimUtils;->getVirtualSimSlot(Landroid/content/Context;ZI)I

    move-result v26

    .line 466
    .local v26, "virtualSlot":I
    const/4 v12, 0x0

    .end local v8    # "fdnDisabled":Z
    .end local v10    # "hasICC":Z
    .local v12, "i":I
    :goto_8
    move/from16 v0, v21

    if-ge v12, v0, :cond_19

    .line 467
    invoke-static {v12}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v17

    .line 468
    .local v17, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lcom/android/internal/telephony/IccCard;->hasIccCard()Z

    move-result v27

    if-eqz v27, :cond_17

    move/from16 v0, v26

    if-eq v12, v0, :cond_17

    const/4 v11, 0x1

    .line 469
    .local v11, "hasIccCard":Z
    :goto_9
    if-eqz v11, :cond_18

    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lcom/android/internal/telephony/IccCard;->getIccFdnEnabled()Z

    move-result v27

    xor-int/lit8 v27, v27, 0x1

    :goto_a
    or-int v8, v8, v27

    .line 470
    .local v8, "fdnDisabled":Z
    or-int/2addr v10, v11

    .line 466
    .local v10, "hasICC":Z
    add-int/lit8 v12, v12, 0x1

    goto :goto_8

    .line 342
    .end local v8    # "fdnDisabled":Z
    .end local v10    # "hasICC":Z
    .end local v11    # "hasIccCard":Z
    .end local v12    # "i":I
    .end local v14    # "isAirPlane":Z
    .end local v17    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v21    # "slotCount":I
    .end local v26    # "virtualSlot":I
    .restart local v22    # "summaryId":I
    .restart local v24    # "value":I
    :pswitch_0
    const v22, 0x7f0b0641

    .line 343
    goto/16 :goto_0

    .line 350
    .end local v22    # "summaryId":I
    .end local v24    # "value":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mDialPadTouchTone:Lmiui/preference/ValuePreference;

    move-object/from16 v27, v0

    const v28, 0x7f0b0642

    invoke-virtual/range {v27 .. v28}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto/16 :goto_1

    .line 370
    :cond_11
    const/16 v27, 0x0

    goto/16 :goto_2

    .line 396
    .restart local v18    # "preciseCallState":I
    :cond_12
    const/16 v27, 0x0

    goto/16 :goto_3

    .line 421
    .end local v18    # "preciseCallState":I
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallAdvancedSetting;->mRespondViaSms:Lmiui/preference/ValuePreference;

    move-object/from16 v27, v0

    const v28, 0x7f0b05af

    invoke-virtual/range {v27 .. v28}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto/16 :goto_4

    .line 433
    .restart local v3    # "autoretry":I
    :cond_14
    const/16 v27, 0x0

    goto/16 :goto_5

    .line 438
    .end local v3    # "autoretry":I
    .restart local v9    # "hac":I
    :cond_15
    const/16 v27, 0x0

    goto/16 :goto_6

    .line 459
    .end local v9    # "hac":I
    :cond_16
    const/4 v14, 0x0

    .restart local v14    # "isAirPlane":Z
    goto :goto_7

    .line 468
    .restart local v12    # "i":I
    .restart local v17    # "phone":Lcom/android/internal/telephony/Phone;
    .restart local v21    # "slotCount":I
    .restart local v26    # "virtualSlot":I
    :cond_17
    const/4 v11, 0x0

    .restart local v11    # "hasIccCard":Z
    goto :goto_9

    .line 469
    :cond_18
    const/16 v27, 0x0

    goto :goto_a

    .line 474
    .end local v11    # "hasIccCard":Z
    .end local v12    # "i":I
    .end local v17    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v21    # "slotCount":I
    .end local v26    # "virtualSlot":I
    :cond_19
    if-eqz v8, :cond_1a

    if-eqz v10, :cond_1a

    xor-int/lit8 v6, v14, 0x1

    .line 475
    :goto_b
    const-string/jumbo v27, "button_voice_privacy"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v6, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->setEnabledForKey(ZLjava/lang/String;)V

    .line 476
    const-string/jumbo v27, "button_caller_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v6, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->setEnabledForKey(ZLjava/lang/String;)V

    .line 477
    if-eqz v10, :cond_1b

    xor-int/lit8 v27, v14, 0x1

    :goto_c
    const-string/jumbo v28, "button_fdn"

    move-object/from16 v0, p0

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/android/phone/settings/CallAdvancedSetting;->setEnabledForKey(ZLjava/lang/String;)V

    .line 478
    if-eqz v10, :cond_1c

    xor-int/lit8 v27, v14, 0x1

    :goto_d
    const-string/jumbo v28, "button_voicemail"

    move-object/from16 v0, p0

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/android/phone/settings/CallAdvancedSetting;->setEnabledForKey(ZLjava/lang/String;)V

    .line 479
    if-eqz v10, :cond_1d

    xor-int/lit8 v27, v14, 0x1

    :goto_e
    const-string/jumbo v28, "button_call_barring"

    move-object/from16 v0, p0

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/android/phone/settings/CallAdvancedSetting;->setEnabledForKey(ZLjava/lang/String;)V

    .line 481
    return-void

    .line 474
    :cond_1a
    const/4 v6, 0x0

    .local v6, "canBeEnabled":Z
    goto :goto_b

    .line 477
    .end local v6    # "canBeEnabled":Z
    :cond_1b
    const/16 v27, 0x0

    goto :goto_c

    .line 478
    :cond_1c
    const/16 v27, 0x0

    goto :goto_d

    .line 479
    :cond_1d
    const/16 v27, 0x0

    goto :goto_e

    .line 340
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 164
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 166
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 167
    .local v0, "actionBar":Lmiui/app/ActionBar;
    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {v0, v3}, Lmiui/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 170
    :cond_0
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/telephony/TelephonyManager;->getIccCardCount()I

    move-result v2

    iput v2, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIccCardCount:I

    .line 171
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isDcOnlyVirtualSim(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 172
    iget v2, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIccCardCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIccCardCount:I

    .line 174
    :cond_1
    invoke-direct {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->initScreen()V

    .line 175
    new-instance v1, Landroid/content/IntentFilter;

    .line 176
    const-string/jumbo v2, "android.intent.action.RADIO_TECHNOLOGY"

    .line 175
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 177
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 178
    iget-object v2, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/android/phone/settings/CallAdvancedSetting;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 179
    iput-boolean v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIsReceiverRegistered:Z

    .line 180
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    .line 191
    iget-object v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 192
    iget-boolean v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIsReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mIsReceiverRegistered:Z

    .line 194
    iget-object v0, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/CallAdvancedSetting;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 196
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 200
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->finish()V

    .line 202
    const/4 v0, 0x1

    return v0

    .line 204
    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 493
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    if-ne p1, v3, :cond_1

    .line 494
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v3, p2}, Lcom/android/phone/settings/ValueListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 495
    .local v1, "index":I
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 496
    const-string/jumbo v4, "t9_indexing_key"

    .line 497
    invoke-static {}, Landroid/provider/MiuiSettings$System;->getT9IndexingKeyDefault()I

    move-result v5

    .line 495
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eq v1, v3, :cond_0

    .line 498
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "t9_indexing_key"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 500
    invoke-direct {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->showRebuildT9IndexDialog()V

    .line 501
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mT9IndexingMethod:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 502
    const v5, 0x7f07006f

    .line 501
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 557
    .end local v1    # "index":I
    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 504
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_1
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBackground:Lcom/android/phone/settings/ValueListPreference;

    if-ne p1, v3, :cond_2

    .line 505
    iget-object v4, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBackground:Lcom/android/phone/settings/ValueListPreference;

    move-object v3, p2

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/android/phone/settings/ValueListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 506
    .restart local v1    # "index":I
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallBackground:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 507
    const v5, 0x7f070071

    .line 506
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 508
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v3, v4}, Landroid/provider/MiuiSettings$Telephony;->setCallBackgroundType(Landroid/content/ContentResolver;I)Z

    goto :goto_0

    .line 509
    .end local v1    # "index":I
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_2
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    if-ne p1, v3, :cond_3

    .line 510
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object v3, p2

    .line 511
    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 510
    invoke-static {v4, v3}, Landroid/provider/MiuiSettings$Telephony;->setMissedCallNotifyTimes(Landroid/content/ContentResolver;I)V

    .line 512
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v3, p2}, Lcom/android/phone/settings/ValueListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 513
    .restart local v1    # "index":I
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mMissedCallNotifyTimes:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 514
    const v5, 0x7f070073

    .line 513
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    goto :goto_0

    .line 515
    .end local v1    # "index":I
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_3
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

    if-ne p1, v3, :cond_4

    .line 516
    iget-object v4, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

    move-object v3, p2

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/android/phone/settings/ValueListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 517
    .restart local v1    # "index":I
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 518
    const v4, 0x7f070075

    .line 517
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 519
    .local v2, "vibrateSummary":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-static {v3, p2}, Landroid/provider/MiuiSettings$Telephony;->setVibrateKey(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 520
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mConDisconVibrate:Lcom/android/phone/settings/ValueListPreference;

    aget-object v4, v2, v1

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 521
    .end local v1    # "index":I
    .end local v2    # "vibrateSummary":[Ljava/lang/String;
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_4
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    if-ne p1, v3, :cond_6

    .line 522
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 523
    new-instance v3, Lmiui/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 524
    const v4, 0x7f0b061b

    .line 523
    invoke-virtual {v3, v4}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    .line 525
    const v4, 0x7f0b061d

    .line 523
    invoke-virtual {v3, v4}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    .line 526
    const/high16 v4, 0x1040000

    const/4 v5, 0x0

    .line 523
    invoke-virtual {v3, v4, v5}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    .line 528
    new-instance v4, Lcom/android/phone/settings/CallAdvancedSetting$3;

    invoke-direct {v4, p0}, Lcom/android/phone/settings/CallAdvancedSetting$3;-><init>(Lcom/android/phone/settings/CallAdvancedSetting;)V

    .line 527
    const v5, 0x104000a

    .line 523
    invoke-virtual {v3, v5, v4}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/app/AlertDialog$Builder;->show()Lmiui/app/AlertDialog;

    .line 543
    :goto_1
    return v6

    .line 538
    :cond_5
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCdmaDisplayPreciseCallState:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    invoke-virtual {v3, v6}, Lcom/android/phone/settings/CheckBoxTitleIconPreference;->setChecked(Z)V

    .line 539
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 540
    const-string/jumbo v4, "cdma_precise_answer_state"

    .line 539
    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    .line 544
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_6
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallWaitingTone:Lcom/android/phone/settings/ValueListPreference;

    if-ne p1, v3, :cond_7

    .line 545
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallWaitingTone:Lcom/android/phone/settings/ValueListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v3, p2}, Lcom/android/phone/settings/ValueListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 546
    .restart local v1    # "index":I
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070077

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 547
    .local v0, "callWaitToneSummary":[Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mCallWaitingTone:Lcom/android/phone/settings/ValueListPreference;

    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 548
    invoke-static {p0, v1}, Landroid/provider/MiuiSettings$Telephony;->setCallWaitingTone(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 549
    .end local v0    # "callWaitToneSummary":[Ljava/lang/String;
    .end local v1    # "index":I
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_7
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonDTMF:Landroid/preference/ListPreference;

    if-ne p1, v3, :cond_8

    .line 550
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonDTMF:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v3, p2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 551
    .restart local v1    # "index":I
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 552
    const-string/jumbo v4, "dtmf_tone_type"

    .line 551
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 553
    .end local v1    # "index":I
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_8
    iget-object v3, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonTTY:Landroid/preference/ListPreference;

    if-ne p1, v3, :cond_0

    .line 554
    invoke-direct {p0, p1, p2}, Lcom/android/phone/settings/CallAdvancedSetting;->handleTTYChange(Landroid/preference/Preference;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const v6, 0x7f0b063c

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 562
    iget-object v5, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mDialPadTouchTone:Lmiui/preference/ValuePreference;

    if-ne p1, v5, :cond_0

    .line 563
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "miui.intent.action.APP_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 564
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "com.android.contacts"

    .line 565
    const-string/jumbo v5, "com.android.contacts.preference.ContactsPreferenceActivity"

    .line 564
    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 566
    const-string/jumbo v3, ":android:show_fragment"

    .line 567
    const-string/jumbo v5, "com.android.contacts.preference.DialpadTouchTonePreferenceFragment"

    .line 566
    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 568
    const-string/jumbo v3, ":android:show_fragment_title"

    .line 569
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 568
    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 570
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/phone/MiuiPhoneUtils;->addWindowLable(Landroid/content/Intent;Ljava/lang/CharSequence;)V

    .line 572
    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallAdvancedSetting;->startActivity(Landroid/content/Intent;)V

    .line 573
    return v4

    .line 574
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v5, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    if-ne p1, v5, :cond_1

    .line 576
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v5, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mAutoRedial:Lcom/android/phone/settings/CheckBoxTitleIconPreference;

    invoke-virtual {v5}, Lcom/android/phone/settings/CheckBoxTitleIconPreference;->isChecked()Z

    move-result v5

    .line 575
    invoke-static {v3, v5}, Landroid/provider/MiuiSettings$Telephony;->setAutoRedialEnabled(Landroid/content/ContentResolver;Z)V

    .line 577
    return v4

    .line 578
    :cond_1
    iget-object v5, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

    if-ne p1, v5, :cond_3

    .line 579
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 580
    const-string/jumbo v6, "call_auto_retry"

    .line 581
    iget-object v7, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonAutoRetry:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_2

    move v3, v4

    .line 579
    :cond_2
    invoke-static {v5, v6, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 582
    return v4

    .line 583
    :cond_3
    iget-object v5, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonHAC:Landroid/preference/CheckBoxPreference;

    if-ne p1, v5, :cond_6

    .line 584
    iget-object v5, p0, Lcom/android/phone/settings/CallAdvancedSetting;->mButtonHAC:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v1, 0x1

    .line 586
    .local v1, "hac":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "hearing_aid"

    invoke-static {v5, v6, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 588
    const-string/jumbo v5, "audio"

    invoke-virtual {p0, v5}, Lcom/android/phone/settings/CallAdvancedSetting;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 589
    .local v0, "am":Landroid/media/AudioManager;
    const-string/jumbo v5, "%s=%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const-string/jumbo v7, "HACSetting"

    aput-object v7, v6, v3

    if-eqz v1, :cond_5

    const-string/jumbo v3, "ON"

    :goto_1
    aput-object v3, v6, v4

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 590
    return v4

    .line 584
    .end local v0    # "am":Landroid/media/AudioManager;
    .end local v1    # "hac":I
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "hac":I
    goto :goto_0

    .line 589
    .restart local v0    # "am":Landroid/media/AudioManager;
    :cond_5
    const-string/jumbo v3, "OFF"

    goto :goto_1

    .line 593
    .end local v0    # "am":Landroid/media/AudioManager;
    .end local v1    # "hac":I
    :cond_6
    return v3
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 184
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 185
    invoke-direct {p0}, Lcom/android/phone/settings/CallAdvancedSetting;->updateScreen()V

    .line 186
    return-void
.end method
