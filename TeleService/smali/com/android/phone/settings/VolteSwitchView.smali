.class public Lcom/android/phone/settings/VolteSwitchView;
.super Ljava/lang/Object;
.source "VolteSwitchView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/VolteSwitchView$1;,
        Lcom/android/phone/settings/VolteSwitchView$2;
    }
.end annotation


# static fields
.field private static final DUAL_VOLTE_SUPPORTED:Z

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mButtonVolte:Landroid/preference/CheckBoxPreference;

.field private final mHandler:Landroid/os/Handler;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mPrefActivity:Landroid/preference/PreferenceActivity;

.field private mPrefGroup:Landroid/preference/PreferenceGroup;

.field private mPromptDialog:Lmiui/app/AlertDialog;

.field private mPromptDialogCdmaVolte:Lmiui/app/AlertDialog;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/phone/settings/VolteSwitchView;->DUAL_VOLTE_SUPPORTED:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/VolteSwitchView;)Landroid/os/Handler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/VolteSwitchView;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/PreferenceActivity;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/VolteSwitchView;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VolteSwitchView;->isPhoneInCall()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/VolteSwitchView;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VolteSwitchView;->setEnhanced4gLteModeSetting()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/VolteSwitchView;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VolteSwitchView;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/VolteSwitchView;->showDual4gDialog()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/phone/settings/VolteSwitchView;Lcom/android/internal/telephony/Phone;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/VolteSwitchView;
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/VolteSwitchView;->showTipsWhenTurnCdmaVolteOn(Lcom/android/internal/telephony/Phone;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/android/phone/settings/VolteSwitchView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    .line 61
    sget-boolean v0, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    sput-boolean v0, Lcom/android/phone/settings/VolteSwitchView;->DUAL_VOLTE_SUPPORTED:Z

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/preference/PreferenceActivity;Landroid/preference/PreferenceGroup;Lcom/android/internal/telephony/Phone;)V
    .locals 3
    .param p1, "prefActivity"    # Landroid/preference/PreferenceActivity;
    .param p2, "prefGroup"    # Landroid/preference/PreferenceGroup;
    .param p3, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 67
    iput-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialog:Lmiui/app/AlertDialog;

    .line 68
    iput-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialogCdmaVolte:Lmiui/app/AlertDialog;

    .line 144
    new-instance v1, Lcom/android/phone/settings/VolteSwitchView$1;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/VolteSwitchView$1;-><init>(Lcom/android/phone/settings/VolteSwitchView;)V

    iput-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 164
    new-instance v1, Lcom/android/phone/settings/VolteSwitchView$2;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/VolteSwitchView$2;-><init>(Lcom/android/phone/settings/VolteSwitchView;)V

    iput-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mHandler:Landroid/os/Handler;

    .line 71
    iput-object p1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    .line 72
    iput-object p2, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefGroup:Landroid/preference/PreferenceGroup;

    .line 73
    iput-object p3, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 74
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefGroup:Landroid/preference/PreferenceGroup;

    const-string/jumbo v2, "enhanced_4g_lte"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    .line 75
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/android/phone/settings/VolteSwitchView$3;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/VolteSwitchView$3;-><init>(Lcom/android/phone/settings/VolteSwitchView;)V

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 107
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.android.ims.IMS_SERVICE_UP"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 108
    .local v0, "mIntentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 109
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/preference/PreferenceActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 110
    return-void
.end method

.method private hasSimInserted()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 262
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 263
    .local v1, "phones":[Lcom/android/internal/telephony/Phone;
    array-length v4, v1

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v1, v2

    .line 264
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v5

    invoke-interface {v5}, Lcom/android/internal/telephony/IccCard;->hasIccCard()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 265
    const/4 v2, 0x1

    return v2

    .line 263
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 268
    .end local v0    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    return v3
.end method

.method private isEnhanced4gLteModeSettingEnabledByUser()Z
    .locals 4

    .prologue
    .line 252
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_1

    .line 253
    sget-boolean v1, Lcom/android/phone/settings/VolteSwitchView;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 254
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    :goto_0
    sget-object v1, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "phoneId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    invoke-virtual {v1, v2}, Lmiui/telephony/TelephonyManager;->isVolteEnabledByUser(I)Z

    move-result v1

    return v1

    .line 253
    .end local v0    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_0
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getDataPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .restart local v0    # "phone":Lcom/android/internal/telephony/Phone;
    goto :goto_0

    .line 257
    .end local v0    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v1}, Lcom/android/ims/ImsManager;->isEnhanced4gLteModeSettingEnabledByUser(Landroid/content/Context;)Z

    move-result v1

    return v1
.end method

.method private isNetworkNotSupportTurnOffVoLTE()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 276
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 277
    return v2

    .line 280
    :cond_0
    sget-boolean v1, Lcom/android/phone/settings/VolteSwitchView;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v1, :cond_1

    .line 281
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v0

    .line 285
    .local v0, "defaultDataSubId":I
    :goto_0
    invoke-static {v0}, Lmiui/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 286
    return v2

    .line 283
    .end local v0    # "defaultDataSubId":I
    :cond_1
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/SubscriptionManager;->getDefaultDataSubscriptionId()I

    move-result v0

    .restart local v0    # "defaultDataSubId":I
    goto :goto_0

    .line 289
    :cond_2
    const v1, 0x7f0e0036

    invoke-static {v0, v1}, Lcom/android/phone/MiuiPhoneUtils;->getCarrierCofigBooleanValue(II)Z

    move-result v1

    return v1
.end method

.method private isPhoneInCall()Z
    .locals 2

    .prologue
    .line 272
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getCallManager()Lcom/android/internal/telephony/CallManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static removeVolteSwitch(Landroid/preference/PreferenceGroup;)V
    .locals 2
    .param p0, "prefGroup"    # Landroid/preference/PreferenceGroup;

    .prologue
    .line 308
    if-eqz p0, :cond_0

    .line 309
    const-string/jumbo v1, "enhanced_4g_lte"

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 310
    .local v0, "volteSwitchView":Landroid/preference/Preference;
    if-eqz v0, :cond_0

    .line 311
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 314
    .end local v0    # "volteSwitchView":Landroid/preference/Preference;
    :cond_0
    return-void
.end method

.method private setEnhanced4gLteModeSetting()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 232
    sget-boolean v3, Lcom/android/phone/settings/VolteSwitchView;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v3, :cond_1

    .line 233
    iget-object v3, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    iget-object v4, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    iget-object v5, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/android/services/telephony/ims/ImsAdapter;->setEnhanced4gLteModeSetting(Landroid/content/Context;ZI)V

    .line 234
    iget-object v3, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v3

    iget-object v4, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/android/phone/VolteEnableManager;->saveUserSelectedVolteState(IZ)V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-object v3, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    iget-object v4, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/android/services/telephony/ims/ImsAdapter;->setEnhanced4gLteModeSetting(Landroid/content/Context;Z)V

    .line 237
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getDataPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 238
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    .line 239
    .local v2, "subId":I
    iget-object v3, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/android/phone/VolteEnableManager;->saveUserSelectedVolteState(IZ)V

    .line 240
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 242
    .local v0, "context":Landroid/content/Context;
    iget-object v3, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 243
    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->isCTVoLTESupport(Lcom/android/internal/telephony/Phone;)Z

    move-result v3

    .line 242
    if-eqz v3, :cond_0

    .line 244
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1a

    if-ge v3, v4, :cond_0

    .line 245
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "volte_vt_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eq v3, v6, :cond_0

    .line 246
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "volte_vt_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method private showDual4gDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 114
    sget-boolean v0, Lcom/android/phone/settings/VolteSwitchView;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v0

    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 116
    invoke-static {}, Lcom/android/phone/Dual4GManager;->isDual4GEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 115
    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Lmiui/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    invoke-direct {v0, v1}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 118
    const v1, 0x7f0b06ec

    .line 117
    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    .line 120
    new-instance v1, Lcom/android/phone/settings/VolteSwitchView$4;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/VolteSwitchView$4;-><init>(Lcom/android/phone/settings/VolteSwitchView;)V

    .line 119
    const v2, 0x104000a

    .line 117
    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    .line 135
    const/high16 v1, 0x1040000

    .line 117
    invoke-virtual {v0, v1, v3}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    .line 136
    const/4 v1, 0x0

    .line 117
    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialog:Lmiui/app/AlertDialog;

    .line 138
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->show()V

    .line 139
    sget-object v0, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "show dual 4g enable dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_0
    return-void
.end method

.method private showNoServiceTipsWhenTurnCdmaVolteOn(Lcom/android/internal/telephony/Phone;)V
    .locals 4
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 341
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v1

    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->isLte(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    const v0, 0x7f0b072d

    .line 343
    .local v0, "messageId":I
    :goto_0
    new-instance v1, Lmiui/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    invoke-direct {v1, v2}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    .line 346
    new-instance v2, Lcom/android/phone/settings/VolteSwitchView$5;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/VolteSwitchView$5;-><init>(Lcom/android/phone/settings/VolteSwitchView;)V

    .line 345
    const v3, 0x104000a

    .line 343
    invoke-virtual {v1, v3, v2}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    .line 361
    new-instance v2, Lcom/android/phone/settings/VolteSwitchView$6;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/VolteSwitchView$6;-><init>(Lcom/android/phone/settings/VolteSwitchView;)V

    .line 360
    const/high16 v3, 0x1040000

    .line 343
    invoke-virtual {v1, v3, v2}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    .line 367
    const/4 v2, 0x0

    .line 343
    invoke-virtual {v1, v2}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialogCdmaVolte:Lmiui/app/AlertDialog;

    .line 369
    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialogCdmaVolte:Lmiui/app/AlertDialog;

    invoke-virtual {v1}, Lmiui/app/AlertDialog;->show()V

    .line 370
    sget-object v1, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "show cdma volte enable dialog"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    return-void

    .line 342
    .end local v0    # "messageId":I
    :cond_0
    const v0, 0x7f0b072f

    .restart local v0    # "messageId":I
    goto :goto_0
.end method

.method private showTipsWhenTurnCdmaVolteOn(Lcom/android/internal/telephony/Phone;)V
    .locals 4
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/16 v1, 0x65

    .line 324
    invoke-direct {p0, p1}, Lcom/android/phone/settings/VolteSwitchView;->showNoServiceTipsWhenTurnCdmaVolteOn(Lcom/android/internal/telephony/Phone;)V

    .line 326
    sget-boolean v0, Lmiui/os/Build;->IS_CT_CUSTOMIZATION_TEST:Z

    if-nez v0, :cond_0

    .line 327
    return-void

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 333
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mHandler:Landroid/os/Handler;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 334
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 293
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 294
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    iget-object v1, p0, Lcom/android/phone/settings/VolteSwitchView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 296
    iput-object v2, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialog:Lmiui/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialog:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->dismiss()V

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialogCdmaVolte:Lmiui/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialogCdmaVolte:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 303
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView;->mPromptDialogCdmaVolte:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->dismiss()V

    .line 305
    :cond_2
    return-void
.end method

.method public updateVolteButtonUI()V
    .locals 13

    .prologue
    const/4 v9, 0x0

    .line 179
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    if-eqz v10, :cond_2

    .line 180
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v10}, Lcom/android/services/telephony/ims/ImsAdapter;->isVolteSupportedByDevice(Landroid/content/Context;)Z

    move-result v0

    .line 181
    .local v0, "deviceSupported":Z
    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v5

    .line 182
    .local v5, "region":Ljava/lang/String;
    sget-boolean v10, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefActivity:Landroid/preference/PreferenceActivity;

    invoke-static {v10, v5}, Lcom/android/phone/MiuiPhoneUtils;->isRegionSupportVoLTE(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    .line 183
    :goto_0
    sget-object v10, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "updateVolteButtonUI deviceSupported="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ", regionSupported="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefGroup:Landroid/preference/PreferenceGroup;

    const-string/jumbo v11, "enhanced_4g_lte"

    invoke-virtual {v10, v11}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v10

    if-eqz v10, :cond_4

    const/4 v2, 0x1

    .line 185
    .local v2, "isVolteButtionExist":Z
    :goto_1
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isVolteRegionFeatureEnabled()Z

    move-result v3

    .line 191
    .local v3, "isVolteRegionFeatureEnabled":Z
    if-eqz v0, :cond_1

    .line 192
    if-eqz v3, :cond_0

    xor-int/lit8 v10, v6, 0x1

    .line 191
    if-nez v10, :cond_1

    .line 193
    :cond_0
    sget-boolean v10, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v10, :cond_5

    xor-int/lit8 v10, v3, 0x1

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-static {v10}, Lcom/android/phone/MiuiPhoneUtils;->isVolteEnabledByPlatform(Lcom/android/internal/telephony/Phone;)Z

    move-result v10

    xor-int/lit8 v10, v10, 0x1

    .line 191
    if-eqz v10, :cond_5

    .line 194
    :cond_1
    if-eqz v2, :cond_2

    .line 195
    iget-object v9, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefGroup:Landroid/preference/PreferenceGroup;

    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 229
    .end local v0    # "deviceSupported":Z
    .end local v2    # "isVolteButtionExist":Z
    .end local v3    # "isVolteRegionFeatureEnabled":Z
    .end local v5    # "region":Ljava/lang/String;
    :cond_2
    :goto_2
    return-void

    .line 182
    .restart local v0    # "deviceSupported":Z
    .restart local v5    # "region":Ljava/lang/String;
    :cond_3
    const/4 v6, 0x1

    .local v6, "regionSupported":Z
    goto :goto_0

    .line 184
    .end local v6    # "regionSupported":Z
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "isVolteButtionExist":Z
    goto :goto_1

    .line 198
    .restart local v3    # "isVolteRegionFeatureEnabled":Z
    :cond_5
    if-nez v2, :cond_6

    .line 199
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mPrefGroup:Landroid/preference/PreferenceGroup;

    iget-object v11, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v10, v11}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 201
    :cond_6
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-static {v10}, Lcom/android/phone/MiuiPhoneUtils;->isVolteEnabledByPlatform(Lcom/android/internal/telephony/Phone;)Z

    move-result v7

    .line 202
    .local v7, "simSupported":Z
    sget-object v10, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "updateVolteButtonUI simSupported="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-direct {p0}, Lcom/android/phone/settings/VolteSwitchView;->hasSimInserted()Z

    move-result v1

    .line 204
    .local v1, "isSimInserted":Z
    if-nez v7, :cond_7

    xor-int/lit8 v10, v1, 0x1

    if-eqz v10, :cond_c

    .line 205
    :cond_7
    sget-boolean v10, Lcom/android/phone/settings/VolteSwitchView;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v10, :cond_8

    iget-object v4, p0, Lcom/android/phone/settings/VolteSwitchView;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 206
    .local v4, "phone":Lcom/android/internal/telephony/Phone;
    :goto_3
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v10

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v11

    invoke-virtual {v10, v11}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v8

    .line 207
    .local v8, "subInfo":Lmiui/telephony/SubscriptionInfo;
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0}, Lcom/android/phone/settings/VolteSwitchView;->isEnhanced4gLteModeSettingEnabledByUser()Z

    move-result v11

    invoke-virtual {v10, v11}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 208
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isCTDualVolteSupported()Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v10

    invoke-static {v10}, Lcom/android/phone/MiuiPhoneUtils;->isViceSlotOfDualCTSim(I)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 209
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 210
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v10, v9}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 211
    iget-object v9, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    const v10, 0x7f0b0735

    invoke-virtual {v9, v10}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_2

    .line 205
    .end local v4    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v8    # "subInfo":Lmiui/telephony/SubscriptionInfo;
    :cond_8
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v10

    invoke-static {v10}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    .restart local v4    # "phone":Lcom/android/internal/telephony/Phone;
    goto :goto_3

    .line 213
    .restart local v8    # "subInfo":Lmiui/telephony/SubscriptionInfo;
    :cond_9
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_a

    invoke-direct {p0}, Lcom/android/phone/settings/VolteSwitchView;->isNetworkNotSupportTurnOffVoLTE()Z

    move-result v11

    xor-int/lit8 v11, v11, 0x1

    if-eqz v11, :cond_a

    .line 214
    if-eqz v8, :cond_a

    invoke-virtual {v8}, Lmiui/telephony/SubscriptionInfo;->isActivated()Z

    move-result v9

    .line 213
    :cond_a
    invoke-virtual {v10, v9}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 215
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-static {v4}, Lcom/android/phone/MiuiPhoneUtils;->isCTVoLTESupport(Lcom/android/internal/telephony/Phone;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 216
    const v9, 0x7f0b06e9

    .line 215
    :goto_4
    invoke-virtual {v10, v9}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto/16 :goto_2

    .line 216
    :cond_b
    const v9, 0x7f0b06e8

    goto :goto_4

    .line 219
    .end local v4    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v8    # "subInfo":Lmiui/telephony/SubscriptionInfo;
    :cond_c
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v10, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 220
    iget-object v10, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v10, v9}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 221
    sget-boolean v9, Lcom/android/phone/settings/VolteSwitchView;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v9, :cond_d

    .line 222
    iget-object v9, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    const v10, 0x7f0b06eb

    invoke-virtual {v9, v10}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto/16 :goto_2

    .line 224
    :cond_d
    iget-object v9, p0, Lcom/android/phone/settings/VolteSwitchView;->mButtonVolte:Landroid/preference/CheckBoxPreference;

    const v10, 0x7f0b06ea

    invoke-virtual {v9, v10}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto/16 :goto_2
.end method
