.class Lcom/android/phone/CallForwardType$2;
.super Ljava/lang/Object;
.source "CallForwardType.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/CallForwardType;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CallForwardType;


# direct methods
.method constructor <init>(Lcom/android/phone/CallForwardType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/CallForwardType;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/CallForwardType$2;->this$0:Lcom/android/phone/CallForwardType;

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "pref"    # Landroid/preference/Preference;

    .prologue
    .line 88
    iget-object v1, p0, Lcom/android/phone/CallForwardType$2;->this$0:Lcom/android/phone/CallForwardType;

    invoke-static {v1}, Lcom/android/phone/CallForwardType;->-get0(Lcom/android/phone/CallForwardType;)Lcom/android/phone/SubscriptionInfoHelper;

    move-result-object v1

    const-class v2, Lcom/android/phone/GsmUmtsCallForwardOptions;

    invoke-virtual {v1, v2}, Lcom/android/phone/SubscriptionInfoHelper;->getIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 89
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "CallForwardType"

    const-string/jumbo v2, "Video button clicked!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string/jumbo v1, "service_class"

    .line 91
    const/16 v2, 0x50

    .line 90
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 93
    iget-object v1, p0, Lcom/android/phone/CallForwardType$2;->this$0:Lcom/android/phone/CallForwardType;

    invoke-virtual {v1, v0}, Lcom/android/phone/CallForwardType;->startActivity(Landroid/content/Intent;)V

    .line 94
    const/4 v1, 0x1

    return v1
.end method
