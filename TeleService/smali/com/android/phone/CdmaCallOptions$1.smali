.class Lcom/android/phone/CdmaCallOptions$1;
.super Ljava/lang/Object;
.source "CdmaCallOptions.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/CdmaCallOptions;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CdmaCallOptions;

.field final synthetic val$phone:Lcom/android/internal/telephony/Phone;


# direct methods
.method constructor <init>(Lcom/android/phone/CdmaCallOptions;Lcom/android/internal/telephony/Phone;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/CdmaCallOptions;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/CdmaCallOptions$1;->this$0:Lcom/android/phone/CdmaCallOptions;

    iput-object p2, p0, Lcom/android/phone/CdmaCallOptions$1;->val$phone:Lcom/android/internal/telephony/Phone;

    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 218
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "org.codeaurora.settings.CDMA_CALL_WAITING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 219
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "subscription"

    .line 220
    iget-object v2, p0, Lcom/android/phone/CdmaCallOptions$1;->val$phone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    .line 219
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 221
    iget-object v1, p0, Lcom/android/phone/CdmaCallOptions$1;->this$0:Lcom/android/phone/CdmaCallOptions;

    invoke-virtual {v1, v0}, Lcom/android/phone/CdmaCallOptions;->startActivity(Landroid/content/Intent;)V

    .line 222
    const/4 v1, 0x1

    return v1
.end method
