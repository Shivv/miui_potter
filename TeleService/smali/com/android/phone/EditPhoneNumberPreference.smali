.class public Lcom/android/phone/EditPhoneNumberPreference;
.super Landroid/preference/EditTextPreference;
.source "EditPhoneNumberPreference.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;,
        Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private canShowTimerSetting:Z

.field private mButtonClicked:I

.field private mChangeNumberText:Ljava/lang/CharSequence;

.field private mChecked:Z

.field private mConfirmationMode:I

.field private mContactListIntent:Landroid/content/Intent;

.field private mContactPickButton:Landroid/widget/ImageButton;

.field private mDialogFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private mDialogOnClosedListener:Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;

.field private mDisableText:Ljava/lang/CharSequence;

.field private mEnableText:Ljava/lang/CharSequence;

.field private mEncodedText:Ljava/lang/String;

.field private mEndDate:Ljava/util/Calendar;

.field private mEndTimeAMPM:I

.field private mEndTimeHour:I

.field private mEndTimeMinute:I

.field private mEndTimeSetting:Landroid/view/View;

.field private mGetDefaultNumberListener:Lcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;

.field private mParentActivity:Landroid/app/Activity;

.field private mPhoneNumber:Ljava/lang/String;

.field private mPrefId:I

.field private mStartDate:Ljava/util/Calendar;

.field private mStartTimeAPMP:I

.field private mStartTimeHour:I

.field private mStartTimeMinute:I

.field private mStartTimeSetting:Landroid/view/View;

.field private mSummaryOff:Ljava/lang/CharSequence;

.field private mSummaryOn:Ljava/lang/CharSequence;

.field private mTimeEndFormate:Landroid/widget/Spinner;

.field private mTimeEndHourSpinner:Landroid/widget/Spinner;

.field private mTimeEndMinuteSpinner:Landroid/widget/Spinner;

.field private mTimeEndTextView:Landroid/widget/TextView;

.field private mTimePeriodAllDay:Landroid/widget/CheckBox;

.field private mTimePeriodAllDayChecked:Z

.field private mTimePeriodString:Ljava/lang/String;

.field private mTimeStartFormate:Landroid/widget/Spinner;

.field private mTimeStartHourSpinner:Landroid/widget/Spinner;

.field private mTimeStartMinuteSpinner:Landroid/widget/Spinner;

.field private mTimeStartTextView:Landroid/widget/TextView;

.field private mValidEndTimeHour:I

.field private mValidEndTimeMinute:I

.field private mValidStartTimeHour:I

.field private mValidStartTimeMinute:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/EditPhoneNumberPreference;)Landroid/content/Intent;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/EditPhoneNumberPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mContactListIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/EditPhoneNumberPreference;)Landroid/app/Activity;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/EditPhoneNumberPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mParentActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/EditPhoneNumberPreference;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/EditPhoneNumberPreference;

    .prologue
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPrefId:I

    return v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/EditPhoneNumberPreference;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/EditPhoneNumberPreference;
    .param p1, "isChecked"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/EditPhoneNumberPreference;->onAllDayChecked(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 191
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/EditPhoneNumberPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 192
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v5, 0x16

    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 166
    invoke-direct {p0, p1, p2}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const-string/jumbo v1, "EditPhoneNumberPreference"

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->TAG:Ljava/lang/String;

    .line 78
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartDate:Ljava/util/Calendar;

    .line 79
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndDate:Ljava/util/Calendar;

    .line 95
    iput-boolean v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->canShowTimerSetting:Z

    .line 96
    iput v5, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeHour:I

    .line 97
    iput v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeMinute:I

    .line 98
    iput v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeHour:I

    .line 99
    iput v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeMinute:I

    .line 101
    iput v5, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeHour:I

    .line 102
    iput v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeMinute:I

    .line 103
    iput v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeAPMP:I

    .line 104
    iput v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeHour:I

    .line 105
    iput v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeMinute:I

    .line 106
    iput v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeAMPM:I

    .line 110
    iput-boolean v4, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDayChecked:Z

    .line 590
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEncodedText:Ljava/lang/String;

    .line 168
    const v1, 0x7f040041

    invoke-virtual {p0, v1}, Lcom/android/phone/EditPhoneNumberPreference;->setDialogLayoutResource(I)V

    .line 171
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.PICK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mContactListIntent:Landroid/content/Intent;

    .line 172
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mContactListIntent:Landroid/content/Intent;

    const-string/jumbo v2, "vnd.android.cursor.dir/phone_v2"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    sget-object v1, Lcom/android/phone/R$styleable;->EditPhoneNumberPreference:[I

    const v2, 0x7f0c0194

    .line 175
    invoke-virtual {p1, p2, v1, v3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 177
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEnableText:Ljava/lang/CharSequence;

    .line 178
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mDisableText:Ljava/lang/CharSequence;

    .line 179
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mChangeNumberText:Ljava/lang/CharSequence;

    .line 180
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mConfirmationMode:I

    .line 181
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 184
    sget-object v1, Landroid/R$styleable;->CheckBoxPreference:[I

    invoke-virtual {p1, p2, v1, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 185
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mSummaryOn:Ljava/lang/CharSequence;

    .line 186
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mSummaryOff:Ljava/lang/CharSequence;

    .line 187
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 188
    return-void
.end method

.method private formateTime(Ljava/util/Calendar;II)Ljava/lang/String;
    .locals 11
    .param p1, "mDate"    # Ljava/util/Calendar;
    .param p2, "hour"    # I
    .param p3, "minute"    # I

    .prologue
    .line 528
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 529
    .local v10, "now":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v8

    .line 530
    .local v8, "mDateFormat":Ljava/text/DateFormat;
    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 531
    const/4 v0, 0x2

    invoke-virtual {v10, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 532
    const/4 v0, 0x5

    invoke-virtual {v10, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 535
    const/4 v6, 0x0

    move-object v0, p1

    move v4, p2

    move v5, p3

    .line 530
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 536
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v9

    .line 537
    .local v9, "mDateTimeOnly":Ljava/util/Date;
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 539
    .local v7, "fomatedTimeString":Ljava/lang/String;
    return-object v7
.end method

.method private initTimeSettingsView(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 696
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "initTimeSettingsView, mPrefId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPrefId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    const v0, 0x7f0d00f3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDay:Landroid/widget/CheckBox;

    .line 699
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDay:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDayChecked:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 700
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDay:Landroid/widget/CheckBox;

    new-instance v1, Lcom/android/phone/EditPhoneNumberPreference$2;

    invoke-direct {v1, p0}, Lcom/android/phone/EditPhoneNumberPreference$2;-><init>(Lcom/android/phone/EditPhoneNumberPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 709
    const v0, 0x7f0d00e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartTextView:Landroid/widget/TextView;

    .line 710
    const v0, 0x7f0d00e7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeSetting:Landroid/view/View;

    .line 712
    invoke-direct {p0}, Lcom/android/phone/EditPhoneNumberPreference;->is24Hour()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0d00e8

    .line 711
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartHourSpinner:Landroid/widget/Spinner;

    .line 713
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartHourSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 714
    const v0, 0x7f0d00ea

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartMinuteSpinner:Landroid/widget/Spinner;

    .line 715
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartMinuteSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 718
    const v0, 0x7f0d00ec

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndTextView:Landroid/widget/TextView;

    .line 719
    const v0, 0x7f0d00ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeSetting:Landroid/view/View;

    .line 721
    invoke-direct {p0}, Lcom/android/phone/EditPhoneNumberPreference;->is24Hour()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d00ee

    .line 720
    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndHourSpinner:Landroid/widget/Spinner;

    .line 722
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndHourSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 723
    const v0, 0x7f0d00f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndMinuteSpinner:Landroid/widget/Spinner;

    .line 724
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndMinuteSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 727
    const v0, 0x7f0d00eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartFormate:Landroid/widget/Spinner;

    .line 728
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartFormate:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 729
    const v0, 0x7f0d00f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndFormate:Landroid/widget/Spinner;

    .line 730
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndFormate:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 732
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPrefId:I

    if-nez v0, :cond_0

    .line 733
    iget-boolean v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->canShowTimerSetting:Z

    .line 732
    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "show time setting for cfut"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDay:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 736
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 737
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 738
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeSetting:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 739
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeSetting:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 740
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartHourSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 741
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndHourSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 743
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartMinuteSpinner:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeMinute:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 744
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndMinuteSpinner:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeMinute:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 746
    invoke-direct {p0}, Lcom/android/phone/EditPhoneNumberPreference;->is24Hour()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 747
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartHourSpinner:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeHour:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 748
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndHourSpinner:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeHour:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 755
    :goto_2
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDay:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/phone/EditPhoneNumberPreference;->onAllDayChecked(Z)V

    .line 757
    :cond_0
    return-void

    .line 712
    :cond_1
    const v0, 0x7f0d00e9

    goto/16 :goto_0

    .line 721
    :cond_2
    const v0, 0x7f0d00ef

    goto/16 :goto_1

    .line 750
    :cond_3
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartFormate:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 751
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndFormate:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 752
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeHour:I

    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartHourSpinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartFormate:Landroid/widget/Spinner;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/phone/EditPhoneNumberPreference;->showTimeWith12Formate(ILandroid/widget/Spinner;Landroid/widget/Spinner;)V

    .line 753
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeHour:I

    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndHourSpinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndFormate:Landroid/widget/Spinner;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/phone/EditPhoneNumberPreference;->showTimeWith12Formate(ILandroid/widget/Spinner;Landroid/widget/Spinner;)V

    goto :goto_2
.end method

.method private is24Hour()Z
    .locals 1

    .prologue
    .line 780
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private onAllDayChecked(Z)V
    .locals 4
    .param p1, "isChecked"    # Z

    .prologue
    const v1, -0x777778

    const/high16 v2, -0x1000000

    .line 760
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartHourSpinner:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 761
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartHourSpinner:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setClickable(Z)V

    .line 762
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartMinuteSpinner:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 763
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartMinuteSpinner:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setClickable(Z)V

    .line 764
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndHourSpinner:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 765
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndHourSpinner:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setClickable(Z)V

    .line 766
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndMinuteSpinner:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 767
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndMinuteSpinner:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setClickable(Z)V

    .line 768
    invoke-direct {p0}, Lcom/android/phone/EditPhoneNumberPreference;->is24Hour()Z

    move-result v0

    if-nez v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartFormate:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 770
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartFormate:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setClickable(Z)V

    .line 771
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndFormate:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 772
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndFormate:Landroid/widget/Spinner;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setClickable(Z)V

    .line 774
    :cond_0
    iget-object v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 775
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 776
    return-void

    :cond_1
    move v0, v2

    .line 774
    goto :goto_0

    :cond_2
    move v1, v2

    .line 775
    goto :goto_1
.end method

.method private setSelectionEndTimePreiod()V
    .locals 4

    .prologue
    .line 814
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndMinuteSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeMinute:I

    .line 816
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndHourSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v0, v2

    .line 818
    .local v0, "endhourposion":I
    invoke-direct {p0}, Lcom/android/phone/EditPhoneNumberPreference;->is24Hour()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 819
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndHourSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeHour:I

    .line 831
    :cond_0
    :goto_0
    return-void

    .line 821
    :cond_1
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeEndFormate:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeAMPM:I

    .line 823
    iget v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeAMPM:I

    if-nez v1, :cond_2

    .line 824
    iput v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeHour:I

    goto :goto_0

    .line 825
    :cond_2
    iget v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeAMPM:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 826
    add-int/lit8 v1, v0, 0xc

    iput v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeHour:I

    goto :goto_0
.end method

.method private setSelectionStartTimePreiod()V
    .locals 4

    .prologue
    .line 794
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartMinuteSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeMinute:I

    .line 796
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartHourSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v0, v2

    .line 798
    .local v0, "starthourposion":I
    invoke-direct {p0}, Lcom/android/phone/EditPhoneNumberPreference;->is24Hour()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 799
    iput v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeHour:I

    .line 809
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setSelectionTimePreiod, mStartTimeHour = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 810
    iget v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeHour:I

    .line 809
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 810
    const-string/jumbo v3, ", mStartTimeMinute = "

    .line 809
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 810
    iget v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeMinute:I

    .line 809
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    return-void

    .line 801
    :cond_1
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimeStartFormate:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeAPMP:I

    .line 802
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setSelectionTimePreiod, mStartTimeAPMP = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeAPMP:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    iget v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeAPMP:I

    if-nez v1, :cond_2

    .line 804
    iput v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeHour:I

    goto :goto_0

    .line 805
    :cond_2
    iget v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeAPMP:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 806
    add-int/lit8 v1, v0, 0xc

    iput v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeHour:I

    goto :goto_0
.end method

.method private showTimeWith12Formate(ILandroid/widget/Spinner;Landroid/widget/Spinner;)V
    .locals 1
    .param p1, "hour"    # I
    .param p2, "hourSpinner"    # Landroid/widget/Spinner;
    .param p3, "hourFormate"    # Landroid/widget/Spinner;

    .prologue
    const/16 v0, 0xc

    .line 784
    if-ge p1, v0, :cond_1

    .line 785
    invoke-virtual {p2, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 786
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 791
    :cond_0
    :goto_0
    return-void

    .line 787
    :cond_1
    if-lt p1, v0, :cond_0

    .line 788
    add-int/lit8 v0, p1, -0xc

    invoke-virtual {p2, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 789
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method


# virtual methods
.method public getEndTimeHour()I
    .locals 1

    .prologue
    .line 441
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeHour:I

    return v0
.end method

.method public getEndTimeMinute()I
    .locals 1

    .prologue
    .line 445
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeMinute:I

    return v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPhoneNumber:Ljava/lang/String;

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRawPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method protected getRawPhoneNumberWithTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 458
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStartTimeHour()I
    .locals 1

    .prologue
    .line 433
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeHour:I

    return v0
.end method

.method public getStartTimeMinute()I
    .locals 1

    .prologue
    .line 437
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeMinute:I

    return v0
.end method

.method protected getStringValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 662
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPrefId:I

    if-nez v0, :cond_1

    .line 663
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->isToggled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "1"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 664
    iget-object v1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodString:Ljava/lang/String;

    .line 663
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "0"

    goto :goto_0

    .line 666
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->isToggled()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "1"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string/jumbo v0, "0"

    goto :goto_1
.end method

.method public getSummaryOn()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mSummaryOn:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public isAllDayChecked()Z
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDay:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public isToggled()Z
    .locals 1

    .prologue
    .line 409
    iget-boolean v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mChecked:Z

    return v0
.end method

.method protected onAddEditTextToDialogView(Landroid/view/View;Landroid/widget/EditText;)V
    .locals 3
    .param p1, "dialogView"    # Landroid/view/View;
    .param p2, "editText"    # Landroid/widget/EditText;

    .prologue
    .line 289
    const v1, 0x7f0d00e4

    .line 288
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 292
    .local v0, "container":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 293
    const/4 v1, -0x1

    .line 294
    const/4 v2, -0x2

    .line 293
    invoke-virtual {v0, p2, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 296
    :cond_0
    return-void
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 237
    const/4 v2, -0x2

    iput v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mButtonClicked:I

    .line 239
    invoke-super {p0, p1}, Landroid/preference/EditTextPreference;->onBindDialogView(Landroid/view/View;)V

    .line 242
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    .line 244
    .local v1, "editText":Landroid/widget/EditText;
    const v2, 0x7f0d00e5

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mContactPickButton:Landroid/widget/ImageButton;

    .line 247
    if-eqz v1, :cond_1

    .line 250
    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mGetDefaultNumberListener:Lcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;

    if-eqz v2, :cond_0

    .line 251
    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mGetDefaultNumberListener:Lcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;

    invoke-interface {v2, p0}, Lcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;->onGetDefaultNumber(Lcom/android/phone/EditPhoneNumberPreference;)Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "defaultNumber":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 253
    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPhoneNumber:Ljava/lang/String;

    .line 256
    .end local v0    # "defaultNumber":Ljava/lang/String;
    :cond_0
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v2

    .line 257
    iget-object v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPhoneNumber:Ljava/lang/String;

    sget-object v4, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 256
    invoke-virtual {v2, v3, v4}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 258
    invoke-static {}, Landroid/text/method/ArrowKeyMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 259
    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 260
    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mDialogFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 264
    :cond_1
    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mContactPickButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    .line 265
    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mContactPickButton:Landroid/widget/ImageButton;

    new-instance v3, Lcom/android/phone/EditPhoneNumberPreference$1;

    invoke-direct {v3, p0}, Lcom/android/phone/EditPhoneNumberPreference$1;-><init>(Lcom/android/phone/EditPhoneNumberPreference;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/phone/EditPhoneNumberPreference;->initTimeSettingsView(Landroid/view/View;)V

    .line 276
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 201
    invoke-super {p0, p1}, Landroid/preference/EditTextPreference;->onBindView(Landroid/view/View;)V

    .line 204
    const v3, 0x1020010

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 205
    .local v1, "summaryView":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 210
    iget v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mConfirmationMode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 211
    iget-boolean v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mChecked:Z

    if-eqz v3, :cond_2

    .line 212
    iget-object v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mSummaryOn:Ljava/lang/CharSequence;

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    .line 220
    .local v0, "sum":Ljava/lang/CharSequence;
    :goto_0
    if-eqz v0, :cond_5

    .line 221
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    const/4 v2, 0x0

    .line 227
    .local v2, "vis":I
    :goto_1
    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 228
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 231
    .end local v0    # "sum":Ljava/lang/CharSequence;
    .end local v2    # "vis":I
    :cond_0
    return-void

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mSummaryOn:Ljava/lang/CharSequence;

    .restart local v0    # "sum":Ljava/lang/CharSequence;
    goto :goto_0

    .line 214
    .end local v0    # "sum":Ljava/lang/CharSequence;
    :cond_2
    iget-object v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mSummaryOff:Ljava/lang/CharSequence;

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    .restart local v0    # "sum":Ljava/lang/CharSequence;
    goto :goto_0

    .end local v0    # "sum":Ljava/lang/CharSequence;
    :cond_3
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mSummaryOff:Ljava/lang/CharSequence;

    .restart local v0    # "sum":Ljava/lang/CharSequence;
    goto :goto_0

    .line 217
    .end local v0    # "sum":Ljava/lang/CharSequence;
    :cond_4
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    .restart local v0    # "sum":Ljava/lang/CharSequence;
    goto :goto_0

    .line 224
    :cond_5
    const/16 v2, 0x8

    .restart local v2    # "vis":I
    goto :goto_1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 361
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mConfirmationMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, -0x3

    if-ne p2, v0, :cond_0

    .line 363
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->isToggled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/phone/EditPhoneNumberPreference;->setToggled(Z)Lcom/android/phone/EditPhoneNumberPreference;

    .line 366
    :cond_0
    iput p2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mButtonClicked:I

    .line 367
    invoke-super {p0, p1, p2}, Landroid/preference/EditTextPreference;->onClick(Landroid/content/DialogInterface;I)V

    .line 368
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 6
    .param p1, "positiveResult"    # Z

    .prologue
    .line 375
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mButtonClicked:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 376
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mButtonClicked:I

    const/4 v2, -0x3

    if-ne v0, v2, :cond_4

    .line 377
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "number":Ljava/lang/String;
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPrefId:I

    if-nez v0, :cond_3

    .line 379
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->isAllDayChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDayChecked:Z

    if-eqz v0, :cond_2

    .line 380
    invoke-virtual {p0, v1}, Lcom/android/phone/EditPhoneNumberPreference;->setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;

    .line 391
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/EditTextPreference;->onDialogClosed(Z)V

    .line 392
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getStringValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/phone/EditPhoneNumberPreference;->setText(Ljava/lang/String;)V

    .line 398
    .end local v1    # "number":Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mDialogOnClosedListener:Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;

    if-eqz v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mDialogOnClosedListener:Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;

    iget v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mButtonClicked:I

    invoke-interface {v0, p0, v2}, Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;->onDialogClosed(Lcom/android/phone/EditPhoneNumberPreference;I)V

    .line 401
    :cond_1
    return-void

    .line 382
    .restart local v1    # "number":Ljava/lang/String;
    :cond_2
    iget v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeHour:I

    .line 383
    iget v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartTimeMinute:I

    iget v4, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeHour:I

    iget v5, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndTimeMinute:I

    move-object v0, p0

    .line 382
    invoke-virtual/range {v0 .. v5}, Lcom/android/phone/EditPhoneNumberPreference;->setPhoneNumberWithTimePeriod(Ljava/lang/String;IIII)Lcom/android/phone/EditPhoneNumberPreference;

    goto :goto_0

    .line 388
    :cond_3
    invoke-virtual {p0, v1}, Lcom/android/phone/EditPhoneNumberPreference;->setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;

    goto :goto_0

    .line 394
    .end local v1    # "number":Ljava/lang/String;
    :cond_4
    invoke-super {p0, p1}, Landroid/preference/EditTextPreference;->onDialogClosed(Z)V

    goto :goto_1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 684
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 690
    .local v0, "selectedItem":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/phone/EditPhoneNumberPreference;->setSelectionStartTimePreiod()V

    .line 691
    invoke-direct {p0}, Lcom/android/phone/EditPhoneNumberPreference;->setSelectionEndTimePreiod()V

    .line 693
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 680
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public onPickActivityResult(Ljava/lang/String;)V
    .locals 1
    .param p1, "pickedValue"    # Ljava/lang/String;

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 352
    .local v0, "editText":Landroid/widget/EditText;
    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 355
    :cond_0
    return-void
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 3
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v2, 0x0

    .line 304
    iget v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mConfirmationMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 305
    iget-boolean v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mChecked:Z

    if-eqz v0, :cond_1

    .line 306
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mChangeNumberText:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 307
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mDisableText:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 314
    :cond_0
    :goto_0
    const v0, 0x7f030001

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 315
    return-void

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEnableText:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 310
    invoke-virtual {p1, v2, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1
    .param p1, "restoreValue"    # Z
    .param p2, "defaultValue"    # Ljava/lang/Object;

    .prologue
    .line 560
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getStringValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/phone/EditPhoneNumberPreference;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .end local p2    # "defaultValue":Ljava/lang/Object;
    :goto_0
    invoke-virtual {p0, p2}, Lcom/android/phone/EditPhoneNumberPreference;->setValueFromString(Ljava/lang/String;)V

    .line 562
    return-void

    .line 561
    .restart local p2    # "defaultValue":Ljava/lang/Object;
    :cond_0
    check-cast p2, Ljava/lang/String;

    goto :goto_0
.end method

.method protected persistString(Ljava/lang/String;)Z
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 593
    iput-object p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEncodedText:Ljava/lang/String;

    .line 594
    invoke-super {p0, p1}, Landroid/preference/EditTextPreference;->persistString(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setAllDayCheckBox(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 474
    iput-boolean p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDayChecked:Z

    .line 475
    return-void
.end method

.method public setDialogOnClosedListener(Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;

    .prologue
    .line 328
    iput-object p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mDialogOnClosedListener:Lcom/android/phone/EditPhoneNumberPreference$OnDialogClosedListener;

    .line 329
    return-void
.end method

.method public setParentActivity(Landroid/app/Activity;I)V
    .locals 1
    .param p1, "parent"    # Landroid/app/Activity;
    .param p2, "identifier"    # I

    .prologue
    .line 333
    iput-object p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mParentActivity:Landroid/app/Activity;

    .line 334
    iput p2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPrefId:I

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/EditPhoneNumberPreference;->mGetDefaultNumberListener:Lcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;

    .line 336
    return-void
.end method

.method public setParentActivity(Landroid/app/Activity;ILcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;)V
    .locals 0
    .param p1, "parent"    # Landroid/app/Activity;
    .param p2, "identifier"    # I
    .param p3, "l"    # Lcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mParentActivity:Landroid/app/Activity;

    .line 342
    iput p2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPrefId:I

    .line 343
    iput-object p3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mGetDefaultNumberListener:Lcom/android/phone/EditPhoneNumberPreference$GetDefaultNumberListener;

    .line 344
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;
    .locals 1
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 464
    iput-object p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPhoneNumber:Ljava/lang/String;

    .line 465
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getStringValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/phone/EditPhoneNumberPreference;->setText(Ljava/lang/String;)V

    .line 466
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->notifyChanged()V

    .line 468
    return-object p0
.end method

.method public setPhoneNumberWithTimePeriod(Ljava/lang/String;IIII)Lcom/android/phone/EditPhoneNumberPreference;
    .locals 1
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "starthour"    # I
    .param p3, "startminute"    # I
    .param p4, "endhour"    # I
    .param p5, "endminute"    # I

    .prologue
    .line 488
    iput-object p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPhoneNumber:Ljava/lang/String;

    .line 489
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getStringValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/phone/EditPhoneNumberPreference;->setText(Ljava/lang/String;)V

    .line 490
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/android/phone/EditPhoneNumberPreference;->setTimePeriodInfo(IIII)V

    .line 491
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->notifyChanged()V

    .line 493
    return-object p0
.end method

.method public setPhoneNumberWithTimePeriod(Ljava/lang/String;Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;
    .locals 1
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "timeperiod"    # Ljava/lang/String;

    .prologue
    .line 545
    iput-object p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPhoneNumber:Ljava/lang/String;

    .line 546
    iput-object p2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodString:Ljava/lang/String;

    .line 547
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getStringValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/phone/EditPhoneNumberPreference;->setText(Ljava/lang/String;)V

    .line 548
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->notifyChanged()V

    .line 550
    return-object p0
.end method

.method public setSummaryOn(Ljava/lang/CharSequence;)Lcom/android/phone/EditPhoneNumberPreference;
    .locals 1
    .param p1, "summary"    # Ljava/lang/CharSequence;

    .prologue
    .line 603
    iput-object p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mSummaryOn:Ljava/lang/CharSequence;

    .line 604
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->isToggled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->notifyChanged()V

    .line 607
    :cond_0
    return-object p0
.end method

.method public setTimePeriodInfo(IIII)V
    .locals 5
    .param p1, "starthour"    # I
    .param p2, "startminute"    # I
    .param p3, "endhour"    # I
    .param p4, "endminute"    # I

    .prologue
    .line 502
    iput p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeHour:I

    .line 503
    iput p2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeMinute:I

    .line 504
    iput p3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeHour:I

    .line 505
    iput p4, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeMinute:I

    .line 506
    iget-boolean v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodAllDayChecked:Z

    if-eqz v2, :cond_1

    .line 507
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b033f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodString:Ljava/lang/String;

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 509
    :cond_1
    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mStartDate:Ljava/util/Calendar;

    .line 510
    iget v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeHour:I

    iget v4, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeMinute:I

    .line 509
    invoke-direct {p0, v2, v3, v4}, Lcom/android/phone/EditPhoneNumberPreference;->formateTime(Ljava/util/Calendar;II)Ljava/lang/String;

    move-result-object v1

    .line 511
    .local v1, "fomatedStartTimeString":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEndDate:Ljava/util/Calendar;

    .line 512
    iget v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeHour:I

    iget v4, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeMinute:I

    .line 511
    invoke-direct {p0, v2, v3, v4}, Lcom/android/phone/EditPhoneNumberPreference;->formateTime(Ljava/util/Calendar;II)Ljava/lang/String;

    move-result-object v0

    .line 513
    .local v0, "fomatedEndTimeString":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0341

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 515
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0344

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 513
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodString:Ljava/lang/String;

    .line 517
    iget v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeHour:I

    mul-int/lit8 v2, v2, 0x3c

    iget v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidEndTimeMinute:I

    add-int/2addr v2, v3

    .line 518
    iget v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeHour:I

    mul-int/lit8 v3, v3, 0x3c

    iget v4, p0, Lcom/android/phone/EditPhoneNumberPreference;->mValidStartTimeMinute:I

    add-int/2addr v3, v4

    .line 517
    if-ge v2, v3, :cond_0

    .line 519
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 520
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0345

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 519
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mTimePeriodString:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public setTimeSettingVisibility(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 478
    iput-boolean p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->canShowTimerSetting:Z

    .line 479
    return-void
.end method

.method public setToggled(Z)Lcom/android/phone/EditPhoneNumberPreference;
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 415
    iput-boolean p1, p0, Lcom/android/phone/EditPhoneNumberPreference;->mChecked:Z

    .line 416
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->getStringValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/phone/EditPhoneNumberPreference;->setText(Ljava/lang/String;)V

    .line 417
    invoke-virtual {p0}, Lcom/android/phone/EditPhoneNumberPreference;->notifyChanged()V

    .line 419
    return-object p0
.end method

.method protected setValueFromString(Ljava/lang/String;)V
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 651
    const-string/jumbo v1, ":"

    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 652
    .local v0, "inValues":[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const-string/jumbo v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/phone/EditPhoneNumberPreference;->setToggled(Z)Lcom/android/phone/EditPhoneNumberPreference;

    .line 653
    array-length v1, v0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 654
    aget-object v1, v0, v3

    aget-object v2, v0, v4

    invoke-virtual {p0, v1, v2}, Lcom/android/phone/EditPhoneNumberPreference;->setPhoneNumberWithTimePeriod(Ljava/lang/String;Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;

    .line 658
    :goto_0
    return-void

    .line 656
    :cond_0
    aget-object v1, v0, v3

    invoke-virtual {p0, v1}, Lcom/android/phone/EditPhoneNumberPreference;->setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;

    goto :goto_0
.end method

.method public shouldDisableDependents()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 576
    const/4 v1, 0x0

    .line 577
    .local v1, "shouldDisable":Z
    iget v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mConfirmationMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEncodedText:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 578
    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mEncodedText:Ljava/lang/String;

    const-string/jumbo v3, ":"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 579
    .local v0, "inValues":[Ljava/lang/String;
    aget-object v2, v0, v5

    const-string/jumbo v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 583
    .end local v0    # "inValues":[Ljava/lang/String;
    .end local v1    # "shouldDisable":Z
    :goto_0
    return v1

    .line 581
    .restart local v1    # "shouldDisable":Z
    :cond_0
    iget-object v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mPhoneNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/phone/EditPhoneNumberPreference;->mConfirmationMode:I

    if-nez v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
