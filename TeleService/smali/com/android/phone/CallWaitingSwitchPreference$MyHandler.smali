.class Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;
.super Landroid/os/Handler;
.source "CallWaitingSwitchPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/CallWaitingSwitchPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CallWaitingSwitchPreference;


# direct methods
.method private constructor <init>(Lcom/android/phone/CallWaitingSwitchPreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/CallWaitingSwitchPreference;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/CallWaitingSwitchPreference;Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/CallWaitingSwitchPreference;
    .param p2, "-this1"    # Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;-><init>(Lcom/android/phone/CallWaitingSwitchPreference;)V

    return-void
.end method

.method private handleGetCallWaitingResponse(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 78
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 80
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v5, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-static {v5}, Lcom/android/phone/CallWaitingSwitchPreference;->-get1(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/phone/TimeConsumingPreferenceListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 81
    iget v5, p1, Landroid/os/Message;->arg2:I

    if-ne v5, v3, :cond_2

    .line 82
    iget-object v5, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-static {v5}, Lcom/android/phone/CallWaitingSwitchPreference;->-get1(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/phone/TimeConsumingPreferenceListener;

    move-result-object v5

    iget-object v6, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-interface {v5, v6, v4}, Lcom/android/phone/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    .line 88
    :cond_0
    :goto_0
    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_3

    .line 93
    iget-object v3, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-static {v3}, Lcom/android/phone/CallWaitingSwitchPreference;->-get1(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/phone/TimeConsumingPreferenceListener;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 94
    iget-object v3, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-static {v3}, Lcom/android/phone/CallWaitingSwitchPreference;->-get1(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/phone/TimeConsumingPreferenceListener;

    move-result-object v4

    iget-object v5, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    .line 95
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v3, Lcom/android/internal/telephony/CommandException;

    .line 94
    invoke-interface {v4, v5, v3}, Lcom/android/phone/TimeConsumingPreferenceListener;->onException(Landroid/preference/Preference;Lcom/android/internal/telephony/CommandException;)V

    .line 120
    :cond_1
    :goto_1
    return-void

    .line 84
    :cond_2
    iget-object v5, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-static {v5}, Lcom/android/phone/CallWaitingSwitchPreference;->-get1(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/phone/TimeConsumingPreferenceListener;

    move-result-object v5

    iget-object v6, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-interface {v5, v6, v3}, Lcom/android/phone/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    goto :goto_0

    .line 97
    :cond_3
    iget-object v5, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    instance-of v5, v5, Ljava/lang/Throwable;

    if-nez v5, :cond_4

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_5

    .line 102
    :cond_4
    iget-object v3, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-static {v3}, Lcom/android/phone/CallWaitingSwitchPreference;->-get1(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/phone/TimeConsumingPreferenceListener;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 103
    iget-object v3, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-static {v3}, Lcom/android/phone/CallWaitingSwitchPreference;->-get1(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/phone/TimeConsumingPreferenceListener;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    const/16 v5, 0x190

    invoke-interface {v3, v4, v5}, Lcom/android/phone/TimeConsumingPreferenceListener;->onError(Landroid/preference/Preference;I)V

    goto :goto_1

    .line 109
    :cond_5
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, [I

    .line 114
    .local v1, "cwArray":[I
    :try_start_0
    iget-object v5, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    const/4 v6, 0x0

    aget v6, v1, v6

    if-ne v6, v3, :cond_6

    const/4 v6, 0x1

    aget v6, v1, v6

    and-int/lit8 v6, v6, 0x1

    if-ne v6, v3, :cond_6

    :goto_2
    invoke-virtual {v5, v3}, Lcom/android/phone/CallWaitingSwitchPreference;->setChecked(Z)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 115
    :catch_0
    move-exception v2

    .line 116
    .local v2, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v3, "CallWaitingSwitchPreference"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "handleGetCallWaitingResponse: improper result: err ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 117
    invoke-virtual {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 116
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .end local v2    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_6
    move v3, v4

    .line 114
    goto :goto_2
.end method

.method private handleSetCallWaitingResponse(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 123
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 125
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 133
    iget-object v1, p0, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->this$0:Lcom/android/phone/CallWaitingSwitchPreference;

    invoke-static {v1}, Lcom/android/phone/CallWaitingSwitchPreference;->-get0(Lcom/android/phone/CallWaitingSwitchPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 134
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 133
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v4, v4, v2}, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/Phone;->getCallWaiting(Landroid/os/Message;)V

    .line 135
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 67
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 75
    :goto_0
    return-void

    .line 69
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->handleGetCallWaitingResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 72
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/phone/CallWaitingSwitchPreference$MyHandler;->handleSetCallWaitingResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
