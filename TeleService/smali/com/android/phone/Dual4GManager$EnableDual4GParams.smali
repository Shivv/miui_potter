.class Lcom/android/phone/Dual4GManager$EnableDual4GParams;
.super Ljava/lang/Object;
.source "Dual4GManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/Dual4GManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EnableDual4GParams"
.end annotation


# instance fields
.field callback:Lcom/android/phone/Dual4GManager$CallBack;

.field enabled:Z

.field final synthetic this$0:Lcom/android/phone/Dual4GManager;


# direct methods
.method constructor <init>(Lcom/android/phone/Dual4GManager;ZLcom/android/phone/Dual4GManager$CallBack;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/Dual4GManager;
    .param p2, "enabled"    # Z
    .param p3, "callback"    # Lcom/android/phone/Dual4GManager$CallBack;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->this$0:Lcom/android/phone/Dual4GManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-boolean p2, p0, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->enabled:Z

    .line 58
    iput-object p3, p0, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->callback:Lcom/android/phone/Dual4GManager$CallBack;

    .line 59
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 64
    const-string/jumbo v1, ", callback="

    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->callback:Lcom/android/phone/Dual4GManager$CallBack;

    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 64
    const-string/jumbo v1, "}"

    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
