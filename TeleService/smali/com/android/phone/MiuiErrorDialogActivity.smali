.class public Lcom/android/phone/MiuiErrorDialogActivity;
.super Lmiui/app/Activity;
.source "MiuiErrorDialogActivity.java"


# direct methods
.method static synthetic -wrap0(Lcom/android/phone/MiuiErrorDialogActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/MiuiErrorDialogActivity;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/MiuiErrorDialogActivity;->isActivityFront(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method private isActivityFront(Ljava/lang/String;)Z
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 144
    const-string/jumbo v2, "activity"

    invoke-virtual {p0, v2}, Lcom/android/phone/MiuiErrorDialogActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 145
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 146
    .local v1, "tastList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/android/phone/MiuiErrorDialogActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 147
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 146
    if-eqz v2, :cond_0

    .line 148
    return v5

    .line 150
    :cond_0
    return v4
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 155
    invoke-super {p0}, Lmiui/app/Activity;->finish()V

    .line 158
    invoke-virtual {p0, v0, v0}, Lcom/android/phone/MiuiErrorDialogActivity;->overridePendingTransition(II)V

    .line 159
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lcom/android/phone/MiuiErrorDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_0

    .line 36
    return-void

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/MiuiErrorDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "dialog_type"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 40
    .local v0, "dialogType":I
    const-string/jumbo v1, "MiuiErrorDialogActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "dialogType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    if-eqz v0, :cond_1

    .line 42
    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiErrorDialogActivity;->showDialog(I)V

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/MiuiErrorDialogActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .param p1, "id"    # I

    .prologue
    const v5, 0x104000a

    const/4 v6, 0x0

    .line 50
    const/4 v0, 0x0

    .line 51
    .local v0, "builder":Lmiui/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/android/phone/MiuiErrorDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "dialog_messaga_id"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 52
    .local v1, "messageId":I
    invoke-virtual {p0}, Lcom/android/phone/MiuiErrorDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "show_negative"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 53
    .local v2, "showNegative":Z
    packed-switch p1, :pswitch_data_0

    .line 139
    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v3

    return-object v3

    .line 55
    :pswitch_0
    new-instance v3, Lmiui/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 56
    const v4, 0x7f0b06e6

    .line 55
    invoke-virtual {v3, v4}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    .line 57
    invoke-virtual {p0, v5}, Lcom/android/phone/MiuiErrorDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 58
    new-instance v5, Lcom/android/phone/MiuiErrorDialogActivity$1;

    invoke-direct {v5, p0}, Lcom/android/phone/MiuiErrorDialogActivity$1;-><init>(Lcom/android/phone/MiuiErrorDialogActivity;)V

    .line 55
    invoke-virtual {v3, v4, v5}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    .line 65
    .local v0, "builder":Lmiui/app/AlertDialog$Builder;
    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v3

    return-object v3

    .line 67
    .local v0, "builder":Lmiui/app/AlertDialog$Builder;
    :pswitch_1
    new-instance v3, Lmiui/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 68
    const v4, 0x7f0b0707

    .line 67
    invoke-virtual {v3, v4}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    .line 70
    new-instance v4, Lcom/android/phone/MiuiErrorDialogActivity$2;

    invoke-direct {v4, p0}, Lcom/android/phone/MiuiErrorDialogActivity$2;-><init>(Lcom/android/phone/MiuiErrorDialogActivity;)V

    .line 69
    const v5, 0x7f0b06f7

    .line 67
    invoke-virtual {v3, v5, v4}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    .line 82
    new-instance v4, Lcom/android/phone/MiuiErrorDialogActivity$3;

    invoke-direct {v4, p0}, Lcom/android/phone/MiuiErrorDialogActivity$3;-><init>(Lcom/android/phone/MiuiErrorDialogActivity;)V

    .line 81
    const v5, 0x7f0b06f8

    .line 67
    invoke-virtual {v3, v5, v4}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    .line 89
    .local v0, "builder":Lmiui/app/AlertDialog$Builder;
    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v3

    return-object v3

    .line 91
    .local v0, "builder":Lmiui/app/AlertDialog$Builder;
    :pswitch_2
    new-instance v3, Lmiui/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 93
    new-instance v4, Lcom/android/phone/MiuiErrorDialogActivity$4;

    invoke-direct {v4, p0, v1}, Lcom/android/phone/MiuiErrorDialogActivity$4;-><init>(Lcom/android/phone/MiuiErrorDialogActivity;I)V

    .line 91
    invoke-virtual {v3, v5, v4}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    move-result-object v0

    .line 128
    .local v0, "builder":Lmiui/app/AlertDialog$Builder;
    if-eqz v2, :cond_0

    .line 130
    new-instance v3, Lcom/android/phone/MiuiErrorDialogActivity$5;

    invoke-direct {v3, p0}, Lcom/android/phone/MiuiErrorDialogActivity$5;-><init>(Lcom/android/phone/MiuiErrorDialogActivity;)V

    .line 129
    const/high16 v4, 0x1040000

    invoke-virtual {v0, v4, v3}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    .line 137
    :cond_0
    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v3

    return-object v3

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
