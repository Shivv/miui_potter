.class public Lcom/android/phone/Assert;
.super Ljava/lang/Object;
.source "Assert.java"


# static fields
.field private static sIsMainThreadForTest:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fail(Ljava/lang/String;)V
    .locals 1
    .param p0, "reason"    # Ljava/lang/String;

    .prologue
    .line 56
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static isMainThread()V
    .locals 2

    .prologue
    .line 36
    sget-object v0, Lcom/android/phone/Assert;->sIsMainThreadForTest:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 37
    sget-object v0, Lcom/android/phone/Assert;->sIsMainThreadForTest:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Lcom/android/phone/Assert;->isTrue(Z)V

    .line 38
    return-void

    .line 40
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Looper;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/android/phone/Assert;->isTrue(Z)V

    .line 41
    return-void
.end method

.method public static isTrue(Z)V
    .locals 2
    .param p0, "condition"    # Z

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 31
    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Expected condition to be true"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 33
    :cond_0
    return-void
.end method

.method public static setIsMainThreadForTesting(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "isMainThread"    # Ljava/lang/Boolean;

    .prologue
    .line 64
    sput-object p0, Lcom/android/phone/Assert;->sIsMainThreadForTest:Ljava/lang/Boolean;

    .line 65
    return-void
.end method
