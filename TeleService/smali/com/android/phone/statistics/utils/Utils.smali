.class public Lcom/android/phone/statistics/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static DEBUG_ALL:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/phone/statistics/utils/Utils;->DEBUG_ALL:Z

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertEventToBitIndex(Ljava/lang/String;)I
    .locals 1
    .param p0, "event"    # Ljava/lang/String;

    .prologue
    .line 133
    const-string/jumbo v0, "call_unexpected_disconnected"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x1

    return v0

    .line 135
    :cond_0
    const-string/jumbo v0, "data_setup_call_error"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    const/4 v0, 0x2

    return v0

    .line 137
    :cond_1
    const-string/jumbo v0, "data_fake_connection"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    const/4 v0, 0x3

    return v0

    .line 139
    :cond_2
    const-string/jumbo v0, "nw_out_of_service"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 140
    const/4 v0, 0x4

    return v0

    .line 141
    :cond_3
    const-string/jumbo v0, "nw_without_4g"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 142
    const/4 v0, 0x5

    return v0

    .line 144
    :cond_4
    const/4 v0, -0x1

    return v0
.end method

.method public static hasRuleEvent(II)Z
    .locals 3
    .param p0, "ruleEvent"    # I
    .param p1, "ruleEventId"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149
    shl-int v2, v0, p1

    and-int/2addr v2, p0

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isAnyValidRuleEventExist(I)Z
    .locals 4
    .param p0, "ruleEvent"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 161
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 162
    shl-int v1, v3, v0

    and-int/2addr v1, p0

    if-eqz v1, :cond_0

    .line 163
    return v3

    .line 161
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    :cond_1
    return v2
.end method

.method public static isNeedToRevokeUserPermission(J)Z
    .locals 4
    .param p0, "lastTime"    # J

    .prologue
    .line 170
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p0

    const-wide/32 v2, 0x240c8400

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 171
    const/4 v0, 0x1

    return v0

    .line 173
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static isPermanentUserDisagree(I)Z
    .locals 1
    .param p0, "ruleEvent"    # I

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/phone/statistics/utils/Utils;->hasRuleEvent(II)Z

    move-result v0

    return v0
.end method

.method public static isUserExperienceProgramEnabled(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 109
    const/4 v0, 0x0

    .line 110
    .local v0, "enabled":Z
    if-eqz p0, :cond_0

    .line 111
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 112
    const-string/jumbo v2, "upload_log_pref"

    .line 111
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    .line 114
    :cond_0
    :goto_0
    const-string/jumbo v1, "Utils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "isUserExperienceProgramEnabled() enabled:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return v0

    .line 111
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidParams(Ljava/util/Map;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 128
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 129
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    const/16 v2, 0xa

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    .line 128
    :cond_0
    return v0
.end method

.method public static logD(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 181
    sget-boolean v0, Lcom/android/phone/statistics/utils/Utils;->DEBUG_ALL:Z

    if-eqz v0, :cond_0

    .line 182
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_0
    return-void
.end method

.method public static logE(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 195
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return-void
.end method

.method public static logI(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 187
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    return-void
.end method

.method public static logW(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 191
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    return-void
.end method

.method public static setDebugAll(Z)V
    .locals 0
    .param p0, "open"    # Z

    .prologue
    .line 177
    sput-boolean p0, Lcom/android/phone/statistics/utils/Utils;->DEBUG_ALL:Z

    .line 178
    return-void
.end method
