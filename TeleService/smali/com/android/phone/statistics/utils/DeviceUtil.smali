.class public Lcom/android/phone/statistics/utils/DeviceUtil;
.super Ljava/lang/Object;
.source "DeviceUtil.java"


# static fields
.field private static final ChosenModel:[Ljava/lang/String;

.field public static final IS_ALPHA_VERSION:Z

.field public static final IS_CTA_BUILD:Z

.field public static final IS_CTS_BUILD:Z

.field public static final IS_DEV_VERSION:Z

.field public static final IS_INTERNATIONAL_BUILD:Z

.field public static final IS_STABLE_VERSION:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    .line 24
    sput-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_INTERNATIONAL_BUILD:Z

    .line 28
    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    .line 27
    sput-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_STABLE_VERSION:Z

    .line 31
    sget-boolean v0, Lmiui/os/Build;->IS_ALPHA_BUILD:Z

    .line 30
    sput-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_ALPHA_VERSION:Z

    .line 34
    sget-boolean v0, Lmiui/os/Build;->IS_DEVELOPMENT_VERSION:Z

    .line 33
    sput-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_DEV_VERSION:Z

    .line 37
    sget-boolean v0, Lmiui/os/Build;->IS_CTA_BUILD:Z

    .line 36
    sput-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_CTA_BUILD:Z

    .line 40
    sget-boolean v0, Lmiui/os/Build;->IS_CTS_BUILD:Z

    .line 39
    sput-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_CTS_BUILD:Z

    .line 42
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    .line 43
    const-string/jumbo v1, "sagit"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 44
    const-string/jumbo v1, "tiffany"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 45
    const-string/jumbo v1, "mido"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 46
    const-string/jumbo v1, "chiron"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 47
    const-string/jumbo v1, "jason"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 48
    const-string/jumbo v1, "ugg"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 49
    const-string/jumbo v1, "ugglite"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 50
    const-string/jumbo v1, "gemini"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 42
    sput-object v0, Lcom/android/phone/statistics/utils/DeviceUtil;->ChosenModel:[Ljava/lang/String;

    .line 17
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getVersionType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    const-string/jumbo v0, "UNKNOWN"

    .line 95
    .local v0, "versionType":Ljava/lang/String;
    sget-boolean v1, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_ALPHA_VERSION:Z

    if-eqz v1, :cond_1

    .line 96
    const-string/jumbo v0, "ALPHA"

    .line 102
    :cond_0
    :goto_0
    return-object v0

    .line 97
    :cond_1
    sget-boolean v1, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_DEV_VERSION:Z

    if-eqz v1, :cond_2

    .line 98
    const-string/jumbo v0, "DEV"

    goto :goto_0

    .line 99
    :cond_2
    sget-boolean v1, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_STABLE_VERSION:Z

    if-eqz v1, :cond_0

    .line 100
    const-string/jumbo v0, "STABLE"

    goto :goto_0
.end method

.method private static isChosenModel()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 106
    sget-object v3, Lcom/android/phone/statistics/utils/DeviceUtil;->ChosenModel:[Ljava/lang/String;

    array-length v4, v3

    move v1, v2

    .local v0, "cm":Ljava/lang/String;
    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 107
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 108
    const/4 v1, 0x1

    return v1

    .line 106
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 111
    :cond_1
    return v2
.end method

.method public static isLegalReleaseVersion()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 74
    sget-boolean v0, Lcom/android/phone/statistics/utils/Utils;->DEBUG_ALL:Z

    if-eqz v0, :cond_0

    .line 75
    return v3

    .line 77
    :cond_0
    const-string/jumbo v0, "user"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    return v2

    .line 79
    :cond_1
    const-string/jumbo v0, "release-keys"

    sget-object v1, Landroid/os/Build;->TAGS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 80
    return v2

    .line 82
    :cond_2
    return v3
.end method

.method public static isQcomPlatform()Z
    .locals 1

    .prologue
    .line 86
    sget-boolean v0, Lmiui/telephony/MiuiTelephony;->IS_QCOM:Z

    return v0
.end method

.method public static isValidVersion()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    sget-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_CTA_BUILD:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_CTS_BUILD:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_1

    .line 55
    :cond_0
    return v2

    .line 57
    :cond_1
    sget-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_ALPHA_VERSION:Z

    if-eqz v0, :cond_2

    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_5

    .line 59
    return v2

    .line 61
    :cond_2
    sget-boolean v0, Lcom/android/phone/statistics/utils/DeviceUtil;->IS_DEV_VERSION:Z

    if-eqz v0, :cond_4

    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_3

    invoke-static {}, Lcom/android/phone/statistics/utils/DeviceUtil;->isChosenModel()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    .line 63
    :cond_3
    return v2

    .line 67
    :cond_4
    return v2

    .line 69
    :cond_5
    const/4 v0, 0x1

    return v0
.end method
