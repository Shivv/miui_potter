.class final enum Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;
.super Ljava/lang/Enum;
.source "MiuiTelephonyStatistic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/statistics/MiuiTelephonyStatistic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "OOSStep"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

.field public static final enum CAMP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

.field public static final enum DROP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

.field public static final enum INIT:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

.field public static final enum POWEROFF:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

.field public static final enum RESUME:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 143
    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    const-string/jumbo v1, "INIT"

    invoke-direct {v0, v1, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->INIT:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    const-string/jumbo v1, "CAMP"

    invoke-direct {v0, v1, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->CAMP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    const-string/jumbo v1, "DROP"

    invoke-direct {v0, v1, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->DROP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    const-string/jumbo v1, "RESUME"

    invoke-direct {v0, v1, v5}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->RESUME:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    const-string/jumbo v1, "POWEROFF"

    invoke-direct {v0, v1, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->POWEROFF:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    .line 142
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->INIT:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->CAMP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->DROP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->RESUME:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->POWEROFF:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->$VALUES:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 142
    const-class v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    return-object v0
.end method

.method public static values()[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->$VALUES:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    return-object v0
.end method
