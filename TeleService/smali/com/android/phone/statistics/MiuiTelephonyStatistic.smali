.class public Lcom/android/phone/statistics/MiuiTelephonyStatistic;
.super Ljava/lang/Object;
.source "MiuiTelephonyStatistic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;,
        Lcom/android/phone/statistics/MiuiTelephonyStatistic$2;,
        Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;,
        Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;,
        Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;,
        Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;
    }
.end annotation


# static fields
.field private static final synthetic -miui-telephony-TelephonyStatAdapter$DataStatTypeSwitchesValues:[I

.field private static DBG:Z

.field private static mTelephonyCallBack:Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;

.field private static sInstance:Lcom/android/phone/statistics/MiuiTelephonyStatistic;


# instance fields
.field MdlogCallBack:Lcom/android/phone/statistics/ui/ModemLogDialog$DialogCallBack;

.field private mCellLocation:[Landroid/telephony/CellLocation;

.field private mContext:Landroid/content/Context;

.field private mDataConnectionStartTime:J

.field private mDataConnectionStateDuration:J

.field private mDataSlot:I

.field private mForegroundCallState:[I

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsAirPlaneMode:Z

.field private mLastNetworkClassTime:[J

.field private mLastPlmnWithNetworkClass:[Ljava/lang/String;

.field private mLastPlmnWithServiceState:[Ljava/lang/String;

.field private mLastServiceStateTime:[J

.field private mLastVoLTERegisteredPlmn:[Ljava/lang/String;

.field private mLastVoLTERegisteredTime:[J

.field private mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNwOosCount:[I

.field private mNwOosStartTime:[J

.field private mNwWithout4GCount:[I

.field private mNwWithout4GStartTime:[J

.field private mOldDataConnectionState:I

.field private mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

.field private mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

.field private mServiceState:[Landroid/telephony/ServiceState;

.field private mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSignalStrength:[Landroid/telephony/SignalStrength;

.field private mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

.field mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mUserPermissionDialog:Landroid/app/Dialog;

.field private mVoLTERegisteredDuration:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Landroid/content/Context;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .prologue
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)[J
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .prologue
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastNetworkClassTime:[J

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)[Landroid/telephony/ServiceState;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .prologue
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .prologue
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    return-object v0
.end method

.method private static synthetic -getmiui-telephony-TelephonyStatAdapter$DataStatTypeSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-miui-telephony-TelephonyStatAdapter$DataStatTypeSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-miui-telephony-TelephonyStatAdapter$DataStatTypeSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->values()[Lmiui/telephony/TelephonyStatAdapter$DataStatType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->DATA_SETUP_FAIL_CN:Lmiui/telephony/TelephonyStatAdapter$DataStatType;

    invoke-virtual {v1}, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    sget-object v1, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->DATA_SETUP_OK_CN:Lmiui/telephony/TelephonyStatAdapter$DataStatType;

    invoke-virtual {v1}, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    sget-object v1, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->FAKE_CONNECTION_CN:Lmiui/telephony/TelephonyStatAdapter$DataStatType;

    invoke-virtual {v1}, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    sput-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-miui-telephony-TelephonyStatAdapter$DataStatTypeSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataSlot:I

    return p1
.end method

.method static synthetic -set1(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mIsAirPlaneMode:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->init()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Lcom/android/internal/telephony/Connection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "conn"    # Lcom/android/internal/telephony/Connection;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processCallDisconnectEvent(Lcom/android/internal/telephony/Connection;)V

    return-void
.end method

.method static synthetic -wrap10(Lcom/android/phone/statistics/MiuiTelephonyStatistic;II)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "phoneId"    # I
    .param p2, "foregroundCallState"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processPreciseCallStateEvent(II)V

    return-void
.end method

.method static synthetic -wrap11(Lcom/android/phone/statistics/MiuiTelephonyStatistic;ILandroid/telephony/ServiceState;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "phoneId"    # I
    .param p2, "ss"    # Landroid/telephony/ServiceState;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processServiceStateChangeEvent(ILandroid/telephony/ServiceState;)V

    return-void
.end method

.method static synthetic -wrap12(Lcom/android/phone/statistics/MiuiTelephonyStatistic;ILandroid/telephony/SignalStrength;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "phoneId"    # I
    .param p2, "signal"    # Landroid/telephony/SignalStrength;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processSignalStrengthEvent(ILandroid/telephony/SignalStrength;)V

    return-void
.end method

.method static synthetic -wrap13(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Landroid/content/Intent;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processSimStateChanged(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic -wrap14(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "ruleEventId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processUserUploadMdlogEvent(I)V

    return-void
.end method

.method static synthetic -wrap15(Lcom/android/phone/statistics/MiuiTelephonyStatistic;IZZ)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "phoneId"    # I
    .param p2, "status"    # Z
    .param p3, "force"    # Z

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processVoLTERegisteredStateChange(IZZ)V

    return-void
.end method

.method static synthetic -wrap16(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Landroid/telephony/ServiceState;IZ)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "ss"    # Landroid/telephony/ServiceState;
    .param p2, "phoneId"    # I
    .param p3, "force"    # Z

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordNetworkClassDuration(Landroid/telephony/ServiceState;IZ)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/statistics/MiuiTelephonyStatistic;ILandroid/telephony/CellLocation;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "phoneId"    # I
    .param p2, "location"    # Landroid/telephony/CellLocation;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processCellLocationChangeEvent(ILandroid/telephony/CellLocation;)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;II)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "phoneId"    # I
    .param p2, "dataState"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processDataDuration(II)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/phone/statistics/MiuiTelephonyStatistic;IILjava/util/HashMap;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "dataType"    # I
    .param p2, "phoneId"    # I
    .param p3, "params"    # Ljava/util/HashMap;

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processDataStatisticChange(IILjava/util/HashMap;)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "debugAll"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processDebugAll(Z)V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "ruleEventId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processMdlogReportAgree(I)V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processMdlogReportDisagree()V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "phoneId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processNwOosStatEvent(I)V

    return-void
.end method

.method static synthetic -wrap9(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p1, "phoneId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processNwWithout4GStatEvent(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    .line 106
    sput-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->sInstance:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .line 176
    sput-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mTelephonyCallBack:Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;

    .line 101
    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    .line 109
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [Landroid/telephony/ServiceState;

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    .line 110
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [Landroid/telephony/CellLocation;

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mCellLocation:[Landroid/telephony/CellLocation;

    .line 111
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [Landroid/telephony/SignalStrength;

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mSignalStrength:[Landroid/telephony/SignalStrength;

    .line 118
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    .line 120
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastServiceStateTime:[J

    .line 122
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastPlmnWithServiceState:[Ljava/lang/String;

    .line 127
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    .line 129
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastNetworkClassTime:[J

    .line 131
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastPlmnWithNetworkClass:[Ljava/lang/String;

    .line 134
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredTime:[J

    .line 135
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredPlmn:[Ljava/lang/String;

    .line 136
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mVoLTERegisteredDuration:Ljava/util/concurrent/ConcurrentHashMap;

    .line 139
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    .line 145
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    .line 161
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosCount:[I

    .line 162
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosStartTime:[J

    .line 164
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GCount:[I

    .line 165
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GStartTime:[J

    .line 167
    iput v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOldDataConnectionState:I

    .line 168
    iput-wide v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStartTime:J

    .line 169
    iput-wide v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    .line 173
    iput-boolean v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mIsAirPlaneMode:Z

    .line 174
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataSlot:I

    .line 175
    iput-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mUserPermissionDialog:Landroid/app/Dialog;

    .line 178
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mForegroundCallState:[I

    .line 183
    iput-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    .line 297
    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;

    invoke-direct {v0, p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;-><init>(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)V

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 1665
    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$2;

    invoke-direct {v0, p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$2;-><init>(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)V

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->MdlogCallBack:Lcom/android/phone/statistics/ui/ModemLogDialog$DialogCallBack;

    .line 233
    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    invoke-static {}, Lcom/android/phone/utils/MiuiThreadUtil;->getWorkThreadLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;-><init>(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Landroid/os/Looper;)V

    .line 232
    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    .line 234
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 235
    return-void
.end method

.method private convertOssDuration(J)Ljava/lang/String;
    .locals 13
    .param p1, "oosDuration"    # J

    .prologue
    .line 750
    const-string/jumbo v3, "NA"

    .line 751
    .local v3, "result":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .local v6, "duration_start":J
    const-wide/16 v4, 0x0

    .line 752
    .local v4, "duration_end":J
    const/4 v8, 0x7

    new-array v2, v8, [J

    .local v2, "durationMap":[J
    fill-array-data v2, :array_0

    .line 759
    const/4 v8, 0x0

    array-length v9, v2

    :goto_0
    if-ge v8, v9, :cond_1

    aget-wide v0, v2, v8

    .line 760
    .local v0, "duration":J
    cmp-long v10, p1, v0

    if-lez v10, :cond_0

    .line 761
    move-wide v6, v0

    .line 759
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 763
    :cond_0
    move-wide v4, v0

    .line 767
    .end local v0    # "duration":J
    :cond_1
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-ltz v8, :cond_3

    .line 768
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_2

    .line 769
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, ""

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v10, 0x3e8

    div-long v10, v6, v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-wide/16 v10, 0x3e8

    div-long v10, v4, v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 776
    :goto_1
    return-object v3

    .line 771
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, ">"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v10, 0x3e8

    div-long v10, v6, v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 774
    :cond_3
    const-string/jumbo v3, "NA"

    goto :goto_1

    .line 752
    nop

    :array_0
    .array-data 8
        0x0
        0x2710
        0x7530
        0x1d4c0
        0x493e0
        0x927c0
        0x1b7740
    .end array-data
.end method

.method private convertStatisticDuration(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/StringBuilder;)V
    .locals 8
    .param p2, "sb"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 975
    .local p1, "networkClassDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;>;"
    if-nez p1, :cond_0

    return-void

    .line 976
    :cond_0
    const-string/jumbo v6, "["

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 977
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 978
    .local v2, "iter1":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 979
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 980
    .local v4, "plmnDurationEntry":Ljava/util/Map$Entry;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "={"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 981
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ConcurrentHashMap;

    .line 982
    .local v5, "plmnMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    if-nez v5, :cond_2

    .line 983
    const-string/jumbo v6, "=}"

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 986
    :cond_2
    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 987
    .local v3, "iter2":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 988
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 989
    .local v1, "durationEntry":Ljava/util/Map$Entry;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 990
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 991
    .local v0, "duration":Ljava/lang/Long;
    if-nez v0, :cond_4

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 992
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7, p2}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    .line 993
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 994
    const-string/jumbo v6, ", "

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 997
    .end local v0    # "duration":Ljava/lang/Long;
    .end local v1    # "durationEntry":Ljava/util/Map$Entry;
    :cond_5
    const-string/jumbo v6, "}"

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 998
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 999
    const-string/jumbo v6, ", "

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1002
    .end local v3    # "iter2":Ljava/util/Iterator;
    .end local v4    # "plmnDurationEntry":Ljava/util/Map$Entry;
    .end local v5    # "plmnMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    :cond_6
    const-string/jumbo v6, "]"

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1003
    return-void
.end method

.method private dispose()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 221
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "dispose"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->getInstance()Lcom/android/internal/telephony/CallManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/CallManager;->unregisterForDisconnect(Landroid/os/Handler;)V

    .line 223
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 226
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->unregisterTelephonyCallBack()V

    .line 227
    invoke-virtual {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->clearTelephonyStatisticData()V

    .line 228
    sput-object v2, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->sInstance:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .line 229
    return-void
.end method

.method private doDataCallEvent(ZILjava/util/Map;)V
    .locals 9
    .param p1, "status"    # Z
    .param p2, "phoneId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1148
    .local p3, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v6, "MiuiTelephonyStatistic"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "doDataCallEvent status="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    invoke-direct {p0, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v6

    if-nez v6, :cond_0

    return-void

    .line 1150
    :cond_0
    invoke-static {p2}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    .line 1151
    .local v4, "phone":Lcom/android/internal/telephony/Phone;
    invoke-direct {p0, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCellLocation(Lcom/android/internal/telephony/Phone;)Ljava/lang/String;

    move-result-object v0

    .line 1152
    .local v0, "cellLocation":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-direct {p0, v6, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getAvailableOperatorNumeric(Landroid/telephony/ServiceState;I)Ljava/lang/String;

    move-result-object v3

    .line 1153
    .local v3, "network":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v6, v6, p2

    invoke-direct {p0, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDataRat(Landroid/telephony/ServiceState;)I

    move-result v5

    .line 1154
    .local v5, "rat":I
    new-instance v2, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;

    invoke-direct {v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;-><init>()V

    .line 1155
    .local v2, "mDataStat":Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;
    invoke-virtual {v2, p3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->updateDataStat(Ljava/util/Map;)V

    .line 1156
    const-string/jumbo v6, "MiuiTelephonyStatistic"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "mDataStat "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCommonParamInfo()Ljava/util/HashMap;

    move-result-object v1

    .line 1159
    .local v1, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v6, "Apn"

    iget-object v7, v2, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->apn:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1160
    const-string/jumbo v6, "ApnType"

    iget-object v7, v2, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->apnType:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1161
    const-string/jumbo v7, "Cause"

    if-eqz p1, :cond_1

    const-string/jumbo v6, "NA"

    :goto_0
    invoke-virtual {v1, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1162
    const-string/jumbo v6, "CellLocation"

    invoke-virtual {v1, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1163
    const-string/jumbo v6, "Network"

    invoke-virtual {v1, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1164
    const-string/jumbo v6, "RAT"

    invoke-static {v5}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166
    const-string/jumbo v6, "MiuiTelephonyStatistic"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "doDataCallEvent data ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    invoke-static {v1}, Lcom/android/phone/statistics/utils/Utils;->isValidParams(Ljava/util/Map;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1169
    const-string/jumbo v6, "MiuiTelephonyStatistic"

    const-string/jumbo v7, "doDataCallEvent invalid paramerters!!!"

    invoke-static {v6, v7}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    return-void

    .line 1161
    :cond_1
    iget-object v6, v2, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->cause:Ljava/lang/String;

    goto :goto_0

    .line 1172
    :cond_2
    const-string/jumbo v6, "MiuiTelephonyStatistic"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Readyfor doDataCallEvent status="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ",phoneId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 1174
    const-string/jumbo v6, "telephony_statistic"

    .line 1175
    const-string/jumbo v7, "data_setup_call_error"

    .line 1174
    invoke-static {v6, v7, v1}, Lcom/android/phone/utils/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1176
    const-string/jumbo v6, "data_setup_call_error"

    invoke-direct {p0, v1, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordLogInfo(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 1177
    return-void
.end method

.method private doFakeDataConnectionEvent(ILjava/util/Map;)V
    .locals 13
    .param p1, "phoneId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v11, 0x0

    .line 1180
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v10

    if-nez v10, :cond_0

    return-void

    .line 1181
    :cond_0
    invoke-static {p1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v8

    .line 1182
    .local v8, "phone":Lcom/android/internal/telephony/Phone;
    invoke-direct {p0, v8}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCellLocation(Lcom/android/internal/telephony/Phone;)Ljava/lang/String;

    move-result-object v1

    .line 1183
    .local v1, "cellLocation":Ljava/lang/String;
    invoke-direct {p0, v11, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getAvailableOperatorNumeric(Landroid/telephony/ServiceState;I)Ljava/lang/String;

    move-result-object v7

    .line 1184
    .local v7, "network":Ljava/lang/String;
    const-string/jumbo v4, "NA"

    .line 1185
    .local v4, "ifaceName":Ljava/lang/String;
    const-string/jumbo v6, "NA"

    .line 1186
    .local v6, "mtu":Ljava/lang/String;
    const-string/jumbo v3, "NA"

    .line 1187
    .local v3, "dnsServers":Ljava/lang/String;
    const-string/jumbo v10, "default"

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/Phone;->getActiveApnHost(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1188
    .local v0, "apn":Ljava/lang/String;
    const-string/jumbo v10, "default"

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/Phone;->getLinkProperties(Ljava/lang/String;)Landroid/net/LinkProperties;

    move-result-object v5

    .line 1189
    .local v5, "link":Landroid/net/LinkProperties;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    xor-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_1

    .line 1190
    invoke-virtual {v5}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v4

    .line 1191
    invoke-virtual {v5}, Landroid/net/LinkProperties;->getMtu()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 1192
    invoke-virtual {v5}, Landroid/net/LinkProperties;->getDnsServers()Ljava/util/List;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDnsServers(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 1194
    :cond_1
    iget-object v10, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v10, v10, p1

    invoke-direct {p0, v10}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDataRat(Landroid/telephony/ServiceState;)I

    move-result v9

    .line 1196
    .local v9, "rat":I
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCommonParamInfo()Ljava/util/HashMap;

    move-result-object v2

    .line 1197
    .local v2, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v10, "CellLocation"

    invoke-virtual {v2, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198
    const-string/jumbo v10, "Network"

    invoke-virtual {v2, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1199
    const-string/jumbo v10, "RAT"

    invoke-static {v9}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200
    const-string/jumbo v10, "Apn"

    if-nez v0, :cond_2

    const-string/jumbo v0, "NA"

    .end local v0    # "apn":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1201
    const-string/jumbo v10, "Interface"

    invoke-virtual {v2, v10, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1202
    const-string/jumbo v10, "Mtu"

    invoke-virtual {v2, v10, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1203
    const-string/jumbo v10, "DnsServers"

    invoke-virtual {v2, v10, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1205
    const-string/jumbo v10, "MiuiTelephonyStatistic"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "doFakeDataConnectionEvent data ="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    invoke-static {v2}, Lcom/android/phone/statistics/utils/Utils;->isValidParams(Ljava/util/Map;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 1208
    const-string/jumbo v10, "MiuiTelephonyStatistic"

    const-string/jumbo v11, "doFakeDataConnectionEvent invalid paramerters!!!"

    invoke-static {v10, v11}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1209
    return-void

    .line 1211
    :cond_3
    const-string/jumbo v10, "MiuiTelephonyStatistic"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Readyfor doFakeDataConnectionEvent  phoneId="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 1213
    const-string/jumbo v10, "telephony_statistic"

    .line 1214
    const-string/jumbo v11, "data_fake_connection"

    .line 1213
    invoke-static {v10, v11, v2}, Lcom/android/phone/utils/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1215
    const-string/jumbo v10, "data_fake_connection"

    invoke-direct {p0, v2, v10}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordLogInfo(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 1216
    return-void
.end method

.method private forceUpdateStatisticData()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 914
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v2, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v2, :cond_0

    .line 915
    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 916
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-direct {p0, v1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getServiceState(Lcom/android/internal/telephony/Phone;)Landroid/telephony/ServiceState;

    move-result-object v2

    invoke-direct {p0, v0, v2, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordServiceState(ILandroid/telephony/ServiceState;Z)V

    .line 917
    invoke-static {v1}, Lcom/android/phone/PhoneProxy;->isVolteEnabled(Lcom/android/internal/telephony/Phone;)Z

    move-result v2

    invoke-direct {p0, v0, v2, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->processVoLTERegisteredStateChange(IZZ)V

    .line 914
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 919
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_0
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v2

    invoke-direct {p0, v2, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordDataConnectionDuration(IZ)V

    .line 920
    return-void
.end method

.method private getAvailableOperatorNumeric(Landroid/telephony/ServiceState;I)Ljava/lang/String;
    .locals 2
    .param p1, "ss"    # Landroid/telephony/ServiceState;
    .param p2, "phoneId"    # I

    .prologue
    .line 780
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v0

    .line 781
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 782
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v0

    .line 784
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 785
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, p2}, Lmiui/telephony/TelephonyManager;->getSimOperatorForSlot(I)Ljava/lang/String;

    move-result-object v0

    .line 787
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 788
    const-string/jumbo v0, "NoSim"

    .line 790
    :cond_2
    return-object v0

    .line 780
    :cond_3
    const/4 v0, 0x0

    .local v0, "currentPlmn":Ljava/lang/String;
    goto :goto_0
.end method

.method private getCallDisconnectCause(Lcom/android/internal/telephony/Connection;)Ljava/lang/String;
    .locals 2
    .param p1, "conn"    # Lcom/android/internal/telephony/Connection;

    .prologue
    .line 818
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getPreciseDisconnectCause()I

    move-result v0

    .line 819
    .local v0, "cause":I
    if-nez v0, :cond_0

    .line 820
    instance-of v1, p1, Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;

    if-eqz v1, :cond_1

    .line 821
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getDisconnectCause()I

    move-result v1

    add-int/lit16 v0, v1, 0x2710

    .line 827
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 824
    :cond_1
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getDisconnectCause()I

    move-result v1

    add-int/lit16 v0, v1, 0x4e20

    goto :goto_0
.end method

.method private getCellLocation(Lcom/android/internal/telephony/Phone;)Ljava/lang/String;
    .locals 4
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 794
    const-string/jumbo v1, "NA"

    .line 795
    .local v1, "result":Ljava/lang/String;
    invoke-static {p1}, Lcom/android/phone/PhoneProxy;->getCellLocation(Lcom/android/internal/telephony/Phone;)Landroid/telephony/CellLocation;

    move-result-object v0

    .line 796
    .local v0, "location":Landroid/telephony/CellLocation;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/CellLocation;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 797
    :cond_0
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mCellLocation:[Landroid/telephony/CellLocation;

    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v3

    aget-object v0, v2, v3

    .line 799
    :cond_1
    instance-of v2, v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v2, :cond_3

    .line 800
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object v2, v0

    check-cast v2, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual {v2}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    .end local v0    # "location":Landroid/telephony/CellLocation;
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 804
    :cond_2
    :goto_0
    return-object v1

    .line 801
    .restart local v0    # "location":Landroid/telephony/CellLocation;
    :cond_3
    instance-of v2, v0, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v2, :cond_2

    .line 802
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object v2, v0

    check-cast v2, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-virtual {v2}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object v2, v0

    check-cast v2, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-virtual {v2}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    .end local v0    # "location":Landroid/telephony/CellLocation;
    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getCommonParamInfo()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 687
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 688
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v1, "Version"

    sget-object v2, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 689
    const-string/jumbo v1, "VersionType"

    invoke-static {}, Lcom/android/phone/statistics/utils/DeviceUtil;->getVersionType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 690
    return-object v0
.end method

.method private getDataNetworkClass(Landroid/telephony/ServiceState;)I
    .locals 1
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 1465
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v0

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->getNetworkClass(I)I

    move-result v0

    return v0
.end method

.method private getDataRat(Landroid/telephony/ServiceState;)I
    .locals 1
    .param p1, "ss"    # Landroid/telephony/ServiceState;

    .prologue
    .line 1234
    if-nez p1, :cond_0

    .line 1235
    const/4 v0, 0x0

    return v0

    .line 1237
    :cond_0
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRilDataRadioTechnology()I

    move-result v0

    return v0
.end method

.method private getDbm(Landroid/telephony/SignalStrength;)I
    .locals 1
    .param p1, "signal"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 1219
    const v0, 0x7fffffff

    .line 1220
    .local v0, "dBm":I
    if-eqz p1, :cond_0

    .line 1221
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getDbm()I

    move-result v0

    .line 1223
    :cond_0
    return v0
.end method

.method private getDefaultDataSlotId()I
    .locals 1

    .prologue
    .line 1602
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    return v0
.end method

.method private getDnsServers(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1290
    .local p1, "dnses":Ljava/util/List;, "Ljava/util/List<Ljava/net/InetAddress;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1291
    .local v0, "dnsServers":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1292
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/net/InetAddress;

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1293
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 1294
    const-string/jumbo v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1291
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1297
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static getEmptyCellLocation(I)Landroid/telephony/CellLocation;
    .locals 1
    .param p0, "phoneType"    # I

    .prologue
    .line 1322
    packed-switch p0, :pswitch_data_0

    .line 1328
    const/4 v0, 0x0

    return-object v0

    .line 1324
    :pswitch_0
    new-instance v0, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-direct {v0}, Landroid/telephony/cdma/CdmaCellLocation;-><init>()V

    return-object v0

    .line 1326
    :pswitch_1
    new-instance v0, Landroid/telephony/gsm/GsmCellLocation;

    invoke-direct {v0}, Landroid/telephony/gsm/GsmCellLocation;-><init>()V

    return-object v0

    .line 1322
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getInstance()Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->sInstance:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    return-object v0
.end method

.method private getNetworkClass(Landroid/telephony/ServiceState;)I
    .locals 1
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 808
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getNetworkType()I

    move-result v0

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->getNetworkClass(I)I

    move-result v0

    return v0
.end method

.method private getPhone(Lcom/android/internal/telephony/Connection;)Lcom/android/internal/telephony/Phone;
    .locals 2
    .param p1, "conn"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v1, 0x0

    .line 831
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 832
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0

    .line 834
    :cond_0
    return-object v1
.end method

.method private getPreferredNwModeFromDb(I)I
    .locals 1
    .param p1, "phoneId"    # I

    .prologue
    .line 1583
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    invoke-static {v0, p1}, Lmiui/telephony/DefaultSimManager;->getPreferredNetworkModeFromDb(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method private getServiceState(Lcom/android/internal/telephony/Phone;)Landroid/telephony/ServiceState;
    .locals 2
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 923
    invoke-static {p1}, Lcom/android/phone/PhoneProxy;->getServiceStateTracker(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v0

    .line 924
    .local v0, "sst":Lcom/android/internal/telephony/ServiceStateTracker;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mSS:Landroid/telephony/ServiceState;

    if-eqz v1, :cond_0

    .line 925
    iget-object v1, v0, Lcom/android/internal/telephony/ServiceStateTracker;->mSS:Landroid/telephony/ServiceState;

    return-object v1

    .line 927
    :cond_0
    new-instance v1, Landroid/telephony/ServiceState;

    invoke-direct {v1}, Landroid/telephony/ServiceState;-><init>()V

    return-object v1
.end method

.method private getSignalQuality(Landroid/telephony/SignalStrength;I)I
    .locals 1
    .param p1, "signal"    # Landroid/telephony/SignalStrength;
    .param p2, "rat"    # I

    .prologue
    const v0, 0x7fffffff

    .line 1241
    if-nez p1, :cond_0

    .line 1242
    return v0

    .line 1244
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1268
    :pswitch_0
    return v0

    .line 1254
    :pswitch_1
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmBitErrorRate()I

    move-result v0

    return v0

    .line 1258
    :pswitch_2
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getCdmaEcio()I

    move-result v0

    return v0

    .line 1263
    :pswitch_3
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getEvdoEcio()I

    move-result v0

    return v0

    .line 1266
    :pswitch_4
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLteRsrq()I

    move-result v0

    return v0

    .line 1244
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method private getVoiceRat(Landroid/telephony/ServiceState;)I
    .locals 1
    .param p1, "ss"    # Landroid/telephony/ServiceState;

    .prologue
    .line 1227
    if-nez p1, :cond_0

    .line 1228
    const/4 v0, 0x0

    return v0

    .line 1230
    :cond_0
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRilVoiceRadioTechnology()I

    move-result v0

    return v0
.end method

.method private getVolteEnabledStatus(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, -0x1

    .line 562
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 563
    .local v1, "result":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/android/phone/Dual4GManager;->isDual4GEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 564
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v3, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v3, :cond_2

    .line 565
    invoke-static {}, Lcom/android/internal/telephony/SubscriptionController;->getInstance()Lcom/android/internal/telephony/SubscriptionController;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/internal/telephony/SubscriptionController;->getSubIdUsingPhoneId(I)I

    move-result v2

    .line 566
    .local v2, "subId":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "volte_vt_enabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 567
    sget v3, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    add-int/lit8 v3, v3, -0x1

    if-eq v0, v3, :cond_0

    .line 568
    const-string/jumbo v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 572
    .end local v0    # "i":I
    .end local v2    # "subId":I
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "volte_vt_enabled"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 574
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private init()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 238
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    .line 239
    invoke-static {}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getInstance()Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    .line 240
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    iput-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 241
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->getInstance()Lcom/android/internal/telephony/CallManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/internal/telephony/CallManager;->registerForDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 243
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v2, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v2, :cond_0

    .line 244
    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 245
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastServiceStateTime:[J

    aput-wide v6, v2, v0

    .line 246
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastPlmnWithServiceState:[Ljava/lang/String;

    const-string/jumbo v3, "NoSim"

    aput-object v3, v2, v0

    .line 247
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastNetworkClassTime:[J

    aput-wide v6, v2, v0

    .line 248
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastPlmnWithNetworkClass:[Ljava/lang/String;

    const-string/jumbo v3, "NoSim"

    aput-object v3, v2, v0

    .line 249
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    sget-object v3, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->INIT:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v3, v2, v0

    .line 250
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredTime:[J

    aput-wide v6, v2, v0

    .line 251
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredPlmn:[Ljava/lang/String;

    const-string/jumbo v3, "NoSim"

    aput-object v3, v2, v0

    .line 252
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    new-instance v3, Landroid/telephony/ServiceState;

    invoke-direct {v3}, Landroid/telephony/ServiceState;-><init>()V

    aput-object v3, v2, v0

    .line 253
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mSignalStrength:[Landroid/telephony/SignalStrength;

    new-instance v3, Landroid/telephony/SignalStrength;

    invoke-direct {v3}, Landroid/telephony/SignalStrength;-><init>()V

    aput-object v3, v2, v0

    .line 254
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mCellLocation:[Landroid/telephony/CellLocation;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v3

    invoke-static {v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getEmptyCellLocation(I)Landroid/telephony/CellLocation;

    move-result-object v3

    aput-object v3, v2, v0

    .line 255
    invoke-direct {p0, v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->initNwOosValue(I)V

    .line 256
    invoke-direct {p0, v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->initNwWithout4GValue(I)V

    .line 257
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mForegroundCallState:[I

    const/4 v3, 0x0

    aput v3, v2, v0

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 259
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->reloadStatisticDataFromSP()V

    .line 260
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/phone/TelephonyJobService;->initReportJobSchedule(Landroid/content/Context;)V

    .line 262
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->registerReceiver()V

    .line 263
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->registerTelephonyCallBack()V

    .line 265
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    invoke-virtual {v2}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->updateMdlogFromSP()V

    .line 266
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isAirPlaneModeOn()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mIsAirPlaneMode:Z

    .line 267
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDefaultDataSlotId()I

    move-result v2

    iput v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataSlot:I

    .line 268
    const-string/jumbo v2, "MiuiTelephonyStatistic"

    const-string/jumbo v3, "init done"

    invoke-static {v2, v3}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    return-void
.end method

.method private initNwOosValue(I)V
    .locals 4
    .param p1, "phoneId"    # I

    .prologue
    .line 1377
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosCount:[I

    const/4 v1, 0x0

    aput v1, v0, p1

    .line 1378
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosStartTime:[J

    const-wide/16 v2, -0x1

    aput-wide v2, v0, p1

    .line 1379
    return-void
.end method

.method private initNwWithout4GValue(I)V
    .locals 4
    .param p1, "phoneId"    # I

    .prologue
    .line 1382
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GCount:[I

    const/4 v1, 0x0

    aput v1, v0, p1

    .line 1383
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GStartTime:[J

    const-wide/16 v2, -0x1

    aput-wide v2, v0, p1

    .line 1384
    return-void
.end method

.method private isAirPlaneModeOn()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1598
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isAllowedForTrackNwOos(I)Z
    .locals 9
    .param p1, "phoneId"    # I

    .prologue
    const/4 v8, 0x0

    .line 1399
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "isTrackNwOosAllowed  phoneId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1400
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1401
    .local v0, "currentTime":J
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosStartTime:[J

    aget-wide v2, v4, p1

    .line 1402
    .local v2, "oldTime":J
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    sub-long v4, v0, v2

    const-wide/32 v6, 0x5265c00

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 1403
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->initNwOosValue(I)V

    .line 1405
    :cond_0
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosCount:[I

    aget v4, v4, p1

    if-nez v4, :cond_1

    .line 1406
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosStartTime:[J

    aput-wide v0, v4, p1

    .line 1408
    :cond_1
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosCount:[I

    aget v5, v4, p1

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, p1

    .line 1409
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "isAllowedForTrackNwOos cn="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosCount:[I

    aget v6, v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwOosCount:[I

    aget v4, v4, p1

    int-to-long v4, v4

    const-wide/16 v6, 0x5

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    .line 1411
    const/4 v4, 0x1

    return v4

    .line 1413
    :cond_2
    return v8
.end method

.method private isAllowedForTrackNwWithout4G(I)Z
    .locals 9
    .param p1, "phoneId"    # I

    .prologue
    const/4 v8, 0x0

    .line 1512
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "isAllowedForTrackNwWithout4G  phoneId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1513
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1514
    .local v0, "currentTime":J
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GStartTime:[J

    aget-wide v2, v4, p1

    .line 1516
    .local v2, "oldTime":J
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    sub-long v4, v0, v2

    const-wide/32 v6, 0x5265c00

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 1517
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->initNwWithout4GValue(I)V

    .line 1519
    :cond_0
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GCount:[I

    aget v4, v4, p1

    if-nez v4, :cond_1

    .line 1520
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GStartTime:[J

    aput-wide v0, v4, p1

    .line 1522
    :cond_1
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GCount:[I

    aget v5, v4, p1

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, p1

    .line 1524
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "isAllowedForTrackNwWithout4G cn="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GCount:[I

    aget v6, v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1526
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNwWithout4GCount:[I

    aget v4, v4, p1

    int-to-long v4, v4

    const-wide/16 v6, 0x5

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    .line 1527
    const/4 v4, 0x1

    return v4

    .line 1529
    :cond_2
    return v8
.end method

.method private isCallDrop(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/Connection;)Z
    .locals 4
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "conn"    # Lcom/android/internal/telephony/Connection;

    .prologue
    .line 1283
    invoke-virtual {p2}, Lcom/android/internal/telephony/Connection;->getConnectTime()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1284
    const/4 v0, 0x0

    return v0

    .line 1286
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private isDefaultDataSlotId(I)Z
    .locals 1
    .param p1, "slotId"    # I

    .prologue
    .line 1606
    iget v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataSlot:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInLteNwMode(I)Z
    .locals 2
    .param p1, "phoneId"    # I

    .prologue
    .line 1561
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getPreferredNwModeFromDb(I)I

    move-result v0

    .line 1562
    .local v0, "nwmode":I
    invoke-direct {p0, v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isLteMode(I)Z

    move-result v1

    return v1
.end method

.method private isLteMode(I)Z
    .locals 1
    .param p1, "nwMode"    # I

    .prologue
    .line 1566
    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    .line 1567
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 1577
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 1568
    :cond_1
    const/16 v0, 0x9

    if-eq p1, v0, :cond_0

    .line 1569
    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    .line 1570
    const/16 v0, 0xb

    if-eq p1, v0, :cond_0

    .line 1571
    const/16 v0, 0xc

    if-eq p1, v0, :cond_0

    .line 1572
    const/16 v0, 0xf

    if-eq p1, v0, :cond_0

    .line 1573
    const/16 v0, 0x11

    if-eq p1, v0, :cond_0

    .line 1574
    const/16 v0, 0x13

    if-eq p1, v0, :cond_0

    .line 1575
    const/16 v0, 0x14

    if-eq p1, v0, :cond_0

    .line 1576
    const/16 v0, 0x16

    if-eq p1, v0, :cond_0

    .line 1579
    const/4 v0, 0x0

    return v0
.end method

.method private isNormalCaseForDataWithout4G(I)Z
    .locals 3
    .param p1, "phoneId"    # I

    .prologue
    .line 1484
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "isNormalCaseForDataWithout4G phoneId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    iget-boolean v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mIsAirPlaneMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isSimReady(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isPhoneStateIdle()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 1486
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isDefaultDataSlotId(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 1485
    if-nez v0, :cond_0

    .line 1486
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isInLteNwMode(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 1485
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isNormalCaseForVoiceDetach(I)Z
    .locals 1
    .param p1, "phoneId"    # I

    .prologue
    .line 1369
    iget-boolean v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mIsAirPlaneMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isSimReady(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isPhoneStateIdle()Z
    .locals 5

    .prologue
    .line 1587
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v2, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v2, :cond_1

    .line 1588
    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 1589
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    if-eq v2, v3, :cond_0

    .line 1590
    const-string/jumbo v2, "MiuiTelephonyStatistic"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "isPhoneStateIdle: Voice call active on phone: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1591
    const/4 v2, 0x0

    return v2

    .line 1587
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1594
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method private isSimReady(I)Z
    .locals 2
    .param p1, "phoneId"    # I

    .prologue
    .line 1373
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->getSimState(I)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidCaseForDataWithoutLte(I)Z
    .locals 6
    .param p1, "phoneId"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1490
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v3, v3, p1

    if-eqz v3, :cond_2

    .line 1491
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v3, v3, p1

    invoke-direct {p0, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDataNetworkClass(Landroid/telephony/ServiceState;)I

    move-result v0

    .line 1492
    .local v0, "dataClass":I
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "isValidCaseForDataWithoutLte dataClass: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    if-eq v0, v1, :cond_0

    .line 1494
    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 1493
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 1494
    goto :goto_0

    .line 1496
    .end local v0    # "dataClass":I
    :cond_2
    return v2
.end method

.method private isValidatePhoneId(I)Z
    .locals 2
    .param p1, "phoneId"    # I

    .prologue
    .line 1273
    if-ltz p1, :cond_0

    sget v1, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge p1, v1, :cond_0

    const/4 v0, 0x1

    .line 1274
    .local v0, "valid":Z
    :goto_0
    return v0

    .line 1273
    .end local v0    # "valid":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "valid":Z
    goto :goto_0
.end method

.method public static make()Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .locals 1

    .prologue
    .line 186
    sget-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->sInstance:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->shouldStatistic()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-direct {v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;-><init>()V

    sput-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->sInstance:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .line 189
    :cond_0
    sget-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->sInstance:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    return-object v0
.end method

.method private monitorDataNwChange(ILandroid/telephony/ServiceState;)V
    .locals 8
    .param p1, "phoneId"    # I
    .param p2, "ss"    # Landroid/telephony/ServiceState;

    .prologue
    const/4 v7, 0x3

    .line 1444
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v4, v4, p1

    if-eqz v4, :cond_0

    if-nez p2, :cond_1

    .line 1445
    :cond_0
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    const-string/jumbo v5, "monitorDataNwChange invalid params!"

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446
    return-void

    .line 1448
    :cond_1
    invoke-direct {p0, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDataNetworkClass(Landroid/telephony/ServiceState;)I

    move-result v2

    .line 1449
    .local v2, "newDataClass":I
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v4, v4, p1

    invoke-direct {p0, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDataNetworkClass(Landroid/telephony/ServiceState;)I

    move-result v3

    .line 1450
    .local v3, "oldDataClass":I
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "newDataClass:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ",oldDataClass="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    if-eq v3, v7, :cond_4

    .line 1452
    if-ne v2, v7, :cond_3

    const/4 v1, 0x1

    .line 1453
    .local v1, "lteRegistered":Z
    :goto_0
    if-ne v3, v7, :cond_6

    .line 1454
    if-eq v2, v7, :cond_5

    const/4 v0, 0x1

    .line 1455
    .local v0, "lteDeregistered":Z
    :goto_1
    if-eqz v1, :cond_7

    .line 1456
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->stopLteMonitorTimer()V

    .line 1462
    :cond_2
    :goto_2
    return-void

    .line 1452
    .end local v0    # "lteDeregistered":Z
    .end local v1    # "lteRegistered":Z
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "lteRegistered":Z
    goto :goto_0

    .line 1451
    .end local v1    # "lteRegistered":Z
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "lteRegistered":Z
    goto :goto_0

    .line 1454
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "lteDeregistered":Z
    goto :goto_1

    .line 1453
    .end local v0    # "lteDeregistered":Z
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "lteDeregistered":Z
    goto :goto_1

    .line 1457
    :cond_7
    if-eqz v0, :cond_2

    .line 1458
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isNormalCaseForDataWithout4G(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1459
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->startLteMonitorTimer(I)V

    goto :goto_2
.end method

.method private monitorVoiceNwChange(ILandroid/telephony/ServiceState;)V
    .locals 7
    .param p1, "phoneId"    # I
    .param p2, "ss"    # Landroid/telephony/ServiceState;

    .prologue
    .line 1333
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v4, v4, p1

    if-eqz v4, :cond_0

    if-nez p2, :cond_1

    .line 1334
    :cond_0
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    const-string/jumbo v5, "monitorVoiceNwChange invalid params!"

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    return-void

    .line 1337
    :cond_1
    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getVoiceRegState()I

    move-result v0

    .line 1338
    .local v0, "newVoiceReg":I
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Landroid/telephony/ServiceState;->getVoiceRegState()I

    move-result v1

    .line 1339
    .local v1, "oldVoiceReg":I
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "newVoiceReg:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ",oldVoiceReg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1340
    if-eqz v1, :cond_4

    .line 1341
    if-nez v0, :cond_3

    const/4 v3, 0x1

    .line 1342
    .local v3, "voiceRegistered":Z
    :goto_0
    if-nez v1, :cond_6

    .line 1343
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    .line 1344
    .local v2, "voiceDeregistered":Z
    :goto_1
    if-eqz v3, :cond_7

    .line 1345
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->stopNwOosTimer()V

    .line 1351
    :cond_2
    :goto_2
    return-void

    .line 1341
    .end local v2    # "voiceDeregistered":Z
    .end local v3    # "voiceRegistered":Z
    :cond_3
    const/4 v3, 0x0

    .restart local v3    # "voiceRegistered":Z
    goto :goto_0

    .line 1340
    .end local v3    # "voiceRegistered":Z
    :cond_4
    const/4 v3, 0x0

    .restart local v3    # "voiceRegistered":Z
    goto :goto_0

    .line 1343
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "voiceDeregistered":Z
    goto :goto_1

    .line 1342
    .end local v2    # "voiceDeregistered":Z
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "voiceDeregistered":Z
    goto :goto_1

    .line 1346
    :cond_7
    if-eqz v2, :cond_2

    .line 1347
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isNormalCaseForVoiceDetach(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1348
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->startNwOosTimer(I)V

    goto :goto_2
.end method

.method private processCallDisconnectEvent(Lcom/android/internal/telephony/Connection;)V
    .locals 2
    .param p1, "conn"    # Lcom/android/internal/telephony/Connection;

    .prologue
    .line 551
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getPhone(Lcom/android/internal/telephony/Connection;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 552
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v0, :cond_0

    .line 553
    return-void

    .line 556
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isCallDrop(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/Connection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 557
    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-direct {p0, v1, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordCallUnexpectedDisconnectCountEvent(ILcom/android/internal/telephony/Connection;)V

    .line 559
    :cond_1
    return-void
.end method

.method private processCellLocationChangeEvent(ILandroid/telephony/CellLocation;)V
    .locals 1
    .param p1, "phoneId"    # I
    .param p2, "location"    # Landroid/telephony/CellLocation;

    .prologue
    .line 516
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 517
    :cond_0
    return-void

    .line 516
    :cond_1
    invoke-virtual {p2}, Landroid/telephony/CellLocation;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mCellLocation:[Landroid/telephony/CellLocation;

    aget-object v0, v0, p1

    invoke-virtual {p2, v0}, Landroid/telephony/CellLocation;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 520
    return-void

    .line 522
    :cond_2
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mCellLocation:[Landroid/telephony/CellLocation;

    aput-object p2, v0, p1

    .line 523
    return-void
.end method

.method private processDataDuration(II)V
    .locals 1
    .param p1, "phoneId"    # I
    .param p2, "dataState"    # I

    .prologue
    .line 495
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordDataConnectionDuration(IZ)V

    .line 498
    :cond_0
    return-void
.end method

.method private processDataStatisticChange(IILjava/util/HashMap;)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "phoneId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 465
    .local p3, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 466
    const-string/jumbo v1, "MiuiTelephonyStatistic"

    const-string/jumbo v2, "processDataStatisticChange exception phoneId!"

    invoke-static {v1, v2}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    return-void

    .line 469
    :cond_0
    invoke-static {}, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->values()[Lmiui/telephony/TelephonyStatAdapter$DataStatType;

    move-result-object v1

    aget-object v0, v1, p1

    .line 470
    .local v0, "dataStatType":Lmiui/telephony/TelephonyStatAdapter$DataStatType;
    invoke-static {}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-getmiui-telephony-TelephonyStatAdapter$DataStatTypeSwitchesValues()[I

    move-result-object v1

    invoke-virtual {v0}, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 481
    const-string/jumbo v1, "MiuiTelephonyStatistic"

    const-string/jumbo v2, "default data statistic type!"

    invoke-static {v1, v2}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :goto_0
    return-void

    .line 472
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->doFakeDataConnectionEvent(ILjava/util/Map;)V

    goto :goto_0

    .line 475
    :pswitch_1
    const/4 v1, 0x1

    invoke-direct {p0, v1, p2, p3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->doDataCallEvent(ZILjava/util/Map;)V

    goto :goto_0

    .line 478
    :pswitch_2
    const/4 v1, 0x0

    invoke-direct {p0, v1, p2, p3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->doDataCallEvent(ZILjava/util/Map;)V

    goto :goto_0

    .line 470
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private processDebugAll(Z)V
    .locals 3
    .param p1, "debugAll"    # Z

    .prologue
    .line 1702
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "processDebugAll :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logW(Ljava/lang/String;Ljava/lang/String;)V

    .line 1703
    invoke-static {p1}, Lcom/android/phone/statistics/utils/Utils;->setDebugAll(Z)V

    .line 1704
    return-void
.end method

.method private processMdlogReportAgree(I)V
    .locals 4
    .param p1, "ruleEventId"    # I

    .prologue
    .line 1678
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "processMdlogReportAgree id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1679
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setRuleEvent(IZ)V

    .line 1680
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setLastUserMdlogTime(Ljava/lang/Long;)V

    .line 1681
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setUserDisagreeCounter(I)V

    .line 1682
    return-void
.end method

.method private processMdlogReportDisagree()V
    .locals 4

    .prologue
    .line 1689
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    invoke-virtual {v1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getUserDisagreeCounter()I

    move-result v0

    .line 1690
    .local v0, "userCn":I
    add-int/lit8 v0, v0, 0x1

    .line 1691
    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 1692
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setRuleEvent(IZ)V

    .line 1696
    :goto_0
    return-void

    .line 1694
    :cond_0
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    invoke-virtual {v1, v0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setUserDisagreeCounter(I)V

    goto :goto_0
.end method

.method private processNwOosStatEvent(I)V
    .locals 3
    .param p1, "phoneId"    # I

    .prologue
    .line 1387
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "processNwOosStatEvent phoneId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1388
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isNormalCaseForVoiceDetach(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isAllowedForTrackNwOos(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1389
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->initNwOosValue(I)V

    .line 1390
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordNwOosEvent(I)V

    .line 1392
    :cond_0
    return-void
.end method

.method private processNwWithout4GStatEvent(I)V
    .locals 3
    .param p1, "phoneId"    # I

    .prologue
    .line 1500
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "processNwWithout4GStatEvent Enter  phoneId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1501
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidCaseForDataWithoutLte(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isAllowedForTrackNwWithout4G(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1502
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->initNwWithout4GValue(I)V

    .line 1503
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordNwWithout4GEvent(I)V

    .line 1505
    :cond_0
    return-void
.end method

.method private processPreciseCallStateEvent(II)V
    .locals 6
    .param p1, "phoneId"    # I
    .param p2, "foregroundCallState"    # I

    .prologue
    const/4 v5, 0x1

    .line 1625
    const-string/jumbo v2, "MiuiTelephonyStatistic"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "processPreciseCallStateEvent phoneId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ",fc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1626
    const-string/jumbo v4, ",oldfcstate="

    .line 1625
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1626
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mForegroundCallState:[I

    aget v4, v4, p1

    .line 1625
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1627
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mForegroundCallState:[I

    aget v2, v2, p1

    if-ne p2, v2, :cond_1

    .line 1628
    :cond_0
    return-void

    .line 1630
    :cond_1
    if-eqz p2, :cond_2

    .line 1631
    if-ne v5, p2, :cond_3

    .line 1632
    :cond_2
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mForegroundCallState:[I

    aput p2, v2, p1

    .line 1633
    if-ne v5, p2, :cond_3

    .line 1635
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    invoke-virtual {v2}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getRuleEvent()I

    move-result v1

    .line 1636
    .local v1, "ruleEvent":I
    invoke-static {v1, v5}, Lcom/android/phone/statistics/utils/Utils;->hasRuleEvent(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1637
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCommonParamInfo()Ljava/util/HashMap;

    move-result-object v0

    .line 1638
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v2, "CallState"

    const-string/jumbo v3, "CallOffhook"

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1639
    const-string/jumbo v2, "MiuiTelephonyStatistic"

    const-string/jumbo v3, "Readyfor Offhook Call"

    invoke-static {v2, v3}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 1640
    const-string/jumbo v2, "call_unexpected_disconnected"

    invoke-direct {p0, v0, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordLogInfo(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 1644
    .end local v0    # "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "ruleEvent":I
    :cond_3
    return-void
.end method

.method private processServiceStateChangeEvent(ILandroid/telephony/ServiceState;)V
    .locals 3
    .param p1, "phoneId"    # I
    .param p2, "ss"    # Landroid/telephony/ServiceState;

    .prologue
    .line 501
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "processServiceStateChangeEvent phoneId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ",ss="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 503
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordServiceState(ILandroid/telephony/ServiceState;Z)V

    .line 504
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "processServiceStateChangeEvent manual="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getIsManualSelection()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    invoke-virtual {p2}, Landroid/telephony/ServiceState;->getIsManualSelection()Z

    move-result v0

    if-nez v0, :cond_0

    .line 506
    invoke-direct {p0, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->monitorVoiceNwChange(ILandroid/telephony/ServiceState;)V

    .line 507
    invoke-direct {p0, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->monitorDataNwChange(ILandroid/telephony/ServiceState;)V

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    new-instance v1, Landroid/telephony/ServiceState;

    invoke-direct {v1, p2}, Landroid/telephony/ServiceState;-><init>(Landroid/telephony/ServiceState;)V

    aput-object v1, v0, p1

    .line 513
    :goto_0
    return-void

    .line 511
    :cond_1
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "processServiceStateChangeEvent invalid params!!!"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processSignalStrengthEvent(ILandroid/telephony/SignalStrength;)V
    .locals 2
    .param p1, "phoneId"    # I
    .param p2, "signal"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 487
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 488
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mSignalStrength:[Landroid/telephony/SignalStrength;

    new-instance v1, Landroid/telephony/SignalStrength;

    invoke-direct {v1, p2}, Landroid/telephony/SignalStrength;-><init>(Landroid/telephony/SignalStrength;)V

    aput-object v1, v0, p1

    .line 492
    :goto_0
    return-void

    .line 490
    :cond_0
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "processSignalStrengthEvent-invalid phone id!"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processSimStateChanged(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 325
    const-string/jumbo v2, "slot"

    sget v3, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 326
    .local v1, "slotId":I
    invoke-static {v1}, Lmiui/telephony/SubscriptionManager;->isRealSlotId(I)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    .line 327
    :cond_0
    const-string/jumbo v2, "ss"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 328
    .local v0, "iccState":Ljava/lang/String;
    const-string/jumbo v2, "ABSENT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 329
    new-instance v2, Landroid/telephony/ServiceState;

    invoke-direct {v2}, Landroid/telephony/ServiceState;-><init>()V

    invoke-direct {p0, v1, v2, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordServiceState(ILandroid/telephony/ServiceState;Z)V

    .line 333
    :cond_1
    :goto_0
    return-void

    .line 330
    :cond_2
    const-string/jumbo v2, "LOADED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 331
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v2, v2, v1

    invoke-direct {p0, v1, v2, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordServiceState(ILandroid/telephony/ServiceState;Z)V

    goto :goto_0
.end method

.method private declared-synchronized processUserUploadMdlogEvent(I)V
    .locals 4
    .param p1, "ruleEventId"    # I

    .prologue
    monitor-enter p0

    .line 1647
    :try_start_0
    const-string/jumbo v1, "MiuiTelephonyStatistic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "processUserUploadMdlogEvent id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1648
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    monitor-exit p0

    .line 1649
    return-void

    .line 1651
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    invoke-virtual {v1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getRuleEvent()I

    move-result v0

    .line 1652
    .local v0, "ruleEvent":I
    const-string/jumbo v1, "MiuiTelephonyStatistic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "processUserUploadMdlogEvent ruleEvent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1653
    invoke-static {v0}, Lcom/android/phone/statistics/utils/Utils;->isPermanentUserDisagree(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0, p1}, Lcom/android/phone/statistics/utils/Utils;->hasRuleEvent(II)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    monitor-exit p0

    .line 1654
    return-void

    .line 1658
    :cond_2
    :try_start_2
    invoke-static {v0}, Lcom/android/phone/statistics/utils/Utils;->isAnyValidRuleEventExist(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1659
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->showModemDialog(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    .line 1663
    return-void

    .line 1661
    :cond_3
    :try_start_3
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setRuleEvent(IZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .end local v0    # "ruleEvent":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private processVoLTERegisteredStateChange(IZZ)V
    .locals 10
    .param p1, "phoneId"    # I
    .param p2, "status"    # Z
    .param p3, "force"    # Z

    .prologue
    .line 526
    sget v5, Lmiui/telephony/SubscriptionManager;->INVALID_PHONE_ID:I

    if-ne p1, v5, :cond_0

    return-void

    .line 527
    :cond_0
    if-nez p3, :cond_1

    xor-int/lit8 v5, p2, 0x1

    if-nez v5, :cond_2

    :cond_1
    if-eqz p3, :cond_4

    if-eqz p2, :cond_4

    .line 528
    :cond_2
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredTime:[J

    aget-wide v6, v5, p1

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredPlmn:[Ljava/lang/String;

    aget-object v5, v5, p1

    if-eqz v5, :cond_4

    .line 529
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredTime:[J

    aget-wide v8, v5, p1

    sub-long v0, v6, v8

    .line 530
    .local v0, "duration":J
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mVoLTERegisteredDuration:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredPlmn:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 531
    .local v4, "oldDuration":Ljava/lang/Long;
    if-nez v4, :cond_3

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 532
    :cond_3
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mVoLTERegisteredDuration:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredPlmn:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    add-long/2addr v8, v0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    :try_start_0
    const-string/jumbo v5, "volte_duration"

    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mVoLTERegisteredDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v6}, Lcom/android/phone/statistics/utils/ObjectSerializer;->serialize(Ljava/io/Serializable;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->saveStringDataToSP(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 540
    .end local v0    # "duration":J
    .end local v4    # "oldDuration":Ljava/lang/Long;
    :cond_4
    :goto_0
    if-eqz p2, :cond_5

    .line 541
    invoke-static {p1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    check-cast v3, Lcom/android/internal/telephony/imsphone/ImsPhone;

    .line 542
    .local v3, "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredTime:[J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    aput-wide v6, v5, p1

    .line 543
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredPlmn:[Ljava/lang/String;

    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, p1

    .line 548
    .end local v3    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    :goto_1
    return-void

    .line 535
    .restart local v0    # "duration":J
    .restart local v4    # "oldDuration":Ljava/lang/Long;
    :catch_0
    move-exception v2

    .line 536
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v5, "MiuiTelephonyStatistic"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "save KEY_VOLTE_DURATION exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 545
    .end local v0    # "duration":J
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "oldDuration":Ljava/lang/Long;
    :cond_5
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredTime:[J

    const-wide/16 v6, -0x1

    aput-wide v6, v5, p1

    .line 546
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastVoLTERegisteredPlmn:[Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v6, v5, p1

    goto :goto_1
.end method

.method private recordCallUnexpectedDisconnectCountEvent(ILcom/android/internal/telephony/Connection;)V
    .locals 6
    .param p1, "phoneId"    # I
    .param p2, "conn"    # Lcom/android/internal/telephony/Connection;

    .prologue
    .line 716
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v3

    if-nez v3, :cond_0

    return-void

    .line 717
    :cond_0
    invoke-static {p1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 718
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v3, v3, p1

    invoke-direct {p0, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getVoiceRat(Landroid/telephony/ServiceState;)I

    move-result v2

    .line 719
    .local v2, "rat":I
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCommonParamInfo()Ljava/util/HashMap;

    move-result-object v0

    .line 720
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v3, "CellLocation"

    invoke-direct {p0, v1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCellLocation(Lcom/android/internal/telephony/Phone;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 721
    const-string/jumbo v3, "Network"

    const/4 v4, 0x0

    invoke-direct {p0, v4, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getAvailableOperatorNumeric(Landroid/telephony/ServiceState;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 722
    const-string/jumbo v3, "DiconnectCause"

    invoke-direct {p0, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCallDisconnectCause(Lcom/android/internal/telephony/Connection;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 723
    const-string/jumbo v4, "Type"

    invoke-virtual {p2}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string/jumbo v3, "MT"

    :goto_0
    invoke-virtual {v0, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 724
    const-string/jumbo v3, "RAT"

    invoke-static {v2}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    const-string/jumbo v3, "SignalStrength"

    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v4, v4, p1

    invoke-direct {p0, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDbm(Landroid/telephony/SignalStrength;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 726
    const-string/jumbo v3, "SignalQuality"

    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v4, v4, p1

    invoke-direct {p0, v4, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getSignalQuality(Landroid/telephony/SignalStrength;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 728
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "recordCallUnexpectedDisconnectCountEvent data="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    invoke-static {v0}, Lcom/android/phone/statistics/utils/Utils;->isValidParams(Ljava/util/Map;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 731
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    const-string/jumbo v4, "recordCallUnexpectedDisconnectCountEvent invalid paramerters!!!"

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    return-void

    .line 723
    :cond_1
    const-string/jumbo v3, "MO"

    goto :goto_0

    .line 734
    :cond_2
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Readyfor recordCallUnexpectedDisconnectCountEvent phoneId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    const-string/jumbo v3, "telephony_statistic"

    .line 737
    const-string/jumbo v4, "call_unexpected_disconnected"

    .line 736
    invoke-static {v3, v4, v0}, Lcom/android/phone/utils/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 738
    const-string/jumbo v3, "call_unexpected_disconnected"

    invoke-direct {p0, v0, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordLogInfo(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 739
    return-void
.end method

.method private recordDataConnectionDuration(IZ)V
    .locals 10
    .param p1, "newDataState"    # I
    .param p2, "force"    # Z

    .prologue
    const/4 v6, 0x2

    .line 336
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 337
    .local v0, "currentTime":J
    iget v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOldDataConnectionState:I

    if-eq v5, v6, :cond_1

    if-ne p1, v6, :cond_1

    .line 338
    iput-wide v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStartTime:J

    .line 339
    iput p1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOldDataConnectionState:I

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iget v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOldDataConnectionState:I

    if-ne v5, v6, :cond_3

    if-eq p1, v6, :cond_3

    .line 341
    iget-wide v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStartTime:J

    sub-long v2, v0, v6

    .line 342
    .local v2, "duration":J
    iget-wide v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    add-long/2addr v6, v2

    iput-wide v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    .line 343
    sget-boolean v5, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v5, :cond_2

    const-string/jumbo v5, "MiuiTelephonyStatistic"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "recordDataConnectionDuration duration:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " total duration:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :cond_2
    :try_start_0
    const-string/jumbo v5, "data_connection_state_duration"

    iget-wide v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->saveStringDataToSP(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    :goto_1
    iput p1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOldDataConnectionState:I

    goto :goto_0

    .line 346
    :catch_0
    move-exception v4

    .line 347
    .local v4, "e":Ljava/lang/Exception;
    const-string/jumbo v5, "MiuiTelephonyStatistic"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "save KEY_DATA_CONNECTION_STATE_DURATION exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 350
    .end local v2    # "duration":J
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_3
    iget v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOldDataConnectionState:I

    if-ne v5, v6, :cond_0

    if-ne p1, v6, :cond_0

    if-eqz p2, :cond_0

    .line 351
    iget-wide v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStartTime:J

    sub-long v2, v0, v6

    .line 352
    .restart local v2    # "duration":J
    iget-wide v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    add-long/2addr v6, v2

    iput-wide v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    .line 354
    :try_start_1
    const-string/jumbo v5, "data_connection_state_duration"

    iget-wide v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->saveStringDataToSP(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 358
    :goto_2
    iput-wide v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStartTime:J

    goto/16 :goto_0

    .line 355
    :catch_1
    move-exception v4

    .line 356
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string/jumbo v5, "MiuiTelephonyStatistic"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "save KEY_DATA_CONNECTION_STATE_DURATION exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private declared-synchronized recordLogInfo(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 7
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    .line 1301
    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1302
    :cond_0
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    const-string/jumbo v5, "recordLogInfo:invalid params"

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    .line 1303
    return-void

    .line 1305
    :cond_1
    :try_start_1
    const-string/jumbo v4, "SubBussiness"

    invoke-virtual {p1, v4, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1306
    invoke-static {p2}, Lcom/android/phone/statistics/utils/Utils;->convertEventToBitIndex(Ljava/lang/String;)I

    move-result v3

    .line 1307
    .local v3, "ruleEventId":I
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 1308
    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mPreference:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    invoke-virtual {v4}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getRuleEvent()I

    move-result v2

    .line 1309
    .local v2, "ruleEvent":I
    invoke-static {v2, v3}, Lcom/android/phone/statistics/utils/Utils;->hasRuleEvent(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1310
    const-string/jumbo v4, "UserPermission"

    const-string/jumbo v5, "Agree"

    invoke-virtual {p1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1314
    .end local v2    # "ruleEvent":I
    :cond_2
    :try_start_2
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1315
    .local v1, "info":Ljava/lang/String;
    invoke-static {v1}, Lcom/android/phone/statistics/telmqs/MqsasTool;->reportTelephonyException(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v1    # "info":Ljava/lang/String;
    :goto_0
    monitor-exit p0

    .line 1319
    return-void

    .line 1316
    :catch_0
    move-exception v0

    .line 1317
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string/jumbo v4, "MiuiTelephonyStatistic"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "recordLogInfo:e ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "ruleEventId":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private recordNetworkClassDuration(Landroid/telephony/ServiceState;IZ)V
    .locals 18
    .param p1, "ss"    # Landroid/telephony/ServiceState;
    .param p2, "phoneId"    # I
    .param p3, "force"    # Z

    .prologue
    .line 653
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 654
    .local v4, "currentTime":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v12, v12, p2

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getNetworkClass(Landroid/telephony/ServiceState;)I

    move-result v10

    .line 655
    .local v10, "oldNetworkClass":I
    invoke-direct/range {p0 .. p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getNetworkClass(Landroid/telephony/ServiceState;)I

    move-result v8

    .line 656
    .local v8, "newNetworkClass":I
    if-ne v10, v8, :cond_0

    if-eqz p3, :cond_9

    .line 657
    :cond_0
    sget-boolean v12, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v12, :cond_1

    const-string/jumbo v12, "MiuiTelephonyStatistic"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "recordNetworkClassDuration phoneId="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ", newNetworkClass="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, ", oldNetworkClass="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :cond_1
    sget-boolean v12, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v12, :cond_2

    const-string/jumbo v12, "MiuiTelephonyStatistic"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "mLastNetworkClassTime="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastNetworkClassTime:[J

    aget-wide v14, v14, p2

    const-wide/16 v16, 0x3e8

    div-long v14, v14, v16

    invoke-static {v14, v15}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastNetworkClassTime:[J

    aget-wide v12, v12, p2

    const-wide/16 v14, -0x1

    cmp-long v12, v12, v14

    if-eqz v12, :cond_8

    .line 660
    invoke-direct/range {p0 .. p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getAvailableOperatorNumeric(Landroid/telephony/ServiceState;I)Ljava/lang/String;

    move-result-object v2

    .line 661
    .local v2, "currentPlmn":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v12, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ConcurrentHashMap;

    .line 662
    .local v11, "plmnNetworkClassDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    if-nez v11, :cond_3

    .line 663
    new-instance v11, Ljava/util/concurrent/ConcurrentHashMap;

    .end local v11    # "plmnNetworkClassDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    invoke-direct {v11}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 664
    .restart local v11    # "plmnNetworkClassDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v12, v2, v11}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    :cond_3
    if-eqz v11, :cond_8

    .line 667
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastNetworkClassTime:[J

    aget-wide v12, v12, p2

    sub-long v6, v4, v12

    .line 668
    .local v6, "duration":J
    sget-boolean v12, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v12, :cond_4

    const-string/jumbo v12, "MiuiTelephonyStatistic"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "old network class duration="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    :cond_4
    sget-boolean v12, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v12, :cond_5

    const-string/jumbo v12, "MiuiTelephonyStatistic"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "duration="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    :cond_5
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    .line 671
    .local v9, "oldDuration":Ljava/lang/Long;
    if-nez v9, :cond_6

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 672
    :cond_6
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    add-long/2addr v14, v6

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 673
    sget-boolean v12, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v12, :cond_7

    const-string/jumbo v12, "MiuiTelephonyStatistic"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "new network class duration="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    :cond_7
    :try_start_0
    const-string/jumbo v12, "network_class_duration"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v13}, Lcom/android/phone/statistics/utils/ObjectSerializer;->serialize(Ljava/io/Serializable;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->saveStringDataToSP(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 681
    .end local v2    # "currentPlmn":Ljava/lang/String;
    .end local v6    # "duration":J
    .end local v9    # "oldDuration":Ljava/lang/Long;
    .end local v11    # "plmnNetworkClassDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    :cond_8
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastNetworkClassTime:[J

    aput-wide v4, v12, p2

    .line 682
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastPlmnWithNetworkClass:[Ljava/lang/String;

    invoke-direct/range {p0 .. p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getAvailableOperatorNumeric(Landroid/telephony/ServiceState;I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, p2

    .line 684
    :cond_9
    return-void

    .line 676
    .restart local v2    # "currentPlmn":Ljava/lang/String;
    .restart local v6    # "duration":J
    .restart local v9    # "oldDuration":Ljava/lang/Long;
    .restart local v11    # "plmnNetworkClassDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    :catch_0
    move-exception v3

    .line 677
    .local v3, "e":Ljava/lang/Exception;
    const-string/jumbo v12, "MiuiTelephonyStatistic"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "save KEY_NETWORK_CLASS_DURATION exception:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private recordNwOosEvent(I)V
    .locals 6
    .param p1, "phoneId"    # I

    .prologue
    .line 1422
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v3

    if-nez v3, :cond_0

    return-void

    .line 1423
    :cond_0
    invoke-static {p1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 1424
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v3, v3, p1

    invoke-direct {p0, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getVoiceRat(Landroid/telephony/ServiceState;)I

    move-result v2

    .line 1425
    .local v2, "rat":I
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCommonParamInfo()Ljava/util/HashMap;

    move-result-object v0

    .line 1426
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v3, "CellLocation"

    invoke-direct {p0, v1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCellLocation(Lcom/android/internal/telephony/Phone;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1427
    const-string/jumbo v3, "Network"

    const/4 v4, 0x0

    invoke-direct {p0, v4, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getAvailableOperatorNumeric(Landroid/telephony/ServiceState;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1428
    const-string/jumbo v3, "SignalStrength"

    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mSignalStrength:[Landroid/telephony/SignalStrength;

    aget-object v4, v4, p1

    invoke-direct {p0, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDbm(Landroid/telephony/SignalStrength;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1430
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "recordNwOosEvent data="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1432
    invoke-static {v0}, Lcom/android/phone/statistics/utils/Utils;->isValidParams(Ljava/util/Map;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1433
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    const-string/jumbo v4, "recordNwOosEvent invalid paramerters!!!"

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    return-void

    .line 1436
    :cond_1
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Readyfor recordNwOosEvent phoneId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 1438
    const-string/jumbo v3, "telephony_statistic"

    .line 1439
    const-string/jumbo v4, "nw_out_of_service"

    .line 1438
    invoke-static {v3, v4, v0}, Lcom/android/phone/utils/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1440
    const-string/jumbo v3, "nw_out_of_service"

    invoke-direct {p0, v0, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordLogInfo(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 1441
    return-void
.end method

.method private recordNwWithout4GEvent(I)V
    .locals 6
    .param p1, "phoneId"    # I

    .prologue
    .line 1538
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v3

    if-nez v3, :cond_0

    return-void

    .line 1539
    :cond_0
    invoke-static {p1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 1540
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v3, v3, p1

    invoke-direct {p0, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getDataRat(Landroid/telephony/ServiceState;)I

    move-result v2

    .line 1541
    .local v2, "rat":I
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCommonParamInfo()Ljava/util/HashMap;

    move-result-object v0

    .line 1542
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v3, "CellLocation"

    invoke-direct {p0, v1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getCellLocation(Lcom/android/internal/telephony/Phone;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1543
    const-string/jumbo v3, "Network"

    const/4 v4, 0x0

    invoke-direct {p0, v4, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getAvailableOperatorNumeric(Landroid/telephony/ServiceState;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1544
    const-string/jumbo v3, "RAT"

    invoke-static {v2}, Landroid/telephony/ServiceState;->rilRadioTechnologyToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1546
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "recordNwWithout4GEvent data="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1548
    invoke-static {v0}, Lcom/android/phone/statistics/utils/Utils;->isValidParams(Ljava/util/Map;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1549
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    const-string/jumbo v4, "recordNwWithout4GEvent invalid paramerters!!!"

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logE(Ljava/lang/String;Ljava/lang/String;)V

    .line 1550
    return-void

    .line 1552
    :cond_1
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Readyfor recordNwWithout4GEvent phoneId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 1554
    const-string/jumbo v3, "telephony_statistic"

    .line 1555
    const-string/jumbo v4, "nw_without_4g"

    .line 1554
    invoke-static {v3, v4, v0}, Lcom/android/phone/utils/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1556
    const-string/jumbo v3, "nw_without_4g"

    invoke-direct {p0, v0, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordLogInfo(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 1557
    return-void
.end method

.method private recordOutOfServiceCount(JLandroid/telephony/ServiceState;I)V
    .locals 11
    .param p1, "duration"    # J
    .param p3, "ss"    # Landroid/telephony/ServiceState;
    .param p4, "phoneId"    # I

    .prologue
    .line 694
    invoke-direct {p0, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->convertOssDuration(J)Ljava/lang/String;

    move-result-object v3

    .line 695
    .local v3, "oosDuration":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 696
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 697
    .local v1, "currentPlmn":Ljava/lang/String;
    sget-boolean v5, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v5, :cond_0

    const-string/jumbo v5, "MiuiTelephonyStatistic"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "old out of service count="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    :cond_0
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ConcurrentHashMap;

    .line 699
    .local v4, "plmnOutOfServiceCount":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    if-nez v4, :cond_1

    .line 700
    new-instance v4, Ljava/util/concurrent/ConcurrentHashMap;

    .end local v4    # "plmnOutOfServiceCount":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-direct {v4}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 701
    .restart local v4    # "plmnOutOfServiceCount":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v1, v4}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 703
    :cond_1
    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 704
    .local v0, "count":Ljava/lang/Long;
    if-nez v0, :cond_2

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 705
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    sget-boolean v5, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v5, :cond_3

    const-string/jumbo v5, "MiuiTelephonyStatistic"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "new out of service count="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    :cond_3
    :try_start_0
    const-string/jumbo v5, "out_of_service_count"

    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v6}, Lcom/android/phone/statistics/utils/ObjectSerializer;->serialize(Ljava/io/Serializable;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->saveStringDataToSP(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 713
    .end local v0    # "count":Ljava/lang/Long;
    .end local v1    # "currentPlmn":Ljava/lang/String;
    .end local v4    # "plmnOutOfServiceCount":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_4
    :goto_0
    return-void

    .line 709
    .restart local v0    # "count":Ljava/lang/Long;
    .restart local v1    # "currentPlmn":Ljava/lang/String;
    .restart local v4    # "plmnOutOfServiceCount":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :catch_0
    move-exception v2

    .line 710
    .local v2, "e":Ljava/lang/Exception;
    const-string/jumbo v5, "MiuiTelephonyStatistic"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "save KEY_OUT_OF_SERVICE_COUNT exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private recordServiceState(ILandroid/telephony/ServiceState;Z)V
    .locals 2
    .param p1, "phoneId"    # I
    .param p2, "ss"    # Landroid/telephony/ServiceState;
    .param p3, "force"    # Z

    .prologue
    const/4 v1, 0x0

    .line 599
    invoke-direct {p0, p1, p2, v1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordServiceStateDuration(ILandroid/telephony/ServiceState;Z)V

    .line 600
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isDefaultDataSlotId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    invoke-direct {p0, p2, p1, v1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordNetworkClassDuration(Landroid/telephony/ServiceState;IZ)V

    .line 603
    :cond_0
    return-void
.end method

.method private recordServiceStateDuration(ILandroid/telephony/ServiceState;Z)V
    .locals 20
    .param p1, "phoneId"    # I
    .param p2, "ss"    # Landroid/telephony/ServiceState;
    .param p3, "force"    # Z

    .prologue
    .line 606
    if-eqz p2, :cond_0

    invoke-direct/range {p0 .. p1}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isValidatePhoneId(I)Z

    move-result v14

    xor-int/lit8 v14, v14, 0x1

    if-eqz v14, :cond_1

    .line 607
    :cond_0
    return-void

    .line 609
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 610
    .local v6, "currentTime":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceState:[Landroid/telephony/ServiceState;

    aget-object v14, v14, p1

    invoke-virtual {v14}, Landroid/telephony/ServiceState;->getState()I

    move-result v12

    .line 611
    .local v12, "oldServiceState":I
    invoke-virtual/range {p2 .. p2}, Landroid/telephony/ServiceState;->getState()I

    move-result v10

    .line 613
    .local v10, "newServiceState":I
    if-ne v12, v10, :cond_2

    if-eqz p3, :cond_b

    .line 614
    :cond_2
    sget-boolean v14, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v14, :cond_3

    const-string/jumbo v14, "MiuiTelephonyStatistic"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "recordServiceStateDuration phoneId="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, "newSS="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ", oldSS="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    :cond_3
    sget-boolean v14, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v14, :cond_4

    const-string/jumbo v14, "MiuiTelephonyStatistic"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "mLastServiceStateTime="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastServiceStateTime:[J

    move-object/from16 v16, v0

    aget-wide v16, v16, p1

    const-wide/16 v18, 0x3e8

    div-long v16, v16, v18

    invoke-static/range {v16 .. v17}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->updateOutOfServiceStep(Landroid/telephony/ServiceState;I)V

    .line 619
    const-wide/16 v8, 0x0

    .line 620
    .local v8, "duration":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastServiceStateTime:[J

    aget-wide v14, v14, p1

    const-wide/16 v16, -0x1

    cmp-long v14, v14, v16

    if-eqz v14, :cond_a

    .line 621
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastPlmnWithServiceState:[Ljava/lang/String;

    aget-object v15, v15, p1

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 622
    .local v4, "currentPlmn":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v14, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ConcurrentHashMap;

    .line 623
    .local v13, "plmnServiceStateDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    if-nez v13, :cond_5

    .line 624
    new-instance v13, Ljava/util/concurrent/ConcurrentHashMap;

    .end local v13    # "plmnServiceStateDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    invoke-direct {v13}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 625
    .restart local v13    # "plmnServiceStateDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v14, v4, v13}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 627
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastServiceStateTime:[J

    aget-wide v14, v14, p1

    sub-long v8, v6, v14

    .line 628
    sget-boolean v14, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v14, :cond_6

    const-string/jumbo v14, "MiuiTelephonyStatistic"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "old service state duration="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    :cond_6
    sget-boolean v14, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v14, :cond_7

    const-string/jumbo v14, "MiuiTelephonyStatistic"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "duration="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    :cond_7
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    .line 631
    .local v11, "oldDuration":Ljava/lang/Long;
    if-nez v11, :cond_8

    const-wide/16 v14, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 632
    :cond_8
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    add-long v16, v16, v8

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 633
    sget-boolean v14, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v14, :cond_9

    const-string/jumbo v14, "MiuiTelephonyStatistic"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "new service state duration="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    :cond_9
    :try_start_0
    const-string/jumbo v14, "service_state_duration"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v15}, Lcom/android/phone/statistics/utils/ObjectSerializer;->serialize(Ljava/io/Serializable;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->saveStringDataToSP(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 642
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aget-object v14, v14, p1

    sget-object v15, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->RESUME:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    if-ne v14, v15, :cond_a

    .line 643
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-direct {v0, v8, v9, v1, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->recordOutOfServiceCount(JLandroid/telephony/ServiceState;I)V

    .line 644
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    sget-object v15, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->CAMP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v15, v14, p1

    .line 647
    .end local v4    # "currentPlmn":Ljava/lang/String;
    .end local v11    # "oldDuration":Ljava/lang/Long;
    .end local v13    # "plmnServiceStateDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastServiceStateTime:[J

    aput-wide v6, v14, p1

    .line 648
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mLastPlmnWithServiceState:[Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getAvailableOperatorNumeric(Landroid/telephony/ServiceState;I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v14, p1

    .line 650
    .end local v8    # "duration":J
    :cond_b
    return-void

    .line 636
    .restart local v4    # "currentPlmn":Ljava/lang/String;
    .restart local v8    # "duration":J
    .restart local v11    # "oldDuration":Ljava/lang/Long;
    .restart local v13    # "plmnServiceStateDuration":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Integer;Ljava/lang/Long;>;"
    :catch_0
    move-exception v5

    .line 637
    .local v5, "e":Ljava/lang/Exception;
    const-string/jumbo v14, "MiuiTelephonyStatistic"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "save KEY_SERVIC_STATE_DURATION exception:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerReceiver()V
    .locals 3

    .prologue
    .line 287
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 288
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 289
    const-string/jumbo v1, "android.intent.action.ACTION_IMS_REGISTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 290
    const-string/jumbo v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 291
    const-string/jumbo v1, "miui.intent.action.ACTION_DEFAULT_DATA_SLOT_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 292
    const-string/jumbo v1, "miui.intent.action.CLOUD_MODEM_LOG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 293
    const-string/jumbo v1, "telephonyStatistics.intent.debugall"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 294
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 295
    return-void
.end method

.method private registerTelephonyCallBack()V
    .locals 2

    .prologue
    .line 272
    sget-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mTelephonyCallBack:Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;

    if-nez v0, :cond_0

    .line 273
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "new  TelephonyCallBack"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    new-instance v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;

    invoke-direct {v0, p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;-><init>(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)V

    sput-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mTelephonyCallBack:Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;

    .line 275
    sget-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mTelephonyCallBack:Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;

    invoke-static {v0}, Lmiui/telephony/TelephonyStatAdapter;->registerCallBack(Lmiui/telephony/TelephonyStatAdapter$CallBack;)V

    .line 277
    :cond_0
    return-void
.end method

.method private reloadStatisticDataFromSP()V
    .locals 8

    .prologue
    .line 852
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "telephony_statistic_data"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 854
    .local v2, "sp":Landroid/content/SharedPreferences;
    :try_start_0
    const-string/jumbo v3, "service_state_duration"

    const-string/jumbo v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 855
    .local v0, "dbData":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 856
    invoke-static {v0}, Lcom/android/phone/statistics/utils/ObjectSerializer;->deserialize(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap;

    iput-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    .line 858
    :cond_0
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reloadStatisticDataFromSP mServiceStateDuration="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    const-string/jumbo v3, "network_class_duration"

    const-string/jumbo v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 861
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 862
    invoke-static {v0}, Lcom/android/phone/statistics/utils/ObjectSerializer;->deserialize(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap;

    iput-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    .line 864
    :cond_1
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reloadStatisticDataFromSP mNetworkClassDuration="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    const-string/jumbo v3, "volte_duration"

    const-string/jumbo v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 867
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 868
    invoke-static {v0}, Lcom/android/phone/statistics/utils/ObjectSerializer;->deserialize(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap;

    iput-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mVoLTERegisteredDuration:Ljava/util/concurrent/ConcurrentHashMap;

    .line 870
    :cond_2
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reloadStatisticDataFromSP mVoLTERegisteredDuration="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mVoLTERegisteredDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    const-string/jumbo v3, "data_connection_state_duration"

    const-string/jumbo v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 873
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 874
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    .line 876
    :cond_3
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reloadStatisticDataFromSP mDataConnectionStateDuration="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    const-string/jumbo v3, "out_of_service_count"

    const-string/jumbo v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 879
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 880
    invoke-static {v0}, Lcom/android/phone/statistics/utils/ObjectSerializer;->deserialize(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap;

    iput-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    .line 882
    :cond_4
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reloadStatisticDataFromSP mOutOfServiceCount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 886
    .end local v0    # "dbData":Ljava/lang/String;
    :goto_0
    return-void

    .line 883
    :catch_0
    move-exception v1

    .line 884
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reloadStatisticDataFromSP exception="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static reportTelephonyStatisticData()V
    .locals 7

    .prologue
    .line 889
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    const-string/jumbo v4, "reportTelephonyStatisticData enter"

    invoke-static {v3, v4}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "com.android.phone"

    const-string/jumbo v5, "statis_enable"

    const/4 v6, 0x1

    .line 890
    invoke-static {v3, v4, v5, v6}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    .line 892
    .local v0, "cloudStatisEnabled":Z
    invoke-static {}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getInstance()Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getStatisticEnabled()Z

    move-result v3

    if-eq v3, v0, :cond_2

    .line 893
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reportTelephonyStatisticData cloudStatisEnabled="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    invoke-static {}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getInstance()Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setStatisticEnabled(Z)V

    .line 895
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getInstance()Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    move-result-object v3

    if-nez v3, :cond_1

    .line 896
    invoke-static {}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->make()Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .line 900
    :cond_0
    :goto_0
    return-void

    .line 897
    :cond_1
    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getInstance()Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 898
    invoke-static {}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getInstance()Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    move-result-object v3

    invoke-direct {v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->dispose()V

    goto :goto_0

    .line 902
    :cond_2
    invoke-static {}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getInstance()Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    move-result-object v2

    .line 903
    .local v2, "statisticInstance":Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    if-eqz v2, :cond_3

    .line 904
    invoke-virtual {v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getStatisticData()Ljava/lang/String;

    move-result-object v1

    .line 905
    .local v1, "statisticData":Ljava/lang/String;
    invoke-static {v1}, Lcom/android/phone/utils/MiStatInterfaceUtil;->addCustomEvent(Ljava/lang/String;)V

    .line 906
    const-string/jumbo v3, "MiuiTelephonyStatistic"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reportTelephonyStatisticData data="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    invoke-virtual {v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->clearTelephonyStatisticData()V

    .line 909
    invoke-static {}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getInstance()Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->revokeUserPermission()V

    .line 911
    .end local v1    # "statisticData":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method private saveStringDataToSP(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 838
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "telephony_statistic_data"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 839
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 840
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 841
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 842
    return-void
.end method

.method private static shouldStatistic()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 198
    invoke-static {}, Lcom/android/phone/statistics/utils/DeviceUtil;->isQcomPlatform()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "isQcomPlatform return!"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    return v2

    .line 203
    :cond_0
    invoke-static {}, Lcom/android/phone/statistics/utils/DeviceUtil;->isLegalReleaseVersion()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/phone/statistics/utils/DeviceUtil;->isValidVersion()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 204
    :cond_1
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "Version & Model return!"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    return v2

    .line 208
    :cond_2
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/statistics/utils/Utils;->isUserExperienceProgramEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 209
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "User Setting return!"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    return v2

    .line 213
    :cond_3
    invoke-static {}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getInstance()Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getStatisticEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 214
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "Cloud Control return!"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    return v2

    .line 217
    :cond_4
    const/4 v0, 0x1

    return v0
.end method

.method private showModemDialog(I)V
    .locals 3
    .param p1, "ruleEventId"    # I

    .prologue
    .line 1614
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "showModemDialog: Enter"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1615
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mUserPermissionDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mUserPermissionDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1616
    return-void

    .line 1618
    :cond_0
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 1619
    new-instance v0, Lcom/android/phone/statistics/ui/ModemLogDialog;

    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->MdlogCallBack:Lcom/android/phone/statistics/ui/ModemLogDialog$DialogCallBack;

    invoke-direct {v0, v1, v2, p1}, Lcom/android/phone/statistics/ui/ModemLogDialog;-><init>(Landroid/content/Context;Lcom/android/phone/statistics/ui/ModemLogDialog$DialogCallBack;I)V

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mUserPermissionDialog:Landroid/app/Dialog;

    .line 1620
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mUserPermissionDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1622
    :cond_1
    return-void
.end method

.method private startLteMonitorTimer(I)V
    .locals 6
    .param p1, "phoneId"    # I

    .prologue
    .line 1469
    const-string/jumbo v1, "MiuiTelephonyStatistic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startLteMonitorTimer phoneId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1470
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->stopLteMonitorTimer()V

    .line 1471
    const v0, 0xea60

    .line 1472
    .local v0, "delayInMs":I
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    .line 1473
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0xd

    invoke-virtual {v2, v4, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/32 v4, 0xea60

    .line 1472
    invoke-virtual {v1, v2, v4, v5}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1474
    return-void
.end method

.method private startNwOosTimer(I)V
    .locals 6
    .param p1, "phoneId"    # I

    .prologue
    .line 1354
    const-string/jumbo v1, "MiuiTelephonyStatistic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startNwOosTimer phoneId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1355
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->stopNwOosTimer()V

    .line 1356
    const v0, 0xea60

    .line 1357
    .local v0, "delayInMs":I
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    .line 1358
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0xc

    invoke-virtual {v2, v4, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/32 v4, 0xea60

    .line 1357
    invoke-virtual {v1, v2, v4, v5}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1359
    return-void
.end method

.method private stopLteMonitorTimer()V
    .locals 3

    .prologue
    const/16 v2, 0xd

    .line 1477
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    invoke-virtual {v0, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1478
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "stopLteMonitorTimer"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1479
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    invoke-virtual {v0, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->removeMessages(I)V

    .line 1481
    :cond_0
    return-void
.end method

.method private stopNwOosTimer()V
    .locals 3

    .prologue
    const/16 v2, 0xc

    .line 1362
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    invoke-virtual {v0, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1363
    const-string/jumbo v0, "MiuiTelephonyStatistic"

    const-string/jumbo v1, "stopNwOosTimer"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1364
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mStatisticHandler:Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    invoke-virtual {v0, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->removeMessages(I)V

    .line 1366
    :cond_0
    return-void
.end method

.method private unregisterTelephonyCallBack()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 280
    sget-object v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mTelephonyCallBack:Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;

    if-eqz v0, :cond_0

    .line 281
    invoke-static {}, Lmiui/telephony/TelephonyStatAdapter;->unregisterCallBack()V

    .line 282
    sput-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mTelephonyCallBack:Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;

    .line 284
    :cond_0
    return-void
.end method

.method private updateOutOfServiceStep(Landroid/telephony/ServiceState;I)V
    .locals 3
    .param p1, "state"    # Landroid/telephony/ServiceState;
    .param p2, "phoneId"    # I

    .prologue
    .line 578
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-nez v0, :cond_3

    .line 579
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aget-object v0, v0, p2

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->DROP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    if-ne v0, v1, :cond_2

    .line 580
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->RESUME:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, p2

    .line 595
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->DBG:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "MiuiTelephonyStatistic"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateOutOfServiceStep phoneId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", step="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_1
    return-void

    .line 582
    :cond_2
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->CAMP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, p2

    goto :goto_0

    .line 584
    :cond_3
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 585
    :cond_4
    invoke-direct {p0, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isSimReady(I)Z

    move-result v0

    .line 584
    if-eqz v0, :cond_5

    .line 586
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aget-object v0, v0, p2

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->CAMP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    if-ne v0, v1, :cond_0

    .line 587
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->DROP:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, p2

    goto :goto_0

    .line 590
    :cond_5
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    .line 591
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->POWEROFF:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, p2

    goto :goto_0

    .line 592
    :cond_6
    invoke-direct {p0, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->isSimReady(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceStep:[Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    sget-object v1, Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;->INIT:Lcom/android/phone/statistics/MiuiTelephonyStatistic$OOSStep;

    aput-object v1, v0, p2

    goto :goto_0
.end method


# virtual methods
.method public clearTelephonyStatisticData()V
    .locals 5

    .prologue
    .line 932
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 933
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 934
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mVoLTERegisteredDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 935
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 936
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    .line 937
    iget-object v2, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "telephony_statistic_data"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 938
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 939
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 940
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 941
    return-void
.end method

.method public getStatisticData()Ljava/lang/String;
    .locals 6

    .prologue
    .line 944
    invoke-direct {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->forceUpdateStatisticData()V

    .line 945
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 946
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v3, "ServiceStateDuration:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 947
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mServiceStateDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p0, v3, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->convertStatisticDuration(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/StringBuilder;)V

    .line 948
    const-string/jumbo v3, "NetworkClassDuration:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 949
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mNetworkClassDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p0, v3, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->convertStatisticDuration(Ljava/util/concurrent/ConcurrentHashMap;Ljava/lang/StringBuilder;)V

    .line 950
    const-string/jumbo v3, "VoLTERegisteredDuration:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 951
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mVoLTERegisteredDuration:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 952
    .local v0, "iter":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 953
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 954
    .local v1, "plmnDurationEntry":Ljava/util/Map$Entry;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "={"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 955
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, v2}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    .line 956
    const-string/jumbo v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 957
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 958
    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 961
    .end local v1    # "plmnDurationEntry":Ljava/util/Map$Entry;
    :cond_1
    const-string/jumbo v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 962
    const-string/jumbo v3, "OutOfServiceCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mOutOfServiceCount:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 963
    const-string/jumbo v3, "VolteSwitch:{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->getVolteEnabledStatus(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 964
    const-string/jumbo v3, "DataConnectionStateDuration:{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->mDataConnectionStateDuration:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 965
    const-string/jumbo v3, "UpTime:{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 966
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5, v2}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    .line 967
    const-string/jumbo v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 968
    const-string/jumbo v3, "Version:{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 969
    const-string/jumbo v3, "UserDisagreeCounter:{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getInstance()Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getUserDisagreeCounter()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 970
    const-string/jumbo v3, "RuleEvent:{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getInstance()Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getRuleEvent()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 971
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
