.class public Lcom/android/phone/CallForwardEditPreference;
.super Lcom/android/phone/EditPhoneNumberPreference;
.source "CallForwardEditPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/CallForwardEditPreference$1;,
        Lcom/android/phone/CallForwardEditPreference$MyHandler;
    }
.end annotation


# static fields
.field private static final SRC_TAGS:[Ljava/lang/String;


# instance fields
.field callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

.field private imsInterfaceListener:Lorg/codeaurora/ims/QtiImsExtListenerBaseImpl;

.field isTimerEnabled:Z

.field mAllowSetCallFwding:Z

.field private mButtonClicked:I

.field private mContext:Landroid/content/Context;

.field private mEndHour:I

.field private mEndMinute:I

.field private mHandler:Lcom/android/phone/CallForwardEditPreference$MyHandler;

.field private mNumber:Ljava/lang/String;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mQtiImsExtManager:Lorg/codeaurora/ims/QtiImsExtManager;

.field private mReplaceInvalidCFNumber:Z

.field private mServiceClass:I

.field private mStartHour:I

.field private mStartMinute:I

.field private mStatus:I

.field private mSummaryOnTemplate:Ljava/lang/CharSequence;

.field private mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

.field reason:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/CallForwardEditPreference;)Lorg/codeaurora/ims/QtiImsExtListenerBaseImpl;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CallForwardEditPreference;->imsInterfaceListener:Lorg/codeaurora/ims/QtiImsExtListenerBaseImpl;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/CallForwardEditPreference;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/CallForwardEditPreference;)Lorg/codeaurora/ims/QtiImsExtManager;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CallForwardEditPreference;->mQtiImsExtManager:Lorg/codeaurora/ims/QtiImsExtManager;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/CallForwardEditPreference;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;

    .prologue
    iget v0, p0, Lcom/android/phone/CallForwardEditPreference;->mServiceClass:I

    return v0
.end method

.method static synthetic -get4(Lcom/android/phone/CallForwardEditPreference;)Lcom/android/phone/TimeConsumingPreferenceListener;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CallForwardEditPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/CallForwardEditPreference;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/CallForwardEditPreference;->mEndHour:I

    return p1
.end method

.method static synthetic -set1(Lcom/android/phone/CallForwardEditPreference;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/CallForwardEditPreference;->mEndMinute:I

    return p1
.end method

.method static synthetic -set2(Lcom/android/phone/CallForwardEditPreference;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;
    .param p1, "-value"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/android/phone/CallForwardEditPreference;->mNumber:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -set3(Lcom/android/phone/CallForwardEditPreference;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/CallForwardEditPreference;->mStartHour:I

    return p1
.end method

.method static synthetic -set4(Lcom/android/phone/CallForwardEditPreference;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/CallForwardEditPreference;->mStartMinute:I

    return p1
.end method

.method static synthetic -set5(Lcom/android/phone/CallForwardEditPreference;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/CallForwardEditPreference;->mStatus:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/CallForwardEditPreference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/CallForwardEditPreference;->handleGetCFTimerResponse()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/CallForwardEditPreference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CallForwardEditPreference;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/CallForwardEditPreference;->updateSummaryText()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "{0}"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/phone/CallForwardEditPreference;->SRC_TAGS:[Ljava/lang/String;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/CallForwardEditPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/android/phone/EditPhoneNumberPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v1, Lcom/android/phone/CallForwardEditPreference$MyHandler;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/phone/CallForwardEditPreference$MyHandler;-><init>(Lcom/android/phone/CallForwardEditPreference;Lcom/android/phone/CallForwardEditPreference$MyHandler;)V

    iput-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mHandler:Lcom/android/phone/CallForwardEditPreference$MyHandler;

    .line 54
    iput-boolean v3, p0, Lcom/android/phone/CallForwardEditPreference;->mReplaceInvalidCFNumber:Z

    .line 57
    iput-boolean v3, p0, Lcom/android/phone/CallForwardEditPreference;->mAllowSetCallFwding:Z

    .line 299
    new-instance v1, Lcom/android/phone/CallForwardEditPreference$1;

    invoke-direct {v1, p0}, Lcom/android/phone/CallForwardEditPreference$1;-><init>(Lcom/android/phone/CallForwardEditPreference;)V

    .line 298
    iput-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->imsInterfaceListener:Lorg/codeaurora/ims/QtiImsExtListenerBaseImpl;

    .line 71
    invoke-virtual {p0}, Lcom/android/phone/CallForwardEditPreference;->getSummaryOn()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mSummaryOnTemplate:Ljava/lang/CharSequence;

    .line 72
    iput-object p1, p0, Lcom/android/phone/CallForwardEditPreference;->mContext:Landroid/content/Context;

    .line 75
    sget-object v1, Lcom/android/phone/R$styleable;->CallForwardEditPreference:[I

    const v2, 0x7f0c0194

    .line 74
    invoke-virtual {p1, p2, v1, v3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 76
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    .line 78
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 80
    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mServiceClass="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/phone/CallForwardEditPreference;->mServiceClass:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", reason="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return-void
.end method

.method private getCurrentCountryIso()Ljava/lang/String;
    .locals 3

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/android/phone/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 366
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_0

    .line 367
    const-string/jumbo v1, ""

    return-object v1

    .line 369
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private handleGetCFTimerResponse()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 343
    const-string/jumbo v0, "CallForwardEditPreference"

    const-string/jumbo v1, "handleGetCFTimerResponse: done"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    iget-boolean v0, p0, Lcom/android/phone/CallForwardEditPreference;->mAllowSetCallFwding:Z

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/android/phone/CallForwardEditPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    invoke-interface {v0, p0, v2}, Lcom/android/phone/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    .line 346
    iput-boolean v2, p0, Lcom/android/phone/CallForwardEditPreference;->mAllowSetCallFwding:Z

    .line 350
    :goto_0
    invoke-virtual {p0}, Lcom/android/phone/CallForwardEditPreference;->handleCallForwardTimerResult()V

    .line 351
    invoke-direct {p0}, Lcom/android/phone/CallForwardEditPreference;->updateSummaryText()V

    .line 352
    return-void

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/android/phone/CallForwardEditPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/android/phone/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    goto :goto_0
.end method

.method private isTimerEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 123
    const-string/jumbo v1, "persist.radio.ims.cmcc"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v1, :cond_1

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    .line 126
    iget-object v2, p0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "config_enable_cfu_time"

    .line 125
    invoke-static {v1, v2, v3}, Lorg/codeaurora/ims/utils/QtiImsExtUtils;->isCarrierConfigEnabled(ILandroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 123
    if-eqz v1, :cond_1

    .line 127
    iget-object v0, p0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v0

    .line 123
    :cond_1
    return v0
.end method

.method private isTimerValid()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 356
    iget v2, p0, Lcom/android/phone/CallForwardEditPreference;->mStartHour:I

    if-nez v2, :cond_0

    iget v2, p0, Lcom/android/phone/CallForwardEditPreference;->mStartMinute:I

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lcom/android/phone/CallForwardEditPreference;->mEndHour:I

    if-nez v2, :cond_0

    iget v2, p0, Lcom/android/phone/CallForwardEditPreference;->mEndMinute:I

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private updateSummaryText()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 271
    const-string/jumbo v6, "CallForwardEditPreference"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "updateSummaryText, complete fetching for reason "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-virtual {p0}, Lcom/android/phone/CallForwardEditPreference;->isToggled()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 273
    invoke-virtual {p0}, Lcom/android/phone/CallForwardEditPreference;->getRawPhoneNumber()Ljava/lang/String;

    move-result-object v0

    .line 274
    .local v0, "number":Ljava/lang/String;
    iget v6, p0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    if-nez v6, :cond_0

    .line 275
    iget-boolean v6, p0, Lcom/android/phone/CallForwardEditPreference;->isTimerEnabled:Z

    .line 274
    if-eqz v6, :cond_0

    .line 275
    invoke-direct {p0}, Lcom/android/phone/CallForwardEditPreference;->isTimerValid()Z

    move-result v6

    .line 274
    if-eqz v6, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/android/phone/CallForwardEditPreference;->getRawPhoneNumberWithTime()Ljava/lang/String;

    move-result-object v0

    .line 278
    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 280
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v6

    .line 281
    sget-object v7, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 280
    invoke-virtual {v6, v0, v7}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v5

    .line 282
    .local v5, "wrappedNumber":Ljava/lang/String;
    const/4 v6, 0x1

    new-array v4, v6, [Ljava/lang/String;

    aput-object v5, v4, v9

    .line 284
    .local v4, "values":[Ljava/lang/String;
    iget-object v6, p0, Lcom/android/phone/CallForwardEditPreference;->mSummaryOnTemplate:Ljava/lang/CharSequence;

    sget-object v7, Lcom/android/phone/CallForwardEditPreference;->SRC_TAGS:[Ljava/lang/String;

    invoke-static {v6, v7, v4}, Landroid/text/TextUtils;->replace(Ljava/lang/CharSequence;[Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 283
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 285
    .local v3, "summaryOn":Ljava/lang/String;
    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 287
    .local v2, "start":I
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 289
    .local v1, "spannableSummaryOn":Landroid/text/SpannableString;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v2

    .line 288
    invoke-static {v1, v2, v6}, Landroid/telephony/PhoneNumberUtils;->addTtsSpan(Landroid/text/Spannable;II)V

    .line 290
    invoke-virtual {p0, v1}, Lcom/android/phone/CallForwardEditPreference;->setSummaryOn(Ljava/lang/CharSequence;)Lcom/android/phone/EditPhoneNumberPreference;

    .line 296
    .end local v0    # "number":Ljava/lang/String;
    .end local v1    # "spannableSummaryOn":Landroid/text/SpannableString;
    .end local v2    # "start":I
    .end local v3    # "summaryOn":Ljava/lang/String;
    .end local v4    # "values":[Ljava/lang/String;
    .end local v5    # "wrappedNumber":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 292
    .restart local v0    # "number":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/android/phone/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b033c

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/android/phone/CallForwardEditPreference;->setSummaryOn(Ljava/lang/CharSequence;)Lcom/android/phone/EditPhoneNumberPreference;

    goto :goto_0
.end method


# virtual methods
.method handleCallForwardResult(Lcom/android/internal/telephony/CallForwardInfo;)V
    .locals 5
    .param p1, "cf"    # Lcom/android/internal/telephony/CallForwardInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 248
    iput-object p1, p0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    .line 249
    const-string/jumbo v2, "CallForwardEditPreference"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "handleGetCFResponse done, callForwardInfo="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iget v2, p0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    if-nez v2, :cond_0

    .line 251
    iput v1, p0, Lcom/android/phone/CallForwardEditPreference;->mStartHour:I

    .line 252
    iput v1, p0, Lcom/android/phone/CallForwardEditPreference;->mStartMinute:I

    .line 253
    iput v1, p0, Lcom/android/phone/CallForwardEditPreference;->mEndHour:I

    .line 254
    iput v1, p0, Lcom/android/phone/CallForwardEditPreference;->mEndMinute:I

    .line 260
    :cond_0
    iget-boolean v2, p0, Lcom/android/phone/CallForwardEditPreference;->mReplaceInvalidCFNumber:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget-object v2, v2, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    .line 261
    invoke-direct {p0}, Lcom/android/phone/CallForwardEditPreference;->getCurrentCountryIso()Ljava/lang/String;

    move-result-object v3

    .line 260
    invoke-static {v2, v3}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 262
    iget-object v2, p0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    invoke-virtual {p0}, Lcom/android/phone/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b0320

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    .line 263
    const-string/jumbo v2, "CallForwardEditPreference"

    const-string/jumbo v3, "handleGetCFResponse: Overridding CF number"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_1
    iget-object v2, p0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget v2, v2, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    if-ne v2, v0, :cond_2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/phone/CallForwardEditPreference;->setToggled(Z)Lcom/android/phone/EditPhoneNumberPreference;

    .line 267
    iget-object v0, p0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget-object v0, v0, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/phone/CallForwardEditPreference;->setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;

    .line 268
    return-void

    :cond_2
    move v0, v1

    .line 266
    goto :goto_0
.end method

.method handleCallForwardTimerResult()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 236
    const-string/jumbo v0, "CallForwardEditPreference"

    const-string/jumbo v3, "handleCallForwardTimerResult: "

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget v0, p0, Lcom/android/phone/CallForwardEditPreference;->mStatus:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/phone/CallForwardEditPreference;->setToggled(Z)Lcom/android/phone/EditPhoneNumberPreference;

    .line 238
    iget-object v0, p0, Lcom/android/phone/CallForwardEditPreference;->mNumber:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/phone/CallForwardEditPreference;->setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/EditPhoneNumberPreference;

    .line 240
    iget v0, p0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    if-nez v0, :cond_1

    .line 241
    iget v0, p0, Lcom/android/phone/CallForwardEditPreference;->mStatus:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/phone/CallForwardEditPreference;->isTimerValid()Z

    move-result v2

    :cond_0
    xor-int/lit8 v0, v2, 0x1

    invoke-virtual {p0, v0}, Lcom/android/phone/CallForwardEditPreference;->setAllDayCheckBox(Z)V

    .line 243
    iget-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mNumber:Ljava/lang/String;

    iget v2, p0, Lcom/android/phone/CallForwardEditPreference;->mStartHour:I

    iget v3, p0, Lcom/android/phone/CallForwardEditPreference;->mStartMinute:I

    iget v4, p0, Lcom/android/phone/CallForwardEditPreference;->mEndHour:I

    iget v5, p0, Lcom/android/phone/CallForwardEditPreference;->mEndMinute:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/phone/CallForwardEditPreference;->setPhoneNumberWithTimePeriod(Ljava/lang/String;IIII)Lcom/android/phone/EditPhoneNumberPreference;

    .line 245
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 237
    goto :goto_0
.end method

.method init(Lcom/android/phone/TimeConsumingPreferenceListener;ZLcom/android/internal/telephony/Phone;ZI)V
    .locals 8
    .param p1, "listener"    # Lcom/android/phone/TimeConsumingPreferenceListener;
    .param p2, "skipReading"    # Z
    .param p3, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p4, "replaceInvalidCFNumber"    # Z
    .param p5, "serviceClass"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 89
    iput p5, p0, Lcom/android/phone/CallForwardEditPreference;->mServiceClass:I

    .line 90
    iput-object p3, p0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 91
    iput-object p1, p0, Lcom/android/phone/CallForwardEditPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    .line 92
    invoke-direct {p0}, Lcom/android/phone/CallForwardEditPreference;->isTimerEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/phone/CallForwardEditPreference;->isTimerEnabled:Z

    .line 93
    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "isTimerEnabled="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/phone/CallForwardEditPreference;->isTimerEnabled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iput-boolean p4, p0, Lcom/android/phone/CallForwardEditPreference;->mReplaceInvalidCFNumber:Z

    .line 96
    if-nez p2, :cond_0

    .line 97
    iget v1, p0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/phone/CallForwardEditPreference;->isTimerEnabled:Z

    if-eqz v1, :cond_1

    .line 98
    invoke-virtual {p0, v6}, Lcom/android/phone/CallForwardEditPreference;->setTimeSettingVisibility(Z)V

    .line 100
    :try_start_0
    new-instance v1, Lorg/codeaurora/ims/QtiImsExtManager;

    iget-object v2, p0, Lcom/android/phone/CallForwardEditPreference;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lorg/codeaurora/ims/QtiImsExtManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mQtiImsExtManager:Lorg/codeaurora/ims/QtiImsExtManager;

    .line 101
    iget-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mQtiImsExtManager:Lorg/codeaurora/ims/QtiImsExtManager;

    iget-object v2, p0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    .line 102
    iget v3, p0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    .line 103
    iget v4, p0, Lcom/android/phone/CallForwardEditPreference;->mServiceClass:I

    .line 104
    iget-object v5, p0, Lcom/android/phone/CallForwardEditPreference;->imsInterfaceListener:Lorg/codeaurora/ims/QtiImsExtListenerBaseImpl;

    .line 101
    invoke-virtual {v1, v2, v3, v4, v5}, Lorg/codeaurora/ims/QtiImsExtManager;->getCallForwardUncondTimer(IIILorg/codeaurora/ims/internal/IQtiImsExtListener;)V
    :try_end_0
    .catch Lorg/codeaurora/ims/QtiImsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    iget-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    if-eqz v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    invoke-interface {v1, p0, v6}, Lcom/android/phone/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    .line 119
    :cond_0
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Lorg/codeaurora/ims/QtiImsException;
    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getCallForwardUncondTimer failed. Exception = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 109
    .end local v0    # "e":Lorg/codeaurora/ims/QtiImsException;
    :cond_1
    iget-object v1, p0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    iget v2, p0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    iget v3, p0, Lcom/android/phone/CallForwardEditPreference;->mServiceClass:I

    .line 110
    iget-object v4, p0, Lcom/android/phone/CallForwardEditPreference;->mHandler:Lcom/android/phone/CallForwardEditPreference$MyHandler;

    invoke-virtual {v4, v5, v5, v5, v7}, Lcom/android/phone/CallForwardEditPreference$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 109
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/internal/telephony/Phone;->getCallForwardingOption(IILandroid/os/Message;)V

    goto :goto_0
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 133
    const/4 v0, -0x2

    iput v0, p0, Lcom/android/phone/CallForwardEditPreference;->mButtonClicked:I

    .line 134
    invoke-super {p0, p1}, Lcom/android/phone/EditPhoneNumberPreference;->onBindDialogView(Landroid/view/View;)V

    .line 135
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 139
    invoke-super {p0, p1, p2}, Lcom/android/phone/EditPhoneNumberPreference;->onClick(Landroid/content/DialogInterface;I)V

    .line 140
    iput p2, p0, Lcom/android/phone/CallForwardEditPreference;->mButtonClicked:I

    .line 141
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 20
    .param p1, "positiveResult"    # Z

    .prologue
    .line 145
    invoke-super/range {p0 .. p1}, Lcom/android/phone/EditPhoneNumberPreference;->onDialogClosed(Z)V

    .line 147
    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "mButtonClicked="

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/phone/CallForwardEditPreference;->mButtonClicked:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v8, ", positiveResult="

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/phone/CallForwardEditPreference;->mButtonClicked:I

    const/4 v2, -0x2

    if-eq v1, v2, :cond_2

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->isToggled()Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/phone/CallForwardEditPreference;->mButtonClicked:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 152
    :cond_0
    const/4 v7, 0x3

    .line 154
    .local v7, "action":I
    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    const/16 v16, 0x0

    .line 155
    .local v16, "time":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->getPhoneNumber()Ljava/lang/String;

    move-result-object v10

    .line 156
    .local v10, "number":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->isAllDayChecked()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v3, 0x0

    .line 157
    .local v3, "editStartHour":I
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->isAllDayChecked()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v4, 0x0

    .line 158
    .local v4, "editStartMinute":I
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->isAllDayChecked()Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v5, 0x0

    .line 159
    .local v5, "editEndHour":I
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->isAllDayChecked()Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v6, 0x0

    .line 160
    .local v6, "editEndMinute":I
    :goto_5
    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "callForwardInfo="

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/16 v19, 0x1

    .line 163
    .local v19, "isCFSettingChanged":Z
    const/4 v1, 0x3

    if-ne v7, v1, :cond_1

    .line 164
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    if-eqz v1, :cond_1

    .line 165
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget v1, v1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 166
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget-object v1, v1, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 163
    if-eqz v1, :cond_1

    .line 167
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    if-nez v1, :cond_d

    .line 169
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->isAllDayChecked()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 170
    invoke-direct/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->isTimerValid()Z

    move-result v19

    .line 183
    .end local v19    # "isCFSettingChanged":Z
    :cond_1
    :goto_6
    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "isCFSettingChanged = "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    if-eqz v19, :cond_2

    .line 186
    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "reason="

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v8, ", action="

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 187
    const-string/jumbo v8, ", number="

    .line 186
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const-string/jumbo v1, ""

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/android/phone/CallForwardEditPreference;->setSummaryOn(Ljava/lang/CharSequence;)Lcom/android/phone/EditPhoneNumberPreference;

    .line 195
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    if-nez v1, :cond_e

    .line 196
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->isAllDayChecked()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 195
    if-eqz v1, :cond_e

    .line 196
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/phone/CallForwardEditPreference;->isTimerEnabled:Z

    .line 195
    if-eqz v1, :cond_e

    .line 197
    if-eqz v7, :cond_e

    .line 199
    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setCallForwardingUncondTimerOption,starthour = "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 201
    const-string/jumbo v8, "startminute = "

    .line 199
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 202
    const-string/jumbo v8, "endhour = "

    .line 199
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 203
    const-string/jumbo v8, "endminute = "

    .line 199
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/CallForwardEditPreference;->mQtiImsExtManager:Lorg/codeaurora/ims/QtiImsExtManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    .line 211
    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    .line 212
    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/phone/CallForwardEditPreference;->mServiceClass:I

    .line 214
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/phone/CallForwardEditPreference;->imsInterfaceListener:Lorg/codeaurora/ims/QtiImsExtListenerBaseImpl;

    .line 205
    invoke-virtual/range {v1 .. v11}, Lorg/codeaurora/ims/QtiImsExtManager;->setCallForwardUncondTimer(IIIIIIIILjava/lang/String;Lorg/codeaurora/ims/internal/IQtiImsExtListener;)V
    :try_end_0
    .catch Lorg/codeaurora/ims/QtiImsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :goto_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/CallForwardEditPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    if-eqz v1, :cond_2

    .line 229
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/CallForwardEditPreference;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-interface {v1, v0, v2}, Lcom/android/phone/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    .line 233
    .end local v3    # "editStartHour":I
    .end local v4    # "editStartMinute":I
    .end local v5    # "editEndHour":I
    .end local v6    # "editEndMinute":I
    .end local v7    # "action":I
    .end local v10    # "number":Ljava/lang/String;
    .end local v16    # "time":I
    :cond_2
    return-void

    .line 153
    :cond_3
    const/4 v7, 0x0

    .restart local v7    # "action":I
    goto/16 :goto_0

    .line 154
    :cond_4
    const/16 v16, 0x14

    .restart local v16    # "time":I
    goto/16 :goto_1

    .line 156
    .restart local v10    # "number":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->getStartTimeHour()I

    move-result v3

    .restart local v3    # "editStartHour":I
    goto/16 :goto_2

    .line 157
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->getStartTimeMinute()I

    move-result v4

    .restart local v4    # "editStartMinute":I
    goto/16 :goto_3

    .line 158
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->getEndTimeHour()I

    move-result v5

    .restart local v5    # "editEndHour":I
    goto/16 :goto_4

    .line 159
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/CallForwardEditPreference;->getEndTimeMinute()I

    move-result v6

    .restart local v6    # "editEndMinute":I
    goto/16 :goto_5

    .line 172
    .restart local v19    # "isCFSettingChanged":Z
    :cond_9
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/phone/CallForwardEditPreference;->mStartHour:I

    if-ne v1, v3, :cond_a

    .line 173
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/phone/CallForwardEditPreference;->mStartMinute:I

    if-eq v1, v4, :cond_b

    .line 172
    :cond_a
    const/16 v19, 0x1

    goto/16 :goto_6

    .line 174
    :cond_b
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/phone/CallForwardEditPreference;->mEndHour:I

    if-ne v1, v5, :cond_a

    .line 175
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/phone/CallForwardEditPreference;->mEndMinute:I

    if-eq v1, v6, :cond_c

    const/16 v19, 0x1

    goto/16 :goto_6

    :cond_c
    const/16 v19, 0x0

    goto/16 :goto_6

    .line 179
    :cond_d
    const-string/jumbo v1, "CallForwardEditPreference"

    const-string/jumbo v2, "no change, do nothing"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/16 v19, 0x0

    goto/16 :goto_6

    .line 215
    .end local v19    # "isCFSettingChanged":Z
    :catch_0
    move-exception v18

    .line 216
    .local v18, "e":Lorg/codeaurora/ims/QtiImsException;
    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setCallForwardUncondTimer exception!"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 219
    .end local v18    # "e":Lorg/codeaurora/ims/QtiImsException;
    :cond_e
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/phone/CallForwardEditPreference;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 220
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/phone/CallForwardEditPreference;->reason:I

    .line 222
    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/phone/CallForwardEditPreference;->mServiceClass:I

    .line 224
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/CallForwardEditPreference;->mHandler:Lcom/android/phone/CallForwardEditPreference$MyHandler;

    const/4 v2, 0x1

    .line 226
    const/4 v8, 0x1

    .line 224
    invoke-virtual {v1, v2, v7, v8}, Lcom/android/phone/CallForwardEditPreference$MyHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v17

    move v12, v7

    move-object v14, v10

    .line 219
    invoke-virtual/range {v11 .. v17}, Lcom/android/internal/telephony/Phone;->setCallForwardingOption(IILjava/lang/String;IILandroid/os/Message;)V

    goto/16 :goto_7
.end method
