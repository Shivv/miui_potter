.class Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;
.super Ljava/lang/Object;
.source "CarrierConfigLoaderInjector.java"

# interfaces
.implements Lcom/android/phone/Dual4GManager$CallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/CarrierConfigLoaderInjector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EnableDual4GForDualCTSim"
.end annotation


# instance fields
.field private mMaxRetryTimes:I

.field private mTimesRetried:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "maxRetryTimes"    # I

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput p1, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mMaxRetryTimes:I

    .line 77
    return-void
.end method


# virtual methods
.method public onSetDual4GResult(ZI)V
    .locals 3
    .param p1, "enabled"    # Z
    .param p2, "resultCode"    # I

    .prologue
    .line 90
    const-string/jumbo v0, "CarrierConfigLoaderInjector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSetDual4GResult enabled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", mTimesRetried="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mTimesRetried:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mTimesRetried:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mTimesRetried:I

    .line 92
    if-eqz p2, :cond_1

    iget v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mTimesRetried:I

    iget v1, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mMaxRetryTimes:I

    if-ge v0, v1, :cond_1

    .line 93
    invoke-static {}, Lcom/android/phone/Dual4GManager;->isDual4GEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    invoke-static {}, Lcom/android/phone/Dual4GManager;->getInstance()Lcom/android/phone/Dual4GManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/android/phone/Dual4GManager;->setDual4GMode(ZLcom/android/phone/Dual4GManager$CallBack;)V

    .line 101
    :goto_0
    return-void

    .line 96
    :cond_0
    iget v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mMaxRetryTimes:I

    iput v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mTimesRetried:I

    goto :goto_0

    .line 99
    :cond_1
    iget v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mMaxRetryTimes:I

    iput v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mTimesRetried:I

    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mTimesRetried:I

    .line 81
    invoke-static {}, Lcom/android/phone/Dual4GManager;->getInstance()Lcom/android/phone/Dual4GManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/android/phone/Dual4GManager;->setDual4GMode(ZLcom/android/phone/Dual4GManager$CallBack;)V

    .line 82
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mMaxRetryTimes:I

    iput v0, p0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->mTimesRetried:I

    .line 86
    return-void
.end method
