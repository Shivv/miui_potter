.class Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;
.super Landroid/os/Handler;
.source "PhoneInterfaceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/phone/PhoneInterfaceManager$UnlockSim;


# direct methods
.method constructor <init>(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/phone/PhoneInterfaceManager$UnlockSim;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;->this$1:Lcom/android/phone/PhoneInterfaceManager$UnlockSim;

    .line 1436
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1439
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 1440
    .local v0, "ar":Landroid/os/AsyncResult;
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1461
    :goto_0
    return-void

    .line 1442
    :pswitch_0
    const-string/jumbo v1, "PhoneInterfaceManager"

    const-string/jumbo v2, "SUPPLY_PIN_COMPLETE"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1443
    iget-object v2, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;->this$1:Lcom/android/phone/PhoneInterfaceManager$UnlockSim;

    monitor-enter v2

    .line 1444
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;->this$1:Lcom/android/phone/PhoneInterfaceManager$UnlockSim;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1, v3}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->-set2(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;I)I

    .line 1445
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_1

    .line 1446
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v1, v1, Lcom/android/internal/telephony/CommandException;

    if-eqz v1, :cond_0

    .line 1447
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v1, Lcom/android/internal/telephony/CommandException;

    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v1

    .line 1448
    sget-object v3, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    .line 1447
    if-ne v1, v3, :cond_0

    .line 1449
    iget-object v1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;->this$1:Lcom/android/phone/PhoneInterfaceManager$UnlockSim;

    const/4 v3, 0x1

    invoke-static {v1, v3}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->-set1(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;I)I

    .line 1456
    :goto_1
    iget-object v1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;->this$1:Lcom/android/phone/PhoneInterfaceManager$UnlockSim;

    const/4 v3, 0x1

    invoke-static {v1, v3}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->-set0(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;Z)Z

    .line 1457
    iget-object v1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;->this$1:Lcom/android/phone/PhoneInterfaceManager$UnlockSim;

    invoke-virtual {v1}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    goto :goto_0

    .line 1451
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;->this$1:Lcom/android/phone/PhoneInterfaceManager$UnlockSim;

    const/4 v3, 0x2

    invoke-static {v1, v3}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->-set1(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1443
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 1454
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/android/phone/PhoneInterfaceManager$UnlockSim$1;->this$1:Lcom/android/phone/PhoneInterfaceManager$UnlockSim;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/android/phone/PhoneInterfaceManager$UnlockSim;->-set1(Lcom/android/phone/PhoneInterfaceManager$UnlockSim;I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1440
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
