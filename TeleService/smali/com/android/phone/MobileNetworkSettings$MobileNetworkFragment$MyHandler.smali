.class Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;
.super Landroid/os/Handler;
.source "MobileNetworkSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;


# direct methods
.method private constructor <init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    .line 1239
    iput-object p1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;
    .param p2, "-this1"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;-><init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V

    return-void
.end method

.method private handleSetPreferredNetworkTypeResponse(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1253
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-virtual {v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1254
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1257
    :cond_0
    return-void

    .line 1260
    :cond_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    .line 1261
    .local v1, "ar":Landroid/os/AsyncResult;
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-get2(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v3

    .line 1263
    .local v3, "phoneSubId":I
    iget-object v4, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v4, :cond_4

    .line 1265
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-virtual {v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    .line 1266
    const-string/jumbo v5, "preferred_network_mode_key"

    .line 1265
    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1267
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-get1(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Landroid/preference/ListPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1269
    .local v2, "networkMode":I
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-get2(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 1270
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "preferred_network_mode"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1268
    invoke-static {v4, v5, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1274
    .end local v2    # "networkMode":I
    :cond_2
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-virtual {v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const-string/jumbo v5, "enabled_networks_key"

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1275
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-get0(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Landroid/preference/ListPreference;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1277
    .restart local v2    # "networkMode":I
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-get2(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 1278
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "preferred_network_mode"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1276
    invoke-static {v4, v5, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1287
    .end local v2    # "networkMode":I
    :cond_3
    :goto_0
    return-void

    .line 1283
    :cond_4
    const-string/jumbo v4, "NetworkSettings"

    const-string/jumbo v5, "handleSetPreferredNetworkTypeResponse:exception in setting network mode."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1285
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-wrap7(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1245
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1250
    :goto_0
    return-void

    .line 1247
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->handleSetPreferredNetworkTypeResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 1245
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
