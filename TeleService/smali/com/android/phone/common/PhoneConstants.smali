.class public Lcom/android/phone/common/PhoneConstants;
.super Ljava/lang/Object;
.source "PhoneConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyMiuiCallExtras(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5
    .param p0, "newExtras"    # Landroid/os/Bundle;
    .param p1, "oldExtras"    # Landroid/os/Bundle;
    .param p2, "reservedExtraPrefix"    # Ljava/lang/String;

    .prologue
    .line 178
    if-ne p0, p1, :cond_0

    .line 179
    return-object p0

    .line 182
    :cond_0
    if-eqz p1, :cond_3

    .line 183
    const-string/jumbo v4, "com.miui.phone.CALL_EXTRAS"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    .line 184
    if-eqz p1, :cond_3

    .line 185
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 186
    .end local p1    # "oldExtras":Landroid/os/Bundle;
    .local v3, "oldExtras":Landroid/os/Bundle;
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "key$iterator":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 187
    .local v1, "key":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    .line 188
    invoke-virtual {v3, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0

    .line 192
    .end local v1    # "key":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3}, Landroid/os/Bundle;->size()I

    move-result v4

    if-nez v4, :cond_7

    .line 193
    const/4 p1, 0x0

    .line 198
    .end local v2    # "key$iterator":Ljava/util/Iterator;
    .end local v3    # "oldExtras":Landroid/os/Bundle;
    :cond_3
    :goto_1
    if-eqz p1, :cond_4

    .line 199
    if-nez p0, :cond_5

    .line 200
    new-instance p0, Landroid/os/Bundle;

    .end local p0    # "newExtras":Landroid/os/Bundle;
    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 201
    .restart local p0    # "newExtras":Landroid/os/Bundle;
    const-string/jumbo v4, "com.miui.phone.CALL_EXTRAS"

    invoke-virtual {p0, v4, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 211
    :cond_4
    :goto_2
    return-object p0

    .line 203
    :cond_5
    const-string/jumbo v4, "com.miui.phone.CALL_EXTRAS"

    invoke-virtual {p0, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 204
    .local v0, "e":Landroid/os/Bundle;
    if-nez v0, :cond_6

    .line 205
    const-string/jumbo v4, "com.miui.phone.CALL_EXTRAS"

    invoke-virtual {p0, v4, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    .line 207
    :cond_6
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_2

    .end local v0    # "e":Landroid/os/Bundle;
    .restart local v2    # "key$iterator":Ljava/util/Iterator;
    .restart local v3    # "oldExtras":Landroid/os/Bundle;
    :cond_7
    move-object p1, v3

    .end local v3    # "oldExtras":Landroid/os/Bundle;
    .restart local p1    # "oldExtras":Landroid/os/Bundle;
    goto :goto_1
.end method
