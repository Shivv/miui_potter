.class public Lcom/android/phone/CarrierConfigLoader;
.super Lcom/android/internal/telephony/ICarrierConfigLoader$Stub;
.source "CarrierConfigLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/CarrierConfigLoader$1;,
        Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;,
        Lcom/android/phone/CarrierConfigLoader$ConfigLoaderBroadcastReceiver;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/android/phone/CarrierConfigLoader;


# instance fields
.field private final mBootReceiver:Landroid/content/BroadcastReceiver;

.field private mConfigFromCarrierApp:[Landroid/os/PersistableBundle;

.field private mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHasSentConfigChange:[Z

.field private final mPackageReceiver:Landroid/content/BroadcastReceiver;

.field private final mPlatformCarrierConfigPackage:Ljava/lang/String;

.field private mServiceConnection:[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;


# direct methods
.method static synthetic -get0(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromCarrierApp:[Landroid/os/PersistableBundle;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/CarrierConfigLoader;)Landroid/os/Handler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/phone/CarrierConfigLoader;)[Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mHasSentConfigChange:[Z

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/phone/CarrierConfigLoader;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mPlatformCarrierConfigPackage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/phone/CarrierConfigLoader;)[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mServiceConnection:[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/PersistableBundle;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "iccid"    # Ljava/lang/String;
    .param p3, "operator"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lcom/android/phone/CarrierConfigLoader;->restoreConfigFromXml(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/PersistableBundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/CarrierConfigLoader;I)Landroid/service/carrier/CarrierIdentifier;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "phoneId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->getCarrierIdForPhoneId(I)Landroid/service/carrier/CarrierIdentifier;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap10(Lcom/android/phone/CarrierConfigLoader;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "phoneId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->unbindService(I)V

    return-void
.end method

.method static synthetic -wrap11(Lcom/android/phone/CarrierConfigLoader;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "phoneId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->updateConfigForPhoneId(I)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;IILjava/lang/String;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "phoneId"    # I
    .param p3, "eventId"    # I
    .param p4, "operatorNumeric"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/phone/CarrierConfigLoader;->bindToConfigPackage(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap3(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->clearCachedConfigForPackage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap4(Lcom/android/phone/CarrierConfigLoader;I)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "phoneId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->getCarrierPackageForPhoneId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap5(Lcom/android/phone/CarrierConfigLoader;I)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "phoneId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->getIccIdForPhoneId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap6(Lcom/android/phone/CarrierConfigLoader;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "phoneId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->broadcastConfigChangedIntent(I)V

    return-void
.end method

.method static synthetic -wrap7(Ljava/lang/String;)V
    .locals 0
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/phone/CarrierConfigLoader;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap8(Ljava/lang/String;)V
    .locals 0
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap9(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/PersistableBundle;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/CarrierConfigLoader;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "iccid"    # Ljava/lang/String;
    .param p3, "operator"    # Ljava/lang/String;
    .param p4, "config"    # Landroid/os/PersistableBundle;

    .prologue
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/phone/CarrierConfigLoader;->saveConfigToXml(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/PersistableBundle;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 420
    invoke-direct {p0}, Lcom/android/internal/telephony/ICarrierConfigLoader$Stub;-><init>()V

    .line 100
    new-instance v0, Lcom/android/phone/CarrierConfigLoader$ConfigLoaderBroadcastReceiver;

    invoke-direct {v0, p0, v4}, Lcom/android/phone/CarrierConfigLoader$ConfigLoaderBroadcastReceiver;-><init>(Lcom/android/phone/CarrierConfigLoader;Lcom/android/phone/CarrierConfigLoader$ConfigLoaderBroadcastReceiver;)V

    iput-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mBootReceiver:Landroid/content/BroadcastReceiver;

    .line 102
    new-instance v0, Lcom/android/phone/CarrierConfigLoader$ConfigLoaderBroadcastReceiver;

    invoke-direct {v0, p0, v4}, Lcom/android/phone/CarrierConfigLoader$ConfigLoaderBroadcastReceiver;-><init>(Lcom/android/phone/CarrierConfigLoader;Lcom/android/phone/CarrierConfigLoader$ConfigLoaderBroadcastReceiver;)V

    iput-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    .line 159
    new-instance v0, Lcom/android/phone/CarrierConfigLoader$1;

    invoke-direct {v0, p0}, Lcom/android/phone/CarrierConfigLoader$1;-><init>(Lcom/android/phone/CarrierConfigLoader;)V

    iput-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mHandler:Landroid/os/Handler;

    .line 421
    iput-object p1, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    .line 423
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0273

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 422
    iput-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mPlatformCarrierConfigPackage:Ljava/lang/String;

    .line 425
    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    .line 426
    .local v6, "bootFilter":Landroid/content/IntentFilter;
    const-string/jumbo v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mBootReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 431
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 432
    .local v3, "pkgFilter":Landroid/content/IntentFilter;
    const-string/jumbo v0, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 433
    const-string/jumbo v0, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 434
    const-string/jumbo v0, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 435
    const-string/jumbo v0, "package"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 436
    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v0, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 438
    invoke-static {p1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v7

    .line 439
    .local v7, "numPhones":I
    new-array v0, v7, [Landroid/os/PersistableBundle;

    iput-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

    .line 440
    new-array v0, v7, [Landroid/os/PersistableBundle;

    iput-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromCarrierApp:[Landroid/os/PersistableBundle;

    .line 441
    new-array v0, v7, [Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    iput-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mServiceConnection:[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    .line 442
    new-array v0, v7, [Z

    iput-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mHasSentConfigChange:[Z

    .line 444
    const-string/jumbo v0, "carrier_config"

    invoke-static {v0, p0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 445
    const-string/jumbo v0, "CarrierConfigLoader has started"

    invoke-static {v0}, Lcom/android/phone/CarrierConfigLoader;->log(Ljava/lang/String;)V

    .line 446
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 448
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

    invoke-static {v0}, Lcom/android/phone/MiuiCloudDataManager;->init([Landroid/os/PersistableBundle;)Lcom/android/phone/MiuiCloudDataManager;

    .line 449
    return-void
.end method

.method private bindToConfigPackage(Ljava/lang/String;IILjava/lang/String;)Z
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "phoneId"    # I
    .param p3, "eventId"    # I
    .param p4, "operatorNumeric"    # Ljava/lang/String;

    .prologue
    .line 482
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Binding to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " for phone "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/CarrierConfigLoader;->log(Ljava/lang/String;)V

    .line 483
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.service.carrier.CarrierService"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 484
    .local v0, "carrierService":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 485
    iget-object v2, p0, Lcom/android/phone/CarrierConfigLoader;->mServiceConnection:[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    new-instance v3, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    invoke-direct {v3, p0, p2, p3, p4}, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;-><init>(Lcom/android/phone/CarrierConfigLoader;IILjava/lang/String;)V

    aput-object v3, v2, p2

    .line 487
    :try_start_0
    iget-object v2, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/phone/CarrierConfigLoader;->mServiceConnection:[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    aget-object v3, v3, p2

    .line 488
    const/4 v4, 0x1

    .line 487
    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 489
    :catch_0
    move-exception v1

    .line 490
    .local v1, "ex":Ljava/lang/SecurityException;
    const/4 v2, 0x0

    return v2
.end method

.method private broadcastConfigChangedIntent(I)V
    .locals 3
    .param p1, "phoneId"    # I

    .prologue
    .line 470
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "broadcastConfigChangedIntent phoneId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/CarrierConfigLoader;->log(Ljava/lang/String;)V

    .line 471
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 472
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x5000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 474
    invoke-static {v0, p1}, Landroid/telephony/SubscriptionManager;->putPhoneIdAndSubIdExtra(Landroid/content/Intent;I)V

    .line 475
    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/app/ActivityManager;->broadcastStickyIntent(Landroid/content/Intent;I)V

    .line 476
    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader;->mHasSentConfigChange:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, p1

    .line 477
    return-void
.end method

.method private clearCachedConfigForPackage(Ljava/lang/String;)Z
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 688
    iget-object v4, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 689
    .local v0, "dir":Ljava/io/File;
    new-instance v4, Lcom/android/phone/CarrierConfigLoader$2;

    invoke-direct {v4, p0, p1}, Lcom/android/phone/CarrierConfigLoader$2;-><init>(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 698
    .local v2, "packageFiles":[Ljava/io/File;
    if-eqz v2, :cond_0

    array-length v4, v2

    if-ge v4, v7, :cond_1

    :cond_0
    return v3

    .line 699
    :cond_1
    array-length v4, v2

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v1, v2, v3

    .line 700
    .local v1, "f":Ljava/io/File;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "deleting "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/CarrierConfigLoader;->log(Ljava/lang/String;)V

    .line 701
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 699
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 703
    .end local v1    # "f":Ljava/io/File;
    :cond_2
    return v7
.end method

.method private getCarrierIdForPhoneId(I)Landroid/service/carrier/CarrierIdentifier;
    .locals 10
    .param p1, "phoneId"    # I

    .prologue
    const/4 v9, 0x3

    .line 495
    const-string/jumbo v1, ""

    .line 496
    .local v1, "mcc":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 497
    .local v2, "mnc":Ljava/lang/String;
    const-string/jumbo v4, ""

    .line 498
    .local v4, "imsi":Ljava/lang/String;
    const-string/jumbo v5, ""

    .line 499
    .local v5, "gid1":Ljava/lang/String;
    const-string/jumbo v6, ""

    .line 500
    .local v6, "gid2":Ljava/lang/String;
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->getSimOperatorNameForPhone(I)Ljava/lang/String;

    move-result-object v3

    .line 501
    .local v3, "spn":Ljava/lang/String;
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->getSimOperatorNumericForPhone(I)Ljava/lang/String;

    move-result-object v8

    .line 503
    .local v8, "simOperator":Ljava/lang/String;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v9, :cond_0

    .line 504
    const/4 v0, 0x0

    invoke-virtual {v8, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 505
    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 507
    :cond_0
    invoke-static {p1}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v7

    .line 508
    .local v7, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v7, :cond_1

    .line 509
    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getSubscriberId()Ljava/lang/String;

    move-result-object v4

    .line 510
    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getGroupIdLevel1()Ljava/lang/String;

    move-result-object v5

    .line 511
    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getGroupIdLevel2()Ljava/lang/String;

    move-result-object v6

    .line 514
    :cond_1
    new-instance v0, Landroid/service/carrier/CarrierIdentifier;

    invoke-direct/range {v0 .. v6}, Landroid/service/carrier/CarrierIdentifier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private getCarrierPackageForPhoneId(I)Ljava/lang/String;
    .locals 6
    .param p1, "phoneId"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 519
    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 521
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.service.carrier.CarrierService"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 519
    invoke-virtual {v1, v2, p1}, Landroid/telephony/TelephonyManager;->getCarrierPackageNamesForIntentAndPhone(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 522
    .local v0, "carrierPackageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 523
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    return-object v1

    .line 525
    :cond_0
    return-object v5
.end method

.method private getFilenameForConfig(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "iccid"    # Ljava/lang/String;

    .prologue
    .line 708
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "carrierconfig-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getIccIdForPhoneId(I)Ljava/lang/String;
    .locals 3
    .param p1, "phoneId"    # I

    .prologue
    const/4 v2, 0x0

    .line 530
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 531
    return-object v2

    .line 533
    :cond_0
    invoke-static {p1}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 534
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v0, :cond_1

    .line 535
    return-object v2

    .line 537
    :cond_1
    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getIccSerialNumber()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getPackageVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 714
    :try_start_0
    iget-object v2, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 715
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 716
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 717
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    return-object v2
.end method

.method static init(Landroid/content/Context;)Lcom/android/phone/CarrierConfigLoader;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 458
    const-class v1, Lcom/android/phone/CarrierConfigLoader;

    monitor-enter v1

    .line 459
    :try_start_0
    sget-object v0, Lcom/android/phone/CarrierConfigLoader;->sInstance:Lcom/android/phone/CarrierConfigLoader;

    if-nez v0, :cond_0

    .line 460
    new-instance v0, Lcom/android/phone/CarrierConfigLoader;

    invoke-direct {v0, p0}, Lcom/android/phone/CarrierConfigLoader;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/phone/CarrierConfigLoader;->sInstance:Lcom/android/phone/CarrierConfigLoader;

    .line 464
    :goto_0
    sget-object v0, Lcom/android/phone/CarrierConfigLoader;->sInstance:Lcom/android/phone/CarrierConfigLoader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 462
    :cond_0
    :try_start_1
    const-string/jumbo v0, "CarrierConfigLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "init() called multiple times!  sInstance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/phone/CarrierConfigLoader;->sInstance:Lcom/android/phone/CarrierConfigLoader;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 458
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 928
    const-string/jumbo v0, "CarrierConfigLoader"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 932
    const-string/jumbo v0, "CarrierConfigLoader"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    return-void
.end method

.method private printConfig(Landroid/os/PersistableBundle;Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 6
    .param p1, "configApp"    # Landroid/os/PersistableBundle;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 841
    new-instance v0, Lcom/android/internal/util/IndentingPrintWriter;

    const-string/jumbo v4, "    "

    invoke-direct {v0, p2, v4}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    .line 842
    .local v0, "indentPW":Lcom/android/internal/util/IndentingPrintWriter;
    if-nez p1, :cond_0

    .line 843
    invoke-virtual {v0}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    .line 844
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " : null "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 845
    return-void

    .line 847
    :cond_0
    invoke-virtual {v0}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    .line 848
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 849
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/PersistableBundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 850
    .local v3, "sortedKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 851
    invoke-virtual {v0}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    .line 852
    invoke-virtual {v0}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    .line 853
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "key$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 854
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p1, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, [Ljava/lang/Object;

    if-eqz v4, :cond_1

    .line 855
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 856
    invoke-virtual {p1, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 855
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 857
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, [I

    if-eqz v4, :cond_2

    .line 858
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [I

    invoke-static {v4}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 860
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1, v1}, Landroid/os/PersistableBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 863
    .end local v1    # "key":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method private restoreConfigFromXml(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/PersistableBundle;
    .locals 14
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "iccid"    # Ljava/lang/String;
    .param p3, "operator"    # Ljava/lang/String;

    .prologue
    .line 620
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->getPackageVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 621
    .local v10, "version":Ljava/lang/String;
    if-nez v10, :cond_0

    .line 622
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Failed to get package version for: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    .line 623
    const/4 v11, 0x0

    return-object v11

    .line 625
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    .line 626
    :cond_1
    const-string/jumbo v11, "Cannot restore config with null packageName or iccid."

    invoke-static {v11}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    .line 627
    const/4 v11, 0x0

    return-object v11

    .line 629
    :cond_2
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 630
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 633
    :cond_3
    const/4 v8, 0x0

    .line 634
    .local v8, "restoredBundle":Landroid/os/PersistableBundle;
    const/4 v5, 0x0

    .line 636
    .local v5, "inFile":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    .line 637
    new-instance v11, Ljava/io/File;

    iget-object v12, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v12

    invoke-direct/range {p0 .. p2}, Lcom/android/phone/CarrierConfigLoader;->getFilenameForConfig(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 636
    invoke-direct {v6, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 638
    .local v6, "inFile":Ljava/io/FileInputStream;
    :try_start_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    .end local v5    # "inFile":Ljava/io/FileInputStream;
    move-result-object v11

    invoke-virtual {v11}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v7

    .line 639
    .local v7, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const-string/jumbo v11, "utf-8"

    invoke-interface {v7, v6, v11}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 642
    .end local v8    # "restoredBundle":Landroid/os/PersistableBundle;
    :cond_4
    :goto_0
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    .local v4, "event":I
    const/4 v11, 0x1

    if-eq v4, v11, :cond_5

    .line 644
    const/4 v11, 0x2

    if-ne v4, v11, :cond_6

    const-string/jumbo v11, "package_version"

    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 645
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v9

    .line 646
    .local v9, "savedVersion":Ljava/lang/String;
    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 647
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Saved version mismatch: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " vs "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/phone/CarrierConfigLoader;->log(Ljava/lang/String;)V

    .line 656
    .end local v9    # "savedVersion":Ljava/lang/String;
    :cond_5
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    move-object v5, v6

    .line 675
    .end local v4    # "event":I
    .end local v6    # "inFile":Ljava/io/FileInputStream;
    .end local v7    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :goto_1
    return-object v8

    .line 652
    .restart local v4    # "event":I
    .restart local v6    # "inFile":Ljava/io/FileInputStream;
    .restart local v7    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :cond_6
    const/4 v11, 0x2

    if-ne v4, v11, :cond_4

    const-string/jumbo v11, "bundle_data"

    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 653
    invoke-static {v7}, Landroid/os/PersistableBundle;->restoreFromXml(Lorg/xmlpull/v1/XmlPullParser;)Landroid/os/PersistableBundle;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v8

    .local v8, "restoredBundle":Landroid/os/PersistableBundle;
    goto :goto_0

    .line 671
    .end local v4    # "event":I
    .end local v6    # "inFile":Ljava/io/FileInputStream;
    .end local v7    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v5    # "inFile":Ljava/io/FileInputStream;
    .local v8, "restoredBundle":Landroid/os/PersistableBundle;
    :catch_0
    move-exception v2

    .line 672
    .end local v5    # "inFile":Ljava/io/FileInputStream;
    .end local v8    # "restoredBundle":Landroid/os/PersistableBundle;
    .local v2, "e":Ljava/io/IOException;
    :goto_2
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    goto :goto_1

    .line 668
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v5    # "inFile":Ljava/io/FileInputStream;
    .restart local v8    # "restoredBundle":Landroid/os/PersistableBundle;
    :catch_1
    move-exception v3

    .line 669
    .end local v5    # "inFile":Ljava/io/FileInputStream;
    .end local v8    # "restoredBundle":Landroid/os/PersistableBundle;
    .local v3, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_3
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    goto :goto_1

    .line 658
    .end local v3    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v5    # "inFile":Ljava/io/FileInputStream;
    .restart local v8    # "restoredBundle":Landroid/os/PersistableBundle;
    :catch_2
    move-exception v1

    .line 661
    .end local v5    # "inFile":Ljava/io/FileInputStream;
    .end local v8    # "restoredBundle":Landroid/os/PersistableBundle;
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_4
    sget-boolean v11, Lmiui/telephony/PhoneDebug;->VDBG:Z

    if-eqz v11, :cond_7

    .line 662
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    goto :goto_1

    .line 664
    :cond_7
    const-string/jumbo v11, "java.io.FileNotFoundException"

    invoke-static {v11}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    goto :goto_1

    .line 658
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v6    # "inFile":Ljava/io/FileInputStream;
    :catch_3
    move-exception v1

    .restart local v1    # "e":Ljava/io/FileNotFoundException;
    move-object v5, v6

    .end local v6    # "inFile":Ljava/io/FileInputStream;
    .local v5, "inFile":Ljava/io/FileInputStream;
    goto :goto_4

    .line 668
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v5    # "inFile":Ljava/io/FileInputStream;
    .restart local v6    # "inFile":Ljava/io/FileInputStream;
    :catch_4
    move-exception v3

    .restart local v3    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    move-object v5, v6

    .end local v6    # "inFile":Ljava/io/FileInputStream;
    .restart local v5    # "inFile":Ljava/io/FileInputStream;
    goto :goto_3

    .line 671
    .end local v3    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .end local v5    # "inFile":Ljava/io/FileInputStream;
    .restart local v6    # "inFile":Ljava/io/FileInputStream;
    :catch_5
    move-exception v2

    .restart local v2    # "e":Ljava/io/IOException;
    move-object v5, v6

    .end local v6    # "inFile":Ljava/io/FileInputStream;
    .restart local v5    # "inFile":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method private saveConfigToXml(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/PersistableBundle;)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "iccid"    # Ljava/lang/String;
    .param p3, "operator"    # Ljava/lang/String;
    .param p4, "config"    # Landroid/os/PersistableBundle;

    .prologue
    .line 555
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 556
    :cond_0
    const-string/jumbo v6, "Cannot save config with null packageName or iccid."

    invoke-static {v6}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    .line 557
    return-void

    .line 564
    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Landroid/os/PersistableBundle;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 565
    :cond_2
    return-void

    .line 568
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->getPackageVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 569
    .local v5, "version":Ljava/lang/String;
    if-nez v5, :cond_4

    .line 570
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Failed to get package version for: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    .line 571
    return-void

    .line 573
    :cond_4
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 574
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 577
    :cond_5
    const/4 v3, 0x0

    .line 579
    .local v3, "outFile":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    .line 580
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-direct {p0, p1, p2}, Lcom/android/phone/CarrierConfigLoader;->getFilenameForConfig(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 579
    invoke-direct {v4, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 581
    .local v4, "outFile":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    .end local v3    # "outFile":Ljava/io/FileOutputStream;
    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 582
    .local v2, "out":Lcom/android/internal/util/FastXmlSerializer;
    const-string/jumbo v6, "utf-8"

    invoke-virtual {v2, v4, v6}, Lcom/android/internal/util/FastXmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 583
    const-string/jumbo v6, "utf-8"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/android/internal/util/FastXmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 584
    const-string/jumbo v6, "carrier_config"

    const/4 v7, 0x0

    invoke-virtual {v2, v7, v6}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 585
    const-string/jumbo v6, "package_version"

    const/4 v7, 0x0

    invoke-virtual {v2, v7, v6}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 586
    invoke-virtual {v2, v5}, Lcom/android/internal/util/FastXmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 587
    const-string/jumbo v6, "package_version"

    const/4 v7, 0x0

    invoke-virtual {v2, v7, v6}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 588
    const-string/jumbo v6, "bundle_data"

    const/4 v7, 0x0

    invoke-virtual {v2, v7, v6}, Lcom/android/internal/util/FastXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 589
    invoke-virtual {p4, v2}, Landroid/os/PersistableBundle;->saveToXml(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 590
    const-string/jumbo v6, "bundle_data"

    const/4 v7, 0x0

    invoke-virtual {v2, v7, v6}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 591
    const-string/jumbo v6, "carrier_config"

    const/4 v7, 0x0

    invoke-virtual {v2, v7, v6}, Lcom/android/internal/util/FastXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 592
    invoke-virtual {v2}, Lcom/android/internal/util/FastXmlSerializer;->endDocument()V

    .line 593
    invoke-virtual {v2}, Lcom/android/internal/util/FastXmlSerializer;->flush()V

    .line 594
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v3, v4

    .line 602
    .end local v2    # "out":Lcom/android/internal/util/FastXmlSerializer;
    .end local v4    # "outFile":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 599
    .restart local v3    # "outFile":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 600
    .end local v3    # "outFile":Ljava/io/FileOutputStream;
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_1
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 596
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v3    # "outFile":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    .line 597
    .end local v3    # "outFile":Ljava/io/FileOutputStream;
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/phone/CarrierConfigLoader;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 596
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v4    # "outFile":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v0

    .restart local v0    # "e":Ljava/io/IOException;
    move-object v3, v4

    .end local v4    # "outFile":Ljava/io/FileOutputStream;
    .local v3, "outFile":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 599
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "outFile":Ljava/io/FileOutputStream;
    .restart local v4    # "outFile":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    .restart local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    move-object v3, v4

    .end local v4    # "outFile":Ljava/io/FileOutputStream;
    .restart local v3    # "outFile":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method private unbindService(I)V
    .locals 2
    .param p1, "phoneId"    # I

    .prologue
    .line 939
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mServiceConnection:[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mServiceConnection:[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    aget-object v0, v0, p1

    iget-boolean v0, v0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->bound:Z

    if-eqz v0, :cond_0

    .line 940
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader;->mServiceConnection:[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 941
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mServiceConnection:[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    aget-object v0, v0, p1

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->bound:Z

    .line 943
    :cond_0
    return-void
.end method

.method private updateConfigForPhoneId(I)V
    .locals 4
    .param p1, "phoneId"    # I

    .prologue
    const/4 v1, 0x0

    .line 730
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromCarrierApp:[Landroid/os/PersistableBundle;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 731
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->getCarrierPackageForPhoneId(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromCarrierApp:[Landroid/os/PersistableBundle;

    aput-object v1, v0, p1

    .line 734
    :cond_0
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    const/4 v3, -0x1

    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 735
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 819
    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "android.permission.DUMP"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 821
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Permission Denial: can\'t dump carrierconfig from from pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 822
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    .line 821
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 822
    const-string/jumbo v2, ", uid="

    .line 821
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 822
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 821
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 823
    return-void

    .line 825
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "CarrierConfigLoader: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 826
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 827
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Phone Id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 829
    invoke-static {}, Landroid/telephony/CarrierConfigManager;->getDefaultConfig()Landroid/os/PersistableBundle;

    move-result-object v1

    .line 830
    const-string/jumbo v2, "Default Values from CarrierConfigManager"

    .line 829
    invoke-direct {p0, v1, p2, v2}, Lcom/android/phone/CarrierConfigLoader;->printConfig(Landroid/os/PersistableBundle;Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 831
    const-string/jumbo v1, ""

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 833
    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

    aget-object v1, v1, v0

    const-string/jumbo v2, "mConfigFromDefaultApp"

    invoke-direct {p0, v1, p2, v2}, Lcom/android/phone/CarrierConfigLoader;->printConfig(Landroid/os/PersistableBundle;Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 834
    const-string/jumbo v1, ""

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 836
    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromCarrierApp:[Landroid/os/PersistableBundle;

    aget-object v1, v1, v0

    const-string/jumbo v2, "mConfigFromCarrierApp"

    invoke-direct {p0, v1, p2, v2}, Lcom/android/phone/CarrierConfigLoader;->printConfig(Landroid/os/PersistableBundle;Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 826
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 838
    :cond_1
    return-void
.end method

.method public getConfigForSubId(I)Landroid/os/PersistableBundle;
    .locals 8
    .param p1, "subId"    # I

    .prologue
    const/4 v7, 0x0

    .line 741
    :try_start_0
    iget-object v4, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "android.permission.READ_PRIVILEGED_PHONE_STATE"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    :goto_0
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v2

    .line 747
    .local v2, "phoneId":I
    invoke-static {}, Landroid/telephony/CarrierConfigManager;->getDefaultConfig()Landroid/os/PersistableBundle;

    move-result-object v3

    .line 748
    .local v3, "retConfig":Landroid/os/PersistableBundle;
    invoke-static {v2}, Landroid/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 749
    iget-object v4, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromDefaultApp:[Landroid/os/PersistableBundle;

    aget-object v0, v4, v2

    .line 750
    .local v0, "config":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    .line 751
    invoke-virtual {v3, v0}, Landroid/os/PersistableBundle;->putAll(Landroid/os/PersistableBundle;)V

    .line 752
    :cond_0
    iget-object v4, p0, Lcom/android/phone/CarrierConfigLoader;->mConfigFromCarrierApp:[Landroid/os/PersistableBundle;

    aget-object v0, v4, v2

    .line 753
    if-eqz v0, :cond_1

    .line 754
    invoke-virtual {v3, v0}, Landroid/os/PersistableBundle;->putAll(Landroid/os/PersistableBundle;)V

    .line 756
    .end local v0    # "config":Landroid/os/PersistableBundle;
    :cond_1
    return-object v3

    .line 743
    .end local v2    # "phoneId":I
    .end local v3    # "retConfig":Landroid/os/PersistableBundle;
    :catch_0
    move-exception v1

    .line 744
    .local v1, "e":Ljava/lang/SecurityException;
    iget-object v4, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "android.permission.READ_PHONE_STATE"

    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getDefaultCarrierServicePackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 814
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mPlatformCarrierConfigPackage:Ljava/lang/String;

    return-object v0
.end method

.method public notifyConfigChangedForSubId(I)V
    .locals 6
    .param p1, "subId"    # I

    .prologue
    .line 761
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v1

    .line 762
    .local v1, "phoneId":I
    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 763
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Ignore invalid phoneId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " for subId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/CarrierConfigLoader;->log(Ljava/lang/String;)V

    .line 764
    return-void

    .line 766
    :cond_0
    iget-object v3, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 767
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    .line 766
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 769
    .local v0, "callingPackageName":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/telephony/TelephonyManager;->checkCarrierPrivilegesForPackage(Ljava/lang/String;)I

    move-result v2

    .line 773
    .local v2, "privilegeStatus":I
    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 774
    iget-object v3, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    const-string/jumbo v4, "android.permission.MODIFY_PHONE_STATE"

    .line 775
    const-string/jumbo v5, "Require carrier privileges or MODIFY_PHONE_STATE permission."

    .line 774
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    :cond_1
    invoke-direct {p0, v0}, Lcom/android/phone/CarrierConfigLoader;->clearCachedConfigForPackage(Ljava/lang/String;)Z

    .line 782
    invoke-direct {p0, v1}, Lcom/android/phone/CarrierConfigLoader;->updateConfigForPhoneId(I)V

    .line 783
    return-void
.end method

.method public updateConfigForPhoneId(ILjava/lang/String;)V
    .locals 4
    .param p1, "phoneId"    # I
    .param p2, "simState"    # Ljava/lang/String;

    .prologue
    .line 787
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mContext:Landroid/content/Context;

    .line 788
    const-string/jumbo v1, "android.permission.MODIFY_PHONE_STATE"

    const/4 v2, 0x0

    .line 787
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "update config for phoneId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " simState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/CarrierConfigLoader;->log(Ljava/lang/String;)V

    .line 790
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 791
    return-void

    .line 794
    :cond_0
    const-string/jumbo v0, "ABSENT"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 797
    invoke-static {p1}, Lcom/android/phone/CarrierConfigLoaderInjector;->onIccAbsent(I)V

    .line 801
    :cond_1
    iget-object v0, p0, Lcom/android/phone/CarrierConfigLoader;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/phone/CarrierConfigLoader;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 810
    :cond_2
    :goto_0
    return-void

    .line 794
    :cond_3
    const-string/jumbo v0, "CARD_IO_ERROR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "CARD_RESTRICTED"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "UNKNOWN"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "LOADED"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 805
    invoke-static {p1}, Lcom/android/phone/CarrierConfigLoaderInjector;->onIccLoaded(I)V

    .line 807
    :goto_1
    invoke-direct {p0, p1}, Lcom/android/phone/CarrierConfigLoader;->updateConfigForPhoneId(I)V

    goto :goto_0

    .line 794
    :cond_4
    const-string/jumbo v0, "LOCKED"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1
.end method
