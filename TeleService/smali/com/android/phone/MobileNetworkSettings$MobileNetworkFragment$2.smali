.class Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$2;
.super Ljava/lang/Object;
.source "MobileNetworkSettings.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;


# direct methods
.method constructor <init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$2;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 299
    const-string/jumbo v0, "connection created, binding local service."

    invoke-static {v0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-wrap3(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$2;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    check-cast p2, Lcom/android/phone/NetworkQueryService$LocalBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/android/phone/NetworkQueryService$LocalBinder;->getService()Lcom/android/phone/INetworkQueryService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-set0(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;Lcom/android/phone/INetworkQueryService;)Lcom/android/phone/INetworkQueryService;

    .line 301
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$2;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-wrap4(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V

    .line 302
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 306
    const-string/jumbo v0, "connection disconnected, cleaning local binding."

    invoke-static {v0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-wrap3(Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$2;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-set0(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;Lcom/android/phone/INetworkQueryService;)Lcom/android/phone/INetworkQueryService;

    .line 308
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$2;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-wrap4(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V

    .line 309
    return-void
.end method
