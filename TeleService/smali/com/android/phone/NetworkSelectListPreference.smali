.class public Lcom/android/phone/NetworkSelectListPreference;
.super Landroid/preference/ListPreference;
.source "NetworkSelectListPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/NetworkSelectListPreference$1;,
        Lcom/android/phone/NetworkSelectListPreference$2;,
        Lcom/android/phone/NetworkSelectListPreference$SavedState;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

.field private final mHandler:Landroid/os/Handler;

.field private mNetworkOperators:Lcom/android/phone/NetworkOperators;

.field mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

.field private mOperatorInfo:Lcom/android/internal/telephony/OperatorInfo;

.field private mOperatorInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/OperatorInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPhoneId:I

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSubId:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/NetworkSelectListPreference;)Landroid/os/Handler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/NetworkSelectListPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/NetworkSelectListPreference;)Lcom/android/phone/NetworkOperators;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/NetworkSelectListPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkOperators:Lcom/android/phone/NetworkOperators;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/NetworkSelectListPreference;)Lcom/android/internal/telephony/OperatorInfo;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/NetworkSelectListPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfo:Lcom/android/internal/telephony/OperatorInfo;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/NetworkSelectListPreference;Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/NetworkSelectListPreference;
    .param p1, "ni"    # Lcom/android/internal/telephony/OperatorInfo;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/NetworkSelectListPreference;->getNetworkTitle(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/NetworkSelectListPreference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/NetworkSelectListPreference;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->dismissProgressBar()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/NetworkSelectListPreference;Ljava/lang/String;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/NetworkSelectListPreference;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/NetworkSelectListPreference;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/phone/NetworkSelectListPreference;Ljava/util/List;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/NetworkSelectListPreference;
    .param p1, "result"    # Ljava/util/List;
    .param p2, "status"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/NetworkSelectListPreference;->networksListLoaded(Ljava/util/List;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mPhoneId:I

    .line 86
    new-instance v0, Lcom/android/phone/NetworkSelectListPreference$1;

    invoke-direct {v0, p0}, Lcom/android/phone/NetworkSelectListPreference$1;-><init>(Lcom/android/phone/NetworkSelectListPreference;)V

    iput-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mHandler:Landroid/os/Handler;

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    .line 127
    new-instance v0, Lcom/android/phone/NetworkSelectListPreference$2;

    invoke-direct {v0, p0}, Lcom/android/phone/NetworkSelectListPreference$2;-><init>(Lcom/android/phone/NetworkSelectListPreference;)V

    iput-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mPhoneId:I

    .line 86
    new-instance v0, Lcom/android/phone/NetworkSelectListPreference$1;

    invoke-direct {v0, p0}, Lcom/android/phone/NetworkSelectListPreference$1;-><init>(Lcom/android/phone/NetworkSelectListPreference;)V

    iput-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mHandler:Landroid/os/Handler;

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    .line 127
    new-instance v0, Lcom/android/phone/NetworkSelectListPreference$2;

    invoke-direct {v0, p0}, Lcom/android/phone/NetworkSelectListPreference$2;-><init>(Lcom/android/phone/NetworkSelectListPreference;)V

    iput-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    .line 79
    return-void
.end method

.method private clearList()V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 361
    :cond_0
    return-void
.end method

.method private destroy()V
    .locals 4

    .prologue
    .line 211
    :try_start_0
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->dismissProgressBar()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    if-eqz v2, :cond_0

    .line 219
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    iget-object v3, p0, Lcom/android/phone/NetworkSelectListPreference;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    invoke-interface {v2, v3}, Lcom/android/phone/INetworkQueryService;->unregisterCallback(Lcom/android/phone/INetworkQueryServiceCallback;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 224
    :cond_0
    :goto_1
    return-void

    .line 212
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onDestroy: exception from dismissProgressBar "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/phone/NetworkSelectListPreference;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 221
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 222
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onDestroy: exception from unregisterCallback "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/phone/NetworkSelectListPreference;->loge(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private dismissProgressBar()V
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 367
    :cond_0
    return-void
.end method

.method private displayEmptyNetworkList()V
    .locals 4

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0381

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 229
    .local v1, "status":Ljava/lang/String;
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 230
    .local v0, "app":Lcom/android/phone/PhoneGlobals;
    iget-object v2, v0, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    .line 231
    const/4 v3, 0x2

    .line 230
    invoke-virtual {v2, v3, v1}, Lcom/android/phone/NotificationMgr;->postTransientNotification(ILjava/lang/CharSequence;)V

    .line 232
    return-void
.end method

.method private displayNetworkQueryFailed(I)V
    .locals 5
    .param p1, "error"    # I

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0382

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 242
    .local v2, "status":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->dismissProgressBar()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :goto_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 248
    .local v0, "app":Lcom/android/phone/PhoneGlobals;
    iget-object v3, v0, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    .line 249
    const/4 v4, 0x2

    .line 248
    invoke-virtual {v3, v4, v2}, Lcom/android/phone/NotificationMgr;->postTransientNotification(ILjava/lang/CharSequence;)V

    .line 250
    return-void

    .line 243
    .end local v0    # "app":Lcom/android/phone/PhoneGlobals;
    :catch_0
    move-exception v1

    .local v1, "e1":Ljava/lang/IllegalArgumentException;
    goto :goto_0
.end method

.method private displayNetworkSelectionInProgress()V
    .locals 1

    .prologue
    .line 235
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/android/phone/NetworkSelectListPreference;->showProgressBar(I)V

    .line 236
    return-void
.end method

.method private getNetworkTitle(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;
    .locals 3
    .param p1, "ni"    # Lcom/android/internal/telephony/OperatorInfo;

    .prologue
    .line 347
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 348
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 349
    :cond_0
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaShort()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 350
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorAlphaShort()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 352
    :cond_1
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    .line 353
    .local v0, "bidiFormatter":Landroid/text/BidiFormatter;
    invoke-virtual {p1}, Lcom/android/internal/telephony/OperatorInfo;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v0, v1, v2}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private loadNetworksList()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 253
    const-string/jumbo v1, "load networks list..."

    invoke-direct {p0, v1}, Lcom/android/phone/NetworkSelectListPreference;->logd(Ljava/lang/String;)V

    .line 255
    const/16 v1, 0xc8

    invoke-direct {p0, v1}, Lcom/android/phone/NetworkSelectListPreference;->showProgressBar(I)V

    .line 259
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    iget v3, p0, Lcom/android/phone/NetworkSelectListPreference;->mPhoneId:I

    invoke-interface {v1, v2, v3}, Lcom/android/phone/INetworkQueryService;->startNetworkQuery(Lcom/android/phone/INetworkQueryServiceCallback;I)V

    .line 268
    :goto_0
    return-void

    .line 262
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/phone/NetworkSelectListPreference;->displayNetworkQueryFailed(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "loadNetworksList: exception from startNetworkQuery "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/phone/NetworkSelectListPreference;->loge(Ljava/lang/String;)V

    .line 266
    invoke-direct {p0, v4}, Lcom/android/phone/NetworkSelectListPreference;->displayNetworkQueryFailed(I)V

    goto :goto_0
.end method

.method private logd(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 509
    const-string/jumbo v0, "networkSelect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[NetworksList] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 513
    const-string/jumbo v0, "networkSelect"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[NetworksList] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    return-void
.end method

.method private networksListLoaded(Ljava/util/List;I)V
    .locals 8
    .param p2, "status"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/OperatorInfo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 278
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/OperatorInfo;>;"
    const-string/jumbo v5, "networks list loaded"

    invoke-direct {p0, v5}, Lcom/android/phone/NetworkSelectListPreference;->logd(Ljava/lang/String;)V

    .line 282
    :try_start_0
    iget-object v5, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    if-eqz v5, :cond_0

    .line 283
    iget-object v5, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    iget-object v6, p0, Lcom/android/phone/NetworkSelectListPreference;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    invoke-interface {v5, v6}, Lcom/android/phone/INetworkQueryService;->unregisterCallback(Lcom/android/phone/INetworkQueryServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :cond_0
    :goto_0
    const-string/jumbo v5, "hideProgressPanel"

    invoke-direct {p0, v5}, Lcom/android/phone/NetworkSelectListPreference;->logd(Ljava/lang/String;)V

    .line 295
    :try_start_1
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->dismissProgressBar()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 303
    :goto_1
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/android/phone/NetworkSelectListPreference;->setEnabled(Z)V

    .line 304
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->clearList()V

    .line 306
    if-eqz p2, :cond_1

    .line 307
    const-string/jumbo v5, "error while querying available networks"

    invoke-direct {p0, v5}, Lcom/android/phone/NetworkSelectListPreference;->logd(Ljava/lang/String;)V

    .line 308
    invoke-direct {p0, p2}, Lcom/android/phone/NetworkSelectListPreference;->displayNetworkQueryFailed(I)V

    .line 336
    :goto_2
    return-void

    .line 285
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "networksListLoaded: exception from unregisterCallback "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/phone/NetworkSelectListPreference;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 296
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 300
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Fail to dismiss network load list dialog "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/phone/NetworkSelectListPreference;->loge(Ljava/lang/String;)V

    goto :goto_1

    .line 310
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    if-eqz p1, :cond_4

    .line 314
    iput-object p1, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    .line 315
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    new-array v3, v5, [Ljava/lang/CharSequence;

    .line 316
    .local v3, "networkEntries":[Ljava/lang/CharSequence;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    new-array v4, v5, [Ljava/lang/CharSequence;

    .line 317
    .local v4, "networkEntryValues":[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    iget-object v5, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_3

    .line 318
    iget-object v5, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/internal/telephony/OperatorInfo;

    invoke-virtual {v5}, Lcom/android/internal/telephony/OperatorInfo;->getState()Lcom/android/internal/telephony/OperatorInfo$State;

    move-result-object v5

    sget-object v6, Lcom/android/internal/telephony/OperatorInfo$State;->FORBIDDEN:Lcom/android/internal/telephony/OperatorInfo$State;

    if-ne v5, v6, :cond_2

    .line 319
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/internal/telephony/OperatorInfo;

    invoke-direct {p0, v5}, Lcom/android/phone/NetworkSelectListPreference;->getNetworkTitle(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 320
    const-string/jumbo v6, " "

    .line 319
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 321
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b038d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 319
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    .line 325
    :goto_4
    add-int/lit8 v5, v2, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 317
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 323
    :cond_2
    iget-object v5, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/internal/telephony/OperatorInfo;

    invoke-direct {p0, v5}, Lcom/android/phone/NetworkSelectListPreference;->getNetworkTitle(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    goto :goto_4

    .line 328
    :cond_3
    invoke-virtual {p0, v3}, Lcom/android/phone/NetworkSelectListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 329
    invoke-virtual {p0, v4}, Lcom/android/phone/NetworkSelectListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 331
    invoke-super {p0}, Landroid/preference/ListPreference;->onClick()V

    goto/16 :goto_2

    .line 333
    .end local v2    # "i":I
    .end local v3    # "networkEntries":[Ljava/lang/CharSequence;
    .end local v4    # "networkEntryValues":[Ljava/lang/CharSequence;
    :cond_4
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->displayEmptyNetworkList()V

    goto/16 :goto_2
.end method

.method private showProgressBar(I)V
    .locals 6
    .param p1, "id"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 370
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    if-nez v1, :cond_2

    .line 371
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 377
    :goto_0
    const/16 v1, 0x64

    if-eq p1, v1, :cond_0

    const/16 v1, 0xc8

    if-ne p1, v1, :cond_1

    .line 378
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 398
    :goto_1
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 400
    :cond_1
    return-void

    .line 374
    :cond_2
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->dismissProgressBar()V

    goto :goto_0

    .line 380
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    .line 382
    iget-object v3, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfo:Lcom/android/internal/telephony/OperatorInfo;

    invoke-direct {p0, v3}, Lcom/android/phone/NetworkSelectListPreference;->getNetworkTitle(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 381
    const v3, 0x7f0b0383

    .line 380
    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 383
    .local v0, "networkSelectMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 384
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 385
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 386
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    goto :goto_1

    .line 389
    .end local v0    # "networkSelectMsg":Ljava/lang/String;
    :sswitch_1
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 390
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0380

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 389
    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 391
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 392
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 393
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 394
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, p0}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_1

    .line 378
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method protected initialize(ILcom/android/phone/INetworkQueryService;Lcom/android/phone/NetworkOperators;Landroid/app/ProgressDialog;)V
    .locals 3
    .param p1, "subId"    # I
    .param p2, "queryService"    # Lcom/android/phone/INetworkQueryService;
    .param p3, "networkOperators"    # Lcom/android/phone/NetworkOperators;
    .param p4, "progressDialog"    # Landroid/app/ProgressDialog;

    .prologue
    .line 185
    iput p1, p0, Lcom/android/phone/NetworkSelectListPreference;->mSubId:I

    .line 186
    iput-object p2, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    .line 187
    iput-object p3, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkOperators:Lcom/android/phone/NetworkOperators;

    .line 189
    iput-object p4, p0, Lcom/android/phone/NetworkSelectListPreference;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 191
    iget v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mSubId:I

    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    iget v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mSubId:I

    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v1

    iput v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mPhoneId:I

    .line 196
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 195
    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 198
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/phone/NetworkSelectListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 200
    invoke-virtual {p0, p0}, Lcom/android/phone/NetworkSelectListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 201
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference;->mCallback:Lcom/android/phone/INetworkQueryServiceCallback;

    invoke-interface {v1, v2}, Lcom/android/phone/INetworkQueryService;->stopNetworkQuery(Lcom/android/phone/INetworkQueryServiceCallback;)V

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkOperators:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v1}, Lcom/android/phone/NetworkOperators;->getNetworkSelectionMode()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onCancel: exception from stopNetworkQuery "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/phone/NetworkSelectListPreference;->loge(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onClick()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->loadNetworksList()V

    .line 84
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 1
    .param p1, "positiveResult"    # Z

    .prologue
    .line 155
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onDialogClosed(Z)V

    .line 158
    if-nez p1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkOperators:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v0}, Lcom/android/phone/NetworkOperators;->getNetworkSelectionMode()V

    .line 161
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    .line 410
    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p0, p2}, Lcom/android/phone/NetworkSelectListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 411
    .local v1, "operatorIndex":I
    iget-object v3, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/telephony/OperatorInfo;

    iput-object v3, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfo:Lcom/android/internal/telephony/OperatorInfo;

    .line 413
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "selected network: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfo:Lcom/android/internal/telephony/OperatorInfo;

    invoke-direct {p0, v4}, Lcom/android/phone/NetworkSelectListPreference;->getNetworkTitle(Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/phone/NetworkSelectListPreference;->logd(Ljava/lang/String;)V

    .line 415
    iget-object v3, p0, Lcom/android/phone/NetworkSelectListPreference;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 416
    .local v0, "msg":Landroid/os/Message;
    iget v3, p0, Lcom/android/phone/NetworkSelectListPreference;->mPhoneId:I

    invoke-static {v3}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 417
    .local v2, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v2, :cond_0

    .line 418
    iget-object v3, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfo:Lcom/android/internal/telephony/OperatorInfo;

    invoke-virtual {v2, v3, v5, v0}, Lcom/android/internal/telephony/Phone;->selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;ZLandroid/os/Message;)V

    .line 419
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->displayNetworkSelectionInProgress()V

    .line 424
    :goto_0
    return v5

    .line 421
    :cond_0
    const-string/jumbo v3, "Error selecting network. phone is null."

    invoke-direct {p0, v3}, Lcom/android/phone/NetworkSelectListPreference;->loge(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPrepareForRemoval()V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/android/phone/NetworkSelectListPreference;->destroy()V

    .line 206
    invoke-super {p0}, Landroid/preference/ListPreference;->onPrepareForRemoval()V

    .line 207
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 444
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/android/phone/NetworkSelectListPreference$SavedState;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 446
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 447
    return-void

    :cond_1
    move-object v0, p1

    .line 450
    check-cast v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;

    .line 452
    .local v0, "myState":Lcom/android/phone/NetworkSelectListPreference$SavedState;
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntries:[Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    .line 453
    iget-object v1, v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntries:[Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/android/phone/NetworkSelectListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 455
    :cond_2
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntryValues:[Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    .line 456
    iget-object v1, v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntryValues:[Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/android/phone/NetworkSelectListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 458
    :cond_3
    iget-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mOperatorInfoList:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 459
    iget-object v1, v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mOperatorInfoList:Ljava/util/List;

    iput-object v1, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    .line 462
    :cond_4
    invoke-virtual {v0}, Lcom/android/phone/NetworkSelectListPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/preference/ListPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 463
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 429
    invoke-super {p0}, Landroid/preference/ListPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 430
    .local v1, "superState":Landroid/os/Parcelable;
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->isPersistent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 432
    return-object v1

    .line 435
    :cond_0
    new-instance v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;

    invoke-direct {v0, v1}, Lcom/android/phone/NetworkSelectListPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 436
    .local v0, "myState":Lcom/android/phone/NetworkSelectListPreference$SavedState;
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntries:[Ljava/lang/CharSequence;

    .line 437
    invoke-virtual {p0}, Lcom/android/phone/NetworkSelectListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mDialogListEntryValues:[Ljava/lang/CharSequence;

    .line 438
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference;->mOperatorInfoList:Ljava/util/List;

    iput-object v2, v0, Lcom/android/phone/NetworkSelectListPreference$SavedState;->mOperatorInfoList:Ljava/util/List;

    .line 439
    return-object v0
.end method

.method public setNetworkQueryService(Lcom/android/phone/INetworkQueryService;)V
    .locals 0
    .param p1, "queryService"    # Lcom/android/phone/INetworkQueryService;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/android/phone/NetworkSelectListPreference;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    .line 180
    return-void
.end method
