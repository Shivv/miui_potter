.class public Lcom/android/phone/OutgoingCallBroadcaster$OutgoingCallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "OutgoingCallBroadcaster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/OutgoingCallBroadcaster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OutgoingCallReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/OutgoingCallBroadcaster;


# direct methods
.method public constructor <init>(Lcom/android/phone/OutgoingCallBroadcaster;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/OutgoingCallBroadcaster;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/android/phone/OutgoingCallBroadcaster$OutgoingCallReceiver;->this$0:Lcom/android/phone/OutgoingCallBroadcaster;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public doReceive(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x0

    .line 170
    invoke-static {}, Lcom/android/phone/OutgoingCallBroadcaster;->-get0()Z

    move-result v6

    if-eqz v6, :cond_0

    const-string/jumbo v6, "OutgoingCallReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "doReceive: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_0
    const-string/jumbo v6, "android.phone.extra.ALREADY_CALLED"

    .line 176
    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 178
    .local v0, "alreadyCalled":Z
    if-eqz v0, :cond_2

    .line 179
    invoke-static {}, Lcom/android/phone/OutgoingCallBroadcaster;->-get0()Z

    move-result v6

    if-eqz v6, :cond_1

    const-string/jumbo v6, "OutgoingCallReceiver"

    const-string/jumbo v7, "CALL already placed -- returning."

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :cond_1
    return v9

    .line 187
    :cond_2
    invoke-virtual {p0}, Lcom/android/phone/OutgoingCallBroadcaster$OutgoingCallReceiver;->getResultData()Ljava/lang/String;

    move-result-object v2

    .line 190
    .local v2, "number":Ljava/lang/String;
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    .line 191
    .local v1, "app":Lcom/android/phone/PhoneGlobals;
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    .line 193
    .local v4, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v2, :cond_4

    .line 194
    invoke-static {}, Lcom/android/phone/OutgoingCallBroadcaster;->-get0()Z

    move-result v6

    if-eqz v6, :cond_3

    const-string/jumbo v6, "OutgoingCallReceiver"

    const-string/jumbo v7, "CALL cancelled (null number), returning..."

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_3
    return v9

    .line 196
    :cond_4
    invoke-static {v4}, Lcom/android/internal/telephony/TelephonyCapabilities;->supportsOtasp(Lcom/android/internal/telephony/Phone;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 197
    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v6

    sget-object v7, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    if-eq v6, v7, :cond_6

    .line 198
    invoke-virtual {v4, v2}, Lcom/android/internal/telephony/Phone;->isOtaSpNumber(Ljava/lang/String;)Z

    move-result v6

    .line 196
    if-eqz v6, :cond_6

    .line 199
    invoke-static {}, Lcom/android/phone/OutgoingCallBroadcaster;->-get0()Z

    move-result v6

    if-eqz v6, :cond_5

    const-string/jumbo v6, "OutgoingCallReceiver"

    const-string/jumbo v7, "Call is active, a 2nd OTA call cancelled -- returning."

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_5
    return v9

    .line 201
    :cond_6
    invoke-static {v2}, Lcom/android/phone/PhoneUtils;->isPotentialLocalEmergencyNumber(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 206
    const-string/jumbo v6, "OutgoingCallReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Cannot modify outgoing call to emergency number "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    return v9

    .line 211
    :cond_7
    const-string/jumbo v6, "android.phone.extra.ORIGINAL_URI"

    .line 210
    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 212
    .local v3, "originalUri":Ljava/lang/String;
    if-nez v3, :cond_8

    .line 213
    const-string/jumbo v6, "OutgoingCallReceiver"

    const-string/jumbo v7, "Intent is missing EXTRA_ORIGINAL_URI -- returning."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    return v9

    .line 217
    :cond_8
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 224
    .local v5, "uri":Landroid/net/Uri;
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->convertKeypadLettersToDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 225
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 227
    invoke-static {}, Lcom/android/phone/OutgoingCallBroadcaster;->-get0()Z

    move-result v6

    if-eqz v6, :cond_9

    const-string/jumbo v6, "OutgoingCallReceiver"

    const-string/jumbo v7, "doReceive: proceeding with call..."

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_9
    iget-object v6, p0, Lcom/android/phone/OutgoingCallBroadcaster$OutgoingCallReceiver;->this$0:Lcom/android/phone/OutgoingCallBroadcaster;

    invoke-static {v6, p1, p2, v5, v2}, Lcom/android/phone/OutgoingCallBroadcaster;->-wrap1(Lcom/android/phone/OutgoingCallBroadcaster;Landroid/content/Context;Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;)V

    .line 233
    const/4 v6, 0x1

    return v6
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 146
    iget-object v1, p0, Lcom/android/phone/OutgoingCallBroadcaster$OutgoingCallReceiver;->this$0:Lcom/android/phone/OutgoingCallBroadcaster;

    invoke-static {v1}, Lcom/android/phone/OutgoingCallBroadcaster;->-get1(Lcom/android/phone/OutgoingCallBroadcaster;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 147
    invoke-virtual {p0, p1, p2}, Lcom/android/phone/OutgoingCallBroadcaster$OutgoingCallReceiver;->doReceive(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    .line 148
    .local v0, "isAttemptingCall":Z
    invoke-static {}, Lcom/android/phone/OutgoingCallBroadcaster;->-get0()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "OutgoingCallReceiver"

    const-string/jumbo v2, "OutgoingCallReceiver is going to finish the Activity itself."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    if-eqz v0, :cond_1

    .line 157
    iget-object v1, p0, Lcom/android/phone/OutgoingCallBroadcaster$OutgoingCallReceiver;->this$0:Lcom/android/phone/OutgoingCallBroadcaster;

    invoke-static {v1}, Lcom/android/phone/OutgoingCallBroadcaster;->-wrap0(Lcom/android/phone/OutgoingCallBroadcaster;)V

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/android/phone/OutgoingCallBroadcaster$OutgoingCallReceiver;->this$0:Lcom/android/phone/OutgoingCallBroadcaster;

    invoke-virtual {v1}, Lcom/android/phone/OutgoingCallBroadcaster;->finish()V

    goto :goto_0
.end method
