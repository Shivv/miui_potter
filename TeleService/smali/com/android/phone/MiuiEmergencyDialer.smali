.class public Lcom/android/phone/MiuiEmergencyDialer;
.super Landroid/app/Activity;
.source "MiuiEmergencyDialer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/MiuiEmergencyDialer$1;
    }
.end annotation


# static fields
.field private static final DIALER_KEYS:[I


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mDTMFToneEnabled:Z

.field private mDelete:Landroid/view/View;

.field private mDialButton:Landroid/view/View;

.field mDigits:Landroid/widget/EditText;

.field private mDigitsNumber:Ljava/lang/String;

.field private mLastNumber:Ljava/lang/String;

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mToneGenerator:Landroid/media/ToneGenerator;

.field private mToneGeneratorLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/phone/MiuiEmergencyDialer;->DIALER_KEYS:[I

    .line 65
    return-void

    .line 74
    :array_0
    .array-data 4
        0x7f0d0090
        0x7f0d0082
        0x7f0d0083
        0x7f0d0084
        0x7f0d0085
        0x7f0d0086
        0x7f0d0087
        0x7f0d0088
        0x7f0d0089
        0x7f0d0093
        0x7f0d0094
        0x7f0d0092
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 101
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    .line 106
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGeneratorLock:Ljava/lang/Object;

    .line 112
    new-instance v0, Lcom/android/phone/MiuiEmergencyDialer$1;

    invoke-direct {v0, p0}, Lcom/android/phone/MiuiEmergencyDialer$1;-><init>(Lcom/android/phone/MiuiEmergencyDialer;)V

    iput-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 65
    return-void
.end method

.method private createErrorMessage(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 613
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 614
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f0b04ae

    invoke-virtual {p0, v1, v0}, Lcom/android/phone/MiuiEmergencyDialer;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 616
    :cond_0
    const v0, 0x7f0b04af

    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private keyPressed(I)V
    .locals 4
    .param p1, "keyCode"    # I

    .prologue
    const/4 v3, 0x0

    .line 326
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->performHapticFeedback(I)Z

    .line 327
    new-instance v0, Landroid/view/KeyEvent;

    invoke-direct {v0, v3, p1}, Landroid/view/KeyEvent;-><init>(II)V

    .line 328
    .local v0, "event":Landroid/view/KeyEvent;
    const/16 v1, 0x43

    if-ne p1, v1, :cond_1

    .line 329
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    .line 335
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 337
    return-void

    .line 333
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/KeyEvent;->getNumber()C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    goto :goto_0
.end method

.method private placeCall()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 548
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    .line 549
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    invoke-static {p0, v1}, Lmiui/telephony/TelephonyManagerEx;->isLocalEmergencyNumber(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 553
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 555
    :cond_0
    const/16 v1, 0x1a

    invoke-virtual {p0, v1}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 556
    return-void

    .line 558
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.CALL_EMERGENCY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 559
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "tel"

    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    invoke-static {v1, v2, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 560
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 561
    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->startActivity(Landroid/content/Intent;)V

    .line 562
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->finish()V

    .line 563
    invoke-virtual {p0, v3, v3}, Lcom/android/phone/MiuiEmergencyDialer;->overridePendingTransition(II)V

    .line 572
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 568
    :cond_2
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v1, v3, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 569
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    .line 570
    invoke-virtual {p0, v3}, Lcom/android/phone/MiuiEmergencyDialer;->showDialog(I)V

    goto :goto_0
.end method

.method private setupKeypad()V
    .locals 6

    .prologue
    .line 288
    sget-object v4, Lcom/android/phone/MiuiEmergencyDialer;->DIALER_KEYS:[I

    const/4 v3, 0x0

    array-length v5, v4

    :goto_0
    if-ge v3, v5, :cond_0

    aget v0, v4, v3

    .line 292
    .local v0, "id":I
    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 293
    .local v1, "key":Landroid/view/View;
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 288
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 298
    .end local v0    # "id":I
    .end local v1    # "key":Landroid/view/View;
    :cond_0
    const v3, 0x7f0d0094

    invoke-virtual {p0, v3}, Lcom/android/phone/MiuiEmergencyDialer;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 299
    .local v2, "view":Landroid/view/View;
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 300
    return-void
.end method

.method private updateDialAndDeleteButtonStateEnabledAttr()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 651
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 652
    .local v0, "notEmpty":Z
    :goto_0
    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    .line 653
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0900c5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    :goto_1
    int-to-float v1, v1

    .line 652
    invoke-virtual {v2, v4, v1}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 656
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDialButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 657
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDelete:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 658
    return-void

    .line 651
    .end local v0    # "notEmpty":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "notEmpty":Z
    goto :goto_0

    .line 654
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0900c4

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1, "input"    # Landroid/text/Editable;

    .prologue
    .line 144
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p0}, Lcom/android/phone/SpecialCharSequenceMgr;->handleCharsForLockedDevice(Landroid/content/Context;Ljava/lang/String;Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 147
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    .line 150
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/MiuiEmergencyDialer;->updateDialAndDeleteButtonStateEnabledAttr()V

    .line 151
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 126
    return-void
.end method

.method protected maybeAddNumberFormatting()V
    .locals 0

    .prologue
    .line 273
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 441
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->finish()V

    .line 442
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/android/phone/MiuiEmergencyDialer;->overridePendingTransition(II)V

    .line 443
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v5, 0x9

    const/16 v4, 0x8

    const/4 v3, 0x7

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 357
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 437
    :goto_0
    :pswitch_0
    return-void

    .line 359
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 360
    invoke-direct {p0, v4}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 361
    return-void

    .line 364
    :pswitch_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 365
    invoke-direct {p0, v5}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 366
    return-void

    .line 369
    :pswitch_3
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 370
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 371
    return-void

    .line 374
    :pswitch_4
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 375
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 376
    return-void

    .line 379
    :pswitch_5
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 380
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 381
    return-void

    .line 384
    :pswitch_6
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 385
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 386
    return-void

    .line 389
    :pswitch_7
    invoke-virtual {p0, v3}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 390
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 391
    return-void

    .line 394
    :pswitch_8
    invoke-virtual {p0, v4}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 395
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 396
    return-void

    .line 399
    :pswitch_9
    invoke-virtual {p0, v5}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 400
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 401
    return-void

    .line 404
    :pswitch_a
    invoke-virtual {p0, v2}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 405
    invoke-direct {p0, v3}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 406
    return-void

    .line 409
    :pswitch_b
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 410
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 411
    return-void

    .line 414
    :pswitch_c
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->playTone(I)V

    .line 415
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 416
    return-void

    .line 419
    :pswitch_d
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->performHapticFeedback(I)Z

    .line 420
    invoke-direct {p0}, Lcom/android/phone/MiuiEmergencyDialer;->placeCall()V

    .line 421
    return-void

    .line 424
    :pswitch_e
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->onBackPressed()V

    goto :goto_0

    .line 427
    :pswitch_f
    const/16 v0, 0x43

    invoke-direct {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 428
    return-void

    .line 431
    :pswitch_10
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 434
    :cond_0
    return-void

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0082
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_10
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x8

    .line 155
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 157
    const-string/jumbo v8, "statusbar"

    invoke-virtual {p0, v8}, Lcom/android/phone/MiuiEmergencyDialer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/StatusBarManager;

    iput-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mStatusBarManager:Landroid/app/StatusBarManager;

    .line 158
    const-string/jumbo v8, "accessibility"

    invoke-virtual {p0, v8}, Lcom/android/phone/MiuiEmergencyDialer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/accessibility/AccessibilityManager;

    iput-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 161
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 162
    .local v4, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v8, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v9, 0x4080000

    or-int/2addr v8, v9

    iput v8, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 167
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 169
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    .line 170
    const/16 v9, 0x300

    .line 169
    invoke-virtual {v8, v9}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 173
    const v8, 0x7f04003a

    invoke-virtual {p0, v8}, Lcom/android/phone/MiuiEmergencyDialer;->setContentView(I)V

    .line 175
    const v8, 0x7f0d009d

    invoke-virtual {p0, v8}, Lcom/android/phone/MiuiEmergencyDialer;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    iput-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    .line 176
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 177
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v8, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v8, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 179
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setLongClickable(Z)V

    .line 180
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v8}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 182
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setSelected(Z)V

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->maybeAddNumberFormatting()V

    .line 187
    const v8, 0x7f0d0090

    invoke-virtual {p0, v8}, Lcom/android/phone/MiuiEmergencyDialer;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 188
    .local v7, "view":Landroid/view/View;
    if-eqz v7, :cond_1

    .line 189
    invoke-direct {p0}, Lcom/android/phone/MiuiEmergencyDialer;->setupKeypad()V

    .line 192
    :cond_1
    const v8, 0x7f0d008d

    invoke-virtual {p0, v8}, Lcom/android/phone/MiuiEmergencyDialer;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDelete:Landroid/view/View;

    .line 193
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDelete:Landroid/view/View;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDelete:Landroid/view/View;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 195
    const v8, 0x7f0d008c

    invoke-virtual {p0, v8}, Lcom/android/phone/MiuiEmergencyDialer;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    const v8, 0x7f0d008b

    invoke-virtual {p0, v8}, Lcom/android/phone/MiuiEmergencyDialer;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDialButton:Landroid/view/View;

    .line 200
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 201
    .local v6, "res":Landroid/content/res/Resources;
    const v8, 0x7f0e000d

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 202
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDialButton:Landroid/view/View;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    :goto_0
    if-eqz p1, :cond_2

    .line 208
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 211
    :cond_2
    invoke-static {p0}, Lcom/android/phone/ImageUtils;->getEmergencyBackground(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 212
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_3

    .line 213
    const v8, 0x7f0d002c

    invoke-virtual {p0, v8}, Lcom/android/phone/MiuiEmergencyDialer;->findViewById(I)Landroid/view/View;

    move-result-object v8

    new-instance v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v9, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 217
    :cond_3
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 218
    .local v1, "data":Landroid/net/Uri;
    if-eqz v1, :cond_4

    const-string/jumbo v8, "tel"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 219
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-static {v8, p0}, Lmiui/telephony/PhoneNumberUtils;->getNumberFromIntent(Landroid/content/Intent;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 220
    .local v5, "number":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 221
    iput-object v5, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    .line 222
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-static {v5}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 228
    .end local v5    # "number":Ljava/lang/String;
    :cond_4
    iget-object v9, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGeneratorLock:Ljava/lang/Object;

    monitor-enter v9

    .line 229
    :try_start_0
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v8, :cond_5

    .line 231
    :try_start_1
    new-instance v8, Landroid/media/ToneGenerator;

    const/16 v10, 0x8

    const/16 v11, 0x50

    invoke-direct {v8, v10, v11}, Landroid/media/ToneGenerator;-><init>(II)V

    iput-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    :goto_1
    monitor-exit v9

    .line 239
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 240
    .local v3, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v8, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 241
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v3}, Lcom/android/phone/MiuiEmergencyDialer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 242
    return-void

    .line 204
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "data":Landroid/net/Uri;
    .end local v3    # "intentFilter":Landroid/content/IntentFilter;
    :cond_6
    iget-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDialButton:Landroid/view/View;

    invoke-virtual {v8, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 232
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "data":Landroid/net/Uri;
    :catch_0
    move-exception v2

    .line 233
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_2
    const-string/jumbo v8, "EmergencyDialer"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Exception caught while creating local tone generator: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 228
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    monitor-exit v9

    throw v8
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 622
    const/4 v0, 0x0

    .line 623
    .local v0, "dialog":Landroid/app/AlertDialog;
    if-nez p1, :cond_0

    .line 625
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 626
    const v2, 0x7f0b04aa

    invoke-virtual {p0, v2}, Lcom/android/phone/MiuiEmergencyDialer;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 625
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 627
    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/phone/MiuiEmergencyDialer;->createErrorMessage(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 625
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 628
    const v2, 0x7f0b02f6

    const/4 v3, 0x0

    .line 625
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 629
    const/4 v2, 0x1

    .line 625
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 632
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 633
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 635
    .end local v0    # "dialog":Landroid/app/AlertDialog;
    :cond_0
    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 246
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 247
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGeneratorLock:Ljava/lang/Object;

    monitor-enter v1

    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;

    invoke-virtual {v0}, Landroid/media/ToneGenerator;->release()V

    .line 250
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    .line 253
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/MiuiEmergencyDialer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 254
    return-void

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    .line 449
    iget-object v6, p0, Lcom/android/phone/MiuiEmergencyDialer;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 450
    iget-object v6, p0, Lcom/android/phone/MiuiEmergencyDialer;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v6

    .line 449
    if-eqz v6, :cond_0

    .line 452
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 472
    :cond_0
    :goto_0
    return v8

    .line 455
    :pswitch_0
    invoke-virtual {p1, v8}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 458
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 459
    .local v1, "left":I
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v7

    sub-int v2, v6, v7

    .line 460
    .local v2, "right":I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    .line 461
    .local v3, "top":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v7

    sub-int v0, v6, v7

    .line 462
    .local v0, "bottom":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v4, v6

    .line 463
    .local v4, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v5, v6

    .line 464
    .local v5, "y":I
    if-le v4, v1, :cond_1

    if-ge v4, v2, :cond_1

    if-le v5, v3, :cond_1

    if-ge v5, v0, :cond_1

    .line 465
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 467
    :cond_1
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 452
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 341
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 352
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 345
    :pswitch_0
    const/16 v0, 0x42

    if-ne p2, v0, :cond_0

    .line 346
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 347
    invoke-direct {p0}, Lcom/android/phone/MiuiEmergencyDialer;->placeCall()V

    .line 348
    return v1

    .line 341
    :pswitch_data_0
    .packed-switch 0x7f0d009d
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 307
    packed-switch p1, :pswitch_data_0

    .line 322
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 310
    :pswitch_0
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->finish()V

    .line 319
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 317
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/MiuiEmergencyDialer;->placeCall()V

    goto :goto_0

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 479
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 480
    .local v0, "id":I
    sparse-switch v0, :sswitch_data_0

    .line 494
    const/4 v1, 0x0

    return v1

    .line 482
    :sswitch_0
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->clear()V

    .line 483
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigitsNumber:Ljava/lang/String;

    .line 487
    return v2

    .line 490
    :sswitch_1
    const/16 v1, 0x51

    invoke-direct {p0, v1}, Lcom/android/phone/MiuiEmergencyDialer;->keyPressed(I)V

    .line 491
    return v2

    .line 480
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d008d -> :sswitch_0
        0x7f0d0094 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 532
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mStatusBarManager:Landroid/app/StatusBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 534
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 536
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGeneratorLock:Ljava/lang/Object;

    monitor-enter v1

    .line 537
    :try_start_0
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;

    invoke-virtual {v0}, Landroid/media/ToneGenerator;->release()V

    .line 539
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    .line 542
    return-void

    .line 536
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 277
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 283
    iget-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDigits:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 284
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 640
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 641
    if-nez p1, :cond_0

    move-object v0, p2

    .line 642
    check-cast v0, Landroid/app/AlertDialog;

    .line 643
    .local v0, "alert":Landroid/app/AlertDialog;
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/phone/MiuiEmergencyDialer;->createErrorMessage(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 645
    .end local v0    # "alert":Landroid/app/AlertDialog;
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 258
    const-string/jumbo v0, "lastNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    .line 259
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 499
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 502
    invoke-virtual {p0}, Lcom/android/phone/MiuiEmergencyDialer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 503
    const-string/jumbo v3, "dtmf_tone"

    .line 502
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_1

    :goto_0
    iput-boolean v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDTMFToneEnabled:Z

    .line 507
    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGeneratorLock:Ljava/lang/Object;

    monitor-enter v2

    .line 508
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 510
    :try_start_1
    new-instance v1, Landroid/media/ToneGenerator;

    const/16 v3, 0x8

    .line 511
    const/16 v4, 0x50

    .line 510
    invoke-direct {v1, v3, v4}, Landroid/media/ToneGenerator;-><init>(II)V

    iput-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_1
    monitor-exit v2

    .line 522
    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mStatusBarManager:Landroid/app/StatusBarManager;

    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/app/StatusBarManager;->disable(I)V

    .line 524
    invoke-direct {p0}, Lcom/android/phone/MiuiEmergencyDialer;->updateDialAndDeleteButtonStateEnabledAttr()V

    .line 525
    return-void

    .line 502
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 512
    :catch_0
    move-exception v0

    .line 513
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    const-string/jumbo v1, "EmergencyDialer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception caught while creating local tone generator: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 507
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 263
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 264
    const-string/jumbo v0, "lastNumber"

    iget-object v1, p0, Lcom/android/phone/MiuiEmergencyDialer;->mLastNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "input"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "changeCount"    # I

    .prologue
    .line 131
    return-void
.end method

.method playTone(I)V
    .locals 6
    .param p1, "tone"    # I

    .prologue
    .line 585
    iget-boolean v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mDTMFToneEnabled:Z

    if-nez v2, :cond_0

    .line 586
    return-void

    .line 594
    :cond_0
    const-string/jumbo v2, "audio"

    invoke-virtual {p0, v2}, Lcom/android/phone/MiuiEmergencyDialer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 595
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    .line 596
    .local v1, "ringerMode":I
    if-eqz v1, :cond_1

    .line 597
    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 598
    :cond_1
    return-void

    .line 601
    :cond_2
    iget-object v3, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGeneratorLock:Ljava/lang/Object;

    monitor-enter v3

    .line 602
    :try_start_0
    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;

    if-nez v2, :cond_3

    .line 603
    const-string/jumbo v2, "EmergencyDialer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "playTone: mToneGenerator == null, tone: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    .line 604
    return-void

    .line 608
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/android/phone/MiuiEmergencyDialer;->mToneGenerator:Landroid/media/ToneGenerator;

    const/16 v4, 0x96

    invoke-virtual {v2, p1, v4}, Landroid/media/ToneGenerator;->startTone(II)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v3

    .line 610
    return-void

    .line 601
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method
