.class Lcom/android/phone/CarrierConfigLoader$1;
.super Landroid/os/Handler;
.source "CarrierConfigLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/CarrierConfigLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CarrierConfigLoader;


# direct methods
.method constructor <init>(Lcom/android/phone/CarrierConfigLoader;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/CarrierConfigLoader;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    .line 159
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 22
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 162
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v17, v0

    .line 163
    .local v17, "phoneId":I
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "mHandler: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, " phoneId: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-wrap7(Ljava/lang/String;)V

    .line 170
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v19, v0

    packed-switch v19, :pswitch_data_0

    .line 413
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 174
    :pswitch_1
    invoke-static/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v16

    .line 175
    .local v16, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v16, :cond_1

    .line 176
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/telephony/Phone;->isShuttingDown()Z

    move-result v19

    if-nez v19, :cond_0

    .line 181
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get1(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    aget-object v19, v19, v17

    if-nez v19, :cond_2

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get0(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    aget-object v19, v19, v17

    if-eqz v19, :cond_0

    .line 185
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get1(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    const/16 v20, 0x0

    aput-object v20, v19, v17

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get0(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    const/16 v20, 0x0

    aput-object v20, v19, v17

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get6(Lcom/android/phone/CarrierConfigLoader;)[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    move-result-object v19

    const/16 v20, 0x0

    aput-object v20, v19, v17

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap6(Lcom/android/phone/CarrierConfigLoader;I)V

    goto :goto_0

    .line 192
    .end local v16    # "phone":Lcom/android/internal/telephony/Phone;
    :pswitch_2
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v19

    move/from16 v0, v19

    if-ge v10, v0, :cond_0

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get4(Lcom/android/phone/CarrierConfigLoader;)[Z

    move-result-object v19

    aget-boolean v19, v19, v10

    if-eqz v19, :cond_3

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v10}, Lcom/android/phone/CarrierConfigLoader;->-wrap11(Lcom/android/phone/CarrierConfigLoader;I)V

    .line 192
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 203
    .end local v10    # "i":I
    :pswitch_3
    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    .line 206
    .local v5, "carrierPackageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v5}, Lcom/android/phone/CarrierConfigLoader;->-wrap3(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v14

    .line 208
    .local v14, "numPhones":I
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_2
    if-ge v10, v14, :cond_0

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v10}, Lcom/android/phone/CarrierConfigLoader;->-wrap11(Lcom/android/phone/CarrierConfigLoader;I)V

    .line 208
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 215
    .end local v5    # "carrierPackageName":Ljava/lang/String;
    .end local v10    # "i":I
    .end local v14    # "numPhones":I
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap5(Lcom/android/phone/CarrierConfigLoader;I)Ljava/lang/String;

    move-result-object v11

    .line 216
    .local v11, "iccid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getSimOperatorNumericForPhone(I)Ljava/lang/String;

    move-result-object v15

    .line 218
    .local v15, "operatorNumeric":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/phone/CarrierConfigLoader;->-get5(Lcom/android/phone/CarrierConfigLoader;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v11, v15}, Lcom/android/phone/CarrierConfigLoader;->-wrap0(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/PersistableBundle;

    move-result-object v7

    .line 220
    .local v7, "config":Landroid/os/PersistableBundle;
    if-eqz v7, :cond_4

    .line 222
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "Loaded config from XML. package="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/phone/CarrierConfigLoader;->-get5(Lcom/android/phone/CarrierConfigLoader;)Ljava/lang/String;

    move-result-object v20

    .line 222
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 224
    const-string/jumbo v20, " phoneId="

    .line 222
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 226
    const-string/jumbo v20, " operator="

    .line 222
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 221
    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-wrap7(Ljava/lang/String;)V

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get1(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    aput-object v7, v19, v17

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/phone/CarrierConfigLoader;->-get1(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v20

    aget-object v20, v20, v17

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v15}, Lcom/android/phone/CarrierConfigLoaderInjector;->updateVoLTECarrierConfig(Landroid/content/Context;Landroid/os/PersistableBundle;Ljava/lang/String;)V

    .line 230
    const/16 v19, 0x5

    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/phone/CarrierConfigLoader$1;->obtainMessage(III)Landroid/os/Message;

    move-result-object v13

    .line 231
    .local v13, "newMsg":Landroid/os/Message;
    invoke-virtual {v13}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v19

    const-string/jumbo v20, "loaded_from_xml"

    const/16 v21, 0x1

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get3(Lcom/android/phone/CarrierConfigLoader;)Landroid/os/Handler;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 234
    .end local v13    # "newMsg":Landroid/os/Message;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/phone/CarrierConfigLoader;->-get5(Lcom/android/phone/CarrierConfigLoader;)Ljava/lang/String;

    move-result-object v20

    .line 235
    const/16 v21, 0x3

    .line 234
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v17

    move/from16 v3, v21

    invoke-static {v0, v1, v2, v3, v15}, Lcom/android/phone/CarrierConfigLoader;->-wrap2(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;IILjava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 236
    const/16 v19, 0xa

    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/phone/CarrierConfigLoader$1;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    .line 237
    const-wide/16 v20, 0x7530

    .line 236
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/phone/CarrierConfigLoader$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 240
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap6(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 246
    .end local v7    # "config":Landroid/os/PersistableBundle;
    .end local v11    # "iccid":Ljava/lang/String;
    .end local v15    # "operatorNumeric":Ljava/lang/String;
    :pswitch_5
    const/16 v19, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/phone/CarrierConfigLoader$1;->removeMessages(I)V

    .line 247
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v19

    const-string/jumbo v20, "operator_number"

    const-string/jumbo v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 248
    .restart local v15    # "operatorNumeric":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap1(Lcom/android/phone/CarrierConfigLoader;I)Landroid/service/carrier/CarrierIdentifier;

    move-result-object v4

    .line 249
    .local v4, "carrierId":Landroid/service/carrier/CarrierIdentifier;
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    .line 251
    .local v8, "conn":Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get6(Lcom/android/phone/CarrierConfigLoader;)[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    move-result-object v19

    aget-object v19, v19, v17

    move-object/from16 v0, v19

    if-ne v0, v8, :cond_6

    iget-object v0, v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->service:Landroid/os/IBinder;

    move-object/from16 v19, v0

    if-nez v19, :cond_7

    .line 253
    :cond_6
    iget-boolean v0, v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->bound:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 259
    const/16 v19, 0x0

    move/from16 v0, v19

    iput-boolean v0, v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->bound:Z

    goto/16 :goto_0

    .line 264
    :cond_7
    :try_start_0
    iget-object v0, v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->service:Landroid/os/IBinder;

    move-object/from16 v19, v0

    .line 263
    invoke-static/range {v19 .. v19}, Landroid/service/carrier/ICarrierService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/carrier/ICarrierService;

    move-result-object v6

    .line 265
    .local v6, "carrierService":Landroid/service/carrier/ICarrierService;
    invoke-interface {v6, v4}, Landroid/service/carrier/ICarrierService;->getCarrierConfig(Landroid/service/carrier/CarrierIdentifier;)Landroid/os/PersistableBundle;

    move-result-object v7

    .line 266
    .restart local v7    # "config":Landroid/os/PersistableBundle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap5(Lcom/android/phone/CarrierConfigLoader;I)Ljava/lang/String;

    move-result-object v11

    .line 267
    .restart local v11    # "iccid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/phone/CarrierConfigLoader;->-get5(Lcom/android/phone/CarrierConfigLoader;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v11, v15, v7}, Lcom/android/phone/CarrierConfigLoader;->-wrap9(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/PersistableBundle;)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get1(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    aput-object v7, v19, v17

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/phone/CarrierConfigLoader;->-get1(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v20

    aget-object v20, v20, v17

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v15}, Lcom/android/phone/CarrierConfigLoaderInjector;->updateVoLTECarrierConfig(Landroid/content/Context;Landroid/os/PersistableBundle;Ljava/lang/String;)V

    .line 272
    const/16 v19, 0x5

    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/phone/CarrierConfigLoader$1;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/phone/CarrierConfigLoader$1;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap10(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 273
    .end local v6    # "carrierService":Landroid/service/carrier/ICarrierService;
    .end local v7    # "config":Landroid/os/PersistableBundle;
    .end local v11    # "iccid":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 275
    .local v9, "ex":Ljava/lang/Exception;
    :try_start_1
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "Failed to get carrier config: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-wrap8(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap10(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 276
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v19

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap10(Lcom/android/phone/CarrierConfigLoader;I)V

    .line 276
    throw v19

    .line 286
    .end local v4    # "carrierId":Landroid/service/carrier/CarrierIdentifier;
    .end local v8    # "conn":Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;
    .end local v15    # "operatorNumeric":Ljava/lang/String;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap10(Lcom/android/phone/CarrierConfigLoader;I)V

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap6(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 293
    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v19

    const-string/jumbo v20, "operator_number"

    const-string/jumbo v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 294
    .restart local v15    # "operatorNumeric":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v19

    const-string/jumbo v20, "loaded_from_xml"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    if-nez v19, :cond_8

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get6(Lcom/android/phone/CarrierConfigLoader;)[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    move-result-object v19

    aget-object v19, v19, v17

    if-eqz v19, :cond_0

    .line 299
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getSimOperatorNumericForPhone(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_9

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get1(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    aget-object v19, v19, v17

    if-eqz v19, :cond_9

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get1(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    aget-object v19, v19, v17

    invoke-static/range {v19 .. v19}, Lcom/android/phone/PhoneProxy;->getVolteCarrierConfigValue(Landroid/os/PersistableBundle;)Z

    move-result v19

    .line 299
    if-eqz v19, :cond_9

    .line 302
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoaderInjector;->updateEnhanced4GLteDefaultValue(Landroid/content/Context;I)V

    .line 305
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap4(Lcom/android/phone/CarrierConfigLoader;I)Ljava/lang/String;

    move-result-object v5

    .line 306
    .restart local v5    # "carrierPackageName":Ljava/lang/String;
    if-eqz v5, :cond_a

    .line 307
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "Found carrier config app: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string/jumbo v20, " operator: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-wrap7(Ljava/lang/String;)V

    .line 309
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    const/16 v20, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/android/phone/CarrierConfigLoader$1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    .line 310
    .restart local v13    # "newMsg":Landroid/os/Message;
    invoke-virtual {v13}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v19

    const-string/jumbo v20, "operator_number"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/phone/CarrierConfigLoader$1;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 313
    .end local v13    # "newMsg":Landroid/os/Message;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap6(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 318
    .end local v5    # "carrierPackageName":Ljava/lang/String;
    .end local v15    # "operatorNumeric":Ljava/lang/String;
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap4(Lcom/android/phone/CarrierConfigLoader;I)Ljava/lang/String;

    move-result-object v5

    .line 319
    .restart local v5    # "carrierPackageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap5(Lcom/android/phone/CarrierConfigLoader;I)Ljava/lang/String;

    move-result-object v11

    .line 320
    .restart local v11    # "iccid":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v19

    const-string/jumbo v20, "operator_number"

    const-string/jumbo v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 322
    .restart local v15    # "operatorNumeric":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v5, v11, v15}, Lcom/android/phone/CarrierConfigLoader;->-wrap0(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/PersistableBundle;

    move-result-object v7

    .line 323
    .restart local v7    # "config":Landroid/os/PersistableBundle;
    if-eqz v7, :cond_b

    .line 325
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "Loaded config from XML. package="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 327
    const-string/jumbo v20, " phoneId="

    .line 325
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 329
    const-string/jumbo v20, " operator="

    .line 325
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 324
    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-wrap7(Ljava/lang/String;)V

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get0(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    aput-object v7, v19, v17

    .line 331
    const/16 v19, 0x6

    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/phone/CarrierConfigLoader$1;->obtainMessage(III)Landroid/os/Message;

    move-result-object v13

    .line 332
    .restart local v13    # "newMsg":Landroid/os/Message;
    invoke-virtual {v13}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v19

    const-string/jumbo v20, "loaded_from_xml"

    const/16 v21, 0x1

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 333
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/phone/CarrierConfigLoader$1;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 335
    .end local v13    # "newMsg":Landroid/os/Message;
    :cond_b
    if-eqz v5, :cond_c

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    .line 337
    const/16 v20, 0x4

    .line 336
    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v20

    invoke-static {v0, v5, v1, v2, v15}, Lcom/android/phone/CarrierConfigLoader;->-wrap2(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;IILjava/lang/String;)Z

    move-result v19

    .line 335
    if-eqz v19, :cond_c

    .line 338
    const/16 v19, 0xb

    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/phone/CarrierConfigLoader$1;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    .line 339
    const-wide/16 v20, 0x7530

    .line 338
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/phone/CarrierConfigLoader$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 342
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap6(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 348
    .end local v5    # "carrierPackageName":Ljava/lang/String;
    .end local v7    # "config":Landroid/os/PersistableBundle;
    .end local v11    # "iccid":Ljava/lang/String;
    .end local v15    # "operatorNumeric":Ljava/lang/String;
    :pswitch_9
    const/16 v19, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/phone/CarrierConfigLoader$1;->removeMessages(I)V

    .line 349
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v19

    const-string/jumbo v20, "operator_number"

    const-string/jumbo v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 350
    .restart local v15    # "operatorNumeric":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap1(Lcom/android/phone/CarrierConfigLoader;I)Landroid/service/carrier/CarrierIdentifier;

    move-result-object v4

    .line 351
    .restart local v4    # "carrierId":Landroid/service/carrier/CarrierIdentifier;
    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    .line 353
    .restart local v8    # "conn":Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get6(Lcom/android/phone/CarrierConfigLoader;)[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    move-result-object v19

    aget-object v19, v19, v17

    move-object/from16 v0, v19

    if-ne v0, v8, :cond_d

    .line 354
    iget-object v0, v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->service:Landroid/os/IBinder;

    move-object/from16 v19, v0

    if-nez v19, :cond_e

    .line 356
    :cond_d
    iget-boolean v0, v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->bound:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    .line 360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 362
    const/16 v19, 0x0

    move/from16 v0, v19

    iput-boolean v0, v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->bound:Z

    goto/16 :goto_0

    .line 367
    :cond_e
    :try_start_2
    iget-object v0, v8, Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;->service:Landroid/os/IBinder;

    move-object/from16 v19, v0

    .line 366
    invoke-static/range {v19 .. v19}, Landroid/service/carrier/ICarrierService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/carrier/ICarrierService;

    move-result-object v6

    .line 368
    .restart local v6    # "carrierService":Landroid/service/carrier/ICarrierService;
    invoke-interface {v6, v4}, Landroid/service/carrier/ICarrierService;->getCarrierConfig(Landroid/service/carrier/CarrierIdentifier;)Landroid/os/PersistableBundle;

    move-result-object v7

    .line 369
    .restart local v7    # "config":Landroid/os/PersistableBundle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap4(Lcom/android/phone/CarrierConfigLoader;I)Ljava/lang/String;

    move-result-object v5

    .line 370
    .restart local v5    # "carrierPackageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap5(Lcom/android/phone/CarrierConfigLoader;I)Ljava/lang/String;

    move-result-object v11

    .line 371
    .restart local v11    # "iccid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v5, v11, v15, v7}, Lcom/android/phone/CarrierConfigLoader;->-wrap9(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/PersistableBundle;)V

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get0(Lcom/android/phone/CarrierConfigLoader;)[Landroid/os/PersistableBundle;

    move-result-object v19

    aput-object v7, v19, v17

    .line 373
    const/16 v19, 0x6

    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/phone/CarrierConfigLoader$1;->obtainMessage(III)Landroid/os/Message;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/phone/CarrierConfigLoader$1;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap10(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 374
    .end local v5    # "carrierPackageName":Ljava/lang/String;
    .end local v6    # "carrierService":Landroid/service/carrier/ICarrierService;
    .end local v7    # "config":Landroid/os/PersistableBundle;
    .end local v11    # "iccid":Ljava/lang/String;
    :catch_1
    move-exception v9

    .line 376
    .restart local v9    # "ex":Ljava/lang/Exception;
    :try_start_3
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "Failed to get carrier config: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-wrap8(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap10(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 377
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v19

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap10(Lcom/android/phone/CarrierConfigLoader;I)V

    .line 377
    throw v19

    .line 387
    .end local v4    # "carrierId":Landroid/service/carrier/CarrierIdentifier;
    .end local v8    # "conn":Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;
    .end local v15    # "operatorNumeric":Ljava/lang/String;
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap10(Lcom/android/phone/CarrierConfigLoader;I)V

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap6(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 394
    :pswitch_b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v19

    const-string/jumbo v20, "loaded_from_xml"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    if-nez v19, :cond_f

    .line 395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get6(Lcom/android/phone/CarrierConfigLoader;)[Lcom/android/phone/CarrierConfigLoader$CarrierServiceConnection;

    move-result-object v19

    aget-object v19, v19, v17

    if-eqz v19, :cond_0

    .line 398
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/phone/CarrierConfigLoader;->-wrap6(Lcom/android/phone/CarrierConfigLoader;I)V

    goto/16 :goto_0

    .line 403
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-get2(Lcom/android/phone/CarrierConfigLoader;)Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v18

    .line 404
    .local v18, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string/jumbo v19, "build_fingerprint"

    const/16 v20, 0x0

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 405
    .local v12, "lastFingerprint":Ljava/lang/String;
    sget-object v19, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 406
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "Build fingerprint changed. old: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 407
    const-string/jumbo v20, " new: "

    .line 406
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 407
    sget-object v20, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    .line 406
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/android/phone/CarrierConfigLoader;->-wrap7(Ljava/lang/String;)V

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/CarrierConfigLoader$1;->this$0:Lcom/android/phone/CarrierConfigLoader;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {v19 .. v20}, Lcom/android/phone/CarrierConfigLoader;->-wrap3(Lcom/android/phone/CarrierConfigLoader;Ljava/lang/String;)Z

    .line 409
    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    const-string/jumbo v20, "build_fingerprint"

    sget-object v21, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_9
        :pswitch_7
        :pswitch_b
        :pswitch_4
        :pswitch_8
        :pswitch_3
        :pswitch_6
        :pswitch_a
        :pswitch_c
        :pswitch_2
    .end packed-switch
.end method
