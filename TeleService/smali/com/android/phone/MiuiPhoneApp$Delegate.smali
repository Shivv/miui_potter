.class Lcom/android/phone/MiuiPhoneApp$Delegate;
.super Lmiui/external/ApplicationDelegate;
.source "MiuiPhoneApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/MiuiPhoneApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Delegate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/MiuiPhoneApp;


# direct methods
.method private constructor <init>(Lcom/android/phone/MiuiPhoneApp;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/MiuiPhoneApp;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    invoke-direct {p0}, Lmiui/external/ApplicationDelegate;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/MiuiPhoneApp;Lcom/android/phone/MiuiPhoneApp$Delegate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/MiuiPhoneApp;
    .param p2, "-this1"    # Lcom/android/phone/MiuiPhoneApp$Delegate;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/MiuiPhoneApp$Delegate;-><init>(Lcom/android/phone/MiuiPhoneApp;)V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 4

    .prologue
    .line 53
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_2

    .line 56
    iget-object v1, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    new-instance v2, Lcom/android/phone/PhoneGlobals;

    invoke-direct {v2, p0}, Lcom/android/phone/PhoneGlobals;-><init>(Landroid/content/Context;)V

    iput-object v2, v1, Lcom/android/phone/MiuiPhoneApp;->mPhoneGlobals:Lcom/android/phone/PhoneGlobals;

    .line 57
    iget-object v1, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    iget-object v1, v1, Lcom/android/phone/MiuiPhoneApp;->mPhoneGlobals:Lcom/android/phone/PhoneGlobals;

    invoke-virtual {v1}, Lcom/android/phone/PhoneGlobals;->onCreate()V

    .line 58
    invoke-static {}, Lcom/android/phone/NetworkModeManager;->make()Lcom/android/phone/NetworkModeManager;

    .line 59
    invoke-static {}, Lcom/android/phone/DefaultSlotSelectorImpl;->make()Lcom/android/phone/DefaultSlotSelectorImpl;

    .line 60
    invoke-static {}, Lcom/android/phone/MiuiSimHotSwapManager;->init()V

    .line 61
    iget-object v1, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    iget-object v1, v1, Lcom/android/phone/MiuiPhoneApp;->mPhoneGlobals:Lcom/android/phone/PhoneGlobals;

    invoke-static {v1}, Lcom/android/phone/utils/MiStatInterfaceUtil;->init(Landroid/content/Context;)V

    .line 62
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v1

    array-length v0, v1

    .line 63
    .local v0, "numPhones":I
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiPhoneApp;->-get0()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    new-instance v2, Lcom/android/phone/utils/SimCardStatMonitor;

    invoke-direct {v2}, Lcom/android/phone/utils/SimCardStatMonitor;-><init>()V

    invoke-static {v1, v2}, Lcom/android/phone/MiuiPhoneApp;->-set1(Lcom/android/phone/MiuiPhoneApp;Lcom/android/phone/utils/SimCardStatMonitor;)Lcom/android/phone/utils/SimCardStatMonitor;

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    new-instance v2, Lcom/android/services/telephony/TelephonyGlobals;

    invoke-direct {v2, p0}, Lcom/android/services/telephony/TelephonyGlobals;-><init>(Landroid/content/Context;)V

    iput-object v2, v1, Lcom/android/phone/MiuiPhoneApp;->mTelephonyGlobals:Lcom/android/services/telephony/TelephonyGlobals;

    .line 68
    iget-object v1, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    iget-object v1, v1, Lcom/android/phone/MiuiPhoneApp;->mTelephonyGlobals:Lcom/android/services/telephony/TelephonyGlobals;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyGlobals;->onCreate()V

    .line 70
    iget-object v1, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    new-instance v2, Lcom/android/phone/MiuiNotificationMgr;

    iget-object v3, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    iget-object v3, v3, Lcom/android/phone/MiuiPhoneApp;->mPhoneGlobals:Lcom/android/phone/PhoneGlobals;

    invoke-direct {v2, v3}, Lcom/android/phone/MiuiNotificationMgr;-><init>(Lcom/android/phone/PhoneGlobals;)V

    iput-object v2, v1, Lcom/android/phone/MiuiPhoneApp;->mMiuiNotificationMgr:Lcom/android/phone/MiuiNotificationMgr;

    .line 71
    invoke-static {}, Lcom/android/phone/MiuiLog;->register()V

    .line 72
    invoke-static {}, Lcom/android/phone/MiuiPhoneApp;->-get0()Z

    move-result v1

    if-nez v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/android/phone/MiuiPhoneApp$Delegate;->this$0:Lcom/android/phone/MiuiPhoneApp;

    new-instance v2, Lcom/android/phone/utils/DcStatMonitor;

    invoke-direct {v2}, Lcom/android/phone/utils/DcStatMonitor;-><init>()V

    invoke-static {v1, v2}, Lcom/android/phone/MiuiPhoneApp;->-set0(Lcom/android/phone/MiuiPhoneApp;Lcom/android/phone/utils/DcStatMonitor;)Lcom/android/phone/utils/DcStatMonitor;

    .line 75
    :cond_1
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneReceiver;->register(Landroid/content/Context;)V

    .line 76
    invoke-static {}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->make()Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .line 77
    invoke-static {}, Lcom/android/phone/VolteEnableManager;->init()V

    .line 78
    sget-boolean v1, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v1, :cond_2

    .line 79
    invoke-static {}, Lcom/android/phone/Dual4GManager;->init()V

    .line 82
    .end local v0    # "numPhones":I
    :cond_2
    return-void
.end method
