.class final Lcom/android/services/telephony/CdmaConferenceController;
.super Ljava/lang/Object;
.source "CdmaConferenceController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/CdmaConferenceController$1;
    }
.end annotation


# instance fields
.field private final mCdmaConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/services/telephony/CdmaConnection;",
            ">;"
        }
    .end annotation
.end field

.field private mConference:Lcom/android/services/telephony/CdmaConference;

.field private final mConnectionListener:Landroid/telecom/Connection$Listener;

.field private final mConnectionService:Lcom/android/services/telephony/TelephonyConnectionService;

.field private final mHandler:Landroid/os/Handler;

.field private final mPendingOutgoingConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/services/telephony/CdmaConnection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -wrap0(Lcom/android/services/telephony/CdmaConferenceController;Lcom/android/services/telephony/CdmaConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/CdmaConferenceController;
    .param p1, "connection"    # Lcom/android/services/telephony/CdmaConnection;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/CdmaConferenceController;->addInternal(Lcom/android/services/telephony/CdmaConnection;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/CdmaConferenceController;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/CdmaConferenceController;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConferenceController;->recalculateConference()V

    return-void
.end method

.method public constructor <init>(Lcom/android/services/telephony/TelephonyConnectionService;)V
    .locals 1
    .param p1, "connectionService"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/android/services/telephony/CdmaConferenceController$1;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/CdmaConferenceController$1;-><init>(Lcom/android/services/telephony/CdmaConferenceController;)V

    iput-object v0, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/services/telephony/CdmaConferenceController;->mPendingOutgoingConnections:Ljava/util/List;

    .line 83
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/services/telephony/CdmaConferenceController;->mHandler:Landroid/os/Handler;

    .line 86
    iput-object p1, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConnectionService:Lcom/android/services/telephony/TelephonyConnectionService;

    .line 87
    return-void
.end method

.method private addInternal(Lcom/android/services/telephony/CdmaConnection;)V
    .locals 1
    .param p1, "connection"    # Lcom/android/services/telephony/CdmaConnection;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v0, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {p1, v0}, Lcom/android/services/telephony/CdmaConnection;->addConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 143
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConferenceController;->recalculateConference()V

    .line 144
    return-void
.end method

.method private recalculateConference()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 160
    new-instance v0, Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-direct {v0, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 161
    .local v0, "conferenceConnections":Ljava/util/List;, "Ljava/util/List<Lcom/android/services/telephony/CdmaConnection;>;"
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "connection$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/services/telephony/CdmaConnection;

    .line 163
    .local v1, "connection":Lcom/android/services/telephony/CdmaConnection;
    invoke-virtual {v1}, Lcom/android/services/telephony/CdmaConnection;->isCallWaiting()Z

    move-result v9

    if-nez v9, :cond_0

    .line 164
    invoke-virtual {v1}, Lcom/android/services/telephony/CdmaConnection;->getState()I

    move-result v9

    const/4 v10, 0x6

    if-eq v9, v10, :cond_0

    .line 165
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 169
    .end local v1    # "connection":Lcom/android/services/telephony/CdmaConnection;
    :cond_1
    const-string/jumbo v9, "recalculating conference calls %d"

    new-array v10, v13, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {p0, v9, v10}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x2

    if-lt v9, v10, :cond_8

    .line 171
    const/4 v4, 0x0

    .line 173
    .local v4, "isNewlyCreated":Z
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    iget-object v10, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/services/telephony/CdmaConnection;

    .line 177
    .local v5, "newConnection":Lcom/android/services/telephony/CdmaConnection;
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    if-nez v9, :cond_2

    .line 178
    const-string/jumbo v9, "Creating new Cdma conference call"

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {p0, v9, v10}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180
    invoke-virtual {v5}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v9

    invoke-static {v9}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v8

    .line 181
    .local v8, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    new-instance v9, Lcom/android/services/telephony/CdmaConference;

    invoke-direct {v9, v8}, Lcom/android/services/telephony/CdmaConference;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    iput-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    .line 182
    const/4 v4, 0x1

    .line 185
    .end local v8    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :cond_2
    invoke-virtual {v5}, Lcom/android/services/telephony/CdmaConnection;->isOutgoing()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 187
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Lcom/android/services/telephony/CdmaConference;->updateCapabilities(I)V

    .line 196
    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    invoke-virtual {v9}, Lcom/android/services/telephony/CdmaConference;->getConnections()Ljava/util/List;

    move-result-object v9

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 197
    .local v3, "existingChildConnections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/services/telephony/CdmaConnection;

    .line 198
    .restart local v1    # "connection":Lcom/android/services/telephony/CdmaConnection;
    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 199
    const-string/jumbo v9, "Adding connection to conference call: %s"

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v1, v10, v12

    invoke-static {p0, v9, v10}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    invoke-virtual {v9, v1}, Lcom/android/services/telephony/CdmaConference;->addConnection(Landroid/telecom/Connection;)Z

    .line 202
    :cond_3
    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 191
    .end local v1    # "connection":Lcom/android/services/telephony/CdmaConnection;
    .end local v3    # "existingChildConnections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    :cond_4
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/android/services/telephony/CdmaConference;->updateCapabilities(I)V

    goto :goto_1

    .line 206
    .restart local v3    # "existingChildConnections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    :cond_5
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "oldConnection$iterator":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telecom/Connection;

    .line 207
    .local v6, "oldConnection":Landroid/telecom/Connection;
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    invoke-virtual {v9, v6}, Lcom/android/services/telephony/CdmaConference;->removeConnection(Landroid/telecom/Connection;)V

    .line 208
    const-string/jumbo v9, "Removing connection from conference call: %s"

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v6, v10, v12

    invoke-static {p0, v9, v10}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 212
    .end local v6    # "oldConnection":Landroid/telecom/Connection;
    :cond_6
    if-eqz v4, :cond_7

    .line 213
    const-string/jumbo v9, "Adding the conference call"

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {p0, v9, v10}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 214
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConnectionService:Lcom/android/services/telephony/TelephonyConnectionService;

    iget-object v10, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    invoke-virtual {v9, v10}, Lcom/android/services/telephony/TelephonyConnectionService;->addConference(Landroid/telecom/Conference;)V

    .line 224
    .end local v3    # "existingChildConnections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    .end local v4    # "isNewlyCreated":Z
    .end local v5    # "newConnection":Lcom/android/services/telephony/CdmaConnection;
    .end local v7    # "oldConnection$iterator":Ljava/util/Iterator;
    :cond_7
    :goto_4
    return-void

    .line 216
    :cond_8
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 218
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    if-eqz v9, :cond_7

    .line 219
    const-string/jumbo v9, "Destroying the CDMA conference connection."

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {p0, v9, v10}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    iget-object v9, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    invoke-virtual {v9}, Lcom/android/services/telephony/CdmaConference;->destroy()V

    .line 221
    iput-object v14, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConference:Lcom/android/services/telephony/CdmaConference;

    goto :goto_4
.end method


# virtual methods
.method add(Lcom/android/services/telephony/CdmaConnection;)V
    .locals 8
    .param p1, "connection"    # Lcom/android/services/telephony/CdmaConnection;

    .prologue
    const/4 v4, 0x1

    .line 93
    iget-object v3, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 95
    const-string/jumbo v3, "add - connection already tracked; connection=%s"

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    return-void

    .line 99
    :cond_0
    iget-object v3, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Lcom/android/services/telephony/CdmaConnection;->isOutgoing()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 106
    invoke-virtual {p1, v4}, Lcom/android/services/telephony/CdmaConnection;->forceAsDialing(Z)V

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 109
    .local v0, "connectionsToReset":Ljava/util/List;, "Ljava/util/List<Lcom/android/services/telephony/CdmaConnection;>;"
    iget-object v3, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "current$iterator":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/services/telephony/CdmaConnection;

    .line 110
    .local v1, "current":Lcom/android/services/telephony/CdmaConnection;
    invoke-virtual {v1}, Lcom/android/services/telephony/CdmaConnection;->setHoldingForConference()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 111
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 114
    .end local v1    # "current":Lcom/android/services/telephony/CdmaConnection;
    :cond_2
    iget-object v3, p0, Lcom/android/services/telephony/CdmaConferenceController;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/android/services/telephony/CdmaConferenceController$2;

    invoke-direct {v4, p0, p1, v0}, Lcom/android/services/telephony/CdmaConferenceController$2;-><init>(Lcom/android/services/telephony/CdmaConferenceController;Lcom/android/services/telephony/CdmaConnection;Ljava/util/List;)V

    .line 123
    const-wide/16 v6, 0x1770

    .line 114
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 138
    .end local v0    # "connectionsToReset":Ljava/util/List;, "Ljava/util/List<Lcom/android/services/telephony/CdmaConnection;>;"
    .end local v2    # "current$iterator":Ljava/util/Iterator;
    :goto_1
    return-void

    .line 136
    :cond_3
    iget-object v3, p0, Lcom/android/services/telephony/CdmaConferenceController;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/android/services/telephony/-$Lambda$T3VpF1jA6weO6353r9WnLiKwgL4;

    invoke-direct {v4, p0, p1}, Lcom/android/services/telephony/-$Lambda$T3VpF1jA6weO6353r9WnLiKwgL4;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method synthetic lambda$-com_android_services_telephony_CdmaConferenceController_6648(Lcom/android/services/telephony/CdmaConnection;)V
    .locals 0
    .param p1, "connection"    # Lcom/android/services/telephony/CdmaConnection;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/android/services/telephony/CdmaConferenceController;->addInternal(Lcom/android/services/telephony/CdmaConnection;)V

    return-void
.end method

.method remove(Lcom/android/services/telephony/CdmaConnection;)V
    .locals 3
    .param p1, "connection"    # Lcom/android/services/telephony/CdmaConnection;

    .prologue
    .line 147
    iget-object v0, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    const-string/jumbo v0, "remove - connection not tracked; connection=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/CdmaConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {p1, v0}, Lcom/android/services/telephony/CdmaConnection;->removeConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 155
    iget-object v0, p0, Lcom/android/services/telephony/CdmaConferenceController;->mCdmaConnections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 156
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConferenceController;->recalculateConference()V

    .line 157
    return-void
.end method
