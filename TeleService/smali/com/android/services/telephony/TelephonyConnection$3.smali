.class Lcom/android/services/telephony/TelephonyConnection$3;
.super Lcom/android/internal/telephony/Connection$ListenerBase;
.source "TelephonyConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelephonyConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/TelephonyConnection;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    .line 554
    invoke-direct {p0}, Lcom/android/internal/telephony/Connection$ListenerBase;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onAudioQualityChanged(I)V
    .locals 3
    .param p1, "audioQuality"    # I

    .prologue
    .line 601
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-static {v0}, Lcom/android/services/telephony/TelephonyConnection;->-get0(Lcom/android/services/telephony/TelephonyConnection;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 602
    return-void
.end method

.method public onCallPullFailed(Lcom/android/internal/telephony/Connection;)V
    .locals 4
    .param p1, "externalConnection"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v3, 0x0

    .line 654
    if-nez p1, :cond_0

    .line 655
    return-void

    .line 658
    :cond_0
    const-string/jumbo v0, "onCallPullFailed - pull failed; swapping back to call: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 659
    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 658
    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 663
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    const-string/jumbo v1, "android.telecom.event.CALL_PULL_FAILED"

    invoke-virtual {v0, v1, v3}, Lcom/android/services/telephony/TelephonyConnection;->sendConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 667
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnection;->setOriginalConnection(Lcom/android/internal/telephony/Connection;)V

    .line 670
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->setActiveInternal()V

    .line 671
    return-void
.end method

.method public onConferenceMergedFailed()V
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-static {v0}, Lcom/android/services/telephony/TelephonyConnection;->-wrap1(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 631
    return-void
.end method

.method public onConferenceParticipantsChanged(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/ConferenceParticipant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 611
    .local p1, "participants":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/ConferenceParticipant;>;"
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-static {v0}, Lcom/android/services/telephony/TelephonyConnection;->-get0(Lcom/android/services/telephony/TelephonyConnection;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 612
    return-void
.end method

.method public onConnectionCapabilitiesChanged(I)V
    .locals 3
    .param p1, "capabilities"    # I

    .prologue
    .line 567
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-static {v0}, Lcom/android/services/telephony/TelephonyConnection;->-get0(Lcom/android/services/telephony/TelephonyConnection;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xd

    .line 568
    const/4 v2, 0x0

    .line 567
    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 569
    return-void
.end method

.method public onConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 689
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0, p1, p2}, Lcom/android/services/telephony/TelephonyConnection;->sendConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 690
    return-void
.end method

.method public onExitedEcmMode()V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->handleExitedEcmMode()V

    .line 645
    return-void
.end method

.method public onExtrasChanged(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 635
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-static {v0}, Lcom/android/services/telephony/TelephonyConnection;->-get0(Lcom/android/services/telephony/TelephonyConnection;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 636
    return-void
.end method

.method public onHandoverToWifiFailed()V
    .locals 3

    .prologue
    .line 678
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    const-string/jumbo v1, "android.telephony.event.EVENT_HANDOVER_TO_WIFI_FAILED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/services/telephony/TelephonyConnection;->sendConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 679
    return-void
.end method

.method public onMultipartyStateChanged(Z)V
    .locals 1
    .param p1, "isMultiParty"    # Z

    .prologue
    .line 622
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-static {v0, p1}, Lcom/android/services/telephony/TelephonyConnection;->-wrap2(Lcom/android/services/telephony/TelephonyConnection;Z)V

    .line 623
    return-void
.end method

.method public onRttModifyRequestReceived()V
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->sendRemoteRttRequest()V

    .line 695
    return-void
.end method

.method public onRttModifyResponseReceived(I)V
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 699
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->sendRttInitiationSuccess()V

    .line 704
    :goto_0
    return-void

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnection;->sendRttInitiationFailure(I)V

    goto :goto_0
.end method

.method public onVideoProviderChanged(Landroid/telecom/Connection$VideoProvider;)V
    .locals 2
    .param p1, "videoProvider"    # Landroid/telecom/Connection$VideoProvider;

    .prologue
    .line 579
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-static {v0}, Lcom/android/services/telephony/TelephonyConnection;->-get0(Lcom/android/services/telephony/TelephonyConnection;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 580
    return-void
.end method

.method public onVideoStateChanged(I)V
    .locals 3
    .param p1, "videoState"    # I

    .prologue
    .line 557
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-static {v0}, Lcom/android/services/telephony/TelephonyConnection;->-get0(Lcom/android/services/telephony/TelephonyConnection;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 558
    return-void
.end method

.method public onWifiChanged(Z)V
    .locals 1
    .param p1, "isWifi"    # Z

    .prologue
    .line 590
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection$3;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnection;->setWifi(Z)V

    .line 591
    return-void
.end method
