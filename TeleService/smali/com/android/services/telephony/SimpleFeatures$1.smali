.class final Lcom/android/services/telephony/SimpleFeatures$1;
.super Ljava/lang/Object;
.source "SimpleFeatures.java"

# interfaces
.implements Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor$ExtraContainer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/services/telephony/SimpleFeatures;->accessMiuiConnectionExtra(Landroid/telecom/Connection;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$conn:Landroid/telecom/Connection;


# direct methods
.method constructor <init>(Landroid/telecom/Connection;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/SimpleFeatures$1;->val$conn:Landroid/telecom/Connection;

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/services/telephony/SimpleFeatures$1;->val$conn:Landroid/telecom/Connection;

    invoke-virtual {v0}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/services/telephony/SimpleFeatures$1;->val$conn:Landroid/telecom/Connection;

    invoke-virtual {v0}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "telephony."

    invoke-static {p1, v0, v1}, Lcom/android/phone/common/PhoneConstants;->copyMiuiCallExtras(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    .line 48
    iget-object v0, p0, Lcom/android/services/telephony/SimpleFeatures$1;->val$conn:Landroid/telecom/Connection;

    invoke-virtual {v0, p1}, Landroid/telecom/Connection;->setExtras(Landroid/os/Bundle;)V

    .line 49
    return-void
.end method
