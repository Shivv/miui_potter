.class public interface abstract Lcom/android/services/telephony/TelephonyConnectionServiceProxy;
.super Ljava/lang/Object;
.source "TelephonyConnectionServiceProxy.java"


# virtual methods
.method public abstract addConference(Lcom/android/services/telephony/ImsConference;)V
.end method

.method public abstract addConference(Lcom/android/services/telephony/TelephonyConference;)V
.end method

.method public abstract addConnectionToConferenceController(Lcom/android/services/telephony/TelephonyConnection;)V
.end method

.method public abstract addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;)V
.end method

.method public abstract addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;Landroid/telecom/Conference;)V
.end method

.method public abstract getAllConnections()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/telecom/Connection;",
            ">;"
        }
    .end annotation
.end method

.method public abstract removeConnection(Landroid/telecom/Connection;)V
.end method
