.class public Lcom/android/services/telephony/CdmaConference;
.super Landroid/telecom/Conference;
.source "CdmaConference.java"


# instance fields
.field private mCapabilities:I

.field private mProperties:I


# direct methods
.method public constructor <init>(Landroid/telecom/PhoneAccountHandle;)V
    .locals 1
    .param p1, "phoneAccount"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/telecom/Conference;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    .line 42
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConference;->setActive()V

    .line 44
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/services/telephony/CdmaConference;->mProperties:I

    .line 45
    iget v0, p0, Lcom/android/services/telephony/CdmaConference;->mProperties:I

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/CdmaConference;->setConnectionProperties(I)V

    .line 46
    return-void
.end method

.method private getFirstConnection()Lcom/android/services/telephony/CdmaConnection;
    .locals 2

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConference;->getConnections()Ljava/util/List;

    move-result-object v0

    .line 211
    .local v0, "connections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    const/4 v1, 0x0

    return-object v1

    .line 214
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/services/telephony/CdmaConnection;

    return-object v1
.end method

.method private getOriginalCall()Lcom/android/internal/telephony/Call;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 156
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConference;->getConnections()Ljava/util/List;

    move-result-object v0

    .line 157
    .local v0, "connections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 159
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/Connection;

    invoke-direct {p0, v2}, Lcom/android/services/telephony/CdmaConference;->getOriginalConnection(Landroid/telecom/Connection;)Lcom/android/internal/telephony/Connection;

    move-result-object v1

    .line 160
    .local v1, "originalConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v1, :cond_0

    .line 161
    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v2

    return-object v2

    .line 164
    .end local v1    # "originalConnection":Lcom/android/internal/telephony/Connection;
    :cond_0
    return-object v3
.end method

.method private getOriginalConnection(Landroid/telecom/Connection;)Lcom/android/internal/telephony/Connection;
    .locals 3
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    const/4 v2, 0x0

    .line 201
    instance-of v0, p1, Lcom/android/services/telephony/CdmaConnection;

    if-eqz v0, :cond_0

    .line 202
    check-cast p1, Lcom/android/services/telephony/CdmaConnection;

    .end local p1    # "connection":Landroid/telecom/Connection;
    invoke-virtual {p1}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    return-object v0

    .line 204
    .restart local p1    # "connection":Landroid/telecom/Connection;
    :cond_0
    const-string/jumbo v0, "Non CDMA connection found in a CDMA conference"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v2, v0, v1}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    return-object v2
.end method

.method private final isSwapSupportedAfterMerge()Z
    .locals 7

    .prologue
    .line 174
    const/4 v4, 0x1

    .line 175
    .local v4, "supportSwapAfterMerge":Z
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v3

    .line 177
    .local v3, "context":Landroid/content/Context;
    if-eqz v3, :cond_0

    .line 179
    const-string/jumbo v5, "carrier_config"

    .line 178
    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/CarrierConfigManager;

    .line 182
    .local v2, "configManager":Landroid/telephony/CarrierConfigManager;
    const/4 v0, 0x0

    .line 183
    .local v0, "b":Landroid/os/PersistableBundle;
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConference;->getOriginalCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    .line 184
    .local v1, "call":Lcom/android/internal/telephony/Call;
    if-eqz v1, :cond_1

    .line 185
    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 190
    .local v0, "b":Landroid/os/PersistableBundle;
    :goto_0
    if-eqz v0, :cond_0

    .line 192
    const-string/jumbo v5, "support_swap_after_merge_bool"

    invoke-virtual {v0, v5}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 193
    .local v4, "supportSwapAfterMerge":Z
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Current network support swap after call merged capability is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    .end local v0    # "b":Landroid/os/PersistableBundle;
    .end local v1    # "call":Lcom/android/internal/telephony/Call;
    .end local v2    # "configManager":Landroid/telephony/CarrierConfigManager;
    .end local v4    # "supportSwapAfterMerge":Z
    :cond_0
    return v4

    .line 187
    .local v0, "b":Landroid/os/PersistableBundle;
    .restart local v1    # "call":Lcom/android/internal/telephony/Call;
    .restart local v2    # "configManager":Landroid/telephony/CarrierConfigManager;
    .local v4, "supportSwapAfterMerge":Z
    :cond_1
    invoke-virtual {v2}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .local v0, "b":Landroid/os/PersistableBundle;
    goto :goto_0
.end method

.method private sendFlash()V
    .locals 4

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConference;->getOriginalCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 133
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_0

    .line 136
    :try_start_0
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v2, "Error while trying to send flash command."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public onAddParticipant(Ljava/lang/String;)V
    .locals 3
    .param p1, "participant"    # Ljava/lang/String;

    .prologue
    .line 76
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    const-string/jumbo v1, "Add participant not supported for CDMA conference call."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    return-void
.end method

.method public onDisconnect()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 58
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConference;->getOriginalCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 59
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_0

    .line 60
    const-string/jumbo v2, "Found multiparty call to hangup for conference."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    :try_start_0
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->hangup()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v1

    .line 64
    .local v1, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v2, "Exception thrown trying to hangup conference"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onHold()V
    .locals 3

    .prologue
    .line 81
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    const-string/jumbo v1, "Hold not supported for CDMA conference call."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    return-void
.end method

.method public onMerge()V
    .locals 2

    .prologue
    .line 94
    const-string/jumbo v0, "Merging CDMA conference call."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    iget v0, p0, Lcom/android/services/telephony/CdmaConference;->mCapabilities:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/android/services/telephony/CdmaConference;->mCapabilities:I

    .line 98
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConference;->isSwapSupportedAfterMerge()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget v0, p0, Lcom/android/services/telephony/CdmaConference;->mCapabilities:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/services/telephony/CdmaConference;->mCapabilities:I

    .line 101
    :cond_0
    iget v0, p0, Lcom/android/services/telephony/CdmaConference;->mCapabilities:I

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/CdmaConference;->updateCapabilities(I)V

    .line 102
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConference;->sendFlash()V

    .line 103
    return-void
.end method

.method public onPlayDtmfTone(C)V
    .locals 3
    .param p1, "c"    # C

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConference;->getFirstConnection()Lcom/android/services/telephony/CdmaConnection;

    move-result-object v0

    .line 108
    .local v0, "connection":Lcom/android/services/telephony/CdmaConnection;
    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {v0, p1}, Lcom/android/services/telephony/CdmaConnection;->onPlayDtmfTone(C)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    const-string/jumbo v1, "No CDMA connection found while trying to play dtmf tone."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onSeparate(Landroid/telecom/Connection;)V
    .locals 3
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 71
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    const-string/jumbo v1, "Separate not supported for CDMA conference call."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    return-void
.end method

.method public onStopDtmfTone()V
    .locals 3

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConference;->getFirstConnection()Lcom/android/services/telephony/CdmaConnection;

    move-result-object v0

    .line 118
    .local v0, "connection":Lcom/android/services/telephony/CdmaConnection;
    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {v0}, Lcom/android/services/telephony/CdmaConnection;->onStopDtmfTone()V

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    const-string/jumbo v1, "No CDMA connection found while trying to stop dtmf tone."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onSwap()V
    .locals 2

    .prologue
    .line 127
    const-string/jumbo v0, "Swapping CDMA conference call."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConference;->sendFlash()V

    .line 129
    return-void
.end method

.method public onUnhold()V
    .locals 3

    .prologue
    .line 89
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    const-string/jumbo v1, "Unhold not supported for CDMA conference call."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    return-void
.end method

.method public updateCapabilities(I)V
    .locals 0
    .param p1, "capabilities"    # I

    .prologue
    .line 49
    or-int/lit8 p1, p1, 0x40

    .line 50
    invoke-virtual {p0, p1}, Lcom/android/services/telephony/CdmaConference;->setConnectionCapabilities(I)V

    .line 51
    return-void
.end method
