.class abstract Lcom/android/services/telephony/TelephonyConnection;
.super Landroid/telecom/Connection;
.source "TelephonyConnection.java"

# interfaces
.implements Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/TelephonyConnection$1;,
        Lcom/android/services/telephony/TelephonyConnection$2;,
        Lcom/android/services/telephony/TelephonyConnection$3;,
        Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-internal-telephony-Call$StateSwitchesValues:[I

.field private static final mLock:Ljava/lang/Object;

.field private static final sExtrasMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field isLiveTalkConn:Z

.field private mConnectionOverriddenState:Lcom/android/internal/telephony/Call$State;

.field private mConnectionState:Lcom/android/internal/telephony/Call$State;

.field private final mHandler:Landroid/os/Handler;

.field private mHasHighDefAudio:Z

.field private mIsCarrierVideoConferencingSupported:Z

.field private mIsCdmaVoicePrivacyEnabled:Z

.field private mIsConferenceSupported:Z

.field private mIsEmergencyNumber:Z

.field private mIsMultiParty:Z

.field protected final mIsOutgoing:Z

.field private mIsStateOverridden:Z

.field private mIsVideoPauseSupported:Z

.field private mIsWifi:Z

.field protected mOriginalConnection:Lcom/android/internal/telephony/Connection;

.field private mOriginalConnectionCapabilities:I

.field private mOriginalConnectionExtras:Landroid/os/Bundle;

.field private final mOriginalConnectionListener:Lcom/android/internal/telephony/Connection$Listener;

.field private mOriginalConnectionState:Lcom/android/internal/telephony/Call$State;

.field private mPhoneId:I

.field private final mPostDialListener:Lcom/android/internal/telephony/Connection$PostDialListener;

.field private mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

.field private mSubName:[Ljava/lang/String;

.field private final mTelephonyListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private mTreatAsEmergencyCall:Z

.field private mWasImsConnection:Z


# direct methods
.method static synthetic -get0(Lcom/android/services/telephony/TelephonyConnection;)Landroid/os/Handler;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static synthetic -getcom-android-internal-telephony-Call$StateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/services/telephony/TelephonyConnection;->-com-android-internal-telephony-Call$StateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/services/telephony/TelephonyConnection;->-com-android-internal-telephony-Call$StateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/internal/telephony/Call$State;->values()[Lcom/android/internal/telephony/Call$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DISCONNECTING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    sput-object v0, Lcom/android/services/telephony/TelephonyConnection;->-com-android-internal-telephony-Call$StateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1

    :catch_8
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/services/telephony/TelephonyConnection;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/services/telephony/TelephonyConnection;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/services/telephony/TelephonyConnection;->mWasImsConnection:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/services/telephony/TelephonyConnection;)Lcom/android/internal/telephony/Connection;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->getForegroundConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->handleConferenceMergeFailed()V

    return-void
.end method

.method static synthetic -wrap10(Lcom/android/services/telephony/TelephonyConnection;Ljava/util/List;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p1, "conferenceParticipants"    # Ljava/util/List;

    .prologue
    invoke-virtual {p0, p1}, Lcom/android/services/telephony/TelephonyConnection;->updateConferenceParticipants(Ljava/util/List;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/services/telephony/TelephonyConnection;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p1, "isMultiParty"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/TelephonyConnection;->handleMultipartyStateChange(Z)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/services/telephony/TelephonyConnection;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p1, "asyncResult"    # Landroid/os/AsyncResult;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/TelephonyConnection;->handleSuppServiceNotifyMessage(Landroid/os/AsyncResult;)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->notifyConferenceMergeFailed()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->notifyConferenceStarted()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->refreshConferenceSupported()V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->refreshDisableAddCall()V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->refreshHoldSupported()V

    return-void
.end method

.method static synthetic -wrap9(Lcom/android/services/telephony/TelephonyConnection;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p1, "isEnabled"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/TelephonyConnection;->setCdmaVoicePrivacy(Z)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    invoke-static {}, Lcom/android/services/telephony/TelephonyConnection;->createExtrasMap()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/android/services/telephony/TelephonyConnection;->sExtrasMap:Ljava/util/Map;

    .line 114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/services/telephony/TelephonyConnection;->mLock:Ljava/lang/Object;

    .line 80
    return-void
.end method

.method protected constructor <init>(Lcom/android/internal/telephony/Connection;Ljava/lang/String;Z)V
    .locals 5
    .param p1, "originalConnection"    # Lcom/android/internal/telephony/Connection;
    .param p2, "callId"    # Ljava/lang/String;
    .param p3, "isOutgoingCall"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 782
    invoke-direct {p0}, Landroid/telecom/Connection;-><init>()V

    .line 111
    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    .line 112
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "SIM1"

    aput-object v1, v0, v3

    const-string/jumbo v1, "SIM2"

    aput-object v1, v0, v4

    const-string/jumbo v1, "SIM3"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mSubName:[Ljava/lang/String;

    .line 115
    iput-boolean v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsEmergencyNumber:Z

    .line 119
    iput-boolean v3, p0, Lcom/android/services/telephony/TelephonyConnection;->isLiveTalkConn:Z

    .line 131
    new-instance v0, Lcom/android/services/telephony/TelephonyConnection$1;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelephonyConnection$1;-><init>(Lcom/android/services/telephony/TelephonyConnection;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    .line 532
    new-instance v0, Lcom/android/services/telephony/TelephonyConnection$2;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelephonyConnection$2;-><init>(Lcom/android/services/telephony/TelephonyConnection;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mPostDialListener:Lcom/android/internal/telephony/Connection$PostDialListener;

    .line 554
    new-instance v0, Lcom/android/services/telephony/TelephonyConnection$3;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelephonyConnection$3;-><init>(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 553
    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionListener:Lcom/android/internal/telephony/Connection$Listener;

    .line 708
    sget-object v0, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    .line 709
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    .line 710
    iput-boolean v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsStateOverridden:Z

    .line 711
    sget-object v0, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionState:Lcom/android/internal/telephony/Call$State;

    .line 712
    sget-object v0, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionOverriddenState:Lcom/android/internal/telephony/Call$State;

    .line 719
    iput-boolean v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    .line 780
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x8

    const v2, 0x3f666666    # 0.9f

    invoke-direct {v0, v1, v2, v4}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 779
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mTelephonyListeners:Ljava/util/Set;

    .line 784
    iput-boolean p3, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsOutgoing:Z

    .line 785
    invoke-virtual {p0, p2}, Lcom/android/services/telephony/TelephonyConnection;->setTelecomCallId(Ljava/lang/String;)V

    .line 786
    if-eqz p1, :cond_0

    .line 787
    invoke-virtual {p0, p1}, Lcom/android/services/telephony/TelephonyConnection;->setOriginalConnection(Lcom/android/internal/telephony/Connection;)V

    .line 789
    :cond_0
    return-void
.end method

.method private applyAddParticipantCapabilities(I)I
    .locals 3
    .param p1, "callCapabilities"    # I

    .prologue
    const/high16 v2, 0x2000000

    .line 1955
    move v0, p1

    .line 1956
    .local v0, "currentCapabilities":I
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->isAddParticipantCapable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1958
    const/4 v1, 0x1

    .line 1957
    invoke-direct {p0, p1, v2, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result v0

    .line 1964
    :goto_0
    return v0

    .line 1961
    :cond_0
    const/4 v1, 0x0

    .line 1960
    invoke-direct {p0, p1, v2, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result v0

    goto :goto_0
.end method

.method private applyConferenceTerminationCapabilities(I)I
    .locals 2
    .param p1, "capabilities"    # I

    .prologue
    .line 1936
    move v0, p1

    .line 1940
    .local v0, "currentCapabilities":I
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mWasImsConnection:Z

    if-nez v1, :cond_0

    .line 1941
    or-int/lit16 v0, p1, 0x2000

    .line 1942
    or-int/lit16 v0, v0, 0x1000

    .line 1945
    :cond_0
    return v0
.end method

.method private static areBundlesEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z
    .locals 8
    .param p0, "extras"    # Landroid/os/Bundle;
    .param p1, "newExtras"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1646
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 1647
    :cond_0
    if-ne p0, p1, :cond_1

    :goto_0
    return v4

    :cond_1
    move v4, v5

    goto :goto_0

    .line 1650
    :cond_2
    invoke-virtual {p0}, Landroid/os/Bundle;->size()I

    move-result v6

    invoke-virtual {p1}, Landroid/os/Bundle;->size()I

    move-result v7

    if-eq v6, v7, :cond_3

    .line 1651
    return v5

    .line 1654
    :cond_3
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "key$iterator":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1655
    .local v0, "key":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 1656
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 1657
    .local v3, "value":Ljava/lang/Object;
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 1658
    .local v2, "newValue":Ljava/lang/Object;
    invoke-static {v3, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1659
    return v5

    .line 1663
    .end local v0    # "key":Ljava/lang/String;
    .end local v2    # "newValue":Ljava/lang/Object;
    .end local v3    # "value":Ljava/lang/Object;
    :cond_5
    return v4
.end method

.method private canHoldImsCalls()Z
    .locals 2

    .prologue
    .line 1394
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 1396
    .local v0, "b":Landroid/os/PersistableBundle;
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->doesDeviceRespectHoldCarrierConfig()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    .line 1398
    :cond_0
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->shouldAllowHoldingVideoCall()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1399
    :cond_1
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getVideoState()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 1396
    :goto_0
    return v1

    .line 1397
    :cond_2
    const-string/jumbo v1, "allow_hold_in_ims_call"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1396
    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 1398
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private changeBitmask(IIZ)I
    .locals 1
    .param p1, "bitmask"    # I
    .param p2, "bitfield"    # I
    .param p3, "enabled"    # Z

    .prologue
    .line 2147
    if-eqz p3, :cond_0

    .line 2148
    or-int v0, p1, p2

    return v0

    .line 2150
    :cond_0
    not-int v0, p2

    and-int/2addr v0, p1

    return v0
.end method

.method private static createExtrasMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2318
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2319
    .local v0, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v1, "ChildNum"

    .line 2320
    const-string/jumbo v2, "android.telecom.extra.CHILD_ADDRESS"

    .line 2319
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2321
    const-string/jumbo v1, "DisplayText"

    .line 2322
    const-string/jumbo v2, "android.telecom.extra.CALL_SUBJECT"

    .line 2321
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2323
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method private doesDeviceRespectHoldCarrierConfig()Z
    .locals 3

    .prologue
    .line 1419
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 1420
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v0, :cond_0

    .line 1421
    const/4 v1, 0x1

    return v1

    .line 1423
    :cond_0
    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1424
    const v2, 0x112003d

    .line 1423
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    return v1
.end method

.method private filterCnapName(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "cnapName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 1267
    if-nez p1, :cond_0

    .line 1268
    return-object v4

    .line 1270
    :cond_0
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 1271
    .local v0, "carrierConfig":Landroid/os/PersistableBundle;
    const/4 v1, 0x0

    .line 1272
    .local v1, "filteredCnapNames":[Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1274
    const-string/jumbo v4, "filtered_cnap_names_string_array"

    .line 1273
    invoke-virtual {v0, v4}, Landroid/os/PersistableBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1276
    .end local v1    # "filteredCnapNames":[Ljava/lang/String;
    :cond_1
    if-eqz v1, :cond_2

    .line 1277
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v4

    .line 1279
    new-instance v5, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;

    const/4 v6, 0x3

    invoke-direct {v5, v6, p1}, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;-><init>(BLjava/lang/Object;)V

    .line 1277
    invoke-interface {v4, v5}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/stream/Stream;->count()J

    move-result-wide v2

    .line 1281
    .local v2, "cnapNameMatches":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    .line 1282
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "filterCnapName: Filtered CNAP Name: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1283
    const-string/jumbo v4, ""

    return-object v4

    .line 1286
    .end local v2    # "cnapNameMatches":J
    :cond_2
    return-object p1
.end method

.method private final fireOnOriginalConnectionConfigured()V
    .locals 3

    .prologue
    .line 2217
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mTelephonyListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "l$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    .line 2218
    .local v0, "l":Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;
    invoke-virtual {v0, p0}, Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;->onOriginalConnectionConfigured(Lcom/android/services/telephony/TelephonyConnection;)V

    goto :goto_0

    .line 2220
    .end local v0    # "l":Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;
    :cond_0
    return-void
.end method

.method private final fireOnOriginalConnectionRetryDial(Z)V
    .locals 3
    .param p1, "isPermanentFailure"    # Z

    .prologue
    .line 2223
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mTelephonyListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "l$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    .line 2224
    .local v0, "l":Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;
    invoke-virtual {v0, p0, p1}, Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;->onOriginalConnectionRetry(Lcom/android/services/telephony/TelephonyConnection;Z)V

    goto :goto_0

    .line 2226
    .end local v0    # "l":Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;
    :cond_0
    return-void
.end method

.method private final fireResetDisconnectCause()V
    .locals 3

    .prologue
    .line 2229
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mTelephonyListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "l$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    .line 2230
    .local v0, "l":Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;
    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;->onCallDisconnectResetDisconnectCause()V

    goto :goto_0

    .line 2232
    .end local v0    # "l":Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;
    :cond_0
    return-void
.end method

.method private static getAddressFromNumber(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 2132
    if-nez p0, :cond_0

    .line 2133
    const-string/jumbo p0, ""

    .line 2135
    :cond_0
    const-string/jumbo v0, "tel"

    invoke-static {v0, p0, v1}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private getCarrierConfig()Landroid/os/PersistableBundle;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1403
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 1404
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v0, :cond_0

    .line 1405
    return-object v1

    .line 1407
    :cond_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v1

    return-object v1
.end method

.method private getForegroundConnection()Lcom/android/internal/telephony/Connection;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1553
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1554
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getEarliestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    return-object v0

    .line 1556
    :cond_0
    return-object v1
.end method

.method private handleConferenceMergeFailed()V
    .locals 2

    .prologue
    .line 1823
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1824
    return-void
.end method

.method private handleMultipartyStateChange(Z)V
    .locals 4
    .param p1, "isMultiParty"    # Z

    .prologue
    .line 1840
    const-string/jumbo v1, "Update multiparty state to %s"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    const-string/jumbo v0, "Y"

    :goto_0
    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1841
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1842
    return-void

    .line 1840
    :cond_0
    const-string/jumbo v0, "N"

    goto :goto_0
.end method

.method private handleSuppServiceNotifyMessage(Landroid/os/AsyncResult;)V
    .locals 4
    .param p1, "asyncResult"    # Landroid/os/AsyncResult;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2371
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 2372
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    .line 2375
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    if-nez v0, :cond_1

    .line 2376
    const-string/jumbo v0, "handleSuppServiceNotifyMessage: mSsNotification is null, return"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2377
    return-void

    .line 2380
    :cond_1
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    iget v0, v0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->notificationType:I

    if-ne v0, v3, :cond_2

    .line 2381
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->isRinging()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2383
    const-string/jumbo v0, "handleSuppServiceNotifyMessage: MT, but call state is not Ringing, return"

    .line 2382
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2384
    return-void

    .line 2387
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "handleSuppServiceNotifyMessage: do not support notificationType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2388
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    iget v1, v1, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->notificationType:I

    .line 2387
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2389
    return-void

    .line 2392
    :cond_3
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    iget v0, v0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->code:I

    if-eqz v0, :cond_4

    .line 2393
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    iget v0, v0, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->code:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_5

    .line 2394
    :cond_4
    new-instance v0, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    .line 2395
    new-instance v1, Lcom/android/services/telephony/TelephonyConnection$4;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/TelephonyConnection$4;-><init>(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 2394
    invoke-direct {v0, v1}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;-><init>(Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor$ExtraContainer;)V

    .line 2405
    const-string/jumbo v1, "telephony.IS_FORWARDED_CALL"

    .line 2394
    invoke-virtual {v0, v1, v3}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->putBoolean(Ljava/lang/String;Z)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->commit()V

    .line 2408
    :cond_5
    return-void
.end method

.method private hasHighDefAudioProperty()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1364
    iget-boolean v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHasHighDefAudio:Z

    if-nez v5, :cond_0

    .line 1365
    return v6

    .line 1368
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getVideoState()I

    move-result v5

    invoke-static {v5}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v3

    .line 1370
    .local v3, "isVideoCall":Z
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 1372
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_1

    const-string/jumbo v5, "wifi_calls_can_be_hd_audio"

    invoke-virtual {v0, v5}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 1374
    :goto_0
    if-eqz v0, :cond_2

    const-string/jumbo v5, "video_calls_can_be_hd_audio"

    invoke-virtual {v0, v5}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1376
    :goto_1
    if-eqz v0, :cond_3

    const-string/jumbo v5, "display_hd_audio_property_bool"

    invoke-virtual {v0, v5}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 1378
    :goto_2
    if-nez v4, :cond_4

    .line 1379
    return v6

    .line 1372
    :cond_1
    const/4 v2, 0x0

    .local v2, "canWifiCallsBeHdAudio":Z
    goto :goto_0

    .line 1374
    .end local v2    # "canWifiCallsBeHdAudio":Z
    :cond_2
    const/4 v1, 0x0

    .local v1, "canVideoCallsBeHdAudio":Z
    goto :goto_1

    .line 1376
    .end local v1    # "canVideoCallsBeHdAudio":Z
    :cond_3
    const/4 v4, 0x0

    .local v4, "shouldDisplayHdAudio":Z
    goto :goto_2

    .line 1382
    .end local v4    # "shouldDisplayHdAudio":Z
    :cond_4
    if-eqz v3, :cond_5

    xor-int/lit8 v5, v1, 0x1

    if-eqz v5, :cond_5

    .line 1383
    return v6

    .line 1386
    :cond_5
    iget-boolean v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsWifi:Z

    if-eqz v5, :cond_6

    xor-int/lit8 v5, v2, 0x1

    if-eqz v5, :cond_6

    .line 1387
    return v6

    .line 1390
    :cond_6
    const/4 v5, 0x1

    return v5
.end method

.method private isAddParticipantCapable()Z
    .locals 4

    .prologue
    .line 1968
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1969
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    .line 1970
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsEmergencyNumber:Z

    xor-int/lit8 v1, v1, 0x1

    .line 1968
    if-eqz v1, :cond_3

    .line 1970
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    sget-object v2, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    if-eq v1, v2, :cond_1

    .line 1971
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    sget-object v2, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    if-ne v1, v2, :cond_2

    const/4 v0, 0x1

    .line 1977
    .local v0, "isCapable":Z
    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1978
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    .line 1979
    const-string/jumbo v2, "ConfSupportInd"

    const/4 v3, 0x1

    .line 1978
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1981
    .end local v0    # "isCapable":Z
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "isAddParticipantCapable: isCapable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1982
    return v0

    .line 1970
    :cond_1
    const/4 v0, 0x1

    .restart local v0    # "isCapable":Z
    goto :goto_0

    .line 1971
    .end local v0    # "isCapable":Z
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "isCapable":Z
    goto :goto_0

    .line 1968
    .end local v0    # "isCapable":Z
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "isCapable":Z
    goto :goto_0
.end method

.method private isExternalConnection()Z
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 1900
    iget v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    invoke-static {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1901
    iget v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    invoke-static {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v0

    .line 1900
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPullable()Z
    .locals 2

    .prologue
    .line 1914
    iget v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1915
    iget v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    const/16 v1, 0x20

    invoke-static {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v0

    .line 1914
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidRingingCall()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1577
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1578
    const-string/jumbo v1, "isValidRingingCall, phone is null"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1579
    return v3

    .line 1582
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 1583
    .local v0, "ringingCall":Lcom/android/internal/telephony/Call;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->isRinging()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1584
    const-string/jumbo v1, "isValidRingingCall, ringing call is not in ringing state"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1585
    return v3

    .line 1588
    :cond_1
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getEarliestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eq v1, v2, :cond_2

    .line 1589
    const-string/jumbo v1, "isValidRingingCall, ringing call connection does not match"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1590
    return v3

    .line 1593
    :cond_2
    const-string/jumbo v1, "isValidRingingCall, returning true"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1594
    const/4 v1, 0x1

    return v1
.end method

.method private isVideoCapable()Z
    .locals 2

    .prologue
    .line 1886
    iget v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1887
    iget v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    .line 1888
    const/16 v1, 0x8

    .line 1887
    invoke-static {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v0

    .line 1886
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$-com_android_services_telephony_TelephonyConnection_54807(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "cnapName"    # Ljava/lang/String;
    .param p1, "filteredCnapName"    # Ljava/lang/String;

    .prologue
    .line 1279
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private onDisconnectLivetalkCallback()V
    .locals 2

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->isLiveTalkConn:Z

    iget v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mPhoneId:I

    invoke-static {v0, v1, p0}, Lcom/android/services/telephony/LivetalkTelephonyHelper;->onTeleDisconnectCallback(ZILcom/android/services/telephony/TelephonyConnection;)V

    .line 128
    return-void
.end method

.method private refreshConferenceSupported()V
    .locals 15

    .prologue
    .line 2250
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getVideoState()I

    move-result v11

    invoke-static {v11}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v6

    .line 2251
    .local v6, "isVideoCall":Z
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    .line 2252
    .local v8, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v8, :cond_1

    .line 2253
    const-string/jumbo v11, "refreshConferenceSupported = false; phone is null"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2254
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isConferenceSupported()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2255
    const/4 v11, 0x0

    invoke-virtual {p0, v11}, Lcom/android/services/telephony/TelephonyConnection;->setConferenceSupported(Z)V

    .line 2256
    const/4 v11, 0x0

    invoke-virtual {p0, v11}, Lcom/android/services/telephony/TelephonyConnection;->notifyConferenceSupportedChanged(Z)V

    .line 2258
    :cond_0
    return-void

    .line 2261
    :cond_1
    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v11

    const/4 v12, 0x5

    if-ne v11, v12, :cond_4

    const/4 v3, 0x1

    .line 2262
    .local v3, "isIms":Z
    :goto_0
    const/4 v7, 0x0

    .line 2263
    .local v7, "isVoWifiEnabled":Z
    if-eqz v3, :cond_2

    move-object v0, v8

    .line 2264
    check-cast v0, Lcom/android/internal/telephony/imsphone/ImsPhone;

    .line 2265
    .local v0, "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v12

    invoke-static {v11, v12}, Lcom/android/phone/ImsUtil;->isWfcEnabled(Landroid/content/Context;I)Z

    move-result v7

    .line 2267
    .end local v0    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    .end local v7    # "isVoWifiEnabled":Z
    :cond_2
    if-eqz v3, :cond_5

    .line 2268
    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v11

    .line 2267
    invoke-static {v11}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v9

    .line 2271
    .local v9, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :goto_1
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v11

    .line 2270
    invoke-static {v11}, Lcom/android/services/telephony/TelecomAccountRegistry;->getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;

    move-result-object v10

    .line 2272
    .local v10, "telecomAccountRegistry":Lcom/android/services/telephony/TelecomAccountRegistry;
    invoke-virtual {v10, v9}, Lcom/android/services/telephony/TelecomAccountRegistry;->isMergeCallSupported(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v2

    .line 2274
    .local v2, "isConferencingSupported":Z
    invoke-virtual {v10, v9}, Lcom/android/services/telephony/TelecomAccountRegistry;->isMergeImsCallSupported(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v4

    .line 2276
    .local v4, "isImsConferencingSupported":Z
    invoke-virtual {v10, v9}, Lcom/android/services/telephony/TelecomAccountRegistry;->isVideoConferencingSupported(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v11

    iput-boolean v11, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsCarrierVideoConferencingSupported:Z

    .line 2278
    invoke-virtual {v10, v9}, Lcom/android/services/telephony/TelecomAccountRegistry;->isMergeOfWifiCallsAllowedWhenVoWifiOff(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v5

    .line 2281
    .local v5, "isMergeOfWifiCallsAllowedWhenVoWifiOff":Z
    const-string/jumbo v11, "refreshConferenceSupported : isConfSupp=%b, isImsConfSupp=%b, isVidConfSupp=%b, isMergeOfWifiAllowed=%b, isWifi=%b, isVoWifiEnabled=%b"

    const/4 v12, 0x6

    new-array v12, v12, [Ljava/lang/Object;

    .line 2284
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x0

    aput-object v13, v12, v14

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x1

    aput-object v13, v12, v14

    .line 2285
    iget-boolean v13, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsCarrierVideoConferencingSupported:Z

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x2

    aput-object v13, v12, v14

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x3

    aput-object v13, v12, v14

    .line 2286
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isWifi()Z

    move-result v13

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x4

    aput-object v13, v12, v14

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x5

    aput-object v13, v12, v14

    .line 2281
    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2287
    const/4 v1, 0x1

    .line 2288
    .local v1, "isConferenceSupported":Z
    iget-boolean v11, p0, Lcom/android/services/telephony/TelephonyConnection;->mTreatAsEmergencyCall:Z

    if-eqz v11, :cond_6

    .line 2289
    const/4 v1, 0x0

    .line 2290
    const-string/jumbo v11, "refreshConferenceSupported = false; emergency call"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2305
    :goto_2
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isConferenceSupported()Z

    move-result v11

    if-eq v1, v11, :cond_3

    .line 2306
    invoke-virtual {p0, v1}, Lcom/android/services/telephony/TelephonyConnection;->setConferenceSupported(Z)V

    .line 2307
    invoke-virtual {p0, v1}, Lcom/android/services/telephony/TelephonyConnection;->notifyConferenceSupportedChanged(Z)V

    .line 2309
    :cond_3
    return-void

    .line 2261
    .end local v1    # "isConferenceSupported":Z
    .end local v2    # "isConferencingSupported":Z
    .end local v3    # "isIms":Z
    .end local v4    # "isImsConferencingSupported":Z
    .end local v5    # "isMergeOfWifiCallsAllowedWhenVoWifiOff":Z
    .end local v9    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    .end local v10    # "telecomAccountRegistry":Lcom/android/services/telephony/TelecomAccountRegistry;
    :cond_4
    const/4 v3, 0x0

    .restart local v3    # "isIms":Z
    goto/16 :goto_0

    .line 2269
    :cond_5
    invoke-static {v8}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v9

    .restart local v9    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    goto :goto_1

    .line 2291
    .restart local v1    # "isConferenceSupported":Z
    .restart local v2    # "isConferencingSupported":Z
    .restart local v4    # "isImsConferencingSupported":Z
    .restart local v5    # "isMergeOfWifiCallsAllowedWhenVoWifiOff":Z
    .restart local v10    # "telecomAccountRegistry":Lcom/android/services/telephony/TelecomAccountRegistry;
    :cond_6
    if-eqz v2, :cond_7

    if-eqz v3, :cond_8

    xor-int/lit8 v11, v4, 0x1

    if-eqz v11, :cond_8

    .line 2292
    :cond_7
    const/4 v1, 0x0

    .line 2293
    const-string/jumbo v11, "refreshConferenceSupported = false; carrier doesn\'t support conf."

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 2294
    :cond_8
    if-eqz v6, :cond_9

    iget-boolean v11, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsCarrierVideoConferencingSupported:Z

    xor-int/lit8 v11, v11, 0x1

    if-eqz v11, :cond_9

    .line 2295
    const/4 v1, 0x0

    .line 2296
    const-string/jumbo v11, "refreshConferenceSupported = false; video conf not supported."

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 2297
    :cond_9
    if-nez v5, :cond_a

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isWifi()Z

    move-result v11

    if-eqz v11, :cond_a

    xor-int/lit8 v11, v7, 0x1

    if-eqz v11, :cond_a

    .line 2298
    const/4 v1, 0x0

    .line 2300
    const-string/jumbo v11, "refreshConferenceSupported = false; can\'t merge wifi calls when voWifi off."

    .line 2299
    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 2302
    :cond_a
    const-string/jumbo v11, "refreshConferenceSupported = true."

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private refreshDisableAddCall()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1311
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->shouldSetDisableAddCallExtra()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1312
    const-string/jumbo v0, "android.telecom.extra.DISABLE_ADD_CALL"

    invoke-virtual {p0, v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->putExtra(Ljava/lang/String;Z)V

    .line 1316
    :goto_0
    return-void

    .line 1314
    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, "android.telecom.extra.DISABLE_ADD_CALL"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->removeExtras([Ljava/lang/String;)V

    goto :goto_0
.end method

.method private refreshHoldSupported()V
    .locals 3

    .prologue
    .line 1299
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-nez v0, :cond_0

    .line 1300
    const-string/jumbo v0, "refreshHoldSupported org conn is null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1301
    return-void

    .line 1304
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->shouldAllowHoldingVideoCall()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->canHoldImsCalls()Z

    move-result v0

    .line 1305
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionCapabilities()I

    move-result v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v1

    .line 1304
    if-eq v0, v1, :cond_1

    .line 1306
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionCapabilities()V

    .line 1308
    :cond_1
    return-void
.end method

.method private setCdmaVoicePrivacy(Z)V
    .locals 1
    .param p1, "isEnabled"    # Z

    .prologue
    .line 1922
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsCdmaVoicePrivacyEnabled:Z

    if-eq v0, p1, :cond_0

    .line 1923
    iput-boolean p1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsCdmaVoicePrivacyEnabled:Z

    .line 1924
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionProperties()V

    .line 1926
    :cond_0
    return-void
.end method

.method private setTechnologyTypeExtra()V
    .locals 2

    .prologue
    .line 1293
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1294
    const-string/jumbo v0, "android.telecom.extra.CALL_TECHNOLOGY_TYPE"

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->putExtra(Ljava/lang/String;I)V

    .line 1296
    :cond_0
    return-void
.end method

.method private shouldSetDisableAddCallExtra()Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1319
    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-nez v7, :cond_0

    .line 1320
    return v9

    .line 1323
    :cond_0
    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v7}, Lcom/android/internal/telephony/Connection;->shouldAllowAddCallDuringVideoCall()Z

    move-result v1

    .line 1324
    .local v1, "carrierShouldAllowAddCall":Z
    if-eqz v1, :cond_1

    .line 1325
    return v9

    .line 1327
    :cond_1
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v5

    .line 1328
    .local v5, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v5, :cond_2

    .line 1329
    return v9

    .line 1331
    :cond_2
    const/4 v3, 0x0

    .line 1332
    .local v3, "isCurrentVideoCall":Z
    const/4 v6, 0x0

    .line 1333
    .local v6, "wasVideoCall":Z
    const/4 v4, 0x0

    .line 1334
    .local v4, "isVowifiEnabled":Z
    instance-of v7, v5, Lcom/android/internal/telephony/imsphone/ImsPhone;

    if-eqz v7, :cond_5

    move-object v2, v5

    .line 1335
    check-cast v2, Lcom/android/internal/telephony/imsphone/ImsPhone;

    .line 1336
    .local v2, "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    const/4 v0, 0x0

    .line 1337
    .local v0, "call":Lcom/android/ims/ImsCall;
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getForegroundCall()Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 1338
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getForegroundCall()Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->getImsCall()Lcom/android/ims/ImsCall;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 1339
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getForegroundCall()Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->getImsCall()Lcom/android/ims/ImsCall;

    move-result-object v0

    .line 1347
    .end local v0    # "call":Lcom/android/ims/ImsCall;
    :cond_3
    :goto_0
    if-eqz v0, :cond_4

    .line 1348
    invoke-virtual {v0}, Lcom/android/ims/ImsCall;->isVideoCall()Z

    move-result v3

    .line 1349
    .local v3, "isCurrentVideoCall":Z
    invoke-virtual {v0}, Lcom/android/ims/ImsCall;->wasVideoCall()Z

    move-result v6

    .line 1352
    .end local v3    # "isCurrentVideoCall":Z
    .end local v6    # "wasVideoCall":Z
    :cond_4
    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v8

    invoke-static {v7, v8}, Lcom/android/phone/ImsUtil;->isWfcEnabled(Landroid/content/Context;I)Z

    move-result v4

    .line 1355
    .end local v2    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    .end local v4    # "isVowifiEnabled":Z
    :cond_5
    if-eqz v3, :cond_8

    .line 1356
    return v10

    .line 1340
    .restart local v0    # "call":Lcom/android/ims/ImsCall;
    .restart local v2    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    .local v3, "isCurrentVideoCall":Z
    .restart local v4    # "isVowifiEnabled":Z
    .restart local v6    # "wasVideoCall":Z
    :cond_6
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getBackgroundCall()Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    move-result-object v7

    if-eqz v7, :cond_7

    .line 1341
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getBackgroundCall()Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->getImsCall()Lcom/android/ims/ImsCall;

    move-result-object v7

    if-eqz v7, :cond_7

    .line 1342
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getBackgroundCall()Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->getImsCall()Lcom/android/ims/ImsCall;

    move-result-object v0

    .local v0, "call":Lcom/android/ims/ImsCall;
    goto :goto_0

    .line 1343
    .local v0, "call":Lcom/android/ims/ImsCall;
    :cond_7
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getRingingCall()Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 1344
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getRingingCall()Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->getImsCall()Lcom/android/ims/ImsCall;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 1345
    invoke-virtual {v2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getRingingCall()Lcom/android/internal/telephony/imsphone/ImsPhoneCall;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/imsphone/ImsPhoneCall;->getImsCall()Lcom/android/ims/ImsCall;

    move-result-object v0

    .local v0, "call":Lcom/android/ims/ImsCall;
    goto :goto_0

    .line 1357
    .end local v0    # "call":Lcom/android/ims/ImsCall;
    .end local v2    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    .end local v3    # "isCurrentVideoCall":Z
    .end local v4    # "isVowifiEnabled":Z
    .end local v6    # "wasVideoCall":Z
    :cond_8
    if-eqz v6, :cond_9

    iget-boolean v7, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsWifi:Z

    if-eqz v7, :cond_9

    xor-int/lit8 v7, v4, 0x1

    if-eqz v7, :cond_9

    .line 1358
    return v10

    .line 1360
    :cond_9
    return v9
.end method

.method private updateMultiparty()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1793
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-nez v1, :cond_0

    .line 1794
    return-void

    .line 1797
    :cond_0
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->isMultiparty()Z

    move-result v2

    if-eq v1, v2, :cond_1

    .line 1798
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->isMultiparty()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    .line 1800
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    if-eqz v1, :cond_1

    .line 1801
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->notifyConferenceStarted()V

    .line 1807
    :cond_1
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v1, :cond_2

    .line 1808
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getConnectionExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1809
    .local v0, "connExtras":Landroid/os/Bundle;
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    const-string/jumbo v1, "incomingConference"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1810
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    const-string/jumbo v2, "incomingConference"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    or-int/2addr v1, v2

    iput-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    .line 1811
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "current: isMultiparty:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1815
    .end local v0    # "connExtras":Landroid/os/Bundle;
    :cond_2
    return-void
.end method

.method private updateStatusHints()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2155
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->isValidRingingCall()Z

    move-result v2

    .line 2156
    .local v2, "isIncoming":Z
    iget-boolean v6, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsWifi:Z

    if-eqz v6, :cond_3

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_3

    .line 2157
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 2158
    if-eqz v2, :cond_2

    .line 2159
    const v3, 0x7f0b04ff

    .line 2161
    .local v3, "labelId":I
    :goto_0
    const-string/jumbo v1, ""

    .line 2162
    .local v1, "displaySubId":Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_1

    .line 2163
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 2164
    .local v4, "phoneId":I
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoForSimSlotIndex(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v5

    .line 2166
    .local v5, "sub":Landroid/telephony/SubscriptionInfo;
    if-eqz v5, :cond_1

    .line 2167
    invoke-virtual {v5}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2168
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2172
    .end local v4    # "phoneId":I
    .end local v5    # "sub":Landroid/telephony/SubscriptionInfo;
    :cond_1
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2173
    .local v0, "context":Landroid/content/Context;
    new-instance v6, Landroid/telecom/StatusHints;

    .line 2174
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2176
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 2177
    const v9, 0x7f02008f

    .line 2175
    invoke-static {v8, v9}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Icon;

    move-result-object v8

    .line 2173
    invoke-direct {v6, v7, v8, v10}, Landroid/telecom/StatusHints;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Icon;Landroid/os/Bundle;)V

    invoke-virtual {p0, v6}, Lcom/android/services/telephony/TelephonyConnection;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 2182
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "displaySubId":Ljava/lang/String;
    .end local v3    # "labelId":I
    :goto_1
    return-void

    .line 2160
    :cond_2
    const v3, 0x7f0b0500

    .restart local v3    # "labelId":I
    goto :goto_0

    .line 2180
    .end local v3    # "labelId":I
    :cond_3
    invoke-virtual {p0, v10}, Lcom/android/services/telephony/TelephonyConnection;->setStatusHints(Landroid/telecom/StatusHints;)V

    goto :goto_1
.end method


# virtual methods
.method public final addTelephonyConnectionListener(Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;)Lcom/android/services/telephony/TelephonyConnection;
    .locals 1
    .param p1, "l"    # Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    .prologue
    .line 2190
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mTelephonyListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2193
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v0, :cond_0

    .line 2194
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->fireOnOriginalConnectionConfigured()V

    .line 2196
    :cond_0
    return-object p0
.end method

.method public applyOriginalConnectionCapabilities(I)I
    .locals 3
    .param p1, "capabilities"    # I

    .prologue
    .line 2010
    iget v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    .line 2011
    const/4 v2, 0x3

    .line 2010
    invoke-static {v1, v2}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v0

    .line 2014
    .local v0, "supportsDowngradeToAudio":Z
    xor-int/lit8 v1, v0, 0x1

    const/high16 v2, 0x800000

    .line 2013
    invoke-direct {p0, p1, v2, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result p1

    .line 2017
    iget v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v1

    .line 2016
    const/16 v2, 0xc00

    invoke-direct {p0, p1, v2, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result p1

    .line 2020
    iget v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/android/services/telephony/TelephonyConnection;->can(II)Z

    move-result v1

    .line 2019
    const/16 v2, 0x300

    invoke-direct {p0, p1, v2, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result p1

    .line 2022
    return p1
.end method

.method protected buildConnectionCapabilities()I
    .locals 3

    .prologue
    .line 1045
    const/4 v0, 0x0

    .line 1046
    .local v0, "callCapabilities":I
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1047
    const/high16 v0, 0x40000

    .line 1049
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->shouldTreatAsEmergencyCall()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isImsConnection()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->canHoldImsCalls()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1050
    or-int/lit8 v0, v0, 0x2

    .line 1051
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 1052
    :cond_1
    or-int/lit8 v0, v0, 0x1

    .line 1056
    :cond_2
    return v0
.end method

.method protected buildConnectionProperties()I
    .locals 3

    .prologue
    .line 1076
    const/4 v0, 0x0

    .line 1080
    .local v0, "connectionProperties":I
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 1081
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->isInEcm()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1082
    const/4 v0, 0x1

    .line 1085
    :cond_0
    return v0
.end method

.method clearOriginalConnection()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1440
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v0, :cond_1

    .line 1441
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1442
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForPreciseCallStateChanged(Landroid/os/Handler;)V

    .line 1443
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForRingbackTone(Landroid/os/Handler;)V

    .line 1444
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForHandoverStateChanged(Landroid/os/Handler;)V

    .line 1445
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForDisconnect(Landroid/os/Handler;)V

    .line 1446
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForSuppServiceNotification(Landroid/os/Handler;)V

    .line 1447
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForOnHoldTone(Landroid/os/Handler;)V

    .line 1448
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForInCallVoicePrivacyOn(Landroid/os/Handler;)V

    .line 1449
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForInCallVoicePrivacyOff(Landroid/os/Handler;)V

    .line 1451
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForSuppServiceFailed(Landroid/os/Handler;)V

    .line 1453
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mPostDialListener:Lcom/android/internal/telephony/Connection$PostDialListener;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Connection;->removePostDialListener(Lcom/android/internal/telephony/Connection$PostDialListener;)V

    .line 1454
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionListener:Lcom/android/internal/telephony/Connection$Listener;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Connection;->removeListener(Lcom/android/internal/telephony/Connection$Listener;)V

    .line 1455
    iput-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    .line 1457
    :cond_1
    return-void
.end method

.method public abstract cloneConnection()Lcom/android/services/telephony/TelephonyConnection;
.end method

.method protected close()V
    .locals 2

    .prologue
    .line 1872
    const-string/jumbo v0, "close"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1873
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->clearOriginalConnection()V

    .line 1874
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->destroy()V

    .line 1875
    return-void
.end method

.method protected getCall()Lcom/android/internal/telephony/Call;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1519
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v0, :cond_0

    .line 1520
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    return-object v0

    .line 1522
    :cond_0
    return-object v1
.end method

.method public getConferenceParticipants()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/ConferenceParticipant;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1564
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-nez v0, :cond_0

    .line 1565
    const-string/jumbo v0, "Null mOriginalConnection, cannot get conf participants."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1566
    return-object v2

    .line 1568
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getConferenceParticipants()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getConnectionState()Lcom/android/internal/telephony/Call$State;
    .locals 1

    .prologue
    .line 1514
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    return-object v0
.end method

.method getOriginalConnection()Lcom/android/internal/telephony/Connection;
    .locals 1

    .prologue
    .line 1509
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    return-object v0
.end method

.method getPhone()Lcom/android/internal/telephony/Phone;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1526
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 1527
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_0

    .line 1528
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    return-object v1

    .line 1530
    :cond_0
    return-object v1
.end method

.method public getVideoPauseSupported()Z
    .locals 1

    .prologue
    .line 2091
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsVideoPauseSupported:Z

    return v0
.end method

.method protected handleExitedEcmMode()V
    .locals 0

    .prologue
    .line 2238
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionProperties()V

    .line 2239
    return-void
.end method

.method public handleRttUpgradeResponse(Landroid/telecom/Connection$RttTextStream;)V
    .locals 3
    .param p1, "textStream"    # Landroid/telecom/Connection$RttTextStream;

    .prologue
    .line 937
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isImsConnection()Z

    move-result v1

    if-nez v1, :cond_0

    .line 938
    const-string/jumbo v1, "handleRttUpgradeResponse - not in IMS, so RTT cannot be enabled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 939
    return-void

    .line 941
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    check-cast v0, Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;

    .line 942
    .local v0, "originalConnection":Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;->sendRttModifyResponse(Landroid/telecom/Connection$RttTextStream;)V

    .line 943
    return-void
.end method

.method protected hangup(I)V
    .locals 6
    .param p1, "telephonyDisconnectCode"    # I

    .prologue
    .line 1460
    sget-object v3, Lcom/android/services/telephony/TelephonyConnection;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1461
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    .line 1466
    :try_start_1
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->isValidRingingCall()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1467
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 1468
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_0

    .line 1469
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->hangup()V
    :try_end_1
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "call":Lcom/android/internal/telephony/Call;
    :goto_0
    monitor-exit v3

    .line 1506
    return-void

    .line 1471
    .restart local v0    # "call":Lcom/android/internal/telephony/Call;
    :cond_0
    :try_start_2
    const-string/jumbo v2, "Attempting to hangup a connection without backing call."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v4}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1480
    .end local v0    # "call":Lcom/android/internal/telephony/Call;
    :catch_0
    move-exception v1

    .line 1481
    .local v1, "e":Lcom/android/internal/telephony/CallStateException;
    :try_start_3
    const-string/jumbo v2, "Call to Connection.hangup failed with exception"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v4}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1460
    .end local v1    # "e":Lcom/android/internal/telephony/CallStateException;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 1478
    :cond_1
    :try_start_4
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->hangup()V
    :try_end_4
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1484
    :cond_2
    :try_start_5
    iget-boolean v2, p0, Lcom/android/services/telephony/TelephonyConnection;->isLiveTalkConn:Z

    if-eqz v2, :cond_3

    .line 1486
    const-string/jumbo v2, "livetalk"

    invoke-static {p1, v2}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;)Landroid/telecom/DisconnectCause;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 1487
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->onDisconnectLivetalkCallback()V

    .line 1488
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->close()V

    goto :goto_0

    .line 1491
    :cond_3
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v2

    const/4 v4, 0x6

    if-ne v2, v4, :cond_4

    .line 1492
    const-string/jumbo v2, "hangup called on an already disconnected call!"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1493
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->close()V

    goto :goto_0

    .line 1500
    :cond_4
    const-string/jumbo v2, "Local Disconnect before connection established."

    .line 1501
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 1499
    const/4 v5, 0x3

    .line 1498
    invoke-static {v5, v2, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 1502
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0
.end method

.method protected hasMultipleTopLevelCalls()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1536
    const/4 v0, 0x0

    .line 1537
    .local v0, "numCalls":I
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 1538
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_2

    .line 1539
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->isIdle()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1540
    const/4 v0, 0x1

    .line 1542
    :cond_0
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->isIdle()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1543
    add-int/lit8 v0, v0, 0x1

    .line 1545
    :cond_1
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->isIdle()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1546
    add-int/lit8 v0, v0, 0x1

    .line 1549
    :cond_2
    if-le v0, v2, :cond_3

    :goto_0
    return v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isCarrierVideoConferencingSupported()Z
    .locals 1

    .prologue
    .line 518
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsCarrierVideoConferencingSupported:Z

    return v0
.end method

.method public isConferenceSupported()Z
    .locals 1

    .prologue
    .line 2107
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsConferenceSupported:Z

    return v0
.end method

.method protected isImsConnection()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2116
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    .line 2117
    .local v0, "originalConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v0, :cond_0

    .line 2118
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getPhoneType()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 2117
    :cond_0
    return v1
.end method

.method public isMultiparty()Z
    .locals 1

    .prologue
    .line 2411
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    return v0
.end method

.method protected isOutgoing()Z
    .locals 1

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsOutgoing:Z

    return v0
.end method

.method isOutgoingCall()Z
    .locals 1

    .prologue
    .line 2047
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsOutgoing:Z

    return v0
.end method

.method isWifi()Z
    .locals 1

    .prologue
    .line 2040
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsWifi:Z

    return v0
.end method

.method public onAbort()V
    .locals 2

    .prologue
    .line 850
    const-string/jumbo v0, "onAbort"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 851
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->hangup(I)V

    .line 852
    return-void
.end method

.method public onAnswer(I)V
    .locals 4
    .param p1, "videoState"    # I

    .prologue
    const/4 v3, 0x0

    .line 866
    const-string/jumbo v1, "onAnswer"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 867
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->isValidRingingCall()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 869
    :try_start_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/Phone;->acceptCall(I)V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 874
    :cond_0
    :goto_0
    return-void

    .line 870
    :catch_0
    move-exception v0

    .line 871
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v1, "Failed to accept call."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onCallAudioStateChanged(Landroid/telecom/CallAudioState;)V
    .locals 1
    .param p1, "audioState"    # Landroid/telecom/CallAudioState;

    .prologue
    .line 801
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 802
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->setEchoSuppressionEnabled()V

    .line 804
    :cond_0
    return-void
.end method

.method public onConnectionRemoved(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 2
    .param p1, "conn"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 915
    if-eq p1, p0, :cond_0

    .line 916
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 918
    :cond_0
    return-void
.end method

.method public onDisconnect()V
    .locals 2

    .prologue
    .line 814
    const-string/jumbo v0, "onDisconnect"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 815
    invoke-static {}, Landroid/telephony/PhoneNumberUtils;->resetCountryDetectorInfo()V

    .line 816
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->hangup(I)V

    .line 817
    return-void
.end method

.method public onDisconnectConferenceParticipant(Landroid/net/Uri;)V
    .locals 3
    .param p1, "endpoint"    # Landroid/net/Uri;

    .prologue
    .line 827
    const-string/jumbo v0, "onDisconnectConferenceParticipant %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 829
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-nez v0, :cond_0

    .line 830
    return-void

    .line 833
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/Connection;->onDisconnectConferenceParticipant(Landroid/net/Uri;)V

    .line 834
    return-void
.end method

.method public onHold()V
    .locals 0

    .prologue
    .line 856
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->performHold()V

    .line 857
    return-void
.end method

.method public onPostDialContinue(Z)V
    .locals 2
    .param p1, "proceed"    # Z

    .prologue
    .line 887
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onPostDialContinue, proceed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 888
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v0, :cond_0

    .line 889
    if-eqz p1, :cond_1

    .line 890
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->proceedAfterWaitChar()V

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 892
    :cond_1
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->cancelPostDial()V

    goto :goto_0
.end method

.method public onPullExternalCall()V
    .locals 2

    .prologue
    .line 902
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionProperties()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    .line 903
    const/16 v1, 0x10

    .line 902
    if-eq v0, v1, :cond_0

    .line 904
    const-string/jumbo v0, "onPullExternalCall - cannot pull non-external call"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 905
    return-void

    .line 908
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v0, :cond_1

    .line 909
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->pullExternalCall()V

    .line 911
    :cond_1
    return-void
.end method

.method public onReject()V
    .locals 2

    .prologue
    .line 878
    const-string/jumbo v0, "onReject"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 879
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->isValidRingingCall()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 880
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->hangup(I)V

    .line 882
    :cond_0
    invoke-super {p0}, Landroid/telecom/Connection;->onReject()V

    .line 883
    return-void
.end method

.method public onSeparate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 838
    const-string/jumbo v1, "onSeparate"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 839
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v1, :cond_0

    .line 841
    :try_start_0
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->separate()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 846
    :cond_0
    :goto_0
    return-void

    .line 842
    :catch_0
    move-exception v0

    .line 843
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v1, "Call to Connection.separate failed with exception"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onStartRtt(Landroid/telecom/Connection$RttTextStream;)V
    .locals 3
    .param p1, "textStream"    # Landroid/telecom/Connection$RttTextStream;

    .prologue
    .line 922
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isImsConnection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 923
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    check-cast v0, Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;

    .line 924
    .local v0, "originalConnection":Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;
    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;->sendRttModifyRequest(Landroid/telecom/Connection$RttTextStream;)V

    .line 928
    .end local v0    # "originalConnection":Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;
    :goto_0
    return-void

    .line 926
    :cond_0
    const-string/jumbo v1, "onStartRtt - not in IMS, so RTT cannot be enabled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 808
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onStateChanged, state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 809
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateStatusHints()V

    .line 810
    return-void
.end method

.method public onStopRtt()V
    .locals 0

    .prologue
    .line 933
    return-void
.end method

.method public onUnhold()V
    .locals 0

    .prologue
    .line 861
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->performUnhold()V

    .line 862
    return-void
.end method

.method public performAddParticipant(Ljava/lang/String;)V
    .locals 4
    .param p1, "participant"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1027
    const-string/jumbo v1, "performAddParticipant - %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1028
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1033
    :try_start_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/internal/telephony/Phone;->addParticipant(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1038
    :cond_0
    :goto_0
    return-void

    .line 1034
    :catch_0
    move-exception v0

    .line 1035
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v1, "Failed to performAddParticipant."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public performConference(Landroid/telecom/Connection;)V
    .locals 4
    .param p1, "otherConnection"    # Landroid/telecom/Connection;

    .prologue
    const/4 v3, 0x0

    .line 1011
    const-string/jumbo v1, "performConference - %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1012
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1019
    :try_start_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->conference()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1024
    :cond_0
    :goto_0
    return-void

    .line 1020
    :catch_0
    move-exception v0

    .line 1021
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v1, "Failed to conference call."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public performHold()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 946
    const-string/jumbo v3, "performHold"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 949
    sget-object v3, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    if-ne v3, v4, :cond_1

    .line 950
    const-string/jumbo v3, "Holding active call"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 952
    :try_start_0
    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 953
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v2

    .line 965
    .local v2, "ringingCall":Lcom/android/internal/telephony/Call;
    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    if-eq v3, v4, :cond_0

    .line 966
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 976
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v2    # "ringingCall":Lcom/android/internal/telephony/Call;
    :cond_0
    :goto_0
    return-void

    .line 970
    :catch_0
    move-exception v0

    .line 971
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v3, "Exception occurred while trying to put call on hold."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v0, v3, v4}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 974
    .end local v0    # "e":Lcom/android/internal/telephony/CallStateException;
    :cond_1
    const-string/jumbo v3, "Cannot put a call that is not currently active on hold."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public performUnhold()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 979
    const-string/jumbo v1, "performUnhold"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 980
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    if-ne v1, v2, :cond_1

    .line 997
    :try_start_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->hasMultipleTopLevelCalls()Z

    move-result v1

    if-nez v1, :cond_0

    .line 998
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V

    .line 1008
    :goto_0
    return-void

    .line 1000
    :cond_0
    const-string/jumbo v1, "Skipping unhold command for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1002
    :catch_0
    move-exception v0

    .line 1003
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v1, "Exception occurred while trying to release call from hold."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1006
    .end local v0    # "e":Lcom/android/internal/telephony/CallStateException;
    :cond_1
    const-string/jumbo v1, "Cannot release a call that is not already on hold from hold."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final removeTelephonyConnectionListener(Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;)Lcom/android/services/telephony/TelephonyConnection;
    .locals 1
    .param p1, "l"    # Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    .prologue
    .line 2206
    if-eqz p1, :cond_0

    .line 2207
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mTelephonyListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2209
    :cond_0
    return-object p0
.end method

.method resetStateForConference()V
    .locals 2

    .prologue
    .line 2063
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 2064
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->resetStateOverride()V

    .line 2066
    :cond_0
    return-void
.end method

.method resetStateOverride()V
    .locals 1

    .prologue
    .line 1675
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsStateOverridden:Z

    .line 1676
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateStateInternal()V

    .line 1677
    return-void
.end method

.method protected setActiveInternal()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 1847
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 1848
    const-string/jumbo v3, "Should not be called if this is already ACTIVE"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1849
    return-void

    .line 1858
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionService()Landroid/telecom/ConnectionService;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1859
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionService()Landroid/telecom/ConnectionService;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telecom/ConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "current$iterator":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 1860
    .local v0, "current":Landroid/telecom/Connection;
    if-eq v0, p0, :cond_1

    instance-of v3, v0, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v3, :cond_1

    move-object v2, v0

    .line 1861
    check-cast v2, Lcom/android/services/telephony/TelephonyConnection;

    .line 1862
    .local v2, "other":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual {v2}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v3

    if-ne v3, v4, :cond_1

    .line 1863
    invoke-virtual {v2}, Lcom/android/services/telephony/TelephonyConnection;->updateState()V

    goto :goto_0

    .line 1868
    .end local v0    # "current":Landroid/telecom/Connection;
    .end local v1    # "current$iterator":Ljava/util/Iterator;
    .end local v2    # "other":Lcom/android/services/telephony/TelephonyConnection;
    :cond_2
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->setActive()V

    .line 1869
    return-void
.end method

.method public setAudioQuality(I)V
    .locals 1
    .param p1, "audioQuality"    # I

    .prologue
    .line 2058
    const/4 v0, 0x2

    .line 2057
    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mHasHighDefAudio:Z

    .line 2059
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionProperties()V

    .line 2060
    return-void

    .line 2057
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setConferenceSupported(Z)V
    .locals 0
    .param p1, "isConferenceSupported"    # Z

    .prologue
    .line 2100
    iput-boolean p1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsConferenceSupported:Z

    .line 2101
    return-void
.end method

.method setHoldingForConference()Z
    .locals 2

    .prologue
    .line 2069
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 2070
    sget-object v0, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->setStateOverride(Lcom/android/internal/telephony/Call$State;)V

    .line 2071
    const/4 v0, 0x1

    return v0

    .line 2073
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method setOriginalConnection(Lcom/android/internal/telephony/Connection;)V
    .locals 9
    .param p1, "originalConnection"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1171
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "new TelephonyConnection, originalConnection: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1172
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->clearOriginalConnection()V

    .line 1173
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/os/Bundle;->clear()V

    .line 1174
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    .line 1175
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getTelecomCallId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/Connection;->setTelecomCallId(Ljava/lang/String;)V

    .line 1177
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1178
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5, v7, v3}, Lcom/android/internal/telephony/Phone;->registerForPreciseCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 1180
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    .line 1181
    const/4 v6, 0x3

    .line 1180
    invoke-virtual {v4, v5, v6, v3}, Lcom/android/internal/telephony/Phone;->registerForHandoverStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 1182
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6, v3}, Lcom/android/internal/telephony/Phone;->registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 1184
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x4

    invoke-virtual {v4, v5, v6, v3}, Lcom/android/internal/telephony/Phone;->registerForDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 1185
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    .line 1186
    const/4 v6, 0x7

    .line 1185
    invoke-virtual {v4, v5, v6, v3}, Lcom/android/internal/telephony/Phone;->registerForSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 1187
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    const/16 v6, 0xe

    invoke-virtual {v4, v5, v6, v3}, Lcom/android/internal/telephony/Phone;->registerForOnHoldTone(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 1188
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    .line 1189
    const/16 v6, 0xf

    .line 1188
    invoke-virtual {v4, v5, v6, v3}, Lcom/android/internal/telephony/Phone;->registerForInCallVoicePrivacyOn(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 1190
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    .line 1191
    const/16 v6, 0x10

    .line 1190
    invoke-virtual {v4, v5, v6, v3}, Lcom/android/internal/telephony/Phone;->registerForInCallVoicePrivacyOff(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 1193
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    const/16 v6, 0x12

    invoke-virtual {v4, v5, v6, v3}, Lcom/android/internal/telephony/Phone;->registerForSuppServiceFailed(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 1196
    :cond_0
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mPostDialListener:Lcom/android/internal/telephony/Connection$PostDialListener;

    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/Connection;->addPostDialListener(Lcom/android/internal/telephony/Connection$PostDialListener;)V

    .line 1197
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionListener:Lcom/android/internal/telephony/Connection$Listener;

    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/Connection;->addListener(Lcom/android/internal/telephony/Connection$Listener;)V

    .line 1199
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1200
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsEmergencyNumber:Z

    .line 1204
    :cond_1
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateAddress()V

    .line 1207
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getVideoState()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/TelephonyConnection;->setVideoState(I)V

    .line 1208
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getConnectionCapabilities()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/TelephonyConnection;->setOriginalConnectionCapabilities(I)V

    .line 1209
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->isWifi()Z

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/TelephonyConnection;->setWifi(Z)V

    .line 1210
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getAudioModeIsVoip()Z

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/TelephonyConnection;->setAudioModeIsVoip(Z)V

    .line 1211
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getVideoProvider()Landroid/telecom/Connection$VideoProvider;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/TelephonyConnection;->setVideoProvider(Landroid/telecom/Connection$VideoProvider;)V

    .line 1212
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getAudioQuality()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/TelephonyConnection;->setAudioQuality(I)V

    .line 1213
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->setTechnologyTypeExtra()V

    .line 1218
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getConnectionExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1219
    .local v0, "connExtras":Landroid/os/Bundle;
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_4

    :goto_0
    const/16 v5, 0xc

    invoke-virtual {v4, v5, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 1222
    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1223
    iput-boolean v7, p0, Lcom/android/services/telephony/TelephonyConnection;->mTreatAsEmergencyCall:Z

    .line 1226
    :cond_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1227
    .local v1, "extrasToPut":Landroid/os/Bundle;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1229
    .local v2, "extrasToRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isImsConnection()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1230
    iput-boolean v7, p0, Lcom/android/services/telephony/TelephonyConnection;->mWasImsConnection:Z

    .line 1235
    :goto_1
    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Connection;->isMultiparty()Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsMultiParty:Z

    .line 1237
    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Connection;->isActiveCallDisconnectedOnAnswer()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1238
    const-string/jumbo v3, "android.telecom.extra.ANSWERING_DROPS_FG_CALL"

    invoke-virtual {v1, v3, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1243
    :goto_2
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->shouldSetDisableAddCallExtra()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1244
    const-string/jumbo v3, "android.telecom.extra.DISABLE_ADD_CALL"

    invoke-virtual {v1, v3, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1248
    :goto_3
    invoke-virtual {p0, v1}, Lcom/android/services/telephony/TelephonyConnection;->putExtras(Landroid/os/Bundle;)V

    .line 1249
    invoke-virtual {p0, v2}, Lcom/android/services/telephony/TelephonyConnection;->removeExtras(Ljava/util/List;)V

    .line 1253
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateState()V

    .line 1254
    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-nez v3, :cond_3

    .line 1255
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "original Connection was nulled out as part of setOriginalConnection. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1259
    :cond_3
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->fireOnOriginalConnectionConfigured()V

    .line 1260
    return-void

    .line 1220
    .end local v1    # "extrasToPut":Landroid/os/Bundle;
    .end local v2    # "extrasToRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1232
    .restart local v1    # "extrasToPut":Landroid/os/Bundle;
    .restart local v2    # "extrasToRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    const-string/jumbo v3, "phoneId"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1240
    :cond_6
    const-string/jumbo v3, "android.telecom.extra.ANSWERING_DROPS_FG_CALL"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1246
    :cond_7
    const-string/jumbo v3, "android.telecom.extra.DISABLE_ADD_CALL"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method public setOriginalConnectionCapabilities(I)V
    .locals 0
    .param p1, "connectionCapabilities"    # I

    .prologue
    .line 1992
    iput p1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionCapabilities:I

    .line 1993
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionCapabilities()V

    .line 1994
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionProperties()V

    .line 1995
    return-void
.end method

.method setStateOverride(Lcom/android/internal/telephony/Call$State;)V
    .locals 1
    .param p1, "state"    # Lcom/android/internal/telephony/Call$State;

    .prologue
    .line 1667
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsStateOverridden:Z

    .line 1668
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionOverriddenState:Lcom/android/internal/telephony/Call$State;

    .line 1670
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionState:Lcom/android/internal/telephony/Call$State;

    .line 1671
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateStateInternal()V

    .line 1672
    return-void
.end method

.method public setUseLivetalk(ZI)V
    .locals 0
    .param p1, "useLivetalk"    # Z
    .param p2, "phoneId"    # I

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/android/services/telephony/TelephonyConnection;->isLiveTalkConn:Z

    .line 123
    iput p2, p0, Lcom/android/services/telephony/TelephonyConnection;->mPhoneId:I

    .line 124
    return-void
.end method

.method public setVideoPauseSupported(Z)V
    .locals 0
    .param p1, "isVideoPauseSupported"    # Z

    .prologue
    .line 2083
    iput-boolean p1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsVideoPauseSupported:Z

    .line 2084
    return-void
.end method

.method public setWifi(Z)V
    .locals 0
    .param p1, "isWifi"    # Z

    .prologue
    .line 2030
    iput-boolean p1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsWifi:Z

    .line 2031
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionProperties()V

    .line 2032
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateStatusHints()V

    .line 2033
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->refreshDisableAddCall()V

    .line 2034
    return-void
.end method

.method protected shouldTreatAsEmergencyCall()Z
    .locals 1

    .prologue
    .line 1433
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mTreatAsEmergencyCall:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2335
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[TelephonyConnection objId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2336
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2337
    const-string/jumbo v1, " telecomCallID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2338
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getTelecomCallId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2339
    const-string/jumbo v1, " type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2340
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isImsConnection()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2341
    const-string/jumbo v1, "ims"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2347
    :cond_0
    :goto_0
    const-string/jumbo v1, " state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2348
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2349
    const-string/jumbo v1, " capabilities:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2350
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionCapabilities()I

    move-result v1

    invoke-static {v1}, Lcom/android/services/telephony/TelephonyConnection;->capabilitiesToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2351
    const-string/jumbo v1, " properties:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2352
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionProperties()I

    move-result v1

    invoke-static {v1}, Lcom/android/services/telephony/TelephonyConnection;->propertiesToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2353
    const-string/jumbo v1, " address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2354
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getAddress()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2355
    const-string/jumbo v1, " originalConnection:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2356
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2357
    const-string/jumbo v1, " partOfConf:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2358
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConference()Landroid/telecom/Conference;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2359
    const-string/jumbo v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2363
    :goto_1
    const-string/jumbo v1, " confSupported:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2364
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsConferenceSupported:Z

    if-eqz v1, :cond_4

    const-string/jumbo v1, "Y"

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2365
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2366
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 2342
    :cond_1
    instance-of v1, p0, Lcom/android/services/telephony/GsmConnection;

    if-eqz v1, :cond_2

    .line 2343
    const-string/jumbo v1, "gsm"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 2344
    :cond_2
    instance-of v1, p0, Lcom/android/services/telephony/CdmaConnection;

    if-eqz v1, :cond_0

    .line 2345
    const-string/jumbo v1, "cdma"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 2361
    :cond_3
    const-string/jumbo v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2364
    :cond_4
    const-string/jumbo v1, "N"

    goto :goto_2
.end method

.method protected final updateAddress()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1109
    iget-boolean v9, p0, Lcom/android/services/telephony/TelephonyConnection;->isLiveTalkConn:Z

    if-eqz v9, :cond_0

    .line 1110
    return-void

    .line 1113
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionCapabilities()V

    .line 1114
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionProperties()V

    .line 1115
    iget-object v9, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v9, :cond_8

    .line 1117
    const/4 v8, 0x0

    .line 1118
    .local v8, "showOrigDialString":Z
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v6

    .line 1119
    .local v6, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_1

    .line 1120
    iget-object v9, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v9}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    .line 1119
    if-eqz v9, :cond_1

    .line 1121
    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 1122
    const-string/jumbo v10, "carrier_config"

    .line 1121
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/CarrierConfigManager;

    .line 1123
    .local v1, "configManager":Landroid/telephony/CarrierConfigManager;
    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v5

    .line 1124
    .local v5, "pb":Landroid/os/PersistableBundle;
    if-eqz v5, :cond_1

    .line 1125
    const-string/jumbo v9, "config_show_orig_dial_string_for_cdma"

    invoke-virtual {v5, v9}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 1126
    .local v8, "showOrigDialString":Z
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "showOrigDialString: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-array v10, v11, [Ljava/lang/Object;

    invoke-static {p0, v9, v10}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1129
    .end local v1    # "configManager":Landroid/telephony/CarrierConfigManager;
    .end local v5    # "pb":Landroid/os/PersistableBundle;
    .end local v8    # "showOrigDialString":Z
    :cond_1
    if-eqz v8, :cond_9

    .line 1131
    iget-object v9, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v9}, Lcom/android/internal/telephony/Connection;->getOrigDialString()Ljava/lang/String;

    move-result-object v9

    .line 1130
    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortionAlt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1132
    .local v2, "dialPart":Ljava/lang/String;
    invoke-static {v2}, Lcom/android/services/telephony/TelephonyConnection;->getAddressFromNumber(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1136
    .end local v2    # "dialPart":Ljava/lang/String;
    .local v0, "address":Landroid/net/Uri;
    :goto_0
    iget-object v9, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v9}, Lcom/android/internal/telephony/Connection;->getNumberPresentation()I

    move-result v7

    .line 1137
    .local v7, "presentation":I
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getAddress()Landroid/net/Uri;

    move-result-object v9

    invoke-static {v0, v9}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1138
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getAddressPresentation()I

    move-result v9

    if-eq v7, v9, :cond_4

    .line 1139
    :cond_2
    const-string/jumbo v9, "updateAddress, address changed"

    new-array v10, v11, [Ljava/lang/Object;

    invoke-static {p0, v9, v10}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1140
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionProperties()I

    move-result v9

    and-int/lit8 v9, v9, 0x40

    if-eqz v9, :cond_3

    .line 1141
    const/4 v0, 0x0

    .line 1144
    .end local v0    # "address":Landroid/net/Uri;
    :cond_3
    invoke-static {p0, v0}, Lcom/android/services/telephony/SimpleFeatures;->updateAddresIfCallForwdNumExist(Lcom/android/services/telephony/TelephonyConnection;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 1145
    .restart local v0    # "address":Landroid/net/Uri;
    invoke-virtual {p0, v0, v7}, Lcom/android/services/telephony/TelephonyConnection;->setAddress(Landroid/net/Uri;I)V

    .line 1148
    :cond_4
    iget-object v9, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v9}, Lcom/android/internal/telephony/Connection;->getCnapName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/android/services/telephony/TelephonyConnection;->filterCnapName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1149
    .local v3, "name":Ljava/lang/String;
    iget-object v9, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v9}, Lcom/android/internal/telephony/Connection;->getCnapNamePresentation()I

    move-result v4

    .line 1150
    .local v4, "namePresentation":I
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getCallerDisplayName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1151
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getCallerDisplayNamePresentation()I

    move-result v9

    if-eq v4, v9, :cond_6

    .line 1152
    :cond_5
    const-string/jumbo v9, "updateAddress, caller display name changed"

    new-array v10, v11, [Ljava/lang/Object;

    invoke-static {p0, v9, v10}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1153
    invoke-virtual {p0, v3, v4}, Lcom/android/services/telephony/TelephonyConnection;->setCallerDisplayName(Ljava/lang/String;I)V

    .line 1156
    :cond_6
    iget-object v9, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v9}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1157
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/android/services/telephony/TelephonyConnection;->mTreatAsEmergencyCall:Z

    .line 1162
    :cond_7
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->refreshConferenceSupported()V

    .line 1164
    .end local v0    # "address":Landroid/net/Uri;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "namePresentation":I
    .end local v6    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v7    # "presentation":I
    :cond_8
    return-void

    .line 1134
    .restart local v6    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_9
    iget-object v9, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v9}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/services/telephony/TelephonyConnection;->getAddressFromNumber(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .restart local v0    # "address":Landroid/net/Uri;
    goto :goto_0
.end method

.method protected final updateConnectionCapabilities()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1060
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->buildConnectionCapabilities()I

    move-result v0

    .line 1062
    .local v0, "newCapabilities":I
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->applyOriginalConnectionCapabilities(I)I

    move-result v0

    .line 1064
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsVideoPauseSupported:Z

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->isVideoCapable()Z

    move-result v1

    .line 1063
    :goto_0
    const/high16 v3, 0x100000

    invoke-direct {p0, v0, v3, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result v0

    .line 1066
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->isExternalConnection()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->isPullable()Z

    move-result v2

    .line 1065
    :cond_0
    const/high16 v1, 0x1000000

    invoke-direct {p0, v0, v1, v2}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result v0

    .line 1067
    invoke-direct {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->applyConferenceTerminationCapabilities(I)I

    move-result v0

    .line 1068
    invoke-direct {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->applyAddParticipantCapabilities(I)I

    move-result v0

    .line 1070
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionCapabilities()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 1071
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->setConnectionCapabilities(I)V

    .line 1073
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 1064
    goto :goto_0
.end method

.method protected final updateConnectionProperties()V
    .locals 3

    .prologue
    .line 1092
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->buildConnectionProperties()I

    move-result v0

    .line 1095
    .local v0, "newProperties":I
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->hasHighDefAudioProperty()Z

    move-result v1

    .line 1094
    const/4 v2, 0x4

    invoke-direct {p0, v0, v2, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result v0

    .line 1096
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsWifi:Z

    const/16 v2, 0x8

    invoke-direct {p0, v0, v2, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result v0

    .line 1098
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->isExternalConnection()Z

    move-result v1

    .line 1097
    const/16 v2, 0x10

    invoke-direct {p0, v0, v2, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result v0

    .line 1100
    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsCdmaVoicePrivacyEnabled:Z

    .line 1099
    const/16 v2, 0x20

    invoke-direct {p0, v0, v2, v1}, Lcom/android/services/telephony/TelephonyConnection;->changeBitmask(IIZ)I

    move-result v0

    .line 1102
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionProperties()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 1103
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConnection;->setConnectionProperties(I)V

    .line 1105
    :cond_0
    return-void
.end method

.method protected updateExtras(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 1601
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v4, :cond_4

    .line 1602
    if-eqz p1, :cond_6

    .line 1604
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    invoke-static {v4, p1}, Lcom/android/services/telephony/TelephonyConnection;->areBundlesEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1605
    sget-boolean v4, Lcom/android/services/telephony/Log;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 1606
    const-string/jumbo v4, "Updating extras:"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1607
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "key$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1608
    .local v0, "key":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 1609
    .local v3, "value":Ljava/lang/Object;
    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1610
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateExtras Key="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1611
    const-string/jumbo v5, " value="

    .line 1610
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1611
    check-cast v3, Ljava/lang/String;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-static {v3}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1610
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1615
    .end local v0    # "key":Ljava/lang/String;
    .end local v1    # "key$iterator":Ljava/util/Iterator;
    :cond_1
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/os/Bundle;->clear()V

    .line 1617
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    invoke-virtual {v4, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1620
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "key$iterator":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1621
    .restart local v0    # "key":Ljava/lang/String;
    sget-object v4, Lcom/android/services/telephony/TelephonyConnection;->sExtrasMap:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1622
    sget-object v4, Lcom/android/services/telephony/TelephonyConnection;->sExtrasMap:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1623
    .local v2, "newKey":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1624
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_1

    .line 1629
    .end local v0    # "key":Ljava/lang/String;
    .end local v2    # "newKey":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/TelephonyConnection;->putExtras(Landroid/os/Bundle;)V

    .line 1632
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionExtras:Landroid/os/Bundle;

    .line 1633
    const-string/jumbo v5, "ConfSupportInd"

    .line 1632
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1634
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionCapabilities()V

    .line 1643
    .end local v1    # "key$iterator":Ljava/util/Iterator;
    :cond_4
    :goto_2
    return-void

    .line 1637
    :cond_5
    const-string/jumbo v4, "Extras update not required"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 1640
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateExtras extras: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p1}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method updateState()V
    .locals 1

    .prologue
    .line 1777
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-nez v0, :cond_0

    .line 1778
    return-void

    .line 1781
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateStateInternal()V

    .line 1782
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateStatusHints()V

    .line 1783
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionCapabilities()V

    .line 1784
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionProperties()V

    .line 1785
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateAddress()V

    .line 1786
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->updateMultiparty()V

    .line 1787
    return-void
.end method

.method updateStateInternal()V
    .locals 10

    .prologue
    const/16 v9, 0x6a

    const/4 v8, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1680
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-nez v5, :cond_0

    .line 1681
    return-void

    .line 1686
    :cond_0
    iget-boolean v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mIsStateOverridden:Z

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnectionState:Lcom/android/internal/telephony/Call$State;

    iget-object v6, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v6}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v6

    if-ne v5, v6, :cond_3

    .line 1687
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionOverriddenState:Lcom/android/internal/telephony/Call$State;

    .line 1691
    .local v2, "newState":Lcom/android/internal/telephony/Call$State;
    :goto_0
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Connection;->getDisconnectCause()I

    move-result v0

    .line 1693
    .local v0, "cause":I
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 1692
    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v1

    .line 1694
    .local v1, "isEmergencyNumber":Z
    const-string/jumbo v5, "Update state from %s to %s for %s"

    new-array v6, v8, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    aput-object v7, v6, v4

    aput-object v2, v6, v3

    const/4 v7, 0x2

    aput-object p0, v6, v7

    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1696
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    if-eq v5, v2, :cond_2

    .line 1699
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    sget-object v6, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    if-ne v5, v6, :cond_1

    sget-object v5, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    if-ne v2, v5, :cond_1

    .line 1700
    if-ne v0, v8, :cond_1

    .line 1701
    sget-object v2, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    .line 1704
    :cond_1
    iput-object v2, p0, Lcom/android/services/telephony/TelephonyConnection;->mConnectionState:Lcom/android/internal/telephony/Call$State;

    .line 1705
    invoke-static {}, Lcom/android/services/telephony/TelephonyConnection;->-getcom-android-internal-telephony-Call$StateSwitchesValues()[I

    move-result-object v5

    invoke-virtual {v2}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1774
    :cond_2
    :goto_1
    :pswitch_0
    return-void

    .line 1689
    .end local v0    # "cause":I
    .end local v1    # "isEmergencyNumber":Z
    .end local v2    # "newState":Lcom/android/internal/telephony/Call$State;
    :cond_3
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    .restart local v2    # "newState":Lcom/android/internal/telephony/Call$State;
    goto :goto_0

    .line 1709
    .restart local v0    # "cause":I
    .restart local v1    # "isEmergencyNumber":Z
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->setActiveInternal()V

    goto :goto_1

    .line 1712
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->setOnHold()V

    goto :goto_1

    .line 1716
    :pswitch_3
    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Connection;->isPulledCall()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1717
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->setPulling()V

    goto :goto_1

    .line 1719
    :cond_4
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->setDialing()V

    goto :goto_1

    .line 1726
    :pswitch_4
    iget-boolean v3, p0, Lcom/android/services/telephony/TelephonyConnection;->isLiveTalkConn:Z

    if-nez v3, :cond_2

    .line 1727
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->setRinging()V

    goto :goto_1

    .line 1732
    :pswitch_5
    sget-object v5, Lcom/android/services/telephony/TelephonyConnection;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 1733
    if-eqz v1, :cond_7

    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v6

    if-le v6, v3, :cond_7

    .line 1735
    const/16 v6, 0x69

    .line 1734
    if-eq v0, v6, :cond_5

    .line 1736
    if-ne v0, v9, :cond_7

    .line 1743
    :cond_5
    if-ne v0, v9, :cond_6

    :goto_2
    invoke-direct {p0, v3}, Lcom/android/services/telephony/TelephonyConnection;->fireOnOriginalConnectionRetryDial(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_3
    monitor-exit v5

    .line 1767
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->onDisconnectLivetalkCallback()V

    goto :goto_1

    :cond_6
    move v3, v4

    .line 1743
    goto :goto_2

    .line 1746
    :cond_7
    :try_start_1
    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    if-eqz v3, :cond_8

    .line 1748
    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Connection;->getDisconnectCause()I

    move-result v3

    .line 1749
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getVendorDisconnectCause()Ljava/lang/String;

    move-result-object v4

    .line 1750
    iget-object v6, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    iget v6, v6, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->notificationType:I

    .line 1751
    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    iget v7, v7, Lcom/android/internal/telephony/gsm/SuppServiceNotification;->code:I

    .line 1752
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v8

    .line 1747
    invoke-static {v3, v4, v6, v7, v8}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;III)Landroid/telecom/DisconnectCause;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 1753
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mSsNotification:Lcom/android/internal/telephony/gsm/SuppServiceNotification;

    .line 1754
    const/16 v3, 0xff

    sput v3, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationCode:I

    .line 1755
    const/16 v3, 0xff

    sput v3, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationType:I

    .line 1762
    :goto_4
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection;->fireResetDisconnectCause()V

    .line 1763
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 1732
    :catchall_0
    move-exception v3

    monitor-exit v5

    throw v3

    .line 1758
    :cond_8
    :try_start_2
    iget-object v3, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Connection;->getDisconnectCause()I

    move-result v3

    .line 1759
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getVendorDisconnectCause()Ljava/lang/String;

    move-result-object v4

    .line 1760
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v6

    .line 1757
    invoke-static {v3, v4, v6}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 1705
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public wasImsConnection()Z
    .locals 1

    .prologue
    .line 2127
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConnection;->mWasImsConnection:Z

    return v0
.end method
