.class public Lcom/android/services/telephony/ImsConferenceController;
.super Ljava/lang/Object;
.source "ImsConferenceController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/ImsConferenceController$1;,
        Lcom/android/services/telephony/ImsConferenceController$2;
    }
.end annotation


# instance fields
.field private final mConferenceListener:Landroid/telecom/Conference$Listener;

.field private final mConnectionListener:Landroid/telecom/Connection$Listener;

.field private final mConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

.field private final mImsConferences:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/services/telephony/ImsConference;",
            ">;"
        }
    .end annotation
.end field

.field private mTelecomAccountRegistry:Lcom/android/services/telephony/TelecomAccountRegistry;

.field private final mTelephonyConnections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/services/telephony/TelephonyConnection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0(Lcom/android/services/telephony/ImsConferenceController;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConferenceController;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mImsConferences:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/services/telephony/ImsConferenceController;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConferenceController;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConferenceController;->recalculate()V

    return-void
.end method

.method public constructor <init>(Lcom/android/services/telephony/TelecomAccountRegistry;Lcom/android/services/telephony/TelephonyConnectionServiceProxy;)V
    .locals 2
    .param p1, "telecomAccountRegistry"    # Lcom/android/services/telephony/TelecomAccountRegistry;
    .param p2, "connectionService"    # Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lcom/android/services/telephony/ImsConferenceController$1;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/ImsConferenceController$1;-><init>(Lcom/android/services/telephony/ImsConferenceController;)V

    iput-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mConferenceListener:Landroid/telecom/Conference$Listener;

    .line 67
    new-instance v0, Lcom/android/services/telephony/ImsConferenceController$2;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/ImsConferenceController$2;-><init>(Lcom/android/services/telephony/ImsConferenceController;)V

    iput-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelephonyConnections:Ljava/util/ArrayList;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mImsConferences:Ljava/util/ArrayList;

    .line 123
    iput-object p2, p0, Lcom/android/services/telephony/ImsConferenceController;->mConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    .line 124
    iput-object p1, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelecomAccountRegistry:Lcom/android/services/telephony/TelecomAccountRegistry;

    .line 125
    return-void
.end method

.method private isMemberOfPeerConference(Landroid/telecom/Connection;)Z
    .locals 4
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    const/4 v2, 0x0

    .line 313
    instance-of v3, p1, Lcom/android/services/telephony/TelephonyConnection;

    if-nez v3, :cond_0

    .line 314
    return v2

    :cond_0
    move-object v1, p1

    .line 316
    check-cast v1, Lcom/android/services/telephony/TelephonyConnection;

    .line 318
    .local v1, "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    .line 320
    .local v0, "originalConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->isMultiparty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 321
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->isMemberOfPeerConference()Z

    move-result v2

    .line 320
    :cond_1
    return v2
.end method

.method static synthetic lambda$-com_android_services_telephony_ImsConferenceController_10011(Landroid/telecom/Conferenceable;Landroid/telecom/Conferenceable;)Z
    .locals 1
    .param p0, "c"    # Landroid/telecom/Conferenceable;
    .param p1, "conferenceable"    # Landroid/telecom/Conferenceable;

    .prologue
    .line 272
    if-eq p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$-com_android_services_telephony_ImsConferenceController_11605(Landroid/telecom/Conferenceable;)Z
    .locals 1
    .param p0, "conferenceable"    # Landroid/telecom/Conferenceable;

    .prologue
    .line 297
    instance-of v0, p0, Landroid/telecom/Connection;

    return v0
.end method

.method static synthetic lambda$-com_android_services_telephony_ImsConferenceController_11690(Landroid/telecom/Conferenceable;)Landroid/telecom/Connection;
    .locals 0
    .param p0, "conferenceable"    # Landroid/telecom/Conferenceable;

    .prologue
    .line 298
    check-cast p0, Landroid/telecom/Connection;

    .end local p0    # "conferenceable":Landroid/telecom/Conferenceable;
    return-object p0
.end method

.method private recalculate()V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConferenceController;->recalculateConferenceable()V

    .line 191
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConferenceController;->recalculateConference()V

    .line 192
    return-void
.end method

.method private recalculateConference()V
    .locals 4

    .prologue
    .line 328
    const-string/jumbo v2, "recalculateConference"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 330
    iget-object v2, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelephonyConnections:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 331
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/services/telephony/TelephonyConnection;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 332
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    .line 333
    .local v0, "connection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->isImsConnection()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 334
    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->isMultiparty()Z

    move-result v2

    .line 333
    if-eqz v2, :cond_0

    .line 341
    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 342
    invoke-direct {p0, v0}, Lcom/android/services/telephony/ImsConferenceController;->startConference(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 343
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 348
    .end local v0    # "connection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_1
    return-void
.end method

.method private recalculateConferenceable()V
    .locals 15

    .prologue
    .line 198
    const-string/jumbo v11, "recalculateConferenceable : %d"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    iget-object v13, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelephonyConnections:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x0

    aput-object v13, v12, v14

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 199
    new-instance v5, Ljava/util/HashSet;

    iget-object v11, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelephonyConnections:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 200
    iget-object v12, p0, Lcom/android/services/telephony/ImsConferenceController;->mImsConferences:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 199
    add-int/2addr v11, v12

    invoke-direct {v5, v11}, Ljava/util/HashSet;-><init>(I)V

    .line 201
    .local v5, "conferenceableSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/telecom/Conferenceable;>;"
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 204
    .local v4, "conferenceParticipantsSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/telecom/Conferenceable;>;"
    iget-object v11, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelephonyConnections:Ljava/util/ArrayList;

    invoke-interface {v11}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "connection$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/services/telephony/TelephonyConnection;

    .line 205
    .local v7, "connection":Lcom/android/services/telephony/TelephonyConnection;
    sget-boolean v11, Lcom/android/services/telephony/Log;->DEBUG:Z

    if-eqz v11, :cond_1

    .line 206
    const-string/jumbo v11, "recalc - %s %s supportsConf? %s"

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x0

    aput-object v13, v12, v14

    const/4 v13, 0x1

    aput-object v7, v12, v13

    .line 207
    invoke-virtual {v7}, Lcom/android/services/telephony/TelephonyConnection;->isConferenceSupported()Z

    move-result v13

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x2

    aput-object v13, v12, v14

    .line 206
    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    :cond_1
    invoke-direct {p0, v7}, Lcom/android/services/telephony/ImsConferenceController;->isMemberOfPeerConference(Landroid/telecom/Connection;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 213
    sget-boolean v11, Lcom/android/services/telephony/Log;->VERBOSE:Z

    if-eqz v11, :cond_0

    .line 214
    const-string/jumbo v11, "Skipping connection in peer conference: %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v7, v12, v13

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 221
    :cond_2
    invoke-virtual {v7}, Lcom/android/services/telephony/TelephonyConnection;->isConferenceSupported()Z

    move-result v11

    if-nez v11, :cond_3

    .line 222
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/android/services/telephony/TelephonyConnection;->setConferenceables(Ljava/util/List;)V

    goto :goto_0

    .line 226
    :cond_3
    invoke-virtual {v7}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v11

    packed-switch v11, :pswitch_data_0

    .line 236
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/android/services/telephony/TelephonyConnection;->setConferenceables(Ljava/util/List;)V

    goto :goto_0

    .line 230
    :pswitch_0
    invoke-virtual {v5, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 239
    .end local v7    # "connection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_4
    iget-object v11, p0, Lcom/android/services/telephony/ImsConferenceController;->mImsConferences:Ljava/util/ArrayList;

    invoke-interface {v11}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "conference$iterator":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/services/telephony/ImsConference;

    .line 240
    .local v2, "conference":Lcom/android/services/telephony/ImsConference;
    sget-boolean v11, Lcom/android/services/telephony/Log;->DEBUG:Z

    if-eqz v11, :cond_6

    .line 241
    const-string/jumbo v11, "recalc - %s %s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/android/services/telephony/ImsConference;->getState()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x0

    aput-object v13, v12, v14

    const/4 v13, 0x1

    aput-object v2, v12, v13

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    :cond_6
    invoke-virtual {v2}, Lcom/android/services/telephony/ImsConference;->isConferenceHost()Z

    move-result v11

    if-nez v11, :cond_7

    .line 245
    sget-boolean v11, Lcom/android/services/telephony/Log;->VERBOSE:Z

    if-eqz v11, :cond_5

    .line 246
    const-string/jumbo v11, "skipping conference (not hosted on this device): %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 251
    :cond_7
    invoke-virtual {v2}, Lcom/android/services/telephony/ImsConference;->getState()I

    move-result v11

    packed-switch v11, :pswitch_data_1

    goto :goto_1

    .line 255
    :pswitch_1
    invoke-virtual {v2}, Lcom/android/services/telephony/ImsConference;->isFullConference()Z

    move-result v11

    if-nez v11, :cond_5

    .line 256
    invoke-virtual {v2}, Lcom/android/services/telephony/ImsConference;->getConnections()Ljava/util/List;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 257
    invoke-virtual {v5, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 265
    .end local v2    # "conference":Lcom/android/services/telephony/ImsConference;
    :cond_8
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "conferenceableSet size: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {p0, v11, v12}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "c$iterator":Ljava/util/Iterator;
    :cond_9
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Conferenceable;

    .line 268
    .local v0, "c":Landroid/telecom/Conferenceable;
    instance-of v11, v0, Landroid/telecom/Connection;

    if-eqz v11, :cond_a

    .line 270
    invoke-virtual {v5}, Ljava/util/HashSet;->stream()Ljava/util/stream/Stream;

    move-result-object v11

    .line 272
    new-instance v12, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;

    const/4 v13, 0x0

    invoke-direct {v12, v13, v0}, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;-><init>(BLjava/lang/Object;)V

    .line 270
    invoke-interface {v11, v12}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v11

    .line 273
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v12

    .line 270
    invoke-interface {v11, v12}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 282
    .local v6, "conferenceables":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Conferenceable;>;"
    invoke-interface {v6, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 284
    check-cast v0, Landroid/telecom/Connection;

    .end local v0    # "c":Landroid/telecom/Conferenceable;
    invoke-virtual {v0, v6}, Landroid/telecom/Connection;->setConferenceables(Ljava/util/List;)V

    goto :goto_2

    .line 285
    .end local v6    # "conferenceables":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Conferenceable;>;"
    .restart local v0    # "c":Landroid/telecom/Conferenceable;
    :cond_a
    instance-of v11, v0, Lcom/android/services/telephony/ImsConference;

    if-eqz v11, :cond_9

    move-object v10, v0

    .line 286
    check-cast v10, Lcom/android/services/telephony/ImsConference;

    .line 289
    .local v10, "imsConference":Lcom/android/services/telephony/ImsConference;
    invoke-virtual {v10}, Lcom/android/services/telephony/ImsConference;->isFullConference()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 290
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/android/services/telephony/ImsConference;->setConferenceableConnections(Ljava/util/List;)V

    .line 295
    :cond_b
    invoke-virtual {v5}, Ljava/util/HashSet;->stream()Ljava/util/stream/Stream;

    move-result-object v11

    sget-object v12, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;->$INST$0:Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;

    invoke-interface {v11, v12}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v11

    sget-object v12, Lcom/android/services/telephony/-$Lambda$L-cyLbKwhIywlBT-Y0545_oueiA;->$INST$0:Lcom/android/services/telephony/-$Lambda$L-cyLbKwhIywlBT-Y0545_oueiA;

    invoke-interface {v11, v12}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v11

    .line 299
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v12

    .line 295
    invoke-interface {v11, v12}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 301
    .local v9, "connections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    invoke-virtual {v10, v9}, Lcom/android/services/telephony/ImsConference;->setConferenceableConnections(Ljava/util/List;)V

    goto :goto_2

    .line 304
    .end local v0    # "c":Landroid/telecom/Conferenceable;
    .end local v9    # "connections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    .end local v10    # "imsConference":Lcom/android/services/telephony/ImsConference;
    :cond_c
    return-void

    .line 226
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 251
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private startConference(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 9
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    const/16 v8, 0x2d

    const/4 v7, 0x4

    .line 365
    sget-boolean v4, Lcom/android/services/telephony/Log;->VERBOSE:Z

    if-eqz v4, :cond_0

    .line 366
    const-string/jumbo v4, "Start new ImsConference - connection: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 372
    :cond_0
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->cloneConnection()Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v1

    .line 373
    .local v1, "conferenceHostConnection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getVideoPauseSupported()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/android/services/telephony/TelephonyConnection;->setVideoPauseSupported(Z)V

    .line 375
    const/4 v3, 0x0

    .line 378
    .local v3, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 379
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    .line 380
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 384
    .local v2, "imsPhone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-static {v4}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    .line 387
    .end local v2    # "imsPhone":Lcom/android/internal/telephony/Phone;
    .end local v3    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :cond_1
    new-instance v0, Lcom/android/services/telephony/ImsConference;

    iget-object v4, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelecomAccountRegistry:Lcom/android/services/telephony/TelecomAccountRegistry;

    iget-object v5, p0, Lcom/android/services/telephony/ImsConferenceController;->mConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    invoke-direct {v0, v4, v5, v1, v3}, Lcom/android/services/telephony/ImsConference;-><init>(Lcom/android/services/telephony/TelecomAccountRegistry;Lcom/android/services/telephony/TelephonyConnectionServiceProxy;Lcom/android/services/telephony/TelephonyConnection;Landroid/telecom/PhoneAccountHandle;)V

    .line 389
    .local v0, "conference":Lcom/android/services/telephony/ImsConference;
    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/android/services/telephony/ImsConference;->setState(I)V

    .line 390
    iget-object v4, p0, Lcom/android/services/telephony/ImsConferenceController;->mConferenceListener:Landroid/telecom/Conference$Listener;

    invoke-virtual {v0, v4}, Lcom/android/services/telephony/ImsConference;->addListener(Landroid/telecom/Conference$Listener;)Landroid/telecom/Conference;

    .line 391
    invoke-virtual {v0}, Lcom/android/services/telephony/ImsConference;->updateConferenceParticipantsAfterCreation()V

    .line 393
    invoke-static {v0, p1}, Lcom/android/services/telephony/SimpleFeatures;->putImsConferenceExtra(Lcom/android/services/telephony/ImsConference;Lcom/android/services/telephony/TelephonyConnection;)V

    .line 394
    iget-object v4, p0, Lcom/android/services/telephony/ImsConferenceController;->mConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    invoke-interface {v4, v0}, Lcom/android/services/telephony/TelephonyConnectionServiceProxy;->addConference(Lcom/android/services/telephony/ImsConference;)V

    .line 395
    invoke-virtual {v0}, Lcom/android/services/telephony/ImsConference;->getTelecomCallId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/services/telephony/TelephonyConnection;->setTelecomCallId(Ljava/lang/String;)V

    .line 400
    iget-object v4, p0, Lcom/android/services/telephony/ImsConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {p1, v4}, Lcom/android/services/telephony/TelephonyConnection;->removeConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 401
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->clearOriginalConnection()V

    .line 407
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 409
    new-instance v4, Landroid/telecom/DisconnectCause;

    invoke-direct {v4, v7}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p1, v4}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 410
    new-instance v4, Landroid/telecom/DisconnectCause;

    .line 411
    invoke-static {v8}, Landroid/telephony/DisconnectCause;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 410
    invoke-direct {v4, v7, v5}, Landroid/telecom/DisconnectCause;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v4}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 419
    :goto_0
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->destroy()V

    .line 420
    iget-object v4, p0, Lcom/android/services/telephony/ImsConferenceController;->mImsConferences:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConferenceController;->recalculate()V

    .line 424
    return-void

    .line 414
    :cond_2
    new-instance v4, Landroid/telecom/DisconnectCause;

    .line 415
    invoke-static {v8}, Landroid/telephony/DisconnectCause;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 414
    const/16 v6, 0x9

    invoke-direct {v4, v6, v5}, Landroid/telecom/DisconnectCause;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v4}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_0
.end method


# virtual methods
.method add(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 4
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 135
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionProperties()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    .line 136
    const/16 v1, 0x10

    .line 135
    if-ne v0, v1, :cond_0

    .line 137
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelephonyConnections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    const-string/jumbo v0, "add - connection already tracked; connection=%s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    return-void

    .line 148
    :cond_1
    sget-boolean v0, Lcom/android/services/telephony/Log;->VERBOSE:Z

    if-eqz v0, :cond_2

    .line 149
    const-string/jumbo v0, "add connection %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    :cond_2
    iget-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelephonyConnections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {p1, v0}, Lcom/android/services/telephony/TelephonyConnection;->addConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 154
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConferenceController;->recalculateConference()V

    .line 155
    return-void
.end method

.method remove(Landroid/telecom/Connection;)V
    .locals 4
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 164
    invoke-virtual {p1}, Landroid/telecom/Connection;->getConnectionProperties()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    .line 165
    const/16 v1, 0x10

    .line 164
    if-ne v0, v1, :cond_0

    .line 166
    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelephonyConnections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 172
    const-string/jumbo v0, "remove - connection not tracked; connection=%s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    return-void

    .line 176
    :cond_1
    sget-boolean v0, Lcom/android/services/telephony/Log;->VERBOSE:Z

    if-eqz v0, :cond_2

    .line 177
    const-string/jumbo v0, "remove connection: %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {p1, v0}, Landroid/telecom/Connection;->removeConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 181
    iget-object v0, p0, Lcom/android/services/telephony/ImsConferenceController;->mTelephonyConnections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 182
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConferenceController;->recalculateConferenceable()V

    .line 183
    return-void
.end method
