.class Lcom/android/services/telephony/TelephonyConnectionService$3;
.super Ljava/lang/Object;
.source "TelephonyConnectionService.java"

# interfaces
.implements Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelephonyConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final sTelephonyManager:Landroid/telephony/TelephonyManager;

.field final synthetic this$0:Lcom/android/services/telephony/TelephonyConnectionService;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/TelephonyConnectionService;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnectionService$3;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$3;->sTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 1
    return-void
.end method


# virtual methods
.method public getPhoneCount()I
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$3;->sTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v0

    return v0
.end method

.method public hasIccCard(I)Z
    .locals 1
    .param p1, "slotId"    # I

    .prologue
    .line 199
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$3;->sTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->hasIccCard(I)Z

    move-result v0

    return v0
.end method
