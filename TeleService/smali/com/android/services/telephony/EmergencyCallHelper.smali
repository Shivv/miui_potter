.class public Lcom/android/services/telephony/EmergencyCallHelper;
.super Ljava/lang/Object;
.source "EmergencyCallHelper.java"

# interfaces
.implements Lcom/android/services/telephony/EmergencyCallStateListener$Callback;


# instance fields
.field private mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

.field private final mContext:Landroid/content/Context;

.field private mInProgressListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/services/telephony/EmergencyCallStateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mIsEmergencyCallingEnabled:Z

.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/services/telephony/EmergencyCallStateListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mContext:Landroid/content/Context;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mInProgressListeners:Ljava/util/List;

    .line 50
    return-void
.end method

.method private handleOnComplete(Lcom/android/services/telephony/EmergencyCallStateListener;)Z
    .locals 6
    .param p1, "listener"    # Lcom/android/services/telephony/EmergencyCallStateListener;

    .prologue
    const/4 v5, 0x0

    .line 143
    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mInProgressListeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 144
    return v5

    .line 145
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "handleOnComplete: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mIsEmergencyCallingEnabled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mInProgressListeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 151
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 152
    invoke-static {v1}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 153
    .local v2, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v2, :cond_2

    .line 151
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 155
    :cond_2
    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mListeners:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/EmergencyCallStateListener;

    .line 156
    .local v0, "cachedListener":Lcom/android/services/telephony/EmergencyCallStateListener;
    if-eq v0, p1, :cond_1

    .line 157
    invoke-virtual {v0}, Lcom/android/services/telephony/EmergencyCallStateListener;->cleanup()V

    goto :goto_1

    .line 160
    .end local v0    # "cachedListener":Lcom/android/services/telephony/EmergencyCallStateListener;
    .end local v2    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_3
    const/4 v3, 0x1

    return v3
.end method

.method private powerOnRadio()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 96
    const-string/jumbo v1, "powerOnRadio()."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 101
    const-string/jumbo v2, "airplane_mode_on"

    .line 100
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_0

    .line 102
    const-string/jumbo v1, "==> Turning off airplane mode."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 106
    const-string/jumbo v2, "airplane_mode_on"

    .line 105
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 112
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "state"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 114
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 116
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private setupListeners()V
    .locals 3

    .prologue
    .line 53
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mListeners:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 54
    return-void

    .line 56
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mListeners:Ljava/util/List;

    .line 57
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 58
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mListeners:Ljava/util/List;

    new-instance v2, Lcom/android/services/telephony/EmergencyCallStateListener;

    invoke-direct {v2}, Lcom/android/services/telephony/EmergencyCallStateListener;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_1
    return-void
.end method


# virtual methods
.method public enableEmergencyCalling(Lcom/android/services/telephony/EmergencyCallStateListener$Callback;)V
    .locals 4
    .param p1, "callback"    # Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallHelper;->setupListeners()V

    .line 77
    iput-object p1, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    .line 78
    iget-object v2, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mInProgressListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 79
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mIsEmergencyCallingEnabled:Z

    .line 80
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 81
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 82
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v1, :cond_0

    .line 80
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_0
    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mInProgressListeners:Ljava/util/List;

    iget-object v2, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mListeners:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/services/telephony/EmergencyCallStateListener;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v2, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mListeners:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/services/telephony/EmergencyCallStateListener;

    invoke-virtual {v2, v1, p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->waitForRadioOn(Lcom/android/internal/telephony/Phone;Lcom/android/services/telephony/EmergencyCallStateListener$Callback;)V

    goto :goto_1

    .line 89
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallHelper;->powerOnRadio()V

    .line 90
    return-void
.end method

.method public onComplete(Lcom/android/services/telephony/EmergencyCallStateListener;Z)V
    .locals 3
    .param p1, "listener"    # Lcom/android/services/telephony/EmergencyCallStateListener;
    .param p2, "isRadioReady"    # Z

    .prologue
    const/4 v2, 0x0

    .line 124
    iget-boolean v0, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mIsEmergencyCallingEnabled:Z

    or-int/2addr v0, p2

    iput-boolean v0, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mIsEmergencyCallingEnabled:Z

    .line 128
    invoke-direct {p0, p1}, Lcom/android/services/telephony/EmergencyCallHelper;->handleOnComplete(Lcom/android/services/telephony/EmergencyCallStateListener;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mInProgressListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    iget-boolean v1, p0, Lcom/android/services/telephony/EmergencyCallHelper;->mIsEmergencyCallingEnabled:Z

    invoke-interface {v0, v2, v1}, Lcom/android/services/telephony/EmergencyCallStateListener$Callback;->onComplete(Lcom/android/services/telephony/EmergencyCallStateListener;Z)V

    .line 134
    :cond_1
    return-void
.end method
