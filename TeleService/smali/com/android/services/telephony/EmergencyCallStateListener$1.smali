.class Lcom/android/services/telephony/EmergencyCallStateListener$1;
.super Landroid/os/Handler;
.source "EmergencyCallStateListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/EmergencyCallStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/EmergencyCallStateListener;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/EmergencyCallStateListener;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/EmergencyCallStateListener;
    .param p2, "$anonymous0"    # Landroid/os/Looper;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/EmergencyCallStateListener$1;->this$0:Lcom/android/services/telephony/EmergencyCallStateListener;

    .line 59
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 62
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 81
    const-string/jumbo v3, "handleMessage: unexpected message: %d."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->wtf(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    :goto_0
    return-void

    .line 64
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/internal/os/SomeArgs;

    .line 66
    .local v0, "args":Lcom/android/internal/os/SomeArgs;
    :try_start_0
    iget-object v2, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    check-cast v2, Lcom/android/internal/telephony/Phone;

    .line 68
    .local v2, "phone":Lcom/android/internal/telephony/Phone;
    iget-object v1, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    check-cast v1, Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    .line 69
    .local v1, "callback":Lcom/android/services/telephony/EmergencyCallStateListener$Callback;
    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallStateListener$1;->this$0:Lcom/android/services/telephony/EmergencyCallStateListener;

    invoke-static {v3, v2, v1}, Lcom/android/services/telephony/EmergencyCallStateListener;->-wrap2(Lcom/android/services/telephony/EmergencyCallStateListener;Lcom/android/internal/telephony/Phone;Lcom/android/services/telephony/EmergencyCallStateListener$Callback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    goto :goto_0

    .line 70
    .end local v1    # "callback":Lcom/android/services/telephony/EmergencyCallStateListener$Callback;
    .end local v2    # "phone":Lcom/android/internal/telephony/Phone;
    :catchall_0
    move-exception v3

    .line 71
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    .line 70
    throw v3

    .line 75
    .end local v0    # "args":Lcom/android/internal/os/SomeArgs;
    :pswitch_1
    iget-object v4, p0, Lcom/android/services/telephony/EmergencyCallStateListener$1;->this$0:Lcom/android/services/telephony/EmergencyCallStateListener;

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/AsyncResult;

    iget-object v3, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v3, Landroid/telephony/ServiceState;

    invoke-static {v4, v3}, Lcom/android/services/telephony/EmergencyCallStateListener;->-wrap1(Lcom/android/services/telephony/EmergencyCallStateListener;Landroid/telephony/ServiceState;)V

    goto :goto_0

    .line 78
    :pswitch_2
    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallStateListener$1;->this$0:Lcom/android/services/telephony/EmergencyCallStateListener;

    invoke-static {v3}, Lcom/android/services/telephony/EmergencyCallStateListener;->-wrap0(Lcom/android/services/telephony/EmergencyCallStateListener;)V

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
