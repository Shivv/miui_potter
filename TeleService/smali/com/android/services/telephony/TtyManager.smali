.class final Lcom/android/services/telephony/TtyManager;
.super Ljava/lang/Object;
.source "TtyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/TtyManager$1;,
        Lcom/android/services/telephony/TtyManager$TtyBroadcastReceiver;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mPhone:Lcom/android/internal/telephony/Phone;

.field private final mReceiver:Lcom/android/services/telephony/TtyManager$TtyBroadcastReceiver;

.field private mTtyMode:I

.field private mUiTtyMode:I


# direct methods
.method static synthetic -get0(Lcom/android/services/telephony/TtyManager;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TtyManager;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TtyManager;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/services/telephony/TtyManager;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TtyManager;

    .prologue
    iget v0, p0, Lcom/android/services/telephony/TtyManager;->mTtyMode:I

    return v0
.end method

.method static synthetic -wrap0(I)I
    .locals 1
    .param p0, "phoneMode"    # I

    .prologue
    invoke-static {p0}, Lcom/android/services/telephony/TtyManager;->phoneModeToTelecomMode(I)I

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/TtyManager;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TtyManager;
    .param p1, "ttyMode"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/TtyManager;->updateTtyMode(I)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/services/telephony/TtyManager;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TtyManager;
    .param p1, "ttyMode"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/TtyManager;->updateUiTtyMode(I)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/internal/telephony/Phone;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v4, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v3, Lcom/android/services/telephony/TtyManager$TtyBroadcastReceiver;

    invoke-direct {v3, p0, v4}, Lcom/android/services/telephony/TtyManager$TtyBroadcastReceiver;-><init>(Lcom/android/services/telephony/TtyManager;Lcom/android/services/telephony/TtyManager$TtyBroadcastReceiver;)V

    iput-object v3, p0, Lcom/android/services/telephony/TtyManager;->mReceiver:Lcom/android/services/telephony/TtyManager$TtyBroadcastReceiver;

    .line 38
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/services/telephony/TtyManager;->mUiTtyMode:I

    .line 40
    new-instance v3, Lcom/android/services/telephony/TtyManager$1;

    invoke-direct {v3, p0}, Lcom/android/services/telephony/TtyManager$1;-><init>(Lcom/android/services/telephony/TtyManager;)V

    iput-object v3, p0, Lcom/android/services/telephony/TtyManager;->mHandler:Landroid/os/Handler;

    .line 74
    iput-object p2, p0, Lcom/android/services/telephony/TtyManager;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 76
    new-instance v0, Landroid/content/IntentFilter;

    .line 77
    const-string/jumbo v3, "android.telecom.action.CURRENT_TTY_MODE_CHANGED"

    .line 76
    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v3, "android.telecom.action.TTY_PREFERRED_MODE_CHANGED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    iget-object v3, p0, Lcom/android/services/telephony/TtyManager;->mReceiver:Lcom/android/services/telephony/TtyManager$TtyBroadcastReceiver;

    invoke-virtual {p1, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 81
    const/4 v2, 0x0

    .line 82
    .local v2, "ttyMode":I
    invoke-static {p1}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v1

    .line 83
    .local v1, "telecomManager":Landroid/telecom/TelecomManager;
    if-eqz v1, :cond_0

    .line 84
    invoke-virtual {v1}, Landroid/telecom/TelecomManager;->getCurrentTtyMode()I

    move-result v2

    .line 86
    :cond_0
    invoke-direct {p0, v2}, Lcom/android/services/telephony/TtyManager;->updateTtyMode(I)V

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 89
    const-string/jumbo v4, "preferred_tty_mode"

    const/4 v5, 0x0

    .line 88
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 90
    invoke-direct {p0, v2}, Lcom/android/services/telephony/TtyManager;->updateUiTtyMode(I)V

    .line 91
    return-void
.end method

.method private static phoneModeToTelecomMode(I)I
    .locals 1
    .param p0, "phoneMode"    # I

    .prologue
    .line 141
    packed-switch p0, :pswitch_data_0

    .line 150
    const/4 v0, 0x0

    return v0

    .line 143
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 145
    :pswitch_1
    const/4 v0, 0x3

    return v0

    .line 147
    :pswitch_2
    const/4 v0, 0x2

    return v0

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static telecomModeToPhoneMode(I)I
    .locals 1
    .param p0, "telecomMode"    # I

    .prologue
    .line 128
    packed-switch p0, :pswitch_data_0

    .line 136
    const/4 v0, 0x0

    return v0

    .line 134
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateTtyMode(I)V
    .locals 5
    .param p1, "ttyMode"    # I

    .prologue
    const/4 v4, 0x1

    .line 94
    const-string/jumbo v0, "updateTtyMode %d -> %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/services/telephony/TtyManager;->mTtyMode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    iput p1, p0, Lcom/android/services/telephony/TtyManager;->mTtyMode:I

    .line 96
    iget-object v0, p0, Lcom/android/services/telephony/TtyManager;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-static {p1}, Lcom/android/services/telephony/TtyManager;->telecomModeToPhoneMode(I)I

    move-result v1

    .line 97
    iget-object v2, p0, Lcom/android/services/telephony/TtyManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 96
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->setTTYMode(ILandroid/os/Message;)V

    .line 98
    return-void
.end method

.method private updateUiTtyMode(I)V
    .locals 5
    .param p1, "ttyMode"    # I

    .prologue
    const/4 v4, 0x0

    .line 101
    const-string/jumbo v0, "updateUiTtyMode %d -> %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/services/telephony/TtyManager;->mUiTtyMode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    iget v0, p0, Lcom/android/services/telephony/TtyManager;->mUiTtyMode:I

    if-eq v0, p1, :cond_0

    .line 103
    iput p1, p0, Lcom/android/services/telephony/TtyManager;->mUiTtyMode:I

    .line 104
    iget-object v0, p0, Lcom/android/services/telephony/TtyManager;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-static {p1}, Lcom/android/services/telephony/TtyManager;->telecomModeToPhoneMode(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->setUiTTYMode(ILandroid/os/Message;)V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    const-string/jumbo v0, "ui tty mode didnt change"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
