.class Lcom/android/services/telephony/TelephonyConnectionService$6;
.super Ljava/lang/Object;
.source "TelephonyConnectionService.java"

# interfaces
.implements Lcom/android/services/telephony/EmergencyCallStateListener$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/services/telephony/TelephonyConnectionService;->onCreateOutgoingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/Connection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/TelephonyConnectionService;

.field final synthetic val$defaultPhoneType:I

.field final synthetic val$emergencyConnection:Landroid/telecom/Connection;

.field final synthetic val$emergencyHandle:Landroid/net/Uri;

.field final synthetic val$isEmergencyNumber:Z

.field final synthetic val$numberToDial:Ljava/lang/String;

.field final synthetic val$request:Landroid/telecom/ConnectionRequest;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/TelephonyConnectionService;Landroid/telecom/Connection;Landroid/telecom/ConnectionRequest;ZILjava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    iput-object p2, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyConnection:Landroid/telecom/Connection;

    iput-object p3, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$request:Landroid/telecom/ConnectionRequest;

    iput-boolean p4, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$isEmergencyNumber:Z

    iput p5, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$defaultPhoneType:I

    iput-object p6, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$numberToDial:Ljava/lang/String;

    iput-object p7, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyHandle:Landroid/net/Uri;

    .line 430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onComplete(Lcom/android/services/telephony/EmergencyCallStateListener;Z)V
    .locals 8
    .param p1, "listener"    # Lcom/android/services/telephony/EmergencyCallStateListener;
    .param p2, "isRadioReady"    # Z

    .prologue
    const/4 v3, 0x0

    .line 434
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyConnection:Landroid/telecom/Connection;

    invoke-virtual {v0}, Landroid/telecom/Connection;->getState()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 435
    const-string/jumbo v0, "Emergency call disconnected before the outgoing call was placed. Skipping emergency call placement."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 437
    return-void

    .line 439
    :cond_0
    if-eqz p2, :cond_6

    .line 442
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$request:Landroid/telecom/ConnectionRequest;

    invoke-virtual {v1}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 443
    iget-boolean v2, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$isEmergencyNumber:Z

    .line 442
    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/TelephonyConnectionService;->-wrap2(Lcom/android/services/telephony/TelephonyConnectionService;Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    .line 447
    .local v5, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v0

    iget v1, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$defaultPhoneType:I

    if-eq v0, v1, :cond_4

    .line 448
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$request:Landroid/telecom/ConnectionRequest;

    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$numberToDial:Ljava/lang/String;

    .line 449
    iget-boolean v3, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$isEmergencyNumber:Z

    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyHandle:Landroid/net/Uri;

    .line 448
    invoke-static/range {v0 .. v5}, Lcom/android/services/telephony/TelephonyConnectionService;->-wrap0(Lcom/android/services/telephony/TelephonyConnectionService;Landroid/telecom/ConnectionRequest;Ljava/lang/String;ZLandroid/net/Uri;Lcom/android/internal/telephony/Phone;)Landroid/telecom/Connection;

    move-result-object v7

    .line 452
    .local v7, "repConnection":Landroid/telecom/Connection;
    instance-of v0, v7, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_1

    .line 453
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    move-object v0, v7

    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    .line 454
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$request:Landroid/telecom/ConnectionRequest;

    .line 453
    invoke-static {v1, v0, v5, v2}, Lcom/android/services/telephony/TelephonyConnectionService;->-wrap3(Lcom/android/services/telephony/TelephonyConnectionService;Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;Landroid/telecom/ConnectionRequest;)V

    .line 460
    :cond_1
    invoke-static {v5}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v6

    .line 462
    .local v6, "repAccountHandle":Landroid/telecom/PhoneAccountHandle;
    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManagerEx;->hasIccCard()Z

    move-result v0

    if-nez v0, :cond_2

    instance-of v0, v7, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_2

    .line 463
    const-string/jumbo v0, ""

    const/4 v1, 0x1

    invoke-static {v5, v0, v1}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandleWithPrefix(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Z)Landroid/telecom/PhoneAccountHandle;

    move-result-object v6

    .line 466
    :cond_2
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-static {v0, v6}, Lcom/android/services/telephony/TelephonyConnectionService;->-wrap1(Lcom/android/services/telephony/TelephonyConnectionService;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 467
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$request:Landroid/telecom/ConnectionRequest;

    invoke-virtual {v0}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v6

    .line 469
    :cond_3
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-virtual {v0, v6, v7}, Lcom/android/services/telephony/TelephonyConnectionService;->addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;)V

    .line 471
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyConnection:Landroid/telecom/Connection;

    .line 474
    const-string/jumbo v1, "Reconnecting outgoing Emergency Call."

    .line 475
    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    .line 473
    const/16 v3, 0x2c

    .line 472
    invoke-static {v3, v1, v2}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v1

    .line 471
    invoke-virtual {v0, v1}, Landroid/telecom/Connection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 476
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyConnection:Landroid/telecom/Connection;

    invoke-virtual {v0}, Landroid/telecom/Connection;->destroy()V

    .line 495
    .end local v5    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v6    # "repAccountHandle":Landroid/telecom/PhoneAccountHandle;
    .end local v7    # "repConnection":Landroid/telecom/Connection;
    :goto_0
    return-void

    .line 479
    .restart local v5    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_4
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-static {v0}, Lcom/android/services/telephony/TelephonyConnectionService;->-get0(Lcom/android/services/telephony/TelephonyConnectionService;)Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v0

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    if-eq v0, v1, :cond_5

    .line 480
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyConnection:Landroid/telecom/Connection;

    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    invoke-static {v1, v0, v5}, Lcom/android/services/telephony/TelephonyConnectionService;->-wrap6(Lcom/android/services/telephony/TelephonyConnectionService;Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;)V

    .line 483
    :cond_5
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyConnection:Landroid/telecom/Connection;

    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    .line 484
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$request:Landroid/telecom/ConnectionRequest;

    .line 483
    invoke-static {v1, v0, v5, v2}, Lcom/android/services/telephony/TelephonyConnectionService;->-wrap3(Lcom/android/services/telephony/TelephonyConnectionService;Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;Landroid/telecom/ConnectionRequest;)V

    goto :goto_0

    .line 487
    .end local v5    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_6
    const-string/jumbo v0, "onCreateOutgoingConnection, failed to turn on radio"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 488
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyConnection:Landroid/telecom/Connection;

    .line 491
    const-string/jumbo v1, "Failed to turn on radio."

    .line 490
    const/16 v2, 0x11

    .line 489
    invoke-static {v2, v1, v3}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v1

    .line 488
    invoke-virtual {v0, v1}, Landroid/telecom/Connection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 493
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$6;->val$emergencyConnection:Landroid/telecom/Connection;

    invoke-virtual {v0}, Landroid/telecom/Connection;->destroy()V

    goto :goto_0
.end method
