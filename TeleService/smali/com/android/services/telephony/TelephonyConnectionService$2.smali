.class Lcom/android/services/telephony/TelephonyConnectionService$2;
.super Ljava/lang/Object;
.source "TelephonyConnectionService.java"

# interfaces
.implements Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelephonyConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/TelephonyConnectionService;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/TelephonyConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnectionService$2;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public getDefaultVoicePhoneId()I
    .locals 1

    .prologue
    .line 169
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultVoicePhoneId()I

    move-result v0

    return v0
.end method

.method public getPhoneId(I)I
    .locals 1
    .param p1, "subId"    # I

    .prologue
    .line 179
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v0

    return v0
.end method

.method public getSimStateForSlotIdx(I)I
    .locals 1
    .param p1, "slotId"    # I

    .prologue
    .line 174
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSimStateForSlotIndex(I)I

    move-result v0

    return v0
.end method
