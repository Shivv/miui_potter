.class Lcom/android/services/telephony/ImsConferenceController$1;
.super Landroid/telecom/Conference$Listener;
.source "ImsConferenceController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/ImsConferenceController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/ImsConferenceController;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/ImsConferenceController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/ImsConferenceController;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/ImsConferenceController$1;->this$0:Lcom/android/services/telephony/ImsConferenceController;

    .line 46
    invoke-direct {p0}, Landroid/telecom/Conference$Listener;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onDestroyed(Landroid/telecom/Conference;)V
    .locals 4
    .param p1, "conference"    # Landroid/telecom/Conference;

    .prologue
    .line 49
    sget-boolean v0, Lcom/android/services/telephony/Log;->VERBOSE:Z

    if-eqz v0, :cond_0

    .line 50
    const-class v0, Lcom/android/services/telephony/ImsConferenceController;

    const-string/jumbo v1, "onDestroyed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/ImsConferenceController$1;->this$0:Lcom/android/services/telephony/ImsConferenceController;

    invoke-static {v0}, Lcom/android/services/telephony/ImsConferenceController;->-get0(Lcom/android/services/telephony/ImsConferenceController;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method public onStateChanged(Landroid/telecom/Conference;II)V
    .locals 2
    .param p1, "conference"    # Landroid/telecom/Conference;
    .param p2, "oldState"    # I
    .param p3, "newState"    # I

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onStateChanged: Conference = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    iget-object v0, p0, Lcom/android/services/telephony/ImsConferenceController$1;->this$0:Lcom/android/services/telephony/ImsConferenceController;

    invoke-static {v0}, Lcom/android/services/telephony/ImsConferenceController;->-wrap0(Lcom/android/services/telephony/ImsConferenceController;)V

    .line 60
    return-void
.end method
