.class final Lcom/android/services/telephony/GsmConnection;
.super Lcom/android/services/telephony/TelephonyConnection;
.source "GsmConnection.java"


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/Connection;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "connection"    # Lcom/android/internal/telephony/Connection;
    .param p2, "telecomCallId"    # Ljava/lang/String;
    .param p3, "isOutgoing"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/android/services/telephony/TelephonyConnection;-><init>(Lcom/android/internal/telephony/Connection;Ljava/lang/String;Z)V

    .line 27
    return-void
.end method


# virtual methods
.method protected buildConnectionCapabilities()I
    .locals 3

    .prologue
    .line 72
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->buildConnectionCapabilities()I

    move-result v0

    .line 73
    .local v0, "capabilities":I
    or-int/lit8 v0, v0, 0x40

    .line 77
    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->shouldTreatAsEmergencyCall()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->isImsConnection()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 78
    or-int/lit8 v0, v0, 0x2

    .line 79
    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getState()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 80
    :cond_0
    or-int/lit8 v0, v0, 0x1

    .line 86
    :cond_1
    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getConnectionProperties()I

    move-result v1

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_2

    .line 87
    const/high16 v1, 0x200000

    or-int/2addr v0, v1

    .line 90
    :cond_2
    return v0
.end method

.method protected buildConnectionProperties()I
    .locals 2

    .prologue
    .line 61
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->buildConnectionProperties()I

    move-result v0

    .line 64
    .local v0, "properties":I
    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getConnectionProperties()I

    move-result v1

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_0

    .line 65
    or-int/lit8 v0, v0, 0x40

    .line 67
    :cond_0
    return v0
.end method

.method public cloneConnection()Lcom/android/services/telephony/TelephonyConnection;
    .locals 4

    .prologue
    .line 38
    new-instance v0, Lcom/android/services/telephony/GsmConnection;

    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    .line 39
    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getTelecomCallId()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/services/telephony/GsmConnection;->mIsOutgoing:Z

    .line 38
    invoke-direct {v0, v1, v2, v3}, Lcom/android/services/telephony/GsmConnection;-><init>(Lcom/android/internal/telephony/Connection;Ljava/lang/String;Z)V

    .line 40
    .local v0, "gsmConnection":Lcom/android/services/telephony/GsmConnection;
    return-object v0
.end method

.method public onPlayDtmfTone(C)V
    .locals 1
    .param p1, "digit"    # C

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/Phone;->startDtmf(C)V

    .line 49
    :cond_0
    return-void
.end method

.method public onStopDtmfTone()V
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/android/services/telephony/GsmConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->stopDtmf()V

    .line 57
    :cond_0
    return-void
.end method
