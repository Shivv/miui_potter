.class public abstract Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;
.super Ljava/lang/Object;
.source "TelephonyConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelephonyConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TelephonyConnectionListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallDisconnectResetDisconnectCause()V
    .locals 0

    .prologue
    .line 529
    return-void
.end method

.method public onOriginalConnectionConfigured(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 0
    .param p1, "c"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 527
    return-void
.end method

.method public onOriginalConnectionRetry(Lcom/android/services/telephony/TelephonyConnection;Z)V
    .locals 0
    .param p1, "c"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "isPermanentFailure"    # Z

    .prologue
    .line 528
    return-void
.end method
