.class Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;
.super Ljava/lang/Object;
.source "SipEditor.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/sip/SipEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AdvancedSettings"
.end annotation


# instance fields
.field private mAdvancedSettingsTrigger:Landroid/preference/Preference;

.field private mPreferences:[Landroid/preference/Preference;

.field private mShowing:Z

.field final synthetic this$0:Lcom/android/services/telephony/sip/SipEditor;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/sip/SipEditor;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/services/telephony/sip/SipEditor;

    .prologue
    .line 568
    iput-object p1, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/SipEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mShowing:Z

    .line 569
    invoke-virtual {p1}, Lcom/android/services/telephony/sip/SipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 570
    const v1, 0x7f0b01e1

    invoke-virtual {p1, v1}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 569
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mAdvancedSettingsTrigger:Landroid/preference/Preference;

    .line 571
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mAdvancedSettingsTrigger:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 573
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->loadAdvancedPreferences()V

    .line 574
    return-void
.end method

.method private hide()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 608
    iput-boolean v2, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mShowing:Z

    .line 609
    iget-object v3, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mAdvancedSettingsTrigger:Landroid/preference/Preference;

    const v4, 0x7f0b01eb

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(I)V

    .line 610
    iget-object v3, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/SipEditor;

    invoke-virtual {v3}, Lcom/android/services/telephony/sip/SipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 611
    .local v1, "screen":Landroid/preference/PreferenceGroup;
    iget-object v3, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 612
    .local v0, "pref":Landroid/preference/Preference;
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 611
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 614
    .end local v0    # "pref":Landroid/preference/Preference;
    :cond_0
    return-void
.end method

.method private loadAdvancedPreferences()V
    .locals 9

    .prologue
    .line 577
    iget-object v7, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/SipEditor;

    invoke-virtual {v7}, Lcom/android/services/telephony/sip/SipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    .line 579
    .local v6, "screen":Landroid/preference/PreferenceGroup;
    iget-object v7, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/SipEditor;

    const v8, 0x7f060039

    invoke-virtual {v7, v8}, Lcom/android/services/telephony/sip/SipEditor;->addPreferencesFromResource(I)V

    .line 581
    iget-object v7, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/SipEditor;

    const v8, 0x7f0b01ff

    invoke-virtual {v7, v8}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 580
    invoke-virtual {v6, v7}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 582
    .local v0, "group":Landroid/preference/PreferenceGroup;
    invoke-virtual {v6, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 584
    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v7

    new-array v7, v7, [Landroid/preference/Preference;

    iput-object v7, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    .line 585
    invoke-virtual {v6}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v3

    .line 586
    .local v3, "order":I
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v7, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    array-length v2, v7

    .local v2, "n":I
    move v4, v3

    .end local v3    # "order":I
    .local v4, "order":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 587
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v5

    .line 588
    .local v5, "pref":Landroid/preference/Preference;
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "order":I
    .restart local v3    # "order":I
    invoke-virtual {v5, v4}, Landroid/preference/Preference;->setOrder(I)V

    .line 589
    iget-object v7, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/SipEditor;

    invoke-static {v7, v5}, Lcom/android/services/telephony/sip/SipEditor;->-wrap5(Lcom/android/services/telephony/sip/SipEditor;Landroid/preference/Preference;)V

    .line 590
    iget-object v7, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    aput-object v5, v7, v1

    .line 586
    add-int/lit8 v1, v1, 0x1

    move v4, v3

    .end local v3    # "order":I
    .restart local v4    # "order":I
    goto :goto_0

    .line 592
    .end local v5    # "pref":Landroid/preference/Preference;
    :cond_0
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 618
    iget-boolean v0, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mShowing:Z

    if-nez v0, :cond_0

    .line 619
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->show()V

    .line 623
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 621
    :cond_0
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->hide()V

    goto :goto_0
.end method

.method show()V
    .locals 5

    .prologue
    .line 595
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mShowing:Z

    .line 596
    iget-object v2, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mAdvancedSettingsTrigger:Landroid/preference/Preference;

    const v3, 0x7f0b01ec

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    .line 597
    iget-object v2, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/SipEditor;

    invoke-virtual {v2}, Lcom/android/services/telephony/sip/SipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 598
    .local v1, "screen":Landroid/preference/PreferenceGroup;
    iget-object v3, p0, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    const/4 v2, 0x0

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 599
    .local v0, "pref":Landroid/preference/Preference;
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 598
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 605
    .end local v0    # "pref":Landroid/preference/Preference;
    :cond_0
    return-void
.end method
