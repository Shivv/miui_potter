.class public Lcom/android/services/telephony/sip/SipIncomingCallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SipIncomingCallReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private isRunningInSystemUser()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 85
    const-string/jumbo v0, "SIP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[SipIncomingCallReceiver] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return-void
.end method

.method private takeCall(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 62
    .local v0, "accountHandle":Landroid/telecom/PhoneAccountHandle;
    :try_start_0
    const-string/jumbo v5, "com.android.services.telephony.sip.phone_account"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .end local v0    # "accountHandle":Landroid/telecom/PhoneAccountHandle;
    check-cast v0, Landroid/telecom/PhoneAccountHandle;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    .local v0, "accountHandle":Landroid/telecom/PhoneAccountHandle;
    if-eqz v0, :cond_0

    .line 68
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 69
    .local v2, "extras":Landroid/os/Bundle;
    const-string/jumbo v5, "com.android.services.telephony.sip.incoming_call_intent"

    invoke-virtual {v2, v5, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 70
    invoke-static {p1}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v4

    .line 71
    .local v4, "tm":Landroid/telecom/TelecomManager;
    invoke-virtual {v4, v0}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v3

    .line 72
    .local v3, "phoneAccount":Landroid/telecom/PhoneAccount;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 73
    invoke-virtual {v4, v0, v2}, Landroid/telecom/TelecomManager;->addNewIncomingCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    .line 78
    .end local v2    # "extras":Landroid/os/Bundle;
    .end local v3    # "phoneAccount":Landroid/telecom/PhoneAccount;
    .end local v4    # "tm":Landroid/telecom/TelecomManager;
    :cond_0
    :goto_0
    return-void

    .line 63
    .end local v0    # "accountHandle":Landroid/telecom/PhoneAccountHandle;
    :catch_0
    move-exception v1

    .line 64
    .local v1, "e":Ljava/lang/ClassCastException;
    const-string/jumbo v5, "takeCall, Bad account handle detected. Bailing!"

    invoke-static {v5}, Lcom/android/services/telephony/sip/SipIncomingCallReceiver;->log(Ljava/lang/String;)V

    .line 65
    return-void

    .line 75
    .end local v1    # "e":Ljava/lang/ClassCastException;
    .restart local v0    # "accountHandle":Landroid/telecom/PhoneAccountHandle;
    .restart local v2    # "extras":Landroid/os/Bundle;
    .restart local v3    # "phoneAccount":Landroid/telecom/PhoneAccount;
    .restart local v4    # "tm":Landroid/telecom/TelecomManager;
    :cond_1
    const-string/jumbo v5, "takeCall, PhoneAccount is disabled. Not accepting incoming call..."

    invoke-static {v5}, Lcom/android/services/telephony/sip/SipIncomingCallReceiver;->log(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 39
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "action":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipIncomingCallReceiver;->isRunningInSystemUser()Z

    move-result v1

    if-nez v1, :cond_0

    .line 43
    return-void

    .line 46
    :cond_0
    invoke-static {p1}, Lcom/android/services/telephony/sip/SipUtil;->isVoipSupported(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 48
    return-void

    .line 51
    :cond_1
    const-string/jumbo v1, "com.android.phone.SIP_INCOMING_CALL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/android/services/telephony/sip/SipIncomingCallReceiver;->takeCall(Landroid/content/Context;Landroid/content/Intent;)V

    .line 56
    :cond_2
    return-void
.end method
