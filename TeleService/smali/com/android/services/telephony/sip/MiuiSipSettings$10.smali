.class Lcom/android/services/telephony/sip/MiuiSipSettings$10;
.super Ljava/lang/Object;
.source "MiuiSipSettings.java"

# interfaces
.implements Landroid/net/sip/SipRegistrationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/services/telephony/sip/MiuiSipSettings;->createRegistrationListener()Landroid/net/sip/SipRegistrationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/sip/MiuiSipSettings;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/sip/MiuiSipSettings;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onRegistering(Ljava/lang/String;)V
    .locals 3
    .param p1, "profileUri"    # Ljava/lang/String;

    .prologue
    .line 419
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 420
    const v2, 0x7f0b01cc

    .line 419
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    return-void
.end method

.method public onRegistrationDone(Ljava/lang/String;J)V
    .locals 3
    .param p1, "profileUri"    # Ljava/lang/String;
    .param p2, "expiryTime"    # J

    .prologue
    .line 413
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 414
    const v2, 0x7f0b01d2

    .line 413
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    return-void
.end method

.method public onRegistrationFailed(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "profileUri"    # Ljava/lang/String;
    .param p2, "errorCode"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 426
    packed-switch p2, :pswitch_data_0

    .line 453
    :pswitch_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 455
    const/4 v3, 0x0

    aput-object p3, v2, v3

    .line 454
    const v3, 0x7f0b01d3

    .line 453
    invoke-virtual {v1, v3, v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :goto_0
    return-void

    .line 428
    :pswitch_1
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 429
    const v2, 0x7f0b01cd

    .line 428
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 432
    :pswitch_2
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 433
    const v2, 0x7f0b01d4

    .line 432
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 436
    :pswitch_3
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 437
    const v2, 0x7f0b01d5

    .line 436
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 440
    :pswitch_4
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    invoke-virtual {v0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/net/sip/SipManager;->isSipWifiOnly(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 442
    const v2, 0x7f0b01d0

    .line 441
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 445
    const v2, 0x7f0b01cf

    .line 444
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 449
    :pswitch_5
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 450
    const v2, 0x7f0b01d1

    .line 449
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 426
    :pswitch_data_0
    .packed-switch -0xc
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
