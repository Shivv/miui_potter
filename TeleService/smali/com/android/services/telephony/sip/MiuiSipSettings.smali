.class public Lcom/android/services/telephony/sip/MiuiSipSettings;
.super Lmiui/preference/PreferenceActivity;
.source "MiuiSipSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;
    }
.end annotation


# instance fields
.field private mButtonSipCallOptions:Landroid/preference/ListPreference;

.field private mButtonSipReceiveCalls:Landroid/preference/CheckBoxPreference;

.field private mCallManager:Lcom/android/internal/telephony/CallManager;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mProfile:Landroid/net/sip/SipProfile;

.field private mProfileDb:Lcom/android/services/telephony/sip/SipProfileDb;

.field private mSipAddAccountTip:Landroid/view/View;

.field private mSipListContainer:Landroid/preference/PreferenceCategory;

.field private mSipManager:Landroid/net/sip/SipManager;

.field private mSipPreferenceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mSipProfileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/sip/SipProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

.field private mUid:I


# direct methods
.method static synthetic -get0(Lcom/android/services/telephony/sip/MiuiSipSettings;)Landroid/net/sip/SipProfile;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mProfile:Landroid/net/sip/SipProfile;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/services/telephony/sip/MiuiSipSettings;)Landroid/net/sip/SipManager;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipManager:Landroid/net/sip/SipManager;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/services/telephony/sip/MiuiSipSettings;)Ljava/util/Map;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipPreferenceMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/services/telephony/sip/MiuiSipSettings;)Lcom/android/services/telephony/sip/SipSharedPreferences;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/services/telephony/sip/MiuiSipSettings;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;

    .prologue
    iget v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mUid:I

    return v0
.end method

.method static synthetic -wrap0(Lcom/android/services/telephony/sip/MiuiSipSettings;I)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;
    .param p1, "uid"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/net/sip/SipProfile;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;
    .param p1, "profile"    # Landroid/net/sip/SipProfile;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getProfileName(Landroid/net/sip/SipProfile;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap10(Lcom/android/services/telephony/sip/MiuiSipSettings;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->updateProfilesStatus()V

    return-void
.end method

.method static synthetic -wrap11(Lcom/android/services/telephony/sip/MiuiSipSettings;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->updateSipAddAccountTip()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/net/sip/SipProfile;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;
    .param p1, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->addProfile(Landroid/net/sip/SipProfile;)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/net/sip/SipProfile;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;
    .param p1, "profile"    # Landroid/net/sip/SipProfile;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->handleProfileClick(Landroid/net/sip/SipProfile;)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/Object;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;
    .param p1, "objValue"    # Ljava/lang/Object;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->handleSipCallOptionsChange(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/services/telephony/sip/MiuiSipSettings;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;
    .param p1, "enabled"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->handleSipReceiveCallsOption(Z)V

    return-void
.end method

.method static synthetic -wrap6(Ljava/lang/String;)V
    .locals 0
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/services/telephony/sip/MiuiSipSettings;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->retrieveSipLists()V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;
    .param p1, "profileUri"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->showRegistrationMessage(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap9(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/net/sip/SipProfile;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/MiuiSipSettings;
    .param p1, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->unregisterProfile(Landroid/net/sip/SipProfile;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 95
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    iput v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mUid:I

    .line 62
    return-void
.end method

.method private addPreferenceFor(Landroid/net/sip/SipProfile;)V
    .locals 3
    .param p1, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 323
    new-instance v0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;

    invoke-direct {v0, p0, p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/content/Context;Landroid/net/sip/SipProfile;)V

    .line 324
    .local v0, "pref":Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipPreferenceMap:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipListContainer:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 328
    new-instance v1, Lcom/android/services/telephony/sip/MiuiSipSettings$6;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/sip/MiuiSipSettings$6;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;)V

    .line 327
    invoke-virtual {v0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 335
    return-void
.end method

.method private addProfile(Landroid/net/sip/SipProfile;)V
    .locals 4
    .param p1, "p"    # Landroid/net/sip/SipProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 380
    :try_start_0
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipManager:Landroid/net/sip/SipManager;

    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    move-result-object v2

    .line 381
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->createRegistrationListener()Landroid/net/sip/SipRegistrationListener;

    move-result-object v3

    .line 380
    invoke-virtual {v1, v2, v3}, Landroid/net/sip/SipManager;->setRegistrationListener(Ljava/lang/String;Landroid/net/sip/SipRegistrationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 385
    :goto_0
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipProfileList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->addPreferenceFor(Landroid/net/sip/SipProfile;)V

    .line 387
    return-void

    .line 382
    :catch_0
    move-exception v0

    .line 383
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "addProfile, cannot set registration listener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createRegistrationListener()Landroid/net/sip/SipRegistrationListener;
    .locals 1

    .prologue
    .line 410
    new-instance v0, Lcom/android/services/telephony/sip/MiuiSipSettings$10;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/sip/MiuiSipSettings$10;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;)V

    return-object v0
.end method

.method private getPackageNameFromUid(I)Ljava/lang/String;
    .locals 6
    .param p1, "uid"    # I

    .prologue
    .line 137
    :try_start_0
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "pkgs":[Ljava/lang/String;
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 139
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 140
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "pkgs":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 141
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getPackageNameFromUid, cannot find name of uid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/services/telephony/sip/MiuiSipSettings;->log(Ljava/lang/String;)V

    .line 143
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "uid:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getProfileFromList(Landroid/net/sip/SipProfile;)Landroid/net/sip/SipProfile;
    .locals 4
    .param p1, "activeProfile"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 312
    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipProfileList:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "p$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/sip/SipProfile;

    .line 313
    .local v0, "p":Landroid/net/sip/SipProfile;
    invoke-virtual {v0}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 314
    return-object v0

    .line 317
    .end local v0    # "p":Landroid/net/sip/SipProfile;
    :cond_1
    const/4 v2, 0x0

    return-object v2
.end method

.method private getProfileName(Landroid/net/sip/SipProfile;)Ljava/lang/String;
    .locals 3
    .param p1, "profile"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 244
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getProfileName()Ljava/lang/String;

    move-result-object v0

    .line 245
    .local v0, "profileName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 246
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUserName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getSipDomain()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 248
    :cond_0
    return-object v0
.end method

.method private handleProfileClick(Landroid/net/sip/SipProfile;)V
    .locals 4
    .param p1, "profile"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 338
    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getCallingUid()I

    move-result v0

    .line 339
    .local v0, "uid":I
    iget v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mUid:I

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_1

    .line 340
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->startSipEditor(Landroid/net/sip/SipProfile;)V

    .line 341
    return-void

    .line 343
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 344
    const v2, 0x7f0b01c8

    .line 343
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 345
    const v2, 0x1010355

    .line 343
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 347
    new-instance v2, Lcom/android/services/telephony/sip/MiuiSipSettings$7;

    invoke-direct {v2, p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings$7;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/net/sip/SipProfile;)V

    .line 346
    const v3, 0x7f0b01ca

    .line 343
    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 354
    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    .line 343
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 356
    return-void
.end method

.method private handleSipCallOptionsChange(Ljava/lang/Object;)V
    .locals 3
    .param p1, "objValue"    # Ljava/lang/Object;

    .prologue
    .line 523
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 524
    .local v0, "option":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

    invoke-virtual {v1, v0}, Lcom/android/services/telephony/sip/SipSharedPreferences;->setSipCallOption(Ljava/lang/String;)V

    .line 525
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    .line 526
    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    .line 525
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 527
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 528
    const/16 v1, 0xbb8

    invoke-direct {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->updateProfilesStatusDelay(I)V

    .line 529
    return-void
.end method

.method private declared-synchronized handleSipReceiveCallsOption(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    monitor-enter p0

    .line 552
    :try_start_0
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

    invoke-virtual {v1, p1}, Lcom/android/services/telephony/sip/SipSharedPreferences;->setReceivingCallsEnabled(Z)V

    .line 554
    invoke-static {p0, p1}, Lcom/android/services/telephony/sip/SipUtil;->useSipToReceiveIncomingCalls(Landroid/content/Context;Z)V

    .line 557
    invoke-static {}, Lcom/android/services/telephony/sip/SipAccountRegistry;->getInstance()Lcom/android/services/telephony/sip/SipAccountRegistry;

    move-result-object v0

    .line 558
    .local v0, "sipAccountRegistry":Lcom/android/services/telephony/sip/SipAccountRegistry;
    invoke-virtual {v0, p0}, Lcom/android/services/telephony/sip/SipAccountRegistry;->restartSipService(Landroid/content/Context;)V

    .line 560
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->updateProfilesStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    .line 561
    return-void

    .end local v0    # "sipAccountRegistry":Lcom/android/services/telephony/sip/SipAccountRegistry;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private static log(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 493
    const-string/jumbo v0, "SIP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[SipSettings] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    return-void
.end method

.method private processActiveProfilesFromSipService()V
    .locals 7

    .prologue
    .line 297
    :try_start_0
    iget-object v4, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipManager:Landroid/net/sip/SipManager;

    invoke-virtual {v4}, Landroid/net/sip/SipManager;->getListOfProfiles()[Landroid/net/sip/SipProfile;

    move-result-object v0

    .line 298
    .local v0, "activeList":[Landroid/net/sip/SipProfile;
    const/4 v4, 0x0

    array-length v5, v0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 299
    .local v1, "activeProfile":Landroid/net/sip/SipProfile;
    invoke-direct {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getProfileFromList(Landroid/net/sip/SipProfile;)Landroid/net/sip/SipProfile;

    move-result-object v3

    .line 300
    .local v3, "profile":Landroid/net/sip/SipProfile;
    if-nez v3, :cond_0

    .line 301
    iget-object v6, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipProfileList:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 303
    :cond_0
    invoke-virtual {v1}, Landroid/net/sip/SipProfile;->getCallingUid()I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/net/sip/SipProfile;->setCallingUid(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 306
    .end local v0    # "activeList":[Landroid/net/sip/SipProfile;
    .end local v1    # "activeProfile":Landroid/net/sip/SipProfile;
    .end local v3    # "profile":Landroid/net/sip/SipProfile;
    :catch_0
    move-exception v2

    .line 307
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "processActiveProfilesFromSipService error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/services/telephony/sip/MiuiSipSettings;->log(Ljava/lang/String;)V

    .line 309
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    return-void
.end method

.method private registerForReceiveCallsCheckBox()V
    .locals 2

    .prologue
    .line 533
    const-string/jumbo v0, "sip_receive_calls_key"

    .line 532
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipReceiveCalls:Landroid/preference/CheckBoxPreference;

    .line 534
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipReceiveCalls:Landroid/preference/CheckBoxPreference;

    .line 535
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipSharedPreferences;->isReceivingCallsEnabled()Z

    move-result v1

    .line 534
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 536
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipReceiveCalls:Landroid/preference/CheckBoxPreference;

    .line 537
    new-instance v1, Lcom/android/services/telephony/sip/MiuiSipSettings$12;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/sip/MiuiSipSettings$12;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;)V

    .line 536
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 549
    return-void
.end method

.method private registerForSipCallOptionPreference()V
    .locals 4

    .prologue
    .line 497
    const-string/jumbo v1, "button_sip_call_options"

    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    .line 498
    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    invoke-static {p0}, Landroid/net/sip/SipManager;->isSipWifiOnly(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 499
    const v1, 0x7f070043

    .line 498
    :goto_0
    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 502
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    new-instance v2, Lcom/android/services/telephony/sip/MiuiSipSettings$11;

    invoke-direct {v2, p0}, Lcom/android/services/telephony/sip/MiuiSipSettings$11;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;)V

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 510
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

    invoke-virtual {v2}, Lcom/android/services/telephony/sip/SipSharedPreferences;->getSipCallOption()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 511
    .local v0, "optionsValueIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 513
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

    .line 514
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 513
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipSharedPreferences;->setSipCallOption(Ljava/lang/String;)V

    .line 516
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

    invoke-virtual {v2}, Lcom/android/services/telephony/sip/SipSharedPreferences;->getSipCallOption()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 518
    :cond_0
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 519
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 520
    return-void

    .line 500
    .end local v0    # "optionsValueIndex":I
    :cond_1
    const v1, 0x7f070042

    goto :goto_0
.end method

.method private retrieveSipLists()V
    .locals 6

    .prologue
    .line 252
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipPreferenceMap:Ljava/util/Map;

    .line 253
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mProfileDb:Lcom/android/services/telephony/sip/SipProfileDb;

    invoke-virtual {v3}, Lcom/android/services/telephony/sip/SipProfileDb;->retrieveSipProfileList()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipProfileList:Ljava/util/List;

    .line 254
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->processActiveProfilesFromSipService()V

    .line 255
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipProfileList:Ljava/util/List;

    new-instance v4, Lcom/android/services/telephony/sip/MiuiSipSettings$4;

    invoke-direct {v4, p0}, Lcom/android/services/telephony/sip/MiuiSipSettings$4;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 266
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipListContainer:Landroid/preference/PreferenceCategory;

    invoke-virtual {v3}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 267
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipProfileList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 268
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    iget-object v4, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipListContainer:Landroid/preference/PreferenceCategory;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 276
    :cond_0
    new-instance v3, Lcom/android/services/telephony/sip/MiuiSipSettings$5;

    invoke-direct {v3, p0}, Lcom/android/services/telephony/sip/MiuiSipSettings$5;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;)V

    invoke-virtual {p0, v3}, Lcom/android/services/telephony/sip/MiuiSipSettings;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 282
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

    invoke-virtual {v3}, Lcom/android/services/telephony/sip/SipSharedPreferences;->isReceivingCallsEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    return-void

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    iget-object v4, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipListContainer:Landroid/preference/PreferenceCategory;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 271
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipProfileList:Ljava/util/List;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "p$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/sip/SipProfile;

    .line 272
    .local v1, "p":Landroid/net/sip/SipProfile;
    invoke-direct {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->addPreferenceFor(Landroid/net/sip/SipProfile;)V

    goto :goto_0

    .line 283
    .end local v1    # "p":Landroid/net/sip/SipProfile;
    .end local v2    # "p$iterator":Ljava/util/Iterator;
    :cond_2
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipProfileList:Ljava/util/List;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "p$iterator":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/sip/SipProfile;

    .line 284
    .restart local v1    # "p":Landroid/net/sip/SipProfile;
    iget v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mUid:I

    invoke-virtual {v1}, Landroid/net/sip/SipProfile;->getCallingUid()I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 286
    :try_start_0
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipManager:Landroid/net/sip/SipManager;

    .line 287
    invoke-virtual {v1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->createRegistrationListener()Landroid/net/sip/SipRegistrationListener;

    move-result-object v5

    .line 286
    invoke-virtual {v3, v4, v5}, Landroid/net/sip/SipManager;->setRegistrationListener(Ljava/lang/String;Landroid/net/sip/SipRegistrationListener;)V
    :try_end_0
    .catch Landroid/net/sip/SipException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 288
    :catch_0
    move-exception v0

    .line 289
    .local v0, "e":Landroid/net/sip/SipException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "retrieveSipLists, cannot set registration listener: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/services/telephony/sip/MiuiSipSettings;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 293
    .end local v0    # "e":Landroid/net/sip/SipException;
    .end local v1    # "p":Landroid/net/sip/SipProfile;
    :cond_4
    return-void
.end method

.method private showRegistrationMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "profileUri"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 398
    new-instance v0, Lcom/android/services/telephony/sip/MiuiSipSettings$9;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/services/telephony/sip/MiuiSipSettings$9;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 407
    return-void
.end method

.method private startSipEditor(Landroid/net/sip/SipProfile;)V
    .locals 2
    .param p1, "profile"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 390
    iput-object p1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mProfile:Landroid/net/sip/SipProfile;

    .line 391
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/services/telephony/sip/MiuiSipEditor;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 392
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "sip_profile"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 393
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->startActivityForResult(Landroid/content/Intent;I)V

    .line 394
    return-void
.end method

.method private unregisterProfile(Landroid/net/sip/SipProfile;)V
    .locals 3
    .param p1, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 360
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/services/telephony/sip/MiuiSipSettings$8;

    invoke-direct {v1, p0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings$8;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/net/sip/SipProfile;)V

    .line 369
    const-string/jumbo v2, "unregisterProfile"

    .line 360
    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 370
    return-void
.end method

.method private updateProfilesStatus()V
    .locals 2

    .prologue
    .line 221
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/services/telephony/sip/MiuiSipSettings$2;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/sip/MiuiSipSettings$2;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 231
    return-void
.end method

.method private updateProfilesStatusDelay(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 234
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 235
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/android/services/telephony/sip/MiuiSipSettings$3;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/sip/MiuiSipSettings$3;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;)V

    .line 240
    int-to-long v2, p1

    .line 235
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 241
    return-void
.end method

.method private updateSipAddAccountTip()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 564
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipAddAccountTip:Landroid/view/View;

    .line 565
    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipListContainer:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v2

    if-lez v2, :cond_0

    const/16 v0, 0x8

    .line 564
    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 567
    return-void
.end method


# virtual methods
.method deleteProfile(Landroid/net/sip/SipProfile;)V
    .locals 3
    .param p1, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 373
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipProfileList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 374
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipPreferenceMap:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getUriString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;

    .line 375
    .local v0, "pref":Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;
    iget-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipListContainer:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 376
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 197
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    return-void

    .line 198
    :cond_0
    new-instance v0, Lcom/android/services/telephony/sip/MiuiSipSettings$1;

    invoke-direct {v0, p0, p3, p2}, Lcom/android/services/telephony/sip/MiuiSipSettings$1;-><init>(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/content/Intent;I)V

    invoke-virtual {v0}, Lcom/android/services/telephony/sip/MiuiSipSettings$1;->start()V

    .line 218
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 148
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 150
    invoke-static {p0}, Landroid/net/sip/SipManager;->newInstance(Landroid/content/Context;)Landroid/net/sip/SipManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipManager:Landroid/net/sip/SipManager;

    .line 151
    new-instance v1, Lcom/android/services/telephony/sip/SipSharedPreferences;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/sip/SipSharedPreferences;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipSharedPreferences:Lcom/android/services/telephony/sip/SipSharedPreferences;

    .line 152
    new-instance v1, Lcom/android/services/telephony/sip/SipProfileDb;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/sip/SipProfileDb;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mProfileDb:Lcom/android/services/telephony/sip/SipProfileDb;

    .line 154
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 155
    const v1, 0x7f04003b

    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->setContentView(I)V

    .line 156
    const v1, 0x7f060026

    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->addPreferencesFromResource(I)V

    .line 157
    const-string/jumbo v1, "sip_account_list"

    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    iput-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipListContainer:Landroid/preference/PreferenceCategory;

    .line 159
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->getInstance()Lcom/android/internal/telephony/CallManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mCallManager:Lcom/android/internal/telephony/CallManager;

    .line 160
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->registerForReceiveCallsCheckBox()V

    .line 161
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->registerForSipCallOptionPreference()V

    .line 162
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->updateProfilesStatus()V

    .line 164
    const v1, 0x7f0d00c9

    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mSipAddAccountTip:Landroid/view/View;

    .line 165
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->updateSipAddAccountTip()V

    .line 167
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 168
    .local v0, "actionBar":Lmiui/app/ActionBar;
    if-eqz v0, :cond_0

    .line 169
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 171
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 463
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 464
    const v0, 0x7f0b01c1

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 465
    sget v1, Lmiui/R$attr;->actionBarNewIcon:I

    invoke-static {p0, v1}, Lmiui/util/AttributeResolver;->resolveDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 464
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 467
    return v2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    .line 191
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->unregisterForContextMenu(Landroid/view/View;)V

    .line 192
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 478
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 479
    .local v0, "itemId":I
    sparse-switch v0, :sswitch_data_0

    .line 489
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 481
    :sswitch_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->startSipEditor(Landroid/net/sip/SipProfile;)V

    .line 482
    return v2

    .line 485
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->onBackPressed()V

    .line 486
    return v2

    .line 479
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x102002c -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 472
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {p0}, Lcom/android/services/telephony/sip/SipUtil;->isPhoneIdle(Landroid/content/Context;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 473
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 175
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 177
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mCallManager:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v0}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v0

    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    if-eq v0, v1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipReceiveCalls:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 179
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 185
    :goto_0
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->updateSipAddAccountTip()V

    .line 186
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipReceiveCalls:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 182
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings;->mButtonSipCallOptions:Landroid/preference/ListPreference;

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_0
.end method
