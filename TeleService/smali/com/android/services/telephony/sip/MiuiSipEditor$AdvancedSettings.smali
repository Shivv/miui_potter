.class Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;
.super Ljava/lang/Object;
.source "MiuiSipEditor.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/sip/MiuiSipEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AdvancedSettings"
.end annotation


# instance fields
.field private mAdvancedSettingsTrigger:Landroid/preference/Preference;

.field private mPreferences:[Landroid/preference/Preference;

.field private mShowing:Z

.field final synthetic this$0:Lcom/android/services/telephony/sip/MiuiSipEditor;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/sip/MiuiSipEditor;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/services/telephony/sip/MiuiSipEditor;

    .prologue
    .line 570
    iput-object p1, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/MiuiSipEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mShowing:Z

    .line 571
    invoke-virtual {p1}, Lcom/android/services/telephony/sip/MiuiSipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 572
    const v1, 0x7f0b01e1

    invoke-virtual {p1, v1}, Lcom/android/services/telephony/sip/MiuiSipEditor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 571
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mAdvancedSettingsTrigger:Landroid/preference/Preference;

    .line 573
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mAdvancedSettingsTrigger:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 575
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->loadAdvancedPreferences()V

    .line 576
    return-void
.end method

.method private hide()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 610
    iput-boolean v2, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mShowing:Z

    .line 611
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mAdvancedSettingsTrigger:Landroid/preference/Preference;

    const v4, 0x7f0b01eb

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(I)V

    .line 612
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/MiuiSipEditor;

    invoke-virtual {v3}, Lcom/android/services/telephony/sip/MiuiSipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 613
    .local v1, "screen":Landroid/preference/PreferenceGroup;
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 614
    .local v0, "pref":Landroid/preference/Preference;
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 613
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 616
    .end local v0    # "pref":Landroid/preference/Preference;
    :cond_0
    return-void
.end method

.method private loadAdvancedPreferences()V
    .locals 9

    .prologue
    .line 579
    iget-object v7, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/MiuiSipEditor;

    invoke-virtual {v7}, Lcom/android/services/telephony/sip/MiuiSipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    .line 581
    .local v6, "screen":Landroid/preference/PreferenceGroup;
    iget-object v7, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/MiuiSipEditor;

    const v8, 0x7f060024

    invoke-virtual {v7, v8}, Lcom/android/services/telephony/sip/MiuiSipEditor;->addPreferencesFromResource(I)V

    .line 583
    iget-object v7, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/MiuiSipEditor;

    const v8, 0x7f0b01ff

    invoke-virtual {v7, v8}, Lcom/android/services/telephony/sip/MiuiSipEditor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 582
    invoke-virtual {v6, v7}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 584
    .local v0, "group":Landroid/preference/PreferenceGroup;
    invoke-virtual {v6, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 586
    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v7

    new-array v7, v7, [Landroid/preference/Preference;

    iput-object v7, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    .line 587
    invoke-virtual {v6}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v3

    .line 588
    .local v3, "order":I
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v7, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    array-length v2, v7

    .local v2, "n":I
    move v4, v3

    .end local v3    # "order":I
    .local v4, "order":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 589
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v5

    .line 590
    .local v5, "pref":Landroid/preference/Preference;
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "order":I
    .restart local v3    # "order":I
    invoke-virtual {v5, v4}, Landroid/preference/Preference;->setOrder(I)V

    .line 591
    iget-object v7, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/MiuiSipEditor;

    invoke-static {v7, v5}, Lcom/android/services/telephony/sip/MiuiSipEditor;->-wrap5(Lcom/android/services/telephony/sip/MiuiSipEditor;Landroid/preference/Preference;)V

    .line 592
    iget-object v7, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    aput-object v5, v7, v1

    .line 588
    add-int/lit8 v1, v1, 0x1

    move v4, v3

    .end local v3    # "order":I
    .restart local v4    # "order":I
    goto :goto_0

    .line 594
    .end local v5    # "pref":Landroid/preference/Preference;
    :cond_0
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 620
    iget-boolean v0, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mShowing:Z

    if-nez v0, :cond_0

    .line 621
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->show()V

    .line 625
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 623
    :cond_0
    invoke-direct {p0}, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->hide()V

    goto :goto_0
.end method

.method show()V
    .locals 5

    .prologue
    .line 597
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mShowing:Z

    .line 598
    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mAdvancedSettingsTrigger:Landroid/preference/Preference;

    const v3, 0x7f0b01ec

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    .line 599
    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->this$0:Lcom/android/services/telephony/sip/MiuiSipEditor;

    invoke-virtual {v2}, Lcom/android/services/telephony/sip/MiuiSipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 600
    .local v1, "screen":Landroid/preference/PreferenceGroup;
    iget-object v3, p0, Lcom/android/services/telephony/sip/MiuiSipEditor$AdvancedSettings;->mPreferences:[Landroid/preference/Preference;

    const/4 v2, 0x0

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 601
    .local v0, "pref":Landroid/preference/Preference;
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 600
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 607
    .end local v0    # "pref":Landroid/preference/Preference;
    :cond_0
    return-void
.end method
