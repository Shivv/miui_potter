.class final Lcom/android/services/telephony/sip/SipConnection;
.super Landroid/telecom/Connection;
.source "SipConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/sip/SipConnection$1;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-internal-telephony-Call$StateSwitchesValues:[I


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mOriginalConnection:Lcom/android/internal/telephony/Connection;

.field private mOriginalConnectionState:Lcom/android/internal/telephony/Call$State;


# direct methods
.method private static synthetic -getcom-android-internal-telephony-Call$StateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/services/telephony/sip/SipConnection;->-com-android-internal-telephony-Call$StateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/services/telephony/sip/SipConnection;->-com-android-internal-telephony-Call$StateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/internal/telephony/Call$State;->values()[Lcom/android/internal/telephony/Call$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DISCONNECTING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    sput-object v0, Lcom/android/services/telephony/sip/SipConnection;->-com-android-internal-telephony-Call$StateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1

    :catch_8
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -wrap0(Lcom/android/services/telephony/sip/SipConnection;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/SipConnection;
    .param p1, "force"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/SipConnection;->updateState(Z)V

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/telecom/Connection;-><init>()V

    .line 44
    new-instance v0, Lcom/android/services/telephony/sip/SipConnection$1;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/sip/SipConnection$1;-><init>(Lcom/android/services/telephony/sip/SipConnection;)V

    iput-object v0, p0, Lcom/android/services/telephony/sip/SipConnection;->mHandler:Landroid/os/Handler;

    .line 56
    sget-object v0, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    iput-object v0, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnectionState:Lcom/android/internal/telephony/Call$State;

    .line 60
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->setInitializing()V

    .line 61
    return-void
.end method

.method private buildCallCapabilities()I
    .locals 3

    .prologue
    .line 280
    const/16 v0, 0x42

    .line 281
    .local v0, "capabilities":I
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getState()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 282
    :cond_0
    const/16 v0, 0x43

    .line 284
    :cond_1
    return v0
.end method

.method private close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 355
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 356
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/sip/SipPhone;->unregisterForPreciseCallStateChanged(Landroid/os/Handler;)V

    .line 358
    :cond_0
    iput-object v2, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    .line 359
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->destroy()V

    .line 360
    return-void
.end method

.method private static getAddressFromNumber(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 348
    if-nez p0, :cond_0

    .line 349
    const-string/jumbo p0, ""

    .line 351
    :cond_0
    const-string/jumbo v0, "sip"

    invoke-static {v0, p0, v1}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private getCall()Lcom/android/internal/telephony/Call;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 219
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    return-object v0

    .line 222
    :cond_0
    return-object v1
.end method

.method private isValidRingingCall()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 234
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 235
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Call$State;->isRinging()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getEarliestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    iget-object v3, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 235
    :cond_0
    return v1
.end method

.method private static log(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 363
    const-string/jumbo v0, "SIP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[SipConnection] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    return-void
.end method

.method private setTechnologyTypeExtra()V
    .locals 4

    .prologue
    .line 330
    const/4 v1, 0x3

    .line 331
    .local v1, "phoneType":I
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_0

    .line 332
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 333
    .local v0, "b":Landroid/os/Bundle;
    const-string/jumbo v2, "android.telecom.extra.CALL_TECHNOLOGY_TYPE"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 334
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/SipConnection;->setExtras(Landroid/os/Bundle;)V

    .line 338
    .end local v0    # "b":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 336
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "android.telecom.extra.CALL_TECHNOLOGY_TYPE"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private updateAddress()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 309
    iget-object v4, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v4, :cond_3

    .line 310
    iget-object v4, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/services/telephony/sip/SipConnection;->getAddressFromNumber(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 311
    .local v0, "address":Landroid/net/Uri;
    iget-object v4, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getNumberPresentation()I

    move-result v3

    .line 312
    .local v3, "presentation":I
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getAddress()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v0, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 313
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getAddressPresentation()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 314
    :cond_0
    const-string/jumbo v4, "updateAddress, address changed"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 315
    invoke-virtual {p0, v0, v3}, Lcom/android/services/telephony/sip/SipConnection;->setAddress(Landroid/net/Uri;I)V

    .line 318
    :cond_1
    iget-object v4, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getCnapName()Ljava/lang/String;

    move-result-object v1

    .line 319
    .local v1, "name":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getCnapNamePresentation()I

    move-result v2

    .line 320
    .local v2, "namePresentation":I
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getCallerDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 321
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getCallerDisplayNamePresentation()I

    move-result v4

    if-eq v2, v4, :cond_3

    .line 323
    :cond_2
    const-string/jumbo v4, "updateAddress, caller display name changed"

    .line 322
    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 324
    invoke-virtual {p0, v1, v2}, Lcom/android/services/telephony/sip/SipConnection;->setCallerDisplayName(Ljava/lang/String;I)V

    .line 327
    .end local v0    # "address":Landroid/net/Uri;
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "namePresentation":I
    .end local v3    # "presentation":I
    :cond_3
    return-void
.end method

.method private updateState(Z)V
    .locals 3
    .param p1, "force"    # Z

    .prologue
    .line 240
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-nez v1, :cond_0

    .line 241
    return-void

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v0

    .line 246
    .local v0, "newState":Lcom/android/internal/telephony/Call$State;
    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnectionState:Lcom/android/internal/telephony/Call$State;

    if-eq v1, v0, :cond_2

    .line 247
    :cond_1
    iput-object v0, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnectionState:Lcom/android/internal/telephony/Call$State;

    .line 248
    invoke-static {}, Lcom/android/services/telephony/sip/SipConnection;->-getcom-android-internal-telephony-Call$StateSwitchesValues()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 275
    :goto_0
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/android/services/telephony/sip/SipConnection;->updateCallCapabilities(Z)V

    .line 277
    :cond_2
    return-void

    .line 252
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->setActive()V

    goto :goto_0

    .line 255
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->setOnHold()V

    goto :goto_0

    .line 259
    :pswitch_3
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->setDialing()V

    .line 261
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/SipConnection;->setRingbackRequested(Z)V

    goto :goto_0

    .line 265
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->setRinging()V

    goto :goto_0

    .line 269
    :pswitch_5
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getDisconnectCause()I

    move-result v1

    .line 268
    invoke-static {v1}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(I)Landroid/telecom/DisconnectCause;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/SipConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 270
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->close()V

    goto :goto_0

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method getPhone()Lcom/android/internal/telephony/sip/SipPhone;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 226
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 227
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_0

    .line 228
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/sip/SipPhone;

    return-object v1

    .line 230
    :cond_0
    return-object v1
.end method

.method initialize(Lcom/android/internal/telephony/Connection;)V
    .locals 4
    .param p1, "connection"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v3, 0x0

    .line 65
    iput-object p1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    .line 66
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/sip/SipPhone;->registerForPreciseCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 70
    :cond_0
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->updateAddress()V

    .line 71
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->setTechnologyTypeExtra()V

    .line 72
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->setInitialized()V

    .line 73
    return-void
.end method

.method public onAbort()V
    .locals 0

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->onDisconnect()V

    .line 134
    return-void
.end method

.method onAddedToCallService()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 296
    invoke-direct {p0, v0}, Lcom/android/services/telephony/sip/SipConnection;->updateState(Z)V

    .line 297
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/SipConnection;->updateCallCapabilities(Z)V

    .line 298
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/SipConnection;->setAudioModeIsVoip(Z)V

    .line 299
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCnapName()Ljava/lang/String;

    move-result-object v0

    .line 301
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getCnapNamePresentation()I

    move-result v1

    .line 300
    invoke-virtual {p0, v0, v1}, Lcom/android/services/telephony/sip/SipConnection;->setCallerDisplayName(Ljava/lang/String;I)V

    .line 303
    :cond_0
    return-void
.end method

.method public onAnswer(I)V
    .locals 10
    .param p1, "videoState"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 180
    :try_start_0
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->isValidRingingCall()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 181
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/internal/telephony/sip/SipPhone;->acceptCall(I)V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v1

    .line 194
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onAnswer, IllegalArgumentException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V

    .line 195
    new-array v3, v9, [Ljava/lang/Object;

    const-string/jumbo v4, "31752213"

    aput-object v4, v3, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const-string/jumbo v4, "Invalid SDP."

    aput-object v4, v3, v8

    const v4, 0x534e4554

    invoke-static {v4, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 196
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->onReject()V

    goto :goto_0

    .line 185
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 188
    .local v2, "e":Ljava/lang/IllegalStateException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onAnswer, IllegalStateException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V

    .line 189
    new-array v3, v9, [Ljava/lang/Object;

    const-string/jumbo v4, "31752213"

    aput-object v4, v3, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const-string/jumbo v4, "Invalid codec."

    aput-object v4, v3, v8

    const v4, 0x534e4554

    invoke-static {v4, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 190
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->onReject()V

    goto :goto_0

    .line 183
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 184
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onAnswer, exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onAudioStateChanged(Landroid/telecom/AudioState;)V
    .locals 1
    .param p1, "state"    # Landroid/telecom/AudioState;

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/sip/SipPhone;->setEchoSuppressionEnabled()V

    .line 81
    :cond_0
    return-void
.end method

.method public onDisconnect()V
    .locals 3

    .prologue
    .line 108
    :try_start_0
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->isMultiparty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 109
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->hangup()V

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v1, :cond_0

    .line 111
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->hangup()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onDisconnect, exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onHold()V
    .locals 3

    .prologue
    .line 140
    :try_start_0
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getState()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/sip/SipPhone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v1

    sget-object v2, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    if-eq v1, v2, :cond_0

    .line 144
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v1, :cond_1

    .line 145
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v1

    sget-object v2, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    if-ne v1, v2, :cond_1

    .line 146
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/sip/SipPhone;->switchHoldingAndActive()V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    const-string/jumbo v1, "skipping switch from onHold due to internal state:"

    invoke-static {v1}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHold, exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPlayDtmfTone(C)V
    .locals 1
    .param p1, "c"    # C

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/sip/SipPhone;->startDtmf(C)V

    .line 94
    :cond_0
    return-void
.end method

.method public onPostDialContinue(Z)V
    .locals 0
    .param p1, "proceed"    # Z

    .prologue
    .line 216
    return-void
.end method

.method public onReject()V
    .locals 3

    .prologue
    .line 204
    :try_start_0
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->isValidRingingCall()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/sip/SipPhone;->rejectCall()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onReject, exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSeparate()V
    .locals 3

    .prologue
    .line 122
    :try_start_0
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->separate()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSeparate, exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 86
    return-void
.end method

.method public onStopDtmfTone()V
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/sip/SipPhone;->stopDtmf()V

    .line 102
    :cond_0
    return-void
.end method

.method public onUnhold()V
    .locals 3

    .prologue
    .line 160
    :try_start_0
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/sip/SipPhone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v1

    sget-object v2, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    if-eq v1, v2, :cond_0

    .line 164
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    if-eqz v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v1

    sget-object v2, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    if-ne v1, v2, :cond_1

    .line 166
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getPhone()Lcom/android/internal/telephony/sip/SipPhone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/sip/SipPhone;->switchHoldingAndActive()V

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    const-string/jumbo v1, "skipping switch from onUnHold due to internal state."

    invoke-static {v1}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onUnhold, exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/services/telephony/sip/SipConnection;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method updateCallCapabilities(Z)V
    .locals 2
    .param p1, "force"    # Z

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipConnection;->buildCallCapabilities()I

    move-result v0

    .line 289
    .local v0, "newCallCapabilities":I
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipConnection;->getConnectionCapabilities()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 290
    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/SipConnection;->setConnectionCapabilities(I)V

    .line 292
    :cond_1
    return-void
.end method
