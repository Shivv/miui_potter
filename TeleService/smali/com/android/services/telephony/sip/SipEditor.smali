.class public Lcom/android/services/telephony/sip/SipEditor;
.super Landroid/preference/PreferenceActivity;
.source "SipEditor.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;,
        Lcom/android/services/telephony/sip/SipEditor$AlertDialogFragment;,
        Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-services-telephony-sip-SipEditor$PreferenceKeySwitchesValues:[I


# instance fields
.field private mAdvancedSettings:Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;

.field private mDisplayNameSet:Z

.field private mHomeButtonClicked:Z

.field private mOldProfile:Landroid/net/sip/SipProfile;

.field private mProfileDb:Lcom/android/services/telephony/sip/SipProfileDb;

.field private mRemoveButton:Landroid/widget/Button;

.field private mSipAccountRegistry:Lcom/android/services/telephony/sip/SipAccountRegistry;

.field private mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

.field private mUpdateRequired:Z


# direct methods
.method private static synthetic -getcom-android-services-telephony-sip-SipEditor$PreferenceKeySwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/services/telephony/sip/SipEditor;->-com-android-services-telephony-sip-SipEditor$PreferenceKeySwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/services/telephony/sip/SipEditor;->-com-android-services-telephony-sip-SipEditor$PreferenceKeySwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->values()[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->AuthUserName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DomainAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Password:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Port:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ProxyAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->SendKeepAlive:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Transport:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Username:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    sput-object v0, Lcom/android/services/telephony/sip/SipEditor;->-com-android-services-telephony-sip-SipEditor$PreferenceKeySwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1

    :catch_8
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -wrap0()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor;->getDefaultDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/services/telephony/sip/SipEditor;->scramble(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap2(Lcom/android/services/telephony/sip/SipEditor;Landroid/net/sip/SipProfile;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/SipEditor;
    .param p1, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/SipEditor;->deleteAndUnregisterProfile(Landroid/net/sip/SipProfile;)V

    return-void
.end method

.method static synthetic -wrap3(Ljava/lang/String;)V
    .locals 0
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/services/telephony/sip/SipEditor;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/services/telephony/sip/SipEditor;Landroid/net/sip/SipProfile;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/SipEditor;
    .param p1, "p"    # Landroid/net/sip/SipProfile;
    .param p2, "enableProfile"    # Z

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/services/telephony/sip/SipEditor;->saveAndRegisterProfile(Landroid/net/sip/SipProfile;Z)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/services/telephony/sip/SipEditor;Landroid/preference/Preference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/SipEditor;
    .param p1, "pref"    # Landroid/preference/Preference;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/SipEditor;->setupPreference(Landroid/preference/Preference;)V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/services/telephony/sip/SipEditor;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/sip/SipEditor;
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/SipEditor;->showAlert(Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private checkIfDisplayNameSet()V
    .locals 3

    .prologue
    .line 542
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 543
    .local v0, "displayName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 544
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor;->getDefaultDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 543
    :goto_0
    iput-boolean v1, p0, Lcom/android/services/telephony/sip/SipEditor;->mDisplayNameSet:Z

    .line 546
    iget-boolean v1, p0, Lcom/android/services/telephony/sip/SipEditor;->mDisplayNameSet:Z

    if-eqz v1, :cond_1

    .line 547
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    iget-object v1, v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 551
    :goto_1
    return-void

    .line 543
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 549
    :cond_1
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->setValue(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private createSipProfile()Landroid/net/sip/SipProfile;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 429
    new-instance v0, Landroid/net/sip/SipProfile$Builder;

    .line 430
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Username:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 431
    sget-object v2, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DomainAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v2}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 429
    invoke-direct {v0, v1, v2}, Landroid/net/sip/SipProfile$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor;->getProfileName()Ljava/lang/String;

    move-result-object v1

    .line 429
    invoke-virtual {v0, v1}, Landroid/net/sip/SipProfile$Builder;->setProfileName(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;

    move-result-object v0

    .line 433
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Password:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 429
    invoke-virtual {v0, v1}, Landroid/net/sip/SipProfile$Builder;->setPassword(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;

    move-result-object v0

    .line 434
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ProxyAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 429
    invoke-virtual {v0, v1}, Landroid/net/sip/SipProfile$Builder;->setOutboundProxy(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;

    move-result-object v0

    .line 435
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Transport:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 429
    invoke-virtual {v0, v1}, Landroid/net/sip/SipProfile$Builder;->setProtocol(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;

    move-result-object v0

    .line 436
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 429
    invoke-virtual {v0, v1}, Landroid/net/sip/SipProfile$Builder;->setDisplayName(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;

    move-result-object v0

    .line 437
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Port:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 429
    invoke-virtual {v0, v1}, Landroid/net/sip/SipProfile$Builder;->setPort(I)Landroid/net/sip/SipProfile$Builder;

    move-result-object v0

    .line 438
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor;->isAlwaysSendKeepAlive()Z

    move-result v1

    .line 429
    invoke-virtual {v0, v1}, Landroid/net/sip/SipProfile$Builder;->setSendKeepAlive(Z)Landroid/net/sip/SipProfile$Builder;

    move-result-object v0

    .line 440
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor;->mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipPreferences;->isReceivingCallsEnabled()Z

    move-result v1

    .line 429
    invoke-virtual {v0, v1}, Landroid/net/sip/SipProfile$Builder;->setAutoRegistration(Z)Landroid/net/sip/SipProfile$Builder;

    move-result-object v0

    .line 441
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->AuthUserName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 429
    invoke-virtual {v0, v1}, Landroid/net/sip/SipProfile$Builder;->setAuthUserName(Ljava/lang/String;)Landroid/net/sip/SipProfile$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/sip/SipProfile$Builder;->build()Landroid/net/sip/SipProfile;

    move-result-object v0

    return-object v0
.end method

.method private deleteAndUnregisterProfile(Landroid/net/sip/SipProfile;)V
    .locals 2
    .param p1, "p"    # Landroid/net/sip/SipProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 298
    if-nez p1, :cond_0

    return-void

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mProfileDb:Lcom/android/services/telephony/sip/SipProfileDb;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/sip/SipProfileDb;->deleteProfile(Landroid/net/sip/SipProfile;)V

    .line 300
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mSipAccountRegistry:Lcom/android/services/telephony/sip/SipAccountRegistry;

    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getProfileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/android/services/telephony/sip/SipAccountRegistry;->stopSipService(Landroid/content/Context;Ljava/lang/String;)V

    .line 301
    return-void
.end method

.method private static getDefaultDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    sget-object v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Username:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v0}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPreferenceKey(Landroid/preference/Preference;)Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    .locals 5
    .param p1, "pref"    # Landroid/preference/Preference;

    .prologue
    .line 474
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->values()[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    move-result-object v2

    const/4 v1, 0x0

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 475
    .local v0, "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    iget-object v4, v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    if-ne v4, p1, :cond_0

    return-object v0

    .line 474
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 477
    .end local v0    # "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "not possible to reach here"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getProfileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 424
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Username:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 425
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DomainAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v1}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 424
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isAlwaysSendKeepAlive()Z
    .locals 3

    .prologue
    .line 521
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->SendKeepAlive:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    iget-object v0, v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/ListPreference;

    .line 522
    .local v0, "pref":Landroid/preference/ListPreference;
    const v1, 0x7f0b01f4

    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private isEditTextEmpty(Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;)Z
    .locals 3
    .param p1, "key"    # Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .prologue
    .line 329
    iget-object v0, p1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/EditTextPreference;

    .line 330
    .local v0, "pref":Landroid/preference/EditTextPreference;
    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 331
    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    iget v2, p1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->defaultSummary:I

    invoke-virtual {p0, v2}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 330
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private loadPreferencesFromProfile(Landroid/net/sip/SipProfile;)V
    .locals 11
    .param p1, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    const/4 v7, 0x0

    .line 481
    if-eqz p1, :cond_4

    .line 484
    :try_start_0
    const-class v3, Landroid/net/sip/SipProfile;

    .line 485
    .local v3, "profileClass":Ljava/lang/Class;
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->values()[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    move-result-object v8

    array-length v9, v8

    :goto_0
    if-ge v7, v9, :cond_3

    aget-object v1, v8, v7

    .line 486
    .local v1, "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "get"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 487
    iget v10, v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->text:I

    invoke-virtual {p0, v10}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 486
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 487
    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Class;

    .line 486
    invoke-virtual {v3, v10, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 488
    .local v2, "meth":Ljava/lang/reflect/Method;
    sget-object v6, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->SendKeepAlive:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    if-ne v1, v6, :cond_1

    .line 489
    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 490
    .local v5, "value":Z
    if-eqz v5, :cond_0

    .line 491
    const v6, 0x7f0b01f4

    .line 490
    :goto_1
    invoke-virtual {p0, v6}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->setValue(Ljava/lang/String;)V

    .line 485
    .end local v5    # "value":Z
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 492
    .restart local v5    # "value":Z
    :cond_0
    const v6, 0x7f0b01f3

    goto :goto_1

    .line 494
    .end local v5    # "value":Z
    :cond_1
    const/4 v6, 0x0

    check-cast v6, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 495
    .local v4, "value":Ljava/lang/Object;
    if-nez v4, :cond_2

    const-string/jumbo v6, ""

    :goto_3
    invoke-virtual {v1, v6}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->setValue(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 499
    .end local v1    # "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    .end local v2    # "meth":Ljava/lang/reflect/Method;
    .end local v3    # "profileClass":Ljava/lang/Class;
    .end local v4    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "loadPreferencesFromProfile, can not load pref from profile, exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/services/telephony/sip/SipEditor;->log(Ljava/lang/String;)V

    .line 518
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    return-void

    .line 495
    .restart local v1    # "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    .restart local v2    # "meth":Ljava/lang/reflect/Method;
    .restart local v3    # "profileClass":Ljava/lang/Class;
    .restart local v4    # "value":Ljava/lang/Object;
    :cond_2
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 498
    .end local v1    # "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    .end local v2    # "meth":Ljava/lang/reflect/Method;
    .end local v4    # "value":Ljava/lang/Object;
    :cond_3
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor;->checkIfDisplayNameSet()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 504
    .end local v3    # "profileClass":Ljava/lang/Class;
    :cond_4
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->values()[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    move-result-object v8

    array-length v9, v8

    move v6, v7

    :goto_5
    if-ge v6, v9, :cond_6

    aget-object v1, v8, v6

    .line 505
    .restart local v1    # "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    iget-object v10, v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    invoke-virtual {v10, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 512
    iget v10, v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->initValue:I

    if-eqz v10, :cond_5

    .line 513
    iget v10, v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->initValue:I

    invoke-virtual {p0, v10}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->setValue(Ljava/lang/String;)V

    .line 504
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 516
    .end local v1    # "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    :cond_6
    iput-boolean v7, p0, Lcom/android/services/telephony/sip/SipEditor;->mDisplayNameSet:Z

    goto :goto_4
.end method

.method private static log(Ljava/lang/String;)V
    .locals 3
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 628
    const-string/jumbo v0, "SIP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[SipEditor] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    return-void
.end method

.method private replaceProfile(Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;)V
    .locals 3
    .param p1, "oldProfile"    # Landroid/net/sip/SipProfile;
    .param p2, "newProfile"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 408
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/services/telephony/sip/SipEditor$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/services/telephony/sip/SipEditor$1;-><init>(Lcom/android/services/telephony/sip/SipEditor;Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;)V

    .line 420
    const-string/jumbo v2, "SipEditor"

    .line 408
    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 421
    return-void
.end method

.method private saveAndRegisterProfile(Landroid/net/sip/SipProfile;Z)V
    .locals 2
    .param p1, "p"    # Landroid/net/sip/SipProfile;
    .param p2, "enableProfile"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 286
    if-nez p1, :cond_0

    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mProfileDb:Lcom/android/services/telephony/sip/SipProfileDb;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/sip/SipProfileDb;->saveProfile(Landroid/net/sip/SipProfile;)V

    .line 288
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mSipAccountRegistry:Lcom/android/services/telephony/sip/SipAccountRegistry;

    invoke-virtual {p1}, Landroid/net/sip/SipProfile;->getProfileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1, p2}, Lcom/android/services/telephony/sip/SipAccountRegistry;->startSipService(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 289
    return-void
.end method

.method private static scramble(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 558
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    new-array v0, v1, [C

    .line 559
    .local v0, "cc":[C
    const/16 v1, 0x2a

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([CC)V

    .line 560
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method private setRemovedProfileAndFinish()V
    .locals 3

    .prologue
    .line 304
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/services/telephony/sip/SipSettings;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 305
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/services/telephony/sip/SipEditor;->setResult(ILandroid/content/Intent;)V

    .line 306
    const v1, 0x7f0b01c5

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 308
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor;->mOldProfile:Landroid/net/sip/SipProfile;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/android/services/telephony/sip/SipEditor;->replaceProfile(Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;)V

    .line 310
    return-void
.end method

.method private setupPreference(Landroid/preference/Preference;)V
    .locals 6
    .param p1, "pref"    # Landroid/preference/Preference;

    .prologue
    .line 531
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 532
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->values()[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    move-result-object v3

    const/4 v2, 0x0

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 533
    .local v0, "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    iget v5, v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->text:I

    invoke-virtual {p0, v5}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 534
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 535
    iput-object p1, v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    .line 536
    return-void

    .line 532
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 539
    .end local v0    # "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private showAlert(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 319
    iget-boolean v1, p0, Lcom/android/services/telephony/sip/SipEditor;->mHomeButtonClicked:Z

    if-eqz v1, :cond_0

    .line 321
    return-void

    .line 324
    :cond_0
    invoke-static {p1}, Lcom/android/services/telephony/sip/SipEditor$AlertDialogFragment;->newInstance(Ljava/lang/String;)Lcom/android/services/telephony/sip/SipEditor$AlertDialogFragment;

    move-result-object v0

    .line 325
    .local v0, "newFragment":Lcom/android/services/telephony/sip/SipEditor$AlertDialogFragment;
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/services/telephony/sip/SipEditor$AlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 326
    return-void
.end method

.method private showAlert(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 313
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 314
    .local v0, "msg":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/services/telephony/sip/SipEditor;->showAlert(Ljava/lang/String;)V

    .line 316
    return-void
.end method

.method private validateAndSetResult()V
    .locals 17

    .prologue
    .line 335
    const/4 v1, 0x1

    .line 336
    .local v1, "allEmpty":Z
    const/4 v5, 0x0

    .line 337
    .local v5, "firstEmptyFieldTitle":Ljava/lang/CharSequence;
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->values()[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    move-result-object v13

    const/4 v12, 0x0

    array-length v14, v13

    .end local v5    # "firstEmptyFieldTitle":Ljava/lang/CharSequence;
    :goto_0
    if-ge v12, v14, :cond_4

    aget-object v7, v13, v12

    .line 338
    .local v7, "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    iget-object v8, v7, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    .line 339
    .local v8, "p":Landroid/preference/Preference;
    instance-of v15, v8, Landroid/preference/EditTextPreference;

    if-eqz v15, :cond_1

    move-object v10, v8

    .line 340
    check-cast v10, Landroid/preference/EditTextPreference;

    .line 341
    .local v10, "pref":Landroid/preference/EditTextPreference;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/services/telephony/sip/SipEditor;->isEditTextEmpty(Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;)Z

    move-result v4

    .line 342
    .local v4, "fieldEmpty":Z
    if-eqz v1, :cond_0

    xor-int/lit8 v15, v4, 0x1

    if-eqz v15, :cond_0

    const/4 v1, 0x0

    .line 345
    :cond_0
    if-eqz v4, :cond_2

    .line 346
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor;->-getcom-android-services-telephony-sip-SipEditor$PreferenceKeySwitchesValues()[I

    move-result-object v15

    invoke-virtual {v7}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ordinal()I

    move-result v16

    aget v15, v15, v16

    packed-switch v15, :pswitch_data_0

    .line 358
    if-nez v5, :cond_1

    .line 359
    invoke-virtual {v10}, Landroid/preference/EditTextPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    .line 337
    .end local v4    # "fieldEmpty":Z
    .end local v10    # "pref":Landroid/preference/EditTextPreference;
    :cond_1
    :goto_1
    :pswitch_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 348
    .restart local v4    # "fieldEmpty":Z
    .restart local v10    # "pref":Landroid/preference/EditTextPreference;
    :pswitch_1
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor;->getDefaultDisplayName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto :goto_1

    .line 355
    :pswitch_2
    const v15, 0x7f0b01e9

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    goto :goto_1

    .line 362
    :cond_2
    sget-object v15, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Port:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    if-ne v7, v15, :cond_1

    .line 365
    :try_start_0
    sget-object v15, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Port:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-virtual {v15}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 370
    .local v9, "port":I
    const/16 v15, 0x3e8

    if-lt v9, v15, :cond_3

    const v15, 0xfffe

    if-le v9, v15, :cond_1

    .line 371
    :cond_3
    const v12, 0x7f0b01ef

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/services/telephony/sip/SipEditor;->showAlert(Ljava/lang/String;)V

    .line 372
    return-void

    .line 366
    .end local v9    # "port":I
    :catch_0
    move-exception v3

    .line 367
    .local v3, "e":Ljava/lang/NumberFormatException;
    const v12, 0x7f0b01ef

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/services/telephony/sip/SipEditor;->showAlert(Ljava/lang/String;)V

    .line 368
    return-void

    .line 378
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    .end local v4    # "fieldEmpty":Z
    .end local v7    # "key":Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    .end local v8    # "p":Landroid/preference/Preference;
    .end local v10    # "pref":Landroid/preference/EditTextPreference;
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/services/telephony/sip/SipEditor;->mUpdateRequired:Z

    if-nez v12, :cond_5

    .line 379
    invoke-virtual/range {p0 .. p0}, Lcom/android/services/telephony/sip/SipEditor;->finish()V

    .line 380
    return-void

    .line 381
    :cond_5
    if-eqz v1, :cond_6

    .line 382
    const v12, 0x7f0b01ed

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/android/services/telephony/sip/SipEditor;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/services/telephony/sip/SipEditor;->showAlert(Ljava/lang/String;)V

    .line 383
    return-void

    .line 384
    :cond_6
    if-eqz v5, :cond_7

    .line 385
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v5, v12, v13

    const v13, 0x7f0b01ee

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12}, Lcom/android/services/telephony/sip/SipEditor;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/services/telephony/sip/SipEditor;->showAlert(Ljava/lang/String;)V

    .line 386
    return-void

    .line 389
    :cond_7
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/sip/SipEditor;->createSipProfile()Landroid/net/sip/SipProfile;

    move-result-object v11

    .line 390
    .local v11, "profile":Landroid/net/sip/SipProfile;
    new-instance v6, Landroid/content/Intent;

    const-class v12, Lcom/android/services/telephony/sip/SipSettings;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 391
    .local v6, "intent":Landroid/content/Intent;
    const-string/jumbo v12, "sip_profile"

    invoke-virtual {v6, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 392
    const/4 v12, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v6}, Lcom/android/services/telephony/sip/SipEditor;->setResult(ILandroid/content/Intent;)V

    .line 393
    const v12, 0x7f0b01c4

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 395
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/services/telephony/sip/SipEditor;->mOldProfile:Landroid/net/sip/SipProfile;

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v11}, Lcom/android/services/telephony/sip/SipEditor;->replaceProfile(Landroid/net/sip/SipProfile;Landroid/net/sip/SipProfile;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 401
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v11    # "profile":Landroid/net/sip/SipProfile;
    :goto_2
    return-void

    .line 397
    :catch_1
    move-exception v2

    .line 398
    .local v2, "e":Ljava/lang/Exception;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "validateAndSetResult, can not create new SipProfile, exception: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/android/services/telephony/sip/SipEditor;->log(Ljava/lang/String;)V

    .line 399
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/services/telephony/sip/SipEditor;->showAlert(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 346
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 187
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 189
    new-instance v4, Lcom/android/services/telephony/sip/SipPreferences;

    invoke-direct {v4, p0}, Lcom/android/services/telephony/sip/SipPreferences;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/services/telephony/sip/SipEditor;->mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

    .line 190
    new-instance v4, Lcom/android/services/telephony/sip/SipProfileDb;

    invoke-direct {v4, p0}, Lcom/android/services/telephony/sip/SipProfileDb;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/services/telephony/sip/SipEditor;->mProfileDb:Lcom/android/services/telephony/sip/SipProfileDb;

    .line 191
    invoke-static {}, Lcom/android/services/telephony/sip/SipAccountRegistry;->getInstance()Lcom/android/services/telephony/sip/SipAccountRegistry;

    move-result-object v4

    iput-object v4, p0, Lcom/android/services/telephony/sip/SipEditor;->mSipAccountRegistry:Lcom/android/services/telephony/sip/SipAccountRegistry;

    .line 193
    const v4, 0x7f040064

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/sip/SipEditor;->setContentView(I)V

    .line 194
    const v4, 0x7f06003a

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/sip/SipEditor;->addPreferencesFromResource(I)V

    .line 196
    if-nez p1, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "sip_profile"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 196
    :goto_0
    check-cast v2, Landroid/net/sip/SipProfile;

    iput-object v2, p0, Lcom/android/services/telephony/sip/SipEditor;->mOldProfile:Landroid/net/sip/SipProfile;

    .line 200
    .local v2, "p":Landroid/net/sip/SipProfile;
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 201
    .local v3, "screen":Landroid/preference/PreferenceGroup;
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v3}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v1

    .local v1, "n":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 202
    invoke-virtual {v3, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/services/telephony/sip/SipEditor;->setupPreference(Landroid/preference/Preference;)V

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 198
    .end local v0    # "i":I
    .end local v1    # "n":I
    .end local v2    # "p":Landroid/net/sip/SipProfile;
    .end local v3    # "screen":Landroid/preference/PreferenceGroup;
    :cond_0
    const-string/jumbo v4, "profile"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    goto :goto_0

    .line 205
    .restart local v0    # "i":I
    .restart local v1    # "n":I
    .restart local v2    # "p":Landroid/net/sip/SipProfile;
    .restart local v3    # "screen":Landroid/preference/PreferenceGroup;
    :cond_1
    if-nez v2, :cond_2

    .line 206
    const v4, 0x7f0b01d8

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceGroup;->setTitle(I)V

    .line 209
    :cond_2
    new-instance v4, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;

    invoke-direct {v4, p0}, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;-><init>(Lcom/android/services/telephony/sip/SipEditor;)V

    iput-object v4, p0, Lcom/android/services/telephony/sip/SipEditor;->mAdvancedSettings:Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;

    .line 211
    invoke-direct {p0, v2}, Lcom/android/services/telephony/sip/SipEditor;->loadPreferencesFromProfile(Landroid/net/sip/SipProfile;)V

    .line 212
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 226
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 227
    const/4 v0, 0x2

    const v1, 0x7f0b01c7

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 229
    const v0, 0x7f0b01c6

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 231
    const/4 v0, 0x3

    const v1, 0x7f0b01c2

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 233
    return v3
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 269
    packed-switch p1, :pswitch_data_0

    .line 274
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 271
    :pswitch_0
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor;->validateAndSetResult()V

    .line 272
    const/4 v0, 0x1

    return v0

    .line 269
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 246
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 264
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 248
    :sswitch_0
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor;->validateAndSetResult()V

    .line 249
    return v1

    .line 252
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->finish()V

    .line 253
    return v1

    .line 256
    :sswitch_2
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor;->setRemovedProfileAndFinish()V

    .line 257
    return v1

    .line 260
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->finish()V

    .line 261
    return v1

    .line 246
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x102002c -> :sswitch_3
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mHomeButtonClicked:Z

    .line 219
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor;->validateAndSetResult()V

    .line 221
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 222
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "pref"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 446
    iget-boolean v1, p0, Lcom/android/services/telephony/sip/SipEditor;->mUpdateRequired:Z

    if-nez v1, :cond_0

    .line 447
    iput-boolean v2, p0, Lcom/android/services/telephony/sip/SipEditor;->mUpdateRequired:Z

    .line 450
    :cond_0
    instance-of v1, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    .line 451
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->invalidateOptionsMenu()V

    .line 452
    return v2

    .line 454
    :cond_1
    if-nez p2, :cond_3

    const-string/jumbo v0, ""

    .line 455
    .local v0, "value":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 456
    invoke-direct {p0, p1}, Lcom/android/services/telephony/sip/SipEditor;->getPreferenceKey(Landroid/preference/Preference;)Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    move-result-object v1

    iget v1, v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->defaultSummary:I

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 463
    :goto_1
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    iget-object v1, v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    if-ne p1, v1, :cond_2

    .line 464
    check-cast p1, Landroid/preference/EditTextPreference;

    .end local p1    # "pref":Landroid/preference/Preference;
    invoke-virtual {p1, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 465
    invoke-direct {p0}, Lcom/android/services/telephony/sip/SipEditor;->checkIfDisplayNameSet()V

    .line 469
    :cond_2
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->invalidateOptionsMenu()V

    .line 470
    return v2

    .line 454
    .end local v0    # "value":Ljava/lang/String;
    .restart local p1    # "pref":Landroid/preference/Preference;
    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "value":Ljava/lang/String;
    goto :goto_0

    .line 457
    :cond_4
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Password:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    iget-object v1, v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    if-ne p1, v1, :cond_5

    .line 458
    invoke-static {v0}, Lcom/android/services/telephony/sip/SipEditor;->scramble(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 460
    :cond_5
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x1

    .line 238
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 239
    .local v0, "removeMenu":Landroid/view/MenuItem;
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor;->mOldProfile:Landroid/net/sip/SipProfile;

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 240
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/services/telephony/sip/SipEditor;->mUpdateRequired:Z

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 241
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1

    .line 239
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 172
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 173
    iput-boolean v1, p0, Lcom/android/services/telephony/sip/SipEditor;->mHomeButtonClicked:Z

    .line 174
    invoke-static {p0}, Lcom/android/services/telephony/sip/SipUtil;->isPhoneIdle(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mAdvancedSettings:Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;

    invoke-virtual {v0}, Lcom/android/services/telephony/sip/SipEditor$AdvancedSettings;->show()V

    .line 176
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 177
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mRemoveButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mRemoveButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 180
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mRemoveButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor;->mRemoveButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
