.class final Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;
.super Ljava/lang/Object;
.source "PstnPhoneCapabilitiesNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$1;,
        Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$Listener;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mListener:Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$Listener;

.field private final mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method static synthetic -wrap0(Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;
    .param p1, "ar"    # Landroid/os/AsyncResult;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->handleVideoCapabilitesChanged(Landroid/os/AsyncResult;)V

    return-void
.end method

.method constructor <init>(Lcom/android/internal/telephony/Phone;Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$Listener;)V
    .locals 1
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "listener"    # Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$Listener;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$1;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$1;-><init>(Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;)V

    iput-object v0, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mHandler:Landroid/os/Handler;

    .line 62
    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iput-object p1, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 65
    iput-object p2, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mListener:Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$Listener;

    .line 67
    invoke-direct {p0}, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->registerForNotifications()V

    .line 68
    return-void
.end method

.method private handleVideoCapabilitesChanged(Landroid/os/AsyncResult;)V
    .locals 5
    .param p1, "ar"    # Landroid/os/AsyncResult;

    .prologue
    const/4 v4, 0x0

    .line 92
    :try_start_0
    iget-object v2, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 93
    .local v1, "isVideoCapable":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handleVideoCapabilitesChanged. Video capability - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    iget-object v2, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mListener:Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$Listener;

    invoke-interface {v2, v1}, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$Listener;->onVideoCapabilitiesChanged(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    .end local v1    # "isVideoCapable":Z
    :goto_0
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handleVideoCapabilitesChanged. Exception="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private registerForNotifications()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    iget-object v0, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v0, :cond_0

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Registering: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    iget-object v0, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/Phone;->registerForVideoCapabilityChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 81
    :cond_0
    return-void
.end method

.method private unregisterForNotifications()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Unregistering: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    iget-object v0, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForVideoCapabilityChanged(Landroid/os/Handler;)V

    .line 88
    :cond_0
    return-void
.end method


# virtual methods
.method teardown()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->unregisterForNotifications()V

    .line 73
    return-void
.end method
