.class Lcom/android/services/telephony/TelephonyConnectionService$4;
.super Ljava/lang/Object;
.source "TelephonyConnectionService.java"

# interfaces
.implements Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelephonyConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/TelephonyConnectionService;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/TelephonyConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnectionService$4;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public getDefaultPhone()Lcom/android/internal/telephony/Phone;
    .locals 1

    .prologue
    .line 218
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0
.end method

.method public getPhone(I)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 213
    invoke-static {p1}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0
.end method

.method public getPhones()[Lcom/android/internal/telephony/Phone;
    .locals 1

    .prologue
    .line 223
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0
.end method
