.class final Lcom/android/services/telephony/SimpleFeatures$2;
.super Ljava/lang/Object;
.source "SimpleFeatures.java"

# interfaces
.implements Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor$ExtraContainer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/services/telephony/SimpleFeatures;->accessMiuiConferenceExtra(Landroid/telecom/Conference;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$conference:Landroid/telecom/Conference;


# direct methods
.method constructor <init>(Landroid/telecom/Conference;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/SimpleFeatures$2;->val$conference:Landroid/telecom/Conference;

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/services/telephony/SimpleFeatures$2;->val$conference:Landroid/telecom/Conference;

    invoke-virtual {v0}, Landroid/telecom/Conference;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/services/telephony/SimpleFeatures$2;->val$conference:Landroid/telecom/Conference;

    invoke-virtual {v0}, Landroid/telecom/Conference;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "telephony."

    invoke-static {p1, v0, v1}, Lcom/android/phone/common/PhoneConstants;->copyMiuiCallExtras(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    .line 63
    iget-object v0, p0, Lcom/android/services/telephony/SimpleFeatures$2;->val$conference:Landroid/telecom/Conference;

    invoke-virtual {v0, p1}, Landroid/telecom/Conference;->setExtras(Landroid/os/Bundle;)V

    .line 64
    return-void
.end method
