.class final Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
.super Ljava/lang/Object;
.source "TelecomAccountRegistry.java"

# interfaces
.implements Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelecomAccountRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "AccountEntry"
.end annotation


# instance fields
.field private mAccount:Landroid/telecom/PhoneAccount;

.field private final mIncomingCallNotifier:Lcom/android/services/telephony/PstnIncomingCallNotifier;

.field private mIsDummy:Z

.field private mIsEmergency:Z

.field private mIsMergeCallSupported:Z

.field private mIsMergeImsCallSupported:Z

.field private mIsMergeOfWifiCallsAllowedWhenVoWifiOff:Z

.field private mIsVideoCapable:Z

.field private mIsVideoConferencingSupported:Z

.field private mIsVideoPauseSupported:Z

.field private mIsVideoPresenceSupported:Z

.field private final mPhone:Lcom/android/internal/telephony/Phone;

.field private final mPhoneCapabilitiesNotifier:Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;

.field final synthetic this$0:Lcom/android/services/telephony/TelecomAccountRegistry;


# direct methods
.method static synthetic -get0(Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;)Landroid/telecom/PhoneAccount;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mAccount:Landroid/telecom/PhoneAccount;

    return-object v0
.end method

.method constructor <init>(Lcom/android/services/telephony/TelecomAccountRegistry;Lcom/android/internal/telephony/Phone;ZZ)V
    .locals 4
    .param p1, "this$0"    # Lcom/android/services/telephony/TelecomAccountRegistry;
    .param p2, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p3, "isEmergency"    # Z
    .param p4, "isDummy"    # Z

    .prologue
    .line 105
    iput-object p1, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 107
    iput-boolean p3, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsEmergency:Z

    .line 108
    iput-boolean p4, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsDummy:Z

    .line 109
    invoke-direct {p0, p3, p4}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->registerPstnPhoneAccount(ZZ)Landroid/telecom/PhoneAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mAccount:Landroid/telecom/PhoneAccount;

    .line 110
    const-string/jumbo v0, "Registered phoneAccount: %s with handle: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 111
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mAccount:Landroid/telecom/PhoneAccount;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mAccount:Landroid/telecom/PhoneAccount;

    invoke-virtual {v2}, Landroid/telecom/PhoneAccount;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 110
    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    new-instance v0, Lcom/android/services/telephony/PstnIncomingCallNotifier;

    iget-object v1, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-direct {v0, v1}, Lcom/android/services/telephony/PstnIncomingCallNotifier;-><init>(Lcom/android/internal/telephony/Phone;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIncomingCallNotifier:Lcom/android/services/telephony/PstnIncomingCallNotifier;

    .line 113
    new-instance v0, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;

    iget-object v1, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-direct {v0, v1, p0}, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;-><init>(Lcom/android/internal/telephony/Phone;Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier$Listener;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhoneCapabilitiesNotifier:Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;

    .line 115
    return-void
.end method

.method private getPhoneAccountExtras()Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 438
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v4

    iget-object v5, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 441
    .local v0, "b":Landroid/os/PersistableBundle;
    const-string/jumbo v4, "carrier_instant_lettering_length_limit_int"

    .line 440
    invoke-virtual {v0, v4}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 443
    .local v2, "instantLetteringMaxLength":I
    const-string/jumbo v4, "carrier_instant_lettering_encoding_string"

    .line 442
    invoke-virtual {v0, v4}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 444
    .local v1, "instantLetteringEncoding":Ljava/lang/String;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 445
    .local v3, "phoneAccountExtras":Landroid/os/Bundle;
    const-string/jumbo v4, "android.telecom.extra.CALL_SUBJECT_MAX_LENGTH"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 447
    const-string/jumbo v4, "android.telecom.extra.CALL_SUBJECT_CHARACTER_ENCODING"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    return-object v3
.end method

.method private isCarrierEmergencyVideoCallsAllowed()Z
    .locals 3

    .prologue
    .line 400
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 401
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    .line 402
    const-string/jumbo v1, "allow_emergency_video_calls_bool"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 401
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isCarrierInstantLetteringSupported()Z
    .locals 3

    .prologue
    .line 365
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 366
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    .line 367
    const-string/jumbo v1, "carrier_instant_lettering_available_bool"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 366
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isCarrierMergeCallSupported()Z
    .locals 3

    .prologue
    .line 377
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 378
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    .line 379
    const-string/jumbo v1, "support_conference_call_bool"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 378
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isCarrierMergeImsCallSupported()Z
    .locals 3

    .prologue
    .line 389
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 390
    .local v0, "b":Landroid/os/PersistableBundle;
    const-string/jumbo v1, "support_ims_conference_call_bool"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private isCarrierMergeOfWifiCallsAllowedWhenVoWifiOff()Z
    .locals 3

    .prologue
    .line 426
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 427
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    .line 428
    const-string/jumbo v1, "allow_merge_wifi_calls_when_vowifi_off_bool"

    .line 427
    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isCarrierVideoConferencingSupported()Z
    .locals 3

    .prologue
    .line 412
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 413
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    .line 414
    const-string/jumbo v1, "support_video_conference_call_bool"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 413
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isCarrierVideoPauseSupported()Z
    .locals 3

    .prologue
    .line 340
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 341
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    .line 342
    const-string/jumbo v1, "support_pause_ims_video_calls_bool"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 341
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isCarrierVideoPresenceSupported()Z
    .locals 3

    .prologue
    .line 353
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 354
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    .line 355
    const-string/jumbo v1, "use_rcs_presence_bool"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 354
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private registerPstnPhoneAccount(ZZ)Landroid/telecom/PhoneAccount;
    .locals 37
    .param p1, "isEmergency"    # Z
    .param p2, "isDummyAccount"    # Z

    .prologue
    .line 126
    if-eqz p2, :cond_10

    const-string/jumbo v11, "Dummy "

    .line 131
    .local v11, "dummyPrefix":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v33, v0

    .line 130
    move-object/from16 v0, v33

    move/from16 v1, p1

    invoke-static {v0, v11, v1}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandleWithPrefix(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Z)Landroid/telecom/PhoneAccountHandle;

    move-result-object v23

    .line 134
    .local v23, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v29

    .line 135
    .local v29, "subId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/Phone;->getSubscriberId()Ljava/lang/String;

    move-result-object v31

    .line 136
    .local v31, "subscriberId":Ljava/lang/String;
    const/4 v8, 0x0

    .line 137
    .local v8, "color":I
    const/16 v26, -0x1

    .line 138
    .local v26, "slotId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get7(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/telephony/TelephonyManager;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getLine1Number(I)Ljava/lang/String;

    move-result-object v21

    .line 139
    .local v21, "line1Number":Ljava/lang/String;
    if-nez v21, :cond_0

    .line 140
    const-string/jumbo v21, ""

    .line 142
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/Phone;->getLine1Number()Ljava/lang/String;

    move-result-object v30

    .line 143
    .local v30, "subNumber":Ljava/lang/String;
    if-nez v30, :cond_1

    .line 144
    const-string/jumbo v30, ""

    .line 149
    :cond_1
    const/4 v15, 0x0

    .line 150
    .local v15, "icon":Landroid/graphics/drawable/Icon;
    const/16 v28, 0x0

    .line 155
    .local v28, "subDisplayName":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get5(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/telephony/SubscriptionManager;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v24

    .line 156
    .local v24, "record":Landroid/telephony/SubscriptionInfo;
    if-eqz v24, :cond_2

    .line 157
    invoke-virtual/range {v24 .. v24}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v28

    .line 159
    .end local v28    # "subDisplayName":Ljava/lang/CharSequence;
    :cond_2
    if-eqz p1, :cond_11

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    const v34, 0x7f0b04fa

    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 162
    .local v20, "label":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    const v34, 0x7f0b04fb

    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 202
    .end local v15    # "icon":Landroid/graphics/drawable/Icon;
    .end local v20    # "label":Ljava/lang/String;
    :goto_1
    const/16 v7, 0x26

    .line 206
    .local v7, "capabilities":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    const v34, 0x7f0e0026

    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v33

    if-eqz v33, :cond_3

    .line 207
    const/16 v7, 0x36

    .line 210
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/Phone;->isVideoEnabled()Z

    move-result v33

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoCapable:Z

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v33

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v34

    .line 212
    invoke-static/range {v33 .. v34}, Lcom/android/ims/ImsManager;->getInstance(Landroid/content/Context;I)Lcom/android/ims/ImsManager;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Lcom/android/ims/ImsManager;->isVtEnabledByPlatformForSlot()Z

    move-result v19

    .line 215
    .local v19, "isVideoEnabledByPlatform":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get3(Lcom/android/services/telephony/TelecomAccountRegistry;)Z

    move-result v33

    if-nez v33, :cond_4

    .line 216
    const-string/jumbo v33, "Disabling video calling for secondary user."

    const/16 v34, 0x0

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 217
    const/16 v33, 0x0

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoCapable:Z

    .line 218
    const/16 v19, 0x0

    .line 221
    .end local v19    # "isVideoEnabledByPlatform":Z
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoCapable:Z

    move/from16 v33, v0

    if-eqz v33, :cond_5

    .line 222
    or-int/lit8 v7, v7, 0x8

    .line 225
    :cond_5
    if-eqz v19, :cond_6

    .line 226
    or-int/lit16 v7, v7, 0x400

    .line 229
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isCarrierVideoPresenceSupported()Z

    move-result v33

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoPresenceSupported:Z

    .line 230
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoCapable:Z

    move/from16 v33, v0

    if-eqz v33, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoPresenceSupported:Z

    move/from16 v33, v0

    if-eqz v33, :cond_7

    .line 231
    or-int/lit16 v7, v7, 0x100

    .line 234
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoCapable:Z

    move/from16 v33, v0

    if-eqz v33, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isCarrierEmergencyVideoCallsAllowed()Z

    move-result v33

    if-eqz v33, :cond_8

    .line 235
    or-int/lit16 v7, v7, 0x200

    .line 238
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isCarrierVideoPauseSupported()Z

    move-result v33

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoPauseSupported:Z

    .line 239
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 240
    .local v12, "extras":Landroid/os/Bundle;
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isCarrierInstantLetteringSupported()Z

    move-result v33

    if-eqz v33, :cond_9

    .line 241
    or-int/lit8 v7, v7, 0x40

    .line 242
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->getPhoneAccountExtras()Landroid/os/Bundle;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v12, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 245
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    .line 246
    const v34, 0x7f0e0028

    .line 245
    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v17

    .line 247
    .local v17, "isHandoverFromSupported":Z
    if-eqz v17, :cond_a

    xor-int/lit8 v33, p1, 0x1

    if-eqz v33, :cond_a

    .line 250
    const-string/jumbo v33, "android.telecom.extra.SUPPORTS_HANDOVER_FROM"

    move-object/from16 v0, v33

    move/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 254
    :cond_a
    const-string/jumbo v33, "android.telecom.extra.SUPPORTS_VIDEO_CALLING_FALLBACK"

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v34

    .line 256
    const v35, 0x7f0e0029

    .line 255
    invoke-virtual/range {v34 .. v35}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v34

    .line 254
    move-object/from16 v0, v33

    move/from16 v1, v34

    invoke-virtual {v12, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 258
    const/16 v33, -0x1

    move/from16 v0, v26

    move/from16 v1, v33

    if-eq v0, v1, :cond_b

    .line 259
    const-string/jumbo v33, "android.telecom.extra.SORT_ORDER"

    .line 260
    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v34

    .line 259
    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-virtual {v12, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isCarrierMergeCallSupported()Z

    move-result v33

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsMergeCallSupported:Z

    .line 264
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isCarrierMergeImsCallSupported()Z

    move-result v33

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsMergeImsCallSupported:Z

    .line 265
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isCarrierVideoConferencingSupported()Z

    move-result v33

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoConferencingSupported:Z

    .line 267
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isCarrierMergeOfWifiCallsAllowedWhenVoWifiOff()Z

    move-result v33

    .line 266
    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsMergeOfWifiCallsAllowedWhenVoWifiOff:Z

    .line 269
    if-eqz p1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    .line 270
    const v34, 0x7f0e0025

    .line 269
    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v33

    if-eqz v33, :cond_c

    .line 271
    or-int/lit16 v7, v7, 0x80

    .line 274
    :cond_c
    if-nez v15, :cond_d

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 277
    .local v25, "res":Landroid/content/res/Resources;
    const v33, 0x7f020088

    const/16 v34, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 278
    .local v10, "drawable":Landroid/graphics/drawable/Drawable;
    const v33, 0x7f080076

    const/16 v34, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v33

    move/from16 v0, v33

    invoke-virtual {v10, v0}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 279
    sget-object v33, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, v33

    invoke-virtual {v10, v0}, Landroid/graphics/drawable/Drawable;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 281
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v32

    .line 282
    .local v32, "width":I
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v14

    .line 283
    .local v14, "height":I
    sget-object v33, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v32

    move-object/from16 v1, v33

    invoke-static {v0, v14, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 284
    .local v5, "bitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 285
    .local v6, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v6}, Landroid/graphics/Canvas;->getWidth()I

    move-result v33

    invoke-virtual {v6}, Landroid/graphics/Canvas;->getHeight()I

    move-result v34

    const/16 v35, 0x0

    const/16 v36, 0x0

    move/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v33

    move/from16 v3, v34

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 286
    invoke-virtual {v10, v6}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 288
    invoke-static {v5}, Landroid/graphics/drawable/Icon;->createWithBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Icon;

    move-result-object v15

    .line 292
    .end local v5    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "canvas":Landroid/graphics/Canvas;
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v14    # "height":I
    .end local v25    # "res":Landroid/content/res/Resources;
    .end local v32    # "width":I
    :cond_d
    const-string/jumbo v13, ""

    .line 293
    .local v13, "groupId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get7(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/telephony/TelephonyManager;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/telephony/TelephonyManager;->getMergedSubscriberIds()[Ljava/lang/String;

    move-result-object v22

    .line 294
    .local v22, "mergedImsis":[Ljava/lang/String;
    const/16 v18, 0x0

    .line 295
    .local v18, "isMergedSim":Z
    if-eqz v22, :cond_e

    if-eqz v31, :cond_e

    xor-int/lit8 v33, p1, 0x1

    if-eqz v33, :cond_e

    .line 296
    const/16 v33, 0x0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v34, v0

    :goto_2
    move/from16 v0, v33

    move/from16 v1, v34

    if-ge v0, v1, :cond_e

    aget-object v16, v22, v33

    .line 297
    .local v16, "imsi":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_17

    .line 298
    const/16 v18, 0x1

    .line 303
    .end local v16    # "imsi":Ljava/lang/String;
    :cond_e
    if-eqz v18, :cond_f

    .line 304
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "group_"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 305
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v34, "Adding Merged Account with group: "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-static {v13}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x0

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    :cond_f
    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/telecom/PhoneAccount;->builder(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    .line 309
    const-string/jumbo v34, "tel"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, v21

    move-object/from16 v2, v35

    invoke-static {v0, v1, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v34

    .line 308
    invoke-virtual/range {v33 .. v34}, Landroid/telecom/PhoneAccount$Builder;->setAddress(Landroid/net/Uri;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    .line 311
    const-string/jumbo v34, "tel"

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    move-object/from16 v2, v35

    invoke-static {v0, v1, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v34

    .line 308
    invoke-virtual/range {v33 .. v34}, Landroid/telecom/PhoneAccount$Builder;->setSubscriptionAddress(Landroid/net/Uri;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Landroid/telecom/PhoneAccount$Builder;->setCapabilities(I)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v15}, Landroid/telecom/PhoneAccount$Builder;->setIcon(Landroid/graphics/drawable/Icon;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v8}, Landroid/telecom/PhoneAccount$Builder;->setHighlightColor(I)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v9}, Landroid/telecom/PhoneAccount$Builder;->setShortDescription(Ljava/lang/CharSequence;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    .line 316
    const/16 v34, 0x2

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v34, v0

    .line 317
    const-string/jumbo v35, "tel"

    const/16 v36, 0x0

    aput-object v35, v34, v36

    const-string/jumbo v35, "voicemail"

    const/16 v36, 0x1

    aput-object v35, v34, v36

    .line 316
    invoke-static/range {v34 .. v34}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    .line 308
    invoke-virtual/range {v33 .. v34}, Landroid/telecom/PhoneAccount$Builder;->setSupportedUriSchemes(Ljava/util/List;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v12}, Landroid/telecom/PhoneAccount$Builder;->setExtras(Landroid/os/Bundle;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Landroid/telecom/PhoneAccount$Builder;->setGroupId(Ljava/lang/String;)Landroid/telecom/PhoneAccount$Builder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/telecom/PhoneAccount$Builder;->build()Landroid/telecom/PhoneAccount;

    move-result-object v4

    .line 323
    .local v4, "account":Landroid/telecom/PhoneAccount;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get6(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/telecom/TelecomManager;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Landroid/telecom/TelecomManager;->registerPhoneAccount(Landroid/telecom/PhoneAccount;)V

    .line 325
    return-object v4

    .line 126
    .end local v4    # "account":Landroid/telecom/PhoneAccount;
    .end local v7    # "capabilities":I
    .end local v8    # "color":I
    .end local v11    # "dummyPrefix":Ljava/lang/String;
    .end local v12    # "extras":Landroid/os/Bundle;
    .end local v13    # "groupId":Ljava/lang/String;
    .end local v17    # "isHandoverFromSupported":Z
    .end local v18    # "isMergedSim":Z
    .end local v21    # "line1Number":Ljava/lang/String;
    .end local v22    # "mergedImsis":[Ljava/lang/String;
    .end local v23    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    .end local v24    # "record":Landroid/telephony/SubscriptionInfo;
    .end local v26    # "slotId":I
    .end local v29    # "subId":I
    .end local v30    # "subNumber":Ljava/lang/String;
    .end local v31    # "subscriberId":Ljava/lang/String;
    :cond_10
    const-string/jumbo v11, ""

    .restart local v11    # "dummyPrefix":Ljava/lang/String;
    goto/16 :goto_0

    .line 163
    .restart local v8    # "color":I
    .restart local v15    # "icon":Landroid/graphics/drawable/Icon;
    .restart local v21    # "line1Number":Ljava/lang/String;
    .restart local v23    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    .restart local v24    # "record":Landroid/telephony/SubscriptionInfo;
    .restart local v26    # "slotId":I
    .restart local v29    # "subId":I
    .restart local v30    # "subNumber":Ljava/lang/String;
    .restart local v31    # "subscriberId":Ljava/lang/String;
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get7(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/telephony/TelephonyManager;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v33

    const/16 v34, 0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_13

    .line 167
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v33

    if-nez v33, :cond_12

    .line 168
    invoke-interface/range {v28 .. v28}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    .restart local v20    # "label":Ljava/lang/String;
    move-object/from16 v9, v20

    .local v9, "description":Ljava/lang/String;
    goto/16 :goto_1

    .line 170
    .end local v9    # "description":Ljava/lang/String;
    .end local v20    # "label":Ljava/lang/String;
    :cond_12
    const/16 v20, 0x0

    .local v20, "label":Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "description":Ljava/lang/String;
    goto/16 :goto_1

    .line 173
    .end local v9    # "description":Ljava/lang/String;
    .end local v20    # "label":Ljava/lang/String;
    :cond_13
    if-eqz v24, :cond_14

    .line 174
    invoke-virtual/range {v24 .. v24}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v28

    .line 175
    .local v28, "subDisplayName":Ljava/lang/CharSequence;
    invoke-virtual/range {v24 .. v24}, Landroid/telephony/SubscriptionInfo;->getSimSlotIndex()I

    move-result v26

    .line 176
    invoke-virtual/range {v24 .. v24}, Landroid/telephony/SubscriptionInfo;->getIconTint()I

    move-result v8

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v33

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionInfo;->createIconBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Landroid/graphics/drawable/Icon;->createWithBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Icon;

    move-result-object v15

    .line 181
    .end local v15    # "icon":Landroid/graphics/drawable/Icon;
    .end local v28    # "subDisplayName":Ljava/lang/CharSequence;
    :cond_14
    invoke-static/range {v26 .. v26}, Landroid/telephony/SubscriptionManager;->isValidSlotIndex(I)Z

    move-result v33

    if-eqz v33, :cond_16

    .line 182
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    .line 187
    .local v27, "slotIdString":Ljava/lang/String;
    :goto_3
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v33

    if-eqz v33, :cond_15

    .line 189
    const-string/jumbo v33, "Could not get a display name for subid: %d"

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    const/16 v36, 0x0

    aput-object v35, v34, v36

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v34, v0

    .line 191
    const/16 v35, 0x0

    aput-object v27, v34, v35

    const v35, 0x7f0b04fc

    .line 190
    move-object/from16 v0, v33

    move/from16 v1, v35

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    .line 196
    :cond_15
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v33

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 197
    .local v20, "label":Ljava/lang/String;
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v33

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v34

    const/16 v35, 0x1

    move/from16 v0, v35

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v35, v0

    .line 198
    const/16 v36, 0x0

    aput-object v27, v35, v36

    const v36, 0x7f0b04fc

    .line 197
    move-object/from16 v0, v34

    move/from16 v1, v36

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .local v9, "description":Ljava/lang/String;
    goto/16 :goto_1

    .line 184
    .end local v9    # "description":Ljava/lang/String;
    .end local v20    # "label":Ljava/lang/String;
    .end local v27    # "slotIdString":Ljava/lang/String;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v33

    const v34, 0x7f0b0094

    invoke-virtual/range {v33 .. v34}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v27

    .restart local v27    # "slotIdString":Ljava/lang/String;
    goto/16 :goto_3

    .line 296
    .end local v27    # "slotIdString":Ljava/lang/String;
    .restart local v7    # "capabilities":I
    .restart local v12    # "extras":Landroid/os/Bundle;
    .restart local v13    # "groupId":Ljava/lang/String;
    .restart local v16    # "imsi":Ljava/lang/String;
    .restart local v17    # "isHandoverFromSupported":Z
    .restart local v18    # "isMergedSim":Z
    .restart local v22    # "mergedImsis":[Ljava/lang/String;
    :cond_17
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 329
    iget-object v1, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mAccount:Landroid/telecom/PhoneAccount;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mAccount:Landroid/telecom/PhoneAccount;

    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public isMergeCallSupported()Z
    .locals 1

    .prologue
    .line 487
    iget-boolean v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsMergeCallSupported:Z

    return v0
.end method

.method public isMergeImsCallSupported()Z
    .locals 1

    .prologue
    .line 495
    iget-boolean v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsMergeImsCallSupported:Z

    return v0
.end method

.method public isMergeOfWifiCallsAllowedWhenVoWifiOff()Z
    .locals 1

    .prologue
    .line 511
    iget-boolean v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsMergeOfWifiCallsAllowedWhenVoWifiOff:Z

    return v0
.end method

.method public isVideoConferencingSupported()Z
    .locals 1

    .prologue
    .line 503
    iget-boolean v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoConferencingSupported:Z

    return v0
.end method

.method public isVideoPauseSupported()Z
    .locals 1

    .prologue
    .line 479
    iget-boolean v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoCapable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoPauseSupported:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onVideoCapabilitiesChanged(Z)V
    .locals 3
    .param p1, "isVideoCapable"    # Z

    .prologue
    .line 460
    iput-boolean p1, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsVideoCapable:Z

    .line 461
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    invoke-static {v0}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get1(Lcom/android/services/telephony/TelecomAccountRegistry;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 462
    :try_start_0
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->this$0:Lcom/android/services/telephony/TelecomAccountRegistry;

    invoke-static {v0}, Lcom/android/services/telephony/TelecomAccountRegistry;->-get0(Lcom/android/services/telephony/TelecomAccountRegistry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v1

    .line 467
    return-void

    .line 469
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsEmergency:Z

    iget-boolean v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIsDummy:Z

    invoke-direct {p0, v0, v2}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->registerPstnPhoneAccount(ZZ)Landroid/telecom/PhoneAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mAccount:Landroid/telecom/PhoneAccount;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    .line 471
    return-void

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method teardown()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mIncomingCallNotifier:Lcom/android/services/telephony/PstnIncomingCallNotifier;

    invoke-virtual {v0}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->teardown()V

    .line 119
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->mPhoneCapabilitiesNotifier:Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;

    invoke-virtual {v0}, Lcom/android/services/telephony/PstnPhoneCapabilitiesNotifier;->teardown()V

    .line 120
    return-void
.end method
