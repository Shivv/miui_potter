.class final Lcom/android/services/telephony/TelephonyConferenceController;
.super Ljava/lang/Object;
.source "TelephonyConferenceController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/TelephonyConferenceController$1;
    }
.end annotation


# instance fields
.field private final mConnectionListener:Landroid/telecom/Connection$Listener;

.field private final mConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

.field private mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

.field private final mTelephonyConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/services/telephony/TelephonyConnection;",
            ">;"
        }
    .end annotation
.end field

.field private mTriggerRecalculate:Z


# direct methods
.method public constructor <init>(Lcom/android/services/telephony/TelephonyConnectionServiceProxy;)V
    .locals 1
    .param p1, "connectionService"    # Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lcom/android/services/telephony/TelephonyConferenceController$1;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelephonyConferenceController$1;-><init>(Lcom/android/services/telephony/TelephonyConferenceController;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTriggerRecalculate:Z

    .line 75
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    .line 76
    return-void
.end method

.method private isFullConference(Landroid/telecom/Conference;)Z
    .locals 2
    .param p1, "conference"    # Landroid/telecom/Conference;

    .prologue
    .line 114
    invoke-virtual {p1}, Landroid/telecom/Conference;->getConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$-com_android_services_telephony_TelephonyConferenceController_6074(Landroid/telecom/Connection;Landroid/telecom/Connection;)Z
    .locals 1
    .param p0, "c"    # Landroid/telecom/Connection;
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 157
    if-eq p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$-com_android_services_telephony_TelephonyConferenceController_6731(Lcom/android/services/telephony/TelephonyConnection;)Z
    .locals 2
    .param p0, "c"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    const/4 v0, 0x0

    .line 170
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->isConferenceSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getConference()Landroid/telecom/Conference;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private participatesInFullConference(Landroid/telecom/Connection;)Z
    .locals 1
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 118
    invoke-virtual {p1}, Landroid/telecom/Connection;->getConference()Landroid/telecom/Conference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p1}, Landroid/telecom/Connection;->getConference()Landroid/telecom/Conference;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/services/telephony/TelephonyConferenceController;->isFullConference(Landroid/telecom/Conference;)Z

    move-result v0

    .line 118
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private recalculateConference()V
    .locals 21

    .prologue
    .line 183
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 184
    .local v7, "conferencedConnections":Ljava/util/Set;, "Ljava/util/Set<Landroid/telecom/Connection;>;"
    const/4 v12, 0x0

    .line 186
    .local v12, "numGsmConnections":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "connection$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/services/telephony/TelephonyConnection;

    .line 188
    .local v9, "connection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual {v9}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v14

    .line 189
    .local v14, "radioConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v14, :cond_0

    .line 190
    invoke-virtual {v14}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v15

    .line 191
    .local v15, "state":Lcom/android/internal/telephony/Call$State;
    invoke-virtual {v14}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v5

    .line 192
    .local v5, "call":Lcom/android/internal/telephony/Call;
    sget-object v17, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    move-object/from16 v0, v17

    if-eq v15, v0, :cond_1

    sget-object v17, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    move-object/from16 v0, v17

    if-ne v15, v0, :cond_0

    .line 193
    :cond_1
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/android/internal/telephony/Call;->isMultiparty()Z

    move-result v17

    .line 192
    if-eqz v17, :cond_0

    .line 194
    add-int/lit8 v12, v12, 0x1

    .line 195
    invoke-interface {v7, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 200
    .end local v5    # "call":Lcom/android/internal/telephony/Call;
    .end local v9    # "connection":Lcom/android/services/telephony/TelephonyConnection;
    .end local v14    # "radioConnection":Lcom/android/internal/telephony/Connection;
    .end local v15    # "state":Lcom/android/internal/telephony/Call$State;
    :cond_2
    const-string/jumbo v17, "Recalculate conference calls %s %s."

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v19, v18, v20

    const/16 v19, 0x1

    aput-object v7, v18, v19

    .line 200
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    const/4 v3, 0x1

    .line 205
    .local v3, "allConnInService":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/android/services/telephony/TelephonyConnectionServiceProxy;->getAllConnections()Ljava/util/Collection;

    move-result-object v4

    .line 206
    .local v4, "allConnections":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/telecom/Connection;>;"
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telecom/Connection;

    .line 207
    .local v8, "connection":Landroid/telecom/Connection;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Finding connection in Connection Service for "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208
    invoke-interface {v4, v8}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 209
    const/4 v3, 0x0

    .line 210
    const-string/jumbo v17, "Finding connection in Connection Service Failed"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 215
    .end local v8    # "connection":Landroid/telecom/Connection;
    :cond_4
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Is there a match for all connections in connection service "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    const/16 v17, 0x2

    move/from16 v0, v17

    if-ge v12, v0, :cond_6

    .line 221
    const-string/jumbo v17, "not enough connections to be a conference!"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    if-eqz v17, :cond_5

    .line 225
    const-string/jumbo v17, "with a conference to destroy!"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/android/services/telephony/TelephonyConference;->destroy()V

    .line 227
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    .line 294
    :cond_5
    :goto_1
    return-void

    .line 230
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    if-eqz v17, :cond_c

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/android/services/telephony/TelephonyConference;->getConnections()Ljava/util/List;

    move-result-object v11

    .line 233
    .local v11, "existingConnections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    invoke-interface {v11}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_7
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telecom/Connection;

    .line 234
    .restart local v8    # "connection":Landroid/telecom/Connection;
    instance-of v0, v8, Lcom/android/services/telephony/TelephonyConnection;

    move/from16 v17, v0

    if-eqz v17, :cond_7

    .line 235
    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v17

    xor-int/lit8 v17, v17, 0x1

    .line 234
    if-eqz v17, :cond_7

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/android/services/telephony/TelephonyConference;->removeConnection(Landroid/telecom/Connection;)V

    goto :goto_2

    .line 239
    .end local v8    # "connection":Landroid/telecom/Connection;
    :cond_8
    if-eqz v3, :cond_a

    .line 240
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelephonyConferenceController;->mTriggerRecalculate:Z

    .line 242
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_9
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telecom/Connection;

    .line 243
    .restart local v8    # "connection":Landroid/telecom/Connection;
    invoke-interface {v11, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_9

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/android/services/telephony/TelephonyConference;->addConnection(Landroid/telecom/Connection;)Z

    goto :goto_3

    .line 248
    .end local v8    # "connection":Landroid/telecom/Connection;
    :cond_a
    const-string/jumbo v17, "Trigger recalculate later"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelephonyConferenceController;->mTriggerRecalculate:Z

    .line 277
    .end local v11    # "existingConnections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    :cond_b
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    if-eqz v17, :cond_5

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/android/services/telephony/TelephonyConference;->getPrimaryConnection()Landroid/telecom/Connection;

    move-result-object v6

    .line 279
    .local v6, "conferencedConnection":Landroid/telecom/Connection;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "Primary Conferenced connection is "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 280
    if-eqz v6, :cond_5

    .line 281
    invoke-virtual {v6}, Landroid/telecom/Connection;->getState()I

    move-result v17

    packed-switch v17, :pswitch_data_0

    goto/16 :goto_1

    .line 283
    :pswitch_0
    const-string/jumbo v17, "Setting conference to active"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/android/services/telephony/TelephonyConference;->setActive()V

    goto/16 :goto_1

    .line 252
    .end local v6    # "conferencedConnection":Landroid/telecom/Connection;
    :cond_c
    if-eqz v3, :cond_f

    .line 253
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelephonyConferenceController;->mTriggerRecalculate:Z

    .line 257
    const/4 v13, 0x0

    .line 258
    .local v13, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_d

    .line 260
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/services/telephony/TelephonyConnection;

    .line 262
    .local v16, "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual/range {v16 .. v16}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v17

    .line 261
    invoke-static/range {v17 .. v17}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v13

    .line 265
    .end local v13    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    .end local v16    # "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_d
    new-instance v17, Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v0, v17

    invoke-direct {v0, v13}, Lcom/android/services/telephony/TelephonyConference;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    .line 266
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_e

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telecom/Connection;

    .line 267
    .restart local v8    # "connection":Landroid/telecom/Connection;
    const-string/jumbo v17, "Adding a connection to a conference call: %s %s"

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v19, v18, v20

    const/16 v19, 0x1

    aput-object v8, v18, v19

    .line 267
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/android/services/telephony/TelephonyConference;->addConnection(Landroid/telecom/Connection;)Z

    goto :goto_5

    .line 271
    .end local v8    # "connection":Landroid/telecom/Connection;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Lcom/android/services/telephony/TelephonyConnectionServiceProxy;->addConference(Lcom/android/services/telephony/TelephonyConference;)V

    goto/16 :goto_4

    .line 273
    :cond_f
    const-string/jumbo v17, "Trigger recalculate later"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/services/telephony/TelephonyConferenceController;->mTriggerRecalculate:Z

    goto/16 :goto_4

    .line 287
    .restart local v6    # "conferencedConnection":Landroid/telecom/Connection;
    :pswitch_1
    const-string/jumbo v17, "Setting conference to hold"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/android/services/telephony/TelephonyConference;->setOnHold()V

    goto/16 :goto_1

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private recalculateConferenceable()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 126
    const-string/jumbo v7, "recalculateConferenceable : %d"

    new-array v8, v11, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {p0, v7, v8}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    new-instance v2, Ljava/util/HashSet;

    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v2, v7}, Ljava/util/HashSet;-><init>(I)V

    .line 130
    .local v2, "conferenceableConnections":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/telecom/Connection;>;"
    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "connection$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/services/telephony/TelephonyConnection;

    .line 131
    .local v3, "connection":Lcom/android/services/telephony/TelephonyConnection;
    const-string/jumbo v7, "recalc - %s %s supportsConf? %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    aput-object v3, v8, v11

    .line 132
    invoke-virtual {v3}, Lcom/android/services/telephony/TelephonyConnection;->isConferenceSupported()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v12

    .line 131
    invoke-static {p0, v7, v8}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    invoke-virtual {v3}, Lcom/android/services/telephony/TelephonyConnection;->isConferenceSupported()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-direct {p0, v3}, Lcom/android/services/telephony/TelephonyConferenceController;->participatesInFullConference(Landroid/telecom/Connection;)Z

    move-result v7

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_0

    .line 135
    invoke-virtual {v3}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 146
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/android/services/telephony/TelephonyConnection;->setConferenceableConnections(Ljava/util/List;)V

    goto :goto_0

    .line 139
    :pswitch_0
    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    .end local v3    # "connection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "conferenceable: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {p0, v7, v8}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "c$iterator":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 154
    .local v0, "c":Landroid/telecom/Connection;
    invoke-virtual {v2}, Ljava/util/HashSet;->stream()Ljava/util/stream/Stream;

    move-result-object v7

    .line 157
    new-instance v8, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;

    invoke-direct {v8, v12, v0}, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;-><init>(BLjava/lang/Object;)V

    .line 154
    invoke-interface {v7, v8}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v7

    .line 158
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v8

    .line 154
    invoke-interface {v7, v8}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 159
    .local v5, "connections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    invoke-virtual {v0, v5}, Landroid/telecom/Connection;->setConferenceableConnections(Ljava/util/List;)V

    goto :goto_1

    .line 164
    .end local v0    # "c":Landroid/telecom/Connection;
    .end local v5    # "connections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    :cond_2
    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    if-eqz v7, :cond_3

    .line 165
    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    invoke-direct {p0, v7}, Lcom/android/services/telephony/TelephonyConferenceController;->isFullConference(Landroid/telecom/Conference;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 166
    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v7

    sget-object v8, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;->$INST$1:Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;

    invoke-interface {v7, v8}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v7

    .line 171
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v8

    .line 166
    invoke-interface {v7, v8}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 172
    .local v6, "nonConferencedConnections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    invoke-virtual {v7, v6}, Lcom/android/services/telephony/TelephonyConference;->setConferenceableConnections(Ljava/util/List;)V

    .line 180
    .end local v6    # "nonConferencedConnections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    :cond_3
    :goto_2
    return-void

    .line 174
    :cond_4
    const-string/jumbo v7, "cannot merge anymore due it is full"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {p0, v7, v8}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    iget-object v7, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConference:Lcom/android/services/telephony/TelephonyConference;

    .line 176
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    .line 175
    invoke-virtual {v7, v8}, Lcom/android/services/telephony/TelephonyConference;->setConferenceableConnections(Ljava/util/List;)V

    goto :goto_2

    .line 135
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method add(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 3
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const-string/jumbo v0, "add - connection already tracked; connection=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {p1, v0}, Lcom/android/services/telephony/TelephonyConnection;->addConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 93
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConferenceController;->recalculate()V

    .line 94
    return-void
.end method

.method recalculate()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConferenceController;->recalculateConference()V

    .line 110
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConferenceController;->recalculateConferenceable()V

    .line 111
    return-void
.end method

.method remove(Landroid/telecom/Connection;)V
    .locals 3
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    const-string/jumbo v0, "remove - connection not tracked; connection=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mConnectionListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {p1, v0}, Landroid/telecom/Connection;->removeConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 104
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTelephonyConnections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 105
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConferenceController;->recalculate()V

    .line 106
    return-void
.end method

.method shouldRecalculate()Z
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "shouldRecalculate is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTriggerRecalculate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    iget-boolean v0, p0, Lcom/android/services/telephony/TelephonyConferenceController;->mTriggerRecalculate:Z

    return v0
.end method
