.class Lcom/android/services/telephony/TelephonyConnection$1;
.super Landroid/os/Handler;
.source "TelephonyConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelephonyConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/TelephonyConnection;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    .line 131
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 26
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 134
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v22, v0

    packed-switch v22, :pswitch_data_0

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 136
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const-string/jumbo v23, "MSG_PRECISE_CALL_STATE_CHANGED"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->updateState()V

    goto :goto_0

    .line 140
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const-string/jumbo v23, "MSG_HANDOVER_STATE_CHANGED"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Landroid/os/AsyncResult;

    .line 143
    .local v3, "ar":Landroid/os/AsyncResult;
    iget-object v6, v3, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v6, Lcom/android/internal/telephony/Connection;

    .line 144
    .local v6, "connection":Lcom/android/internal/telephony/Connection;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    move-object/from16 v22, v0

    if-eqz v22, :cond_6

    .line 145
    if-eqz v6, :cond_0

    .line 146
    invoke-virtual {v6}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_1

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_1

    .line 148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v22

    invoke-virtual {v6}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    .line 146
    if-nez v22, :cond_2

    .line 149
    :cond_1
    invoke-virtual {v6}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/Connection;->getStateBeforeHandover()Lcom/android/internal/telephony/Call$State;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-ne v0, v1, :cond_0

    .line 150
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    .line 151
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "SettingOriginalConnection "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/telephony/Connection;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    .line 152
    const-string/jumbo v24, " with "

    .line 151
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    .line 152
    invoke-virtual {v6}, Lcom/android/internal/telephony/Connection;->toString()Ljava/lang/String;

    move-result-object v24

    .line 151
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 150
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v22

    if-eqz v22, :cond_4

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v22

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v23

    const-string/jumbo v24, "config_show_srvcc_toast"

    .line 154
    invoke-static/range {v22 .. v24}, Lorg/codeaurora/ims/utils/QtiImsExtUtils;->isCarrierConfigEnabled(ILandroid/content/Context;Ljava/lang/String;)Z

    move-result v12

    .line 157
    :goto_1
    const/4 v12, 0x0

    .line 158
    .local v12, "isShowToast":Z
    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->shouldTreatAsEmergencyCall()Z

    move-result v22

    xor-int/lit8 v22, v22, 0x1

    if-eqz v22, :cond_3

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/Connection;->getVideoState()I

    move-result v22

    .line 159
    invoke-static/range {v22 .. v22}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 161
    const v18, 0x7f0b02da

    .line 162
    .local v18, "srvccMessageRes":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v22

    .line 163
    const/16 v23, 0x1

    .line 162
    move-object/from16 v0, v22

    move/from16 v1, v18

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/widget/Toast;->show()V

    .line 165
    .end local v18    # "srvccMessageRes":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Lcom/android/services/telephony/TelephonyConnection;->setOriginalConnection(Lcom/android/internal/telephony/Connection;)V

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static/range {v22 .. v23}, Lcom/android/services/telephony/TelephonyConnection;->-set1(Lcom/android/services/telephony/TelephonyConnection;Z)Z

    goto/16 :goto_0

    .line 155
    .end local v12    # "isShowToast":Z
    :cond_4
    const/4 v12, 0x0

    .restart local v12    # "isShowToast":Z
    goto :goto_1

    .line 161
    :cond_5
    const v18, 0x7f0b02db

    .restart local v18    # "srvccMessageRes":I
    goto :goto_2

    .line 169
    .end local v12    # "isShowToast":Z
    .end local v18    # "srvccMessageRes":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    .line 170
    const-string/jumbo v23, "MSG_HANDOVER_STATE_CHANGED: mOriginalConnection==null - invalid state (not cleaned up)"

    .line 169
    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 174
    .end local v3    # "ar":Landroid/os/AsyncResult;
    .end local v6    # "connection":Lcom/android/internal/telephony/Connection;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const-string/jumbo v23, "MSG_RINGBACK_TONE"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/android/services/telephony/TelephonyConnection;->-wrap0(Lcom/android/services/telephony/TelephonyConnection;)Lcom/android/internal/telephony/Connection;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_7

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const-string/jumbo v23, "handleMessage, original connection is not foreground connection, skipping"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180
    return-void

    .line 182
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Landroid/os/AsyncResult;

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Boolean;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v22

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->setRingbackRequested(Z)V

    goto/16 :goto_0

    .line 185
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->updateState()V

    goto/16 :goto_0

    .line 188
    :pswitch_4
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Boolean;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 189
    .local v11, "isMultiParty":Z
    const-string/jumbo v23, "Update multiparty state to %s"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    if-eqz v11, :cond_8

    const-string/jumbo v22, "Y"

    :goto_3
    const/16 v25, 0x0

    aput-object v22, v24, v25

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v0, v11}, Lcom/android/services/telephony/TelephonyConnection;->-set0(Lcom/android/services/telephony/TelephonyConnection;Z)Z

    .line 191
    if-eqz v11, :cond_0

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->-wrap5(Lcom/android/services/telephony/TelephonyConnection;)V

    goto/16 :goto_0

    .line 189
    :cond_8
    const-string/jumbo v22, "N"

    goto :goto_3

    .line 196
    .end local v11    # "isMultiParty":Z
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->-wrap4(Lcom/android/services/telephony/TelephonyConnection;)V

    goto/16 :goto_0

    .line 200
    :pswitch_6
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v21

    .line 201
    .local v21, "videoState":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->setVideoState(I)V

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->-wrap6(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->-wrap7(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->-wrap8(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->updateConnectionProperties()V

    goto/16 :goto_0

    .line 213
    .end local v21    # "videoState":I
    :pswitch_7
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Landroid/telecom/Connection$VideoProvider;

    .line 214
    .local v20, "videoProvider":Landroid/telecom/Connection$VideoProvider;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->setVideoProvider(Landroid/telecom/Connection$VideoProvider;)V

    goto/16 :goto_0

    .line 218
    .end local v20    # "videoProvider":Landroid/telecom/Connection$VideoProvider;
    :pswitch_8
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 219
    .local v5, "audioQuality":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lcom/android/services/telephony/TelephonyConnection;->setAudioQuality(I)V

    goto/16 :goto_0

    .line 223
    .end local v5    # "audioQuality":I
    :pswitch_9
    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v13, Ljava/util/List;

    .line 224
    .local v13, "participants":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/ConferenceParticipant;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v0, v13}, Lcom/android/services/telephony/TelephonyConnection;->-wrap10(Lcom/android/services/telephony/TelephonyConnection;Ljava/util/List;)V

    goto/16 :goto_0

    .line 228
    .end local v13    # "participants":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/ConferenceParticipant;>;"
    :pswitch_a
    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Landroid/os/Bundle;

    .line 229
    .local v7, "extras":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Lcom/android/services/telephony/TelephonyConnection;->updateExtras(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 233
    .end local v7    # "extras":Landroid/os/Bundle;
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/android/services/telephony/TelephonyConnection;->setOriginalConnectionCapabilities(I)V

    goto/16 :goto_0

    .line 237
    :pswitch_c
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Landroid/os/AsyncResult;

    .line 240
    .local v4, "asyncResult":Landroid/os/AsyncResult;
    iget-object v10, v4, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    .line 239
    check-cast v10, Landroid/util/Pair;

    .line 243
    .local v10, "heldInfo":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/android/internal/telephony/Connection;Ljava/lang/Boolean;>;"
    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Boolean;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    .line 246
    .local v15, "playTone":Z
    iget-object v9, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v9, Lcom/android/internal/telephony/Connection;

    .line 250
    .local v9, "heldConnection":Lcom/android/internal/telephony/Connection;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection;->mOriginalConnection:Lcom/android/internal/telephony/Connection;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    if-ne v9, v0, :cond_0

    .line 253
    if-eqz v15, :cond_9

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const-string/jumbo v23, "android.telecom.event.ON_HOLD_TONE_START"

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Lcom/android/services/telephony/TelephonyConnection;->sendConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 256
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const-string/jumbo v23, "android.telecom.event.ON_HOLD_TONE_END"

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Lcom/android/services/telephony/TelephonyConnection;->sendConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 262
    .end local v4    # "asyncResult":Landroid/os/AsyncResult;
    .end local v9    # "heldConnection":Lcom/android/internal/telephony/Connection;
    .end local v10    # "heldInfo":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/android/internal/telephony/Connection;Ljava/lang/Boolean;>;"
    .end local v15    # "playTone":Z
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v22

    if-eqz v22, :cond_0

    .line 265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v14

    .line 266
    .local v14, "phoneId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "MSG_SUPP_SERVICE_NOTIFY on phoneId : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    const-string/jumbo v22, "MSG_SUPP_SERVICE_NOTIFY event processing failed"

    .line 304
    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Landroid/os/AsyncResult;

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->-wrap3(Lcom/android/services/telephony/TelephonyConnection;Landroid/os/AsyncResult;)V

    goto/16 :goto_0

    .line 313
    .end local v14    # "phoneId":I
    :pswitch_e
    const-string/jumbo v22, "MSG_CDMA_VOICE_PRIVACY_ON received"

    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    invoke-static/range {v22 .. v23}, Lcom/android/services/telephony/TelephonyConnection;->-wrap9(Lcom/android/services/telephony/TelephonyConnection;Z)V

    goto/16 :goto_0

    .line 317
    :pswitch_f
    const-string/jumbo v22, "MSG_CDMA_VOICE_PRIVACY_OFF received"

    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static/range {v22 .. v23}, Lcom/android/services/telephony/TelephonyConnection;->-wrap9(Lcom/android/services/telephony/TelephonyConnection;Z)V

    goto/16 :goto_0

    .line 321
    :pswitch_10
    const-string/jumbo v22, "MSG_CONNECTION_REMOVED"

    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/android/services/telephony/TelephonyConnection;->-wrap7(Lcom/android/services/telephony/TelephonyConnection;)V

    goto/16 :goto_0

    .line 327
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelephonyConnection$1;->this$0:Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v22, v0

    const-string/jumbo v23, "MSG_SUPP_SERVICE_FAILED"

    const/16 v24, 0x0

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 328
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v16, v0

    check-cast v16, Landroid/os/AsyncResult;

    .line 329
    .local v16, "r":Landroid/os/AsyncResult;
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Lcom/android/internal/telephony/PhoneInternalInterface$SuppService;

    .line 330
    .local v17, "service":Lcom/android/internal/telephony/PhoneInternalInterface$SuppService;
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/PhoneInternalInterface$SuppService;->ordinal()I

    move-result v19

    .line 331
    .local v19, "val":I
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 332
    .local v8, "failure":Landroid/content/Intent;
    const-string/jumbo v22, "org.codeaurora.ACTION_SUPP_SERVICE_FAILURE"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    const-string/jumbo v22, "supp_serv_failure"

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 334
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v22

    sget-object v23, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v8, v1}, Lcom/android/phone/PhoneGlobals;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto/16 :goto_0

    .line 134
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_d
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method
