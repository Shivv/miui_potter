.class public Lcom/android/services/telephony/ConferenceParticipantConnection;
.super Landroid/telecom/Connection;
.source "ConferenceParticipantConnection.java"


# instance fields
.field private final mEndpoint:Landroid/net/Uri;

.field private final mParentConnection:Lcom/android/internal/telephony/Connection;

.field private final mUserEntity:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/android/internal/telephony/Connection;Landroid/telecom/ConferenceParticipant;)V
    .locals 4
    .param p1, "parentConnection"    # Lcom/android/internal/telephony/Connection;
    .param p2, "participant"    # Landroid/telecom/ConferenceParticipant;

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/telecom/Connection;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/android/services/telephony/ConferenceParticipantConnection;->mParentConnection:Lcom/android/internal/telephony/Connection;

    .line 68
    invoke-direct {p0, p2}, Lcom/android/services/telephony/ConferenceParticipantConnection;->getParticipantPresentation(Landroid/telecom/ConferenceParticipant;)I

    move-result v2

    .line 70
    .local v2, "presentation":I
    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 71
    const/4 v0, 0x0

    .line 76
    :goto_0
    invoke-virtual {p0, v0, v2}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setAddress(Landroid/net/Uri;I)V

    .line 77
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getVideoState()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setVideoState(I)V

    .line 78
    invoke-virtual {p2}, Landroid/telecom/ConferenceParticipant;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setCallerDisplayName(Ljava/lang/String;I)V

    .line 80
    invoke-virtual {p2}, Landroid/telecom/ConferenceParticipant;->getHandle()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/android/services/telephony/ConferenceParticipantConnection;->mUserEntity:Landroid/net/Uri;

    .line 81
    invoke-virtual {p2}, Landroid/telecom/ConferenceParticipant;->getEndpoint()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/android/services/telephony/ConferenceParticipantConnection;->mEndpoint:Landroid/net/Uri;

    .line 83
    invoke-direct {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setCapabilities()V

    .line 84
    return-void

    .line 73
    :cond_0
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/services/telephony/ConferenceParticipantConnection;->getCountryIso(Lcom/android/internal/telephony/Phone;)Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "countryIso":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/telecom/ConferenceParticipant;->getHandle()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/android/services/telephony/ConferenceParticipantConnection;->getParticipantAddress(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .local v0, "address":Landroid/net/Uri;
    goto :goto_0
.end method

.method private getCountryIso(Lcom/android/internal/telephony/Phone;)Ljava/lang/String;
    .locals 4
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v3, 0x0

    .line 276
    if-nez p1, :cond_0

    .line 277
    return-object v3

    .line 280
    :cond_0
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v0

    .line 282
    .local v0, "subId":I
    invoke-static {v3}, Lcom/android/services/telephony/TelecomAccountRegistry;->getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/services/telephony/TelecomAccountRegistry;->getSubscriptionManager()Landroid/telephony/SubscriptionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v1

    .line 285
    .local v1, "subInfo":Landroid/telephony/SubscriptionInfo;
    if-nez v1, :cond_1

    .line 286
    return-object v3

    .line 291
    :cond_1
    invoke-virtual {v1}, Landroid/telephony/SubscriptionInfo;->getCountryIso()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getParticipantAddress(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 6
    .param p0, "address"    # Landroid/net/Uri;
    .param p1, "countryIso"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 224
    if-nez p0, :cond_0

    .line 225
    return-object p0

    .line 243
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 244
    .local v1, "number":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 245
    return-object p0

    .line 248
    :cond_1
    const-string/jumbo v3, "[@;:]"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 249
    .local v2, "numberParts":[Ljava/lang/String;
    array-length v3, v2

    if-nez v3, :cond_2

    .line 250
    return-object p0

    .line 252
    :cond_2
    aget-object v1, v2, v5

    .line 259
    const/4 v0, 0x0

    .line 260
    .local v0, "formattedNumber":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 261
    invoke-static {v1, p1}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 264
    .end local v0    # "formattedNumber":Ljava/lang/String;
    :cond_3
    const-string/jumbo v3, "tel"

    .line 265
    if-eqz v0, :cond_4

    .line 264
    :goto_0
    invoke-static {v3, v0, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    return-object v3

    :cond_4
    move-object v0, v1

    .line 265
    goto :goto_0
.end method

.method private getParticipantPresentation(Landroid/telecom/ConferenceParticipant;)I
    .locals 9
    .param p1, "participant"    # Landroid/telecom/ConferenceParticipant;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    .line 173
    invoke-virtual {p1}, Landroid/telecom/ConferenceParticipant;->getHandle()Landroid/net/Uri;

    move-result-object v0

    .line 174
    .local v0, "address":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 175
    return v7

    .line 178
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    .line 180
    .local v4, "number":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 181
    return v7

    .line 189
    :cond_1
    const-string/jumbo v6, "[;]"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 190
    .local v3, "hostParts":[Ljava/lang/String;
    const/4 v6, 0x0

    aget-object v1, v3, v6

    .line 194
    .local v1, "addressPart":Ljava/lang/String;
    const-string/jumbo v6, "[@]"

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 198
    .local v5, "numberParts":[Ljava/lang/String;
    array-length v6, v5

    if-eq v6, v7, :cond_2

    .line 199
    return v8

    .line 201
    :cond_2
    aget-object v2, v5, v8

    .line 205
    .local v2, "hostName":Ljava/lang/String;
    const-string/jumbo v6, "anonymous.invalid"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 206
    return v7

    .line 209
    :cond_3
    return v8
.end method

.method private setCapabilities()V
    .locals 1

    .prologue
    .line 160
    const/16 v0, 0x2000

    .line 161
    .local v0, "capabilities":I
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setConnectionCapabilities(I)V

    .line 162
    return-void
.end method


# virtual methods
.method public getUserEntity()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/services/telephony/ConferenceParticipantConnection;->mUserEntity:Landroid/net/Uri;

    return-object v0
.end method

.method public onDisconnect()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/android/services/telephony/ConferenceParticipantConnection;->mParentConnection:Lcom/android/internal/telephony/Connection;

    iget-object v1, p0, Lcom/android/services/telephony/ConferenceParticipantConnection;->mUserEntity:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Connection;->onDisconnectConferenceParticipant(Landroid/net/Uri;)V

    .line 134
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 301
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 302
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[ConferenceParticipantConnection objId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 304
    const-string/jumbo v1, " endPoint:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    iget-object v1, p0, Lcom/android/services/telephony/ConferenceParticipantConnection;->mEndpoint:Landroid/net/Uri;

    invoke-static {v1}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    const-string/jumbo v1, " address:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->getAddress()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    const-string/jumbo v1, " addressPresentation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->getAddressPresentation()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 310
    const-string/jumbo v1, " parentConnection:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    iget-object v1, p0, Lcom/android/services/telephony/ConferenceParticipantConnection;->mParentConnection:Lcom/android/internal/telephony/Connection;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    const-string/jumbo v1, " state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->getState()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public updateState(I)V
    .locals 4
    .param p1, "newState"    # I

    .prologue
    .line 92
    const-string/jumbo v0, "updateState endPoint: %s state: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/services/telephony/ConferenceParticipantConnection;->mEndpoint:Landroid/net/Uri;

    invoke-static {v2}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 93
    invoke-static {p1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 92
    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->getState()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 95
    return-void

    .line 98
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 119
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setActive()V

    .line 121
    :goto_0
    return-void

    .line 100
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setInitializing()V

    goto :goto_0

    .line 103
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setRinging()V

    goto :goto_0

    .line 106
    :pswitch_3
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setDialing()V

    goto :goto_0

    .line 109
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setOnHold()V

    goto :goto_0

    .line 112
    :pswitch_5
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setActive()V

    goto :goto_0

    .line 115
    :pswitch_6
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 116
    invoke-virtual {p0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->destroy()V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method
