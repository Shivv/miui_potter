.class public Lcom/miui/livetalk/push/PushMessageHandler;
.super Ljava/lang/Object;
.source "PushMessageHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final handleMessage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 20
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 21
    .local v3, "object":Lorg/json/JSONObject;
    const-string/jumbo v5, "message_type"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 22
    .local v4, "type":Ljava/lang/String;
    const-string/jumbo v5, "message_update_livetalk_status"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 23
    const-string/jumbo v5, "message_data"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 24
    .local v0, "data":Lorg/json/JSONObject;
    if-eqz v0, :cond_0

    const-string/jumbo v5, "livetalkStatus"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 25
    const-string/jumbo v5, "livetalkStatus"

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 26
    .local v2, "livetalkStatus":I
    invoke-static {p0, v2}, Lcom/miui/livetalk/LivetalkSettings;->setPhoneSupportStatus(Landroid/content/Context;I)V

    .line 27
    const-string/jumbo v5, "PushMessageHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "setPhoneSupportStatus "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    return v8

    .line 31
    .end local v0    # "data":Lorg/json/JSONObject;
    .end local v2    # "livetalkStatus":I
    .end local v3    # "object":Lorg/json/JSONObject;
    .end local v4    # "type":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 32
    .local v1, "e":Lorg/json/JSONException;
    const-string/jumbo v5, "PushMessageHandler"

    const-string/jumbo v6, "handleMessage json parse failed"

    invoke-static {v5, v6}, Lcom/miui/livetalk/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_0
    const/4 v5, 0x0

    return v5
.end method
