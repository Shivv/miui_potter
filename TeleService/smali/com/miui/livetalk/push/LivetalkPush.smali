.class public Lcom/miui/livetalk/push/LivetalkPush;
.super Ljava/lang/Object;
.source "LivetalkPush.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static initPushService(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const-string/jumbo v1, "LivetalkPush"

    const-string/jumbo v2, "initPushService"

    invoke-static {v1, v2}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    invoke-static {p0}, Lcom/miui/livetalk/push/LivetalkPush;->shouldInit(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 22
    invoke-static {p0}, Lcom/miui/livetalk/push/LivetalkPush;->registerPush(Landroid/content/Context;)V

    .line 25
    :cond_0
    new-instance v0, Lcom/miui/livetalk/push/LivetalkPush$1;

    invoke-direct {v0}, Lcom/miui/livetalk/push/LivetalkPush$1;-><init>()V

    .line 41
    .local v0, "newLogger":Lcom/xiaomi/channel/commonutils/logger/LoggerInterface;
    invoke-static {p0, v0}, Lcom/xiaomi/mipush/sdk/Logger;->setLogger(Landroid/content/Context;Lcom/xiaomi/channel/commonutils/logger/LoggerInterface;)V

    .line 42
    return-void
.end method

.method private static registerPush(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const-string/jumbo v0, "2882303761517356493"

    const-string/jumbo v1, "5421735629493"

    invoke-static {p0, v0, v1}, Lcom/xiaomi/mipush/sdk/MiPushClient;->registerPush(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-static {p0}, Lcom/miui/livetalk/account/AccountHelper;->getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/miui/livetalk/push/LivetalkPush;->setUserAccount(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 60
    return-void
.end method

.method public static setUserAccount(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v1, 0x0

    .line 68
    if-nez p1, :cond_0

    .line 69
    const-string/jumbo v0, "LivetalkPush"

    const-string/jumbo v1, "setUserAccount account is null."

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void

    .line 72
    :cond_0
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/xiaomi/mipush/sdk/MiPushClient;->setUserAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method private static shouldInit(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const-string/jumbo v6, "activity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 46
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v5

    .line 47
    .local v5, "processInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 48
    .local v3, "mainProcessName":Ljava/lang/String;
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    .line 49
    .local v4, "myPid":I
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "info$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 50
    .local v1, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v6, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v6, v4, :cond_0

    iget-object v6, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 51
    const/4 v6, 0x1

    return v6

    .line 54
    .end local v1    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    const/4 v6, 0x0

    return v6
.end method

.method public static unsetUserAccount(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v1, 0x0

    .line 81
    if-nez p1, :cond_0

    .line 82
    const-string/jumbo v0, "LivetalkPush"

    const-string/jumbo v1, "unsetUserAccount account is null."

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-void

    .line 85
    :cond_0
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/xiaomi/mipush/sdk/MiPushClient;->unsetUserAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    return-void
.end method
