.class Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;
.super Landroid/os/AsyncTask;
.source "LivetalkApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/livetalk/LivetalkApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DelayedInitializer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    .line 142
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 143
    iput-object p1, p0, Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;->mContext:Landroid/content/Context;

    .line 144
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 146
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 148
    const-string/jumbo v0, "LivetalkApplication"

    const-string/jumbo v1, "DelayedInitializer"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-static {}, Lcom/miui/livetalk/phone/PhoneCall;->getInstance()Lcom/miui/livetalk/phone/PhoneCall;

    .line 153
    iget-object v0, p0, Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/livetalk/account/AccountHelper;->registerAccountListener(Landroid/content/Context;)V

    .line 156
    iget-object v0, p0, Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/livetalk/push/LivetalkPush;->initPushService(Landroid/content/Context;)V

    .line 158
    iget-object v0, p0, Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/livetalk/LivetalkPrefs;->isSlotSettingsCached(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/livetalk/LivetalkPrefs;->isSlotSettingsSynced(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 159
    invoke-static {}, Lcom/miui/livetalk/phone/PhoneCall;->getInstance()Lcom/miui/livetalk/phone/PhoneCall;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/miui/livetalk/phone/PhoneCall;->migrateSlotSettings(Landroid/content/Context;)V

    .line 161
    :cond_0
    const-string/jumbo v0, "LivetalkApplication"

    const-string/jumbo v1, "DelayedInitializer end"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const/4 v0, 0x0

    return-object v0
.end method

.method public execute()V
    .locals 2

    .prologue
    .line 166
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {p0, v1, v0}, Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 167
    return-void
.end method
