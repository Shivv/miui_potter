.class public Lcom/miui/livetalk/phone/PhoneCall;
.super Ljava/lang/Object;
.source "PhoneCall.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/livetalk/phone/PhoneCall$Holder;,
        Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;
    }
.end annotation


# instance fields
.field private mIsBind:Z

.field private mMainHandler:Landroid/os/Handler;

.field private mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

.field private mRetryLivetalkCall:Z

.field private mRetryName:Ljava/lang/String;

.field private mRetryNormalCall:Z

.field private mRetryNumber:Ljava/lang/String;

.field private mRetryNumberType:I

.field private mRetrySlotId:I


# direct methods
.method static synthetic -get0(Lcom/miui/livetalk/phone/PhoneCall;)Z
    .locals 1
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;

    .prologue
    iget-boolean v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryLivetalkCall:Z

    return v0
.end method

.method static synthetic -get1(Lcom/miui/livetalk/phone/PhoneCall;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;

    .prologue
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get2(Lcom/miui/livetalk/phone/PhoneCall;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;

    .prologue
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get3(Lcom/miui/livetalk/phone/PhoneCall;)I
    .locals 1
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;

    .prologue
    iget v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryNumberType:I

    return v0
.end method

.method static synthetic -get4(Lcom/miui/livetalk/phone/PhoneCall;)I
    .locals 1
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;

    .prologue
    iget v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetrySlotId:I

    return v0
.end method

.method static synthetic -set0(Lcom/miui/livetalk/phone/PhoneCall;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/miui/livetalk/phone/PhoneCall;->mIsBind:Z

    return p1
.end method

.method static synthetic -set1(Lcom/miui/livetalk/phone/PhoneCall;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryLivetalkCall:Z

    return p1
.end method

.method static synthetic -set2(Lcom/miui/livetalk/phone/PhoneCall;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryNormalCall:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/miui/livetalk/phone/PhoneCall;Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)Z
    .locals 1
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callOutNumber"    # Ljava/lang/String;
    .param p3, "slotId"    # I
    .param p4, "numberType"    # I
    .param p5, "name"    # Ljava/lang/String;

    .prologue
    invoke-direct/range {p0 .. p5}, Lcom/miui/livetalk/phone/PhoneCall;->useLivetalkCallRPC(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/miui/livetalk/phone/PhoneCall;Landroid/content/Context;)V
    .locals 0
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    invoke-direct {p0, p1}, Lcom/miui/livetalk/phone/PhoneCall;->endCall(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/miui/livetalk/phone/PhoneCall;Landroid/content/Context;)V
    .locals 0
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    invoke-direct {p0, p1}, Lcom/miui/livetalk/phone/PhoneCall;->migrateGeneralSettings(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/miui/livetalk/phone/PhoneCall;)V
    .locals 0
    .param p0, "-this"    # Lcom/miui/livetalk/phone/PhoneCall;

    .prologue
    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->reset()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    iput-boolean v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryLivetalkCall:Z

    .line 144
    iput-boolean v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryNormalCall:Z

    .line 150
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mMainHandler:Landroid/os/Handler;

    .line 153
    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->bindCallService()V

    .line 154
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/livetalk/phone/PhoneCall;)V
    .locals 0
    .param p1, "-this0"    # Lcom/miui/livetalk/phone/PhoneCall;

    .prologue
    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;-><init>()V

    return-void
.end method

.method private bindCallService()V
    .locals 8

    .prologue
    .line 157
    invoke-static {}, Lcom/miui/livetalk/phone/LivetalkManager;->getInstance()Lcom/miui/livetalk/phone/LivetalkManager;

    move-result-object v3

    invoke-static {}, Lcom/miui/livetalk/LivetalkApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/miui/livetalk/phone/LivetalkManager;->enabledLivetalkCall(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 158
    return-void

    .line 161
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    if-nez v3, :cond_1

    .line 162
    new-instance v3, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;-><init>(Lcom/miui/livetalk/phone/PhoneCall;Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;)V

    iput-object v3, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    .line 164
    :cond_1
    invoke-static {}, Lcom/miui/livetalk/LivetalkApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 165
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.miui.livetalk.phone.livetalkcall"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 166
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "com.miui.milivetalk"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    iget-object v3, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/miui/livetalk/phone/PhoneCall;->mIsBind:Z

    .line 168
    const-string/jumbo v3, "PhoneCall"

    const-string/jumbo v4, "bindCallService(): %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-boolean v6, p0, Lcom/miui/livetalk/phone/PhoneCall;->mIsBind:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "PhoneCall"

    const-string/jumbo v4, "bindCallService()"

    invoke-static {v3, v4, v1}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 171
    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->reset()V

    goto :goto_0
.end method

.method private callingBundle(Ljava/lang/String;IILjava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "callOutNumber"    # Ljava/lang/String;
    .param p2, "numberType"    # I
    .param p3, "slotId"    # I
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 326
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 327
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "number"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string/jumbo v1, "number_type"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 329
    const-string/jumbo v1, "slot_id"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 330
    const-string/jumbo v1, "name"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    return-object v0
.end method

.method private endCall(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 378
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/livetalk/phone/PhoneCall$1;

    invoke-direct {v1, p0, p1}, Lcom/miui/livetalk/phone/PhoneCall$1;-><init>(Lcom/miui/livetalk/phone/PhoneCall;Landroid/content/Context;)V

    .line 390
    const-wide/16 v2, 0x3e8

    .line 378
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 391
    return-void
.end method

.method public static getInstance()Lcom/miui/livetalk/phone/PhoneCall;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/miui/livetalk/phone/PhoneCall$Holder;->INSTANCE:Lcom/miui/livetalk/phone/PhoneCall;

    return-object v0
.end method

.method private hangupBundle(Landroid/content/Context;ZLjava/lang/String;JIZJJJ)Landroid/os/Bundle;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isLivetalkConn"    # Z
    .param p3, "callOutNumber"    # Ljava/lang/String;
    .param p4, "durationMillis"    # J
    .param p6, "slotId"    # I
    .param p7, "isIncoming"    # Z
    .param p8, "connectTime"    # J
    .param p10, "disconnectTime"    # J
    .param p12, "startTime"    # J

    .prologue
    .line 344
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 345
    .local v2, "bundle":Landroid/os/Bundle;
    const-string/jumbo v9, "isLivetalk"

    invoke-virtual {v2, v9, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 346
    const-string/jumbo v9, "number"

    invoke-virtual {v2, v9, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string/jumbo v9, "slot_id"

    move/from16 v0, p6

    invoke-virtual {v2, v9, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 348
    const-string/jumbo v9, "incoming"

    move/from16 v0, p7

    invoke-virtual {v2, v9, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 349
    const-string/jumbo v9, "duration"

    move-wide/from16 v0, p4

    invoke-virtual {v2, v9, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 350
    const-string/jumbo v9, "startTime"

    move-wide/from16 v0, p12

    invoke-virtual {v2, v9, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 351
    const-string/jumbo v9, "connectTime"

    move-wide/from16 v0, p8

    invoke-virtual {v2, v9, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 352
    const-string/jumbo v9, "disconnectTime"

    move-wide/from16 v0, p10

    invoke-virtual {v2, v9, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 353
    if-nez p7, :cond_0

    .line 354
    const-string/jumbo v9, "number_type"

    invoke-static {p3}, Lcom/miui/livetalk/phone/NumberUtil;->numberType(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v2, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 357
    :cond_0
    const-string/jumbo v3, ""

    .line 358
    .local v3, "callbackNumber":Ljava/lang/String;
    const-string/jumbo v5, "2"

    .line 359
    .local v5, "isCallbackNumber":Ljava/lang/String;
    const-string/jumbo v6, "2"

    .line 360
    .local v6, "isInCallbackState":Ljava/lang/String;
    invoke-static {p1}, Lcom/miui/livetalk/LivetalkPrefs;->getLatestCallbackInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 361
    .local v8, "latestCallbackInfo":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 363
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 364
    .local v7, "jsonObject":Lorg/json/JSONObject;
    const-string/jumbo v9, "callbackNumber"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 365
    const-string/jumbo v9, "isCallbackNumber"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 366
    const-string/jumbo v9, "isInCallbackState"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 371
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    :goto_0
    const-string/jumbo v9, "callbackNumber"

    invoke-virtual {v2, v9, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string/jumbo v9, "isCallbackNumber"

    invoke-virtual {v2, v9, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string/jumbo v9, "isInCallbackState"

    invoke-virtual {v2, v9, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    return-object v2

    .line 367
    :catch_0
    move-exception v4

    .line 368
    .local v4, "e":Lorg/json/JSONException;
    const-string/jumbo v9, "PhoneCall"

    const-string/jumbo v10, "onDisconnect"

    invoke-static {v9, v10, v4}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private isCallServiceReady()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 181
    iget-boolean v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mIsBind:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    if-nez v0, :cond_1

    .line 182
    :cond_0
    const-string/jumbo v0, "PhoneCall"

    const-string/jumbo v1, "isCallServiceReady(): false"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->bindCallService()V

    .line 184
    return v2

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    iget-object v0, v0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

    if-eqz v0, :cond_2

    .line 188
    const/4 v0, 0x1

    return v0

    .line 191
    :cond_2
    return v2
.end method

.method private migrateGeneralSettings(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 422
    invoke-direct {p0, p1}, Lcom/miui/livetalk/phone/PhoneCall;->needMigrateData(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 423
    invoke-static {p1}, Lcom/miui/livetalk/LivetalkPrefs;->syncGeneralSettings(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 424
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->isCallServiceReady()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 426
    :try_start_0
    iget-object v2, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    iget-object v2, v2, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

    invoke-interface {v2, v0}, Lcom/miui/livetalk/phone/IPhoneCallService;->migrateData(Landroid/os/Bundle;)V

    .line 427
    invoke-static {p1}, Lcom/miui/livetalk/LivetalkPrefs;->setGeneralSettingsSynced(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 428
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 429
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v3, "migrateGeneralSettings()"

    invoke-static {v2, v3, v1}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private needMigrateData(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 414
    sget-boolean v0, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-nez v0, :cond_0

    .line 415
    const/4 v0, 0x0

    return v0

    .line 418
    :cond_0
    invoke-static {}, Lcom/miui/livetalk/phone/LivetalkManager;->getInstance()Lcom/miui/livetalk/phone/LivetalkManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/livetalk/phone/LivetalkManager;->activatedLivetalkService(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 176
    iput-boolean v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryLivetalkCall:Z

    .line 177
    iput-boolean v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mIsBind:Z

    .line 178
    return-void
.end method

.method private startCall(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "slotId"    # I
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 394
    const-string/jumbo v1, "PhoneCall"

    const-string/jumbo v2, "startCall()"

    invoke-static {v1, v2}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.CALL_PRIVILEGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 396
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 397
    const-string/jumbo v1, "tel"

    const/4 v2, 0x0

    invoke-static {v1, p2, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 398
    const-string/jumbo v1, "slot_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 399
    const-string/jumbo v1, "android.phone.extra.CONTACT_NAME"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 400
    const-string/jumbo v1, "callId"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 401
    const-string/jumbo v1, "only_regular_call"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 402
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 403
    return-void
.end method

.method private useLivetalkCallRPC(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callOutNumber"    # Ljava/lang/String;
    .param p3, "slotId"    # I
    .param p4, "numberType"    # I
    .param p5, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 319
    invoke-virtual {p0, p1}, Lcom/miui/livetalk/phone/PhoneCall;->migrateSlotSettings(Landroid/content/Context;)V

    .line 320
    invoke-direct {p0, p2, p4, p3, p5}, Lcom/miui/livetalk/phone/PhoneCall;->callingBundle(Ljava/lang/String;IILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 321
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    iget-object v1, v1, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

    invoke-interface {v1, v0}, Lcom/miui/livetalk/phone/IPhoneCallService;->useLivetalkCall(Landroid/os/Bundle;)V

    .line 322
    invoke-direct {p0, v0}, Lcom/miui/livetalk/phone/PhoneCall;->useLivetalkCallResult(Landroid/os/Bundle;)Z

    move-result v1

    return v1
.end method

.method private useLivetalkCallResult(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 335
    if-nez p1, :cond_0

    .line 336
    const-string/jumbo v0, "PhoneCall"

    const-string/jumbo v1, "useLivetalkCallResult bundle is null"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const/4 v0, 0x0

    return v0

    .line 339
    :cond_0
    const-string/jumbo v0, "useLivetalk"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public isCallBackNumber(Ljava/lang/String;)Z
    .locals 8
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 195
    invoke-static {}, Lcom/miui/livetalk/phone/LivetalkManager;->getInstance()Lcom/miui/livetalk/phone/LivetalkManager;

    move-result-object v2

    invoke-static {}, Lcom/miui/livetalk/LivetalkApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/miui/livetalk/phone/LivetalkManager;->enabledLivetalkCall(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 196
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v3, "isCallBackNumber(): enabledLivetalkCall == false, return false"

    invoke-static {v2, v3}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    return v6

    .line 199
    :cond_0
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v3, "isCallBackNumber(): %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/miui/livetalk/phone/NumberUtil;->getLogNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    const/4 v1, 0x0

    .line 202
    .local v1, "isCallbackNumber":Z
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->isCallServiceReady()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    iget-object v2, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    iget-object v2, v2, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

    invoke-interface {v2, p1}, Lcom/miui/livetalk/phone/IPhoneCallService;->isCallbackNumber(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 208
    .end local v1    # "isCallbackNumber":Z
    :cond_1
    :goto_0
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v3, "isCallBackNumber(): %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    return v1

    .line 205
    .restart local v1    # "isCallbackNumber":Z
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v3, "isCallBackNumber()"

    invoke-static {v2, v3, v0}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public migrateSlotSettings(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 436
    invoke-direct {p0, p1}, Lcom/miui/livetalk/phone/PhoneCall;->needMigrateData(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 437
    invoke-static {p1}, Lcom/miui/livetalk/LivetalkPrefs;->syncSlotSettings(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 438
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->isCallServiceReady()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 440
    :try_start_0
    const-string/jumbo v2, "has_slot"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 441
    iget-object v2, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    iget-object v2, v2, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

    invoke-interface {v2, v0}, Lcom/miui/livetalk/phone/IPhoneCallService;->migrateData(Landroid/os/Bundle;)V

    .line 442
    invoke-static {p1}, Lcom/miui/livetalk/LivetalkPrefs;->setSlotSettingsSynced(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 448
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 443
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 444
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v3, "migrateSlotSettings()"

    invoke-static {v2, v3, v1}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onBackCallAccepted()V
    .locals 3

    .prologue
    .line 242
    sget-boolean v1, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-nez v1, :cond_0

    .line 243
    return-void

    .line 245
    :cond_0
    const-string/jumbo v1, "PhoneCall"

    const-string/jumbo v2, "onBackCallAccepted()"

    invoke-static {v1, v2}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :try_start_0
    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->isCallServiceReady()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 248
    iget-object v1, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    iget-object v1, v1, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v1, v2}, Lcom/miui/livetalk/phone/IPhoneCallService;->onBackCallAccepted(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :cond_1
    :goto_0
    return-void

    .line 250
    :catch_0
    move-exception v0

    .line 251
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "PhoneCall"

    const-string/jumbo v2, "onBackCallAccepted()"

    invoke-static {v1, v2, v0}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onPhoneCallHangup(ZLjava/lang/String;JIZJJJ)V
    .locals 19
    .param p1, "isLivetalkConn"    # Z
    .param p2, "callOutNumber"    # Ljava/lang/String;
    .param p3, "durationMillis"    # J
    .param p5, "slotId"    # I
    .param p6, "isIncoming"    # Z
    .param p7, "connectTime"    # J
    .param p9, "disconnectTime"    # J
    .param p11, "startTime"    # J

    .prologue
    .line 257
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v4, "onPhoneCallHangup()"

    invoke-static {v2, v4}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryLivetalkCall:Z

    .line 259
    invoke-static {}, Lcom/miui/livetalk/LivetalkApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    .line 260
    .local v3, "context":Landroid/content/Context;
    invoke-static {}, Lcom/miui/livetalk/phone/LivetalkManager;->getInstance()Lcom/miui/livetalk/phone/LivetalkManager;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/miui/livetalk/phone/LivetalkManager;->enabledLivetalkCall(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 261
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v4, "enabledLivetalkCall(): false"

    invoke-static {v2, v4}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    return-void

    .line 264
    :cond_0
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v4, "isLivetalkConn:%s callout:%s slotId:%s inComing:%s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    .line 265
    invoke-static/range {p1 .. p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-static/range {p2 .. p2}, Lcom/miui/livetalk/phone/NumberUtil;->getLogNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    aput-object v6, v5, v7

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x2

    aput-object v6, v5, v7

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x3

    aput-object v6, v5, v7

    .line 264
    invoke-static {v2, v4, v5}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/miui/livetalk/phone/PhoneCall;->isCallServiceReady()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    iget-object v0, v2, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

    move-object/from16 v17, v0

    move-object/from16 v2, p0

    move/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    move/from16 v9, p6

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-wide/from16 v14, p11

    .line 269
    invoke-direct/range {v2 .. v15}, Lcom/miui/livetalk/phone/PhoneCall;->hangupBundle(Landroid/content/Context;ZLjava/lang/String;JIZJJJ)Landroid/os/Bundle;

    move-result-object v2

    .line 268
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/miui/livetalk/phone/IPhoneCallService;->onPhoneCallHangup(Landroid/os/Bundle;)V

    .line 274
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryNormalCall:Z

    if-eqz v2, :cond_2

    .line 275
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryNormalCall:Z

    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryNumber:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/miui/livetalk/phone/PhoneCall;->mRetrySlotId:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v4, v5}, Lcom/miui/livetalk/phone/PhoneCall;->startCall(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    :cond_2
    :goto_0
    return-void

    .line 278
    :catch_0
    move-exception v16

    .line 279
    .local v16, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "PhoneCall"

    const-string/jumbo v4, "onPhoneCallHangup"

    move-object/from16 v0, v16

    invoke-static {v2, v4, v0}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public updateCallBackNumberPool()V
    .locals 11

    .prologue
    .line 213
    sget-boolean v6, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-eqz v6, :cond_0

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x13

    if-le v6, v7, :cond_1

    .line 214
    :cond_0
    return-void

    .line 217
    :cond_1
    const-string/jumbo v6, "PhoneCall"

    const-string/jumbo v7, "updateCallBackNumberPool() begin"

    invoke-static {v6, v7}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->isCallServiceReady()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 219
    sget v5, Lmiui/telephony/livetalk/LivetalkUtils;->LIVETALK_NUMBER_POOL_VERSION:I

    .line 220
    .local v5, "version":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 221
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v6, "number_version"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    :try_start_0
    iget-object v6, p0, Lcom/miui/livetalk/phone/PhoneCall;->mPhoneCallConn:Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    iget-object v6, v6, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

    invoke-interface {v6, v0}, Lcom/miui/livetalk/phone/IPhoneCallService;->updateCallbackNumber(Landroid/os/Bundle;)V

    .line 224
    const-string/jumbo v6, "number_version"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 225
    .local v3, "newVersion":I
    const-string/jumbo v6, "number_pool"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 226
    .local v4, "numberPool":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-le v3, v5, :cond_3

    if-eqz v4, :cond_3

    .line 227
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v4}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 228
    .local v2, "jsonArray":Lorg/json/JSONArray;
    if-le v3, v5, :cond_2

    .line 229
    invoke-static {v2, v3}, Lmiui/telephony/livetalk/LivetalkUtils;->updateLivetalkCallBackNumber(Lorg/json/JSONArray;I)V

    .line 230
    :cond_2
    const-string/jumbo v6, "PhoneCall"

    const-string/jumbo v7, "updateCallBackNumberPool() %s -> %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v10, 0x0

    aput-object v9, v8, v10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v10, 0x1

    aput-object v9, v8, v10

    invoke-static {v6, v7, v8}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v3    # "newVersion":I
    .end local v4    # "numberPool":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "version":I
    :cond_3
    :goto_0
    const-string/jumbo v6, "PhoneCall"

    const-string/jumbo v7, "updateCallBackNumberPool() end"

    invoke-static {v6, v7}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    return-void

    .line 232
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v5    # "version":I
    :catch_0
    move-exception v1

    .line 233
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v6, "PhoneCall"

    const-string/jumbo v7, "updateCallBackNumberPool()"

    invoke-static {v6, v7, v1}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 236
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v5    # "version":I
    :cond_4
    const-string/jumbo v6, "PhoneCall"

    const-string/jumbo v7, "call service not ready"

    invoke-static {v6, v7}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public useLivetalkCall(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callOutNumber"    # Ljava/lang/String;
    .param p3, "slotId"    # I
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 284
    sget-boolean v0, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-nez v0, :cond_0

    .line 285
    return v8

    .line 287
    :cond_0
    const-string/jumbo v0, "PhoneCall"

    const-string/jumbo v1, "useLivetalkCall() callout:%s slotId:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/miui/livetalk/phone/NumberUtil;->getLogNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v1, v2}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    const/4 v7, 0x0

    .line 290
    .local v7, "useLivetalkCall":Z
    :try_start_0
    invoke-static {}, Lcom/miui/livetalk/phone/LivetalkManager;->getInstance()Lcom/miui/livetalk/phone/LivetalkManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/livetalk/phone/LivetalkManager;->enabledLivetalkCall(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/miui/livetalk/phone/LivetalkManager;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    invoke-static {p2}, Lcom/miui/livetalk/phone/NumberUtil;->numberType(Ljava/lang/String;)I

    move-result v4

    .line 292
    .local v4, "numberType":I
    const-string/jumbo v0, "PhoneCall"

    const-string/jumbo v1, "useLivetalkCall() numberType: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v5, 0x0

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 293
    const/4 v0, -0x1

    if-eq v4, v0, :cond_1

    .line 294
    invoke-direct {p0}, Lcom/miui/livetalk/phone/PhoneCall;->isCallServiceReady()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    .line 295
    invoke-direct/range {v0 .. v5}, Lcom/miui/livetalk/phone/PhoneCall;->useLivetalkCallRPC(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)Z

    move-result v7

    .line 296
    .local v7, "useLivetalkCall":Z
    if-eqz v7, :cond_1

    .line 297
    invoke-virtual {p0}, Lcom/miui/livetalk/phone/PhoneCall;->updateCallBackNumberPool()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    .end local v4    # "numberType":I
    .end local v7    # "useLivetalkCall":Z
    :cond_1
    :goto_0
    const-string/jumbo v0, "PhoneCall"

    const-string/jumbo v1, "useLivetalkCall() result: %S"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    return v7

    .line 301
    .restart local v4    # "numberType":I
    .local v7, "useLivetalkCall":Z
    :cond_2
    :try_start_1
    iput-object p2, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryNumber:Ljava/lang/String;

    .line 302
    iput v4, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryNumberType:I

    .line 303
    iput p3, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetrySlotId:I

    .line 304
    iput-object p4, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryName:Ljava/lang/String;

    .line 305
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/livetalk/phone/PhoneCall;->mRetryLivetalkCall:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 306
    const/4 v7, 0x1

    goto :goto_0

    .line 310
    .end local v4    # "numberType":I
    .end local v7    # "useLivetalkCall":Z
    :catch_0
    move-exception v6

    .line 311
    .local v6, "e":Ljava/lang/Exception;
    const-string/jumbo v0, "PhoneCall"

    const-string/jumbo v1, "useLivetalkCall()"

    invoke-static {v0, v1, v6}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
