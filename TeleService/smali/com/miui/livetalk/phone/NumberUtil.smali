.class public Lcom/miui/livetalk/phone/NumberUtil;
.super Ljava/lang/Object;
.source "NumberUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLogNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x6

    .line 45
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 46
    :cond_0
    const-string/jumbo v4, "[empty number]"

    return-object v4

    .line 48
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 49
    .local v1, "len":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v4, 0x4

    if-gt v1, v4, :cond_3

    .line 51
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v4, v1, -0x2

    if-ge v0, v4, :cond_2

    .line 52
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_2
    const/4 v0, 0x0

    const/4 v4, 0x2

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .local v2, "n":I
    :goto_1
    if-ge v0, v2, :cond_6

    .line 55
    const-string/jumbo v4, "*"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57
    .end local v0    # "i":I
    .end local v2    # "n":I
    :cond_3
    const/4 v4, 0x5

    if-eq v1, v4, :cond_4

    if-ne v1, v5, :cond_7

    .line 58
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    add-int/lit8 v4, v1, -0x3

    if-ge v0, v4, :cond_5

    .line 59
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 61
    :cond_5
    const-string/jumbo v4, "***"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .end local v0    # "i":I
    :cond_6
    :goto_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 62
    :cond_7
    if-le v1, v5, :cond_6

    .line 63
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    add-int/lit8 v4, v1, -0x4

    if-ge v0, v4, :cond_8

    .line 64
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 66
    :cond_8
    const-string/jumbo v4, "****"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method private static isLocalLandlines(Lmiui/telephony/PhoneNumberUtils$PhoneNumber;)Z
    .locals 1
    .param p0, "phoneNumber"    # Lmiui/telephony/PhoneNumberUtils$PhoneNumber;

    .prologue
    .line 40
    invoke-virtual {p0}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->isNormalMobileNumber()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getAreaCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p0}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->isChineseNumber()Z

    move-result v0

    .line 40
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isShortNumber(Lmiui/telephony/PhoneNumberUtils$PhoneNumber;)Z
    .locals 4
    .param p0, "phoneNumber"    # Lmiui/telephony/PhoneNumberUtils$PhoneNumber;

    .prologue
    const/4 v3, 0x7

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 35
    invoke-virtual {p0}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->isNormalMobileNumber()Z

    move-result v2

    if-nez v2, :cond_2

    .line 36
    invoke-virtual {p0}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getEffectiveNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v3, :cond_0

    invoke-virtual {p0, v0}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getNumberWithoutPrefix(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_1

    .line 35
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 36
    goto :goto_0

    :cond_2
    move v0, v1

    .line 35
    goto :goto_0
.end method

.method public static numberType(Ljava/lang/String;)I
    .locals 4
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    const/4 v3, -0x1

    .line 9
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 10
    return v3

    .line 13
    :cond_0
    invoke-static {p0}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->parse(Ljava/lang/CharSequence;)Lmiui/telephony/PhoneNumberUtils$PhoneNumber;

    move-result-object v1

    .line 14
    .local v1, "phoneNumber":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    if-nez v1, :cond_1

    .line 15
    return v3

    .line 19
    :cond_1
    const-string/jumbo v2, "*"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "#"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p0}, Lmiui/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 20
    invoke-virtual {v1}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->isServiceNumber()Z

    move-result v2

    .line 19
    if-nez v2, :cond_2

    .line 20
    invoke-static {v1}, Lcom/miui/livetalk/phone/NumberUtil;->isShortNumber(Lmiui/telephony/PhoneNumberUtils$PhoneNumber;)Z

    move-result v2

    .line 19
    if-eqz v2, :cond_3

    .line 21
    :cond_2
    return v3

    .line 25
    :cond_3
    invoke-static {v1}, Lcom/miui/livetalk/phone/NumberUtil;->isLocalLandlines(Lmiui/telephony/PhoneNumberUtils$PhoneNumber;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 26
    const/4 v2, 0x1

    return v2

    .line 29
    :cond_4
    invoke-virtual {v1}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->isChineseNumber()Z

    move-result v0

    .line 31
    .local v0, "isChineseNumber":Z
    if-eqz v0, :cond_5

    const/4 v2, 0x2

    :goto_0
    return v2

    :cond_5
    const/4 v2, 0x3

    goto :goto_0
.end method
