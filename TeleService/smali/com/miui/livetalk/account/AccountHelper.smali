.class public Lcom/miui/livetalk/account/AccountHelper;
.super Ljava/lang/Object;
.source "AccountHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/livetalk/account/AccountHelper$AccountBroadcastReceiver;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 34
    const/4 v0, 0x0

    .line 35
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 36
    .local v2, "am":Landroid/accounts/AccountManager;
    const-string/jumbo v3, "com.xiaomi"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 37
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v3, v1

    if-lez v3, :cond_0

    .line 38
    aget-object v0, v1, v4

    .line 40
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    if-nez v0, :cond_1

    .line 41
    const-string/jumbo v3, "AccountHelper"

    const-string/jumbo v4, "xiaomi account is null"

    invoke-static {v3, v4}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_1
    return-object v0
.end method

.method public static onXiaomiAccountLogin(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 48
    const-string/jumbo v0, "AccountHelper"

    const-string/jumbo v1, "onXiaomiAccountLogin"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-static {p0, p1}, Lcom/miui/livetalk/push/LivetalkPush;->setUserAccount(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 50
    return-void
.end method

.method public static onXiaomiAccountLogout(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 53
    const-string/jumbo v0, "AccountHelper"

    const-string/jumbo v1, "onXiaomiAccountLogout"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-static {p0, p1}, Lcom/miui/livetalk/push/LivetalkPush;->unsetUserAccount(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 55
    return-void
.end method

.method public static registerAccountListener(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    new-instance v0, Lcom/miui/livetalk/account/AccountHelper$AccountBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/livetalk/account/AccountHelper$AccountBroadcastReceiver;-><init>(Lcom/miui/livetalk/account/AccountHelper$AccountBroadcastReceiver;)V

    .line 30
    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 31
    return-void
.end method
