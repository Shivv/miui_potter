.class public Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;
.super Landroid/os/statistics/MicroscopicEvent;
.source "InputEventSuperviser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/statistics/InputEventSuperviser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SingleInputEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/InputEventSuperviser$SingleInputEvent$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIELD_INPUT_CHANNEL:Ljava/lang/String; = "inputChannel"

.field private static final FIELD_INPUT_EVENT_DESCRIPTION:Ljava/lang/String; = "inputEventDescription"

.field private static final FIELD_INPUT_EVENT_SEQUENCE_NUMBER:Ljava/lang/String; = "inputEventSequenceNumber"

.field private static final FIELD_INPUT_EVENT_STAGE:Ljava/lang/String; = "inputEventStage"

.field private static final FIELD_INPUT_EVENT_TIME:Ljava/lang/String; = "inputEventTime"

.field private static final FIELD_TARGET_RECEIVER:Ljava/lang/String; = "targetReceiver"


# instance fields
.field private inputChannel:Landroid/view/InputChannel;

.field public inputChannelName:Ljava/lang/String;

.field private inputEventAction:I

.field private inputEventCode:I

.field public inputEventDescription:Ljava/lang/String;

.field public inputEventSequenceNumber:I

.field public inputEventStage:I

.field public inputEventTime:J

.field private inputEventType:I

.field private targetReceiver:Landroid/view/InputEventReceiver;

.field public targetReceiverName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent$1;

    invoke-direct {v0}, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent$1;-><init>()V

    sput-object v0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "InputEvent"

    const/16 v1, 0x9

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MicroscopicEvent;-><init>(ILjava/lang/String;)V

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventDescription:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->eventFlags:I

    return-void
.end method

.method private static compactChannel(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    :try_start_0
    const-string/jumbo v2, "uninitialized"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "uninitialized"

    return-object v2

    :cond_0
    const-string/jumbo v2, " "

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x2

    if-lt v2, v3, :cond_1

    array-length v2, v1

    add-int/lit8 v2, v2, -0x2

    aget-object v2, v1, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :cond_1
    return-object p0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "null"

    return-object v2
.end method

.method private copyFrom(Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventStage:I

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventStage:I

    iget v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventSequenceNumber:I

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventSequenceNumber:I

    iget-wide v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventTime:J

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventTime:J

    iget-object v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventDescription:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventDescription:Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    iget v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventType:I

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventType:I

    iget v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventAction:I

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventAction:I

    iget v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventCode:I

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventCode:I

    iget-object v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiver:Landroid/view/InputEventReceiver;

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiver:Landroid/view/InputEventReceiver;

    iget-object v0, p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannel:Landroid/view/InputChannel;

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannel:Landroid/view/InputChannel;

    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;

    invoke-direct {p0, p1}, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->copyFrom(Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;)V

    return-void
.end method

.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->threadId:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->threadName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->beginUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->endUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Landroid/os/statistics/OsUtils;->decodeThreadSchedulePolicy(I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->schedPolicy:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->schedPriority:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->schedGroup:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->runningTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->runnableTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->sleepingTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->endRealTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventStage:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventSequenceNumber:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventTime:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventType:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventAction:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventCode:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/InputEventReceiver;

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiver:Landroid/view/InputEventReceiver;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/InputChannel;

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannel:Landroid/view/InputChannel;

    return-void
.end method

.method public hasMultiplePeerBlockingEvents()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBy(Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBySameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingMultiplePeer()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingSameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRootEvent()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method isUserPerceptible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "inputEventStage"

    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventStage:I

    const-string/jumbo v0, "inputEventSequenceNumber"

    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventSequenceNumber:I

    const-string/jumbo v0, "inputEventTime"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventTime:J

    const-string/jumbo v0, "inputEventDescription"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventDescription:Ljava/lang/String;

    const-string/jumbo v0, "inputChannel"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    const-string/jumbo v0, "targetReceiver"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventType:I

    iput v4, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventAction:I

    iput v4, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventCode:I

    iput-object v5, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiver:Landroid/view/InputEventReceiver;

    iput-object v5, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannel:Landroid/view/InputChannel;

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventStage:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventSequenceNumber:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventTime:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventDescription:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventType:I

    iput v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventAction:I

    iput v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventCode:I

    iput-object v3, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiver:Landroid/view/InputEventReceiver;

    iput-object v3, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannel:Landroid/view/InputChannel;

    return-void
.end method

.method public reset()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->reset()V

    iput v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventStage:I

    iput v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventSequenceNumber:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventTime:J

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventDescription:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventType:I

    iput v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventAction:I

    iput v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventCode:I

    iput-object v3, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiver:Landroid/view/InputEventReceiver;

    iput-object v3, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannel:Landroid/view/InputChannel;

    return-void
.end method

.method resolveLazyInfo()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-boolean v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->lazyInfoResolved:Z

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->resolveLazyInfo()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventType:I

    if-nez v1, :cond_1

    const-string/jumbo v1, "KeyEvent { action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventAction:I

    invoke-static {v2}, Landroid/view/KeyEvent;->actionToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", keyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventCode:I

    invoke-static {v2}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventDescription:Ljava/lang/String;

    iget-object v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiver:Landroid/view/InputEventReceiver;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiver:Landroid/view/InputEventReceiver;

    invoke-virtual {v1}, Landroid/view/InputEventReceiver;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannel:Landroid/view/InputChannel;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannel:Landroid/view/InputChannel;

    invoke-virtual {v1}, Landroid/view/InputChannel;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->compactChannel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    :goto_2
    const/4 v1, -0x1

    iput v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventType:I

    iput v4, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventAction:I

    iput v4, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventCode:I

    iput-object v3, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiver:Landroid/view/InputEventReceiver;

    iput-object v3, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannel:Landroid/view/InputChannel;

    return-void

    :cond_1
    const-string/jumbo v1, "MotionEvent { action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventAction:I

    invoke-static {v2}, Landroid/view/MotionEvent;->actionToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    const-string/jumbo v1, ""

    iput-object v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string/jumbo v1, ""

    iput-object v1, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    goto :goto_2
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "inputEventStage"

    iget v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventStage:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "inputEventSequenceNumber"

    iget v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventSequenceNumber:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "inputEventTime"

    iget-wide v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventTime:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "inputEventDescription"

    iget-object v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventDescription:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "inputChannel"

    iget-object v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "targetReceiver"

    iget-object v2, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/os/statistics/MicroscopicEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventStage:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventSequenceNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputEventDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->inputChannelName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->targetReceiverName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
