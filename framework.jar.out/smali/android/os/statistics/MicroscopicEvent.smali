.class public abstract Landroid/os/statistics/MicroscopicEvent;
.super Landroid/os/statistics/PerfEvent;
.source "MicroscopicEvent.java"


# static fields
.field public static final FLAG_INITIATOR_POSITION_MASK:I = 0x3

.field public static final FLAG_INITIATOR_POSITION_MASTER:I = 0x1

.field public static final FLAG_INITIATOR_POSITION_SLAVE:I = 0x2

.field public static final FLAG_INITIATOR_POSITION_UNKNOWN:I = 0x0

.field public static final MICRO_EVENT_TYPE_COUNT:I = 0xc

.field public static final MICRO_EVENT_TYPE_START:I = 0x0

.field public static final SCHED_POLICY_UNKNOWN:I = -0x1

.field public static final TYPE_LOOPER_ONCE:I = 0xb

.field public static final TYPE_SINGLE_BINDER_CALL:I = 0x4

.field public static final TYPE_SINGLE_BINDER_EXECUTION:I = 0x5

.field public static final TYPE_SINGLE_CONDITION_AWAKEN:I = 0x2

.field public static final TYPE_SINGLE_CONDITION_WAIT:I = 0x3

.field public static final TYPE_SINGLE_INPUT_EVENT:I = 0x9

.field public static final TYPE_SINGLE_JNI_METHOD:I = 0xa

.field public static final TYPE_SINGLE_LOCK_HOLD:I = 0x0

.field public static final TYPE_SINGLE_LOCK_WAIT:I = 0x1

.field public static final TYPE_SINGLE_LOOPER_MESSAGE:I = 0x8

.field public static final TYPE_SINGLE_SYSTEM_TRACE_EVENT:I = 0x7

.field public static final TYPE_SINGLE_TRACE_POINT:I = 0x6


# instance fields
.field public beginUptimeMillis:J

.field public endRealTimeMs:J

.field public endUptimeMillis:J

.field public eventFlags:I

.field matchedPeerBlockingDuration:J

.field public runnableTimeMs:J

.field public runningTimeMs:J

.field public schedGroup:I

.field public schedPolicy:I

.field public schedPriority:I

.field public sleepingTimeMs:J

.field public threadId:I

.field public threadName:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/os/statistics/PerfEvent;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method private copyFrom(Landroid/os/statistics/MicroscopicEvent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/PerfEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget v0, p1, Landroid/os/statistics/MicroscopicEvent;->threadId:I

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadId:I

    iget-object v0, p1, Landroid/os/statistics/MicroscopicEvent;->threadName:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadName:Ljava/lang/String;

    iget-wide v0, p1, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    iget-wide v0, p1, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget v0, p1, Landroid/os/statistics/MicroscopicEvent;->schedPolicy:I

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedPolicy:I

    iget v0, p1, Landroid/os/statistics/MicroscopicEvent;->schedPriority:I

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedPriority:I

    iget v0, p1, Landroid/os/statistics/MicroscopicEvent;->schedGroup:I

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedGroup:I

    iget-wide v0, p1, Landroid/os/statistics/MicroscopicEvent;->runningTimeMs:J

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->runningTimeMs:J

    iget-wide v0, p1, Landroid/os/statistics/MicroscopicEvent;->runnableTimeMs:J

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->runnableTimeMs:J

    iget-wide v0, p1, Landroid/os/statistics/MicroscopicEvent;->sleepingTimeMs:J

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->sleepingTimeMs:J

    iget-wide v0, p1, Landroid/os/statistics/MicroscopicEvent;->endRealTimeMs:J

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->endRealTimeMs:J

    iget v0, p1, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    iget-wide v0, p1, Landroid/os/statistics/MicroscopicEvent;->matchedPeerBlockingDuration:J

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->matchedPeerBlockingDuration:J

    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/MicroscopicEvent;

    invoke-direct {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->copyFrom(Landroid/os/statistics/MicroscopicEvent;)V

    return-void
.end method

.method public getBeginUptimeMillis()J
    .locals 2

    iget-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    return-wide v0
.end method

.method public getEndUptimeMillis()J
    .locals 2

    iget-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    return-wide v0
.end method

.method public abstract hasMultiplePeerBlockingEvents()Z
.end method

.method hasNativeStack()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract hasPeerBlockingEvent()Z
.end method

.method public abstract isBlockedBy(Landroid/os/statistics/MicroscopicEvent;)Z
.end method

.method public abstract isBlockedBySameProcess()Z
.end method

.method public abstract isBlockingMultiplePeer()Z
.end method

.method public abstract isBlockingSameProcess()Z
.end method

.method public isMasterEvent()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    and-int/lit8 v1, v1, 0x3

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isPeerBlockingEvent()Z
.end method

.method public abstract isRootEvent()Z
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 6

    const/4 v5, -0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/PerfEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "threadId"

    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadId:I

    const-string/jumbo v0, "threadName"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadName:Ljava/lang/String;

    const-string/jumbo v0, "beginTime"

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    const-string/jumbo v0, "endTime"

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    const-string/jumbo v0, "policy"

    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedPolicy:I

    const-string/jumbo v0, "priority"

    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedPriority:I

    const-string/jumbo v0, "schedGroup"

    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedGroup:I

    const-string/jumbo v0, "runningTime"

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->runningTimeMs:J

    const-string/jumbo v0, "runnableTime"

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->runnableTimeMs:J

    const-string/jumbo v0, "sleepingTime"

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->sleepingTimeMs:J

    const-string/jumbo v0, "endRealTime"

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->endRealTimeMs:J

    const-string/jumbo v0, "eventFlags"

    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    iput-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->matchedPeerBlockingDuration:J

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/PerfEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadName:Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadName:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadName:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedPolicy:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedPriority:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedGroup:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->runningTimeMs:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->runnableTimeMs:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->sleepingTimeMs:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->endRealTimeMs:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->matchedPeerBlockingDuration:J

    return-void
.end method

.method public reset()V
    .locals 5

    const/4 v4, -0x1

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-super {p0}, Landroid/os/statistics/PerfEvent;->reset()V

    iput v1, p0, Landroid/os/statistics/MicroscopicEvent;->threadId:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadName:Ljava/lang/String;

    iput-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    iput-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iput v4, p0, Landroid/os/statistics/MicroscopicEvent;->schedPolicy:I

    iput v1, p0, Landroid/os/statistics/MicroscopicEvent;->schedPriority:I

    iput v4, p0, Landroid/os/statistics/MicroscopicEvent;->schedGroup:I

    iput-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->runningTimeMs:J

    iput-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->runnableTimeMs:J

    iput-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->sleepingTimeMs:J

    iput-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->endRealTimeMs:J

    iput v1, p0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    iput-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->matchedPeerBlockingDuration:J

    return-void
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/PerfEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "threadId"

    iget v2, p0, Landroid/os/statistics/MicroscopicEvent;->threadId:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "threadName"

    iget-object v2, p0, Landroid/os/statistics/MicroscopicEvent;->threadName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "beginTime"

    iget-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "endTime"

    iget-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "policy"

    iget v2, p0, Landroid/os/statistics/MicroscopicEvent;->schedPolicy:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "priority"

    iget v2, p0, Landroid/os/statistics/MicroscopicEvent;->schedPriority:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "schedGroup"

    iget v2, p0, Landroid/os/statistics/MicroscopicEvent;->schedGroup:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "runningTime"

    iget-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->runningTimeMs:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "runnableTime"

    iget-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->runnableTimeMs:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "sleepingTime"

    iget-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->sleepingTimeMs:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "endRealTime"

    iget-wide v2, p0, Landroid/os/statistics/MicroscopicEvent;->endRealTimeMs:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "eventFlags"

    iget v2, p0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/os/statistics/PerfEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/os/statistics/MicroscopicEvent;->threadName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedPolicy:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedPriority:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/statistics/MicroscopicEvent;->schedGroup:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->runningTimeMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->runnableTimeMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->sleepingTimeMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Landroid/os/statistics/MicroscopicEvent;->endRealTimeMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
