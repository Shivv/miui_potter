.class public final Landroid/os/statistics/LooperOnce;
.super Landroid/os/statistics/MicroscopicEvent;
.source "LooperOnce.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/LooperOnce$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/statistics/LooperOnce;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/os/statistics/LooperOnce$1;

    invoke-direct {v0}, Landroid/os/statistics/LooperOnce$1;-><init>()V

    sput-object v0, Landroid/os/statistics/LooperOnce;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "LooperOnce"

    const/16 v1, 0xb

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MicroscopicEvent;-><init>(ILjava/lang/String;)V

    const/4 v0, 0x1

    iput v0, p0, Landroid/os/statistics/LooperOnce;->eventFlags:I

    return-void
.end method


# virtual methods
.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/LooperOnce;->threadId:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperOnce;->beginUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperOnce;->endUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Landroid/os/statistics/OsUtils;->decodeThreadSchedulePolicy(I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/LooperOnce;->schedPolicy:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/LooperOnce;->schedPriority:I

    return-void
.end method

.method public hasMultiplePeerBlockingEvents()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBy(Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBySameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingMultiplePeer()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingSameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method isConcerned()Z
    .locals 6

    iget v2, p0, Landroid/os/statistics/LooperOnce;->threadId:I

    sget v3, Landroid/os/statistics/PerfSuperviser;->MY_PID:I

    if-eq v2, v3, :cond_0

    iget v2, p0, Landroid/os/statistics/LooperOnce;->threadId:I

    invoke-static {v2}, Landroid/os/statistics/OsUtils;->isRenderThread(I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Landroid/app/ActivityThreadInjector;->isSystemThread()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Landroid/os/statistics/LooperOnce;->schedPolicy:I

    iget v3, p0, Landroid/os/statistics/LooperOnce;->schedPriority:I

    invoke-static {v2, v3}, Landroid/os/statistics/OsUtils;->isHighPriority(II)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    sget v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionSoftThreshold:I

    int-to-long v0, v2

    :goto_0
    iget-wide v2, p0, Landroid/os/statistics/LooperOnce;->endUptimeMillis:J

    iget-wide v4, p0, Landroid/os/statistics/LooperOnce;->beginUptimeMillis:J

    sub-long/2addr v2, v4

    cmp-long v2, v2, v0

    if-ltz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_1
    sget v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionHardThreshold:I

    const/16 v3, 0x12c

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-long v0, v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public isPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRootEvent()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
