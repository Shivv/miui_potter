.class public abstract Landroid/os/statistics/PerfEvent;
.super Ljava/lang/Object;
.source "PerfEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/PerfEvent$EndTimeComparator;
    }
.end annotation


# static fields
.field public static final FIELD_CURRENT_PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field public static final FIELD_CURRENT_PID:Ljava/lang/String; = "pid"

.field public static final FIELD_CURRENT_PROCESS_NAME:Ljava/lang/String; = "processName"

.field public static final FIELD_CURRENT_UID:Ljava/lang/String; = "uid"

.field public static final FIELD_EVENT_SEQ:Ljava/lang/String; = "seq"

.field public static final FIELD_EVENT_TYPE:Ljava/lang/String; = "eventType"

.field public static final FIELD_EVENT_TYPE_NAME:Ljava/lang/String; = "eventTypeName"

.field private static MY_UID:I = 0x0

.field public static final TYPE_UNKNOWN:I = -0x1

.field public static final endUptimeComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/os/statistics/PerfEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static processNameFromCmdline:Ljava/lang/String;

.field private static processNameFromCmdlineReady:Z


# instance fields
.field public final eventType:I

.field public final eventTypeName:Ljava/lang/String;

.field protected lazyInfoResolved:Z

.field public packageName:Ljava/lang/String;

.field public pid:I

.field public processName:Ljava/lang/String;

.field public uid:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, -0x1

    sput v0, Landroid/os/statistics/PerfEvent;->MY_UID:I

    new-instance v0, Landroid/os/statistics/PerfEvent$EndTimeComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/statistics/PerfEvent$EndTimeComparator;-><init>(Landroid/os/statistics/PerfEvent$EndTimeComparator;)V

    sput-object v0, Landroid/os/statistics/PerfEvent;->endUptimeComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/os/statistics/PerfEvent;->eventType:I

    iput-object p2, p0, Landroid/os/statistics/PerfEvent;->eventTypeName:Ljava/lang/String;

    return-void
.end method

.method public static getPerfEventType(Lorg/json/JSONObject;)I
    .locals 2

    const-string/jumbo v0, "eventType"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 1

    iget v0, p1, Landroid/os/statistics/PerfEvent;->pid:I

    iput v0, p0, Landroid/os/statistics/PerfEvent;->pid:I

    iget v0, p1, Landroid/os/statistics/PerfEvent;->uid:I

    iput v0, p0, Landroid/os/statistics/PerfEvent;->uid:I

    iget-object v0, p1, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    iget-boolean v0, p1, Landroid/os/statistics/PerfEvent;->lazyInfoResolved:Z

    iput-boolean v0, p0, Landroid/os/statistics/PerfEvent;->lazyInfoResolved:Z

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method abstract fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
.end method

.method final fillInCurrentProcessId()V
    .locals 1

    invoke-virtual {p0}, Landroid/os/statistics/PerfEvent;->occursInCurrentProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/statistics/PerfSuperviser;->MY_PID:I

    iput v0, p0, Landroid/os/statistics/PerfEvent;->pid:I

    :cond_0
    return-void
.end method

.method final fillInCurrentProcessInfo()V
    .locals 6

    invoke-virtual {p0}, Landroid/os/statistics/PerfEvent;->occursInCurrentProcess()Z

    move-result v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    sget v3, Landroid/os/statistics/PerfSuperviser;->MY_PID:I

    iput v3, p0, Landroid/os/statistics/PerfEvent;->pid:I

    sget v3, Landroid/os/statistics/PerfEvent;->MY_UID:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    sput v3, Landroid/os/statistics/PerfEvent;->MY_UID:I

    :cond_1
    sget v3, Landroid/os/statistics/PerfEvent;->MY_UID:I

    iput v3, p0, Landroid/os/statistics/PerfEvent;->uid:I

    invoke-static {}, Landroid/app/ActivityThread;->currentProcessName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    iget-object v3, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-boolean v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdlineReady:Z

    if-nez v3, :cond_3

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    new-instance v4, Ljava/io/FileInputStream;

    const-string/jumbo v5, "/proc/self/cmdline"

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdline:Ljava/lang/String;

    sget-object v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdline:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdline:Ljava/lang/String;

    const/4 v4, 0x0

    const/16 v5, 0x20

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdline:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_0
    move-object v1, v2

    :goto_1
    sget-object v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdline:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    sget-object v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdline:Ljava/lang/String;

    const-string/jumbo v4, "<"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_3

    sget-object v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdline:Ljava/lang/String;

    const-string/jumbo v4, "zygote"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    sput-boolean v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdlineReady:Z

    :cond_3
    sget-object v3, Landroid/os/statistics/PerfEvent;->processNameFromCmdline:Ljava/lang/String;

    iput-object v3, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    :cond_4
    iget-object v3, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    if-nez v3, :cond_5

    const-string/jumbo v3, ""

    iput-object v3, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    :cond_5
    invoke-static {}, Landroid/app/ActivityThread;->currentPackageName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    iget-object v3, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    if-nez v3, :cond_6

    const-string/jumbo v3, ""

    iput-object v3, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    :cond_6
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception v3

    :goto_3
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :goto_4
    throw v3

    :catch_3
    move-exception v0

    goto :goto_4

    :catchall_1
    move-exception v3

    move-object v1, v2

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method public abstract getBeginUptimeMillis()J
.end method

.method public abstract getEndUptimeMillis()J
.end method

.method isConcerned()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method isMeaningful()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method isUserPerceptible()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method occursInCurrentProcess()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 2

    const/4 v1, 0x0

    const-string/jumbo v0, "pid"

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/PerfEvent;->pid:I

    const-string/jumbo v0, "uid"

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/PerfEvent;->uid:I

    const-string/jumbo v0, "processName"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    const-string/jumbo v0, "packageName"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/os/statistics/PerfEvent;->lazyInfoResolved:Z

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/PerfEvent;->pid:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/PerfEvent;->uid:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/os/statistics/PerfEvent;->lazyInfoResolved:Z

    return-void
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    iput v1, p0, Landroid/os/statistics/PerfEvent;->pid:I

    iput v1, p0, Landroid/os/statistics/PerfEvent;->uid:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    iput-boolean v1, p0, Landroid/os/statistics/PerfEvent;->lazyInfoResolved:Z

    return-void
.end method

.method resolveLazyInfo()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/os/statistics/PerfEvent;->lazyInfoResolved:Z

    return-void
.end method

.method public final toJson()Lorg/json/JSONObject;
    .locals 1

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0, v0}, Landroid/os/statistics/PerfEvent;->writeToJson(Lorg/json/JSONObject;)V

    return-object v0
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 3

    :try_start_0
    const-string/jumbo v1, "eventType"

    iget v2, p0, Landroid/os/statistics/PerfEvent;->eventType:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "eventTypeName"

    iget-object v2, p0, Landroid/os/statistics/PerfEvent;->eventTypeName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "pid"

    iget v2, p0, Landroid/os/statistics/PerfEvent;->pid:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "uid"

    iget v2, p0, Landroid/os/statistics/PerfEvent;->uid:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "processName"

    iget-object v2, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "packageName"

    iget-object v2, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Landroid/os/statistics/PerfEvent;->pid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/statistics/PerfEvent;->uid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/os/statistics/PerfEvent;->processName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/os/statistics/PerfEvent;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
