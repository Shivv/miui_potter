.class public abstract Landroid/os/statistics/MacroscopicEvent;
.super Landroid/os/statistics/PerfEvent;
.source "MacroscopicEvent.java"


# static fields
.field public static final MACRO_EVENT_TYPE_COUNT:I = 0x5

.field public static final MACRO_EVENT_TYPE_START:I = 0x10000

.field public static final TYPE_BINDER_STARVATION:I = 0x10004

.field public static final TYPE_DEX2OAT:I = 0x10003

.field public static final TYPE_PLACE_HOLDER_1:I = 0x10001

.field public static final TYPE_SINGLE_EVENT_LOG_ITEM:I = 0x10000

.field public static final TYPE_SINGLE_JANK_RECORD:I = 0x10002


# instance fields
.field public occurUptimeMillis:J


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/os/statistics/PerfEvent;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method private copyFrom(Landroid/os/statistics/MacroscopicEvent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/PerfEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget-wide v0, p1, Landroid/os/statistics/MacroscopicEvent;->occurUptimeMillis:J

    iput-wide v0, p0, Landroid/os/statistics/MacroscopicEvent;->occurUptimeMillis:J

    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/MacroscopicEvent;

    invoke-direct {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->copyFrom(Landroid/os/statistics/MacroscopicEvent;)V

    return-void
.end method

.method public getBeginUptimeMillis()J
    .locals 2

    iget-wide v0, p0, Landroid/os/statistics/MacroscopicEvent;->occurUptimeMillis:J

    return-wide v0
.end method

.method public getEndUptimeMillis()J
    .locals 2

    iget-wide v0, p0, Landroid/os/statistics/MacroscopicEvent;->occurUptimeMillis:J

    return-wide v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/PerfEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "occurTime"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MacroscopicEvent;->occurUptimeMillis:J

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/PerfEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MacroscopicEvent;->occurUptimeMillis:J

    return-void
.end method

.method public reset()V
    .locals 2

    invoke-super {p0}, Landroid/os/statistics/PerfEvent;->reset()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/os/statistics/MacroscopicEvent;->occurUptimeMillis:J

    return-void
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/PerfEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "occurTime"

    iget-wide v2, p0, Landroid/os/statistics/MacroscopicEvent;->occurUptimeMillis:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/os/statistics/PerfEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget-wide v0, p0, Landroid/os/statistics/MacroscopicEvent;->occurUptimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
