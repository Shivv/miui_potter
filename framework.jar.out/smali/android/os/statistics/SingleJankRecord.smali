.class public Landroid/os/statistics/SingleJankRecord;
.super Landroid/os/statistics/MacroscopicEvent;
.source "SingleJankRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/SingleJankRecord$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/statistics/SingleJankRecord;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIELD_APP_CAUSE:Ljava/lang/String; = "appCause"

.field private static final FIELD_BATTERY_LEVEL:Ljava/lang/String; = "batteryLevel"

.field private static final FIELD_BATTERY_TEMP:Ljava/lang/String; = "batteryTemperature"

.field private static final FIELD_CHARGING:Ljava/lang/String; = "isCharging"

.field private static final FIELD_CONCLUSION:Ljava/lang/String; = "conclusion"

.field private static final FIELD_MAX_FRAME_DURATION:Ljava/lang/String; = "maxFrameDuration"

.field private static final FIELD_RECEIVED_CURRENT_TIME:Ljava/lang/String; = "receivedCurrentTime"

.field private static final FIELD_RECEIVED_UPTIME:Ljava/lang/String; = "receivedUptime"

.field private static final FIELD_RENDER_THREAD_ID:Ljava/lang/String; = "renderThreadTid"

.field private static final FIELD_SYS_CAUSE:Ljava/lang/String; = "sysCause"

.field private static final FIELD_TOTAL_DURATION:Ljava/lang/String; = "totalDuration"

.field private static final FIELD_WINDOW_NAME:Ljava/lang/String; = "windowName"


# instance fields
.field public appCause:Ljava/lang/String;

.field public batteryLevel:I

.field public batteryTemperature:I

.field public conclusion:Ljava/lang/String;

.field public isCharging:Z

.field public maxFrameDuration:J

.field public receivedCurrentTimeMillis:J

.field public receivedUptimeMillis:J

.field public renderThreadTid:I

.field public sysCause:Ljava/lang/String;

.field public totalDuration:J

.field public windowName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/os/statistics/SingleJankRecord$1;

    invoke-direct {v0}, Landroid/os/statistics/SingleJankRecord$1;-><init>()V

    sput-object v0, Landroid/os/statistics/SingleJankRecord;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "JankRecord"

    const v1, 0x10002

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MacroscopicEvent;-><init>(ILjava/lang/String;)V

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->windowName:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->appCause:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->sysCause:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->conclusion:Ljava/lang/String;

    return-void
.end method

.method private copyFrom(Landroid/os/statistics/SingleJankRecord;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget v0, p1, Landroid/os/statistics/SingleJankRecord;->renderThreadTid:I

    iput v0, p0, Landroid/os/statistics/SingleJankRecord;->renderThreadTid:I

    iget-object v0, p1, Landroid/os/statistics/SingleJankRecord;->windowName:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->windowName:Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/SingleJankRecord;->appCause:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->appCause:Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/SingleJankRecord;->sysCause:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->sysCause:Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/SingleJankRecord;->conclusion:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->conclusion:Ljava/lang/String;

    iget-wide v0, p1, Landroid/os/statistics/SingleJankRecord;->totalDuration:J

    iput-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->totalDuration:J

    iget-wide v0, p1, Landroid/os/statistics/SingleJankRecord;->maxFrameDuration:J

    iput-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->maxFrameDuration:J

    iget-wide v0, p1, Landroid/os/statistics/SingleJankRecord;->receivedUptimeMillis:J

    iput-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->receivedUptimeMillis:J

    iget-wide v0, p1, Landroid/os/statistics/SingleJankRecord;->receivedCurrentTimeMillis:J

    iput-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->receivedCurrentTimeMillis:J

    iget-boolean v0, p1, Landroid/os/statistics/SingleJankRecord;->isCharging:Z

    iput-boolean v0, p0, Landroid/os/statistics/SingleJankRecord;->isCharging:Z

    iget v0, p1, Landroid/os/statistics/SingleJankRecord;->batteryLevel:I

    iput v0, p0, Landroid/os/statistics/SingleJankRecord;->batteryLevel:I

    iget v0, p1, Landroid/os/statistics/SingleJankRecord;->batteryTemperature:I

    iput v0, p0, Landroid/os/statistics/SingleJankRecord;->batteryTemperature:I

    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/SingleJankRecord;

    invoke-direct {p0, p1}, Landroid/os/statistics/SingleJankRecord;->copyFrom(Landroid/os/statistics/SingleJankRecord;)V

    return-void
.end method

.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 0

    return-void
.end method

.method isUserPerceptible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method occursInCurrentProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "renderThreadTid"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/SingleJankRecord;->renderThreadTid:I

    const-string/jumbo v0, "windowName"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->windowName:Ljava/lang/String;

    const-string/jumbo v0, "appCause"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->appCause:Ljava/lang/String;

    const-string/jumbo v0, "sysCause"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->sysCause:Ljava/lang/String;

    const-string/jumbo v0, "conclusion"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->conclusion:Ljava/lang/String;

    const-string/jumbo v0, "totalDuration"

    invoke-virtual {p1, v0, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->totalDuration:J

    const-string/jumbo v0, "maxFrameDuration"

    invoke-virtual {p1, v0, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->maxFrameDuration:J

    const-string/jumbo v0, "receivedUptime"

    invoke-virtual {p1, v0, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->receivedUptimeMillis:J

    const-string/jumbo v0, "receivedCurrentTime"

    invoke-virtual {p1, v0, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->receivedCurrentTimeMillis:J

    const-string/jumbo v0, "isCharging"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Landroid/os/statistics/SingleJankRecord;->isCharging:Z

    const-string/jumbo v0, "batteryLevel"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/SingleJankRecord;->batteryLevel:I

    const-string/jumbo v0, "batteryTemperature"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/SingleJankRecord;->batteryTemperature:I

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 4

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Landroid/os/statistics/SingleJankRecord;->renderThreadTid:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/os/statistics/SingleJankRecord;->windowName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/os/statistics/SingleJankRecord;->appCause:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/os/statistics/SingleJankRecord;->sysCause:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/os/statistics/SingleJankRecord;->conclusion:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->totalDuration:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->maxFrameDuration:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->receivedUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->receivedCurrentTimeMillis:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Landroid/os/statistics/SingleJankRecord;->isCharging:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/SingleJankRecord;->batteryLevel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/SingleJankRecord;->batteryTemperature:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/os/statistics/MacroscopicEvent;->reset()V

    iput v1, p0, Landroid/os/statistics/SingleJankRecord;->renderThreadTid:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->windowName:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->appCause:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->sysCause:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/SingleJankRecord;->conclusion:Ljava/lang/String;

    iput-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->totalDuration:J

    iput-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->maxFrameDuration:J

    iput-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->receivedUptimeMillis:J

    iput-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->receivedCurrentTimeMillis:J

    iput-boolean v1, p0, Landroid/os/statistics/SingleJankRecord;->isCharging:Z

    iput v1, p0, Landroid/os/statistics/SingleJankRecord;->batteryLevel:I

    iput v1, p0, Landroid/os/statistics/SingleJankRecord;->batteryTemperature:I

    return-void
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "renderThreadTid"

    iget v2, p0, Landroid/os/statistics/SingleJankRecord;->renderThreadTid:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "windowName"

    iget-object v2, p0, Landroid/os/statistics/SingleJankRecord;->windowName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "appCause"

    iget-object v2, p0, Landroid/os/statistics/SingleJankRecord;->appCause:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "sysCause"

    iget-object v2, p0, Landroid/os/statistics/SingleJankRecord;->sysCause:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "conclusion"

    iget-object v2, p0, Landroid/os/statistics/SingleJankRecord;->conclusion:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "totalDuration"

    iget-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->totalDuration:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "maxFrameDuration"

    iget-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->maxFrameDuration:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "receivedUptime"

    iget-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->receivedUptimeMillis:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "receivedCurrentTime"

    iget-wide v2, p0, Landroid/os/statistics/SingleJankRecord;->receivedCurrentTimeMillis:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "isCharging"

    iget-boolean v2, p0, Landroid/os/statistics/SingleJankRecord;->isCharging:Z

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string/jumbo v1, "batteryLevel"

    iget v2, p0, Landroid/os/statistics/SingleJankRecord;->batteryLevel:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "batteryTemperature"

    iget v2, p0, Landroid/os/statistics/SingleJankRecord;->batteryTemperature:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/os/statistics/MacroscopicEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget v0, p0, Landroid/os/statistics/SingleJankRecord;->renderThreadTid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/os/statistics/SingleJankRecord;->windowName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/os/statistics/SingleJankRecord;->appCause:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/os/statistics/SingleJankRecord;->sysCause:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/os/statistics/SingleJankRecord;->conclusion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->totalDuration:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->maxFrameDuration:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->receivedUptimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Landroid/os/statistics/SingleJankRecord;->receivedCurrentTimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Landroid/os/statistics/SingleJankRecord;->isCharging:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/statistics/SingleJankRecord;->batteryLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/statistics/SingleJankRecord;->batteryTemperature:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
