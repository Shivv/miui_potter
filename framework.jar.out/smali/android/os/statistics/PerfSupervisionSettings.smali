.class public Landroid/os/statistics/PerfSupervisionSettings;
.super Ljava/lang/Object;
.source "PerfSupervisionSettings.java"


# static fields
.field private static final DEFAULT_DIVISION_RATIO:I = 0x2

.field private static final DEFAULT_HARD_THRESHOLD_MS:I = 0x3e8

.field private static final DEFAULT_LEVEL:I

.field private static final DEFAULT_SOFT_THRESHOLD_MS:I

.field private static LOW_STRESS_MONITORING_DEVICES:Ljava/lang/String; = null

.field private static final MIN_DIVISION_RATIO:I = 0x1

.field private static final MIN_HARD_THRESHOLD_MS:I = 0xc8

.field static final MIN_SOFT_THRESHOLD_MS:I

.field static final PERF_SUPERVISION_OFF:I = 0x0

.field static final PERF_SUPERVISION_ON_HEAVY:I = 0x2

.field static final PERF_SUPERVISION_ON_NORMAL:I = 0x1

.field static final PERF_SUPERVISION_ON_TEST:I = 0x9

.field static final sPerfSupervisionDivisionRatio:I

.field static final sPerfSupervisionHardThreshold:I

.field private static final sPerfSupervisionLevel:I

.field static final sPerfSupervisionSoftThreshold:I

.field private static sReadySupervision:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/16 v8, 0x3e8

    const/4 v7, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    sget-boolean v2, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-nez v2, :cond_0

    const/4 v2, 0x4

    :goto_0
    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->MIN_SOFT_THRESHOLD_MS:I

    sget-boolean v2, Lmiui/os/Build;->IS_ALPHA_BUILD:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->DEFAULT_LEVEL:I

    const-string/jumbo v2, "gemini"

    sput-object v2, Landroid/os/statistics/PerfSupervisionSettings;->LOW_STRESS_MONITORING_DEVICES:Ljava/lang/String;

    sput-boolean v4, Landroid/os/statistics/PerfSupervisionSettings;->sReadySupervision:Z

    sget-object v2, Landroid/os/statistics/PerfSupervisionSettings;->LOW_STRESS_MONITORING_DEVICES:Ljava/lang/String;

    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0xc8

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->DEFAULT_SOFT_THRESHOLD_MS:I

    :goto_2
    const-string/jumbo v2, "persist.sys.perf_mistats_opt"

    const-string/jumbo v5, ""

    invoke-static {v2, v5}, Lmiui/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget v2, Landroid/os/statistics/PerfSupervisionSettings;->DEFAULT_LEVEL:I

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionLevel:I

    sget v2, Landroid/os/statistics/PerfSupervisionSettings;->DEFAULT_SOFT_THRESHOLD_MS:I

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionSoftThreshold:I

    sput v8, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionHardThreshold:I

    sput v6, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionDivisionRatio:I

    :goto_3
    return-void

    :cond_0
    const/16 v2, 0x10

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1

    :cond_2
    const/16 v2, 0x64

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->DEFAULT_SOFT_THRESHOLD_MS:I

    goto :goto_2

    :cond_3
    const-string/jumbo v2, " "

    const-string/jumbo v5, ""

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-lt v2, v3, :cond_4

    aget-object v2, v1, v4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    aget-object v2, v1, v4

    sget v4, Landroid/os/statistics/PerfSupervisionSettings;->DEFAULT_LEVEL:I

    invoke-static {v2, v4}, Landroid/os/statistics/PerfSupervisionSettings;->parseIntWithDefault(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionLevel:I

    :goto_4
    array-length v2, v1

    if-lt v2, v6, :cond_5

    aget-object v2, v1, v3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    aget-object v2, v1, v3

    sget v4, Landroid/os/statistics/PerfSupervisionSettings;->DEFAULT_SOFT_THRESHOLD_MS:I

    invoke-static {v2, v4}, Landroid/os/statistics/PerfSupervisionSettings;->parseIntWithDefault(Ljava/lang/String;I)I

    move-result v2

    sget v4, Landroid/os/statistics/PerfSupervisionSettings;->MIN_SOFT_THRESHOLD_MS:I

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionSoftThreshold:I

    :goto_5
    array-length v2, v1

    if-lt v2, v7, :cond_6

    aget-object v2, v1, v6

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_6

    aget-object v2, v1, v6

    invoke-static {v2, v8}, Landroid/os/statistics/PerfSupervisionSettings;->parseIntWithDefault(Ljava/lang/String;I)I

    move-result v2

    const/16 v4, 0xc8

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionHardThreshold:I

    :goto_6
    array-length v2, v1

    const/4 v4, 0x4

    if-lt v2, v4, :cond_7

    aget-object v2, v1, v7

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_7

    aget-object v2, v1, v7

    invoke-static {v2, v6}, Landroid/os/statistics/PerfSupervisionSettings;->parseIntWithDefault(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    sget v3, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionSoftThreshold:I

    sget v4, Landroid/os/statistics/PerfSupervisionSettings;->MIN_SOFT_THRESHOLD_MS:I

    div-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionDivisionRatio:I

    goto/16 :goto_3

    :cond_4
    sget v2, Landroid/os/statistics/PerfSupervisionSettings;->DEFAULT_LEVEL:I

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionLevel:I

    goto :goto_4

    :cond_5
    sget v2, Landroid/os/statistics/PerfSupervisionSettings;->DEFAULT_SOFT_THRESHOLD_MS:I

    sput v2, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionSoftThreshold:I

    goto :goto_5

    :cond_6
    sput v8, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionHardThreshold:I

    goto :goto_6

    :cond_7
    sput v6, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionDivisionRatio:I

    goto/16 :goto_3
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSupervisionLevel()I
    .locals 1

    sget-boolean v0, Landroid/os/statistics/PerfSupervisionSettings;->sReadySupervision:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionLevel:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAboveHardThreshold(J)Z
    .locals 2

    sget v0, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionHardThreshold:I

    int-to-long v0, v0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInHeavyMode()Z
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Landroid/os/statistics/PerfSupervisionSettings;->sReadySupervision:Z

    if-eqz v1, :cond_0

    sget v1, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionLevel:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static isInTestMode()Z
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Landroid/os/statistics/PerfSupervisionSettings;->sReadySupervision:Z

    if-eqz v1, :cond_0

    sget v1, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionLevel:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static isSupervisionOn()Z
    .locals 2

    const/4 v0, 0x1

    invoke-static {}, Landroid/os/statistics/PerfSupervisionSettings;->getSupervisionLevel()I

    move-result v1

    if-lt v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static notifySupervisionReady()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Landroid/os/statistics/PerfSupervisionSettings;->sReadySupervision:Z

    return-void
.end method

.method static parseIntWithDefault(Ljava/lang/String;I)I
    .locals 2

    move v1, p1

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    goto :goto_0
.end method
