.class public final Landroid/miui/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/miui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final android_config_criticalBatteryWarningLevel:I = 0x11070001

.field public static final android_config_longPressOnPowerBehavior:I = 0x11070006

.field public static final android_config_lowBatteryCloseWarningLevel:I = 0x11070002

.field public static final android_config_lowBatteryWarningLevel:I = 0x11070003

.field public static final android_config_screenBrightnessDim:I = 0x11070004

.field public static final android_config_screenBrightnessSettingMinimum:I = 0x11070005

.field public static final anti_stranger_call_enabled:I = 0x1107000c

.field public static final config_backlightBit:I = 0x1107001a

.field public static final config_buttonBrightnessSettingDefault:I = 0x11070013

.field public static final config_ct_volte_supported_mode:I = 0x1107001b

.field public static final config_defaultNotificationLedFreq:I = 0x1107000b

.field public static final config_mp3_record_quality:I = 0x1107000e

.field public static final config_nightLightBrightnessDefault:I = 0x11070016

.field public static final config_nightLightBrightnessMaximum:I = 0x11070014

.field public static final config_nightLightBrightnessMinimum:I = 0x11070015

.field public static final config_nightLightColorDefault:I = 0x11070019

.field public static final config_nightLightColorMaximum:I = 0x11070017

.field public static final config_nightLightColorMinimum:I = 0x11070018

.field public static final config_show_rounded_corners_default:I = 0x1107000d

.field public static final config_smartcover_smallwindow_bottom:I = 0x11070012

.field public static final config_smartcover_smallwindow_left:I = 0x1107000f

.field public static final config_smartcover_smallwindow_right:I = 0x11070011

.field public static final config_smartcover_smallwindow_top:I = 0x11070010

.field public static final edit_fixed_position:I = 0x11070000

.field public static final expanded_status_bar_toggles_column:I = 0x11070007

.field public static final expanded_status_bar_toggles_land_column:I = 0x11070008

.field public static final expanded_status_bar_toggles_single_page_column:I = 0x11070009

.field public static final expanded_status_bar_toggles_single_page_land_column:I = 0x1107000a

.field public static final securespaces_extension_accelerometer_majorVersion:I = 0x1107001e

.field public static final securespaces_extension_accelerometer_minorVersion:I = 0x1107001f

.field public static final securespaces_extension_delete_userdata_majorVersion:I = 0x11070020

.field public static final securespaces_extension_delete_userdata_minorVersion:I = 0x11070021

.field public static final securespaces_extension_encryption_majorVersion:I = 0x11070022

.field public static final securespaces_extension_encryption_minorVersion:I = 0x11070023

.field public static final securespaces_extension_hidden_majorVersion:I = 0x11070024

.field public static final securespaces_extension_hidden_minorVersion:I = 0x11070025

.field public static final securespaces_extension_read_cross_space_contacts_majorVersion:I = 0x11070026

.field public static final securespaces_extension_read_cross_space_contacts_minorVersion:I = 0x11070027

.field public static final shutdown_progress_frames_count:I = 0x1107001c

.field public static final shutdown_progress_frames_duration:I = 0x1107001d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
