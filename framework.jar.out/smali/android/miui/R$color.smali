.class public final Landroid/miui/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/miui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final android_config_defaultNotificationColor:I = 0x11060002

.field public static final auto_disable_screenbuttons_float_text_color:I = 0x11060013

.field public static final boot_msg_textcolor:I = 0x11060009

.field public static final color_radiobutton:I = 0x11060023

.field public static final delete_system_update_cancel:I = 0x1106001a

.field public static final delete_system_update_delete:I = 0x1106001b

.field public static final divider_color:I = 0x1106001d

.field public static final fc_dialog_body_text_color:I = 0x1106000f

.field public static final float_notification_button_color:I = 0x11060024

.field public static final float_notification_button_color_disable:I = 0x1106000c

.field public static final float_notification_button_color_n:I = 0x1106000a

.field public static final float_notification_button_color_p:I = 0x1106000b

.field public static final global_action_item_text_color_normal:I = 0x11060006

.field public static final global_action_item_text_color_pressed:I = 0x11060005

.field public static final global_actions_item_text_view_color:I = 0x11060025

.field public static final key_text_normal:I = 0x1106000d

.field public static final key_text_pressed:I = 0x1106000e

.field public static final keyboard_key_text:I = 0x11060026

.field public static final keyboard_preview_text:I = 0x1106001c

.field public static final miui_boot_massage_dialog_background_color:I = 0x11060014

.field public static final package_delete_confirm_bg:I = 0x11060017

.field public static final package_delete_confirm_title:I = 0x11060018

.field public static final package_delete_confirm_warning_info:I = 0x11060019

.field public static final pattern_lockscreen_paint_color:I = 0x11060003

.field public static final pattern_lockscreen_paint_error_color:I = 0x11060004

.field public static final power_off_dialog_background_cover_color:I = 0x11060007

.field public static final realtimeblur_bg:I = 0x11060010

.field public static final realtimeblur_bg_oled:I = 0x11060011

.field public static final resolver_icon_mask:I = 0x11060012

.field public static final silent_mode_remaintime_1_color:I = 0x1106001f

.field public static final silent_mode_title:I = 0x1106001e

.field public static final status_bar_toggle_off_color:I = 0x11060001

.field public static final status_bar_toggle_on_color:I = 0x11060000

.field public static final switch_dialog_bg:I = 0x11060022

.field public static final text_edit_action_popup_text:I = 0x11060027

.field public static final text_edit_action_popup_text_normal:I = 0x11060015

.field public static final text_edit_action_popup_text_pressed:I = 0x11060016

.field public static final time_not_selected:I = 0x11060021

.field public static final time_selected:I = 0x11060020

.field public static final volume_panel_text_color:I = 0x11060008


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
