.class public final Landroid/miui/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/miui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final activity_package_delete_confirm_title:I = 0x110800d9

.field public static final add_frequent_phrases:I = 0x11080071

.field public static final all_call_livetalk_enable:I = 0x110800b1

.field public static final all_call_livetalk_remain_mins:I = 0x110800b2

.field public static final always:I = 0x110800eb

.field public static final alwaysUsePrompt:I = 0x11080072

.field public static final android_activity_resolver_use_always:I = 0x1108003e

.field public static final android_activity_resolver_use_once:I = 0x1108003f

.field public static final android_add_account_label:I = 0x11080059

.field public static final android_aerr_application:I = 0x1108002a

.field public static final android_aerr_process:I = 0x1108002b

.field public static final android_aerr_title:I = 0x1108002c

.field public static final android_anr_activity_application:I = 0x1108002d

.field public static final android_anr_activity_process:I = 0x1108002f

.field public static final android_anr_application_process:I = 0x1108002e

.field public static final android_anr_process:I = 0x11080030

.field public static final android_anr_title:I = 0x11080031

.field public static final android_bugreport_message:I = 0x1108004d

.field public static final android_byteShort:I = 0x1108005a

.field public static final android_common_last_name_prefixes:I = 0x11080057

.field public static final android_common_name_conjunctions:I = 0x11080055

.field public static final android_common_name_prefixes:I = 0x11080056

.field public static final android_common_name_suffixes:I = 0x11080058

.field public static final android_fast_scroll_alphabet:I = 0x11080049

.field public static final android_force_close:I = 0x11080032

.field public static final android_forward_intent_to_owner:I = 0x110800fc

.field public static final android_forward_intent_to_work:I = 0x110800fd

.field public static final android_gigabyteShort:I = 0x1108004b

.field public static final android_kilobyteShort:I = 0x1108005b

.field public static final android_lockscreen_emergency_call:I = 0x11080045

.field public static final android_lockscreen_forgot_pattern_button_text:I = 0x11080046

.field public static final android_lockscreen_missing_sim_message_short:I = 0x11080040

.field public static final android_megabyteShort:I = 0x11080047

.field public static final android_noApplications:I = 0x1108003d

.field public static final android_power_off:I = 0x1108003a

.field public static final android_reboot_safemode_confirm:I = 0x11080036

.field public static final android_reboot_safemode_title:I = 0x11080035

.field public static final android_report:I = 0x11080033

.field public static final android_safe_media_volume_warning:I = 0x11080054

.field public static final android_shutdown_confirm:I = 0x11080038

.field public static final android_shutdown_confirm_question:I = 0x11080037

.field public static final android_shutdown_progress:I = 0x11080039

.field public static final android_ssl_certificate:I = 0x11080048

.field public static final android_storage_usb:I = 0x11080041

.field public static final android_usb_cd_installer_notification_title:I = 0x11080044

.field public static final android_usb_mtp_notification_title:I = 0x11080043

.field public static final android_usb_ptp_notification_title:I = 0x11080042

.field public static final android_volume_alarm:I = 0x1108004e

.field public static final android_volume_icon_description_bluetooth:I = 0x1108004f

.field public static final android_volume_icon_description_incall:I = 0x11080050

.field public static final android_volume_icon_description_media:I = 0x11080051

.field public static final android_volume_icon_description_notification:I = 0x11080052

.field public static final android_volume_icon_description_ringer:I = 0x11080053

.field public static final android_wait:I = 0x11080034

.field public static final android_whichApplication:I = 0x1108003c

.field public static final android_whichApplicationNamed:I = 0x110800fe

.field public static final android_whichEditApplication:I = 0x11080101

.field public static final android_whichEditApplicationNamed:I = 0x11080102

.field public static final android_whichHomeApplication:I = 0x1108003b

.field public static final android_whichHomeApplicationNamed:I = 0x11080103

.field public static final android_whichSendApplication:I = 0x11080104

.field public static final android_whichSendApplicationNamed:I = 0x11080105

.field public static final android_whichViewApplication:I = 0x110800ff

.field public static final android_whichViewApplicationNamed:I = 0x11080100

.field public static final android_wifi_tether_configure_ssid_default:I = 0x1108004a

.field public static final android_wireless_display_route_description:I = 0x1108004c

.field public static final apply_password_action:I = 0x110800bb

.field public static final apply_password_confirm_dlg_msg:I = 0x110800ba

.field public static final apply_password_confirm_dlg_title:I = 0x110800b9

.field public static final auto_disable_screenbuttons_disable_toast_text:I = 0x110800cb

.field public static final auto_disable_screenbuttons_enable_toast_text:I = 0x110800ca

.field public static final auto_disable_screenbuttons_float_text:I = 0x110800c9

.field public static final auto_disable_screenbuttons_prompts_message:I = 0x110800ce

.field public static final auto_disable_screenbuttons_prompts_not_show_again:I = 0x110800d0

.field public static final auto_disable_screenbuttons_prompts_ok:I = 0x110800cf

.field public static final auto_disable_screenbuttons_prompts_title:I = 0x110800cd

.field public static final auto_disable_screenbuttons_tap_again_exit_toast_text:I = 0x110800cc

.field public static final auto_start_warning_allow:I = 0x11080078

.field public static final auto_start_warning_deny:I = 0x11080079

.field public static final auto_start_warning_title:I = 0x11080077

.field public static final autofill:I = 0x11080076

.field public static final button_text_cancel:I = 0x110800da

.field public static final button_text_delete:I = 0x110800db

.field public static final button_text_delete_timer:I = 0x110800de

.field public static final button_text_next_step:I = 0x110800dd

.field public static final button_text_next_step_timer:I = 0x110800dc

.field public static final call_record_or:I = 0x1108007b

.field public static final call_record_sample_prefix_miui:I = 0x1108007a

.field public static final caller_info_name_delimiter:I = 0x1108007c

.field public static final cancel_action:I = 0x110800b5

.field public static final close:I = 0x110800ea

.field public static final config_xiaoaiComponent:I = 0x1108005c

.field public static final configure_input_methods:I = 0x110800f1

.field public static final copy:I = 0x1108006d

.field public static final cut:I = 0x1108006c

.field public static final default_name:I = 0x110800bf

.field public static final description_title:I = 0x110800e7

.field public static final eighthours:I = 0x110800ef

.field public static final enable_livetalk_summary:I = 0x110800b4

.field public static final enable_livetalk_title:I = 0x110800b3

.field public static final fc_dialog_body:I = 0x110800ac

.field public static final fc_dialog_jump_to_preview:I = 0x110800ad

.field public static final first_back_long_press_message:I = 0x1108007d

.field public static final fm_record_smaple_prefix_miui:I = 0x11080080

.field public static final force_exit_message:I = 0x1108007e

.field public static final force_exit_skip_message:I = 0x1108007f

.field public static final fourhours:I = 0x110800ee

.field public static final fp_nav_center_action_choose_dlg_msg:I = 0x110800c6

.field public static final fp_nav_center_action_choose_dlg_negative_btn_text:I = 0x110800c8

.field public static final fp_nav_center_action_choose_dlg_positive_btn_text:I = 0x110800c7

.field public static final fp_nav_center_action_choose_dlg_title:I = 0x110800c5

.field public static final frequent_phrases:I = 0x11080070

.field public static final global_actions_airplane_mode:I = 0x11080081

.field public static final global_actions_reboot:I = 0x11080082

.field public static final global_actions_shutdown:I = 0x11080083

.field public static final global_actions_silent_mode:I = 0x11080084

.field public static final group_name_contacts:I = 0x110800c1

.field public static final group_name_coworkers:I = 0x110800c4

.field public static final group_name_family:I = 0x110800c3

.field public static final group_name_friends:I = 0x110800c2

.field public static final handy_mode_enter_dlg_msg:I = 0x110800a3

.field public static final handy_mode_enter_dlg_title:I = 0x110800a2

.field public static final handy_mode_summary:I = 0x110800a1

.field public static final handy_mode_title:I = 0x110800a0

.field public static final ignore_home_when_fingerprint_enrolling_msg:I = 0x110800bd

.field public static final internal_livetalk_enable:I = 0x110800af

.field public static final international_livetalk_enable:I = 0x110800b0

.field public static final key_btn_ok:I = 0x11080088

.field public static final key_btn_space:I = 0x11080087

.field public static final livetalk_not_enable:I = 0x110800ae

.field public static final location_usage_restricted_body:I = 0x110800ab

.field public static final location_usage_restricted_title:I = 0x110800aa

.field public static final lockscreen_access_pattern_cell_added:I = 0x1108008b

.field public static final lockscreen_access_pattern_cleared:I = 0x1108008a

.field public static final lockscreen_access_pattern_detected:I = 0x1108008c

.field public static final lockscreen_access_pattern_start:I = 0x11080089

.field public static final maml_component_customization_title:I = 0x110800be

.field public static final memory_clear_nothing_result:I = 0x1108008e

.field public static final memory_clear_result:I = 0x1108008d

.field public static final miui:I = 0x1108005d

.field public static final miui_screen_ratio_hint:I = 0x110800f3

.field public static final miui_screen_ratio_hint_dont_show_again:I = 0x110800f5

.field public static final miui_screen_ratio_hint_go_to_settings:I = 0x110800f7

.field public static final miui_screen_ratio_hint_message:I = 0x110800f4

.field public static final miui_screen_ratio_hint_ok:I = 0x110800f6

.field public static final miui_upgrading_notification_title:I = 0x110800f2

.field public static final onehour:I = 0x110800ec

.field public static final package_delete_confirm_step_1:I = 0x110800df

.field public static final package_delete_confirm_step_2:I = 0x110800e0

.field public static final package_delete_confirm_step_3:I = 0x110800e1

.field public static final paste:I = 0x1108006e

.field public static final perm_send_push_dspt:I = 0x11080068

.field public static final perm_send_push_lab:I = 0x11080069

.field public static final permdesc_manageLocationPolicy:I = 0x110800a9

.field public static final permlab_manageLocationPolicy:I = 0x110800a8

.field public static final reboot_confirm_title:I = 0x11080091

.field public static final reboot_info_auto_boot:I = 0x11080085

.field public static final reboot_info_shutdown_alarm:I = 0x11080086

.field public static final reboot_progress:I = 0x11080092

.field public static final remain:I = 0x110800f0

.field public static final replace:I = 0x1108006f

.field public static final resource_select:I = 0x110800c0

.field public static final screen_button_notification_text:I = 0x11080098

.field public static final screen_button_notification_title:I = 0x11080097

.field public static final screen_on_proximity_sensor_hint:I = 0x11080095

.field public static final screen_on_proximity_sensor_hint_has_navigation_bar:I = 0x11080096

.field public static final screen_on_proximity_sensor_summary:I = 0x11080094

.field public static final screen_on_proximity_sensor_title:I = 0x11080093

.field public static final screen_paper_mode_enter_toast:I = 0x11080099

.field public static final screen_rotation_freeze_message:I = 0x11080029

.field public static final screenshot_gesture_detect_alert_button:I = 0x110800a6

.field public static final screenshot_gesture_detect_alert_dont_again:I = 0x110800a7

.field public static final screenshot_gesture_detect_alert_msg:I = 0x110800a5

.field public static final screenshot_gesture_detect_alert_title:I = 0x110800a4

.field public static final securespaces_extension_accelerometer_name:I = 0x1108005e

.field public static final securespaces_extension_accelerometer_serviceName:I = 0x1108005f

.field public static final securespaces_extension_delete_userdata_name:I = 0x11080060

.field public static final securespaces_extension_delete_userdata_serviceName:I = 0x11080061

.field public static final securespaces_extension_encryption_name:I = 0x11080062

.field public static final securespaces_extension_encryption_serviceName:I = 0x11080063

.field public static final securespaces_extension_hidden_name:I = 0x11080064

.field public static final securespaces_extension_hidden_serviceName:I = 0x11080065

.field public static final securespaces_extension_read_cross_space_contacts_name:I = 0x11080066

.field public static final securespaces_extension_read_cross_space_contacts_serviceName:I = 0x11080067

.field public static final security_permission_denied_text:I = 0x110800d2

.field public static final security_permission_failed_text:I = 0x110800d3

.field public static final security_permission_validate_warning:I = 0x110800d5

.field public static final security_permission_wifi_and_data_opened_text:I = 0x110800d6

.field public static final security_permission_xiaomi_account_missed_text:I = 0x110800d4

.field public static final select:I = 0x1108006a

.field public static final select_all:I = 0x1108006b

.field public static final set_password_action:I = 0x110800b6

.field public static final set_password_message:I = 0x110800b8

.field public static final set_password_title:I = 0x110800b7

.field public static final shutdown_confirm_title:I = 0x1108008f

.field public static final shutdown_progress:I = 0x11080090

.field public static final silence_mode:I = 0x110800e2

.field public static final sim_added_reboot_confirm:I = 0x1108009b

.field public static final sim_added_reboot_title:I = 0x1108009a

.field public static final softap_notifif_summary:I = 0x1108009d

.field public static final softap_notifif_title:I = 0x1108009c

.field public static final standard:I = 0x110800e3

.field public static final standard_description:I = 0x110800e8

.field public static final standard_title:I = 0x110800e5

.field public static final start_security_permission_verify_text:I = 0x110800d1

.field public static final status_bar_toggle_auto_brightness:I = 0x11080001

.field public static final status_bar_toggle_battery_saver:I = 0x11080015

.field public static final status_bar_toggle_battery_saver_action:I = 0x11080026

.field public static final status_bar_toggle_bluetooth:I = 0x11080000

.field public static final status_bar_toggle_bluetooth_action:I = 0x11080018

.field public static final status_bar_toggle_brightness_action:I = 0x11080019

.field public static final status_bar_toggle_cast:I = 0x11080013

.field public static final status_bar_toggle_data:I = 0x11080002

.field public static final status_bar_toggle_data_action:I = 0x11080017

.field public static final status_bar_toggle_edit:I = 0x11080014

.field public static final status_bar_toggle_flight_mode:I = 0x11080003

.field public static final status_bar_toggle_flight_mode_action:I = 0x1108001a

.field public static final status_bar_toggle_gps:I = 0x11080004

.field public static final status_bar_toggle_gps_action:I = 0x1108001b

.field public static final status_bar_toggle_lock:I = 0x11080005

.field public static final status_bar_toggle_midrop:I = 0x11080012

.field public static final status_bar_toggle_midrop_action:I = 0x11080025

.field public static final status_bar_toggle_mute:I = 0x11080006

.field public static final status_bar_toggle_mute_action:I = 0x1108001c

.field public static final status_bar_toggle_nfc:I = 0x11080016

.field public static final status_bar_toggle_nfc_action:I = 0x11080027

.field public static final status_bar_toggle_paper_mode:I = 0x11080011

.field public static final status_bar_toggle_paper_mode_action:I = 0x11080024

.field public static final status_bar_toggle_power_mode:I = 0x1108000e

.field public static final status_bar_toggle_power_mode_action:I = 0x11080021

.field public static final status_bar_toggle_quiet_mode:I = 0x11080010

.field public static final status_bar_toggle_quiet_mode_action:I = 0x11080023

.field public static final status_bar_toggle_rotate:I = 0x11080007

.field public static final status_bar_toggle_rotate_action:I = 0x1108001d

.field public static final status_bar_toggle_screen_button:I = 0x11080008

.field public static final status_bar_toggle_screenshot:I = 0x11080009

.field public static final status_bar_toggle_silent_mode_action:I = 0x11080028

.field public static final status_bar_toggle_sync:I = 0x1108000a

.field public static final status_bar_toggle_sync_action:I = 0x1108001e

.field public static final status_bar_toggle_torch:I = 0x1108000b

.field public static final status_bar_toggle_vibrate:I = 0x1108000c

.field public static final status_bar_toggle_vibrate_action:I = 0x1108001f

.field public static final status_bar_toggle_wifi:I = 0x1108000d

.field public static final status_bar_toggle_wifi_action:I = 0x11080020

.field public static final status_bar_toggle_wifi_ap:I = 0x1108000f

.field public static final status_bar_toggle_wifi_ap_action:I = 0x11080022

.field public static final subinfo_default_name:I = 0x110800bc

.field public static final switch_brand_desc_tv:I = 0x110800fa

.field public static final switch_desc_tv:I = 0x110800f9

.field public static final switching_tv:I = 0x110800f8

.field public static final total:I = 0x110800e4

.field public static final total_description:I = 0x110800e9

.field public static final total_title:I = 0x110800e6

.field public static final translation_more:I = 0x11080075

.field public static final translation_query_error:I = 0x11080073

.field public static final translation_query_error_network:I = 0x11080074

.field public static final turn_off:I = 0x1108009e

.field public static final twohours:I = 0x110800ed

.field public static final unknown_caller:I = 0x1108009f

.field public static final user_switching_dialog_text:I = 0x110800d8

.field public static final xspace_resolver_activity_title:I = 0x110800d7


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
