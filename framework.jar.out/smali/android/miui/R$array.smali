.class public final Landroid/miui/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/miui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final airtel_mcc_mnc:I = 0x1109000f

.field public static final android_common_nicknames:I = 0x11090006

.field public static final android_config_keyboardTapVibePattern:I = 0x11090000

.field public static final android_config_longPressVibePattern:I = 0x11090001

.field public static final android_config_mobile_hotspot_provision_app:I = 0x11090005

.field public static final android_config_virtualKeyVibePattern:I = 0x11090002

.field public static final android_special_locale_codes:I = 0x11090003

.field public static final android_special_locale_names:I = 0x11090004

.field public static final boot_msg_text:I = 0x11090009

.field public static final config_crossSpaceNotificationPackageNames:I = 0x11090018

.field public static final config_manualBrightnessRemapIn:I = 0x1109001c

.field public static final config_manualBrightnessRemapOut:I = 0x1109001d

.field public static final config_maxBrightnessRemapIn:I = 0x1109001a

.field public static final config_maxBrightnessRemapOut:I = 0x1109001b

.field public static final config_sysBrightnessFileArray:I = 0x11090019

.field public static final config_virtualKeyUpPattern:I = 0x1109000a

.field public static final delay_boot_persistent_list:I = 0x11090015

.field public static final float_notification_whitelist:I = 0x11090007

.field public static final font_size_scale:I = 0x1109000c

.field public static final keyguard_whitelist:I = 0x11090008

.field public static final max_aspect_ratio_suggest_apps:I = 0x11090010

.field public static final need_trace_list:I = 0x11090014

.field public static final organized_languages_default:I = 0x1109000e

.field public static final organized_languages_international:I = 0x1109000d

.field public static final process_always_protect_list:I = 0x11090016

.field public static final process_disable_force_stop_list:I = 0x11090013

.field public static final process_disable_trim_list:I = 0x11090012

.field public static final process_secretly_protect_list:I = 0x11090017

.field public static final process_static_white_list:I = 0x11090011

.field public static final screen_key_long_press_default_action:I = 0x1109000b

.field public static final screen_resolution_supported:I = 0x11090024

.field public static final securespaces_extension_accelerometer_userRestrictions:I = 0x1109001e

.field public static final securespaces_extension_delete_userdata_userRestrictions:I = 0x1109001f

.field public static final securespaces_extension_encryption_userRestrictions:I = 0x11090020

.field public static final securespaces_extension_hidden_userRestrictions:I = 0x11090021

.field public static final securespaces_extension_read_cross_space_contacts_authorities:I = 0x11090023

.field public static final securespaces_extension_read_cross_space_contacts_userRestrictions:I = 0x11090022


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
