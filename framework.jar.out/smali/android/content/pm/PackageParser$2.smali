.class final Landroid/content/pm/PackageParser$2;
.super Ljava/lang/Object;
.source "PackageParser.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/content/pm/PackageParser;->collectCertificates(Landroid/content/pm/PackageParser$Package;Ljava/io/File;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$apkPath:Ljava/lang/String;

.field final synthetic val$entry:Ljava/util/zip/ZipEntry;

.field final synthetic val$pkg:Landroid/content/pm/PackageParser$Package;

.field final synthetic val$sJarFile:Landroid/util/jar/StrictJarFile;


# direct methods
.method constructor <init>(Landroid/util/jar/StrictJarFile;Ljava/util/zip/ZipEntry;Ljava/lang/String;Landroid/content/pm/PackageParser$Package;)V
    .locals 0

    iput-object p1, p0, Landroid/content/pm/PackageParser$2;->val$sJarFile:Landroid/util/jar/StrictJarFile;

    iput-object p2, p0, Landroid/content/pm/PackageParser$2;->val$entry:Ljava/util/zip/ZipEntry;

    iput-object p3, p0, Landroid/content/pm/PackageParser$2;->val$apkPath:Ljava/lang/String;

    iput-object p4, p0, Landroid/content/pm/PackageParser$2;->val$pkg:Landroid/content/pm/PackageParser$Package;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    :try_start_0
    iget-object v5, p0, Landroid/content/pm/PackageParser$2;->val$sJarFile:Landroid/util/jar/StrictJarFile;

    iget-object v6, p0, Landroid/content/pm/PackageParser$2;->val$entry:Ljava/util/zip/ZipEntry;

    invoke-static {v5, v6}, Landroid/content/pm/PackageParser;->-wrap3(Landroid/util/jar/StrictJarFile;Ljava/util/zip/ZipEntry;)[[Ljava/security/cert/Certificate;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/util/ArrayUtils;->isEmpty([Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, Landroid/content/pm/PackageParser$PackageParserException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Package "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Landroid/content/pm/PackageParser$2;->val$apkPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " has no certificates at entry "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Landroid/content/pm/PackageParser$2;->val$entry:Ljava/util/zip/ZipEntry;

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v7, -0x67

    invoke-direct {v5, v7, v6}, Landroid/content/pm/PackageParser$PackageParserException;-><init>(ILjava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/pm/PackageParser$PackageParserException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v1

    invoke-static {}, Landroid/content/pm/PackageParser;->-get0()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    const/16 v5, -0x69

    :try_start_1
    invoke-static {v5}, Landroid/content/pm/PackageParser;->-set1(I)I

    invoke-static {v1}, Landroid/content/pm/PackageParser;->-set0(Ljava/lang/Exception;)Ljava/lang/Exception;

    invoke-static {}, Landroid/content/pm/PackageParser;->-get2()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Landroid/content/pm/PackageParser;->-get0()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :cond_0
    :goto_0
    monitor-exit v6

    :goto_1
    return-void

    :cond_1
    :try_start_2
    invoke-static {v2}, Landroid/content/pm/PackageParser;->-wrap0([[Ljava/security/cert/Certificate;)[Landroid/content/pm/Signature;

    move-result-object v3

    invoke-static {}, Landroid/content/pm/PackageParser;->-get0()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6
    :try_end_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/content/pm/PackageParser$PackageParserException; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    iget-object v5, p0, Landroid/content/pm/PackageParser$2;->val$pkg:Landroid/content/pm/PackageParser$Package;

    iget-object v5, v5, Landroid/content/pm/PackageParser$Package;->mCertificates:[[Ljava/security/cert/Certificate;

    if-nez v5, :cond_2

    iget-object v5, p0, Landroid/content/pm/PackageParser$2;->val$pkg:Landroid/content/pm/PackageParser$Package;

    iput-object v2, v5, Landroid/content/pm/PackageParser$Package;->mCertificates:[[Ljava/security/cert/Certificate;

    iget-object v5, p0, Landroid/content/pm/PackageParser$2;->val$pkg:Landroid/content/pm/PackageParser$Package;

    iput-object v3, v5, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    iget-object v5, p0, Landroid/content/pm/PackageParser$2;->val$pkg:Landroid/content/pm/PackageParser$Package;

    new-instance v7, Landroid/util/ArraySet;

    invoke-direct {v7}, Landroid/util/ArraySet;-><init>()V

    iput-object v7, v5, Landroid/content/pm/PackageParser$Package;->mSigningKeys:Landroid/util/ArraySet;

    const/4 v4, 0x0

    :goto_2
    array-length v5, v2

    if-ge v4, v5, :cond_3

    iget-object v5, p0, Landroid/content/pm/PackageParser$2;->val$pkg:Landroid/content/pm/PackageParser$Package;

    iget-object v5, v5, Landroid/content/pm/PackageParser$Package;->mSigningKeys:Landroid/util/ArraySet;

    aget-object v7, v2, v4

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    iget-object v5, p0, Landroid/content/pm/PackageParser$2;->val$pkg:Landroid/content/pm/PackageParser$Package;

    iget-object v5, v5, Landroid/content/pm/PackageParser$Package;->mSignatures:[Landroid/content/pm/Signature;

    invoke-static {v5, v3}, Landroid/content/pm/Signature;->areExactMatch([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)Z

    move-result v5

    if-nez v5, :cond_3

    new-instance v5, Landroid/content/pm/PackageParser$PackageParserException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Package "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Landroid/content/pm/PackageParser$2;->val$apkPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " has mismatched certificates at entry "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Landroid/content/pm/PackageParser$2;->val$entry:Ljava/util/zip/ZipEntry;

    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/16 v8, -0x68

    invoke-direct {v5, v8, v7}, Landroid/content/pm/PackageParser$PackageParserException;-><init>(ILjava/lang/String;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v5

    :try_start_4
    monitor-exit v6

    throw v5
    :try_end_4
    .catch Ljava/security/GeneralSecurityException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/content/pm/PackageParser$PackageParserException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    invoke-static {}, Landroid/content/pm/PackageParser;->-get0()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    const/16 v5, -0x66

    :try_start_5
    invoke-static {v5}, Landroid/content/pm/PackageParser;->-set1(I)I

    invoke-static {v0}, Landroid/content/pm/PackageParser;->-set0(Ljava/lang/Exception;)Ljava/lang/Exception;

    invoke-static {}, Landroid/content/pm/PackageParser;->-get2()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Landroid/content/pm/PackageParser;->-get0()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v5

    monitor-exit v6

    throw v5

    :cond_3
    :try_start_6
    monitor-exit v6
    :try_end_6
    .catch Ljava/security/GeneralSecurityException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/content/pm/PackageParser$PackageParserException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_1

    :catchall_2
    move-exception v5

    monitor-exit v6

    throw v5
.end method
