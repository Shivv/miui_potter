.class public interface abstract Lcom/miui/gamebooster/service/IMiuiFreeformService;
.super Ljava/lang/Object;
.source "IMiuiFreeformService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/gamebooster/service/IMiuiFreeformService$Stub;
    }
.end annotation


# virtual methods
.method public abstract moveTaskToStack(IIZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract resizeTask(ILandroid/graphics/Rect;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
