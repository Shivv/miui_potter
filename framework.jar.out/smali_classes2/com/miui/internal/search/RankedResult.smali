.class public Lcom/miui/internal/search/RankedResult;
.super Landroid/database/MatrixCursor;
.source "RankedResult.java"


# instance fields
.field private mArrayIndex:[I

.field private mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mScores:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/miui/internal/search/RankedResult;->mScores:Ljava/util/TreeMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/internal/search/RankedResult;->mData:Ljava/util/ArrayList;

    return-void
.end method

.method private ensureData()V
    .locals 4

    iget-object v2, p0, Lcom/miui/internal/search/RankedResult;->mArrayIndex:[I

    if-eqz v2, :cond_0

    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/miui/internal/search/RankedResult;->mScores:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lcom/miui/internal/search/RankedResult$1;

    invoke-direct {v2, p0}, Lcom/miui/internal/search/RankedResult$1;-><init>(Lcom/miui/internal/search/RankedResult;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/miui/internal/search/RankedResult;->mArrayIndex:[I

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lcom/miui/internal/search/RankedResult;->mArrayIndex:[I

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public addRow([Ljava/lang/Object;D)V
    .locals 4

    iget-object v0, p0, Lcom/miui/internal/search/RankedResult;->mScores:Ljava/util/TreeMap;

    iget-object v1, p0, Lcom/miui/internal/search/RankedResult;->mScores:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/miui/internal/search/RankedResult;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/internal/search/RankedResult;->mArrayIndex:[I

    return-void
.end method

.method public get(I)[Ljava/lang/Object;
    .locals 2

    invoke-direct {p0}, Lcom/miui/internal/search/RankedResult;->ensureData()V

    iget-object v0, p0, Lcom/miui/internal/search/RankedResult;->mData:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/miui/internal/search/RankedResult;->mArrayIndex:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    return-object v0
.end method

.method public size()I
    .locals 1

    invoke-direct {p0}, Lcom/miui/internal/search/RankedResult;->ensureData()V

    iget-object v0, p0, Lcom/miui/internal/search/RankedResult;->mArrayIndex:[I

    array-length v0, v0

    return v0
.end method
