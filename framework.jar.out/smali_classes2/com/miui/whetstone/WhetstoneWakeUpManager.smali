.class public Lcom/miui/whetstone/WhetstoneWakeUpManager;
.super Ljava/lang/Object;
.source "WhetstoneWakeUpManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/whetstone/WhetstoneWakeUpManager$WakeUpHandler;
    }
.end annotation


# static fields
.field private static final DELAY_STOP_RTC_APP_TIME:I = 0x1388

.field public static final MAX_WAKE_UP_COUNT:I = 0x3

.field private static final MESSAGE_KILL_RTC_WAKE_UP_APP:I = 0x2

.field private static final MESSAGE_STOP_RTC_APP:I = 0x1

.field private static final TAG:Ljava/lang/String; = "WhetstoneWakeUpManager"

.field private static final THIRD_PARTY_RTC_WAKEUP_RECORD_PROP:Ljava/lang/String; = "persist.sys.rtc.wakeup_record"

.field private static volatile mInstance:Lcom/miui/whetstone/WhetstoneWakeUpManager;


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mContext:Landroid/content/Context;

.field private mDisableApp:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/miui/whetstone/WhetstoneWakeUpRecord;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0(Lcom/miui/whetstone/WhetstoneWakeUpManager;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    iput-object p1, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mAlarmManager:Landroid/app/AlarmManager;

    new-instance v0, Lcom/miui/whetstone/WhetstoneWakeUpManager$WakeUpHandler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/whetstone/WhetstoneWakeUpManager$WakeUpHandler;-><init>(Lcom/miui/whetstone/WhetstoneWakeUpManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mHandler:Landroid/os/Handler;

    sput-object p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mInstance:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mDisableApp:Ljava/util/ArrayList;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/miui/whetstone/WhetstoneWakeUpManager;
    .locals 2

    sget-object v0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mInstance:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/miui/whetstone/WhetstoneWakeUpManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mInstance:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/whetstone/WhetstoneWakeUpManager;

    invoke-direct {v0, p0}, Lcom/miui/whetstone/WhetstoneWakeUpManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mInstance:Lcom/miui/whetstone/WhetstoneWakeUpManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mInstance:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isBelongToBlackList(Ljava/lang/String;)Z
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_0

    return v3

    :cond_0
    iget-object v2, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mDisableApp:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    return v2

    :cond_2
    return v3
.end method


# virtual methods
.method public checkIfAppBeAllowedStartForWakeUpControl(ILjava/lang/String;Landroid/app/PendingIntent;)I
    .locals 12

    if-nez p2, :cond_0

    const-string/jumbo v8, "WhetstoneWakeUpManager"

    const-string/jumbo v9, "the packageName is null, so reject the start"

    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    return v8

    :cond_0
    invoke-direct {p0, p2}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->isBelongToBlackList(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string/jumbo v8, "WhetstoneWakeUpManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "the package belong to black list, so reject the start "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    return v8

    :cond_1
    const/4 v6, -0x1

    const/4 v0, 0x0

    iget-object v9, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v9

    :try_start_0
    iget-object v8, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;

    if-eqz v3, :cond_3

    iget-object v8, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->packageName:Ljava/lang/String;

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget v6, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->uid:I

    const/4 v0, 0x1

    :cond_4
    if-eqz v0, :cond_2

    :cond_5
    iget-object v8, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-nez v4, :cond_6

    if-lez v6, :cond_6

    const-string/jumbo v8, "WhetstoneWakeUpManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "dont get the rtc wakeUp info for uid "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v8, 0x0

    monitor-exit v9

    return v8

    :cond_6
    if-lez v6, :cond_c

    :try_start_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;

    if-eqz v3, :cond_a

    if-eqz p3, :cond_a

    iget-object v8, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->intent:Landroid/app/PendingIntent;

    invoke-virtual {p3, v8}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-virtual {v3}, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->increaseWakeUpCount()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v8, 0x3e8

    if-ne p1, v8, :cond_8

    const/4 v8, 0x0

    monitor-exit v9

    return v8

    :cond_8
    :try_start_2
    invoke-virtual {v3}, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->getWakeUpCount()I

    move-result v8

    const/4 v10, 0x3

    if-le v8, v10, :cond_9

    iget-object v8, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v8, p3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->status:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v8, 0x1

    monitor-exit v9

    return v8

    :cond_9
    :try_start_3
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    const/4 v8, 0x2

    iput v8, v2, Landroid/os/Message;->what:I

    iput p1, v2, Landroid/os/Message;->arg1:I

    iget-object v8, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mHandler:Landroid/os/Handler;

    const-wide/16 v10, 0x1388

    invoke-virtual {v8, v2, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v8, 0x2

    monitor-exit v9

    return v8

    :cond_a
    if-nez p3, :cond_7

    if-eqz v3, :cond_7

    :try_start_4
    iget-object v8, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->packageName:Ljava/lang/String;

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-virtual {v3}, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->increaseWakeUpCount()V

    invoke-virtual {v3}, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->getWakeUpCount()I

    move-result v8

    const/4 v10, 0x3

    if-le v8, v10, :cond_b

    const-string/jumbo v8, "WhetstoneWakeUpManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "the package excess max wake up count : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->status:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v8, 0x1

    monitor-exit v9

    return v8

    :cond_b
    :try_start_5
    const-string/jumbo v8, "WhetstoneWakeUpManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "the package allow to start in sleep mode : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    const/4 v8, 0x2

    iput v8, v2, Landroid/os/Message;->what:I

    iget v8, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->uid:I

    iput v8, v2, Landroid/os/Message;->arg1:I

    iget-object v8, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mHandler:Landroid/os/Handler;

    const-wide/16 v10, 0x1388

    invoke-virtual {v8, v2, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/4 v8, 0x2

    monitor-exit v9

    return v8

    :cond_c
    monitor-exit v9

    const/4 v8, 0x0

    return v8

    :catchall_0
    move-exception v8

    monitor-exit v9

    throw v8
.end method

.method public checkIfAppBelongToRtcWakeUp(ILjava/lang/String;)Z
    .locals 12

    const/4 v11, 0x0

    if-nez p2, :cond_0

    const-string/jumbo v7, "WhetstoneWakeUpManager"

    const-string/jumbo v8, "the packageName is null, so reject the start"

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v11

    :cond_0
    invoke-direct {p0, p2}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->isBelongToBlackList(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string/jumbo v7, "WhetstoneWakeUpManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "the package belong to black list, so reject the start "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v11

    :cond_1
    const/4 v5, -0x1

    const/4 v0, 0x0

    iget-object v8, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v8

    :try_start_0
    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/WhetstoneWakeUpRecord;

    if-eqz v2, :cond_3

    iget-object v7, v2, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->packageName:Ljava/lang/String;

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget v5, v2, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->uid:I

    const/4 v0, 0x1

    :cond_4
    if-eqz v0, :cond_2

    :cond_5
    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-nez v3, :cond_6

    if-lez v5, :cond_6

    const-string/jumbo v7, "WhetstoneWakeUpManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "dont get the rtc wakeUp info for uid "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v8

    return v11

    :cond_6
    if-lez v5, :cond_8

    :try_start_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/WhetstoneWakeUpRecord;

    if-eqz v2, :cond_7

    iget-object v7, v2, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->packageName:Ljava/lang/String;

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    if-eqz v7, :cond_7

    const/4 v7, 0x1

    monitor-exit v8

    return v7

    :cond_8
    monitor-exit v8

    return v11

    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7
.end method

.method public dump()V
    .locals 10

    const-string/jumbo v6, "WhetstoneWakeUpManager"

    const-string/jumbo v7, "===========dump the smart power wake up package info start"

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v1, ""

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;

    if-eqz v3, :cond_1

    const-string/jumbo v6, "WhetstoneWakeUpManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "the wake up record is :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget v8, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->uid:I

    invoke-virtual {v6, v8}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    array-length v6, v2

    if-lez v6, :cond_2

    const/4 v6, 0x0

    aget-object v1, v2, v6

    :cond_2
    const-string/jumbo v6, "WhetstoneWakeUpManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "the package name is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_3
    monitor-exit v7

    const-string/jumbo v6, "WhetstoneWakeUpManager"

    const-string/jumbo v7, "===========dump the smart power wake up package info end"

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 9

    const-string/jumbo v6, "===========dump the smart power wake up package info start"

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v1, ""

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;

    if-eqz v3, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "the wake up record is :"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget v8, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->uid:I

    invoke-virtual {v6, v8}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    array-length v6, v2

    if-lez v6, :cond_2

    const/4 v6, 0x0

    aget-object v1, v2, v6

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "the package name is : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_3
    monitor-exit v7

    const-string/jumbo v6, "===========dump the smart power wake up package info end"

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method public onSleepModeEnter(Z)V
    .locals 6

    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/whetstone/WhetstoneWakeUpRecord;

    if-eqz v1, :cond_1

    const/4 v4, 0x0

    iput v4, v1, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->wakeup_count:I

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->status:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    :cond_2
    monitor-exit v5

    return-void
.end method

.method public recordRTCWakeupInfo(ILandroid/app/PendingIntent;Z)V
    .locals 9

    const/4 v7, 0x0

    if-eqz p2, :cond_0

    const/16 v6, 0x3e8

    if-ne p1, v6, :cond_1

    :cond_0
    const-string/jumbo v6, "WhetstoneWakeUpManager"

    const-string/jumbo v7, "error:record rtc wakeUp info pending intent is null"

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v6, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, ""

    if-eqz v1, :cond_2

    array-length v6, v1

    if-lez v6, :cond_2

    aget-object v0, v1, v7

    :cond_2
    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    if-eqz p3, :cond_3

    if-nez v5, :cond_6

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/miui/whetstone/WhetstoneWakeUpRecord;

    const/4 v6, 0x0

    invoke-direct {v2, p1, v6, v0}, Lcom/miui/whetstone/WhetstoneWakeUpRecord;-><init>(ILandroid/app/PendingIntent;Ljava/lang/String;)V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mWakeUpInfoList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v7

    return-void

    :cond_3
    if-nez v5, :cond_4

    :try_start_1
    const-string/jumbo v6, "WhetstoneWakeUpManager"

    const-string/jumbo v8, "remove the wakeUp info, but wakeUp info is null"

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v7

    return-void

    :cond_4
    :try_start_2
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;

    iget v6, v3, Lcom/miui/whetstone/WhetstoneWakeUpRecord;->uid:I

    if-ne v6, p1, :cond_5

    invoke-interface {v5, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_6
    monitor-exit v7

    return-void
.end method

.method public setDisableRTCList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    const-string/jumbo v0, "WhetstoneWakeUpManager"

    const-string/jumbo v1, "disableRtcList == null, return"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string/jumbo v0, "WhetstoneWakeUpManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Disable RTC List : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager;->mDisableApp:Ljava/util/ArrayList;

    return-void
.end method
