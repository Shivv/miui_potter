.class Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;
.super Ljava/lang/Object;
.source "WhetstoneSleepCleanConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/whetstone/WhetstoneSleepCleanConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageInfo"
.end annotation


# instance fields
.field cleanType:I

.field limitStart:I

.field packageName:Ljava/lang/String;

.field final synthetic this$0:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;


# direct methods
.method constructor <init>(Lcom/miui/whetstone/WhetstoneSleepCleanConfig;Ljava/lang/String;II)V
    .locals 0

    iput-object p1, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->this$0:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    iput p3, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->cleanType:I

    iput p4, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->limitStart:I

    return-void
.end method

.method private cleanType2String(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "CLEAN_TYPE_ALL"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "CLEAN_TYPE_UI"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "CLEAN_TYPE_PROTECTED"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private limitStart2String(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "LIMIT_START_ALL_WAYS"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "LIMIT_START_EXCEPT_UI"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "LIMIT_START_NOT"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "packageName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", cleanType = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->cleanType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", limitStart = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->limitStart:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
