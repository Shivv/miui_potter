.class public Lcom/miui/whetstone/WhetstoneSleepCleanConfig;
.super Ljava/lang/Object;
.source "WhetstoneSleepCleanConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;
    }
.end annotation


# static fields
.field public static final CLEAN_TYPE_ALL:I = 0x0

.field public static final CLEAN_TYPE_PROTECTED:I = 0x2

.field public static final CLEAN_TYPE_UI:I = 0x1

.field public static final LIMIT_START_ALL_WAYS:I = 0x0

.field public static final LIMIT_START_EXCEPT_UI:I = 0x1

.field public static final LIMIT_START_NOT:I = 0x2

.field private static final STATE_DEEP_SLEEP:I = 0x2

.field private static final STATE_LIGHT_SLEEP1:I = 0x1

.field private static final STATE_LIGHT_SLEEP2:I = 0x3

.field private static final STATE_NO_SLEEP:I = 0x0

.field public static final TAG:Ljava/lang/String; = "WhetstoneSleepCleanConfig"


# instance fields
.field private mProtecedProcess:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSleepState:I

.field private mWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mSleepState:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/miui/whetstone/WhetstoneSleepCleanConfig;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mSleepState:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->getWhiteList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->getProtectedProcess()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addPackageControlCleanInfo2Config(Ljava/lang/String;I)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_0

    const-string/jumbo v3, "WhetstoneSleepCleanConfig"

    const-string/jumbo v4, "error, sleep mode packageName is null "

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v5

    :cond_0
    iget-object v3, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    iget-object v3, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput p2, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->cleanType:I

    const-string/jumbo v3, "WhetstoneSleepCleanConfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update the package "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " sleep mode info clean type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " the limit is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->limitStart:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    :cond_2
    new-instance v2, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    invoke-direct {v2, p0, p1, p2, v5}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;-><init>(Lcom/miui/whetstone/WhetstoneSleepCleanConfig;Ljava/lang/String;II)V

    iget-object v3, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return v6
.end method

.method public addPackageControlInfo2Config(Ljava/lang/String;II)Z
    .locals 7

    const/4 v6, 0x1

    if-nez p1, :cond_0

    const-string/jumbo v3, "WhetstoneSleepCleanConfig"

    const-string/jumbo v4, "error, sleep mode packageName is null "

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    return v3

    :cond_0
    iget-object v3, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    iget-object v3, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput p2, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->cleanType:I

    iput p3, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->limitStart:I

    const-string/jumbo v3, "WhetstoneSleepCleanConfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update the package "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " sleep mode info clean type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " the limit is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    :cond_2
    new-instance v2, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;-><init>(Lcom/miui/whetstone/WhetstoneSleepCleanConfig;Ljava/lang/String;II)V

    iget-object v3, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return v6
.end method

.method public addPackageControlStartInfo2Config(Ljava/lang/String;I)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_0

    const-string/jumbo v3, "WhetstoneSleepCleanConfig"

    const-string/jumbo v4, "error, sleep mode packageName is null "

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v5

    :cond_0
    iget-object v3, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    iget-object v3, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput p2, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->limitStart:I

    const-string/jumbo v3, "WhetstoneSleepCleanConfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update the package "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " sleep mode info clean type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->cleanType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " the limit is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    :cond_2
    new-instance v2, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    invoke-direct {v2, p0, p1, v5, p2}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;-><init>(Lcom/miui/whetstone/WhetstoneSleepCleanConfig;Ljava/lang/String;II)V

    iget-object v3, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return v6
.end method

.method public addProtectedProcessList(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addProtectedProcessList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    const-string/jumbo v0, "WhetstoneSleepCleanConfig"

    const-string/jumbo v1, "sleep mode add the process list is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    :cond_0
    const-string/jumbo v0, "WhetstoneSleepCleanConfig"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Process White List : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    return-void
.end method

.method public checkIfProcessBelongProtected(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return v8

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mSleepState:I

    if-eq v5, v7, :cond_2

    iget v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mSleepState:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_4

    :cond_2
    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    return v7

    :cond_4
    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    iget-object v5, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget v5, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->cleanType:I

    if-ne v5, v7, :cond_6

    if-eqz p3, :cond_6

    return v7

    :cond_6
    iget v5, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->cleanType:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    return v7

    :cond_7
    return v8
.end method

.method public checkIfProcessStartAllow(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return v8

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mSleepState:I

    if-eq v5, v7, :cond_2

    iget v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mSleepState:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_4

    :cond_2
    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    return v7

    :cond_4
    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    iget-object v5, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget v5, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->limitStart:I

    if-ne v5, v7, :cond_6

    xor-int/lit8 v5, p3, 0x1

    if-eqz v5, :cond_6

    return v7

    :cond_6
    iget v5, v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->limitStart:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    return v7

    :cond_7
    return v8
.end method

.method public clearData()V
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public dump()V
    .locals 6

    const-string/jumbo v4, "WhetstoneSleepCleanConfig"

    const-string/jumbo v5, "===================dump sleep mode config info======================"

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    const-string/jumbo v4, "WhetstoneSleepCleanConfig"

    invoke-virtual {v0}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string/jumbo v4, "WhetstoneSleepCleanConfig"

    const-string/jumbo v5, "dump the protected process list"

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v4, "WhetstoneSleepCleanConfig"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    const-string/jumbo v4, "===================dump sleep mode config info======================"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    invoke-virtual {v0}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string/jumbo v4, "dump the protected process list"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    return-void
.end method

.method public getProtectedProcess()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    return-object v0
.end method

.method public getWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    return-object v0
.end method

.method public setSleepState(I)V
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mSleepState:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    const-string/jumbo v4, "mWhiteList : \n"

    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig$PackageInfo;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "mProtecedProcess : \n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->mProtecedProcess:Ljava/util/List;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_1
    return-object v4
.end method
