.class public Lcom/miui/whetstone/component/ComponentPolicyInfo;
.super Ljava/lang/Object;
.source "ComponentPolicyInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/whetstone/component/ComponentPolicyInfo$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/miui/whetstone/component/ComponentPolicyInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG:Z

.field public static final POLICY_VALID_BEFORE_ANY_ACTIVITY_START:I = 0x4

.field public static final POLICY_VALID_BEFORE_SELF_ACTIVITY_START:I = 0x3

.field public static final POLICY_VALID_BEFORE_SELF_PROCESS_START:I = 0x2

.field public static final POLICY_VALID_PERSIST:I = 0x1

.field private static final TAG:Ljava/lang/String; = "WhetstoneComponentPolicy"

.field private static final mMutex:Ljava/lang/Object;


# instance fields
.field private mAvalidType:I

.field private mCheckFlag:I

.field private mCheckIntents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckMethod:I

.field private mCommonPolicyExceptPackage:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultResult:Z

.field private mEnable:Z

.field private mId:I

.field private mIsCommonPolicy:Z

.field private mIsRemovedWhenDisable:Z

.field private mMatchMethod:I

.field private mPkg:Ljava/lang/String;

.field private mPolicyModuleName:Ljava/lang/String;

.field private mProcessName:Ljava/lang/String;

.field private mSetter:Ljava/lang/String;

.field private mStartType:I

.field private mTimeOutForBroadCast:I

.field private mUserId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/miui/whetstone/component/WtComponentManager;->isDebug()Z

    move-result v0

    sput-boolean v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMutex:Ljava/lang/Object;

    new-instance v0, Lcom/miui/whetstone/component/ComponentPolicyInfo$1;

    invoke-direct {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo$1;-><init>()V

    sput-object v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPkg:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mSetter:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mDefaultResult:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMatchMethod:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckFlag:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPolicyModuleName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckMethod:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mProcessName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsRemovedWhenDisable:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsCommonPolicy:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mTimeOutForBroadCast:I

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    iput-object p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPkg:Ljava/lang/String;

    iput p2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    iput p3, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    iput-boolean v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mDefaultResult:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMatchMethod:I

    const-string/jumbo v0, "Unknown"

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mSetter:Ljava/lang/String;

    iput p4, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckFlag:I

    const-string/jumbo v0, "Unknown"

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPolicyModuleName:Ljava/lang/String;

    iput v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    iput v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckMethod:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mProcessName:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    iput-boolean v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsRemovedWhenDisable:Z

    iput-boolean v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsCommonPolicy:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    iput v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mTimeOutForBroadCast:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIZIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    iput-object p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPkg:Ljava/lang/String;

    iput p2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    iput p3, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    iput-boolean p4, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mDefaultResult:Z

    iput p6, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMatchMethod:I

    iput-object p11, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mSetter:Ljava/lang/String;

    iput p5, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckFlag:I

    iput-object p10, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPolicyModuleName:Ljava/lang/String;

    iput p7, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    iput p8, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckMethod:I

    if-nez p9, :cond_0

    const-string/jumbo p9, ""

    :cond_0
    iput-object p9, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mProcessName:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    iput-boolean v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsRemovedWhenDisable:Z

    iput-boolean v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsCommonPolicy:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    iput v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mTimeOutForBroadCast:I

    return-void
.end method

.method private checkFlagResult(II)Z
    .locals 4

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mDefaultResult:Z

    iget v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMatchMethod:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    sget-boolean v1, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "WhetstoneComponentPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "check flag result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " matchFlag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " checkFlag:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " MatchMethod:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMatchMethod:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " DefaultResult:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mDefaultResult:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v0

    :pswitch_1
    if-eq p2, p1, :cond_0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_2
    and-int v1, p2, p1

    if-nez v1, :cond_0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_3
    if-ne p2, p1, :cond_0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_4
    and-int v1, p2, p1

    if-eqz v1, :cond_0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public checkPolicyResult(Ljava/lang/String;IILandroid/content/Intent;Ljava/lang/String;)Z
    .locals 18

    sget-boolean v12, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v12, :cond_0

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Check Policy ID:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    if-nez v12, :cond_2

    sget-boolean v12, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v12, :cond_1

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Policy ID:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " is not enable"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v12, 0x1

    return v12

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsCommonPolicy:Z

    if-nez v12, :cond_4

    move-object/from16 v0, p0

    iget v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    move/from16 v0, p2

    if-eq v12, v0, :cond_4

    move-object/from16 v0, p0

    iget v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    const/4 v13, -0x1

    if-eq v12, v13, :cond_4

    sget-boolean v12, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v12, :cond_3

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Check Policy ID:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " not match"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v12, 0x1

    return v12

    :cond_4
    invoke-static {}, Lcom/miui/whetstone/component/WtComponentManager;->getInstance()Lcom/miui/whetstone/component/WtComponentManager;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mProcessName:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_6

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mProcessName:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    xor-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_6

    sget-boolean v12, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v12, :cond_5

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Check Policy ID:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " process check error"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const/4 v12, 0x1

    return v12

    :cond_6
    move-object/from16 v0, p0

    iget v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckMethod:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_8

    sget-boolean v12, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v12, :cond_7

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Check Policy ID:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " allow policy"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    const/4 v12, 0x1

    return v12

    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckFlag:I

    move/from16 v5, p3

    const/4 v7, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    const v12, 0x8000

    and-int v12, v12, p3

    if-eqz v12, :cond_9

    const v12, 0x8000

    xor-int/2addr v3, v12

    move-object/from16 v0, p0

    iget v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_9

    const v12, 0x8000

    xor-int v5, p3, v12

    const v7, 0x8000

    const/4 v2, 0x1

    :cond_9
    if-eqz v2, :cond_14

    if-eqz p4, :cond_a

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;

    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v12

    xor-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_b

    :cond_a
    const/4 v12, 0x1

    return v12

    :cond_b
    sget-boolean v12, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v12, :cond_c

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Check Policy ID:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " Intent:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    if-eqz v9, :cond_10

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/miui/whetstone/component/WtComponentManager;->getPackageComponentPolicy(Ljava/lang/String;)Lcom/miui/whetstone/component/PackageComponentPolicy;

    move-result-object v6

    if-eqz v6, :cond_10

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isIntentWhiteList(Landroid/content/Intent;)Z

    move-result v12

    if-eqz v12, :cond_e

    sget-boolean v12, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v12, :cond_d

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Check Policy ID:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " WhiteList Intent:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p4 .. p4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " for "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    const/4 v12, 0x1

    return v12

    :cond_e
    const/4 v12, 0x2

    invoke-virtual {v6, v12}, Lcom/miui/whetstone/component/PackageComponentPolicy;->getPackageStartTimeWithType(I)J

    move-result-wide v10

    sget-boolean v12, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v12, :cond_f

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Pkg:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " Process:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p5

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " broadcast start duration:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    sub-long/2addr v14, v10

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    const-wide/16 v12, 0x0

    cmp-long v12, v10, v12

    if-lez v12, :cond_13

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    sub-long/2addr v12, v10

    move-object/from16 v0, p0

    iget v14, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mTimeOutForBroadCast:I

    mul-int/lit8 v14, v14, 0x3c

    int-to-long v14, v14

    const-wide/16 v16, 0x3e8

    mul-long v14, v14, v16

    cmp-long v12, v12, v14

    if-lez v12, :cond_10

    const v4, 0x8000

    sget-boolean v12, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v12, :cond_10

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Pkg:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " Process:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p5

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " broadcast start time is enough"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    :goto_0
    move-object/from16 v0, p0

    iget v12, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_11

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "fontFlag:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " inputfontFlag:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " postFlag:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " inputPostFlag:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->checkFlagResult(II)Z

    move-result v12

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->checkFlagResult(II)Z

    move-result v13

    or-int v8, v12, v13

    if-nez v8, :cond_12

    const-string/jumbo v12, "WhetstoneComponentPolicy"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Process: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p5

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, "start is deny by Whetstone, PolicyId: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    return v8

    :cond_13
    const-wide/16 v12, 0x0

    cmp-long v12, v10, v12

    if-nez v12, :cond_10

    const v4, 0x8000

    goto/16 :goto_0

    :cond_14
    const v4, 0x8000

    goto/16 :goto_0
.end method

.method public checkPolicyResultWithStartType(Ljava/lang/String;IIILandroid/content/Intent;Ljava/lang/String;)Z
    .locals 6

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    if-ne p3, v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->checkPolicyResult(Ljava/lang/String;IILandroid/content/Intent;Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v2, " ------ Base Info ------"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, " ------ Intents Info ------"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->isCommonPolicy()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    const-string/jumbo v2, " ------ Common Exception Packages ------"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public getAllowIntents(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    sget-object v1, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getAvalidType()I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    return v0
.end method

.method public getCheckMethod()I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckMethod:I

    return v0
.end method

.method public getCommonPolicyExceptPackage()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    return-object v0
.end method

.method public getDefaultResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mDefaultResult:Z

    return v0
.end method

.method public getEnableStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    return v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    return v0
.end method

.method public getMatchMethod()I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMatchMethod:I

    return v0
.end method

.method public getPkg()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPkg:Ljava/lang/String;

    return-object v0
.end method

.method public getPolicyModuleName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPolicyModuleName:Ljava/lang/String;

    return-object v0
.end method

.method public getProcessName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mProcessName:Ljava/lang/String;

    return-object v0
.end method

.method public getSetter()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mSetter:Ljava/lang/String;

    return-object v0
.end method

.method public getStartType()I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    return v0
.end method

.method public getTimeOutForBroadCast()I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mTimeOutForBroadCast:I

    return v0
.end method

.method public getUserId()I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    return v0
.end method

.method public getmCheckFlag()I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckFlag:I

    return v0
.end method

.method public isCommonPolicy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsCommonPolicy:Z

    return v0
.end method

.method public isCommonPolicyIgnore(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    iget-object v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public isEnable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    return v0
.end method

.method public isRemovedWhenPolicyDisbale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsRemovedWhenDisable:Z

    return v0
.end method

.method public setAllowIntents(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    sget-object v1, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setAvalidType(I)V
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    return-void
.end method

.method public setCheckFlag(I)V
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckFlag:I

    return-void
.end method

.method public setCheckMethod(I)V
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckMethod:I

    return-void
.end method

.method public setCommonPolicy(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsCommonPolicy:Z

    return-void
.end method

.method public setCommonPolicyExceptPackage(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    return-void
.end method

.method public setDefaultResult(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mDefaultResult:Z

    return-void
.end method

.method public setEnable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    return-void
.end method

.method public setEnableState(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    return-void
.end method

.method public setId(I)V
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    return-void
.end method

.method public setMatchMethod(I)V
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMatchMethod:I

    return-void
.end method

.method public setPkg(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPkg:Ljava/lang/String;

    return-void
.end method

.method public setPolicyModuleName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPolicyModuleName:Ljava/lang/String;

    return-void
.end method

.method public setProcessName(Ljava/lang/String;)V
    .locals 0

    if-nez p1, :cond_0

    const-string/jumbo p1, ""

    :cond_0
    iput-object p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mProcessName:Ljava/lang/String;

    return-void
.end method

.method public setRemovedWhenPolicyDisbale(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsRemovedWhenDisable:Z

    return-void
.end method

.method public setSetter(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mSetter:Ljava/lang/String;

    return-void
.end method

.method public setStartType(I)V
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    return-void
.end method

.method public setTimeOutForBroadCast(I)V
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mTimeOutForBroadCast:I

    return-void
.end method

.method public setUserId(I)V
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "{ Package: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPkg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " UserId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Enable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " StartType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " DefaultResult: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mDefaultResult:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " CheckFlag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ProcessName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mProcessName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Setter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mSetter:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " PolicyModuleName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPolicyModuleName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " AvalidType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " CheckMethod: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckMethod:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " MatchMethod: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMatchMethod:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " isRemovedWhenDisable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsRemovedWhenDisable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " isCommonPolicy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsCommonPolicy:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " timeOutForBroadCast: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mTimeOutForBroadCast:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateCommonPolicyExceptPackage(Ljava/lang/String;IZ)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    monitor-enter v2

    if-eqz p3, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public updatePolicyEnableStatus(Ljava/lang/String;II)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsCommonPolicy:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPkg:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    if-ne v1, p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iget v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    :cond_2
    :goto_0
    packed-switch p3, :pswitch_data_0

    :cond_3
    :goto_1
    sget-boolean v1, Lcom/miui/whetstone/component/ComponentPolicyInfo;->DEBUG:Z

    if-eqz v1, :cond_4

    const-string/jumbo v1, "WhetstoneComponentPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Enable Check Policy ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " AvalidType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void

    :cond_5
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iput-boolean v3, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    goto :goto_0

    :pswitch_0
    iget v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_6

    iget v1, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    if-eqz v0, :cond_3

    :cond_6
    iput-boolean v3, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPkg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mUserId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mStartType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mSetter:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mDefaultResult:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mMatchMethod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckFlag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mPolicyModuleName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mAvalidType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckMethod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mProcessName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mEnable:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsRemovedWhenDisable:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-boolean v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mIsCommonPolicy:Z

    if-eqz v0, :cond_3

    :goto_3
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mTimeOutForBroadCast:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCheckIntents:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/miui/whetstone/component/ComponentPolicyInfo;->mCommonPolicyExceptPackage:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method
