.class Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;
.super Ljava/lang/Object;
.source "PackageComponentPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/whetstone/component/PackageComponentPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProcessPermission"
.end annotation


# instance fields
.field checkStartType:[Z

.field processName:Ljava/lang/String;

.field final synthetic this$0:Lcom/miui/whetstone/component/PackageComponentPolicy;


# direct methods
.method constructor <init>(Lcom/miui/whetstone/component/PackageComponentPolicy;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->this$0:Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    iput-object p2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->processName:Ljava/lang/String;

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method constructor <init>(Lcom/miui/whetstone/component/PackageComponentPolicy;Ljava/lang/String;[Z)V
    .locals 1

    iput-object p1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->this$0:Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    iput-object p2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->processName:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Z

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method public getCheckResult(I)Z
    .locals 2

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    return v1

    :pswitch_1
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    aget-boolean v0, v0, v1

    return v0

    :pswitch_2
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    return v0

    :pswitch_3
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v1, 0x2

    aget-boolean v0, v0, v1

    return v0

    :pswitch_4
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v1, 0x3

    aget-boolean v0, v0, v1

    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public hasAllow()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    aget-boolean v0, v3, v1

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{ Process: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Activity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v2, 0x0

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Service: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v2, 0x1

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Receiver: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v2, 0x2

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Provider: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v2, 0x3

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateCheckList(IZ)V
    .locals 3

    const/4 v1, 0x0

    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    aput-boolean p2, v0, v1

    :cond_0
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v1, 0x1

    aput-boolean p2, v0, v1

    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v1, 0x2

    aput-boolean p2, v0, v1

    :cond_2
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    const/4 v1, 0x3

    aput-boolean p2, v0, v1

    :cond_3
    invoke-static {}, Lcom/miui/whetstone/component/PackageComponentPolicy;->-get0()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "PackageComponentPolicy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->processName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " checkList:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    invoke-static {v2}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void
.end method

.method public updateCheckListAll(Z)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->checkStartType:[Z

    aput-boolean p1, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
