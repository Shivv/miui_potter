.class public interface abstract Lcom/juphoon/cmcc/lemon/MtcUtilConstants;
.super Ljava/lang/Object;
.source "MtcUtilConstants.java"


# static fields
.field public static final EN_MTC_REVOKE_RESULT_TYPE_FAIL:I = 0x2

.field public static final EN_MTC_REVOKE_RESULT_TYPE_NO:I = 0x0

.field public static final EN_MTC_REVOKE_RESULT_TYPE_SUCCESS:I = 0x1

.field public static final INVALIDID:I = -0x1

.field public static final MTC_APP_PUSH_TYPE_CMCC_APP:I = 0x10

.field public static final MTC_APP_PUSH_TYPE_LOG_NOTIFICATION:I = 0x20

.field public static final MTC_APP_PUSH_TYPE_NETWORK_ADDR:I = 0x4

.field public static final MTC_APP_PUSH_TYPE_PC:I = 0x8

.field public static final MTC_APP_PUSH_TYPE_PROFILE:I = 0x2

.field public static final MTC_APP_PUSH_TYPE_PUBLIC_PLATFORM:I = 0x80

.field public static final MTC_APP_PUSH_TYPE_RCS_OA:I = 0x100

.field public static final MTC_APP_PUSH_TYPE_SOCIAL_CIRCLE:I = 0x1

.field public static final MTC_APP_PUSH_TYPE_VIDEOCONF_NOTIFICATION:I = 0x40

.field public static final ZFAILED:I = 0x1

.field public static final ZOK:I
