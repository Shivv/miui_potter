.class public interface abstract Lcom/juphoon/cmcc/lemon/MtcPartpConstants;
.super Ljava/lang/Object;
.source "MtcPartpConstants.java"


# static fields
.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_ALERTING:I = 0x3

.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_CONNED:I = 0x4

.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_DIALINGIN:I = 0x1

.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_DIALINGOUT:I = 0x2

.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_DISCED:I = 0x7

.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_DISCING:I = 0x6

.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_HANG_UP:I = 0x9

.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_MUTED:I = 0x8

.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_ONHOLD:I = 0x5

.field public static final EN_MTC_CMCC_CONF_PARTP_STATE_PENDING:I = 0x0

.field public static final EN_MTC_PARTP_ACC_NET_APP:I = 0x1

.field public static final EN_MTC_PARTP_ACC_NET_CS:I = 0x3

.field public static final EN_MTC_PARTP_ACC_NET_OTHER:I = 0x0

.field public static final EN_MTC_PARTP_ACC_NET_VOLTE:I = 0x2

.field public static final EN_MTC_PARTP_ETYPE_GPMANAGE:I = 0x1

.field public static final EN_MTC_PARTP_ETYPE_UNKNOWN:I = 0x0

.field public static final EN_MTC_PARTP_ISCOMP_STAT_ACTIVE:I = 0x2

.field public static final EN_MTC_PARTP_ISCOMP_STAT_IDLE:I = 0x1

.field public static final EN_MTC_PARTP_ISCOMP_STAT_UNKNOWN:I = 0x0

.field public static final EN_MTC_PARTP_LST_ONE:I = 0x0

.field public static final EN_MTC_PARTP_LST_PREDEF_GRP:I = 0x1

.field public static final EN_MTC_PARTP_LST_URI_LST:I = 0x2

.field public static final EN_MTC_PARTP_ROLES_CHAIRMAN:I = 0x1

.field public static final EN_MTC_PARTP_ROLES_PARTP:I = 0x2

.field public static final EN_MTC_PARTP_ROLES_UNKNOWN:I = 0x0

.field public static final EN_MTC_PARTP_STAT_ACTIVE:I = 0x1

.field public static final EN_MTC_PARTP_STAT_BOOTED:I = 0x4

.field public static final EN_MTC_PARTP_STAT_DELETED:I = 0x6

.field public static final EN_MTC_PARTP_STAT_DEPARTED:I = 0x3

.field public static final EN_MTC_PARTP_STAT_FAILED:I = 0x5

.field public static final EN_MTC_PARTP_STAT_INACITVE:I = 0x2

.field public static final EN_MTC_PARTP_STAT_PENDING:I = 0x0

.field public static final EN_MTC_PARTP_VIDEO_STRM_STATE_DISABLE:I = 0x0

.field public static final EN_MTC_PARTP_VIDEO_STRM_STATE_ENABLE:I = 0x1
