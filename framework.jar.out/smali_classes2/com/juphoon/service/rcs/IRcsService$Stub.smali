.class public abstract Lcom/juphoon/service/rcs/IRcsService$Stub;
.super Landroid/os/Binder;
.source "IRcsService.java"

# interfaces
.implements Lcom/juphoon/service/rcs/IRcsService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/juphoon/service/rcs/IRcsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/juphoon/service/rcs/IRcsService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.juphoon.service.rcs.IRcsService"

.field static final TRANSACTION_Mtc_CliDbGetAccount:I = 0x2

.field static final TRANSACTION_Mtc_CliDbGetPcImpu:I = 0x11

.field static final TRANSACTION_Mtc_CliDbGetUserName:I = 0x1

.field static final TRANSACTION_Mtc_IsNeedSms:I = 0x5

.field static final TRANSACTION_Mtc_ProfDbGetCountryCode:I = 0x6

.field static final TRANSACTION_getCanCallSensitiveDeviceApi:I = 0x9

.field static final TRANSACTION_getCmccToken:I = 0x7

.field static final TRANSACTION_getGbaBtidKsNaf:I = 0x12

.field static final TRANSACTION_getNeedNotifySensitiveDeviceApi:I = 0xb

.field static final TRANSACTION_getRcsApplicationDealFirstNotify:I = 0x14

.field static final TRANSACTION_getRcsApplicationDealFirstNotifyByBroadcast:I = 0x16

.field static final TRANSACTION_getState:I = 0x3

.field static final TRANSACTION_getStringForSensitive:I = 0x10

.field static final TRANSACTION_getTranslateSms:I = 0xd

.field static final TRANSACTION_getUseRcs:I = 0xc

.field static final TRANSACTION_login:I = 0x4

.field static final TRANSACTION_setCanCallSensitiveDeviceApi:I = 0x8

.field static final TRANSACTION_setNeedNotifySensitiveDeviceApi:I = 0xa

.field static final TRANSACTION_setRcsApplicationDealFirstNotify:I = 0x13

.field static final TRANSACTION_setRcsApplicationDealFirstNotifyByBroadcast:I = 0x15

.field static final TRANSACTION_setTranslateSms:I = 0xf

.field static final TRANSACTION_setUseRcs:I = 0xe


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string/jumbo v0, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p0, p0, v0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/juphoon/service/rcs/IRcsService;
    .locals 2

    const/4 v1, 0x0

    if-nez p0, :cond_0

    return-object v1

    :cond_0
    const-string/jumbo v1, "com.juphoon.service.rcs.IRcsService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/juphoon/service/rcs/IRcsService;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/juphoon/service/rcs/IRcsService;

    return-object v0

    :cond_1
    new-instance v1, Lcom/juphoon/service/rcs/IRcsService$Stub$Proxy;

    invoke-direct {v1, p0}, Lcom/juphoon/service/rcs/IRcsService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v9, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v8

    return v8

    :sswitch_0
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v9

    :sswitch_1
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->Mtc_CliDbGetUserName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v9

    :sswitch_2
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->Mtc_CliDbGetAccount()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v9

    :sswitch_3
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getState()I

    move-result v5

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    return v9

    :sswitch_4
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->login()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v9

    :sswitch_5
    const-string/jumbo v10, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->Mtc_IsNeedSms()Z

    move-result v7

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v7, :cond_0

    move v8, v9

    :cond_0
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    return v9

    :sswitch_6
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->Mtc_ProfDbGetCountryCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v9

    :sswitch_7
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getCmccToken(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v9

    :sswitch_8
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {p0, v0, v3}, Lcom/juphoon/service/rcs/IRcsService$Stub;->setCanCallSensitiveDeviceApi(Ljava/lang/String;Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v9

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :sswitch_9
    const-string/jumbo v10, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getCanCallSensitiveDeviceApi(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v7, :cond_2

    move v8, v9

    :cond_2
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    return v9

    :sswitch_a
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_3

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {p0, v0, v2, v4}, Lcom/juphoon/service/rcs/IRcsService$Stub;->setNeedNotifySensitiveDeviceApi(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v9

    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    :sswitch_b
    const-string/jumbo v10, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getNeedNotifySensitiveDeviceApi(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v7, :cond_4

    move v8, v9

    :cond_4
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    return v9

    :sswitch_c
    const-string/jumbo v10, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getUseRcs()Z

    move-result v7

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v7, :cond_5

    move v8, v9

    :cond_5
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    return v9

    :sswitch_d
    const-string/jumbo v10, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getTranslateSms()Z

    move-result v7

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v7, :cond_6

    move v8, v9

    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    return v9

    :sswitch_e
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_7

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {p0, v1}, Lcom/juphoon/service/rcs/IRcsService$Stub;->setUseRcs(Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v9

    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    :sswitch_f
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_8

    const/4 v1, 0x1

    :goto_3
    invoke-virtual {p0, v1}, Lcom/juphoon/service/rcs/IRcsService$Stub;->setTranslateSms(Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v9

    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    :sswitch_10
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getStringForSensitive()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v9

    :sswitch_11
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->Mtc_CliDbGetPcImpu()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v9

    :sswitch_12
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getGbaBtidKsNaf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    return v9

    :sswitch_13
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_9

    const/4 v1, 0x1

    :goto_4
    invoke-virtual {p0, v1}, Lcom/juphoon/service/rcs/IRcsService$Stub;->setRcsApplicationDealFirstNotify(Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v9

    :cond_9
    const/4 v1, 0x0

    goto :goto_4

    :sswitch_14
    const-string/jumbo v10, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getRcsApplicationDealFirstNotify()Z

    move-result v7

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v7, :cond_a

    move v8, v9

    :cond_a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    return v9

    :sswitch_15
    const-string/jumbo v8, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_b

    const/4 v1, 0x1

    :goto_5
    invoke-virtual {p0, v1}, Lcom/juphoon/service/rcs/IRcsService$Stub;->setRcsApplicationDealFirstNotifyByBroadcast(Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v9

    :cond_b
    const/4 v1, 0x0

    goto :goto_5

    :sswitch_16
    const-string/jumbo v10, "com.juphoon.service.rcs.IRcsService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/juphoon/service/rcs/IRcsService$Stub;->getRcsApplicationDealFirstNotifyByBroadcast()Z

    move-result v7

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v7, :cond_c

    move v8, v9

    :cond_c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    return v9

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
