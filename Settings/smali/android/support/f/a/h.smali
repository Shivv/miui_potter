.class Landroid/support/f/a/h;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "AnimatedVectorDrawableCompat.java"


# instance fields
.field eVk:I

.field private eVl:Ljava/util/ArrayList;

.field eVm:Landroid/support/f/a/q;

.field eVn:Landroid/animation/AnimatorSet;

.field eVo:Landroid/support/v4/a/u;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/f/a/h;Landroid/graphics/drawable/Drawable$Callback;Landroid/content/res/Resources;)V
    .locals 5

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    if-eqz p2, :cond_3

    iget v0, p2, Landroid/support/f/a/h;->eVk:I

    iput v0, p0, Landroid/support/f/a/h;->eVk:I

    iget-object v0, p2, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    if-eqz v0, :cond_0

    iget-object v0, p2, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    invoke-virtual {v0}, Landroid/support/f/a/q;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    if-eqz p4, :cond_1

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/f/a/q;

    iput-object v0, p0, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    :goto_0
    iget-object v0, p0, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    invoke-virtual {v0}, Landroid/support/f/a/q;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/f/a/q;

    iput-object v0, p0, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    iget-object v0, p0, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    invoke-virtual {v0, p3}, Landroid/support/f/a/q;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    iget-object v2, p2, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    invoke-virtual {v2}, Landroid/support/f/a/q;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/f/a/q;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    invoke-virtual {v0, v1}, Landroid/support/f/a/q;->epK(Z)V

    :cond_0
    iget-object v0, p2, Landroid/support/f/a/h;->eVl:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p2, Landroid/support/f/a/h;->eVl:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/f/a/h;->eVl:Ljava/util/ArrayList;

    new-instance v0, Landroid/support/v4/a/u;

    invoke-direct {v0, v2}, Landroid/support/v4/a/u;-><init>(I)V

    iput-object v0, p0, Landroid/support/f/a/h;->eVo:Landroid/support/v4/a/u;

    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v0, p2, Landroid/support/f/a/h;->eVl:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    move-result-object v3

    iget-object v4, p2, Landroid/support/f/a/h;->eVo:Landroid/support/v4/a/u;

    invoke-virtual {v4, v0}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    invoke-virtual {v4, v0}, Landroid/support/f/a/q;->epQ(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    iget-object v4, p0, Landroid/support/f/a/h;->eVl:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Landroid/support/f/a/h;->eVo:Landroid/support/v4/a/u;

    invoke-virtual {v4, v3, v0}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/f/a/q;

    iput-object v0, p0, Landroid/support/f/a/h;->eVm:Landroid/support/f/a/q;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/support/f/a/h;->epj()V

    :cond_3
    return-void
.end method

.method static synthetic epi(Landroid/support/f/a/h;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Landroid/support/f/a/h;->eVl:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic epk(Landroid/support/f/a/h;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/h;->eVl:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public epj()V
    .locals 2

    iget-object v0, p0, Landroid/support/f/a/h;->eVn:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/h;->eVn:Landroid/animation/AnimatorSet;

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/h;->eVn:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Landroid/support/f/a/h;->eVl:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    return-void
.end method

.method public getChangingConfigurations()I
    .locals 1

    iget v0, p0, Landroid/support/f/a/h;->eVk:I

    return v0
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No constant state support for SDK < 24."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No constant state support for SDK < 24."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
