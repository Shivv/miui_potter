.class Landroid/support/f/a/a;
.super Ljava/lang/Object;
.source "VectorDrawableCompat.java"


# instance fields
.field eUA:I

.field final eUB:Ljava/util/ArrayList;

.field private final eUC:Landroid/graphics/Matrix;

.field private eUD:Ljava/lang/String;

.field private eUr:F

.field private eUs:F

.field private eUt:F

.field private eUu:F

.field private eUv:[I

.field private final eUw:Landroid/graphics/Matrix;

.field private eUx:F

.field eUy:F

.field private eUz:F


# direct methods
.method public constructor <init>()V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/a;->eUC:Landroid/graphics/Matrix;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    iput v1, p0, Landroid/support/f/a/a;->eUy:F

    iput v1, p0, Landroid/support/f/a/a;->eUt:F

    iput v1, p0, Landroid/support/f/a/a;->eUr:F

    iput v2, p0, Landroid/support/f/a/a;->eUs:F

    iput v2, p0, Landroid/support/f/a/a;->eUu:F

    iput v1, p0, Landroid/support/f/a/a;->eUz:F

    iput v1, p0, Landroid/support/f/a/a;->eUx:F

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/f/a/a;->eUD:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/f/a/a;Landroid/support/v4/a/u;)V
    .locals 5

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/a;->eUC:Landroid/graphics/Matrix;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    iput v1, p0, Landroid/support/f/a/a;->eUy:F

    iput v1, p0, Landroid/support/f/a/a;->eUt:F

    iput v1, p0, Landroid/support/f/a/a;->eUr:F

    iput v3, p0, Landroid/support/f/a/a;->eUs:F

    iput v3, p0, Landroid/support/f/a/a;->eUu:F

    iput v1, p0, Landroid/support/f/a/a;->eUz:F

    iput v1, p0, Landroid/support/f/a/a;->eUx:F

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    iput-object v2, p0, Landroid/support/f/a/a;->eUD:Ljava/lang/String;

    iget v0, p1, Landroid/support/f/a/a;->eUy:F

    iput v0, p0, Landroid/support/f/a/a;->eUy:F

    iget v0, p1, Landroid/support/f/a/a;->eUt:F

    iput v0, p0, Landroid/support/f/a/a;->eUt:F

    iget v0, p1, Landroid/support/f/a/a;->eUr:F

    iput v0, p0, Landroid/support/f/a/a;->eUr:F

    iget v0, p1, Landroid/support/f/a/a;->eUs:F

    iput v0, p0, Landroid/support/f/a/a;->eUs:F

    iget v0, p1, Landroid/support/f/a/a;->eUu:F

    iput v0, p0, Landroid/support/f/a/a;->eUu:F

    iget v0, p1, Landroid/support/f/a/a;->eUz:F

    iput v0, p0, Landroid/support/f/a/a;->eUz:F

    iget v0, p1, Landroid/support/f/a/a;->eUx:F

    iput v0, p0, Landroid/support/f/a/a;->eUx:F

    iget-object v0, p1, Landroid/support/f/a/a;->eUv:[I

    iput-object v0, p0, Landroid/support/f/a/a;->eUv:[I

    iget-object v0, p1, Landroid/support/f/a/a;->eUD:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/f/a/a;->eUD:Ljava/lang/String;

    iget v0, p1, Landroid/support/f/a/a;->eUA:I

    iput v0, p0, Landroid/support/f/a/a;->eUA:I

    iget-object v0, p0, Landroid/support/f/a/a;->eUD:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/a;->eUD:Ljava/lang/String;

    invoke-virtual {p2, v0, p0}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    iget-object v1, p1, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p1, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Landroid/support/f/a/a;

    if-eqz v2, :cond_2

    check-cast v0, Landroid/support/f/a/a;

    iget-object v2, p0, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    new-instance v4, Landroid/support/f/a/a;

    invoke-direct {v4, v0, p2}, Landroid/support/f/a/a;-><init>(Landroid/support/f/a/a;Landroid/support/v4/a/u;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    instance-of v2, v0, Landroid/support/f/a/e;

    if-eqz v2, :cond_3

    new-instance v2, Landroid/support/f/a/e;

    check-cast v0, Landroid/support/f/a/e;

    invoke-direct {v2, v0}, Landroid/support/f/a/e;-><init>(Landroid/support/f/a/e;)V

    move-object v0, v2

    :goto_2
    iget-object v2, p0, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v0, Landroid/support/f/a/k;->eVD:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, v0, Landroid/support/f/a/k;->eVD:Ljava/lang/String;

    invoke-virtual {p2, v2, v0}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    instance-of v2, v0, Landroid/support/f/a/p;

    if-eqz v2, :cond_4

    new-instance v2, Landroid/support/f/a/p;

    check-cast v0, Landroid/support/f/a/p;

    invoke-direct {v2, v0}, Landroid/support/f/a/p;-><init>(Landroid/support/f/a/p;)V

    move-object v0, v2

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Unknown object in the tree!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-void
.end method

.method static synthetic eou(Landroid/support/f/a/a;)Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/a;->eUC:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private eov(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/f/a/a;->eUv:[I

    const-string/jumbo v0, "rotation"

    iget v1, p0, Landroid/support/f/a/a;->eUy:F

    const/4 v2, 0x5

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/a;->eUy:F

    iget v0, p0, Landroid/support/f/a/a;->eUt:F

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/a;->eUt:F

    iget v0, p0, Landroid/support/f/a/a;->eUr:F

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/a;->eUr:F

    const-string/jumbo v0, "scaleX"

    iget v1, p0, Landroid/support/f/a/a;->eUs:F

    const/4 v2, 0x3

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/a;->eUs:F

    const-string/jumbo v0, "scaleY"

    iget v1, p0, Landroid/support/f/a/a;->eUu:F

    const/4 v2, 0x4

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/a;->eUu:F

    const-string/jumbo v0, "translateX"

    iget v1, p0, Landroid/support/f/a/a;->eUz:F

    const/4 v2, 0x6

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/a;->eUz:F

    const-string/jumbo v0, "translateY"

    iget v1, p0, Landroid/support/f/a/a;->eUx:F

    const/4 v2, 0x7

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/a;->eUx:F

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Landroid/support/f/a/a;->eUD:Ljava/lang/String;

    :cond_0
    invoke-direct {p0}, Landroid/support/f/a/a;->eoy()V

    return-void
.end method

.method private eoy()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    iget v1, p0, Landroid/support/f/a/a;->eUt:F

    neg-float v1, v1

    iget v2, p0, Landroid/support/f/a/a;->eUr:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    iget v1, p0, Landroid/support/f/a/a;->eUs:F

    iget v2, p0, Landroid/support/f/a/a;->eUu:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v0, p0, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    iget v1, p0, Landroid/support/f/a/a;->eUy:F

    invoke-virtual {v0, v1, v3, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    iget-object v0, p0, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    iget v1, p0, Landroid/support/f/a/a;->eUz:F

    iget v2, p0, Landroid/support/f/a/a;->eUt:F

    add-float/2addr v1, v2

    iget v2, p0, Landroid/support/f/a/a;->eUx:F

    iget v3, p0, Landroid/support/f/a/a;->eUr:F

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void
.end method

.method static synthetic eoz(Landroid/support/f/a/a;)Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/a;->eUw:Landroid/graphics/Matrix;

    return-object v0
.end method


# virtual methods
.method public eow()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/a;->eUD:Ljava/lang/String;

    return-object v0
.end method

.method public eox(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 1

    sget-object v0, Landroid/support/f/a/j;->eVw:[I

    invoke-static {p1, p3, p2, v0}, Landroid/support/v4/content/a/a;->dZt(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Landroid/support/f/a/a;->eov(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method
