.class Landroid/support/f/a/e;
.super Landroid/support/f/a/k;
.source "VectorDrawableCompat.java"


# instance fields
.field eUV:I

.field eUW:Landroid/graphics/Paint$Cap;

.field eUX:F

.field private eUY:[I

.field eUZ:F

.field eVa:F

.field eVb:I

.field eVc:Landroid/graphics/Paint$Join;

.field eVd:F

.field eVe:F

.field eVf:F

.field eVg:F

.field eVh:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/support/f/a/k;-><init>()V

    iput v2, p0, Landroid/support/f/a/e;->eUV:I

    iput v0, p0, Landroid/support/f/a/e;->eVd:F

    iput v2, p0, Landroid/support/f/a/e;->eVb:I

    iput v1, p0, Landroid/support/f/a/e;->eVe:F

    iput v2, p0, Landroid/support/f/a/e;->eVh:I

    iput v1, p0, Landroid/support/f/a/e;->eVa:F

    iput v0, p0, Landroid/support/f/a/e;->eVg:F

    iput v1, p0, Landroid/support/f/a/e;->eVf:F

    iput v0, p0, Landroid/support/f/a/e;->eUZ:F

    sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, Landroid/support/f/a/e;->eUW:Landroid/graphics/Paint$Cap;

    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    iput-object v0, p0, Landroid/support/f/a/e;->eVc:Landroid/graphics/Paint$Join;

    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Landroid/support/f/a/e;->eUX:F

    return-void
.end method

.method public constructor <init>(Landroid/support/f/a/e;)V
    .locals 3

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/support/f/a/k;-><init>(Landroid/support/f/a/k;)V

    iput v2, p0, Landroid/support/f/a/e;->eUV:I

    iput v0, p0, Landroid/support/f/a/e;->eVd:F

    iput v2, p0, Landroid/support/f/a/e;->eVb:I

    iput v1, p0, Landroid/support/f/a/e;->eVe:F

    iput v2, p0, Landroid/support/f/a/e;->eVh:I

    iput v1, p0, Landroid/support/f/a/e;->eVa:F

    iput v0, p0, Landroid/support/f/a/e;->eVg:F

    iput v1, p0, Landroid/support/f/a/e;->eVf:F

    iput v0, p0, Landroid/support/f/a/e;->eUZ:F

    sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, Landroid/support/f/a/e;->eUW:Landroid/graphics/Paint$Cap;

    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    iput-object v0, p0, Landroid/support/f/a/e;->eVc:Landroid/graphics/Paint$Join;

    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Landroid/support/f/a/e;->eUX:F

    iget-object v0, p1, Landroid/support/f/a/e;->eUY:[I

    iput-object v0, p0, Landroid/support/f/a/e;->eUY:[I

    iget v0, p1, Landroid/support/f/a/e;->eUV:I

    iput v0, p0, Landroid/support/f/a/e;->eUV:I

    iget v0, p1, Landroid/support/f/a/e;->eVd:F

    iput v0, p0, Landroid/support/f/a/e;->eVd:F

    iget v0, p1, Landroid/support/f/a/e;->eVe:F

    iput v0, p0, Landroid/support/f/a/e;->eVe:F

    iget v0, p1, Landroid/support/f/a/e;->eVb:I

    iput v0, p0, Landroid/support/f/a/e;->eVb:I

    iget v0, p1, Landroid/support/f/a/e;->eVh:I

    iput v0, p0, Landroid/support/f/a/e;->eVh:I

    iget v0, p1, Landroid/support/f/a/e;->eVa:F

    iput v0, p0, Landroid/support/f/a/e;->eVa:F

    iget v0, p1, Landroid/support/f/a/e;->eVg:F

    iput v0, p0, Landroid/support/f/a/e;->eVg:F

    iget v0, p1, Landroid/support/f/a/e;->eVf:F

    iput v0, p0, Landroid/support/f/a/e;->eVf:F

    iget v0, p1, Landroid/support/f/a/e;->eUZ:F

    iput v0, p0, Landroid/support/f/a/e;->eUZ:F

    iget-object v0, p1, Landroid/support/f/a/e;->eUW:Landroid/graphics/Paint$Cap;

    iput-object v0, p0, Landroid/support/f/a/e;->eUW:Landroid/graphics/Paint$Cap;

    iget-object v0, p1, Landroid/support/f/a/e;->eVc:Landroid/graphics/Paint$Join;

    iput-object v0, p0, Landroid/support/f/a/e;->eVc:Landroid/graphics/Paint$Join;

    iget v0, p1, Landroid/support/f/a/e;->eUX:F

    iput v0, p0, Landroid/support/f/a/e;->eUX:F

    return-void
.end method

.method private epe(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4

    const/4 v3, -0x1

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/f/a/e;->eUY:[I

    const-string/jumbo v0, "pathData"

    invoke-static {p2, v0}, Landroid/support/v4/content/a/a;->dZD(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-object v0, p0, Landroid/support/f/a/e;->eVD:Ljava/lang/String;

    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Landroid/support/v4/c/j;->emD(Ljava/lang/String;)[Landroid/support/v4/c/h;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/e;->eVE:[Landroid/support/v4/c/h;

    :cond_2
    const-string/jumbo v0, "fillColor"

    iget v1, p0, Landroid/support/f/a/e;->eVb:I

    const/4 v2, 0x1

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZr(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eVb:I

    const-string/jumbo v0, "fillAlpha"

    iget v1, p0, Landroid/support/f/a/e;->eVa:F

    const/16 v2, 0xc

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eVa:F

    const-string/jumbo v0, "strokeLineCap"

    const/16 v1, 0x8

    invoke-static {p1, p2, v0, v1, v3}, Landroid/support/v4/content/a/a;->dZH(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I

    move-result v0

    iget-object v1, p0, Landroid/support/f/a/e;->eUW:Landroid/graphics/Paint$Cap;

    invoke-direct {p0, v0, v1}, Landroid/support/f/a/e;->epf(ILandroid/graphics/Paint$Cap;)Landroid/graphics/Paint$Cap;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/e;->eUW:Landroid/graphics/Paint$Cap;

    const-string/jumbo v0, "strokeLineJoin"

    const/16 v1, 0x9

    invoke-static {p1, p2, v0, v1, v3}, Landroid/support/v4/content/a/a;->dZH(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I

    move-result v0

    iget-object v1, p0, Landroid/support/f/a/e;->eVc:Landroid/graphics/Paint$Join;

    invoke-direct {p0, v0, v1}, Landroid/support/f/a/e;->eph(ILandroid/graphics/Paint$Join;)Landroid/graphics/Paint$Join;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/e;->eVc:Landroid/graphics/Paint$Join;

    const-string/jumbo v0, "strokeMiterLimit"

    iget v1, p0, Landroid/support/f/a/e;->eUX:F

    const/16 v2, 0xa

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eUX:F

    const-string/jumbo v0, "strokeColor"

    iget v1, p0, Landroid/support/f/a/e;->eUV:I

    const/4 v2, 0x3

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZr(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eUV:I

    const-string/jumbo v0, "strokeAlpha"

    iget v1, p0, Landroid/support/f/a/e;->eVe:F

    const/16 v2, 0xb

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eVe:F

    const-string/jumbo v0, "strokeWidth"

    iget v1, p0, Landroid/support/f/a/e;->eVd:F

    const/4 v2, 0x4

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eVd:F

    const-string/jumbo v0, "trimPathEnd"

    iget v1, p0, Landroid/support/f/a/e;->eVf:F

    const/4 v2, 0x6

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eVf:F

    const-string/jumbo v0, "trimPathOffset"

    iget v1, p0, Landroid/support/f/a/e;->eUZ:F

    const/4 v2, 0x7

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eUZ:F

    const-string/jumbo v0, "trimPathStart"

    iget v1, p0, Landroid/support/f/a/e;->eVg:F

    const/4 v2, 0x5

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eVg:F

    const-string/jumbo v0, "fillType"

    iget v1, p0, Landroid/support/f/a/e;->eVh:I

    const/16 v2, 0xd

    invoke-static {p1, p2, v0, v2, v1}, Landroid/support/v4/content/a/a;->dZH(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Landroid/support/f/a/e;->eVh:I

    return-void
.end method

.method private epf(ILandroid/graphics/Paint$Cap;)Landroid/graphics/Paint$Cap;
    .locals 1

    packed-switch p1, :pswitch_data_0

    return-object p2

    :pswitch_0
    sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    return-object v0

    :pswitch_1
    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    return-object v0

    :pswitch_2
    sget-object v0, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private eph(ILandroid/graphics/Paint$Join;)Landroid/graphics/Paint$Join;
    .locals 1

    packed-switch p1, :pswitch_data_0

    return-object p2

    :pswitch_0
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    return-object v0

    :pswitch_1
    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    return-object v0

    :pswitch_2
    sget-object v0, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public epg(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 1

    sget-object v0, Landroid/support/f/a/j;->eVy:[I

    invoke-static {p1, p3, p2, v0}, Landroid/support/v4/content/a/a;->dZt(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Landroid/support/f/a/e;->epe(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method
