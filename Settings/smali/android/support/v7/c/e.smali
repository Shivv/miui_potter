.class public Landroid/support/v7/c/e;
.super Ljava/lang/Object;
.source "DiffUtil.java"


# instance fields
.field private final erp:[I

.field private final erq:Landroid/support/v7/c/b;

.field private final err:I

.field private final ers:I

.field private final ert:[I

.field private final eru:Z

.field private final erv:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/support/v7/c/b;Ljava/util/List;[I[IZ)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    iput-object p3, p0, Landroid/support/v7/c/e;->ert:[I

    iput-object p4, p0, Landroid/support/v7/c/e;->erp:[I

    iget-object v0, p0, Landroid/support/v7/c/e;->ert:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Landroid/support/v7/c/e;->erp:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iput-object p1, p0, Landroid/support/v7/c/e;->erq:Landroid/support/v7/c/b;

    invoke-virtual {p1}, Landroid/support/v7/c/b;->getOldListSize()I

    move-result v0

    iput v0, p0, Landroid/support/v7/c/e;->ers:I

    invoke-virtual {p1}, Landroid/support/v7/c/b;->getNewListSize()I

    move-result v0

    iput v0, p0, Landroid/support/v7/c/e;->err:I

    iput-boolean p5, p0, Landroid/support/v7/c/e;->eru:Z

    invoke-direct {p0}, Landroid/support/v7/c/e;->dJe()V

    invoke-direct {p0}, Landroid/support/v7/c/e;->dIZ()V

    return-void
.end method

.method private dIW(III)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/c/e;->ert:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3, v2}, Landroid/support/v7/c/e;->dIY(IIIZ)Z

    return-void
.end method

.method private dIY(IIIZ)Z
    .locals 10

    const/16 v2, 0x8

    const/4 v3, 0x4

    const/4 v9, 0x1

    const/4 v8, 0x0

    if-eqz p4, :cond_0

    add-int/lit8 v0, p2, -0x1

    add-int/lit8 p2, p2, -0x1

    move v1, v0

    move v0, p1

    :goto_0
    move v4, v0

    :goto_1
    if-ltz p3, :cond_7

    iget-object v0, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/c;

    iget v5, v0, Landroid/support/v7/c/c;->erk:I

    iget v6, v0, Landroid/support/v7/c/c;->erg:I

    add-int/2addr v5, v6

    iget v6, v0, Landroid/support/v7/c/c;->erj:I

    iget v7, v0, Landroid/support/v7/c/c;->erg:I

    add-int/2addr v6, v7

    if-eqz p4, :cond_3

    add-int/lit8 v4, v4, -0x1

    :goto_2
    if-lt v4, v5, :cond_6

    iget-object v6, p0, Landroid/support/v7/c/e;->erq:Landroid/support/v7/c/b;

    invoke-virtual {v6, v4, v1}, Landroid/support/v7/c/b;->areItemsTheSame(II)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v0, p0, Landroid/support/v7/c/e;->erq:Landroid/support/v7/c/b;

    invoke-virtual {v0, v4, v1}, Landroid/support/v7/c/b;->areContentsTheSame(II)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_3
    iget-object v2, p0, Landroid/support/v7/c/e;->erp:[I

    shl-int/lit8 v3, v4, 0x5

    or-int/lit8 v3, v3, 0x10

    aput v3, v2, v1

    iget-object v2, p0, Landroid/support/v7/c/e;->ert:[I

    shl-int/lit8 v1, v1, 0x5

    or-int/2addr v0, v1

    aput v0, v2, v4

    return v9

    :cond_0
    add-int/lit8 v1, p1, -0x1

    add-int/lit8 v0, p1, -0x1

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_3

    :cond_2
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v4, p2, -0x1

    :goto_4
    if-lt v4, v6, :cond_6

    iget-object v5, p0, Landroid/support/v7/c/e;->erq:Landroid/support/v7/c/b;

    invoke-virtual {v5, v1, v4}, Landroid/support/v7/c/b;->areItemsTheSame(II)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v0, p0, Landroid/support/v7/c/e;->erq:Landroid/support/v7/c/b;

    invoke-virtual {v0, v1, v4}, Landroid/support/v7/c/b;->areContentsTheSame(II)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_5
    iget-object v0, p0, Landroid/support/v7/c/e;->ert:[I

    add-int/lit8 v1, p1, -0x1

    shl-int/lit8 v3, v4, 0x5

    or-int/lit8 v3, v3, 0x10

    aput v3, v0, v1

    iget-object v0, p0, Landroid/support/v7/c/e;->erp:[I

    add-int/lit8 v1, p1, -0x1

    shl-int/lit8 v1, v1, 0x5

    or-int/2addr v1, v2

    aput v1, v0, v4

    return v9

    :cond_4
    move v2, v3

    goto :goto_5

    :cond_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    :cond_6
    iget v4, v0, Landroid/support/v7/c/c;->erk:I

    iget p2, v0, Landroid/support/v7/c/c;->erj:I

    add-int/lit8 p3, p3, -0x1

    goto :goto_1

    :cond_7
    return v8
.end method

.method private dIZ()V
    .locals 9

    const/4 v2, 0x0

    iget v3, p0, Landroid/support/v7/c/e;->ers:I

    iget v1, p0, Landroid/support/v7/c/e;->err:I

    iget-object v0, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_0
    if-ltz v4, :cond_4

    iget-object v0, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/c;

    iget v5, v0, Landroid/support/v7/c/c;->erk:I

    iget v6, v0, Landroid/support/v7/c/c;->erg:I

    add-int/2addr v5, v6

    iget v6, v0, Landroid/support/v7/c/c;->erj:I

    iget v7, v0, Landroid/support/v7/c/c;->erg:I

    add-int/2addr v6, v7

    iget-boolean v7, p0, Landroid/support/v7/c/e;->eru:Z

    if-eqz v7, :cond_1

    :goto_1
    if-le v3, v5, :cond_0

    invoke-direct {p0, v3, v1, v4}, Landroid/support/v7/c/e;->dIW(III)V

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_0
    :goto_2
    if-le v1, v6, :cond_1

    invoke-direct {p0, v3, v1, v4}, Landroid/support/v7/c/e;->dJd(III)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_1
    move v1, v2

    :goto_3
    iget v3, v0, Landroid/support/v7/c/c;->erg:I

    if-ge v1, v3, :cond_3

    iget v3, v0, Landroid/support/v7/c/c;->erk:I

    add-int v5, v3, v1

    iget v3, v0, Landroid/support/v7/c/c;->erj:I

    add-int v6, v3, v1

    iget-object v3, p0, Landroid/support/v7/c/e;->erq:Landroid/support/v7/c/b;

    invoke-virtual {v3, v5, v6}, Landroid/support/v7/c/b;->areContentsTheSame(II)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_4
    iget-object v7, p0, Landroid/support/v7/c/e;->ert:[I

    shl-int/lit8 v8, v6, 0x5

    or-int/2addr v8, v3

    aput v8, v7, v5

    iget-object v7, p0, Landroid/support/v7/c/e;->erp:[I

    shl-int/lit8 v5, v5, 0x5

    or-int/2addr v3, v5

    aput v3, v7, v6

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    const/4 v3, 0x2

    goto :goto_4

    :cond_3
    iget v3, v0, Landroid/support/v7/c/c;->erk:I

    iget v1, v0, Landroid/support/v7/c/c;->erj:I

    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_0

    :cond_4
    return-void
.end method

.method private dJa(Ljava/util/List;Landroid/support/v7/c/i;III)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-boolean v0, p0, Landroid/support/v7/c/e;->eru:Z

    if-nez v0, :cond_0

    invoke-interface {p2, p3, p4}, Landroid/support/v7/c/i;->dJi(II)V

    return-void

    :cond_0
    add-int/lit8 v0, p4, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/c/e;->ert:[I

    add-int v2, p5, v1

    aget v0, v0, v2

    and-int/lit8 v0, v0, 0x1f

    sparse-switch v0, :sswitch_data_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unknown flag for pos "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/2addr v1, p5

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :sswitch_0
    add-int v0, p3, v1

    invoke-interface {p2, v0, v6}, Landroid/support/v7/c/i;->dJi(II)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/f;

    iget v3, v0, Landroid/support/v7/c/f;->ery:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v0, Landroid/support/v7/c/f;->ery:I

    goto :goto_1

    :sswitch_1
    iget-object v2, p0, Landroid/support/v7/c/e;->ert:[I

    add-int v3, p5, v1

    aget v2, v2, v3

    shr-int/lit8 v2, v2, 0x5

    invoke-static {p1, v2, v7}, Landroid/support/v7/c/e;->dJb(Ljava/util/List;IZ)Landroid/support/v7/c/f;

    move-result-object v3

    add-int v4, p3, v1

    iget v5, v3, Landroid/support/v7/c/f;->ery:I

    add-int/lit8 v5, v5, -0x1

    invoke-interface {p2, v4, v5}, Landroid/support/v7/c/i;->dJh(II)V

    const/4 v4, 0x4

    if-ne v0, v4, :cond_1

    iget v0, v3, Landroid/support/v7/c/f;->ery:I

    add-int/lit8 v0, v0, -0x1

    iget-object v3, p0, Landroid/support/v7/c/e;->erq:Landroid/support/v7/c/b;

    add-int v4, p5, v1

    invoke-virtual {v3, v4, v2}, Landroid/support/v7/c/b;->getChangePayload(II)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v0, v6, v2}, Landroid/support/v7/c/i;->dJj(IILjava/lang/Object;)V

    :cond_1
    :goto_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :sswitch_2
    new-instance v0, Landroid/support/v7/c/f;

    add-int v2, p5, v1

    add-int v3, p3, v1

    invoke-direct {v0, v2, v3, v6}, Landroid/support/v7/c/f;-><init>(IIZ)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method private static dJb(Ljava/util/List;IZ)Landroid/support/v7/c/f;
    .locals 5

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_3

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/f;

    iget v2, v0, Landroid/support/v7/c/f;->erx:I

    if-ne v2, p1, :cond_2

    iget-boolean v2, v0, Landroid/support/v7/c/f;->erw:Z

    if-ne v2, p2, :cond_2

    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v2, v1

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/c/f;

    iget v4, v1, Landroid/support/v7/c/f;->ery:I

    if-eqz p2, :cond_0

    const/4 v3, 0x1

    :goto_2
    add-int/2addr v3, v4

    iput v3, v1, Landroid/support/v7/c/f;->ery:I

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_0
    const/4 v3, -0x1

    goto :goto_2

    :cond_1
    return-object v0

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method private dJd(III)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/c/e;->erp:[I

    add-int/lit8 v1, p2, -0x1

    aget v0, v0, v1

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/c/e;->dIY(IIIZ)Z

    return-void
.end method

.method private dJe()V
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_0
    if-eqz v0, :cond_0

    iget v1, v0, Landroid/support/v7/c/c;->erk:I

    if-eqz v1, :cond_3

    :cond_0
    :goto_1
    new-instance v0, Landroid/support/v7/c/c;

    invoke-direct {v0}, Landroid/support/v7/c/c;-><init>()V

    iput v2, v0, Landroid/support/v7/c/c;->erk:I

    iput v2, v0, Landroid/support/v7/c/c;->erj:I

    iput-boolean v2, v0, Landroid/support/v7/c/c;->eri:Z

    iput v2, v0, Landroid/support/v7/c/c;->erg:I

    iput-boolean v2, v0, Landroid/support/v7/c/c;->erh:Z

    iget-object v1, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/c;

    goto :goto_0

    :cond_3
    iget v0, v0, Landroid/support/v7/c/c;->erj:I

    if-eqz v0, :cond_1

    goto :goto_1
.end method

.method private dJf(Ljava/util/List;Landroid/support/v7/c/i;III)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-boolean v0, p0, Landroid/support/v7/c/e;->eru:Z

    if-nez v0, :cond_0

    invoke-interface {p2, p3, p4}, Landroid/support/v7/c/i;->dJk(II)V

    return-void

    :cond_0
    add-int/lit8 v0, p4, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/c/e;->erp:[I

    add-int v2, p5, v1

    aget v0, v0, v2

    and-int/lit8 v0, v0, 0x1f

    sparse-switch v0, :sswitch_data_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unknown flag for pos "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/2addr v1, p5

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :sswitch_0
    invoke-interface {p2, p3, v4}, Landroid/support/v7/c/i;->dJk(II)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/f;

    iget v3, v0, Landroid/support/v7/c/f;->ery:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/support/v7/c/f;->ery:I

    goto :goto_1

    :sswitch_1
    iget-object v2, p0, Landroid/support/v7/c/e;->erp:[I

    add-int v3, p5, v1

    aget v2, v2, v3

    shr-int/lit8 v2, v2, 0x5

    invoke-static {p1, v2, v4}, Landroid/support/v7/c/e;->dJb(Ljava/util/List;IZ)Landroid/support/v7/c/f;

    move-result-object v3

    iget v3, v3, Landroid/support/v7/c/f;->ery:I

    invoke-interface {p2, v3, p3}, Landroid/support/v7/c/i;->dJh(II)V

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Landroid/support/v7/c/e;->erq:Landroid/support/v7/c/b;

    add-int v3, p5, v1

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/c/b;->getChangePayload(II)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, p3, v4, v0}, Landroid/support/v7/c/i;->dJj(IILjava/lang/Object;)V

    :cond_1
    :goto_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :sswitch_2
    new-instance v0, Landroid/support/v7/c/f;

    add-int v2, p5, v1

    invoke-direct {v0, v2, p3, v5}, Landroid/support/v7/c/f;-><init>(IIZ)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public dIX(Landroid/support/v7/c/i;)V
    .locals 11

    instance-of v0, p1, Landroid/support/v7/c/j;

    if-eqz v0, :cond_3

    check-cast p1, Landroid/support/v7/c/j;

    move-object v2, p1

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget v4, p0, Landroid/support/v7/c/e;->ers:I

    iget v3, p0, Landroid/support/v7/c/e;->err:I

    iget-object v0, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v7, v0

    move v8, v3

    :goto_1
    if-ltz v7, :cond_5

    iget-object v0, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/support/v7/c/c;

    iget v9, v6, Landroid/support/v7/c/c;->erg:I

    iget v0, v6, Landroid/support/v7/c/c;->erk:I

    add-int v3, v0, v9

    iget v0, v6, Landroid/support/v7/c/c;->erj:I

    add-int v10, v0, v9

    if-ge v3, v4, :cond_0

    sub-int/2addr v4, v3

    move-object v0, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/c/e;->dJa(Ljava/util/List;Landroid/support/v7/c/i;III)V

    :cond_0
    if-ge v10, v8, :cond_1

    sub-int v4, v8, v10

    move-object v0, p0

    move v5, v10

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/c/e;->dJf(Ljava/util/List;Landroid/support/v7/c/i;III)V

    :cond_1
    add-int/lit8 v0, v9, -0x1

    :goto_2
    if-ltz v0, :cond_4

    iget-object v3, p0, Landroid/support/v7/c/e;->ert:[I

    iget v4, v6, Landroid/support/v7/c/c;->erk:I

    add-int/2addr v4, v0

    aget v3, v3, v4

    and-int/lit8 v3, v3, 0x1f

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    iget v3, v6, Landroid/support/v7/c/c;->erk:I

    add-int/2addr v3, v0

    iget-object v4, p0, Landroid/support/v7/c/e;->erq:Landroid/support/v7/c/b;

    iget v5, v6, Landroid/support/v7/c/c;->erk:I

    add-int/2addr v5, v0

    iget v8, v6, Landroid/support/v7/c/c;->erj:I

    add-int/2addr v8, v0

    invoke-virtual {v4, v5, v8}, Landroid/support/v7/c/b;->getChangePayload(II)Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v5, v4}, Landroid/support/v7/c/j;->dJj(IILjava/lang/Object;)V

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_3
    new-instance v2, Landroid/support/v7/c/j;

    invoke-direct {v2, p1}, Landroid/support/v7/c/j;-><init>(Landroid/support/v7/c/i;)V

    goto :goto_0

    :cond_4
    iget v4, v6, Landroid/support/v7/c/c;->erk:I

    iget v3, v6, Landroid/support/v7/c/c;->erj:I

    add-int/lit8 v0, v7, -0x1

    move v7, v0

    move v8, v3

    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Landroid/support/v7/c/j;->dJl()V

    return-void
.end method

.method public dJc(Landroid/support/v7/widget/b;)V
    .locals 1

    new-instance v0, Landroid/support/v7/c/h;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/c/h;-><init>(Landroid/support/v7/c/e;Landroid/support/v7/widget/b;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/c/e;->dIX(Landroid/support/v7/c/i;)V

    return-void
.end method

.method getSnakes()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/c/e;->erv:Ljava/util/List;

    return-object v0
.end method
