.class public Landroid/support/v7/widget/Z;
.super Landroid/support/v7/widget/aa;
.source "DefaultItemAnimator.java"


# static fields
.field private static ebw:Landroid/animation/TimeInterpolator;


# instance fields
.field ebA:Ljava/util/ArrayList;

.field ebB:Ljava/util/ArrayList;

.field ebC:Ljava/util/ArrayList;

.field ebD:Ljava/util/ArrayList;

.field private ebE:Ljava/util/ArrayList;

.field ebF:Ljava/util/ArrayList;

.field private ebG:Ljava/util/ArrayList;

.field private ebH:Ljava/util/ArrayList;

.field ebx:Ljava/util/ArrayList;

.field eby:Ljava/util/ArrayList;

.field private ebz:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/aa;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->eby:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebC:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebB:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebx:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebD:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebA:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Z;->ebF:Ljava/util/ArrayList;

    return-void
.end method

.method private dxp(Ljava/util/List;Landroid/support/v7/widget/p;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ac;

    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/Z;->dxz(Landroid/support/v7/widget/ac;Landroid/support/v7/widget/p;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Landroid/support/v7/widget/ac;->ebR:Landroid/support/v7/widget/p;

    if-nez v2, :cond_0

    iget-object v2, v0, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    if-nez v2, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private dxq(Landroid/support/v7/widget/p;)V
    .locals 2

    sget-object v0, Landroid/support/v7/widget/Z;->ebw:Landroid/animation/TimeInterpolator;

    if-nez v0, :cond_0

    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/Z;->ebw:Landroid/animation/TimeInterpolator;

    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Landroid/support/v7/widget/Z;->ebw:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/Z;->duT(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method private dxr(Landroid/support/v7/widget/ac;)V
    .locals 1

    iget-object v0, p1, Landroid/support/v7/widget/ac;->ebR:Landroid/support/v7/widget/p;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/support/v7/widget/ac;->ebR:Landroid/support/v7/widget/p;

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/Z;->dxz(Landroid/support/v7/widget/ac;Landroid/support/v7/widget/p;)Z

    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/Z;->dxz(Landroid/support/v7/widget/ac;Landroid/support/v7/widget/p;)Z

    :cond_1
    return-void
.end method

.method private dxu(Landroid/support/v7/widget/p;)V
    .locals 4

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/Z;->ebA:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->dvc()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Landroid/support/v7/widget/ag;

    invoke-direct {v3, p0, p1, v1, v0}, Landroid/support/v7/widget/ag;-><init>(Landroid/support/v7/widget/Z;Landroid/support/v7/widget/p;Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method private dxz(Landroid/support/v7/widget/ac;Landroid/support/v7/widget/p;)Z
    .locals 5

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v2, p1, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    if-ne v2, p2, :cond_0

    iput-object v3, p1, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    :goto_0
    iget-object v2, p2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    iget-object v2, p2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setTranslationX(F)V

    iget-object v2, p2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p0, p2, v0}, Landroid/support/v7/widget/Z;->dxB(Landroid/support/v7/widget/p;Z)V

    return v1

    :cond_0
    iget-object v2, p1, Landroid/support/v7/widget/ac;->ebR:Landroid/support/v7/widget/p;

    if-ne v2, p2, :cond_1

    iput-object v3, p1, Landroid/support/v7/widget/ac;->ebR:Landroid/support/v7/widget/p;

    move v0, v1

    goto :goto_0

    :cond_1
    return v0
.end method


# virtual methods
.method public CY(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;IIII)Z
    .locals 8

    if-ne p1, p2, :cond_0

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/Z;->dxt(Landroid/support/v7/widget/p;IIII)Z

    move-result v0

    return v0

    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    iget-object v2, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getAlpha()F

    move-result v2

    invoke-direct {p0, p1}, Landroid/support/v7/widget/Z;->dxq(Landroid/support/v7/widget/p;)V

    sub-int v3, p5, p3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    float-to-int v3, v3

    sub-int v4, p6, p4

    int-to-float v4, v4

    sub-float/2addr v4, v1

    float-to-int v4, v4

    iget-object v5, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    if-eqz p2, :cond_1

    invoke-direct {p0, p2}, Landroid/support/v7/widget/Z;->dxq(Landroid/support/v7/widget/p;)V

    iget-object v0, p2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    neg-int v1, v3

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    neg-int v1, v4

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    iget-object v7, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    new-instance v0, Landroid/support/v7/widget/ac;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/ac;-><init>(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;IIII)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public duO()V
    .locals 13

    const-wide/16 v2, 0x0

    const/4 v12, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v1, v0, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v4, v0, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v8, v0, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v5, v0, 0x1

    if-nez v1, :cond_0

    xor-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_0

    xor-int/lit8 v0, v5, 0x1

    if-eqz v0, :cond_0

    xor-int/lit8 v0, v8, 0x1

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-direct {p0, v0}, Landroid/support/v7/widget/Z;->dxu(Landroid/support/v7/widget/p;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz v4, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v6, p0, Landroid/support/v7/widget/Z;->ebC:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    new-instance v6, Landroid/support/v7/widget/ad;

    invoke-direct {v6, p0, v0}, Landroid/support/v7/widget/ad;-><init>(Landroid/support/v7/widget/Z;Ljava/util/ArrayList;)V

    if-eqz v1, :cond_6

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ab;

    iget-object v0, v0, Landroid/support/v7/widget/ab;->ebM:Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->dvc()J

    move-result-wide v10

    invoke-static {v0, v6, v10, v11}, Landroid/support/v4/view/z;->dPA(Landroid/view/View;Ljava/lang/Runnable;J)V

    :cond_2
    :goto_1
    if-eqz v8, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v6, p0, Landroid/support/v7/widget/Z;->ebB:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    new-instance v6, Landroid/support/v7/widget/ae;

    invoke-direct {v6, p0, v0}, Landroid/support/v7/widget/ae;-><init>(Landroid/support/v7/widget/Z;Ljava/util/ArrayList;)V

    if-eqz v1, :cond_7

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ac;

    iget-object v0, v0, Landroid/support/v7/widget/ac;->ebR:Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->dvc()J

    move-result-wide v10

    invoke-static {v0, v6, v10, v11}, Landroid/support/v4/view/z;->dPA(Landroid/view/View;Ljava/lang/Runnable;J)V

    :cond_3
    :goto_2
    if-eqz v5, :cond_5

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Landroid/support/v7/widget/Z;->eby:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    new-instance v10, Landroid/support/v7/widget/af;

    invoke-direct {v10, p0, v9}, Landroid/support/v7/widget/af;-><init>(Landroid/support/v7/widget/Z;Ljava/util/ArrayList;)V

    if-nez v1, :cond_4

    if-nez v4, :cond_4

    if-eqz v8, :cond_b

    :cond_4
    if-eqz v1, :cond_8

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->dvc()J

    move-result-wide v0

    move-wide v6, v0

    :goto_3
    if-eqz v4, :cond_9

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->duX()J

    move-result-wide v0

    move-wide v4, v0

    :goto_4
    if-eqz v8, :cond_a

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->duM()J

    move-result-wide v0

    :goto_5
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    add-long v2, v6, v0

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-static {v0, v10, v2, v3}, Landroid/support/v4/view/z;->dPA(Landroid/view/View;Ljava/lang/Runnable;J)V

    :cond_5
    :goto_6
    return-void

    :cond_6
    invoke-interface {v6}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    :cond_7
    invoke-interface {v6}, Ljava/lang/Runnable;->run()V

    goto :goto_2

    :cond_8
    move-wide v6, v2

    goto :goto_3

    :cond_9
    move-wide v4, v2

    goto :goto_4

    :cond_a
    move-wide v0, v2

    goto :goto_5

    :cond_b
    invoke-interface {v10}, Ljava/lang/Runnable;->run()V

    goto :goto_6
.end method

.method public duT(Landroid/support/v7/widget/p;)V
    .locals 7

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    iget-object v4, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ab;

    iget-object v0, v0, Landroid/support/v7/widget/ab;->ebM:Landroid/support/v7/widget/p;

    if-ne v0, p1, :cond_0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/Z;->dxQ(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1}, Landroid/support/v7/widget/Z;->dxp(Ljava/util/List;Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4, v6}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/Z;->dxN(Landroid/support/v7/widget/p;)V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v4, v6}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/Z;->dxE(Landroid/support/v7/widget/p;)V

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebB:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1}, Landroid/support/v7/widget/Z;->dxp(Ljava/util/List;Landroid/support/v7/widget/p;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebB:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_2
    if-ltz v3, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebC:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_3
    if-ltz v2, :cond_6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ab;

    iget-object v1, v1, Landroid/support/v7/widget/ab;->ebM:Landroid/support/v7/widget/p;

    if-ne v1, p1, :cond_7

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/Z;->dxQ(Landroid/support/v7/widget/p;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebC:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_6
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_2

    :cond_7
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_3

    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/Z;->eby:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4
    if-ltz v1, :cond_a

    iget-object v0, p0, Landroid/support/v7/widget/Z;->eby:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v4, v6}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/Z;->dxE(Landroid/support/v7/widget/p;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/Z;->eby:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_9
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4

    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebA:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebx:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebF:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebD:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->dxw()V

    return-void
.end method

.method public duV(Landroid/support/v7/widget/p;Ljava/util/List;)Z
    .locals 1

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/aa;->duV(Landroid/support/v7/widget/p;Ljava/util/List;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dvh()V
    .locals 7

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ab;

    iget-object v2, v0, Landroid/support/v7/widget/ab;->ebM:Landroid/support/v7/widget/p;

    iget-object v2, v2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, v0, Landroid/support/v7/widget/ab;->ebM:Landroid/support/v7/widget/p;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/Z;->dxQ(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/Z;->dxN(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/Z;->dxE(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ac;

    invoke-direct {p0, v0}, Landroid/support/v7/widget/Z;->dxr(Landroid/support/v7/widget/ac;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->isRunning()Z

    move-result v0

    if-nez v0, :cond_4

    return-void

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_4
    if-ltz v3, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebC:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_5
    if-ltz v2, :cond_6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ab;

    iget-object v4, v1, Landroid/support/v7/widget/ab;->ebM:Landroid/support/v7/widget/p;

    iget-object v4, v4, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-object v1, v1, Landroid/support/v7/widget/ab;->ebM:Landroid/support/v7/widget/p;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/Z;->dxQ(Landroid/support/v7/widget/p;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Landroid/support/v7/widget/Z;->ebC:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_5

    :cond_6
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_4

    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/Z;->eby:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_6
    if-ltz v3, :cond_a

    iget-object v0, p0, Landroid/support/v7/widget/Z;->eby:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_7
    if-ltz v2, :cond_9

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/p;

    iget-object v4, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/Z;->dxE(Landroid/support/v7/widget/p;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Landroid/support/v7/widget/Z;->eby:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_8
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_7

    :cond_9
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_6

    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_8
    if-ltz v3, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebB:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_9
    if-ltz v2, :cond_c

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ac;

    invoke-direct {p0, v1}, Landroid/support/v7/widget/Z;->dxr(Landroid/support/v7/widget/ac;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Landroid/support/v7/widget/Z;->ebB:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_b
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_9

    :cond_c
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_8

    :cond_d
    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebA:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/Z;->dxy(Ljava/util/List;)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebD:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/Z;->dxy(Ljava/util/List;)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebx:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/Z;->dxy(Ljava/util/List;)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebF:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/Z;->dxy(Ljava/util/List;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->duN()V

    return-void
.end method

.method dxA(Landroid/support/v7/widget/p;)V
    .locals 6

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/Z;->ebx:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->duY()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Landroid/support/v7/widget/ah;

    invoke-direct {v3, p0, p1, v0, v1}, Landroid/support/v7/widget/ah;-><init>(Landroid/support/v7/widget/Z;Landroid/support/v7/widget/p;Landroid/view/View;Landroid/view/ViewPropertyAnimator;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method dxo(Landroid/support/v7/widget/p;IIII)V
    .locals 8

    const/4 v1, 0x0

    iget-object v4, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    sub-int v3, p4, p2

    sub-int v5, p5, p3

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    :cond_0
    if-eqz v5, :cond_1

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    :cond_1
    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebD:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->duX()J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    new-instance v0, Landroid/support/v7/widget/ai;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/ai;-><init>(Landroid/support/v7/widget/Z;Landroid/support/v7/widget/p;ILandroid/view/View;ILandroid/view/ViewPropertyAnimator;)V

    invoke-virtual {v7, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method dxs(Landroid/support/v7/widget/ac;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    iget-object v0, p1, Landroid/support/v7/widget/ac;->ebR:Landroid/support/v7/widget/p;

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_0
    iget-object v2, p1, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    if-eqz v2, :cond_0

    iget-object v1, v2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->duM()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v7/widget/Z;->ebF:Ljava/util/ArrayList;

    iget-object v4, p1, Landroid/support/v7/widget/ac;->ebR:Landroid/support/v7/widget/p;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v3, p1, Landroid/support/v7/widget/ac;->ebT:I

    iget v4, p1, Landroid/support/v7/widget/ac;->ebP:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    iget v3, p1, Landroid/support/v7/widget/ac;->ebS:I

    iget v4, p1, Landroid/support/v7/widget/ac;->ebO:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v2, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    new-instance v4, Landroid/support/v7/widget/aj;

    invoke-direct {v4, p0, p1, v2, v0}, Landroid/support/v7/widget/aj;-><init>(Landroid/support/v7/widget/Z;Landroid/support/v7/widget/ac;Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v7/widget/Z;->ebF:Ljava/util/ArrayList;

    iget-object v3, p1, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->duM()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Landroid/support/v7/widget/ak;

    invoke-direct {v3, p0, p1, v0, v1}, Landroid/support/v7/widget/ak;-><init>(Landroid/support/v7/widget/Z;Landroid/support/v7/widget/ac;Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    goto :goto_0
.end method

.method public dxt(Landroid/support/v7/widget/p;IIII)Z
    .locals 7

    const/4 v5, 0x0

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    move-result v1

    float-to-int v1, v1

    add-int v2, p2, v1

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    float-to-int v1, v1

    add-int v3, p3, v1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/Z;->dxq(Landroid/support/v7/widget/p;)V

    sub-int v1, p4, v2

    sub-int v4, p5, v3

    if-nez v1, :cond_0

    if-nez v4, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/Z;->dxQ(Landroid/support/v7/widget/p;)V

    return v5

    :cond_0
    if-eqz v1, :cond_1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    :cond_1
    if-eqz v4, :cond_2

    neg-int v1, v4

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    :cond_2
    iget-object v6, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    new-instance v0, Landroid/support/v7/widget/ab;

    move-object v1, p1

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/ab;-><init>(Landroid/support/v7/widget/p;IIII)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public dxv(Landroid/support/v7/widget/p;)Z
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/Z;->dxq(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method

.method dxw()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/Z;->duN()V

    :cond_0
    return-void
.end method

.method public dxx(Landroid/support/v7/widget/p;)Z
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v7/widget/Z;->dxq(Landroid/support/v7/widget/p;)V

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method

.method dxy(Ljava/util/List;)V
    .locals 2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public isRunning()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebz:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebG:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebD:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebF:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->eby:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/Z;->ebB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
