.class Landroid/support/v7/widget/Q;
.super Ljava/lang/Object;
.source "ViewBoundsCheck.java"


# instance fields
.field ebj:Landroid/support/v7/widget/cS;

.field final ebk:Landroid/support/v7/widget/P;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/P;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/widget/Q;->ebk:Landroid/support/v7/widget/P;

    new-instance v0, Landroid/support/v7/widget/cS;

    invoke-direct {v0}, Landroid/support/v7/widget/cS;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/Q;->ebj:Landroid/support/v7/widget/cS;

    return-void
.end method


# virtual methods
.method dxj(IIII)Landroid/view/View;
    .locals 8

    iget-object v0, p0, Landroid/support/v7/widget/Q;->ebk:Landroid/support/v7/widget/P;

    invoke-interface {v0}, Landroid/support/v7/widget/P;->dxe()I

    move-result v3

    iget-object v0, p0, Landroid/support/v7/widget/Q;->ebk:Landroid/support/v7/widget/P;

    invoke-interface {v0}, Landroid/support/v7/widget/P;->dxg()I

    move-result v4

    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x0

    :goto_1
    if-eq p1, p2, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/Q;->ebk:Landroid/support/v7/widget/P;

    invoke-interface {v1, p1}, Landroid/support/v7/widget/P;->dxh(I)Landroid/view/View;

    move-result-object v1

    iget-object v5, p0, Landroid/support/v7/widget/Q;->ebk:Landroid/support/v7/widget/P;

    invoke-interface {v5, v1}, Landroid/support/v7/widget/P;->dxf(Landroid/view/View;)I

    move-result v5

    iget-object v6, p0, Landroid/support/v7/widget/Q;->ebk:Landroid/support/v7/widget/P;

    invoke-interface {v6, v1}, Landroid/support/v7/widget/P;->dxi(Landroid/view/View;)I

    move-result v6

    iget-object v7, p0, Landroid/support/v7/widget/Q;->ebj:Landroid/support/v7/widget/cS;

    invoke-virtual {v7, v3, v4, v5, v6}, Landroid/support/v7/widget/cS;->setBounds(IIII)V

    if-eqz p3, :cond_1

    iget-object v5, p0, Landroid/support/v7/widget/Q;->ebj:Landroid/support/v7/widget/cS;

    invoke-virtual {v5}, Landroid/support/v7/widget/cS;->dIA()V

    iget-object v5, p0, Landroid/support/v7/widget/Q;->ebj:Landroid/support/v7/widget/cS;

    invoke-virtual {v5, p3}, Landroid/support/v7/widget/cS;->addFlags(I)V

    iget-object v5, p0, Landroid/support/v7/widget/Q;->ebj:Landroid/support/v7/widget/cS;

    invoke-virtual {v5}, Landroid/support/v7/widget/cS;->dIB()Z

    move-result v5

    if-eqz v5, :cond_1

    return-object v1

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_3

    iget-object v5, p0, Landroid/support/v7/widget/Q;->ebj:Landroid/support/v7/widget/cS;

    invoke-virtual {v5}, Landroid/support/v7/widget/cS;->dIA()V

    iget-object v5, p0, Landroid/support/v7/widget/Q;->ebj:Landroid/support/v7/widget/cS;

    invoke-virtual {v5, p4}, Landroid/support/v7/widget/cS;->addFlags(I)V

    iget-object v5, p0, Landroid/support/v7/widget/Q;->ebj:Landroid/support/v7/widget/cS;

    invoke-virtual {v5}, Landroid/support/v7/widget/cS;->dIB()Z

    move-result v5

    if-eqz v5, :cond_3

    :goto_2
    add-int/2addr p1, v0

    move-object v2, v1

    goto :goto_1

    :cond_2
    return-object v2

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method
