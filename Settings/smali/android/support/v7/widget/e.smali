.class public Landroid/support/v7/widget/e;
.super Ljava/lang/Object;
.source "RecyclerView.java"


# instance fields
.field private dZA:I

.field dZB:I

.field dZC:I

.field dZD:Z

.field dZE:I

.field dZF:Z

.field dZG:Z

.field dZH:I

.field dZI:I

.field dZJ:Z

.field dZK:Z

.field private dZu:Landroid/util/SparseArray;

.field dZv:I

.field dZw:J

.field dZx:I

.field dZy:Z

.field dZz:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/e;->dZA:I

    iput v1, p0, Landroid/support/v7/widget/e;->dZC:I

    iput v1, p0, Landroid/support/v7/widget/e;->dZv:I

    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/e;->dZI:I

    iput v1, p0, Landroid/support/v7/widget/e;->dZH:I

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->dZG:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->dZD:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->dZy:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->dZK:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->dZF:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->dZJ:Z

    return-void
.end method

.method static synthetic dto(Landroid/support/v7/widget/e;I)I
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/e;->dZA:I

    return p1
.end method


# virtual methods
.method dtj(I)V
    .locals 3

    iget v0, p0, Landroid/support/v7/widget/e;->dZI:I

    and-int/2addr v0, p1

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Layout state should be one of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " but it is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/widget/e;->dZI:I

    invoke-static {v2}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public dtk()Z
    .locals 2

    iget v0, p0, Landroid/support/v7/widget/e;->dZA:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dtl()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/e;->dZJ:Z

    return v0
.end method

.method dtm(Landroid/support/v7/widget/b;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/e;->dZI:I

    invoke-virtual {p1}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/e;->dZH:I

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->dZD:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->dZy:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->dZK:Z

    return-void
.end method

.method public dtn()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/e;->dZD:Z

    return v0
.end method

.method public getItemCount()I
    .locals 2

    iget-boolean v0, p0, Landroid/support/v7/widget/e;->dZD:Z

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/e;->dZC:I

    iget v1, p0, Landroid/support/v7/widget/e;->dZv:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/e;->dZH:I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "State{mTargetPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/e;->dZA:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/e;->dZu:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mItemCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/e;->dZH:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mPreviousLayoutItemCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/e;->dZC:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mDeletedInvisibleItemCountSincePreviousLayout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/e;->dZv:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mStructureChanged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/e;->dZG:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mInPreLayout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/e;->dZD:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mRunSimpleAnimations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/e;->dZF:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mRunPredictiveAnimations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/e;->dZJ:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
