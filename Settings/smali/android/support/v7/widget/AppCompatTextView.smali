.class public Landroid/support/v7/widget/AppCompatTextView;
.super Landroid/widget/TextView;
.source "AppCompatTextView.java"

# interfaces
.implements Landroid/support/v4/view/ae;
.implements Landroid/support/v4/widget/z;


# instance fields
.field private final ekK:Landroid/support/v7/widget/cJ;

.field private final ekL:Landroid/support/v7/widget/aC;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1010084

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-static {p1}, Landroid/support/v7/widget/bM;->dEu(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/support/v7/widget/aC;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/aC;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/aC;->dAg(Landroid/util/AttributeSet;I)V

    invoke-static {p0}, Landroid/support/v7/widget/cJ;->dHU(Landroid/widget/TextView;)Landroid/support/v7/widget/cJ;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/cJ;->dHC(Landroid/util/AttributeSet;I)V

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0}, Landroid/support/v7/widget/cJ;->dHD()V

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 1

    invoke-super {p0}, Landroid/widget/TextView;->drawableStateChanged()V

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    invoke-virtual {v0}, Landroid/support/v7/widget/aC;->dAi()V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0}, Landroid/support/v7/widget/cJ;->dHD()V

    :cond_1
    return-void
.end method

.method public getAutoSizeMaxTextSize()I
    .locals 1

    sget-boolean v0, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeMaxTextSize()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0}, Landroid/support/v7/widget/cJ;->getAutoSizeMaxTextSize()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getAutoSizeMinTextSize()I
    .locals 1

    sget-boolean v0, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeMinTextSize()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0}, Landroid/support/v7/widget/cJ;->getAutoSizeMinTextSize()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getAutoSizeStepGranularity()I
    .locals 1

    sget-boolean v0, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeStepGranularity()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0}, Landroid/support/v7/widget/cJ;->getAutoSizeStepGranularity()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getAutoSizeTextAvailableSizes()[I
    .locals 1

    sget-boolean v0, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeTextAvailableSizes()[I

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0}, Landroid/support/v7/widget/cJ;->getAutoSizeTextAvailableSizes()[I

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0
.end method

.method public getAutoSizeTextType()I
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-boolean v2, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    if-eqz v2, :cond_1

    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeTextType()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0}, Landroid/support/v7/widget/cJ;->getAutoSizeTextType()I

    move-result v0

    return v0

    :cond_2
    return v1
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    invoke-virtual {v0}, Landroid/support/v7/widget/aC;->getSupportBackgroundTintList()Landroid/content/res/ColorStateList;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    invoke-virtual {v0}, Landroid/support/v7/widget/aC;->getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    invoke-super/range {p0 .. p5}, Landroid/widget/TextView;->onLayout(ZIIII)V

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/cJ;->onLayout(ZIIII)V

    :cond_0
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_0

    sget-boolean v0, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0}, Landroid/support/v7/widget/cJ;->dHW()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0}, Landroid/support/v7/widget/cJ;->dHO()V

    :cond_0
    return-void
.end method

.method public setAutoSizeTextTypeUniformWithConfiguration(IIII)V
    .locals 1

    sget-boolean v0, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    if-eqz v0, :cond_1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setAutoSizeTextTypeUniformWithConfiguration(IIII)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v7/widget/cJ;->setAutoSizeTextTypeUniformWithConfiguration(IIII)V

    goto :goto_0
.end method

.method public setAutoSizeTextTypeUniformWithPresetSizes([II)V
    .locals 1

    sget-boolean v0, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    if-eqz v0, :cond_1

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setAutoSizeTextTypeUniformWithPresetSizes([II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/cJ;->setAutoSizeTextTypeUniformWithPresetSizes([II)V

    goto :goto_0
.end method

.method public setAutoSizeTextTypeWithDefaults(I)V
    .locals 1

    sget-boolean v0, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    if-eqz v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setAutoSizeTextTypeWithDefaults(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/cJ;->setAutoSizeTextTypeWithDefaults(I)V

    goto :goto_0
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aC;->dAd(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aC;->dAe(I)V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aC;->setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekL:Landroid/support/v7/widget/aC;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aC;->setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/cJ;->dHR(Landroid/content/Context;I)V

    :cond_0
    return-void
.end method

.method public setTextSize(IF)V
    .locals 1

    sget-boolean v0, Landroid/support/v7/widget/AppCompatTextView;->eDr:Z

    if-eqz v0, :cond_1

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatTextView;->ekK:Landroid/support/v7/widget/cJ;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/cJ;->setTextSize(IF)V

    goto :goto_0
.end method
