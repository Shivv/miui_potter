.class public abstract Landroid/support/v7/widget/a;
.super Ljava/lang/Object;
.source "RecyclerView.java"


# instance fields
.field private dZc:Z

.field dZd:Z

.field private dZe:I

.field private final dZf:Landroid/support/v7/widget/P;

.field dZg:Landroid/support/v7/widget/RecyclerView;

.field dZh:Z

.field dZi:Z

.field dZj:Landroid/support/v7/widget/Q;

.field private dZk:I

.field private dZl:Z

.field private dZm:I

.field private dZn:I

.field dZo:Landroid/support/v7/widget/G;

.field dZp:Landroid/support/v7/widget/q;

.field dZq:I

.field dZr:Landroid/support/v7/widget/Q;

.field dZs:Z

.field private final dZt:Landroid/support/v7/widget/P;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v7/widget/N;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/N;-><init>(Landroid/support/v7/widget/a;)V

    iput-object v0, p0, Landroid/support/v7/widget/a;->dZf:Landroid/support/v7/widget/P;

    new-instance v0, Landroid/support/v7/widget/O;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/O;-><init>(Landroid/support/v7/widget/a;)V

    iput-object v0, p0, Landroid/support/v7/widget/a;->dZt:Landroid/support/v7/widget/P;

    new-instance v0, Landroid/support/v7/widget/Q;

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZf:Landroid/support/v7/widget/P;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/Q;-><init>(Landroid/support/v7/widget/P;)V

    iput-object v0, p0, Landroid/support/v7/widget/a;->dZr:Landroid/support/v7/widget/Q;

    new-instance v0, Landroid/support/v7/widget/Q;

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZt:Landroid/support/v7/widget/P;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/Q;-><init>(Landroid/support/v7/widget/P;)V

    iput-object v0, p0, Landroid/support/v7/widget/a;->dZj:Landroid/support/v7/widget/Q;

    iput-boolean v2, p0, Landroid/support/v7/widget/a;->dZs:Z

    iput-boolean v2, p0, Landroid/support/v7/widget/a;->dZi:Z

    iput-boolean v2, p0, Landroid/support/v7/widget/a;->dZd:Z

    iput-boolean v3, p0, Landroid/support/v7/widget/a;->dZl:Z

    iput-boolean v3, p0, Landroid/support/v7/widget/a;->dZc:Z

    return-void
.end method

.method private dqV(Landroid/view/View;IZ)V
    .locals 6

    const/4 v5, -0x1

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v1

    if-nez p3, :cond_0

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/H;->dwl(Landroid/support/v7/widget/p;)V

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->wasReturnedFromScrap()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->isScrap()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_1
    invoke-virtual {v1}, Landroid/support/v7/widget/p;->isScrap()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->unScrap()V

    :goto_1
    iget-object v2, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v2, p1, p2, v3, v4}, Landroid/support/v7/widget/G;->dvV(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    :cond_2
    :goto_2
    iget-boolean v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eah:Z

    if-eqz v2, :cond_3

    iget-object v1, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    iput-boolean v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eah:Z

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/H;->dwn(Landroid/support/v7/widget/p;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Landroid/support/v7/widget/p;->clearReturnedFromScrapFlag()V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-ne v2, v3, :cond_9

    iget-object v2, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/G;->dvM(Landroid/view/View;)I

    move-result v2

    if-ne p2, v5, :cond_7

    iget-object v3, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v3}, Landroid/support/v7/widget/G;->dvZ()I

    move-result p2

    :cond_7
    if-ne v2, v5, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Added View has RecyclerView as parent but view is not a real child. Unfiltered index:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    if-eq v2, p2, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v3, v2, p2}, Landroid/support/v7/widget/a;->dtc(II)V

    goto :goto_2

    :cond_9
    iget-object v2, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v2, p1, p2, v4}, Landroid/support/v7/widget/G;->dvR(Landroid/view/View;IZ)V

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eae:Z

    iget-object v2, p0, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    invoke-virtual {v2}, Landroid/support/v7/widget/q;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/q;->duF(Landroid/view/View;)V

    goto :goto_2
.end method

.method private static drL(III)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    if-lez p2, :cond_0

    if-eq p0, p2, :cond_0

    return v1

    :cond_0
    sparse-switch v2, :sswitch_data_0

    return v1

    :sswitch_0
    return v0

    :sswitch_1
    if-lt v3, p0, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :sswitch_2
    if-ne v3, p0, :cond_2

    :goto_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method private drj(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;Z)[I
    .locals 14

    const/4 v1, 0x2

    new-array v4, v1, [I

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dqU()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsg()I

    move-result v6

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drZ()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsS()I

    move-result v2

    sub-int v7, v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dre()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsR()I

    move-result v2

    sub-int v8, v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLeft()I

    move-result v1

    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getScrollX()I

    move-result v2

    sub-int v9, v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTop()I

    move-result v1

    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getScrollY()I

    move-result v2

    sub-int v10, v1, v2

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int v11, v9, v1

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int v12, v10, v1

    sub-int v1, v9, v5

    const/4 v2, 0x0

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    sub-int v1, v10, v6

    const/4 v2, 0x0

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int v2, v11, v7

    const/4 v13, 0x0

    invoke-static {v13, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    sub-int v8, v12, v8

    const/4 v12, 0x0

    invoke-static {v12, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drR()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    if-eqz v2, :cond_0

    :goto_0
    if-eqz v1, :cond_3

    :goto_1
    const/4 v3, 0x0

    aput v2, v4, v3

    const/4 v2, 0x1

    aput v1, v4, v2

    return-object v4

    :cond_0
    sub-int v2, v11, v7

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    sub-int v3, v9, v5

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_0

    :cond_3
    sub-int v1, v10, v6

    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_1
.end method

.method private drw(Landroid/support/v7/widget/j;ILandroid/view/View;)V
    .locals 2

    invoke-static {p3}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p2}, Landroid/support/v7/widget/a;->drF(I)V

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/j;->dur(Landroid/support/v7/widget/p;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/a;->dsn(I)V

    invoke-virtual {p1, p3}, Landroid/support/v7/widget/j;->dtP(Landroid/view/View;)V

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/H;->dwf(Landroid/support/v7/widget/p;)V

    goto :goto_0
.end method

.method public static dry(III)I
    .locals 2

    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    :sswitch_0
    return v1

    :sswitch_1
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method private dsH(ILandroid/view/View;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/G;->dvW(I)V

    return-void
.end method

.method public static dsP(IIIIZ)I
    .locals 6

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/high16 v2, -0x80000000

    const/high16 v0, 0x40000000    # 2.0f

    const/4 v1, 0x0

    sub-int v3, p0, p2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-eqz p4, :cond_3

    if-ltz p3, :cond_0

    move p1, v0

    move v1, p3

    :goto_0
    invoke-static {v1, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0

    :cond_0
    if-ne p3, v5, :cond_2

    sparse-switch p1, :sswitch_data_0

    :cond_1
    move p1, v1

    goto :goto_0

    :sswitch_0
    move v1, v3

    goto :goto_0

    :sswitch_1
    move p1, v1

    goto :goto_0

    :cond_2
    if-ne p3, v4, :cond_1

    move p1, v1

    goto :goto_0

    :cond_3
    if-ltz p3, :cond_4

    move p1, v0

    move v1, p3

    goto :goto_0

    :cond_4
    if-ne p3, v5, :cond_5

    move v1, v3

    goto :goto_0

    :cond_5
    if-ne p3, v4, :cond_1

    if-eq p1, v2, :cond_6

    if-ne p1, v0, :cond_7

    :cond_6
    move p1, v2

    move v1, v3

    goto :goto_0

    :cond_7
    move p1, v1

    move v1, v3

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method private dsl(Landroid/support/v7/widget/q;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    :cond_0
    return-void
.end method

.method static synthetic dsv(Landroid/support/v7/widget/a;Landroid/support/v7/widget/q;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/a;->dsl(Landroid/support/v7/widget/q;)V

    return-void
.end method

.method private dte(Landroid/support/v7/widget/RecyclerView;II)Z
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    return v6

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dqU()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsg()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drZ()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsS()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dre()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsR()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v5}, Landroid/support/v7/widget/a;->dsr(Landroid/view/View;Landroid/graphics/Rect;)V

    iget v0, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, p2

    if-ge v0, v3, :cond_1

    iget v0, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, p2

    if-gt v0, v1, :cond_2

    :cond_1
    return v6

    :cond_2
    iget v0, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, p3

    if-ge v0, v4, :cond_1

    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, p3

    if-le v0, v2, :cond_1

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public Hh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/View;Landroid/support/v4/view/a/a;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p3}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, p3}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v2

    :goto_1
    move v3, v1

    move v5, v4

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/a/e;->dNI(IIIIZZ)Landroid/support/v4/view/a/e;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/support/v4/view/a/a;->dMT(Ljava/lang/Object;)V

    return-void

    :cond_0
    move v0, v4

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1
.end method

.method public Hi(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public addView(Landroid/view/View;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/a;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/a;->dqV(Landroid/view/View;IZ)V

    return-void
.end method

.method public dqS(Landroid/support/v7/widget/e;)V
    .locals 0

    return-void
.end method

.method public dqT(Landroid/view/View;IIII)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, p2

    iget v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v2, v3

    iget v3, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, p3

    iget v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v3, v4

    iget v4, v1, Landroid/graphics/Rect;->right:I

    sub-int v4, p4, v4

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    sub-int/2addr v4, v5

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v1, p5, v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    sub-int v0, v1, v0

    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public dqU()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dqW()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/a;->dZc:Z

    return v0
.end method

.method public dqX(Landroid/support/v7/widget/j;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drc()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0, p1}, Landroid/support/v7/widget/a;->drY(ILandroid/support/v7/widget/j;)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method dqY()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dqZ(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_0

    invoke-virtual {p2, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->doz(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    return-void
.end method

.method public drA(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drT()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->dqu()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public drB(I)Landroid/view/View;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public drC(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-nez v1, :cond_1

    :cond_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v0

    :cond_2
    return v0
.end method

.method public drD(Landroid/view/View;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/a;->dsf(Landroid/view/View;I)V

    return-void
.end method

.method public drE(Landroid/view/View;II)V
    .locals 7

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->doz(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    add-int/2addr v2, p2

    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v3

    add-int/2addr v1, p3

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drZ()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drV()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dqU()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsS()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v5, v6

    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->width:I

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v6

    invoke-static {v3, v4, v2, v5, v6}, Landroid/support/v7/widget/a;->dsP(IIIIZ)I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dre()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dro()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsg()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsR()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v5, v6

    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v5, v6

    add-int/2addr v1, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->height:I

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v6

    invoke-static {v3, v4, v1, v5, v6}, Landroid/support/v7/widget/a;->dsP(IIIIZ)I

    move-result v1

    invoke-virtual {p0, p1, v2, v1, v0}, Landroid/support/v7/widget/a;->drQ(Landroid/view/View;IILandroid/support/v7/widget/RecyclerView$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2, v1}, Landroid/view/View;->measure(II)V

    :cond_0
    return-void
.end method

.method public drF(I)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/G;->dvS(I)V

    :cond_0
    return-void
.end method

.method public drG(I)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drc()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v4

    if-ne v4, p1, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    iget-object v4, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v4}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    :cond_2
    return-object v2

    :cond_3
    return-object v5
.end method

.method public drH(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/widget/a;->dZd:Z

    return-void
.end method

.method public drI(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/j;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drp(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public drJ(Landroid/view/View;)I
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duw()I

    move-result v0

    return v0
.end method

.method public drK(Landroid/view/View;)I
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    return v0
.end method

.method public drM(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    invoke-virtual {p0, p1, p3, p4}, Landroid/support/v7/widget/a;->drA(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method drN(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 6

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/a;->Hi(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public drO(Landroid/view/View;)I
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method drP(Landroid/view/View;Landroid/support/v4/view/a/a;)V
    .locals 2

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/G;->dvL(Landroid/view/View;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/a;->Hh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/View;Landroid/support/v4/view/a/a;)V

    :cond_0
    return-void
.end method

.method drQ(Landroid/view/View;IILandroid/support/v7/widget/RecyclerView$LayoutParams;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/a;->dZl:Z

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    iget v1, p4, Landroid/support/v7/widget/RecyclerView$LayoutParams;->width:I

    invoke-static {v0, p2, v1}, Landroid/support/v7/widget/a;->drL(III)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    iget v1, p4, Landroid/support/v7/widget/RecyclerView$LayoutParams;->height:I

    invoke-static {v0, p3, v1}, Landroid/support/v7/widget/a;->drL(III)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public drR()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public drS(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v4/view/a/a;)V
    .locals 4

    const/4 v2, -0x1

    const/4 v1, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, 0x2000

    invoke-virtual {p3, v0}, Landroid/support/v4/view/a/a;->dNf(I)V

    invoke-virtual {p3, v1}, Landroid/support/v4/view/a/a;->dMX(Z)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/16 v0, 0x1000

    invoke-virtual {p3, v0}, Landroid/support/v4/view/a/a;->dNf(I)V

    invoke-virtual {p3, v1}, Landroid/support/v4/view/a/a;->dMX(Z)V

    :cond_3
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/a;->drC(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v0

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/a;->dsa(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v1

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/a;->drd(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Z

    move-result v2

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/a;->dtb(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/view/a/d;->dNF(IIZI)Landroid/support/v4/view/a/d;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/support/v4/view/a/a;->dMH(Ljava/lang/Object;)V

    return-void
.end method

.method public drT()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->isRunning()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method drU(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iput-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    iput v1, p0, Landroid/support/v7/widget/a;->dZn:I

    iput v1, p0, Landroid/support/v7/widget/a;->dZe:I

    :goto_0
    iput v2, p0, Landroid/support/v7/widget/a;->dZk:I

    iput v2, p0, Landroid/support/v7/widget/a;->dZm:I

    return-void

    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    iput-object v0, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a;->dZn:I

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a;->dZe:I

    goto :goto_0
.end method

.method public drV()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/a;->dZk:I

    return v0
.end method

.method public drW(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;ZZ)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/a;->drj(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;Z)[I

    move-result-object v0

    aget v1, v0, v3

    aget v0, v0, v4

    if-eqz p5, :cond_0

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/a;->dte(Landroid/support/v7/widget/RecyclerView;II)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    if-nez v1, :cond_1

    if-eqz v0, :cond_3

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p1, v1, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    :goto_0
    return v4

    :cond_2
    invoke-virtual {p1, v1, v0}, Landroid/support/v7/widget/RecyclerView;->dqO(II)V

    goto :goto_0

    :cond_3
    return v3
.end method

.method public drX(Landroid/support/v7/widget/e;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public drY(ILandroid/support/v7/widget/j;)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drF(I)V

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/j;->dtZ(Landroid/view/View;)V

    return-void
.end method

.method public drZ()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/a;->dZn:I

    return v0
.end method

.method dra()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->stop()V

    :cond_0
    return-void
.end method

.method public drb(Landroid/view/View;)I
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    return v0
.end method

.method public drc()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public drd(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dre()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/a;->dZe:I

    return v0
.end method

.method public drf(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1

    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public drg(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drK(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public drh(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    return-void
.end method

.method public dri(Landroid/view/View;)I
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    return v0
.end method

.method public drk(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drn(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public drl(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->doY(I)V

    :cond_0
    return-void
.end method

.method public drm(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->doZ(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public drn(Landroid/view/View;)I
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0
.end method

.method public dro()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/a;->dZm:I

    return v0
.end method

.method public drp(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    return-void
.end method

.method drq(Landroid/support/v4/view/a/a;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {p0, v0, v1, p1}, Landroid/support/v7/widget/a;->drS(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v4/view/a/a;)V

    return-void
.end method

.method public drr(Landroid/support/v7/widget/e;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public drs(Landroid/view/View;)I
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public drt(Landroid/view/View;ILandroid/support/v7/widget/RecyclerView$LayoutParams;)V
    .locals 2

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/H;->dwl(Landroid/support/v7/widget/p;)V

    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v0

    invoke-virtual {v1, p1, p2, p3, v0}, Landroid/support/v7/widget/G;->dvV(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    return-void

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/H;->dwn(Landroid/support/v7/widget/p;)V

    goto :goto_0
.end method

.method public dru(Landroid/support/v7/widget/RecyclerView;III)V
    .locals 0

    return-void
.end method

.method public drv(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drs(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method drx(Landroid/support/v7/widget/j;)V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/support/v7/widget/j;->dtN()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/j;->dtR(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v3, v5}, Landroid/support/v7/widget/p;->setIsRecyclable(Z)V

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isTmpDetached()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v2, v5}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    :cond_1
    iget-object v4, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v4, :cond_2

    iget-object v4, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/u;->duT(Landroid/support/v7/widget/p;)V

    :cond_2
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/p;->setIsRecyclable(Z)V

    invoke-virtual {p1, v2}, Landroid/support/v7/widget/j;->duq(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/support/v7/widget/j;->dum()V

    if-lez v1, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    :cond_4
    return-void
.end method

.method drz(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/j;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/a;->dZi:Z

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/a;->drI(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/j;)V

    return-void
.end method

.method public dsA(I)V
    .locals 0

    return-void
.end method

.method public dsB(ILandroid/support/v7/widget/M;)V
    .locals 0

    return-void
.end method

.method public dsC(Landroid/view/View;Landroid/support/v7/widget/j;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->removeView(Landroid/view/View;)V

    invoke-virtual {p2, p1}, Landroid/support/v7/widget/j;->dtZ(Landroid/view/View;)V

    return-void
.end method

.method public dsD(Landroid/support/v7/widget/e;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dsE()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dsF(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    return-void
.end method

.method public dsG(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drO(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method dsI()Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drc()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gez v4, :cond_0

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-gez v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public dsJ(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dsK(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3

    const/4 v2, -0x1

    const/4 v0, 0x1

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v0

    :cond_2
    invoke-virtual {p3, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    :cond_3
    return-void
.end method

.method public dsL(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/a;->drW(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;ZZ)Z

    move-result v0

    return v0
.end method

.method public dsM(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dsN(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    return-void
.end method

.method public dsO(Landroid/support/v7/widget/e;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method dsQ(II)V
    .locals 2

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a;->dZn:I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a;->dZk:I

    iget v0, p0, Landroid/support/v7/widget/a;->dZk:I

    if-nez v0, :cond_0

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->dXI:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iput v1, p0, Landroid/support/v7/widget/a;->dZn:I

    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a;->dZe:I

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a;->dZm:I

    iget v0, p0, Landroid/support/v7/widget/a;->dZm:I

    if-nez v0, :cond_1

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->dXI:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iput v1, p0, Landroid/support/v7/widget/a;->dZe:I

    :cond_1
    return-void
.end method

.method public dsR()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dsS()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dsT(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->dqF(I)V

    :cond_0
    return-void
.end method

.method public dsU(Landroid/support/v7/widget/RecyclerView;Ljava/util/ArrayList;II)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method dsV(ILandroid/os/Bundle;)Z
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/a;->dsu(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public dsW(IILandroid/support/v7/widget/e;Landroid/support/v7/widget/M;)V
    .locals 0

    return-void
.end method

.method public dsX()Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_0

    return-object v2

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/G;->dvL(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    return-object v2

    :cond_2
    return-object v0
.end method

.method public dsY(Landroid/support/v7/widget/RecyclerView;IILjava/lang/Object;)V
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/a;->dsF(Landroid/support/v7/widget/RecyclerView;II)V

    return-void
.end method

.method public dsZ(Landroid/support/v7/widget/e;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dsa(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-nez v1, :cond_1

    :cond_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v0

    :cond_2
    return v0
.end method

.method public dsb(Landroid/support/v7/widget/b;Landroid/support/v7/widget/b;)V
    .locals 0

    return-void
.end method

.method public dsc(II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->dpA(Landroid/support/v7/widget/RecyclerView;II)V

    return-void
.end method

.method public dsd(Landroid/view/View;I)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v7/widget/a;->drt(Landroid/view/View;ILandroid/support/v7/widget/RecyclerView$LayoutParams;)V

    return-void
.end method

.method public dse(Landroid/view/View;ZLandroid/graphics/Rect;)V
    .locals 6

    const/4 v2, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v4

    invoke-virtual {p3, v1, v2, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYW:Landroid/graphics/RectF;

    invoke-virtual {v1, p3}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget v0, v1, Landroid/graphics/RectF;->left:F

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    iget v2, v1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, v1, Landroid/graphics/RectF;->right:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v1, v4

    invoke-virtual {p3, v0, v2, v3, v1}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p3, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public dsf(Landroid/view/View;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/a;->dqV(Landroid/view/View;IZ)V

    return-void
.end method

.method public dsg()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dsh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p3, p4}, Landroid/support/v7/widget/RecyclerView;->dpv(II)V

    return-void
.end method

.method public dsi(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)V
    .locals 2

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "You must override onLayoutChildren(Recycler recycler, State state) "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public dsj(Landroid/view/View;I)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public dsk(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    return-void
.end method

.method dsm(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/a;->dsQ(II)V

    return-void
.end method

.method public dsn(I)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/a;->dsH(ILandroid/view/View;)V

    return-void
.end method

.method dso(II)V
    .locals 8

    const v2, 0x7fffffff

    const/4 v0, 0x0

    const/high16 v3, -0x80000000

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drc()I

    move-result v5

    if-nez v5, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->dpv(II)V

    return-void

    :cond_0
    move v4, v0

    move v1, v3

    move v0, v2

    :goto_0
    if-ge v4, v5, :cond_5

    invoke-virtual {p0, v4}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v6

    iget-object v7, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v7, v7, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {p0, v6, v7}, Landroid/support/v7/widget/a;->dsr(Landroid/view/View;Landroid/graphics/Rect;)V

    iget v6, v7, Landroid/graphics/Rect;->left:I

    if-ge v6, v0, :cond_1

    iget v0, v7, Landroid/graphics/Rect;->left:I

    :cond_1
    iget v6, v7, Landroid/graphics/Rect;->right:I

    if-le v6, v1, :cond_2

    iget v1, v7, Landroid/graphics/Rect;->right:I

    :cond_2
    iget v6, v7, Landroid/graphics/Rect;->top:I

    if-ge v6, v2, :cond_3

    iget v2, v7, Landroid/graphics/Rect;->top:I

    :cond_3
    iget v6, v7, Landroid/graphics/Rect;->bottom:I

    if-le v6, v3, :cond_4

    iget v3, v7, Landroid/graphics/Rect;->bottom:I

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    iget-object v4, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, p1, p2}, Landroid/support/v7/widget/a;->dta(Landroid/graphics/Rect;II)V

    return-void
.end method

.method public dsp()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/a;->dZs:Z

    return-void
.end method

.method public dsq(Landroid/view/View;ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public dsr(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 0

    invoke-static {p1, p2}, Landroid/support/v7/widget/RecyclerView;->dqC(Landroid/view/View;Landroid/graphics/Rect;)V

    return-void
.end method

.method public dss()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dst(Landroid/support/v7/widget/j;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drc()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/a;->drw(Landroid/support/v7/widget/j;ILandroid/view/View;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public dsu(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;ILandroid/os/Bundle;)Z
    .locals 5

    const/4 v3, -0x1

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_0

    return v1

    :cond_0
    sparse-switch p3, :sswitch_data_0

    move v2, v1

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    if-nez v2, :cond_1

    return v1

    :sswitch_0
    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dre()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsg()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsR()I

    move-result v2

    sub-int/2addr v0, v2

    neg-int v0, v0

    :goto_1
    iget-object v2, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drZ()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dqU()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsS()I

    move-result v3

    sub-int/2addr v2, v3

    neg-int v2, v2

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dre()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsg()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsR()I

    move-result v2

    sub-int/2addr v0, v2

    :goto_2
    iget-object v2, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->drZ()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dqU()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsS()I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    return v4

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_1
        0x2000 -> :sswitch_0
    .end sparse-switch
.end method

.method public dsw()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dsx(Landroid/support/v7/widget/e;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dsy(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    return-void
.end method

.method dsz(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/a;->dZi:Z

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->dsk(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public dta(Landroid/graphics/Rect;II)V
    .locals 3

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dqU()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsS()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsg()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->dsR()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->getMinimumWidth()I

    move-result v2

    invoke-static {p2, v0, v2}, Landroid/support/v7/widget/a;->dry(III)I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/a;->getMinimumHeight()I

    move-result v2

    invoke-static {p3, v1, v2}, Landroid/support/v7/widget/a;->dry(III)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/a;->dsc(II)V

    return-void
.end method

.method public dtb(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dtc(II)V
    .locals 3

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot move a child from non-existing index:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a;->dsn(I)V

    invoke-virtual {p0, v0, p2}, Landroid/support/v7/widget/a;->dsd(Landroid/view/View;I)V

    return-void
.end method

.method public dtd(I)V
    .locals 0

    return-void
.end method

.method public dtf(Landroid/support/v7/widget/RecyclerView$LayoutParams;)Z
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract generateDefaultLayoutParams()Landroid/support/v7/widget/RecyclerView$LayoutParams;
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1

    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    check-cast p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/support/v7/widget/RecyclerView$LayoutParams;)V

    return-object v0

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    return-object v0

    :cond_1
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getBaseline()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getItemCount()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/b;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMinimumHeight()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPS(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public getMinimumWidth()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPM(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v1, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {p0, v0, v1, p1}, Landroid/support/v7/widget/a;->dsK(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 0

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZo:Landroid/support/v7/widget/G;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/G;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public requestLayout()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    :cond_0
    return-void
.end method
