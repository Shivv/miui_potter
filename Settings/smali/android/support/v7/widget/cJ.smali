.class Landroid/support/v7/widget/cJ;
.super Ljava/lang/Object;
.source "AppCompatTextHelper.java"


# instance fields
.field private epC:Landroid/support/v7/widget/cw;

.field private epD:Landroid/support/v7/widget/cw;

.field private epE:I

.field private epF:Landroid/graphics/Typeface;

.field private epG:Landroid/support/v7/widget/cw;

.field private final epH:Landroid/support/v7/widget/aM;

.field private epI:Landroid/support/v7/widget/cw;

.field final epJ:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/cJ;->epE:I

    iput-object p1, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    new-instance v0, Landroid/support/v7/widget/aM;

    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/aM;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    return-void
.end method

.method protected static dHS(Landroid/content/Context;Landroid/support/v7/widget/bF;I)Landroid/support/v7/widget/cw;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1, p0, p2}, Landroid/support/v7/widget/bF;->dDM(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/support/v7/widget/cw;

    invoke-direct {v1}, Landroid/support/v7/widget/cw;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/support/v7/widget/cw;->eoN:Z

    iput-object v0, v1, Landroid/support/v7/widget/cw;->eoO:Landroid/content/res/ColorStateList;

    return-object v1

    :cond_0
    return-object v1
.end method

.method private dHT(Landroid/content/Context;Landroid/support/v7/widget/bS;)V
    .locals 3

    const/4 v2, 0x0

    sget v0, Landroid/support/v7/b/j;->dVv:I

    iget v1, p0, Landroid/support/v7/widget/cJ;->epE:I

    invoke-virtual {p2, v0, v1}, Landroid/support/v7/widget/bS;->dFr(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/cJ;->epE:I

    sget v0, Landroid/support/v7/b/j;->dVq:I

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/support/v7/b/j;->dVx:I

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    iput-object v2, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    sget v0, Landroid/support/v7/b/j;->dVq:I

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Landroid/support/v7/b/j;->dVq:I

    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->isRestricted()Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_0
    iget v1, p0, Landroid/support/v7/widget/cJ;->epE:I

    iget-object v2, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {p2, v0, v1, v2}, Landroid/support/v7/widget/bS;->dFD(IILandroid/widget/TextView;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    if-nez v1, :cond_2

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/bS;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/cJ;->epE:I

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    :cond_2
    return-void

    :cond_3
    sget v0, Landroid/support/v7/b/j;->dVx:I

    goto :goto_0

    :cond_4
    sget v0, Landroid/support/v7/b/j;->dVw:I

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Landroid/support/v7/b/j;->dVw:I

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/support/v7/widget/bS;->dFr(II)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_5
    :goto_2
    return-void

    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    goto :goto_2

    :pswitch_1
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    goto :goto_2

    :pswitch_2
    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    iput-object v0, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static dHU(Landroid/widget/TextView;)Landroid/support/v7/widget/cJ;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v7/widget/cz;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cz;-><init>(Landroid/widget/TextView;)V

    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v7/widget/cJ;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cJ;-><init>(Landroid/widget/TextView;)V

    return-object v0
.end method

.method private dHV(IF)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/aM;->dBn(IF)V

    return-void
.end method


# virtual methods
.method dHC(Landroid/util/AttributeSet;I)V
    .locals 12

    const/16 v11, 0x17

    const/4 v1, 0x1

    const/4 v9, -0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Landroid/support/v7/widget/bF;->get()Landroid/support/v7/widget/bF;

    move-result-object v0

    sget-object v3, Landroid/support/v7/b/j;->dRN:[I

    invoke-static {v7, p1, v3, p2, v2}, Landroid/support/v7/widget/bS;->dFw(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/bS;

    move-result-object v3

    sget v4, Landroid/support/v7/b/j;->dRU:I

    invoke-virtual {v3, v4, v9}, Landroid/support/v7/widget/bS;->dFu(II)I

    move-result v4

    sget v6, Landroid/support/v7/b/j;->dRQ:I

    invoke-virtual {v3, v6}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v6

    if-eqz v6, :cond_0

    sget v6, Landroid/support/v7/b/j;->dRQ:I

    invoke-virtual {v3, v6, v2}, Landroid/support/v7/widget/bS;->dFu(II)I

    move-result v6

    invoke-static {v7, v0, v6}, Landroid/support/v7/widget/cJ;->dHS(Landroid/content/Context;Landroid/support/v7/widget/bF;I)Landroid/support/v7/widget/cw;

    move-result-object v6

    iput-object v6, p0, Landroid/support/v7/widget/cJ;->epG:Landroid/support/v7/widget/cw;

    :cond_0
    sget v6, Landroid/support/v7/b/j;->dRT:I

    invoke-virtual {v3, v6}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v6

    if-eqz v6, :cond_1

    sget v6, Landroid/support/v7/b/j;->dRT:I

    invoke-virtual {v3, v6, v2}, Landroid/support/v7/widget/bS;->dFu(II)I

    move-result v6

    invoke-static {v7, v0, v6}, Landroid/support/v7/widget/cJ;->dHS(Landroid/content/Context;Landroid/support/v7/widget/bF;I)Landroid/support/v7/widget/cw;

    move-result-object v6

    iput-object v6, p0, Landroid/support/v7/widget/cJ;->epI:Landroid/support/v7/widget/cw;

    :cond_1
    sget v6, Landroid/support/v7/b/j;->dRR:I

    invoke-virtual {v3, v6}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v6

    if-eqz v6, :cond_2

    sget v6, Landroid/support/v7/b/j;->dRR:I

    invoke-virtual {v3, v6, v2}, Landroid/support/v7/widget/bS;->dFu(II)I

    move-result v6

    invoke-static {v7, v0, v6}, Landroid/support/v7/widget/cJ;->dHS(Landroid/content/Context;Landroid/support/v7/widget/bF;I)Landroid/support/v7/widget/cw;

    move-result-object v6

    iput-object v6, p0, Landroid/support/v7/widget/cJ;->epC:Landroid/support/v7/widget/cw;

    :cond_2
    sget v6, Landroid/support/v7/b/j;->dRO:I

    invoke-virtual {v3, v6}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v6

    if-eqz v6, :cond_3

    sget v6, Landroid/support/v7/b/j;->dRO:I

    invoke-virtual {v3, v6, v2}, Landroid/support/v7/widget/bS;->dFu(II)I

    move-result v6

    invoke-static {v7, v0, v6}, Landroid/support/v7/widget/cJ;->dHS(Landroid/content/Context;Landroid/support/v7/widget/bF;I)Landroid/support/v7/widget/cw;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cJ;->epD:Landroid/support/v7/widget/cw;

    :cond_3
    invoke-virtual {v3}, Landroid/support/v7/widget/bS;->dFB()V

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v0

    instance-of v8, v0, Landroid/text/method/PasswordTransformationMethod;

    if-eq v4, v9, :cond_15

    sget-object v0, Landroid/support/v7/b/j;->dVp:[I

    invoke-static {v7, v4, v0}, Landroid/support/v7/widget/bS;->dFs(Landroid/content/Context;I[I)Landroid/support/v7/widget/bS;

    move-result-object v9

    if-nez v8, :cond_f

    sget v0, Landroid/support/v7/b/j;->dVy:I

    invoke-virtual {v9, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-eqz v0, :cond_14

    sget v0, Landroid/support/v7/b/j;->dVy:I

    invoke-virtual {v9, v0, v2}, Landroid/support/v7/widget/bS;->dFt(IZ)Z

    move-result v0

    move v3, v0

    move v0, v1

    :goto_0
    invoke-direct {p0, v7, v9}, Landroid/support/v7/widget/cJ;->dHT(Landroid/content/Context;Landroid/support/v7/widget/bS;)V

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v4, v11, :cond_13

    sget v4, Landroid/support/v7/b/j;->dVr:I

    invoke-virtual {v9, v4}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v4

    if-eqz v4, :cond_12

    sget v4, Landroid/support/v7/b/j;->dVr:I

    invoke-virtual {v9, v4}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    :goto_1
    sget v6, Landroid/support/v7/b/j;->dVs:I

    invoke-virtual {v9, v6}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v6

    if-eqz v6, :cond_11

    sget v6, Landroid/support/v7/b/j;->dVs:I

    invoke-virtual {v9, v6}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    :goto_2
    sget v10, Landroid/support/v7/b/j;->dVt:I

    invoke-virtual {v9, v10}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v10

    if-eqz v10, :cond_4

    sget v5, Landroid/support/v7/b/j;->dVt:I

    invoke-virtual {v9, v5}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    :cond_4
    :goto_3
    invoke-virtual {v9}, Landroid/support/v7/widget/bS;->dFB()V

    :goto_4
    sget-object v9, Landroid/support/v7/b/j;->dVp:[I

    invoke-static {v7, p1, v9, p2, v2}, Landroid/support/v7/widget/bS;->dFw(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/bS;

    move-result-object v9

    if-nez v8, :cond_5

    sget v10, Landroid/support/v7/b/j;->dVy:I

    invoke-virtual {v9, v10}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v10

    if-eqz v10, :cond_5

    sget v0, Landroid/support/v7/b/j;->dVy:I

    invoke-virtual {v9, v0, v2}, Landroid/support/v7/widget/bS;->dFt(IZ)Z

    move-result v3

    move v0, v1

    :cond_5
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v11, :cond_8

    sget v1, Landroid/support/v7/b/j;->dVr:I

    invoke-virtual {v9, v1}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v1

    if-eqz v1, :cond_6

    sget v1, Landroid/support/v7/b/j;->dVr:I

    invoke-virtual {v9, v1}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    :cond_6
    sget v1, Landroid/support/v7/b/j;->dVs:I

    invoke-virtual {v9, v1}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v1

    if-eqz v1, :cond_7

    sget v1, Landroid/support/v7/b/j;->dVs:I

    invoke-virtual {v9, v1}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    :cond_7
    sget v1, Landroid/support/v7/b/j;->dVt:I

    invoke-virtual {v9, v1}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v1

    if-eqz v1, :cond_8

    sget v1, Landroid/support/v7/b/j;->dVt:I

    invoke-virtual {v9, v1}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    :cond_8
    invoke-direct {p0, v7, v9}, Landroid/support/v7/widget/cJ;->dHT(Landroid/content/Context;Landroid/support/v7/widget/bS;)V

    invoke-virtual {v9}, Landroid/support/v7/widget/bS;->dFB()V

    if-eqz v4, :cond_9

    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_9
    if-eqz v6, :cond_a

    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    :cond_a
    if-eqz v5, :cond_b

    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    :cond_b
    if-nez v8, :cond_c

    if-eqz v0, :cond_c

    invoke-virtual {p0, v3}, Landroid/support/v7/widget/cJ;->dHQ(Z)V

    :cond_c
    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    iget v3, p0, Landroid/support/v7/widget/cJ;->epE:I

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_d
    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/aM;->dBm(Landroid/util/AttributeSet;I)V

    sget-boolean v0, Landroid/support/v4/widget/z;->eDr:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0}, Landroid/support/v7/widget/aM;->getAutoSizeTextType()I

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0}, Landroid/support/v7/widget/aM;->getAutoSizeTextAvailableSizes()[I

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_e

    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getAutoSizeStepGranularity()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v1}, Landroid/support/v7/widget/aM;->getAutoSizeMinTextSize()I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v3}, Landroid/support/v7/widget/aM;->getAutoSizeMaxTextSize()I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v4}, Landroid/support/v7/widget/aM;->getAutoSizeStepGranularity()I

    move-result v4

    invoke-virtual {v0, v1, v3, v4, v2}, Landroid/widget/TextView;->setAutoSizeTextTypeUniformWithConfiguration(IIII)V

    :cond_e
    :goto_5
    return-void

    :cond_f
    move v0, v2

    move v3, v2

    goto/16 :goto_0

    :cond_10
    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2}, Landroid/widget/TextView;->setAutoSizeTextTypeUniformWithPresetSizes([II)V

    goto :goto_5

    :cond_11
    move-object v6, v5

    goto/16 :goto_2

    :cond_12
    move-object v4, v5

    goto/16 :goto_1

    :cond_13
    move-object v6, v5

    move-object v4, v5

    goto/16 :goto_3

    :cond_14
    move v0, v2

    move v3, v2

    goto/16 :goto_0

    :cond_15
    move-object v6, v5

    move-object v4, v5

    move v0, v2

    move v3, v2

    goto/16 :goto_4
.end method

.method dHD()V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epG:Landroid/support/v7/widget/cw;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epI:Landroid/support/v7/widget/cw;

    if-eqz v0, :cond_2

    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    iget-object v2, p0, Landroid/support/v7/widget/cJ;->epG:Landroid/support/v7/widget/cw;

    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/cJ;->dHP(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;)V

    const/4 v1, 0x1

    aget-object v1, v0, v1

    iget-object v2, p0, Landroid/support/v7/widget/cJ;->epI:Landroid/support/v7/widget/cw;

    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/cJ;->dHP(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;)V

    const/4 v1, 0x2

    aget-object v1, v0, v1

    iget-object v2, p0, Landroid/support/v7/widget/cJ;->epC:Landroid/support/v7/widget/cw;

    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/cJ;->dHP(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;)V

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epD:Landroid/support/v7/widget/cw;

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/cJ;->dHP(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epC:Landroid/support/v7/widget/cw;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epD:Landroid/support/v7/widget/cw;

    if-eqz v0, :cond_1

    goto :goto_0
.end method

.method dHO()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0}, Landroid/support/v7/widget/aM;->dBf()V

    return-void
.end method

.method final dHP(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;)V
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getDrawableState()[I

    move-result-object v0

    invoke-static {p1, p2, v0}, Landroid/support/v7/widget/bF;->dDJ(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;[I)V

    :cond_0
    return-void
.end method

.method dHQ(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setAllCaps(Z)V

    return-void
.end method

.method dHR(Landroid/content/Context;I)V
    .locals 3

    sget-object v0, Landroid/support/v7/b/j;->dVp:[I

    invoke-static {p1, p2, v0}, Landroid/support/v7/widget/bS;->dFs(Landroid/content/Context;I[I)Landroid/support/v7/widget/bS;

    move-result-object v0

    sget v1, Landroid/support/v7/b/j;->dVy:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Landroid/support/v7/b/j;->dVy:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bS;->dFt(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/cJ;->dHQ(Z)V

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v1, v2, :cond_1

    sget v1, Landroid/support/v7/b/j;->dVr:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Landroid/support/v7/b/j;->dVr:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_1
    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/cJ;->dHT(Landroid/content/Context;Landroid/support/v7/widget/bS;)V

    invoke-virtual {v0}, Landroid/support/v7/widget/bS;->dFB()V

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epJ:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/widget/cJ;->epF:Landroid/graphics/Typeface;

    iget v2, p0, Landroid/support/v7/widget/cJ;->epE:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_2
    return-void
.end method

.method dHW()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0}, Landroid/support/v7/widget/aM;->dBr()Z

    move-result v0

    return v0
.end method

.method getAutoSizeMaxTextSize()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0}, Landroid/support/v7/widget/aM;->getAutoSizeMaxTextSize()I

    move-result v0

    return v0
.end method

.method getAutoSizeMinTextSize()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0}, Landroid/support/v7/widget/aM;->getAutoSizeMinTextSize()I

    move-result v0

    return v0
.end method

.method getAutoSizeStepGranularity()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0}, Landroid/support/v7/widget/aM;->getAutoSizeStepGranularity()I

    move-result v0

    return v0
.end method

.method getAutoSizeTextAvailableSizes()[I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0}, Landroid/support/v7/widget/aM;->getAutoSizeTextAvailableSizes()[I

    move-result-object v0

    return-object v0
.end method

.method getAutoSizeTextType()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0}, Landroid/support/v7/widget/aM;->getAutoSizeTextType()I

    move-result v0

    return v0
.end method

.method onLayout(ZIIII)V
    .locals 1

    sget-boolean v0, Landroid/support/v4/widget/z;->eDr:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/cJ;->dHO()V

    :cond_0
    return-void
.end method

.method setAutoSizeTextTypeUniformWithConfiguration(IIII)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v7/widget/aM;->setAutoSizeTextTypeUniformWithConfiguration(IIII)V

    return-void
.end method

.method setAutoSizeTextTypeUniformWithPresetSizes([II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/aM;->setAutoSizeTextTypeUniformWithPresetSizes([II)V

    return-void
.end method

.method setAutoSizeTextTypeWithDefaults(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cJ;->epH:Landroid/support/v7/widget/aM;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aM;->setAutoSizeTextTypeWithDefaults(I)V

    return-void
.end method

.method setTextSize(IF)V
    .locals 1

    sget-boolean v0, Landroid/support/v4/widget/z;->eDr:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/cJ;->dHW()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/cJ;->dHV(IF)V

    :cond_0
    return-void
.end method
