.class Landroid/support/v7/widget/aY;
.super Landroid/support/v7/widget/AppCompatImageView;
.source "ActionMenuPresenter.java"

# interfaces
.implements Landroid/support/v7/widget/aH;


# instance fields
.field private final egL:[F

.field final synthetic egM:Landroid/support/v7/widget/ca;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/ca;Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    iput-object p1, p0, Landroid/support/v7/widget/aY;->egM:Landroid/support/v7/widget/ca;

    sget v0, Landroid/support/v7/b/a;->dOj:I

    const/4 v1, 0x0

    invoke-direct {p0, p2, v1, v0}, Landroid/support/v7/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Landroid/support/v7/widget/aY;->egL:[F

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/aY;->setClickable(Z)V

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/aY;->setFocusable(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aY;->setVisibility(I)V

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/aY;->setEnabled(Z)V

    invoke-virtual {p0}, Landroid/support/v7/widget/aY;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/support/v7/widget/az;->dzY(Landroid/view/View;Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/support/v7/widget/ay;

    invoke-direct {v0, p0, p0}, Landroid/support/v7/widget/ay;-><init>(Landroid/support/v7/widget/aY;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aY;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method


# virtual methods
.method public dAm()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dAn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public performClick()Z
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/support/v7/widget/AppCompatImageView;->performClick()Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aY;->playSoundEffect(I)V

    iget-object v0, p0, Landroid/support/v7/widget/aY;->egM:Landroid/support/v7/widget/ca;

    invoke-virtual {v0}, Landroid/support/v7/widget/ca;->dGf()Z

    return v1
.end method

.method protected setFrame(IIII)Z
    .locals 8

    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/AppCompatImageView;->setFrame(IIII)Z

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/aY;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/aY;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/aY;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/aY;->getHeight()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {p0}, Landroid/support/v7/widget/aY;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v7/widget/aY;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Landroid/support/v7/widget/aY;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Landroid/support/v7/widget/aY;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    add-int/2addr v1, v5

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v3, v6

    div-int/lit8 v3, v3, 0x2

    sub-int v5, v1, v4

    sub-int v6, v3, v4

    add-int/2addr v1, v4

    add-int/2addr v3, v4

    invoke-static {v2, v5, v6, v1, v3}, Landroid/support/v4/c/a/g;->elP(Landroid/graphics/drawable/Drawable;IIII)V

    :cond_0
    return v0
.end method
