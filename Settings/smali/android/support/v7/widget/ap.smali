.class public abstract Landroid/support/v7/widget/ap;
.super Ljava/lang/Object;
.source "OrientationHelper.java"


# instance fields
.field protected final edi:Landroid/support/v7/widget/a;

.field final edj:Landroid/graphics/Rect;

.field private edk:I


# direct methods
.method private constructor <init>(Landroid/support/v7/widget/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/ap;->edk:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ap;->edj:Landroid/graphics/Rect;

    iput-object p1, p0, Landroid/support/v7/widget/ap;->edi:Landroid/support/v7/widget/a;

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/widget/a;Landroid/support/v7/widget/ap;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/ap;-><init>(Landroid/support/v7/widget/a;)V

    return-void
.end method

.method public static dzd(Landroid/support/v7/widget/a;)Landroid/support/v7/widget/ap;
    .locals 1

    new-instance v0, Landroid/support/v7/widget/cU;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cU;-><init>(Landroid/support/v7/widget/a;)V

    return-object v0
.end method

.method public static dzm(Landroid/support/v7/widget/a;)Landroid/support/v7/widget/ap;
    .locals 1

    new-instance v0, Landroid/support/v7/widget/cY;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cY;-><init>(Landroid/support/v7/widget/a;)V

    return-object v0
.end method

.method public static dzo(Landroid/support/v7/widget/a;I)Landroid/support/v7/widget/ap;
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "invalid orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-static {p0}, Landroid/support/v7/widget/ap;->dzm(Landroid/support/v7/widget/a;)Landroid/support/v7/widget/ap;

    move-result-object v0

    return-object v0

    :pswitch_1
    invoke-static {p0}, Landroid/support/v7/widget/ap;->dzd(Landroid/support/v7/widget/a;)Landroid/support/v7/widget/ap;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract dzc(Landroid/view/View;)I
.end method

.method public abstract dze()I
.end method

.method public abstract dzf(Landroid/view/View;)I
.end method

.method public abstract dzg(Landroid/view/View;)I
.end method

.method public abstract dzh()I
.end method

.method public abstract dzi(Landroid/view/View;)I
.end method

.method public abstract dzj()I
.end method

.method public abstract dzk()I
.end method

.method public abstract dzl(Landroid/view/View;)I
.end method

.method public abstract dzn()I
.end method

.method public dzp()I
    .locals 2

    iget v0, p0, Landroid/support/v7/widget/ap;->edk:I

    const/high16 v1, -0x80000000

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/ap;->dzh()I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/ap;->edk:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public abstract dzq(Landroid/view/View;)I
.end method

.method public abstract dzr()I
.end method

.method public abstract dzs(I)V
.end method

.method public dzt()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/ap;->dzh()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/ap;->edk:I

    return-void
.end method
