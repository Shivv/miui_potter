.class final Landroid/support/v7/widget/a/h;
.super Ljava/lang/Object;
.source "ItemTouchHelper.java"

# interfaces
.implements Landroid/support/v7/widget/l;


# instance fields
.field final synthetic dXr:Landroid/support/v7/widget/a/b;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/a/b;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dop(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, -0x1

    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v2, v2, Landroid/support/v7/widget/a/b;->dWP:Landroid/support/v4/view/y;

    invoke-virtual {v2, p2}, Landroid/support/v4/view/y;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    iput v3, v2, Landroid/support/v7/widget/a/b;->dWH:I

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, v2, Landroid/support/v7/widget/a/b;->dWG:F

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, v2, Landroid/support/v7/widget/a/b;->dWF:F

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {v2}, Landroid/support/v7/widget/a/b;->dnx()V

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v2, v2, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-nez v2, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {v2, p2}, Landroid/support/v7/widget/a/b;->dns(Landroid/view/MotionEvent;)Landroid/support/v7/widget/a/f;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v4, v3, Landroid/support/v7/widget/a/b;->dWG:F

    iget v5, v2, Landroid/support/v7/widget/a/f;->dXf:F

    sub-float/2addr v4, v5

    iput v4, v3, Landroid/support/v7/widget/a/b;->dWG:F

    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v4, v3, Landroid/support/v7/widget/a/b;->dWF:F

    iget v5, v2, Landroid/support/v7/widget/a/f;->dXh:F

    sub-float/2addr v4, v5

    iput v4, v3, Landroid/support/v7/widget/a/b;->dWF:F

    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v4, v2, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    invoke-virtual {v3, v4, v0}, Landroid/support/v7/widget/a/b;->dnr(Landroid/support/v7/widget/p;Z)I

    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v3, v3, Landroid/support/v7/widget/a/b;->dWE:Ljava/util/List;

    iget-object v4, v2, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iget-object v4, v4, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-interface {v3, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v3, v3, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v4, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v4, v4, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v2, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    invoke-virtual {v3, v4, v5}, Landroid/support/v7/widget/a/c;->doi(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)V

    :cond_0
    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v4, v2, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iget v2, v2, Landroid/support/v7/widget/a/f;->dXc:I

    invoke-virtual {v3, v4, v2}, Landroid/support/v7/widget/a/b;->dny(Landroid/support/v7/widget/p;I)V

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v3, v3, Landroid/support/v7/widget/a/b;->dWN:I

    invoke-virtual {v2, p2, v3, v1}, Landroid/support/v7/widget/a/b;->dnK(Landroid/view/MotionEvent;II)V

    :cond_1
    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v2, v2, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v2, v2, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_2
    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v2, v2, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-eqz v2, :cond_6

    :goto_1
    return v0

    :cond_3
    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    if-ne v2, v0, :cond_5

    :cond_4
    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iput v5, v2, Landroid/support/v7/widget/a/b;->dWH:I

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {v2, v4, v1}, Landroid/support/v7/widget/a/b;->dny(Landroid/support/v7/widget/p;I)V

    goto :goto_0

    :cond_5
    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v3, v3, Landroid/support/v7/widget/a/b;->dWH:I

    if-eq v3, v5, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v3, v3, Landroid/support/v7/widget/a/b;->dWH:I

    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    if-ltz v3, :cond_1

    iget-object v4, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {v4, v2, p2, v3}, Landroid/support/v7/widget/a/b;->dnC(ILandroid/view/MotionEvent;I)Z

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public doq(Z)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/a/b;->dny(Landroid/support/v7/widget/p;I)V

    return-void
.end method

.method public dor(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)V
    .locals 6

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWP:Landroid/support/v4/view/y;

    invoke-virtual {v1, p2}, Landroid/support/v4/view/y;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v1, v1, Landroid/support/v7/widget/a/b;->dWH:I

    if-ne v1, v5, :cond_1

    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v2, v2, Landroid/support/v7/widget/a/b;->dWH:I

    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    if-ltz v2, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {v3, v1, p2, v2}, Landroid/support/v7/widget/a/b;->dnC(ILandroid/view/MotionEvent;I)Z

    :cond_2
    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v3, v3, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-nez v3, :cond_3

    return-void

    :cond_3
    packed-switch v1, :pswitch_data_0

    :cond_4
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    if-ltz v2, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v1, v1, Landroid/support/v7/widget/a/b;->dWN:I

    invoke-virtual {v0, p2, v1, v2}, Landroid/support/v7/widget/a/b;->dnK(Landroid/view/MotionEvent;II)V

    iget-object v0, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/a/b;->dnn(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWK:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWK:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iget-object v0, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_5

    iget-object v1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    :cond_5
    :pswitch_3
    iget-object v1, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {v1, v4, v0}, Landroid/support/v7/widget/a/b;->dny(Landroid/support/v7/widget/p;I)V

    iget-object v0, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iput v5, v0, Landroid/support/v7/widget/a/b;->dWH:I

    goto :goto_0

    :pswitch_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v3, v3, Landroid/support/v7/widget/a/b;->dWH:I

    if-ne v2, v3, :cond_4

    if-nez v1, :cond_6

    const/4 v0, 0x1

    :cond_6
    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, v2, Landroid/support/v7/widget/a/b;->dWH:I

    iget-object v0, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget-object v2, p0, Landroid/support/v7/widget/a/h;->dXr:Landroid/support/v7/widget/a/b;

    iget v2, v2, Landroid/support/v7/widget/a/b;->dWN:I

    invoke-virtual {v0, p2, v2, v1}, Landroid/support/v7/widget/a/b;->dnK(Landroid/view/MotionEvent;II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
