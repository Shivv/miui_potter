.class final Landroid/support/v7/widget/a/j;
.super Ljava/lang/Object;
.source "ItemTouchHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic dXv:Landroid/support/v7/widget/a/f;

.field final synthetic dXw:Landroid/support/v7/widget/a/b;

.field final synthetic dXx:I


# direct methods
.method constructor <init>(Landroid/support/v7/widget/a/b;Landroid/support/v7/widget/a/f;I)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/a/j;->dXw:Landroid/support/v7/widget/a/b;

    iput-object p2, p0, Landroid/support/v7/widget/a/j;->dXv:Landroid/support/v7/widget/a/f;

    iput p3, p0, Landroid/support/v7/widget/a/j;->dXx:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/j;->dXw:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/a/j;->dXw:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/a/j;->dXv:Landroid/support/v7/widget/a/f;

    iget-boolean v0, v0, Landroid/support/v7/widget/a/f;->dXm:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/a/j;->dXv:Landroid/support/v7/widget/a/f;

    iget-object v0, v0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getAdapterPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/a/j;->dXw:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()Landroid/support/v7/widget/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/u;->duZ(Landroid/support/v7/widget/V;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a/j;->dXw:Landroid/support/v7/widget/a/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/a/b;->dnG()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a/j;->dXw:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v1, p0, Landroid/support/v7/widget/a/j;->dXv:Landroid/support/v7/widget/a/f;

    iget-object v1, v1, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iget v2, p0, Landroid/support/v7/widget/a/j;->dXx:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/a/c;->Bt(Landroid/support/v7/widget/p;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/a/j;->dXw:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
