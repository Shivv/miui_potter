.class final Landroid/support/v7/widget/a/i;
.super Landroid/support/v7/widget/a/f;
.source "ItemTouchHelper.java"


# instance fields
.field final synthetic dXs:Landroid/support/v7/widget/p;

.field final synthetic dXt:Landroid/support/v7/widget/a/b;

.field final synthetic dXu:I


# direct methods
.method constructor <init>(Landroid/support/v7/widget/a/b;Landroid/support/v7/widget/p;IIFFFFILandroid/support/v7/widget/p;)V
    .locals 9

    iput-object p1, p0, Landroid/support/v7/widget/a/i;->dXt:Landroid/support/v7/widget/a/b;

    move/from16 v0, p9

    iput v0, p0, Landroid/support/v7/widget/a/i;->dXu:I

    move-object/from16 v0, p10

    iput-object v0, p0, Landroid/support/v7/widget/a/i;->dXs:Landroid/support/v7/widget/p;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Landroid/support/v7/widget/a/f;-><init>(Landroid/support/v7/widget/p;IIFFFF)V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v7/widget/a/f;->onAnimationEnd(Landroid/animation/Animator;)V

    iget-boolean v0, p0, Landroid/support/v7/widget/a/i;->dXm:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/a/i;->dXu:I

    if-gtz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/a/i;->dXt:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v1, p0, Landroid/support/v7/widget/a/i;->dXt:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Landroid/support/v7/widget/a/i;->dXs:Landroid/support/v7/widget/p;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/a/c;->doi(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/a/i;->dXt:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWy:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/widget/a/i;->dXs:Landroid/support/v7/widget/p;

    iget-object v1, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a/i;->dXt:Landroid/support/v7/widget/a/b;

    iget-object v1, p0, Landroid/support/v7/widget/a/i;->dXs:Landroid/support/v7/widget/p;

    iget-object v1, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a/b;->dnM(Landroid/view/View;)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/a/i;->dXt:Landroid/support/v7/widget/a/b;

    iget-object v0, v0, Landroid/support/v7/widget/a/b;->dWE:Ljava/util/List;

    iget-object v1, p0, Landroid/support/v7/widget/a/i;->dXs:Landroid/support/v7/widget/p;

    iget-object v1, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/a/i;->dXl:Z

    iget v0, p0, Landroid/support/v7/widget/a/i;->dXu:I

    if-lez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/a/i;->dXt:Landroid/support/v7/widget/a/b;

    iget v1, p0, Landroid/support/v7/widget/a/i;->dXu:I

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/a/b;->dnH(Landroid/support/v7/widget/a/f;I)V

    goto :goto_0
.end method
