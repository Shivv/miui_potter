.class Landroid/support/v7/widget/a/f;
.super Ljava/lang/Object;
.source "ItemTouchHelper.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final dXc:I

.field final dXd:Landroid/support/v7/widget/p;

.field final dXe:F

.field dXf:F

.field final dXg:F

.field dXh:F

.field final dXi:I

.field dXj:Z

.field final dXk:F

.field public dXl:Z

.field dXm:Z

.field private final dXn:Landroid/animation/ValueAnimator;

.field final dXo:F

.field private dXp:F


# direct methods
.method constructor <init>(Landroid/support/v7/widget/p;IIFFFF)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Landroid/support/v7/widget/a/f;->dXm:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/a/f;->dXj:Z

    iput p3, p0, Landroid/support/v7/widget/a/f;->dXc:I

    iput p2, p0, Landroid/support/v7/widget/a/f;->dXi:I

    iput-object p1, p0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iput p4, p0, Landroid/support/v7/widget/a/f;->dXg:F

    iput p5, p0, Landroid/support/v7/widget/a/f;->dXe:F

    iput p6, p0, Landroid/support/v7/widget/a/f;->dXk:F

    iput p7, p0, Landroid/support/v7/widget/a/f;->dXo:F

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/a/f;->dXn:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXn:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/support/v7/widget/a/o;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/a/o;-><init>(Landroid/support/v7/widget/a/f;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXn:Landroid/animation/ValueAnimator;

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXn:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/a/f;->dom(F)V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public cancel()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXn:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    return-void
.end method

.method public dom(F)V
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/a/f;->dXp:F

    return-void
.end method

.method public don()V
    .locals 4

    iget v0, p0, Landroid/support/v7/widget/a/f;->dXg:F

    iget v1, p0, Landroid/support/v7/widget/a/f;->dXk:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a/f;->dXf:F

    :goto_0
    iget v0, p0, Landroid/support/v7/widget/a/f;->dXe:F

    iget v1, p0, Landroid/support/v7/widget/a/f;->dXo:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a/f;->dXh:F

    :goto_1
    return-void

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/a/f;->dXg:F

    iget v1, p0, Landroid/support/v7/widget/a/f;->dXp:F

    iget v2, p0, Landroid/support/v7/widget/a/f;->dXk:F

    iget v3, p0, Landroid/support/v7/widget/a/f;->dXg:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/a/f;->dXf:F

    goto :goto_0

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/a/f;->dXe:F

    iget v1, p0, Landroid/support/v7/widget/a/f;->dXp:F

    iget v2, p0, Landroid/support/v7/widget/a/f;->dXo:F

    iget v3, p0, Landroid/support/v7/widget/a/f;->dXe:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/a/f;->dXh:F

    goto :goto_1
.end method

.method public doo(J)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXn:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    return-void
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/a/f;->dom(F)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Landroid/support/v7/widget/a/f;->dXj:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/p;->setIsRecyclable(Z)V

    :cond_0
    iput-boolean v1, p0, Landroid/support/v7/widget/a/f;->dXj:Z

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public start()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/p;->setIsRecyclable(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/a/f;->dXn:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method
