.class final Landroid/support/v7/widget/J;
.super Ljava/lang/Object;
.source "GapWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static eaV:Ljava/util/Comparator;

.field static final eaW:Ljava/lang/ThreadLocal;


# instance fields
.field eaT:J

.field eaU:J

.field private eaX:Ljava/util/ArrayList;

.field eaY:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Landroid/support/v7/widget/J;->eaW:Ljava/lang/ThreadLocal;

    new-instance v0, Landroid/support/v7/widget/de;

    invoke-direct {v0}, Landroid/support/v7/widget/de;-><init>()V

    sput-object v0, Landroid/support/v7/widget/J;->eaV:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/J;->eaY:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/J;->eaX:Ljava/util/ArrayList;

    return-void
.end method

.method private dwT(Landroid/support/v7/widget/RecyclerView;IJ)Landroid/support/v7/widget/p;
    .locals 5

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-static {p1, p2}, Landroid/support/v7/widget/J;->dwW(Landroid/support/v7/widget/RecyclerView;I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    :try_start_0
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->dqP()V

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1, p3, p4}, Landroid/support/v7/widget/j;->dug(IZJ)Landroid/support/v7/widget/p;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->isBound()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    iget-object v2, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/j;->dtZ(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    invoke-virtual {p1, v3}, Landroid/support/v7/widget/RecyclerView;->dpJ(Z)V

    return-object v1

    :cond_2
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/j;->due(Landroid/support/v7/widget/p;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1, v3}, Landroid/support/v7/widget/RecyclerView;->dpJ(Z)V

    throw v0
.end method

.method private dwV()V
    .locals 11

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/J;->eaY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v2, v3

    move v1, v3

    :goto_0
    if-ge v2, v7, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/J;->eaY:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWindowVisibility()I

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    invoke-virtual {v4, v0, v3}, Landroid/support/v7/widget/E;->dvF(Landroid/support/v7/widget/RecyclerView;Z)V

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    iget v0, v0, Landroid/support/v7/widget/E;->eaE:I

    add-int/2addr v0, v1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/J;->eaX:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    move v6, v3

    move v1, v3

    :goto_2
    if-ge v6, v7, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/J;->eaY:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWindowVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    :goto_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    :cond_1
    iget-object v8, v0, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    iget v2, v8, Landroid/support/v7/widget/E;->eaC:I

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v4, v8, Landroid/support/v7/widget/E;->eaD:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    add-int v9, v2, v4

    move v2, v3

    move v4, v1

    :goto_4
    iget v1, v8, Landroid/support/v7/widget/E;->eaE:I

    mul-int/lit8 v1, v1, 0x2

    if-ge v2, v1, :cond_5

    iget-object v1, p0, Landroid/support/v7/widget/J;->eaX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v4, v1, :cond_2

    new-instance v1, Landroid/support/v7/widget/dd;

    invoke-direct {v1}, Landroid/support/v7/widget/dd;-><init>()V

    iget-object v5, p0, Landroid/support/v7/widget/J;->eaX:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_5
    iget-object v5, v8, Landroid/support/v7/widget/E;->eaF:[I

    add-int/lit8 v10, v2, 0x1

    aget v10, v5, v10

    if-gt v10, v9, :cond_3

    const/4 v5, 0x1

    :goto_6
    iput-boolean v5, v1, Landroid/support/v7/widget/dd;->era:Z

    iput v9, v1, Landroid/support/v7/widget/dd;->erb:I

    iput v10, v1, Landroid/support/v7/widget/dd;->eqZ:I

    iput-object v0, v1, Landroid/support/v7/widget/dd;->erd:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v8, Landroid/support/v7/widget/E;->eaF:[I

    aget v5, v5, v2

    iput v5, v1, Landroid/support/v7/widget/dd;->erc:I

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v1, v2, 0x2

    move v2, v1

    goto :goto_4

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/J;->eaX:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/dd;

    goto :goto_5

    :cond_3
    move v5, v3

    goto :goto_6

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/J;->eaX:Ljava/util/ArrayList;

    sget-object v1, Landroid/support/v7/widget/J;->eaV:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void

    :cond_5
    move v1, v4

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method static dwW(Landroid/support/v7/widget/RecyclerView;I)Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    iget v4, v3, Landroid/support/v7/widget/p;->mPosition:I

    if-ne v4, p1, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private dwX(Landroid/support/v7/widget/RecyclerView;J)V
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-boolean v1, p1, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    if-eqz v1, :cond_1

    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v1}, Landroid/support/v7/widget/G;->dvO()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->dpV()V

    :cond_1
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Landroid/support/v7/widget/E;->dvF(Landroid/support/v7/widget/RecyclerView;Z)V

    iget v2, v1, Landroid/support/v7/widget/E;->eaE:I

    if-eqz v2, :cond_3

    :try_start_0
    const-string/jumbo v2, "RV Nested Prefetch"

    invoke-static {v2}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    iget-object v2, p1, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v3, p1, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/e;->dtm(Landroid/support/v7/widget/b;)V

    :goto_0
    iget v2, v1, Landroid/support/v7/widget/E;->eaE:I

    mul-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_2

    iget-object v2, v1, Landroid/support/v7/widget/E;->eaF:[I

    aget v2, v2, v0

    invoke-direct {p0, p1, v2, p2, p3}, Landroid/support/v7/widget/J;->dwT(Landroid/support/v7/widget/RecyclerView;IJ)Landroid/support/v7/widget/p;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_2
    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    throw v0
.end method

.method private dwZ(Landroid/support/v7/widget/dd;J)V
    .locals 4

    iget-boolean v0, p1, Landroid/support/v7/widget/dd;->era:Z

    if-eqz v0, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    iget-object v2, p1, Landroid/support/v7/widget/dd;->erd:Landroid/support/v7/widget/RecyclerView;

    iget v3, p1, Landroid/support/v7/widget/dd;->erc:I

    invoke-direct {p0, v2, v3, v0, v1}, Landroid/support/v7/widget/J;->dwT(Landroid/support/v7/widget/RecyclerView;IJ)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/support/v7/widget/p;->mNestedRecyclerView:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isBound()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/p;->mNestedRecyclerView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0, v0, p2, p3}, Landroid/support/v7/widget/J;->dwX(Landroid/support/v7/widget/RecyclerView;J)V

    :cond_0
    return-void

    :cond_1
    move-wide v0, p2

    goto :goto_0
.end method

.method private dxb(J)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/J;->eaX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/J;->eaX:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dd;

    iget-object v2, v0, Landroid/support/v7/widget/dd;->erd:Landroid/support/v7/widget/RecyclerView;

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, v0, p1, p2}, Landroid/support/v7/widget/J;->dwZ(Landroid/support/v7/widget/dd;J)V

    invoke-virtual {v0}, Landroid/support/v7/widget/dd;->clear()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public dwS(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/J;->eaY:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method dwU(J)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/J;->dwV()V

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/J;->dxb(J)V

    return-void
.end method

.method dwY(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Landroid/support/v7/widget/J;->eaT:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getNanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v7/widget/J;->eaT:J

    invoke-virtual {p1, p0}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/E;->dvH(II)V

    return-void
.end method

.method public dxa(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/J;->eaY:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public run()V
    .locals 8

    const/4 v0, 0x0

    const-wide/16 v6, 0x0

    :try_start_0
    const-string/jumbo v1, "RV Prefetch"

    invoke-static {v1}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/widget/J;->eaY:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    iput-wide v6, p0, Landroid/support/v7/widget/J;->eaT:J

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Landroid/support/v7/widget/J;->eaY:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v0

    move-wide v2, v6

    :goto_0
    if-ge v4, v5, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/J;->eaY:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWindowVisibility()I

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getDrawingTime()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_0

    :cond_1
    cmp-long v0, v2, v6

    if-nez v0, :cond_2

    iput-wide v6, p0, Landroid/support/v7/widget/J;->eaT:J

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    return-void

    :cond_2
    :try_start_2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iget-wide v2, p0, Landroid/support/v7/widget/J;->eaU:J

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/J;->dwU(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-wide v6, p0, Landroid/support/v7/widget/J;->eaT:J

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    return-void

    :catchall_0
    move-exception v0

    iput-wide v6, p0, Landroid/support/v7/widget/J;->eaT:J

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    throw v0

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method
