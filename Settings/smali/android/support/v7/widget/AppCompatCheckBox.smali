.class public Landroid/support/v7/widget/AppCompatCheckBox;
.super Landroid/widget/CheckBox;
.source "AppCompatCheckBox.java"

# interfaces
.implements Landroid/support/v4/widget/aD;


# instance fields
.field private final efF:Landroid/support/v7/widget/cq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/AppCompatCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Landroid/support/v7/b/a;->dOo:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/AppCompatCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-static {p1}, Landroid/support/v7/widget/bM;->dEu(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/support/v7/widget/cq;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cq;-><init>(Landroid/widget/CompoundButton;)V

    iput-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/cq;->dHl(Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public getCompoundPaddingLeft()I
    .locals 2

    invoke-super {p0}, Landroid/widget/CheckBox;->getCompoundPaddingLeft()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cq;->dHm(I)I

    move-result v0

    :cond_0
    return v0
.end method

.method public getSupportButtonTintList()Landroid/content/res/ColorStateList;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    invoke-virtual {v0}, Landroid/support/v7/widget/cq;->getSupportButtonTintList()Landroid/content/res/ColorStateList;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    invoke-virtual {v0}, Landroid/support/v7/widget/cq;->getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public setButtonDrawable(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/AppCompatCheckBox;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v7/f/a/d;->dMC(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/AppCompatCheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/CheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    invoke-virtual {v0}, Landroid/support/v7/widget/cq;->dHo()V

    :cond_0
    return-void
.end method

.method public setSupportButtonTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/cq;->setSupportButtonTintList(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/AppCompatCheckBox;->efF:Landroid/support/v7/widget/cq;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/cq;->setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method
