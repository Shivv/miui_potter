.class final Landroid/support/v7/widget/de;
.super Ljava/lang/Object;
.source "GapWorker.java"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Landroid/support/v7/widget/dd;

    check-cast p2, Landroid/support/v7/widget/dd;

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/de;->dIS(Landroid/support/v7/widget/dd;Landroid/support/v7/widget/dd;)I

    move-result v0

    return v0
.end method

.method public dIS(Landroid/support/v7/widget/dd;Landroid/support/v7/widget/dd;)I
    .locals 5

    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p1, Landroid/support/v7/widget/dd;->erd:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v4, p2, Landroid/support/v7/widget/dd;->erd:Landroid/support/v7/widget/RecyclerView;

    if-nez v4, :cond_1

    move v4, v1

    :goto_1
    if-eq v0, v4, :cond_3

    iget-object v0, p1, Landroid/support/v7/widget/dd;->erd:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v4, v2

    goto :goto_1

    :cond_2
    move v1, v3

    goto :goto_2

    :cond_3
    iget-boolean v0, p1, Landroid/support/v7/widget/dd;->era:Z

    iget-boolean v4, p2, Landroid/support/v7/widget/dd;->era:Z

    if-eq v0, v4, :cond_5

    iget-boolean v0, p1, Landroid/support/v7/widget/dd;->era:Z

    if-eqz v0, :cond_4

    :goto_3
    return v3

    :cond_4
    move v3, v1

    goto :goto_3

    :cond_5
    iget v0, p2, Landroid/support/v7/widget/dd;->erb:I

    iget v1, p1, Landroid/support/v7/widget/dd;->erb:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_6

    return v0

    :cond_6
    iget v0, p1, Landroid/support/v7/widget/dd;->eqZ:I

    iget v1, p2, Landroid/support/v7/widget/dd;->eqZ:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_7

    return v0

    :cond_7
    return v2
.end method
