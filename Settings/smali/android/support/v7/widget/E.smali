.class Landroid/support/v7/widget/E;
.super Ljava/lang/Object;
.source "GapWorker.java"

# interfaces
.implements Landroid/support/v7/widget/M;


# instance fields
.field eaC:I

.field eaD:I

.field eaE:I

.field eaF:[I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method dvF(Landroid/support/v7/widget/RecyclerView;Z)V
    .locals 4

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/E;->eaE:I

    iget-object v0, p0, Landroid/support/v7/widget/E;->eaF:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/E;->eaF:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dqW()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_3

    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v1}, Landroid/support/v7/widget/I;->dwN()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/support/v7/widget/a;->dsB(ILandroid/support/v7/widget/M;)V

    :cond_1
    :goto_0
    iget v1, p0, Landroid/support/v7/widget/E;->eaE:I

    iget v2, v0, Landroid/support/v7/widget/a;->dZq:I

    if-le v1, v2, :cond_2

    iget v1, p0, Landroid/support/v7/widget/E;->eaE:I

    iput v1, v0, Landroid/support/v7/widget/a;->dZq:I

    iput-boolean p2, v0, Landroid/support/v7/widget/a;->dZh:Z

    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->dua()V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->dpp()Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Landroid/support/v7/widget/E;->eaC:I

    iget v2, p0, Landroid/support/v7/widget/E;->eaD:I

    iget-object v3, p1, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1, v2, v3, p0}, Landroid/support/v7/widget/a;->dsW(IILandroid/support/v7/widget/e;Landroid/support/v7/widget/M;)V

    goto :goto_0
.end method

.method public dvG(II)V
    .locals 5

    const/4 v4, 0x0

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Layout positions must be non-negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-gez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Pixel distance must be non-negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/E;->eaE:I

    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Landroid/support/v7/widget/E;->eaF:[I

    if-nez v1, :cond_3

    const/4 v1, 0x4

    new-array v1, v1, [I

    iput-object v1, p0, Landroid/support/v7/widget/E;->eaF:[I

    iget-object v1, p0, Landroid/support/v7/widget/E;->eaF:[I

    const/4 v2, -0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    :cond_2
    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/E;->eaF:[I

    aput p1, v1, v0

    iget-object v1, p0, Landroid/support/v7/widget/E;->eaF:[I

    add-int/lit8 v0, v0, 0x1

    aput p2, v1, v0

    iget v0, p0, Landroid/support/v7/widget/E;->eaE:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/E;->eaE:I

    return-void

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/E;->eaF:[I

    array-length v1, v1

    if-lt v0, v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/E;->eaF:[I

    mul-int/lit8 v2, v0, 0x2

    new-array v2, v2, [I

    iput-object v2, p0, Landroid/support/v7/widget/E;->eaF:[I

    iget-object v2, p0, Landroid/support/v7/widget/E;->eaF:[I

    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method dvH(II)V
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/E;->eaC:I

    iput p2, p0, Landroid/support/v7/widget/E;->eaD:I

    return-void
.end method

.method dvI()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/E;->eaF:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/E;->eaF:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/E;->eaE:I

    return-void
.end method

.method dvJ(I)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/E;->eaF:[I

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v7/widget/E;->eaE:I

    mul-int/lit8 v2, v0, 0x2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/E;->eaF:[I

    aget v3, v3, v0

    if-ne v3, p1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    return v1
.end method
