.class Landroid/support/v7/widget/an;
.super Ljava/lang/Object;
.source "LinearLayoutManager.java"


# instance fields
.field eda:I

.field edb:Z

.field edc:Z

.field final synthetic edd:Landroid/support/v7/widget/al;

.field mPosition:I


# direct methods
.method constructor <init>(Landroid/support/v7/widget/al;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Landroid/support/v7/widget/an;->dza()V

    return-void
.end method


# virtual methods
.method dyX()V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/an;->edb:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v0, v0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v0

    :goto_0
    iput v0, p0, Landroid/support/v7/widget/an;->eda:I

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v0, v0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v0

    goto :goto_0
.end method

.method public dyY(Landroid/view/View;)V
    .locals 2

    iget-boolean v0, p0, Landroid/support/v7/widget/an;->edb:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v0, v0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v1, v1, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1}, Landroid/support/v7/widget/ap;->dzp()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/an;->eda:I

    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/an;->mPosition:I

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v0, v0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/an;->eda:I

    goto :goto_0
.end method

.method dyZ(Landroid/view/View;Landroid/support/v7/widget/e;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duz()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duw()I

    move-result v2

    if-ltz v2, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duw()I

    move-result v0

    invoke-virtual {p2}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method dza()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/an;->mPosition:I

    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/an;->eda:I

    iput-boolean v1, p0, Landroid/support/v7/widget/an;->edb:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/an;->edc:Z

    return-void
.end method

.method public dzb(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v0, v0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzp()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/an;->dyY(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/an;->mPosition:I

    iget-boolean v1, p0, Landroid/support/v7/widget/an;->edb:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v1, v1, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v1

    sub-int v0, v1, v0

    iget-object v1, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v1, v1, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v1, v1, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v1

    sub-int/2addr v1, v0

    iput v1, p0, Landroid/support/v7/widget/an;->eda:I

    if-lez v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v1, v1, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/ap;->dzg(Landroid/view/View;)I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/an;->eda:I

    sub-int v1, v2, v1

    iget-object v2, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v2, v2, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v3, v3, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, p1}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v3, v2

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    if-gez v1, :cond_1

    iget v2, p0, Landroid/support/v7/widget/an;->eda:I

    neg-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/2addr v0, v2

    iput v0, p0, Landroid/support/v7/widget/an;->eda:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v1, v1, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v2, v2, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v2

    sub-int v2, v1, v2

    iput v1, p0, Landroid/support/v7/widget/an;->eda:I

    if-lez v2, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v3, v3, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, p1}, Landroid/support/v7/widget/ap;->dzg(Landroid/view/View;)I

    move-result v3

    add-int/2addr v1, v3

    iget-object v3, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v3, v3, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v3

    sub-int v0, v3, v0

    iget-object v3, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v3, v3, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, p1}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v0, v3

    iget-object v3, p0, Landroid/support/v7/widget/an;->edd:Landroid/support/v7/widget/al;

    iget-object v3, v3, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v3

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v0, v3, v0

    sub-int/2addr v0, v1

    if-gez v0, :cond_1

    iget v1, p0, Landroid/support/v7/widget/an;->eda:I

    neg-int v0, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p0, Landroid/support/v7/widget/an;->eda:I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "AnchorInfo{mPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/an;->mPosition:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mCoordinate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/an;->eda:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mLayoutFromEnd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/an;->edb:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/an;->edc:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
