.class final Landroid/support/v7/widget/N;
.super Ljava/lang/Object;
.source "RecyclerView.java"

# interfaces
.implements Landroid/support/v7/widget/P;


# instance fields
.field final synthetic ebh:Landroid/support/v7/widget/a;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/a;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/N;->ebh:Landroid/support/v7/widget/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dxe()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/N;->ebh:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dqU()I

    move-result v0

    return v0
.end method

.method public dxf(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, p0, Landroid/support/v7/widget/N;->ebh:Landroid/support/v7/widget/a;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/a;->drv(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    sub-int v0, v1, v0

    return v0
.end method

.method public dxg()I
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/N;->ebh:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->drZ()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/N;->ebh:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsS()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public dxh(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/N;->ebh:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public dxi(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, p0, Landroid/support/v7/widget/N;->ebh:Landroid/support/v7/widget/a;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/a;->drg(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method
