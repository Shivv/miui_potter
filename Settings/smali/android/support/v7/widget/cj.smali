.class final Landroid/support/v7/widget/cj;
.super Ljava/lang/Object;
.source "AppCompatSpinner.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic enS:Landroid/support/v7/widget/cf;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/cf;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/cj;->enS:Landroid/support/v7/widget/cf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/cj;->enS:Landroid/support/v7/widget/cf;

    iget-object v0, v0, Landroid/support/v7/widget/cf;->enG:Landroid/support/v7/widget/AppCompatSpinner;

    invoke-virtual {v0, p3}, Landroid/support/v7/widget/AppCompatSpinner;->setSelection(I)V

    iget-object v0, p0, Landroid/support/v7/widget/cj;->enS:Landroid/support/v7/widget/cf;

    iget-object v0, v0, Landroid/support/v7/widget/cf;->enG:Landroid/support/v7/widget/AppCompatSpinner;

    invoke-virtual {v0}, Landroid/support/v7/widget/AppCompatSpinner;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cj;->enS:Landroid/support/v7/widget/cf;

    iget-object v0, v0, Landroid/support/v7/widget/cf;->enG:Landroid/support/v7/widget/AppCompatSpinner;

    iget-object v1, p0, Landroid/support/v7/widget/cj;->enS:Landroid/support/v7/widget/cf;

    iget-object v1, v1, Landroid/support/v7/widget/cf;->enJ:Landroid/widget/ListAdapter;

    invoke-interface {v1, p3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v0, p2, p3, v2, v3}, Landroid/support/v7/widget/AppCompatSpinner;->performItemClick(Landroid/view/View;IJ)Z

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/cj;->enS:Landroid/support/v7/widget/cf;

    invoke-virtual {v0}, Landroid/support/v7/widget/cf;->dismiss()V

    return-void
.end method
