.class public abstract Landroid/support/v7/widget/u;
.super Ljava/lang/Object;
.source "RecyclerView.java"


# instance fields
.field private ear:J

.field private eas:J

.field private eat:J

.field private eau:J

.field private eav:Ljava/util/ArrayList;

.field private eaw:Landroid/support/v7/widget/D;


# direct methods
.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0xfa

    const-wide/16 v2, 0x78

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/u;->eaw:Landroid/support/v7/widget/D;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/u;->eav:Ljava/util/ArrayList;

    iput-wide v2, p0, Landroid/support/v7/widget/u;->eas:J

    iput-wide v2, p0, Landroid/support/v7/widget/u;->ear:J

    iput-wide v4, p0, Landroid/support/v7/widget/u;->eat:J

    iput-wide v4, p0, Landroid/support/v7/widget/u;->eau:J

    return-void
.end method

.method static duP(Landroid/support/v7/widget/p;)I
    .locals 4

    const/4 v3, -0x1

    invoke-static {p0}, Landroid/support/v7/widget/p;->-get0(Landroid/support/v7/widget/p;)I

    move-result v0

    and-int/lit8 v0, v0, 0xe

    invoke-virtual {p0}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    return v0

    :cond_0
    and-int/lit8 v1, v0, 0x4

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/p;->getOldPosition()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/p;->getAdapterPosition()I

    move-result v2

    if-eq v1, v3, :cond_1

    if-eq v2, v3, :cond_1

    if-eq v1, v2, :cond_1

    or-int/lit16 v0, v0, 0x800

    :cond_1
    return v0
.end method


# virtual methods
.method public duM()J
    .locals 2

    iget-wide v0, p0, Landroid/support/v7/widget/u;->eau:J

    return-wide v0
.end method

.method public final duN()V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/u;->eav:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->eav:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/V;

    invoke-interface {v0}, Landroid/support/v7/widget/V;->HX()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/u;->eav:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public abstract duO()V
.end method

.method duQ(Landroid/support/v7/widget/D;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/u;->eaw:Landroid/support/v7/widget/D;

    return-void
.end method

.method public abstract duR(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z
.end method

.method public abstract duS(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z
.end method

.method public abstract duT(Landroid/support/v7/widget/p;)V
.end method

.method public duU()Landroid/support/v7/widget/K;
    .locals 1

    new-instance v0, Landroid/support/v7/widget/K;

    invoke-direct {v0}, Landroid/support/v7/widget/K;-><init>()V

    return-object v0
.end method

.method public duV(Landroid/support/v7/widget/p;Ljava/util/List;)Z
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/u;->dvf(Landroid/support/v7/widget/p;)Z

    move-result v0

    return v0
.end method

.method public duW(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public duX()J
    .locals 2

    iget-wide v0, p0, Landroid/support/v7/widget/u;->eat:J

    return-wide v0
.end method

.method public duY()J
    .locals 2

    iget-wide v0, p0, Landroid/support/v7/widget/u;->eas:J

    return-wide v0
.end method

.method public final duZ(Landroid/support/v7/widget/V;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/widget/u;->isRunning()Z

    move-result v0

    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    invoke-interface {p1}, Landroid/support/v7/widget/V;->HX()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/u;->eav:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public dva(Landroid/support/v7/widget/e;Landroid/support/v7/widget/p;ILjava/util/List;)Landroid/support/v7/widget/K;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/u;->duU()Landroid/support/v7/widget/K;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/K;->dxc(Landroid/support/v7/widget/p;)Landroid/support/v7/widget/K;

    move-result-object v0

    return-object v0
.end method

.method public dvb(Landroid/support/v7/widget/e;Landroid/support/v7/widget/p;)Landroid/support/v7/widget/K;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/u;->duU()Landroid/support/v7/widget/K;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/K;->dxc(Landroid/support/v7/widget/p;)Landroid/support/v7/widget/K;

    move-result-object v0

    return-object v0
.end method

.method public dvc()J
    .locals 2

    iget-wide v0, p0, Landroid/support/v7/widget/u;->ear:J

    return-wide v0
.end method

.method public abstract dvd(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z
.end method

.method public final dve(Landroid/support/v7/widget/p;)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/u;->duW(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/u;->eaw:Landroid/support/v7/widget/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/u;->eaw:Landroid/support/v7/widget/D;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/D;->duL(Landroid/support/v7/widget/p;)V

    :cond_0
    return-void
.end method

.method public dvf(Landroid/support/v7/widget/p;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract dvg(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z
.end method

.method public abstract dvh()V
.end method

.method public abstract isRunning()Z
.end method
