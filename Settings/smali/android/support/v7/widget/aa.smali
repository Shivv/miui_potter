.class public abstract Landroid/support/v7/widget/aa;
.super Landroid/support/v7/widget/u;
.source "SimpleItemAnimator.java"


# instance fields
.field ebI:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/u;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/aa;->ebI:Z

    return-void
.end method


# virtual methods
.method public abstract CY(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;IIII)Z
.end method

.method public duR(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z
    .locals 7

    iget v2, p2, Landroid/support/v7/widget/K;->eba:I

    iget v3, p2, Landroid/support/v7/widget/K;->ebb:I

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    if-nez p3, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    :goto_0
    if-nez p3, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    :goto_1
    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v1

    if-nez v1, :cond_3

    if-ne v2, v4, :cond_0

    if-eq v3, v5, :cond_3

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v1, v4

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v5

    invoke-virtual {v0, v4, v5, v1, v6}, Landroid/view/View;->layout(IIII)V

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/aa;->dxt(Landroid/support/v7/widget/p;IIII)Z

    move-result v0

    return v0

    :cond_1
    iget v4, p3, Landroid/support/v7/widget/K;->eba:I

    goto :goto_0

    :cond_2
    iget v5, p3, Landroid/support/v7/widget/K;->ebb:I

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dxv(Landroid/support/v7/widget/p;)Z

    move-result v0

    return v0
.end method

.method public duS(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z
    .locals 6

    iget v0, p2, Landroid/support/v7/widget/K;->eba:I

    iget v1, p3, Landroid/support/v7/widget/K;->eba:I

    if-ne v0, v1, :cond_0

    iget v0, p2, Landroid/support/v7/widget/K;->ebb:I

    iget v1, p3, Landroid/support/v7/widget/K;->ebb:I

    if-eq v0, v1, :cond_1

    :cond_0
    iget v2, p2, Landroid/support/v7/widget/K;->eba:I

    iget v3, p2, Landroid/support/v7/widget/K;->ebb:I

    iget v4, p3, Landroid/support/v7/widget/K;->eba:I

    iget v5, p3, Landroid/support/v7/widget/K;->ebb:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/aa;->dxt(Landroid/support/v7/widget/p;IIII)Z

    move-result v0

    return v0

    :cond_1
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dxQ(Landroid/support/v7/widget/p;)V

    const/4 v0, 0x0

    return v0
.end method

.method public dvd(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z
    .locals 6

    if-eqz p2, :cond_1

    iget v0, p2, Landroid/support/v7/widget/K;->eba:I

    iget v1, p3, Landroid/support/v7/widget/K;->eba:I

    if-ne v0, v1, :cond_0

    iget v0, p2, Landroid/support/v7/widget/K;->ebb:I

    iget v1, p3, Landroid/support/v7/widget/K;->ebb:I

    if-eq v0, v1, :cond_1

    :cond_0
    iget v2, p2, Landroid/support/v7/widget/K;->eba:I

    iget v3, p2, Landroid/support/v7/widget/K;->ebb:I

    iget v4, p3, Landroid/support/v7/widget/K;->eba:I

    iget v5, p3, Landroid/support/v7/widget/K;->ebb:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/aa;->dxt(Landroid/support/v7/widget/p;IIII)Z

    move-result v0

    return v0

    :cond_1
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dxx(Landroid/support/v7/widget/p;)Z

    move-result v0

    return v0
.end method

.method public dvf(Landroid/support/v7/widget/p;)Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/aa;->ebI:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dvg(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z
    .locals 7

    iget v3, p3, Landroid/support/v7/widget/K;->eba:I

    iget v4, p3, Landroid/support/v7/widget/K;->ebb:I

    invoke-virtual {p2}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v5, p3, Landroid/support/v7/widget/K;->eba:I

    iget v6, p3, Landroid/support/v7/widget/K;->ebb:I

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Landroid/support/v7/widget/aa;->CY(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;IIII)Z

    move-result v0

    return v0

    :cond_0
    iget v5, p4, Landroid/support/v7/widget/K;->eba:I

    iget v6, p4, Landroid/support/v7/widget/K;->ebb:I

    goto :goto_0
.end method

.method public final dxB(Landroid/support/v7/widget/p;Z)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/aa;->dxC(Landroid/support/v7/widget/p;Z)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dve(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method public dxC(Landroid/support/v7/widget/p;Z)V
    .locals 0

    return-void
.end method

.method public dxD(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public final dxE(Landroid/support/v7/widget/p;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dxJ(Landroid/support/v7/widget/p;)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dve(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method public dxF(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public final dxG(Landroid/support/v7/widget/p;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dxK(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method public final dxH(Landroid/support/v7/widget/p;Z)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/aa;->dxL(Landroid/support/v7/widget/p;Z)V

    return-void
.end method

.method public final dxI(Landroid/support/v7/widget/p;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dxO(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method public dxJ(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public dxK(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public dxL(Landroid/support/v7/widget/p;Z)V
    .locals 0

    return-void
.end method

.method public dxM(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public final dxN(Landroid/support/v7/widget/p;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dxM(Landroid/support/v7/widget/p;)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dve(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method public dxO(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public final dxP(Landroid/support/v7/widget/p;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dxF(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method public final dxQ(Landroid/support/v7/widget/p;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dxD(Landroid/support/v7/widget/p;)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/aa;->dve(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method public abstract dxt(Landroid/support/v7/widget/p;IIII)Z
.end method

.method public abstract dxv(Landroid/support/v7/widget/p;)Z
.end method

.method public abstract dxx(Landroid/support/v7/widget/p;)Z
.end method
