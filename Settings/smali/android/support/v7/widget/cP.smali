.class Landroid/support/v7/widget/cP;
.super Ljava/lang/Object;
.source "ScrollbarHelper.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static dIw(Landroid/support/v7/widget/e;Landroid/support/v7/widget/ap;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/a;Z)I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p4}, Landroid/support/v7/widget/a;->drc()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return v1

    :cond_1
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p5, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v0

    return v0

    :cond_2
    invoke-virtual {p1, p3}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p1, p2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p4, p2}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p4, p3}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method static dIx(Landroid/support/v7/widget/e;Landroid/support/v7/widget/ap;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/a;ZZ)I
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p4}, Landroid/support/v7/widget/a;->drc()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return v3

    :cond_1
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p4, p2}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4, p3}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p4, p2}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p4, p3}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    if-eqz p6, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v0

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    if-nez p5, :cond_3

    return v0

    :cond_2
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-virtual {p1, p3}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1, p2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {p4, p2}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v2

    invoke-virtual {p4, p3}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-float v1, v1

    int-to-float v2, v2

    div-float/2addr v1, v2

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v1

    invoke-virtual {p1, p2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method static dIy(Landroid/support/v7/widget/e;Landroid/support/v7/widget/ap;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/a;Z)I
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p4}, Landroid/support/v7/widget/a;->drc()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return v1

    :cond_1
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p5, :cond_2

    invoke-virtual {p4, p2}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4, p3}, Landroid/support/v7/widget/a;->drJ(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_2
    invoke-virtual {p1, p3}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p1, p2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/support/v7/widget/ap;->dzh()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method
