.class Landroid/support/v7/widget/ca;
.super Landroid/support/v7/view/menu/q;
.source "ActionMenuPresenter.java"

# interfaces
.implements Landroid/support/v4/view/aq;


# instance fields
.field private emW:Z

.field private emX:Z

.field private emY:I

.field private emZ:Z

.field final ena:Landroid/support/v7/widget/bZ;

.field private enb:I

.field private enc:I

.field private final end:Landroid/util/SparseBooleanArray;

.field private ene:Landroid/support/v7/widget/bf;

.field private enf:Z

.field eng:Landroid/support/v7/widget/bc;

.field private enh:Landroid/view/View;

.field eni:Landroid/support/v7/widget/be;

.field private enj:I

.field enk:Landroid/support/v7/widget/aY;

.field enl:Landroid/support/v7/widget/cm;

.field private enm:Z

.field private enn:Landroid/graphics/drawable/Drawable;

.field private eno:Z

.field enp:I

.field private enq:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    sget v0, Landroid/support/v7/b/g;->dQF:I

    sget v1, Landroid/support/v7/b/g;->dQE:I

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/view/menu/q;-><init>(Landroid/content/Context;II)V

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ca;->end:Landroid/util/SparseBooleanArray;

    new-instance v0, Landroid/support/v7/widget/bZ;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bZ;-><init>(Landroid/support/v7/widget/ca;)V

    iput-object v0, p0, Landroid/support/v7/widget/ca;->ena:Landroid/support/v7/widget/bZ;

    return-void
.end method

.method private dGg(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    return-object v5

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v1, v2, Landroid/support/v7/view/menu/y;

    if-eqz v1, :cond_1

    move-object v1, v2

    check-cast v1, Landroid/support/v7/view/menu/y;

    invoke-interface {v1}, Landroid/support/v7/view/menu/y;->getItemData()Landroid/support/v7/view/menu/z;

    move-result-object v1

    if-ne v1, p1, :cond_1

    return-object v2

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    return-object v5
.end method

.method static synthetic dGl(Landroid/support/v7/widget/ca;)Landroid/support/v7/view/menu/d;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    return-object v0
.end method

.method static synthetic dGn(Landroid/support/v7/widget/ca;)Landroid/support/v7/view/menu/p;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    return-object v0
.end method


# virtual methods
.method public dBW(Landroid/content/Context;Landroid/support/v7/view/menu/p;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Landroid/support/v7/view/menu/q;->dBW(Landroid/content/Context;Landroid/support/v7/view/menu/p;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1}, Landroid/support/v7/view/a;->dMb(Landroid/content/Context;)Landroid/support/v7/view/a;

    move-result-object v0

    iget-boolean v2, p0, Landroid/support/v7/widget/ca;->emW:Z

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/view/a;->dLX()Z

    move-result v2

    iput-boolean v2, p0, Landroid/support/v7/widget/ca;->enq:Z

    :cond_0
    iget-boolean v2, p0, Landroid/support/v7/widget/ca;->enm:Z

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/view/a;->dLW()I

    move-result v2

    iput v2, p0, Landroid/support/v7/widget/ca;->emY:I

    :cond_1
    iget-boolean v2, p0, Landroid/support/v7/widget/ca;->enf:Z

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/view/a;->dLY()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/ca;->enc:I

    :cond_2
    iget v0, p0, Landroid/support/v7/widget/ca;->emY:I

    iget-boolean v2, p0, Landroid/support/v7/widget/ca;->enq:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    if-nez v2, :cond_4

    new-instance v2, Landroid/support/v7/widget/aY;

    iget-object v3, p0, Landroid/support/v7/widget/ca;->etn:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Landroid/support/v7/widget/aY;-><init>(Landroid/support/v7/widget/ca;Landroid/content/Context;)V

    iput-object v2, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    iget-boolean v2, p0, Landroid/support/v7/widget/ca;->emZ:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    iget-object v3, p0, Landroid/support/v7/widget/ca;->enn:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/aY;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object v4, p0, Landroid/support/v7/widget/ca;->enn:Landroid/graphics/drawable/Drawable;

    iput-boolean v5, p0, Landroid/support/v7/widget/ca;->emZ:Z

    :cond_3
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    invoke-virtual {v3, v2, v2}, Landroid/support/v7/widget/aY;->measure(II)V

    :cond_4
    iget-object v2, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    invoke-virtual {v2}, Landroid/support/v7/widget/aY;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    :goto_0
    iput v0, p0, Landroid/support/v7/widget/ca;->enb:I

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42600000    # 56.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/ca;->enj:I

    iput-object v4, p0, Landroid/support/v7/widget/ca;->enh:Landroid/view/View;

    return-void

    :cond_5
    iput-object v4, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    goto :goto_0
.end method

.method public dBX(Z)V
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v7/view/menu/q;->dBX(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/p;->dKx()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_1

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->dJs()Landroid/support/v4/view/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ai;->dRB(Landroid/support/v4/view/aq;)V

    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/p;->dKk()Ljava/util/ArrayList;

    move-result-object v0

    :goto_1
    iget-boolean v1, p0, Landroid/support/v7/widget/ca;->enq:Z

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v2, :cond_7

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->isActionViewExpanded()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_2
    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    if-nez v0, :cond_2

    new-instance v0, Landroid/support/v7/widget/aY;

    iget-object v1, p0, Landroid/support/v7/widget/ca;->etn:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/aY;-><init>(Landroid/support/v7/widget/ca;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    invoke-virtual {v0}, Landroid/support/v7/widget/aY;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    if-eq v0, v1, :cond_4

    if-eqz v0, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iget-object v1, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->dFp()Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    :goto_3
    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    iget-boolean v1, p0, Landroid/support/v7/widget/ca;->enq:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->setOverflowReserved(Z)V

    return-void

    :cond_5
    move-object v0, v1

    goto :goto_1

    :cond_6
    move v0, v3

    goto :goto_2

    :cond_7
    if-lez v1, :cond_8

    move v0, v2

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_2

    :cond_9
    iget-object v0, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    invoke-virtual {v0}, Landroid/support/v7/widget/aY;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_3
.end method

.method public dCa(Landroid/support/v7/view/menu/h;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    return v2

    :cond_0
    move-object v0, p1

    :goto_0
    invoke-virtual {v0}, Landroid/support/v7/view/menu/h;->dJO()Landroid/view/Menu;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    if-eq v3, v4, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/view/menu/h;->dJO()Landroid/view/Menu;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/h;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/view/menu/h;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/ca;->dGg(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_2

    return v2

    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/ca;->enp:I

    invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->size()I

    move-result v4

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_4

    invoke-virtual {p1, v0}, Landroid/support/v7/view/menu/h;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_3

    move v0, v1

    :goto_2
    new-instance v2, Landroid/support/v7/widget/be;

    iget-object v4, p0, Landroid/support/v7/widget/ca;->mContext:Landroid/content/Context;

    invoke-direct {v2, p0, v4, p1, v3}, Landroid/support/v7/widget/be;-><init>(Landroid/support/v7/widget/ca;Landroid/content/Context;Landroid/support/v7/view/menu/h;Landroid/view/View;)V

    iput-object v2, p0, Landroid/support/v7/widget/ca;->eni:Landroid/support/v7/widget/be;

    iget-object v2, p0, Landroid/support/v7/widget/ca;->eni:Landroid/support/v7/widget/be;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/be;->setForceShowIcon(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eni:Landroid/support/v7/widget/be;

    invoke-virtual {v0}, Landroid/support/v7/widget/be;->show()V

    invoke-super {p0, p1}, Landroid/support/v7/view/menu/q;->dCa(Landroid/support/v7/view/menu/h;)Z

    return v1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public dCb()Z
    .locals 21

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    invoke-virtual {v2}, Landroid/support/v7/view/menu/p;->dJW()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v5, v2

    move-object v6, v3

    :goto_0
    move-object/from16 v0, p0

    iget v9, v0, Landroid/support/v7/widget/ca;->enc:I

    move-object/from16 v0, p0

    iget v11, v0, Landroid/support/v7/widget/ca;->enb:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    move v12, v3

    :goto_1
    if-ge v12, v5, :cond_4

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/view/menu/z;

    invoke-virtual {v3}, Landroid/support/v7/view/menu/z;->dLc()Z

    move-result v13

    if-eqz v13, :cond_1

    add-int/lit8 v8, v8, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Landroid/support/v7/widget/ca;->eno:Z

    if-eqz v13, :cond_3

    invoke-virtual {v3}, Landroid/support/v7/view/menu/z;->isActionViewExpanded()Z

    move-result v3

    if-eqz v3, :cond_21

    const/4 v3, 0x0

    :goto_3
    add-int/lit8 v9, v12, 0x1

    move v12, v9

    move v9, v3

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    const/4 v2, 0x0

    move v5, v2

    move-object v6, v3

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/support/v7/view/menu/z;->dLa()Z

    move-result v13

    if-eqz v13, :cond_2

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    move v3, v9

    goto :goto_3

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Landroid/support/v7/widget/ca;->enq:Z

    if-eqz v3, :cond_6

    if-nez v4, :cond_5

    add-int v3, v8, v7

    if-le v3, v9, :cond_6

    :cond_5
    add-int/lit8 v9, v9, -0x1

    :cond_6
    sub-int v12, v9, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v7/widget/ca;->end:Landroid/util/SparseBooleanArray;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/util/SparseBooleanArray;->clear()V

    const/4 v4, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-boolean v7, v0, Landroid/support/v7/widget/ca;->emX:Z

    if-eqz v7, :cond_20

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/ca;->enj:I

    div-int v3, v11, v3

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/ca;->enj:I

    rem-int v4, v11, v4

    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/ca;->enj:I

    div-int/2addr v4, v3

    add-int/2addr v4, v7

    move v7, v4

    :goto_4
    const/4 v4, 0x0

    move v14, v4

    move v9, v10

    move v4, v3

    :goto_5
    if-ge v14, v5, :cond_1a

    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/view/menu/z;

    invoke-virtual {v3}, Landroid/support/v7/view/menu/z;->dLc()Z

    move-result v8

    if-eqz v8, :cond_a

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/ca;->enh:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8, v2}, Landroid/support/v7/widget/ca;->dGe(Landroid/support/v7/view/menu/z;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v10, v0, Landroid/support/v7/widget/ca;->enh:Landroid/view/View;

    if-nez v10, :cond_7

    move-object/from16 v0, p0

    iput-object v8, v0, Landroid/support/v7/widget/ca;->enh:Landroid/view/View;

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v10, v0, Landroid/support/v7/widget/ca;->emX:Z

    if-eqz v10, :cond_9

    const/4 v10, 0x0

    invoke-static {v8, v7, v4, v15, v10}, Landroid/support/v7/widget/ActionMenuView;->dFj(Landroid/view/View;IIII)I

    move-result v10

    sub-int/2addr v4, v10

    :goto_6
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    sub-int v10, v11, v8

    if-nez v9, :cond_1f

    :goto_7
    invoke-virtual {v3}, Landroid/support/v7/view/menu/z;->getGroupId()I

    move-result v9

    if-eqz v9, :cond_8

    const/4 v11, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v9, v11}, Landroid/util/SparseBooleanArray;->put(IZ)V

    :cond_8
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Landroid/support/v7/view/menu/z;->dLl(Z)V

    move v3, v10

    move v9, v12

    :goto_8
    add-int/lit8 v10, v14, 0x1

    move v14, v10

    move v11, v3

    move v12, v9

    move v9, v8

    goto :goto_5

    :cond_9
    invoke-virtual {v8, v15, v15}, Landroid/view/View;->measure(II)V

    goto :goto_6

    :cond_a
    invoke-virtual {v3}, Landroid/support/v7/view/menu/z;->dLa()Z

    move-result v8

    if-eqz v8, :cond_19

    invoke-virtual {v3}, Landroid/support/v7/view/menu/z;->getGroupId()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v18

    if-gtz v12, :cond_b

    if-eqz v18, :cond_11

    :cond_b
    if-lez v11, :cond_11

    move-object/from16 v0, p0

    iget-boolean v8, v0, Landroid/support/v7/widget/ca;->emX:Z

    if-eqz v8, :cond_c

    if-lez v4, :cond_10

    :cond_c
    const/4 v8, 0x1

    :goto_9
    if-eqz v8, :cond_1e

    move-object/from16 v0, p0

    iget-object v10, v0, Landroid/support/v7/widget/ca;->enh:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10, v2}, Landroid/support/v7/widget/ca;->dGe(Landroid/support/v7/view/menu/z;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v10, v0, Landroid/support/v7/widget/ca;->enh:Landroid/view/View;

    if-nez v10, :cond_d

    move-object/from16 v0, p0

    iput-object v13, v0, Landroid/support/v7/widget/ca;->enh:Landroid/view/View;

    :cond_d
    move-object/from16 v0, p0

    iget-boolean v10, v0, Landroid/support/v7/widget/ca;->emX:Z

    if-eqz v10, :cond_12

    const/4 v10, 0x0

    invoke-static {v13, v7, v4, v15, v10}, Landroid/support/v7/widget/ActionMenuView;->dFj(Landroid/view/View;IIII)I

    move-result v19

    sub-int v10, v4, v19

    if-nez v19, :cond_1d

    const/4 v4, 0x0

    move v8, v10

    :goto_a
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    sub-int/2addr v11, v10

    if-nez v9, :cond_e

    move v9, v10

    :cond_e
    move-object/from16 v0, p0

    iget-boolean v10, v0, Landroid/support/v7/widget/ca;->emX:Z

    if-eqz v10, :cond_14

    if-ltz v11, :cond_13

    const/4 v10, 0x1

    :goto_b
    and-int/2addr v4, v10

    move v13, v4

    move v10, v9

    move v9, v8

    :goto_c
    if-eqz v13, :cond_16

    if-eqz v17, :cond_16

    const/4 v4, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    move v4, v12

    :goto_d
    if-eqz v13, :cond_f

    add-int/lit8 v4, v4, -0x1

    :cond_f
    invoke-virtual {v3, v13}, Landroid/support/v7/view/menu/z;->dLl(Z)V

    move v8, v10

    move v3, v11

    move/from16 v20, v9

    move v9, v4

    move/from16 v4, v20

    goto :goto_8

    :cond_10
    const/4 v8, 0x0

    goto :goto_9

    :cond_11
    const/4 v8, 0x0

    goto :goto_9

    :cond_12
    invoke-virtual {v13, v15, v15}, Landroid/view/View;->measure(II)V

    move/from16 v20, v8

    move v8, v4

    move/from16 v4, v20

    goto :goto_a

    :cond_13
    const/4 v10, 0x0

    goto :goto_b

    :cond_14
    add-int v10, v11, v9

    if-lez v10, :cond_15

    const/4 v10, 0x1

    :goto_e
    and-int/2addr v4, v10

    move v13, v4

    move v10, v9

    move v9, v8

    goto :goto_c

    :cond_15
    const/4 v10, 0x0

    goto :goto_e

    :cond_16
    if-eqz v18, :cond_1c

    const/4 v4, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    const/4 v4, 0x0

    move v8, v12

    move v12, v4

    :goto_f
    if-ge v12, v14, :cond_1b

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/support/v7/view/menu/z;

    invoke-virtual {v4}, Landroid/support/v7/view/menu/z;->getGroupId()I

    move-result v18

    move/from16 v0, v18

    move/from16 v1, v17

    if-ne v0, v1, :cond_18

    invoke-virtual {v4}, Landroid/support/v7/view/menu/z;->dKZ()Z

    move-result v18

    if-eqz v18, :cond_17

    add-int/lit8 v8, v8, 0x1

    :cond_17
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/support/v7/view/menu/z;->dLl(Z)V

    :cond_18
    add-int/lit8 v4, v12, 0x1

    move v12, v4

    goto :goto_f

    :cond_19
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/support/v7/view/menu/z;->dLl(Z)V

    move v8, v9

    move v3, v11

    move v9, v12

    goto/16 :goto_8

    :cond_1a
    const/4 v2, 0x1

    return v2

    :cond_1b
    move v4, v8

    goto :goto_d

    :cond_1c
    move v4, v12

    goto :goto_d

    :cond_1d
    move v4, v8

    move v8, v10

    goto/16 :goto_a

    :cond_1e
    move v13, v8

    move v10, v9

    move v9, v4

    goto :goto_c

    :cond_1f
    move v8, v9

    goto/16 :goto_7

    :cond_20
    move v7, v4

    goto/16 :goto_4

    :cond_21
    move v3, v9

    goto/16 :goto_3
.end method

.method public dCc(Landroid/support/v7/view/menu/p;Z)V
    .locals 0

    invoke-virtual {p0}, Landroid/support/v7/widget/ca;->dGd()Z

    invoke-super {p0, p1, p2}, Landroid/support/v7/view/menu/q;->dCc(Landroid/support/v7/view/menu/p;Z)V

    return-void
.end method

.method public dGd()Z
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/widget/ca;->dGq()Z

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/ca;->dGp()Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public dGe(Landroid/support/v7/view/menu/z;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->getActionView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->dLd()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/view/menu/q;->dGe(Landroid/support/v7/view/menu/z;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->isActionViewExpanded()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    check-cast p3, Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p3, v1}, Landroid/support/v7/widget/ActionMenuView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ActionMenuView$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    return-object v0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dGf()Z
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-boolean v0, p0, Landroid/support/v7/widget/ca;->enq:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/ca;->dGk()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->enl:Landroid/support/v7/widget/cm;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/p;->dKk()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/bc;

    iget-object v2, p0, Landroid/support/v7/widget/ca;->mContext:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    iget-object v4, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/bc;-><init>(Landroid/support/v7/widget/ca;Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/view/View;Z)V

    new-instance v1, Landroid/support/v7/widget/cm;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/widget/cm;-><init>(Landroid/support/v7/widget/ca;Landroid/support/v7/widget/bc;)V

    iput-object v1, p0, Landroid/support/v7/widget/ca;->enl:Landroid/support/v7/widget/cm;

    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/widget/ca;->enl:Landroid/support/v7/widget/cm;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    invoke-super {p0, v6}, Landroid/support/v7/view/menu/q;->dCa(Landroid/support/v7/view/menu/h;)Z

    return v5

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public dGh(ILandroid/support/v7/view/menu/z;)Z
    .locals 1

    invoke-virtual {p2}, Landroid/support/v7/view/menu/z;->dKZ()Z

    move-result v0

    return v0
.end method

.method public dGi(Landroid/support/v7/view/menu/z;Landroid/support/v7/view/menu/y;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/support/v7/view/menu/y;->dKU(Landroid/support/v7/view/menu/z;I)V

    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/support/v7/widget/ActionMenuView;

    check-cast p2, Landroid/support/v7/view/menu/ActionMenuItemView;

    invoke-virtual {p2, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setItemInvoker(Landroid/support/v7/view/menu/v;)V

    iget-object v0, p0, Landroid/support/v7/widget/ca;->ene:Landroid/support/v7/widget/bf;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/bf;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bf;-><init>(Landroid/support/v7/widget/ca;)V

    iput-object v0, p0, Landroid/support/v7/widget/ca;->ene:Landroid/support/v7/widget/bf;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ca;->ene:Landroid/support/v7/widget/bf;

    invoke-virtual {p2, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setPopupCallback(Landroid/support/v7/view/menu/M;)V

    return-void
.end method

.method public dGj(Z)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-super {p0, v0}, Landroid/support/v7/view/menu/q;->dCa(Landroid/support/v7/view/menu/h;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    goto :goto_0
.end method

.method public dGk()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eng:Landroid/support/v7/widget/bc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eng:Landroid/support/v7/widget/bc;

    invoke-virtual {v0}, Landroid/support/v7/widget/bc;->dLM()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dGm(Z)V
    .locals 1

    iput-boolean p1, p0, Landroid/support/v7/widget/ca;->enq:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ca;->emW:Z

    return-void
.end method

.method public dGo(Landroid/view/ViewGroup;I)Z
    .locals 2

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/view/menu/q;->dGo(Landroid/view/ViewGroup;I)Z

    move-result v0

    return v0
.end method

.method public dGp()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eni:Landroid/support/v7/widget/be;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eni:Landroid/support/v7/widget/be;

    invoke-virtual {v0}, Landroid/support/v7/widget/be;->dismiss()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public dGq()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->enl:Landroid/support/v7/widget/cm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/widget/ca;->enl:Landroid/support/v7/widget/cm;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iput-object v2, p0, Landroid/support/v7/widget/ca;->enl:Landroid/support/v7/widget/cm;

    return v3

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ca;->eng:Landroid/support/v7/widget/bc;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/view/menu/G;->dismiss()V

    return v3

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public dGr(Landroid/support/v7/widget/ActionMenuView;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/widget/ca;->etm:Landroid/support/v7/view/menu/d;

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ActionMenuView;->dFn(Landroid/support/v7/view/menu/p;)V

    return-void
.end method

.method public getOverflowIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    invoke-virtual {v0}, Landroid/support/v7/widget/aY;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/ca;->emZ:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/ca;->enn:Landroid/graphics/drawable/Drawable;

    return-object v0

    :cond_1
    return-object v1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    iget-boolean v0, p0, Landroid/support/v7/widget/ca;->enf:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v7/view/a;->dMb(Landroid/content/Context;)Landroid/support/v7/view/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/view/a;->dLY()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/ca;->enc:I

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/ca;->eti:Landroid/support/v7/view/menu/p;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    :cond_1
    return-void
.end method

.method public setExpandedActionViewsExclusive(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/widget/ca;->eno:Z

    return-void
.end method

.method public setOverflowIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ca;->enk:Landroid/support/v7/widget/aY;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aY;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ca;->emZ:Z

    iput-object p1, p0, Landroid/support/v7/widget/ca;->enn:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
