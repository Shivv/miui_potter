.class public abstract Landroid/support/v7/widget/b;
.super Ljava/lang/Object;
.source "RecyclerView.java"


# instance fields
.field private mHasStableIds:Z

.field private final mObservable:Landroid/support/v7/widget/r;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v7/widget/r;

    invoke-direct {v0}, Landroid/support/v7/widget/r;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/b;->mHasStableIds:Z

    return-void
.end method


# virtual methods
.method public final bindViewHolder(Landroid/support/v7/widget/p;I)V
    .locals 3

    const/4 v2, 0x1

    iput p2, p1, Landroid/support/v7/widget/p;->mPosition:I

    invoke-virtual {p0}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Landroid/support/v7/widget/b;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, p1, Landroid/support/v7/widget/p;->mItemId:J

    :cond_0
    const/16 v0, 0x207

    invoke-virtual {p1, v2, v0}, Landroid/support/v7/widget/p;->setFlags(II)V

    const-string/jumbo v0, "RV OnBindView"

    invoke-static {v0}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getUnmodifiedPayloads()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v7/widget/b;->onBindViewHolder(Landroid/support/v7/widget/p;ILjava/util/List;)V

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->clearPayload()V

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iput-boolean v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eae:Z

    :cond_1
    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    return-void
.end method

.method public final createViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;
    .locals 1

    const-string/jumbo v0, "RV CreateView"

    invoke-static {v0}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/b;->onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;

    move-result-object v0

    iput p2, v0, Landroid/support/v7/widget/p;->mItemViewType:I

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    return-object v0
.end method

.method public abstract getItemCount()I
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final hasObservers()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    invoke-virtual {v0}, Landroid/support/v7/widget/r;->hasObservers()Z

    move-result v0

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/b;->mHasStableIds:Z

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    invoke-virtual {v0}, Landroid/support/v7/widget/r;->notifyChanged()V

    return-void
.end method

.method public final notifyItemChanged(I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/r;->notifyItemRangeChanged(II)V

    return-void
.end method

.method public final notifyItemChanged(ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Landroid/support/v7/widget/r;->notifyItemRangeChanged(IILjava/lang/Object;)V

    return-void
.end method

.method public final notifyItemInserted(I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/r;->notifyItemRangeInserted(II)V

    return-void
.end method

.method public final notifyItemMoved(II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/r;->notifyItemMoved(II)V

    return-void
.end method

.method public final notifyItemRangeChanged(II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/r;->notifyItemRangeChanged(II)V

    return-void
.end method

.method public final notifyItemRangeChanged(IILjava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/r;->notifyItemRangeChanged(IILjava/lang/Object;)V

    return-void
.end method

.method public final notifyItemRangeInserted(II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/r;->notifyItemRangeInserted(II)V

    return-void
.end method

.method public final notifyItemRangeRemoved(II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/r;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method public final notifyItemRemoved(I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/r;->notifyItemRangeRemoved(II)V

    return-void
.end method

.method public onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    return-void
.end method

.method public abstract onBindViewHolder(Landroid/support/v7/widget/p;I)V
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/p;ILjava/util/List;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/b;->onBindViewHolder(Landroid/support/v7/widget/p;I)V

    return-void
.end method

.method public abstract onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;
.end method

.method public onDetachedFromRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    return-void
.end method

.method public onFailedToRecycleView(Landroid/support/v7/widget/p;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onViewAttachedToWindow(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public onViewRecycled(Landroid/support/v7/widget/p;)V
    .locals 0

    return-void
.end method

.method public registerAdapterDataObserver(Landroid/support/v7/widget/c;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/r;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public setHasStableIds(Z)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/widget/b;->hasObservers()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot change whether this adapter has stable IDs while the adapter has registered observers."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/b;->mHasStableIds:Z

    return-void
.end method

.method public unregisterAdapterDataObserver(Landroid/support/v7/widget/c;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/b;->mObservable:Landroid/support/v7/widget/r;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/r;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method
