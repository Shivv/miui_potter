.class Landroid/support/v7/widget/am;
.super Ljava/lang/Object;
.source "LinearLayoutManager.java"


# instance fields
.field ecL:I

.field ecM:Z

.field ecN:Ljava/util/List;

.field ecO:Z

.field ecP:I

.field ecQ:I

.field ecR:I

.field ecS:I

.field ecT:I

.field ecU:I

.field ecV:Z

.field ecW:I


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/am;->ecM:Z

    iput v1, p0, Landroid/support/v7/widget/am;->ecT:I

    iput-boolean v1, p0, Landroid/support/v7/widget/am;->ecO:Z

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    return-void
.end method

.method private dyS()Landroid/view/View;
    .locals 5

    iget-object v0, p0, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget-object v3, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duz()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v4, p0, Landroid/support/v7/widget/am;->ecQ:I

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duw()I

    move-result v0

    if-ne v4, v0, :cond_0

    invoke-virtual {p0, v3}, Landroid/support/v7/widget/am;->dyU(Landroid/view/View;)V

    return-object v3

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public dyP()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/am;->dyU(Landroid/view/View;)V

    return-void
.end method

.method dyQ(Landroid/support/v7/widget/j;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v7/widget/am;->dyS()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/am;->ecQ:I

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/j;->dtS(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/am;->ecQ:I

    iget v2, p0, Landroid/support/v7/widget/am;->ecW:I

    add-int/2addr v1, v2

    iput v1, p0, Landroid/support/v7/widget/am;->ecQ:I

    return-object v0
.end method

.method public dyR(Landroid/view/View;)Landroid/view/View;
    .locals 7

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v2, 0x0

    const v1, 0x7fffffff

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget-object v3, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eq v3, p1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duz()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    move v0, v1

    move-object v1, v2

    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duw()I

    move-result v0

    iget v6, p0, Landroid/support/v7/widget/am;->ecQ:I

    sub-int/2addr v0, v6

    iget v6, p0, Landroid/support/v7/widget/am;->ecW:I

    mul-int/2addr v0, v6

    if-gez v0, :cond_2

    move v0, v1

    move-object v1, v2

    goto :goto_1

    :cond_2
    if-ge v0, v1, :cond_4

    if-nez v0, :cond_3

    :goto_2
    return-object v3

    :cond_3
    move-object v1, v3

    goto :goto_1

    :cond_4
    move v0, v1

    move-object v1, v2

    goto :goto_1

    :cond_5
    move-object v3, v2

    goto :goto_2
.end method

.method dyT(Landroid/support/v7/widget/e;)Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/widget/am;->ecQ:I

    if-ltz v1, :cond_0

    iget v1, p0, Landroid/support/v7/widget/am;->ecQ:I

    invoke-virtual {p1}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public dyU(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/am;->dyR(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/am;->ecQ:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duw()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/am;->ecQ:I

    goto :goto_0
.end method
