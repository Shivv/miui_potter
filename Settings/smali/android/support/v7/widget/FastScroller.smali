.class Landroid/support/v7/widget/FastScroller;
.super Landroid/support/v7/widget/d;
.source "FastScroller.java"

# interfaces
.implements Landroid/support/v7/widget/l;


# static fields
.field private static final eqB:[I

.field private static final eqq:[I


# instance fields
.field private eqA:I

.field private final eqC:[I

.field private eqD:I

.field private final eqf:Landroid/graphics/drawable/StateListDrawable;

.field private final eqg:Ljava/lang/Runnable;

.field private final eqh:I

.field private eqi:Z

.field private eqj:Landroid/support/v7/widget/RecyclerView;

.field private final eqk:I

.field private final eql:Landroid/animation/ValueAnimator;

.field private final eqm:I

.field private eqn:I

.field private final eqo:Landroid/graphics/drawable/Drawable;

.field private final eqp:I

.field private final eqr:Landroid/support/v7/widget/m;

.field private final eqs:I

.field private final eqt:I

.field private equ:I

.field private final eqv:Landroid/graphics/drawable/StateListDrawable;

.field private eqw:Z

.field private final eqx:Landroid/graphics/drawable/Drawable;

.field private eqy:I

.field private final eqz:[I

.field mHorizontalDragX:F

.field mHorizontalThumbCenterX:I

.field mHorizontalThumbWidth:I

.field mVerticalDragY:F

.field mVerticalThumbCenterY:I

.field mVerticalThumbHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/FastScroller;->eqq:[I

    new-array v0, v2, [I

    sput-object v0, Landroid/support/v7/widget/FastScroller;->eqB:[I

    return-void
.end method

.method constructor <init>(Landroid/support/v7/widget/RecyclerView;Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;III)V
    .locals 6

    const/4 v5, 0x0

    const/16 v4, 0xff

    const/4 v1, 0x2

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/d;-><init>()V

    iput v3, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    iput v3, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    iput-boolean v3, p0, Landroid/support/v7/widget/FastScroller;->eqw:Z

    iput-boolean v3, p0, Landroid/support/v7/widget/FastScroller;->eqi:Z

    iput v3, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    iput v3, p0, Landroid/support/v7/widget/FastScroller;->eqn:I

    new-array v0, v1, [I

    iput-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqC:[I

    new-array v0, v1, [I

    iput-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqz:[I

    new-array v0, v1, [F

    const/4 v1, 0x0

    aput v1, v0, v3

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    aput v1, v0, v2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    iput v3, p0, Landroid/support/v7/widget/FastScroller;->eqA:I

    new-instance v0, Landroid/support/v7/widget/cO;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cO;-><init>(Landroid/support/v7/widget/FastScroller;)V

    iput-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqg:Ljava/lang/Runnable;

    new-instance v0, Landroid/support/v7/widget/cW;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cW;-><init>(Landroid/support/v7/widget/FastScroller;)V

    iput-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqr:Landroid/support/v7/widget/m;

    iput-object p2, p0, Landroid/support/v7/widget/FastScroller;->eqv:Landroid/graphics/drawable/StateListDrawable;

    iput-object p3, p0, Landroid/support/v7/widget/FastScroller;->eqx:Landroid/graphics/drawable/Drawable;

    iput-object p4, p0, Landroid/support/v7/widget/FastScroller;->eqf:Landroid/graphics/drawable/StateListDrawable;

    iput-object p5, p0, Landroid/support/v7/widget/FastScroller;->eqo:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v0

    invoke-static {p6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->eqp:I

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-static {p6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->eqm:I

    invoke-virtual {p4}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v0

    invoke-static {p6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->eqh:I

    invoke-virtual {p5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-static {p6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->eqt:I

    iput p7, p0, Landroid/support/v7/widget/FastScroller;->eqs:I

    iput p8, p0, Landroid/support/v7/widget/FastScroller;->eqk:I

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqv:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/StateListDrawable;->setAlpha(I)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqx:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/support/v7/widget/cZ;

    invoke-direct {v1, p0, v5}, Landroid/support/v7/widget/cZ;-><init>(Landroid/support/v7/widget/FastScroller;Landroid/support/v7/widget/cZ;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/support/v7/widget/df;

    invoke-direct {v1, p0, v5}, Landroid/support/v7/widget/df;-><init>(Landroid/support/v7/widget/FastScroller;Landroid/support/v7/widget/df;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/FastScroller;->dIa(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method private dIc()[I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqz:[I

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->eqk:I

    const/4 v2, 0x0

    aput v1, v0, v2

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqz:[I

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->eqk:I

    sub-int/2addr v1, v2

    const/4 v2, 0x1

    aput v1, v0, v2

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqz:[I

    return-object v0
.end method

.method private dId()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->doT(Landroid/support/v7/widget/d;)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->dqn(Landroid/support/v7/widget/l;)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eqr:Landroid/support/v7/widget/m;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->dpW(Landroid/support/v7/widget/m;)V

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIg()V

    return-void
.end method

.method private dIe(Landroid/graphics/Canvas;)V
    .locals 8

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->eqp:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbCenterY:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbHeight:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget-object v2, p0, Landroid/support/v7/widget/FastScroller;->eqv:Landroid/graphics/drawable/StateListDrawable;

    iget v3, p0, Landroid/support/v7/widget/FastScroller;->eqp:I

    iget v4, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbHeight:I

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    iget-object v2, p0, Landroid/support/v7/widget/FastScroller;->eqx:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Landroid/support/v7/widget/FastScroller;->eqm:I

    iget v4, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIk()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqx:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqp:I

    int-to-float v0, v0

    int-to-float v2, v1

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p1, v0, v6}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqv:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v6, v6}, Landroid/graphics/Canvas;->scale(FF)V

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqp:I

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_0
    return-void

    :cond_0
    int-to-float v2, v0

    invoke-virtual {p1, v2, v7}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Landroid/support/v7/widget/FastScroller;->eqx:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    int-to-float v2, v1

    invoke-virtual {p1, v7, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Landroid/support/v7/widget/FastScroller;->eqv:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0
.end method

.method private dIf(F)V
    .locals 8

    const/4 v7, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIc()[I

    move-result-object v3

    aget v0, v3, v7

    int-to-float v0, v0

    const/4 v1, 0x1

    aget v1, v3, v1

    int-to-float v1, v1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbCenterX:I

    int-to-float v0, v0

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    return-void

    :cond_0
    iget v1, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalDragX:F

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->computeHorizontalScrollRange()I

    move-result v4

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->computeHorizontalScrollOffset()I

    move-result v5

    iget v6, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/FastScroller;->dIs(FF[IIII)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0, v7}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    :cond_1
    iput v2, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalDragX:F

    return-void
.end method

.method private dIg()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eqg:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic dIh(Landroid/support/v7/widget/FastScroller;)Landroid/graphics/drawable/StateListDrawable;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqv:Landroid/graphics/drawable/StateListDrawable;

    return-object v0
.end method

.method static synthetic dIi(Landroid/support/v7/widget/FastScroller;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqx:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private dIj()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    return-void
.end method

.method private dIk()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private dIl(I)V
    .locals 3

    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqv:Landroid/graphics/drawable/StateListDrawable;

    sget-object v1, Landroid/support/v7/widget/FastScroller;->eqq:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIg()V

    :cond_0
    if-nez p1, :cond_2

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIj()V

    :goto_0
    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-ne v0, v2, :cond_3

    if-eq p1, v2, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqv:Landroid/graphics/drawable/StateListDrawable;

    sget-object v1, Landroid/support/v7/widget/FastScroller;->eqB:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    const/16 v0, 0x4b0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/FastScroller;->dIv(I)V

    :cond_1
    :goto_1
    iput p1, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/FastScroller;->show()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const/16 v0, 0x5dc

    invoke-direct {p0, v0}, Landroid/support/v7/widget/FastScroller;->dIv(I)V

    goto :goto_1
.end method

.method private dIm()[I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqC:[I

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->eqk:I

    const/4 v2, 0x0

    aput v1, v0, v2

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqC:[I

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->eqk:I

    sub-int/2addr v1, v2

    const/4 v2, 0x1

    aput v1, v0, v2

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqC:[I

    return-object v0
.end method

.method private dIn(F)V
    .locals 8

    const/4 v7, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIm()[I

    move-result-object v3

    aget v0, v3, v7

    int-to-float v0, v0

    const/4 v1, 0x1

    aget v1, v3, v1

    int-to-float v1, v1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbCenterY:I

    int-to-float v0, v0

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    return-void

    :cond_0
    iget v1, p0, Landroid/support/v7/widget/FastScroller;->mVerticalDragY:F

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollRange()I

    move-result v4

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollOffset()I

    move-result v5

    iget v6, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/FastScroller;->dIs(FF[IIII)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v7, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    :cond_1
    iput v2, p0, Landroid/support/v7/widget/FastScroller;->mVerticalDragY:F

    return-void
.end method

.method static synthetic dIo(Landroid/support/v7/widget/FastScroller;I)I
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/FastScroller;->eqA:I

    return p1
.end method

.method static synthetic dIp(Landroid/support/v7/widget/FastScroller;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIj()V

    return-void
.end method

.method static synthetic dIq(Landroid/support/v7/widget/FastScroller;)Landroid/animation/ValueAnimator;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method private dIr()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->dpM(Landroid/support/v7/widget/d;)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->doU(Landroid/support/v7/widget/l;)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eqr:Landroid/support/v7/widget/m;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->doX(Landroid/support/v7/widget/m;)V

    return-void
.end method

.method private dIs(FF[IIII)I
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    aget v0, p3, v0

    aget v1, p3, v3

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    return v3

    :cond_0
    sub-float v1, p2, p1

    int-to-float v0, v0

    div-float v0, v1, v0

    sub-int v1, p4, p6

    int-to-float v2, v1

    mul-float/2addr v0, v2

    float-to-int v0, v0

    add-int v2, p5, v0

    if-ge v2, v1, :cond_1

    if-ltz v2, :cond_1

    return v0

    :cond_1
    return v3
.end method

.method static synthetic dIt(Landroid/support/v7/widget/FastScroller;I)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/FastScroller;->dIl(I)V

    return-void
.end method

.method private dIu(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->eqh:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbCenterX:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbWidth:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget-object v2, p0, Landroid/support/v7/widget/FastScroller;->eqf:Landroid/graphics/drawable/StateListDrawable;

    iget v3, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbWidth:I

    iget v4, p0, Landroid/support/v7/widget/FastScroller;->eqh:I

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    iget-object v2, p0, Landroid/support/v7/widget/FastScroller;->eqo:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    iget v4, p0, Landroid/support/v7/widget/FastScroller;->eqt:I

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    int-to-float v2, v0

    invoke-virtual {p1, v6, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Landroid/support/v7/widget/FastScroller;->eqo:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    int-to-float v2, v1

    invoke-virtual {p1, v2, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Landroid/support/v7/widget/FastScroller;->eqf:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method private dIv(I)V
    .locals 4

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIg()V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eqg:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/RecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public Cm(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    invoke-direct {p0, v2}, Landroid/support/v7/widget/FastScroller;->dIl(I)V

    return-void

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqA:I

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v7/widget/FastScroller;->eqw:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Landroid/support/v7/widget/FastScroller;->dIe(Landroid/graphics/Canvas;)V

    :cond_2
    iget-boolean v0, p0, Landroid/support/v7/widget/FastScroller;->eqi:Z

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Landroid/support/v7/widget/FastScroller;->dIu(Landroid/graphics/Canvas;)V

    :cond_3
    return-void
.end method

.method public dIa(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dId()V

    :cond_1
    iput-object p1, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIr()V

    :cond_2
    return-void
.end method

.method dIb(II)V
    .locals 9

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollRange()I

    move-result v3

    iget v4, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    sub-int v0, v3, v4

    if-lez v0, :cond_2

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    iget v5, p0, Landroid/support/v7/widget/FastScroller;->eqs:I

    if-lt v0, v5, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/FastScroller;->eqw:Z

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqj:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->computeHorizontalScrollRange()I

    move-result v5

    iget v6, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    sub-int v0, v5, v6

    if-lez v0, :cond_4

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    iget v7, p0, Landroid/support/v7/widget/FastScroller;->eqs:I

    if-lt v0, v7, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Landroid/support/v7/widget/FastScroller;->eqi:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/FastScroller;->eqw:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Landroid/support/v7/widget/FastScroller;->eqi:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Landroid/support/v7/widget/FastScroller;->dIl(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Landroid/support/v7/widget/FastScroller;->eqw:Z

    if-eqz v0, :cond_6

    int-to-float v0, p2

    int-to-float v2, v4

    div-float/2addr v2, v8

    add-float/2addr v0, v2

    int-to-float v2, v4

    mul-float/2addr v0, v2

    int-to-float v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbCenterY:I

    mul-int v0, v4, v4

    div-int/2addr v0, v3

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbHeight:I

    :cond_6
    iget-boolean v0, p0, Landroid/support/v7/widget/FastScroller;->eqi:Z

    if-eqz v0, :cond_7

    int-to-float v0, p1

    int-to-float v2, v6

    div-float/2addr v2, v8

    add-float/2addr v0, v2

    int-to-float v2, v6

    mul-float/2addr v0, v2

    int-to-float v2, v5

    div-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbCenterX:I

    mul-int v0, v6, v6

    div-int/2addr v0, v5

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbWidth:I

    :cond_7
    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-eqz v0, :cond_8

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-ne v0, v1, :cond_9

    :cond_8
    invoke-direct {p0, v1}, Landroid/support/v7/widget/FastScroller;->dIl(I)V

    :cond_9
    return-void
.end method

.method public dop(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-ne v2, v0, :cond_5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v2, v3}, Landroid/support/v7/widget/FastScroller;->isPointInsideVerticalThumb(FF)Z

    move-result v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p0, v3, v4}, Landroid/support/v7/widget/FastScroller;->isPointInsideHorizontalThumb(FF)Z

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_4

    if-nez v2, :cond_0

    if-eqz v3, :cond_4

    :cond_0
    if-eqz v3, :cond_3

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->eqn:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalDragX:F

    :cond_1
    :goto_0
    invoke-direct {p0, v5}, Landroid/support/v7/widget/FastScroller;->dIl(I)V

    :cond_2
    :goto_1
    return v0

    :cond_3
    if-eqz v2, :cond_1

    iput v5, p0, Landroid/support/v7/widget/FastScroller;->eqn:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/v7/widget/FastScroller;->mVerticalDragY:F

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    iget v2, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-eq v2, v5, :cond_2

    move v0, v1

    goto :goto_1
.end method

.method public doq(Z)V
    .locals 0

    return-void
.end method

.method public dor(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x2

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/FastScroller;->isPointInsideVerticalThumb(FF)Z

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/FastScroller;->isPointInsideHorizontalThumb(FF)Z

    move-result v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_3

    :cond_1
    if-eqz v1, :cond_4

    iput v4, p0, Landroid/support/v7/widget/FastScroller;->eqn:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalDragX:F

    :cond_2
    :goto_0
    invoke-direct {p0, v3}, Landroid/support/v7/widget/FastScroller;->dIl(I)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    if-eqz v0, :cond_2

    iput v3, p0, Landroid/support/v7/widget/FastScroller;->eqn:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->mVerticalDragY:F

    goto :goto_0

    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v4, :cond_6

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-ne v0, v3, :cond_6

    iput v2, p0, Landroid/support/v7/widget/FastScroller;->mVerticalDragY:F

    iput v2, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalDragX:F

    invoke-direct {p0, v4}, Landroid/support/v7/widget/FastScroller;->dIl(I)V

    iput v1, p0, Landroid/support/v7/widget/FastScroller;->eqn:I

    goto :goto_1

    :cond_6
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_3

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-ne v0, v3, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/widget/FastScroller;->show()V

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqn:I

    if-ne v0, v4, :cond_7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/FastScroller;->dIf(F)V

    :cond_7
    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqn:I

    if-ne v0, v3, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/FastScroller;->dIn(F)V

    goto :goto_1
.end method

.method getHorizontalThumbDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqf:Landroid/graphics/drawable/StateListDrawable;

    return-object v0
.end method

.method getHorizontalTrackDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqo:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method getVerticalThumbDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqv:Landroid/graphics/drawable/StateListDrawable;

    return-object v0
.end method

.method getVerticalTrackDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eqx:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method hide(I)V
    .locals 4

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqA:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :pswitch_1
    const/4 v0, 0x3

    iput v0, p0, Landroid/support/v7/widget/FastScroller;->eqA:I

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    const/4 v0, 0x2

    new-array v2, v0, [F

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v3, 0x0

    aput v0, v2, v3

    const/4 v0, 0x0

    const/4 v3, 0x1

    aput v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method isHidden()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method isPointInsideHorizontalThumb(FF)Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->eqD:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->eqh:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_0

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbCenterX:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbWidth:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_0

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbCenterX:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->mHorizontalThumbWidth:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method isPointInsideVerticalThumb(FF)Z
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/FastScroller;->dIk()Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->eqp:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    :goto_0
    iget v1, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbCenterY:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbHeight:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_0

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbCenterY:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->mVerticalThumbHeight:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v1, p2, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget v1, p0, Landroid/support/v7/widget/FastScroller;->equ:I

    iget v2, p0, Landroid/support/v7/widget/FastScroller;->eqp:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_0

    goto :goto_0
.end method

.method isVisible()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Landroid/support/v7/widget/FastScroller;->eqy:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public show()V
    .locals 5

    const/4 v4, 0x1

    iget v0, p0, Landroid/support/v7/widget/FastScroller;->eqA:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :pswitch_2
    iput v4, p0, Landroid/support/v7/widget/FastScroller;->eqA:I

    iget-object v1, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    const/4 v0, 0x2

    new-array v2, v0, [F

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v3, 0x0

    aput v0, v2, v3

    const/high16 v0, 0x3f800000    # 1.0f

    aput v0, v2, v4

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object v0, p0, Landroid/support/v7/widget/FastScroller;->eql:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
