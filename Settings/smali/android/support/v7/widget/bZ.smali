.class Landroid/support/v7/widget/bZ;
.super Ljava/lang/Object;
.source "ActionMenuPresenter.java"

# interfaces
.implements Landroid/support/v7/view/menu/c;


# instance fields
.field final synthetic emV:Landroid/support/v7/widget/ca;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/ca;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/bZ;->emV:Landroid/support/v7/widget/ca;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dCC(Landroid/support/v7/view/menu/p;)Z
    .locals 3

    const/4 v1, 0x0

    if-nez p1, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/bZ;->emV:Landroid/support/v7/widget/ca;

    move-object v0, p1

    check-cast v0, Landroid/support/v7/view/menu/h;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/h;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iput v0, v2, Landroid/support/v7/widget/ca;->enp:I

    iget-object v0, p0, Landroid/support/v7/widget/bZ;->emV:Landroid/support/v7/widget/ca;

    invoke-virtual {v0}, Landroid/support/v7/widget/ca;->dKK()Landroid/support/v7/view/menu/c;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Landroid/support/v7/view/menu/c;->dCC(Landroid/support/v7/view/menu/p;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public dCD(Landroid/support/v7/view/menu/p;Z)V
    .locals 2

    instance-of v0, p1, Landroid/support/v7/view/menu/h;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/view/menu/p;->dJK()Landroid/support/v7/view/menu/p;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bZ;->emV:Landroid/support/v7/widget/ca;

    invoke-virtual {v0}, Landroid/support/v7/widget/ca;->dKK()Landroid/support/v7/view/menu/c;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2}, Landroid/support/v7/view/menu/c;->dCD(Landroid/support/v7/view/menu/p;Z)V

    :cond_1
    return-void
.end method
