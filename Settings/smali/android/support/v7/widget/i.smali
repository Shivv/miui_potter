.class public Landroid/support/v7/widget/i;
.super Ljava/lang/Object;
.source "RecyclerView.java"


# instance fields
.field private dZT:I

.field dZU:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/i;->dZU:Landroid/util/SparseArray;

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/i;->dZT:I

    return-void
.end method

.method private dtJ(I)Landroid/support/v7/widget/L;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/i;->dZU:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/L;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/L;

    invoke-direct {v0}, Landroid/support/v7/widget/L;-><init>()V

    iget-object v1, p0, Landroid/support/v7/widget/i;->dZU:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/i;->dZU:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/i;->dZU:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/L;

    iget-object v0, v0, Landroid/support/v7/widget/L;->ebf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method dtC(IJ)V
    .locals 4

    invoke-direct {p0, p1}, Landroid/support/v7/widget/i;->dtJ(I)Landroid/support/v7/widget/L;

    move-result-object v0

    iget-wide v2, v0, Landroid/support/v7/widget/L;->ebd:J

    invoke-virtual {p0, v2, v3, p2, p3}, Landroid/support/v7/widget/i;->dtM(JJ)J

    move-result-wide v2

    iput-wide v2, v0, Landroid/support/v7/widget/L;->ebd:J

    return-void
.end method

.method dtD(Landroid/support/v7/widget/b;)V
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/i;->dZT:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/i;->dZT:I

    return-void
.end method

.method dtE(IJJ)Z
    .locals 6

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/i;->dtJ(I)Landroid/support/v7/widget/L;

    move-result-object v1

    iget-wide v2, v1, Landroid/support/v7/widget/L;->ebe:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    add-long/2addr v2, p2

    cmp-long v1, v2, p4

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method dtF(IJ)V
    .locals 4

    invoke-direct {p0, p1}, Landroid/support/v7/widget/i;->dtJ(I)Landroid/support/v7/widget/L;

    move-result-object v0

    iget-wide v2, v0, Landroid/support/v7/widget/L;->ebe:J

    invoke-virtual {p0, v2, v3, p2, p3}, Landroid/support/v7/widget/i;->dtM(JJ)J

    move-result-wide v2

    iput-wide v2, v0, Landroid/support/v7/widget/L;->ebe:J

    return-void
.end method

.method public dtG(Landroid/support/v7/widget/p;)V
    .locals 3

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/i;->dtJ(I)Landroid/support/v7/widget/L;

    move-result-object v1

    iget-object v1, v1, Landroid/support/v7/widget/L;->ebf:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v7/widget/i;->dZU:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/L;

    iget v0, v0, Landroid/support/v7/widget/L;->ebg:I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v0, v2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/widget/p;->resetInternal()V

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method dtH(IJJ)Z
    .locals 6

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/i;->dtJ(I)Landroid/support/v7/widget/L;

    move-result-object v1

    iget-wide v2, v1, Landroid/support/v7/widget/L;->ebd:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    add-long/2addr v2, p2

    cmp-long v1, v2, p4

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method dtI()V
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/i;->dZT:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/i;->dZT:I

    return-void
.end method

.method public dtK(I)Landroid/support/v7/widget/p;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/i;->dZU:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/L;

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/support/v7/widget/L;->ebf:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/L;->ebf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    return-object v0

    :cond_0
    return-object v2
.end method

.method dtL(Landroid/support/v7/widget/b;Landroid/support/v7/widget/b;Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/i;->dtI()V

    :cond_0
    if-nez p3, :cond_1

    iget v0, p0, Landroid/support/v7/widget/i;->dZT:I

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/i;->clear()V

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p0, p2}, Landroid/support/v7/widget/i;->dtD(Landroid/support/v7/widget/b;)V

    :cond_2
    return-void
.end method

.method dtM(JJ)J
    .locals 7

    const-wide/16 v4, 0x4

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    return-wide p3

    :cond_0
    div-long v0, p1, v4

    const-wide/16 v2, 0x3

    mul-long/2addr v0, v2

    div-long v2, p3, v4

    add-long/2addr v0, v2

    return-wide v0
.end method
