.class public Landroid/support/v7/widget/aP;
.super Ljava/lang/Object;
.source "AppCompatImageHelper.java"


# instance fields
.field private efY:Landroid/support/v7/widget/cw;

.field private efZ:Landroid/support/v7/widget/cw;

.field private final ega:Landroid/widget/ImageView;

.field private egb:Landroid/support/v7/widget/cw;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    return-void
.end method

.method private dBw()Z
    .locals 4

    const/16 v3, 0x15

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v2, v3, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/aP;->efY:Landroid/support/v7/widget/cw;

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    if-ne v2, v3, :cond_2

    return v0

    :cond_2
    return v1
.end method

.method private dBx(Landroid/graphics/drawable/Drawable;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/aP;->efZ:Landroid/support/v7/widget/cw;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/cw;

    invoke-direct {v0}, Landroid/support/v7/widget/cw;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/aP;->efZ:Landroid/support/v7/widget/cw;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aP;->efZ:Landroid/support/v7/widget/cw;

    invoke-virtual {v0}, Landroid/support/v7/widget/cw;->clear()V

    iget-object v1, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-static {v1}, Landroid/support/v4/widget/g;->dTP(Landroid/widget/ImageView;)Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_1

    iput-boolean v2, v0, Landroid/support/v7/widget/cw;->eoN:Z

    iput-object v1, v0, Landroid/support/v7/widget/cw;->eoO:Landroid/content/res/ColorStateList;

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-static {v1}, Landroid/support/v4/widget/g;->dTS(Landroid/widget/ImageView;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v1

    if-eqz v1, :cond_2

    iput-boolean v2, v0, Landroid/support/v7/widget/cw;->eoP:Z

    iput-object v1, v0, Landroid/support/v7/widget/cw;->eoQ:Landroid/graphics/PorterDuff$Mode;

    :cond_2
    iget-boolean v1, v0, Landroid/support/v7/widget/cw;->eoN:Z

    if-nez v1, :cond_3

    iget-boolean v1, v0, Landroid/support/v7/widget/cw;->eoP:Z

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawableState()[I

    move-result-object v1

    invoke-static {p1, v0, v1}, Landroid/support/v7/widget/bF;->dDJ(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;[I)V

    return v2

    :cond_4
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public dBu(Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v4, -0x1

    iget-object v0, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/b/j;->dRE:[I

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, p2, v2}, Landroid/support/v7/widget/bS;->dFw(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/bS;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    sget v2, Landroid/support/v7/b/j;->dRF:I

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/bS;->dFu(II)I

    move-result v2

    if-eq v2, v4, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/support/v7/f/a/d;->dMC(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/support/v7/widget/cx;->dHu(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    sget v0, Landroid/support/v7/b/j;->dRG:I

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    sget v2, Landroid/support/v7/b/j;->dRG:I

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/widget/g;->dTQ(Landroid/widget/ImageView;Landroid/content/res/ColorStateList;)V

    :cond_2
    sget v0, Landroid/support/v7/b/j;->dRH:I

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    sget v2, Landroid/support/v7/b/j;->dRH:I

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/bS;->dFr(II)I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/support/v7/widget/cx;->dHt(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/widget/g;->dTR(Landroid/widget/ImageView;Landroid/graphics/PorterDuff$Mode;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-virtual {v1}, Landroid/support/v7/widget/bS;->dFB()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/support/v7/widget/bS;->dFB()V

    throw v0
.end method

.method dBv()V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/support/v7/widget/cx;->dHu(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz v0, :cond_2

    invoke-direct {p0}, Landroid/support/v7/widget/aP;->dBw()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Landroid/support/v7/widget/aP;->dBx(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    iget-object v2, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawableState()[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/bF;->dDJ(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;[I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/aP;->efY:Landroid/support/v7/widget/cw;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/aP;->efY:Landroid/support/v7/widget/cw;

    iget-object v2, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawableState()[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/bF;->dDJ(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;[I)V

    goto :goto_0
.end method

.method getSupportImageTintList()Landroid/content/res/ColorStateList;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    iget-object v0, v0, Landroid/support/v7/widget/cw;->eoO:Landroid/content/res/ColorStateList;

    :cond_0
    return-object v0
.end method

.method getSupportImageTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    iget-object v0, v0, Landroid/support/v7/widget/cw;->eoQ:Landroid/graphics/PorterDuff$Mode;

    :cond_0
    return-object v0
.end method

.method hasOverlappingRendering()Z
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    instance-of v0, v0, Landroid/graphics/drawable/RippleDrawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public setImageResource(I)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v7/f/a/d;->dMC(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/support/v7/widget/cx;->dHu(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/aP;->dBv()V

    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/aP;->ega:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method setSupportImageTintList(Landroid/content/res/ColorStateList;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/cw;

    invoke-direct {v0}, Landroid/support/v7/widget/cw;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    iput-object p1, v0, Landroid/support/v7/widget/cw;->eoO:Landroid/content/res/ColorStateList;

    iget-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/cw;->eoN:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/aP;->dBv()V

    return-void
.end method

.method setSupportImageTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/cw;

    invoke-direct {v0}, Landroid/support/v7/widget/cw;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    iput-object p1, v0, Landroid/support/v7/widget/cw;->eoQ:Landroid/graphics/PorterDuff$Mode;

    iget-object v0, p0, Landroid/support/v7/widget/aP;->egb:Landroid/support/v7/widget/cw;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/cw;->eoP:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/aP;->dBv()V

    return-void
.end method
