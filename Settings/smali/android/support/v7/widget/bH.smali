.class Landroid/support/v7/widget/bH;
.super Ljava/lang/Object;
.source "ThemeUtils.java"


# static fields
.field private static final ejk:Ljava/lang/ThreadLocal;

.field static final ejl:[I

.field static final ejm:[I

.field static final ejn:[I

.field static final ejo:[I

.field static final ejp:[I

.field static final ejq:[I

.field static final ejr:[I

.field static final ejs:[I

.field private static final ejt:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Landroid/support/v7/widget/bH;->ejk:Ljava/lang/ThreadLocal;

    new-array v0, v3, [I

    const v1, -0x101009e

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/bH;->ejm:[I

    new-array v0, v3, [I

    const v1, 0x101009c

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/bH;->ejs:[I

    new-array v0, v3, [I

    const v1, 0x10102fe

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/bH;->ejr:[I

    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/bH;->ejl:[I

    new-array v0, v3, [I

    const v1, 0x10100a0

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/bH;->ejo:[I

    new-array v0, v3, [I

    const v1, 0x10100a1

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/bH;->ejn:[I

    const v0, -0x10100a7

    const v1, -0x101009c

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/bH;->ejq:[I

    new-array v0, v2, [I

    sput-object v0, Landroid/support/v7/widget/bH;->ejp:[I

    new-array v0, v3, [I

    sput-object v0, Landroid/support/v7/widget/bH;->ejt:[I

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static dDT(Landroid/content/Context;IF)I
    .locals 2

    invoke-static {p0, p1}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/c/c;->eml(II)I

    move-result v0

    return v0
.end method

.method public static dDU(Landroid/content/Context;I)I
    .locals 4

    invoke-static {p0, p1}, Landroid/support/v7/widget/bH;->dDV(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/support/v7/widget/bH;->ejm:[I

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    return v0

    :cond_0
    invoke-static {}, Landroid/support/v7/widget/bH;->dDX()Landroid/util/TypedValue;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010033

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    invoke-static {p0, p1, v0}, Landroid/support/v7/widget/bH;->dDT(Landroid/content/Context;IF)I

    move-result v0

    return v0
.end method

.method public static dDV(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Landroid/support/v7/widget/bH;->ejt:[I

    aput p1, v0, v1

    sget-object v0, Landroid/support/v7/widget/bH;->ejt:[I

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Landroid/support/v7/widget/bS;->dFy(Landroid/content/Context;Landroid/util/AttributeSet;[I)Landroid/support/v7/widget/bS;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v7/widget/bS;->dFB()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/support/v7/widget/bS;->dFB()V

    throw v1
.end method

.method public static dDW(Landroid/content/Context;I)I
    .locals 3

    const/4 v1, 0x0

    sget-object v0, Landroid/support/v7/widget/bH;->ejt:[I

    aput p1, v0, v1

    sget-object v0, Landroid/support/v7/widget/bH;->ejt:[I

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Landroid/support/v7/widget/bS;->dFy(Landroid/content/Context;Landroid/util/AttributeSet;[I)Landroid/support/v7/widget/bS;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bS;->dFG(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    invoke-virtual {v0}, Landroid/support/v7/widget/bS;->dFB()V

    return v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/support/v7/widget/bS;->dFB()V

    throw v1
.end method

.method private static dDX()Landroid/util/TypedValue;
    .locals 2

    sget-object v0, Landroid/support/v7/widget/bH;->ejk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/TypedValue;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    sget-object v1, Landroid/support/v7/widget/bH;->ejk:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    return-object v0
.end method
