.class public Landroid/support/v7/widget/al;
.super Landroid/support/v7/widget/a;
.source "LinearLayoutManager.java"

# interfaces
.implements Landroid/support/v7/widget/a/a;


# instance fields
.field private ecA:Z

.field private ecB:Z

.field ecC:Z

.field ecD:Landroid/support/v7/widget/ap;

.field ecE:I

.field final ecF:Landroid/support/v7/widget/an;

.field private ecG:Z

.field private ecH:Landroid/support/v7/widget/am;

.field private ecI:I

.field ecJ:I

.field private ecK:Z

.field ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

.field private ecx:Z

.field ecy:I

.field private final ecz:Landroid/support/v7/widget/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/al;-><init>(Landroid/content/Context;IZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/a;-><init>()V

    iput-boolean v0, p0, Landroid/support/v7/widget/al;->ecK:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/al;->ecG:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/al;->ecx:Z

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/al;->ecE:I

    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/al;->ecJ:I

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    new-instance v0, Landroid/support/v7/widget/an;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/an;-><init>(Landroid/support/v7/widget/al;)V

    iput-object v0, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    new-instance v0, Landroid/support/v7/widget/ao;

    invoke-direct {v0}, Landroid/support/v7/widget/ao;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/al;->ecz:Landroid/support/v7/widget/ao;

    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v7/widget/al;->ecI:I

    invoke-virtual {p0, p2}, Landroid/support/v7/widget/al;->setOrientation(I)V

    invoke-virtual {p0, p3}, Landroid/support/v7/widget/al;->dyN(Z)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->drH(Z)V

    return-void
.end method

.method private dxS(Landroid/support/v7/widget/e;)I
    .locals 6

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecx:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/al;->dyI(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecx:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/al;->dyb(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/al;->ecx:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/cP;->dIw(Landroid/support/v7/widget/e;Landroid/support/v7/widget/ap;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/a;Z)I

    move-result v0

    return v0
.end method

.method private dxT(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;Z)I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v0

    sub-int/2addr v0, p1

    if-lez v0, :cond_0

    neg-int v0, v0

    invoke-virtual {p0, v0, p2, p3}, Landroid/support/v7/widget/al;->dxR(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v0

    neg-int v0, v0

    add-int v1, p1, v0

    if-eqz p4, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v2

    sub-int v1, v2, v1

    if-lez v1, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ap;->dzs(I)V

    add-int/2addr v0, v1

    return v0

    :cond_0
    return v1

    :cond_1
    return v0
.end method

.method private dxU(II)V
    .locals 3

    const/4 v1, -0x1

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v2

    sub-int v2, p2, v2

    iput v2, v0, Landroid/support/v7/widget/am;->ecP:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput p1, v0, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v2, Landroid/support/v7/widget/am;->ecW:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v1, v0, Landroid/support/v7/widget/am;->ecR:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput p2, v0, Landroid/support/v7/widget/am;->ecS:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/am;->ecU:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private dxW()Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private dxX(IIZLandroid/support/v7/widget/e;)V
    .locals 6

    const/4 v0, -0x1

    const/4 v1, 0x1

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyj()Z

    move-result v3

    iput-boolean v3, v2, Landroid/support/v7/widget/am;->ecV:Z

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p4}, Landroid/support/v7/widget/al;->dyr(Landroid/support/v7/widget/e;)I

    move-result v3

    iput v3, v2, Landroid/support/v7/widget/am;->ecT:I

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput p1, v2, Landroid/support/v7/widget/am;->ecR:I

    if-ne p1, v1, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v3, v2, Landroid/support/v7/widget/am;->ecT:I

    iget-object v4, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v4}, Landroid/support/v7/widget/ap;->dzn()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/support/v7/widget/am;->ecT:I

    invoke-direct {p0}, Landroid/support/v7/widget/al;->dxW()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget-boolean v4, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v4, :cond_1

    :goto_0
    iput v0, v3, Landroid/support/v7/widget/am;->ecW:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v3, v3, Landroid/support/v7/widget/am;->ecW:I

    add-int/2addr v1, v3

    iput v1, v0, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/am;->ecS:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput p2, v1, Landroid/support/v7/widget/am;->ecP:I

    if-eqz p3, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v2, v1, Landroid/support/v7/widget/am;->ecP:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/support/v7/widget/am;->ecP:I

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v0, v1, Landroid/support/v7/widget/am;->ecU:I

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Landroid/support/v7/widget/al;->dyi()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v4, v3, Landroid/support/v7/widget/am;->ecT:I

    iget-object v5, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v5}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v3, Landroid/support/v7/widget/am;->ecT:I

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget-boolean v4, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v4, :cond_3

    :goto_2
    iput v1, v3, Landroid/support/v7/widget/am;->ecW:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v3, v3, Landroid/support/v7/widget/am;->ecW:I

    add-int/2addr v1, v3

    iput v1, v0, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/am;->ecS:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_2
.end method

.method private dxZ(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyo(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyg(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private dyA(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;II)V
    .locals 9

    invoke-virtual {p2}, Landroid/support/v7/widget/e;->dtl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dss()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/support/v7/widget/j;->dup()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v7

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_6

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v1

    if-eqz v1, :cond_2

    move v0, v2

    move v1, v3

    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v1

    if-ge v1, v7, :cond_3

    const/4 v1, 0x1

    :goto_2
    iget-boolean v8, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eq v1, v8, :cond_4

    const/4 v1, -0x1

    :goto_3
    const/4 v8, -0x1

    if-ne v1, v8, :cond_5

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/ap;->dzg(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v3

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x1

    goto :goto_3

    :cond_5
    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/ap;->dzg(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v2

    move v1, v3

    goto :goto_1

    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput-object v5, v0, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    if-lez v3, :cond_7

    invoke-direct {p0}, Landroid/support/v7/widget/al;->dyi()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0, p3}, Landroid/support/v7/widget/al;->dxU(II)V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v3, v0, Landroid/support/v7/widget/am;->ecT:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/am;->ecP:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {v0}, Landroid/support/v7/widget/am;->dyP()V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    :cond_7
    if-lez v2, :cond_8

    invoke-direct {p0}, Landroid/support/v7/widget/al;->dxW()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0, p4}, Landroid/support/v7/widget/al;->dyq(II)V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v2, v0, Landroid/support/v7/widget/am;->ecT:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/am;->ecP:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {v0}, Landroid/support/v7/widget/am;->dyP()V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    return-void
.end method

.method private dyD(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/al;->dyF(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private dyE(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v7/widget/an;)V
    .locals 1

    invoke-direct {p0, p2, p3}, Landroid/support/v7/widget/al;->dyn(Landroid/support/v7/widget/e;Landroid/support/v7/widget/an;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/al;->dyd(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v7/widget/an;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {p3}, Landroid/support/v7/widget/an;->dyX()V

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecG:Z

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iput v0, p3, Landroid/support/v7/widget/an;->mPosition:I

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private dyG(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyD(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyc(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private dyH(Landroid/support/v7/widget/j;I)V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v2

    if-gez p2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzk()I

    move-result v0

    sub-int v3, v0, p2

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_6

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v5, v4}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v5

    if-lt v5, v3, :cond_1

    iget-object v5, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v5, v4}, Landroid/support/v7/widget/ap;->dzc(Landroid/view/View;)I

    move-result v4

    if-ge v4, v3, :cond_2

    :cond_1
    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/al;->dyt(Landroid/support/v7/widget/j;II)V

    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v2, -0x1

    :goto_1
    if-ltz v0, :cond_6

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v4

    if-lt v4, v3, :cond_4

    iget-object v4, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/ap;->dzc(Landroid/view/View;)I

    move-result v1

    if-ge v1, v3, :cond_5

    :cond_4
    add-int/lit8 v1, v2, -0x1

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/al;->dyt(Landroid/support/v7/widget/j;II)V

    return-void

    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_6
    return-void
.end method

.method private dyI(ZZ)Landroid/view/View;
    .locals 2

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/al;->dyp(IIZZ)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0, p1, p2}, Landroid/support/v7/widget/al;->dyp(IIZZ)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private dyJ(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyg(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyo(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private dyK(Landroid/support/v7/widget/j;I)V
    .locals 5

    const/4 v1, 0x0

    if-gez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v2

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_3

    add-int/lit8 v0, v2, -0x1

    :goto_0
    if-ltz v0, :cond_6

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v3

    if-gt v3, p2, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/ap;->dzq(Landroid/view/View;)I

    move-result v1

    if-le v1, p2, :cond_2

    :cond_1
    add-int/lit8 v1, v2, -0x1

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/al;->dyt(Landroid/support/v7/widget/j;II)V

    return-void

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_6

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v4

    if-gt v4, p2, :cond_4

    iget-object v4, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/ap;->dzq(Landroid/view/View;)I

    move-result v3

    if-le v3, p2, :cond_5

    :cond_4
    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/al;->dyt(Landroid/support/v7/widget/j;II)V

    return-void

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    return-void
.end method

.method private dyM(Landroid/support/v7/widget/an;)V
    .locals 2

    iget v0, p1, Landroid/support/v7/widget/an;->mPosition:I

    iget v1, p1, Landroid/support/v7/widget/an;->eda:I

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/al;->dyq(II)V

    return-void
.end method

.method private dya()V
    .locals 2

    iget v0, p0, Landroid/support/v7/widget/al;->ecy:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyB()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecK:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecK:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    goto :goto_0
.end method

.method private dyb(ZZ)Landroid/view/View;
    .locals 2

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0, p1, p2}, Landroid/support/v7/widget/al;->dyp(IIZZ)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/al;->dyp(IIZZ)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private dyc(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/al;->dyF(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private dyd(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v7/widget/an;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v2

    if-nez v2, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dsX()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p3, v2, p2}, Landroid/support/v7/widget/an;->dyZ(Landroid/view/View;Landroid/support/v7/widget/e;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p3, v2}, Landroid/support/v7/widget/an;->dzb(Landroid/view/View;)V

    return v1

    :cond_1
    iget-boolean v2, p0, Landroid/support/v7/widget/al;->ecB:Z

    iget-boolean v3, p0, Landroid/support/v7/widget/al;->ecG:Z

    if-eq v2, v3, :cond_2

    return v0

    :cond_2
    iget-boolean v2, p3, Landroid/support/v7/widget/an;->edb:Z

    if-eqz v2, :cond_5

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyJ(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_8

    invoke-virtual {p3, v2}, Landroid/support/v7/widget/an;->dyY(Landroid/view/View;)V

    invoke-virtual {p2}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dss()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v4}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v4

    if-ge v3, v4, :cond_6

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v3

    if-ge v2, v3, :cond_3

    move v0, v1

    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    iget-boolean v0, p3, Landroid/support/v7/widget/an;->edb:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v0

    :goto_2
    iput v0, p3, Landroid/support/v7/widget/an;->eda:I

    :cond_4
    return v1

    :cond_5
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dxZ(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v0

    goto :goto_2

    :cond_8
    return v0
.end method

.method private dyg(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 6

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v4

    invoke-virtual {p2}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v5

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/al;->dyv(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private dyh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;)V
    .locals 2

    iget-boolean v0, p2, Landroid/support/v7/widget/am;->ecM:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p2, Landroid/support/v7/widget/am;->ecV:Z

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v0, p2, Landroid/support/v7/widget/am;->ecR:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    iget v0, p2, Landroid/support/v7/widget/am;->ecU:I

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/al;->dyH(Landroid/support/v7/widget/j;I)V

    :goto_0
    return-void

    :cond_2
    iget v0, p2, Landroid/support/v7/widget/am;->ecU:I

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/al;->dyK(Landroid/support/v7/widget/j;I)V

    goto :goto_0
.end method

.method private dyi()Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private dym(Landroid/support/v7/widget/e;)I
    .locals 6

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecx:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/al;->dyI(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecx:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/al;->dyb(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/al;->ecx:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/cP;->dIy(Landroid/support/v7/widget/e;Landroid/support/v7/widget/ap;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/a;Z)I

    move-result v0

    return v0
.end method

.method private dyn(Landroid/support/v7/widget/e;Landroid/support/v7/widget/an;)Z
    .locals 6

    const/4 v5, -0x1

    const/high16 v4, -0x80000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/al;->ecE:I

    if-ne v0, v5, :cond_1

    :cond_0
    return v2

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/al;->ecE:I

    if-ltz v0, :cond_2

    iget v0, p0, Landroid/support/v7/widget/al;->ecE:I

    invoke-virtual {p1}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v3

    if-lt v0, v3, :cond_3

    :cond_2
    iput v5, p0, Landroid/support/v7/widget/al;->ecE:I

    iput v4, p0, Landroid/support/v7/widget/al;->ecJ:I

    return v2

    :cond_3
    iget v0, p0, Landroid/support/v7/widget/al;->ecE:I

    iput v0, p2, Landroid/support/v7/widget/an;->mPosition:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->dyW()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget-boolean v0, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecY:Z

    iput-boolean v0, p2, Landroid/support/v7/widget/an;->edb:Z

    iget-boolean v0, p2, Landroid/support/v7/widget/an;->edb:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v2, v2, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecX:I

    sub-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/an;->eda:I

    :goto_0
    return v1

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v2, v2, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecX:I

    add-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/an;->eda:I

    goto :goto_0

    :cond_5
    iget v0, p0, Landroid/support/v7/widget/al;->ecJ:I

    if-ne v0, v4, :cond_e

    iget v0, p0, Landroid/support/v7/widget/al;->ecE:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drG(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/ap;->dzg(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v4}, Landroid/support/v7/widget/ap;->dzh()I

    move-result v4

    if-le v3, v4, :cond_6

    invoke-virtual {p2}, Landroid/support/v7/widget/an;->dyX()V

    return v1

    :cond_6
    iget-object v3, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v4}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v4

    sub-int/2addr v3, v4

    if-gez v3, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v0

    iput v0, p2, Landroid/support/v7/widget/an;->eda:I

    iput-boolean v2, p2, Landroid/support/v7/widget/an;->edb:Z

    return v1

    :cond_7
    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    if-gez v2, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v0

    iput v0, p2, Landroid/support/v7/widget/an;->eda:I

    iput-boolean v1, p2, Landroid/support/v7/widget/an;->edb:Z

    return v1

    :cond_8
    iget-boolean v2, p2, Landroid/support/v7/widget/an;->edb:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzp()I

    move-result v2

    add-int/2addr v0, v2

    :goto_1
    iput v0, p2, Landroid/support/v7/widget/an;->eda:I

    :goto_2
    return v1

    :cond_9
    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v0

    goto :goto_1

    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-lez v0, :cond_c

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v0

    iget v3, p0, Landroid/support/v7/widget/al;->ecE:I

    if-ge v3, v0, :cond_d

    move v0, v1

    :goto_3
    iget-boolean v3, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-ne v0, v3, :cond_b

    move v2, v1

    :cond_b
    iput-boolean v2, p2, Landroid/support/v7/widget/an;->edb:Z

    :cond_c
    invoke-virtual {p2}, Landroid/support/v7/widget/an;->dyX()V

    goto :goto_2

    :cond_d
    move v0, v2

    goto :goto_3

    :cond_e
    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    iput-boolean v0, p2, Landroid/support/v7/widget/an;->edb:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v0

    iget v2, p0, Landroid/support/v7/widget/al;->ecJ:I

    sub-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/an;->eda:I

    :goto_4
    return v1

    :cond_f
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v0

    iget v2, p0, Landroid/support/v7/widget/al;->ecJ:I

    add-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/an;->eda:I

    goto :goto_4
.end method

.method private dyo(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 6

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p2}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v5

    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/al;->dyv(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private dyq(II)V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v2

    sub-int/2addr v2, p2

    iput v2, v0, Landroid/support/v7/widget/am;->ecP:I

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, v2, Landroid/support/v7/widget/am;->ecW:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput p1, v0, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v1, v0, Landroid/support/v7/widget/am;->ecR:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput p2, v0, Landroid/support/v7/widget/am;->ecS:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/am;->ecU:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private dys(Landroid/support/v7/widget/an;)V
    .locals 2

    iget v0, p1, Landroid/support/v7/widget/an;->mPosition:I

    iget v1, p1, Landroid/support/v7/widget/an;->eda:I

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/al;->dxU(II)V

    return-void
.end method

.method private dyt(Landroid/support/v7/widget/j;II)V
    .locals 1

    if-ne p2, p3, :cond_0

    return-void

    :cond_0
    if-le p3, p2, :cond_1

    add-int/lit8 v0, p3, -0x1

    :goto_0
    if-lt v0, p2, :cond_2

    invoke-virtual {p0, v0, p1}, Landroid/support/v7/widget/al;->drY(ILandroid/support/v7/widget/j;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-le p2, p3, :cond_2

    invoke-virtual {p0, p2, p1}, Landroid/support/v7/widget/al;->drY(ILandroid/support/v7/widget/j;)V

    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private dyx(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;Z)I
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v0

    sub-int v0, p1, v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v0, p2, p3}, Landroid/support/v7/widget/al;->dxR(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v0

    neg-int v0, v0

    add-int v1, p1, v0

    if-eqz p4, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v2

    sub-int/2addr v1, v2

    if-lez v1, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    neg-int v3, v1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ap;->dzs(I)V

    sub-int/2addr v0, v1

    return v0

    :cond_0
    return v1

    :cond_1
    return v0
.end method

.method private dyy(Landroid/support/v7/widget/e;)I
    .locals 7

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecx:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/al;->dyI(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecx:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/al;->dyb(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/al;->ecx:Z

    iget-boolean v6, p0, Landroid/support/v7/widget/al;->ecC:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v7/widget/cP;->dIx(Landroid/support/v7/widget/e;Landroid/support/v7/widget/ap;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/a;ZZ)I

    move-result v0

    return v0
.end method

.method private dyz(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyc(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyD(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public dnk(Landroid/view/View;Landroid/view/View;II)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, -0x1

    const-string/jumbo v0, "Cannot drop a view during a scroll or layout calculation"

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drm(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    invoke-direct {p0}, Landroid/support/v7/widget/al;->dya()V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v3

    if-ge v0, v3, :cond_0

    move v0, v1

    :goto_0
    iget-boolean v4, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v4, :cond_2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1, p2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/ap;->dzg(Landroid/view/View;)I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-virtual {p0, v3, v0}, Landroid/support/v7/widget/al;->dyw(II)V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1, p2}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v3, v0}, Landroid/support/v7/widget/al;->dyw(II)V

    goto :goto_1

    :cond_2
    if-ne v0, v2, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, v3, v0}, Landroid/support/v7/widget/al;->dyw(II)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/ap;->dzg(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v3, v0}, Landroid/support/v7/widget/al;->dyw(II)V

    goto :goto_1
.end method

.method public dqS(Landroid/support/v7/widget/e;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/widget/a;->dqS(Landroid/support/v7/widget/e;)V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/al;->ecE:I

    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/al;->ecJ:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/widget/an;->dza()V

    return-void
.end method

.method dqY()Z
    .locals 2

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dro()I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drV()I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dsI()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public drG(I)Landroid/view/View;
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v1

    sub-int v1, p1, v1

    if-ltz v1, :cond_1

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v1

    if-ne v1, p1, :cond_1

    return-object v0

    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/widget/a;->drG(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public drI(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/j;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/a;->drI(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/j;)V

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecA:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Landroid/support/v7/widget/al;->dqX(Landroid/support/v7/widget/j;)V

    invoke-virtual {p2}, Landroid/support/v7/widget/j;->clear()V

    :cond_0
    return-void
.end method

.method public drX(Landroid/support/v7/widget/e;)I
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/al;->dyy(Landroid/support/v7/widget/e;)I

    move-result v0

    return v0
.end method

.method public drm(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v7/widget/a;->drm(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public drr(Landroid/support/v7/widget/e;)I
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/al;->dym(Landroid/support/v7/widget/e;)I

    move-result v0

    return v0
.end method

.method public dsA(I)V
    .locals 1

    iput p1, p0, Landroid/support/v7/widget/al;->ecE:I

    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/al;->ecJ:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->dyV()V

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->requestLayout()V

    return-void
.end method

.method public dsB(ILandroid/support/v7/widget/M;)V
    .locals 5

    const/4 v0, -0x1

    const/4 v2, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v1}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->dyW()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget-boolean v3, v1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecY:Z

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v1, v1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecZ:I

    :goto_0
    if-eqz v3, :cond_3

    :goto_1
    move v3, v1

    move v1, v2

    :goto_2
    iget v4, p0, Landroid/support/v7/widget/al;->ecI:I

    if-ge v1, v4, :cond_4

    if-ltz v3, :cond_4

    if-ge v3, p1, :cond_4

    invoke-interface {p2, v3, v2}, Landroid/support/v7/widget/M;->dvG(II)V

    add-int/2addr v3, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/al;->dya()V

    iget-boolean v3, p0, Landroid/support/v7/widget/al;->ecC:Z

    iget v1, p0, Landroid/support/v7/widget/al;->ecE:I

    if-ne v1, v0, :cond_2

    if-eqz v3, :cond_1

    add-int/lit8 v1, p1, -0x1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget v1, p0, Landroid/support/v7/widget/al;->ecE:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method

.method public dsD(Landroid/support/v7/widget/e;)I
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/al;->dym(Landroid/support/v7/widget/e;)I

    move-result v0

    return v0
.end method

.method public dsE()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/widget/al;->ecy:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public dsJ(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Landroid/support/v7/widget/al;->ecy:I

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/al;->dxR(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v0

    return v0
.end method

.method public dsM(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I
    .locals 2

    iget v0, p0, Landroid/support/v7/widget/al;->ecy:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/al;->dxR(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v0

    return v0
.end method

.method public dsO(Landroid/support/v7/widget/e;)I
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/al;->dyy(Landroid/support/v7/widget/e;)I

    move-result v0

    return v0
.end method

.method public dsW(IILandroid/support/v7/widget/e;Landroid/support/v7/widget/M;)V
    .locals 3

    const/4 v1, 0x1

    iget v0, p0, Landroid/support/v7/widget/al;->ecy:I

    if-nez v0, :cond_1

    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    return-void

    :cond_1
    move p1, p2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    if-lez p1, :cond_3

    move v0, v1

    :goto_1
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-direct {p0, v0, v2, v1, p3}, Landroid/support/v7/widget/al;->dxX(IIZLandroid/support/v7/widget/e;)V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p3, v0, p4}, Landroid/support/v7/widget/al;->dyu(Landroid/support/v7/widget/e;Landroid/support/v7/widget/am;Landroid/support/v7/widget/M;)V

    return-void

    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public dsZ(Landroid/support/v7/widget/e;)I
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/al;->dxS(Landroid/support/v7/widget/e;)I

    move-result v0

    return v0
.end method

.method public dsi(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)V
    .locals 8

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/al;->ecE:I

    if-eq v0, v3, :cond_1

    :cond_0
    invoke-virtual {p2}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/al;->dqX(Landroid/support/v7/widget/j;)V

    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->dyW()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecZ:I

    iput v0, p0, Landroid/support/v7/widget/al;->ecE:I

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput-boolean v1, v0, Landroid/support/v7/widget/am;->ecM:Z

    invoke-direct {p0}, Landroid/support/v7/widget/al;->dya()V

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dsX()Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    iget-boolean v2, v2, Landroid/support/v7/widget/an;->edc:Z

    if-eqz v2, :cond_3

    iget v2, p0, Landroid/support/v7/widget/al;->ecE:I

    if-eq v2, v3, :cond_9

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/widget/an;->dza()V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    iget-boolean v2, p0, Landroid/support/v7/widget/al;->ecC:Z

    iget-boolean v5, p0, Landroid/support/v7/widget/al;->ecG:Z

    xor-int/2addr v2, v5

    iput-boolean v2, v0, Landroid/support/v7/widget/an;->edb:Z

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/al;->dyE(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v7/widget/an;)V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    iput-boolean v4, v0, Landroid/support/v7/widget/an;->edc:Z

    :cond_4
    :goto_0
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/al;->dyr(Landroid/support/v7/widget/e;)I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->ecL:I

    if-ltz v2, :cond_b

    move v2, v0

    move v0, v1

    :goto_1
    iget-object v5, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v5}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v5

    add-int/2addr v0, v5

    iget-object v5, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v5}, Landroid/support/v7/widget/ap;->dzn()I

    move-result v5

    add-int/2addr v2, v5

    invoke-virtual {p2}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v5

    if-eqz v5, :cond_5

    iget v5, p0, Landroid/support/v7/widget/al;->ecE:I

    if-eq v5, v3, :cond_5

    iget v5, p0, Landroid/support/v7/widget/al;->ecJ:I

    const/high16 v6, -0x80000000

    if-eq v5, v6, :cond_5

    iget v5, p0, Landroid/support/v7/widget/al;->ecE:I

    invoke-virtual {p0, v5}, Landroid/support/v7/widget/al;->drG(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-boolean v6, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v6, :cond_c

    iget-object v6, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v6}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v6

    iget-object v7, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v7, v5}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v5

    sub-int v5, v6, v5

    iget v6, p0, Landroid/support/v7/widget/al;->ecJ:I

    sub-int/2addr v5, v6

    :goto_2
    if-lez v5, :cond_d

    add-int/2addr v0, v5

    :cond_5
    :goto_3
    iget-object v5, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    iget-boolean v5, v5, Landroid/support/v7/widget/an;->edb:Z

    if-eqz v5, :cond_e

    iget-boolean v5, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-eqz v5, :cond_6

    move v3, v4

    :cond_6
    :goto_4
    iget-object v5, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-virtual {p0, p1, p2, v5, v3}, Landroid/support/v7/widget/al;->dye(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v7/widget/an;I)V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/al;->dst(Landroid/support/v7/widget/j;)V

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyj()Z

    move-result v5

    iput-boolean v5, v3, Landroid/support/v7/widget/am;->ecV:Z

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p2}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v5

    iput-boolean v5, v3, Landroid/support/v7/widget/am;->ecO:Z

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    iget-boolean v3, v3, Landroid/support/v7/widget/an;->edb:Z

    if-eqz v3, :cond_f

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-direct {p0, v3}, Landroid/support/v7/widget/al;->dys(Landroid/support/v7/widget/an;)V

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v0, v3, Landroid/support/v7/widget/am;->ecT:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v3, v0, Landroid/support/v7/widget/am;->ecS:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v5, v0, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v0, v0, Landroid/support/v7/widget/am;->ecP:I

    if-lez v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v0, v0, Landroid/support/v7/widget/am;->ecP:I

    add-int/2addr v2, v0

    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-direct {p0, v0}, Landroid/support/v7/widget/al;->dyM(Landroid/support/v7/widget/an;)V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v2, v0, Landroid/support/v7/widget/am;->ecT:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v2, v0, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v6, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v6, v6, Landroid/support/v7/widget/am;->ecW:I

    add-int/2addr v2, v6

    iput v2, v0, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v0, v0, Landroid/support/v7/widget/am;->ecS:I

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->ecP:I

    if-lez v2, :cond_14

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->ecP:I

    invoke-direct {p0, v5, v3}, Landroid/support/v7/widget/al;->dxU(II)V

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v2, v3, Landroid/support/v7/widget/am;->ecT:I

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p1, v2, p2, v1}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->ecS:I

    :goto_5
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v3

    if-lez v3, :cond_8

    iget-boolean v3, p0, Landroid/support/v7/widget/al;->ecC:Z

    iget-boolean v5, p0, Landroid/support/v7/widget/al;->ecG:Z

    xor-int/2addr v3, v5

    if-eqz v3, :cond_11

    invoke-direct {p0, v0, p1, p2, v4}, Landroid/support/v7/widget/al;->dxT(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;Z)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v3

    invoke-direct {p0, v2, p1, p2, v1}, Landroid/support/v7/widget/al;->dyx(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;Z)I

    move-result v1

    add-int/2addr v2, v1

    add-int/2addr v0, v1

    :cond_8
    :goto_6
    invoke-direct {p0, p1, p2, v2, v0}, Landroid/support/v7/widget/al;->dyA(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;II)V

    invoke-virtual {p2}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzt()V

    :goto_7
    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecG:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/al;->ecB:Z

    return-void

    :cond_9
    iget-object v2, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v2, :cond_3

    if-eqz v0, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v2

    iget-object v5, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v5}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v5

    if-ge v2, v5, :cond_a

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v2

    iget-object v5, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v5}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v5

    if-gt v2, v5, :cond_4

    :cond_a
    iget-object v2, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/an;->dzb(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_b
    move v2, v1

    goto/16 :goto_1

    :cond_c
    iget-object v6, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v6, v5}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v5

    iget-object v6, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v6}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v6

    sub-int/2addr v5, v6

    iget v6, p0, Landroid/support/v7/widget/al;->ecJ:I

    sub-int v5, v6, v5

    goto/16 :goto_2

    :cond_d
    sub-int/2addr v2, v5

    goto/16 :goto_3

    :cond_e
    iget-boolean v5, p0, Landroid/support/v7/widget/al;->ecC:Z

    if-nez v5, :cond_6

    move v3, v4

    goto/16 :goto_4

    :cond_f
    iget-object v3, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-direct {p0, v3}, Landroid/support/v7/widget/al;->dyM(Landroid/support/v7/widget/an;)V

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v2, v3, Landroid/support/v7/widget/am;->ecT:I

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p1, v2, p2, v1}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v2, v2, Landroid/support/v7/widget/am;->ecS:I

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v5, v3, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v3, v3, Landroid/support/v7/widget/am;->ecP:I

    if-lez v3, :cond_10

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v3, v3, Landroid/support/v7/widget/am;->ecP:I

    add-int/2addr v0, v3

    :cond_10
    iget-object v3, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-direct {p0, v3}, Landroid/support/v7/widget/al;->dys(Landroid/support/v7/widget/an;)V

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v0, v3, Landroid/support/v7/widget/am;->ecT:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v3, v0, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v6, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v6, v6, Landroid/support/v7/widget/am;->ecW:I

    add-int/2addr v3, v6

    iput v3, v0, Landroid/support/v7/widget/am;->ecQ:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v3, v0, Landroid/support/v7/widget/am;->ecS:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v0, v0, Landroid/support/v7/widget/am;->ecP:I

    if-lez v0, :cond_13

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v0, v0, Landroid/support/v7/widget/am;->ecP:I

    invoke-direct {p0, v5, v2}, Landroid/support/v7/widget/al;->dyq(II)V

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v0, v2, Landroid/support/v7/widget/am;->ecT:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v0, v0, Landroid/support/v7/widget/am;->ecS:I

    move v2, v3

    goto/16 :goto_5

    :cond_11
    invoke-direct {p0, v2, p1, p2, v4}, Landroid/support/v7/widget/al;->dyx(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;Z)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v3

    invoke-direct {p0, v0, p1, p2, v1}, Landroid/support/v7/widget/al;->dxT(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;Z)I

    move-result v1

    add-int/2addr v2, v1

    add-int/2addr v0, v1

    goto/16 :goto_6

    :cond_12
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecF:Landroid/support/v7/widget/an;

    invoke-virtual {v0}, Landroid/support/v7/widget/an;->dza()V

    goto/16 :goto_7

    :cond_13
    move v0, v2

    move v2, v3

    goto/16 :goto_5

    :cond_14
    move v2, v3

    goto/16 :goto_5
.end method

.method public dsq(Landroid/view/View;ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;
    .locals 7

    const/4 v6, -0x1

    const/high16 v5, -0x80000000

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/al;->dya()V

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-nez v0, :cond_0

    return-object v3

    :cond_0
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/al;->dxV(I)I

    move-result v2

    if-ne v2, v5, :cond_1

    return-object v3

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzh()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3eaaaaab

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {p0, v2, v0, v4, p4}, Landroid/support/v7/widget/al;->dxX(IIZLandroid/support/v7/widget/e;)V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput v5, v0, Landroid/support/v7/widget/am;->ecU:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput-boolean v4, v0, Landroid/support/v7/widget/am;->ecM:Z

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    const/4 v1, 0x1

    invoke-virtual {p0, p3, v0, p4, v1}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    if-ne v2, v6, :cond_2

    invoke-direct {p0, p3, p4}, Landroid/support/v7/widget/al;->dyz(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-ne v2, v6, :cond_3

    invoke-direct {p0}, Landroid/support/v7/widget/al;->dyi()Landroid/view/View;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v2

    if-eqz v2, :cond_5

    if-nez v1, :cond_4

    return-object v3

    :cond_2
    invoke-direct {p0, p3, p4}, Landroid/support/v7/widget/al;->dyG(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Landroid/support/v7/widget/al;->dxW()Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :cond_4
    return-object v0

    :cond_5
    return-object v1
.end method

.method public dss()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Landroid/support/v7/widget/al;->ecB:Z

    iget-boolean v2, p0, Landroid/support/v7/widget/al;->ecG:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public dsw()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Landroid/support/v7/widget/al;->ecy:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dsx(Landroid/support/v7/widget/e;)I
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/al;->dxS(Landroid/support/v7/widget/e;)I

    move-result v0

    return v0
.end method

.method dxR(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I
    .locals 5

    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return v4

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput-boolean v1, v0, Landroid/support/v7/widget/am;->ecM:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    if-lez p1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-direct {p0, v0, v2, v1, p3}, Landroid/support/v7/widget/al;->dxX(IIZLandroid/support/v7/widget/e;)V

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget v1, v1, Landroid/support/v7/widget/am;->ecU:I

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    invoke-virtual {p0, p2, v3, p3, v4}, Landroid/support/v7/widget/al;->dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I

    move-result v3

    add-int/2addr v1, v3

    if-gez v1, :cond_3

    return v4

    :cond_2
    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    if-le v2, v1, :cond_4

    mul-int p1, v0, v1

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ap;->dzs(I)V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iput p1, v0, Landroid/support/v7/widget/am;->ecL:I

    return p1
.end method

.method dxV(I)I
    .locals 4

    const/4 v0, -0x1

    const/high16 v1, -0x80000000

    const/4 v2, 0x1

    sparse-switch p1, :sswitch_data_0

    return v1

    :sswitch_0
    iget v1, p0, Landroid/support/v7/widget/al;->ecy:I

    if-ne v1, v2, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyB()Z

    move-result v1

    if-eqz v1, :cond_1

    return v2

    :cond_1
    return v0

    :sswitch_1
    iget v1, p0, Landroid/support/v7/widget/al;->ecy:I

    if-ne v1, v2, :cond_2

    return v2

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyB()Z

    move-result v1

    if-eqz v1, :cond_3

    return v0

    :cond_3
    return v2

    :sswitch_2
    iget v3, p0, Landroid/support/v7/widget/al;->ecy:I

    if-ne v3, v2, :cond_4

    :goto_0
    return v0

    :cond_4
    move v0, v1

    goto :goto_0

    :sswitch_3
    iget v0, p0, Landroid/support/v7/widget/al;->ecy:I

    if-ne v0, v2, :cond_5

    move v1, v2

    :cond_5
    return v1

    :sswitch_4
    iget v2, p0, Landroid/support/v7/widget/al;->ecy:I

    if-nez v2, :cond_6

    :goto_1
    return v0

    :cond_6
    move v0, v1

    goto :goto_1

    :sswitch_5
    iget v0, p0, Landroid/support/v7/widget/al;->ecy:I

    if-nez v0, :cond_7

    :goto_2
    return v2

    :cond_7
    move v2, v1

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x11 -> :sswitch_4
        0x21 -> :sswitch_2
        0x42 -> :sswitch_5
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method public dxY()I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v2, v0, v1, v2}, Landroid/support/v7/widget/al;->dyp(IIZZ)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method protected dyB()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drR()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dyC()I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v2, v0, v2, v1}, Landroid/support/v7/widget/al;->dyp(IIZZ)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method dyF(II)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    if-le p2, p1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_1
    if-ge p2, p1, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v1

    if-ge v0, v1, :cond_3

    const/16 v1, 0x4104

    const/16 v0, 0x4004

    :goto_1
    iget v2, p0, Landroid/support/v7/widget/al;->ecy:I

    if-nez v2, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/al;->dZr:Landroid/support/v7/widget/Q;

    invoke-virtual {v2, p1, p2, v1, v0}, Landroid/support/v7/widget/Q;->dxj(IIII)Landroid/view/View;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_3
    const/16 v1, 0x1041

    const/16 v0, 0x1001

    goto :goto_1

    :cond_4
    iget-object v2, p0, Landroid/support/v7/widget/al;->dZj:Landroid/support/v7/widget/Q;

    invoke-virtual {v2, p1, p2, v1, v0}, Landroid/support/v7/widget/Q;->dxj(IIII)Landroid/view/View;

    move-result-object v0

    goto :goto_2
.end method

.method dyL()Landroid/support/v7/widget/am;
    .locals 1

    new-instance v0, Landroid/support/v7/widget/am;

    invoke-direct {v0}, Landroid/support/v7/widget/am;-><init>()V

    return-object v0
.end method

.method public dyN(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/al;->drm(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v7/widget/al;->ecK:Z

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/al;->ecK:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->requestLayout()V

    return-void
.end method

.method public dyO()I
    .locals 4

    const/4 v0, -0x1

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/support/v7/widget/al;->dyp(IIZZ)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method dye(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v7/widget/an;I)V
    .locals 0

    return-void
.end method

.method dyf(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v7/widget/am;Landroid/support/v7/widget/ao;)V
    .locals 8

    const/4 v4, -0x1

    const/4 v7, 0x1

    const/4 v2, 0x0

    invoke-virtual {p3, p1}, Landroid/support/v7/widget/am;->dyQ(Landroid/support/v7/widget/j;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    iput-boolean v7, p4, Landroid/support/v7/widget/ao;->ede:Z

    return-void

    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, p3, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    if-nez v0, :cond_5

    iget-boolean v3, p0, Landroid/support/v7/widget/al;->ecC:Z

    iget v0, p3, Landroid/support/v7/widget/am;->ecR:I

    if-ne v0, v4, :cond_3

    move v0, v7

    :goto_0
    if-ne v3, v0, :cond_4

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->addView(Landroid/view/View;)V

    :goto_1
    invoke-virtual {p0, v1, v2, v2}, Landroid/support/v7/widget/al;->drE(Landroid/view/View;II)V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ap;->dzg(Landroid/view/View;)I

    move-result v0

    iput v0, p4, Landroid/support/v7/widget/ao;->edf:I

    iget v0, p0, Landroid/support/v7/widget/al;->ecy:I

    if-ne v0, v7, :cond_a

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyB()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drZ()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dsS()I

    move-result v2

    sub-int v2, v0, v2

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ap;->dzf(Landroid/view/View;)I

    move-result v0

    sub-int v0, v2, v0

    :goto_2
    iget v3, p3, Landroid/support/v7/widget/am;->ecR:I

    if-ne v3, v4, :cond_9

    iget v5, p3, Landroid/support/v7/widget/am;->ecS:I

    iget v3, p3, Landroid/support/v7/widget/am;->ecS:I

    iget v4, p4, Landroid/support/v7/widget/ao;->edf:I

    sub-int/2addr v3, v4

    move v4, v2

    move v2, v0

    :goto_3
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/al;->dqT(Landroid/view/View;IIII)V

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duz()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duy()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iput-boolean v7, p4, Landroid/support/v7/widget/ao;->edg:Z

    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->hasFocusable()Z

    move-result v0

    iput-boolean v0, p4, Landroid/support/v7/widget/ao;->edh:Z

    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/al;->addView(Landroid/view/View;I)V

    goto :goto_1

    :cond_5
    iget-boolean v3, p0, Landroid/support/v7/widget/al;->ecC:Z

    iget v0, p3, Landroid/support/v7/widget/am;->ecR:I

    if-ne v0, v4, :cond_6

    move v0, v7

    :goto_4
    if-ne v3, v0, :cond_7

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->drD(Landroid/view/View;)V

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_4

    :cond_7
    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/al;->dsf(Landroid/view/View;I)V

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dqU()I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ap;->dzf(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v0

    goto :goto_2

    :cond_9
    iget v3, p3, Landroid/support/v7/widget/am;->ecS:I

    iget v4, p3, Landroid/support/v7/widget/am;->ecS:I

    iget v5, p4, Landroid/support/v7/widget/ao;->edf:I

    add-int/2addr v5, v4

    move v4, v2

    move v2, v0

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dsg()I

    move-result v3

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ap;->dzf(Landroid/view/View;)I

    move-result v0

    add-int v5, v3, v0

    iget v0, p3, Landroid/support/v7/widget/am;->ecR:I

    if-ne v0, v4, :cond_b

    iget v2, p3, Landroid/support/v7/widget/am;->ecS:I

    iget v0, p3, Landroid/support/v7/widget/am;->ecS:I

    iget v4, p4, Landroid/support/v7/widget/ao;->edf:I

    sub-int/2addr v0, v4

    move v4, v2

    move v2, v0

    goto :goto_3

    :cond_b
    iget v0, p3, Landroid/support/v7/widget/am;->ecS:I

    iget v2, p3, Landroid/support/v7/widget/am;->ecS:I

    iget v4, p4, Landroid/support/v7/widget/ao;->edf:I

    add-int/2addr v2, v4

    move v4, v2

    move v2, v0

    goto :goto_3
.end method

.method dyj()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1}, Landroid/support/v7/widget/ap;->dze()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v1}, Landroid/support/v7/widget/ap;->dzk()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method dyk()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyL()Landroid/support/v7/widget/am;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    if-nez v0, :cond_1

    iget v0, p0, Landroid/support/v7/widget/al;->ecy:I

    invoke-static {p0, v0}, Landroid/support/v7/widget/ap;->dzo(Landroid/support/v7/widget/a;I)Landroid/support/v7/widget/ap;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    :cond_1
    return-void
.end method

.method dyl(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;Landroid/support/v7/widget/e;Z)I
    .locals 7

    const/high16 v6, -0x80000000

    iget v1, p2, Landroid/support/v7/widget/am;->ecP:I

    iget v0, p2, Landroid/support/v7/widget/am;->ecU:I

    if-eq v0, v6, :cond_1

    iget v0, p2, Landroid/support/v7/widget/am;->ecP:I

    if-gez v0, :cond_0

    iget v0, p2, Landroid/support/v7/widget/am;->ecU:I

    iget v2, p2, Landroid/support/v7/widget/am;->ecP:I

    add-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/am;->ecU:I

    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;)V

    :cond_1
    iget v0, p2, Landroid/support/v7/widget/am;->ecP:I

    iget v2, p2, Landroid/support/v7/widget/am;->ecT:I

    add-int/2addr v0, v2

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecz:Landroid/support/v7/widget/ao;

    :cond_2
    iget-boolean v3, p2, Landroid/support/v7/widget/am;->ecV:Z

    if-nez v3, :cond_3

    if-lez v0, :cond_4

    :cond_3
    invoke-virtual {p2, p3}, Landroid/support/v7/widget/am;->dyT(Landroid/support/v7/widget/e;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Landroid/support/v7/widget/ao;->resetInternal()V

    invoke-virtual {p0, p1, p3, p2, v2}, Landroid/support/v7/widget/al;->dyf(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/support/v7/widget/am;Landroid/support/v7/widget/ao;)V

    iget-boolean v3, v2, Landroid/support/v7/widget/ao;->ede:Z

    if-eqz v3, :cond_5

    :cond_4
    :goto_0
    iget v0, p2, Landroid/support/v7/widget/am;->ecP:I

    sub-int v0, v1, v0

    return v0

    :cond_5
    iget v3, p2, Landroid/support/v7/widget/am;->ecS:I

    iget v4, v2, Landroid/support/v7/widget/ao;->edf:I

    iget v5, p2, Landroid/support/v7/widget/am;->ecR:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/am;->ecS:I

    iget-boolean v3, v2, Landroid/support/v7/widget/ao;->edg:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecH:Landroid/support/v7/widget/am;

    iget-object v3, v3, Landroid/support/v7/widget/am;->ecN:Ljava/util/List;

    if-eqz v3, :cond_a

    :cond_6
    :goto_1
    iget v3, p2, Landroid/support/v7/widget/am;->ecP:I

    iget v4, v2, Landroid/support/v7/widget/ao;->edf:I

    sub-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/am;->ecP:I

    iget v3, v2, Landroid/support/v7/widget/ao;->edf:I

    sub-int/2addr v0, v3

    :cond_7
    iget v3, p2, Landroid/support/v7/widget/am;->ecU:I

    if-eq v3, v6, :cond_9

    iget v3, p2, Landroid/support/v7/widget/am;->ecU:I

    iget v4, v2, Landroid/support/v7/widget/ao;->edf:I

    add-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/am;->ecU:I

    iget v3, p2, Landroid/support/v7/widget/am;->ecP:I

    if-gez v3, :cond_8

    iget v3, p2, Landroid/support/v7/widget/am;->ecU:I

    iget v4, p2, Landroid/support/v7/widget/am;->ecP:I

    add-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/am;->ecU:I

    :cond_8
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;->dyh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/am;)V

    :cond_9
    if-eqz p4, :cond_2

    iget-boolean v3, v2, Landroid/support/v7/widget/ao;->edh:Z

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_a
    invoke-virtual {p3}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_7

    goto :goto_1
.end method

.method dyp(IIZZ)Landroid/view/View;
    .locals 3

    const/16 v0, 0x140

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    if-eqz p3, :cond_0

    const/16 v2, 0x6003

    :goto_0
    if-eqz p4, :cond_2

    :goto_1
    iget v1, p0, Landroid/support/v7/widget/al;->ecy:I

    if-nez v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/al;->dZr:Landroid/support/v7/widget/Q;

    invoke-virtual {v1, p1, p2, v2, v0}, Landroid/support/v7/widget/Q;->dxj(IIII)Landroid/view/View;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_0
    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/al;->dZj:Landroid/support/v7/widget/Q;

    invoke-virtual {v1, p1, p2, v2, v0}, Landroid/support/v7/widget/Q;->dxj(IIII)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected dyr(Landroid/support/v7/widget/e;)I
    .locals 1

    invoke-virtual {p1}, Landroid/support/v7/widget/e;->dtk()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzh()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method dyu(Landroid/support/v7/widget/e;Landroid/support/v7/widget/am;Landroid/support/v7/widget/M;)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p2, Landroid/support/v7/widget/am;->ecQ:I

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v1, p2, Landroid/support/v7/widget/am;->ecU:I

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-interface {p3, v0, v1}, Landroid/support/v7/widget/M;->dvG(II)V

    :cond_0
    return-void
.end method

.method dyv(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;III)Landroid/view/View;
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v5

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v6

    if-le p4, p3, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    move-object v4, v3

    :goto_1
    if-eq p3, p4, :cond_5

    invoke-virtual {p0, p3}, Landroid/support/v7/widget/al;->drB(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_1

    if-ge v0, p5, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duz()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez v4, :cond_8

    :goto_2
    add-int/2addr p3, v1

    move-object v4, v2

    goto :goto_1

    :cond_0
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move-object v2, v4

    goto :goto_2

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v0

    if-ge v0, v6, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v0

    if-ge v0, v5, :cond_4

    :cond_3
    if-nez v3, :cond_7

    move-object v0, v2

    :goto_3
    move-object v3, v0

    move-object v2, v4

    goto :goto_2

    :cond_4
    return-object v2

    :cond_5
    if-eqz v3, :cond_6

    :goto_4
    return-object v3

    :cond_6
    move-object v3, v4

    goto :goto_4

    :cond_7
    move-object v0, v3

    goto :goto_3

    :cond_8
    move-object v2, v4

    goto :goto_2
.end method

.method public dyw(II)V
    .locals 1

    iput p1, p0, Landroid/support/v7/widget/al;->ecE:I

    iput p2, p0, Landroid/support/v7/widget/al;->ecJ:I

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->dyV()V

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/al;->requestLayout()V

    return-void
.end method

.method public generateDefaultLayoutParams()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 2

    const/4 v1, -0x2

    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/widget/a;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyC()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyO()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    instance-of v0, p1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iput-object p1, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->requestLayout()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget-object v1, p0, Landroid/support/v7/widget/al;->ecw:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;-><init>(Landroid/support/v7/widget/LinearLayoutManager$SavedState;)V

    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-direct {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;-><init>()V

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->drc()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->dyk()V

    iget-boolean v1, p0, Landroid/support/v7/widget/al;->ecB:Z

    iget-boolean v2, p0, Landroid/support/v7/widget/al;->ecC:Z

    xor-int/2addr v1, v2

    iput-boolean v1, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecY:Z

    if-eqz v1, :cond_1

    invoke-direct {p0}, Landroid/support/v7/widget/al;->dxW()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzr()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/ap;->dzl(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecX:I

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecZ:I

    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/al;->dyi()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->drJ(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecZ:I

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ap;->dzi(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {v2}, Landroid/support/v7/widget/ap;->dzj()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->ecX:I

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->dyV()V

    goto :goto_0
.end method

.method public setOrientation(I)V
    .locals 3

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "invalid orientation:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/al;->drm(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v7/widget/al;->ecy:I

    if-ne p1, v0, :cond_1

    return-void

    :cond_1
    iput p1, p0, Landroid/support/v7/widget/al;->ecy:I

    iput-object v1, p0, Landroid/support/v7/widget/al;->ecD:Landroid/support/v7/widget/ap;

    invoke-virtual {p0}, Landroid/support/v7/widget/al;->requestLayout()V

    return-void
.end method
