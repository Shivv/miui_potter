.class Landroid/support/v7/widget/H;
.super Ljava/lang/Object;
.source "ViewInfoStore.java"


# instance fields
.field final mLayoutHolderMap:Landroid/support/v4/a/u;

.field final mOldChangedHolders:Landroid/support/v4/a/n;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v4/a/u;

    invoke-direct {v0}, Landroid/support/v4/a/u;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    new-instance v0, Landroid/support/v4/a/n;

    invoke-direct {v0}, Landroid/support/v4/a/n;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/H;->mOldChangedHolders:Landroid/support/v4/a/n;

    return-void
.end method

.method private dwo(Landroid/support/v7/widget/p;I)Landroid/support/v7/widget/K;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->dSi(Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_0

    return-object v3

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, v2}, Landroid/support/v4/a/u;->dSe(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cX;

    if-eqz v0, :cond_4

    iget v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    and-int/2addr v1, p2

    if-eqz v1, :cond_4

    iget v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    not-int v3, p2

    and-int/2addr v1, v3

    iput v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    const/4 v1, 0x4

    if-ne p2, v1, :cond_2

    iget-object v1, v0, Landroid/support/v7/widget/cX;->eqS:Landroid/support/v7/widget/K;

    :goto_0
    iget v3, v0, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v3, v3, 0xc

    if-nez v3, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v3, v2}, Landroid/support/v4/a/u;->dRZ(I)Ljava/lang/Object;

    invoke-static {v0}, Landroid/support/v7/widget/cX;->dIJ(Landroid/support/v7/widget/cX;)V

    :cond_1
    return-object v1

    :cond_2
    const/16 v1, 0x8

    if-ne p2, v1, :cond_3

    iget-object v1, v0, Landroid/support/v7/widget/cX;->eqU:Landroid/support/v7/widget/K;

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Must provide flag PRE or POST"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v3
.end method


# virtual methods
.method clear()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->clear()V

    iget-object v0, p0, Landroid/support/v7/widget/H;->mOldChangedHolders:Landroid/support/v4/a/n;

    invoke-virtual {v0}, Landroid/support/v4/a/n;->clear()V

    return-void
.end method

.method dwd(J)Landroid/support/v7/widget/p;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/H;->mOldChangedHolders:Landroid/support/v4/a/n;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/a/n;->dTl(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    return-object v0
.end method

.method dwe(Landroid/support/v7/widget/C;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, v2}, Landroid/support/v4/a/u;->dSf(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget-object v1, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v1, v2}, Landroid/support/v4/a/u;->dRZ(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/cX;

    iget v3, v1, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v3, v3, 0x3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    invoke-interface {p1, v0}, Landroid/support/v7/widget/C;->dvl(Landroid/support/v7/widget/p;)V

    :goto_1
    invoke-static {v1}, Landroid/support/v7/widget/cX;->dIJ(Landroid/support/v7/widget/cX;)V

    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_0
    iget v3, v1, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    iget-object v3, v1, Landroid/support/v7/widget/cX;->eqS:Landroid/support/v7/widget/K;

    if-nez v3, :cond_1

    invoke-interface {p1, v0}, Landroid/support/v7/widget/C;->dvl(Landroid/support/v7/widget/p;)V

    goto :goto_1

    :cond_1
    iget-object v3, v1, Landroid/support/v7/widget/cX;->eqS:Landroid/support/v7/widget/K;

    iget-object v4, v1, Landroid/support/v7/widget/cX;->eqU:Landroid/support/v7/widget/K;

    invoke-interface {p1, v0, v3, v4}, Landroid/support/v7/widget/C;->dvi(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V

    goto :goto_1

    :cond_2
    iget v3, v1, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v3, v3, 0xe

    const/16 v4, 0xe

    if-ne v3, v4, :cond_3

    iget-object v3, v1, Landroid/support/v7/widget/cX;->eqS:Landroid/support/v7/widget/K;

    iget-object v4, v1, Landroid/support/v7/widget/cX;->eqU:Landroid/support/v7/widget/K;

    invoke-interface {p1, v0, v3, v4}, Landroid/support/v7/widget/C;->dvj(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V

    goto :goto_1

    :cond_3
    iget v3, v1, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v3, v3, 0xc

    const/16 v4, 0xc

    if-ne v3, v4, :cond_4

    iget-object v3, v1, Landroid/support/v7/widget/cX;->eqS:Landroid/support/v7/widget/K;

    iget-object v4, v1, Landroid/support/v7/widget/cX;->eqU:Landroid/support/v7/widget/K;

    invoke-interface {p1, v0, v3, v4}, Landroid/support/v7/widget/C;->dvk(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V

    goto :goto_1

    :cond_4
    iget v3, v1, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_5

    iget-object v3, v1, Landroid/support/v7/widget/cX;->eqS:Landroid/support/v7/widget/K;

    invoke-interface {p1, v0, v3, v5}, Landroid/support/v7/widget/C;->dvi(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V

    goto :goto_1

    :cond_5
    iget v3, v1, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_6

    iget-object v3, v1, Landroid/support/v7/widget/cX;->eqS:Landroid/support/v7/widget/K;

    iget-object v4, v1, Landroid/support/v7/widget/cX;->eqU:Landroid/support/v7/widget/K;

    invoke-interface {p1, v0, v3, v4}, Landroid/support/v7/widget/C;->dvj(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V

    goto :goto_1

    :cond_6
    iget v0, v1, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_7
    return-void
.end method

.method public dwf(Landroid/support/v7/widget/p;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/H;->dwn(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method dwg(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cX;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/support/v7/widget/cX;->dIK()Landroid/support/v7/widget/cX;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iput-object p2, v0, Landroid/support/v7/widget/cX;->eqU:Landroid/support/v7/widget/K;

    iget v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    return-void
.end method

.method dwh(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cX;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/support/v7/widget/cX;->dIK()Landroid/support/v7/widget/cX;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iput-object p2, v0, Landroid/support/v7/widget/cX;->eqS:Landroid/support/v7/widget/K;

    iget v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    return-void
.end method

.method dwi(Landroid/support/v7/widget/p;)Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cX;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method dwj(JLandroid/support/v7/widget/p;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/H;->mOldChangedHolders:Landroid/support/v4/a/n;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/a/n;->dTh(JLjava/lang/Object;)V

    return-void
.end method

.method dwk(Landroid/support/v7/widget/p;)Landroid/support/v7/widget/K;
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/H;->dwo(Landroid/support/v7/widget/p;I)Landroid/support/v7/widget/K;

    move-result-object v0

    return-object v0
.end method

.method dwl(Landroid/support/v7/widget/p;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cX;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/support/v7/widget/cX;->dIK()Landroid/support/v7/widget/cX;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    return-void
.end method

.method dwm(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cX;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/support/v7/widget/cX;->dIK()Landroid/support/v7/widget/cX;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    iput-object p2, v0, Landroid/support/v7/widget/cX;->eqS:Landroid/support/v7/widget/K;

    return-void
.end method

.method dwn(Landroid/support/v7/widget/p;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cX;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v1, v1, -0x2

    iput v1, v0, Landroid/support/v7/widget/cX;->eqV:I

    return-void
.end method

.method dwp(Landroid/support/v7/widget/p;)Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cX;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/support/v7/widget/cX;->eqV:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method dwq(Landroid/support/v7/widget/p;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/H;->mOldChangedHolders:Landroid/support/v4/a/n;

    invoke-virtual {v0}, Landroid/support/v4/a/n;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/H;->mOldChangedHolders:Landroid/support/v4/a/n;

    invoke-virtual {v1, v0}, Landroid/support/v4/a/n;->dTk(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/H;->mOldChangedHolders:Landroid/support/v4/a/n;

    invoke-virtual {v1, v0}, Landroid/support/v4/a/n;->dTi(I)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/H;->mLayoutHolderMap:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cX;

    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/support/v7/widget/cX;->dIJ(Landroid/support/v7/widget/cX;)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method dwr(Landroid/support/v7/widget/p;)Landroid/support/v7/widget/K;
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/H;->dwo(Landroid/support/v7/widget/p;I)Landroid/support/v7/widget/K;

    move-result-object v0

    return-object v0
.end method

.method onDetach()V
    .locals 0

    invoke-static {}, Landroid/support/v7/widget/cX;->dIL()V

    return-void
.end method
