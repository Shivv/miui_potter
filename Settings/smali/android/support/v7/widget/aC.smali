.class Landroid/support/v7/widget/aC;
.super Ljava/lang/Object;
.source "AppCompatBackgroundHelper.java"


# instance fields
.field private eeA:Landroid/support/v7/widget/cw;

.field private eeB:I

.field private final eeC:Landroid/support/v7/widget/bF;

.field private eeD:Landroid/support/v7/widget/cw;

.field private final eeE:Landroid/view/View;

.field private eez:Landroid/support/v7/widget/cw;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/aC;->eeB:I

    iput-object p1, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-static {}, Landroid/support/v7/widget/bF;->get()Landroid/support/v7/widget/bF;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/aC;->eeC:Landroid/support/v7/widget/bF;

    return-void
.end method

.method private dAh()Z
    .locals 4

    const/16 v3, 0x15

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v2, v3, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/aC;->eeA:Landroid/support/v7/widget/cw;

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    if-ne v2, v3, :cond_2

    return v0

    :cond_2
    return v1
.end method

.method private dAj(Landroid/graphics/drawable/Drawable;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeD:Landroid/support/v7/widget/cw;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/cw;

    invoke-direct {v0}, Landroid/support/v7/widget/cw;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/aC;->eeD:Landroid/support/v7/widget/cw;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeD:Landroid/support/v7/widget/cw;

    invoke-virtual {v0}, Landroid/support/v7/widget/cw;->clear()V

    iget-object v1, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/z;->dPD(Landroid/view/View;)Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_1

    iput-boolean v2, v0, Landroid/support/v7/widget/cw;->eoN:Z

    iput-object v1, v0, Landroid/support/v7/widget/cw;->eoO:Landroid/content/res/ColorStateList;

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/z;->dPB(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v1

    if-eqz v1, :cond_2

    iput-boolean v2, v0, Landroid/support/v7/widget/cw;->eoP:Z

    iput-object v1, v0, Landroid/support/v7/widget/cw;->eoQ:Landroid/graphics/PorterDuff$Mode;

    :cond_2
    iget-boolean v1, v0, Landroid/support/v7/widget/cw;->eoN:Z

    if-nez v1, :cond_3

    iget-boolean v1, v0, Landroid/support/v7/widget/cw;->eoP:Z

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getDrawableState()[I

    move-result-object v1

    invoke-static {p1, v0, v1}, Landroid/support/v7/widget/bF;->dDJ(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;[I)V

    return v2

    :cond_4
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method dAd(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/aC;->eeB:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aC;->dAf(Landroid/content/res/ColorStateList;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/aC;->dAi()V

    return-void
.end method

.method dAe(I)V
    .locals 2

    const/4 v0, 0x0

    iput p1, p0, Landroid/support/v7/widget/aC;->eeB:I

    iget-object v1, p0, Landroid/support/v7/widget/aC;->eeC:Landroid/support/v7/widget/bF;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeC:Landroid/support/v7/widget/bF;

    iget-object v1, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/bF;->dDM(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aC;->dAf(Landroid/content/res/ColorStateList;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/aC;->dAi()V

    return-void
.end method

.method dAf(Landroid/content/res/ColorStateList;)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeA:Landroid/support/v7/widget/cw;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/cw;

    invoke-direct {v0}, Landroid/support/v7/widget/cw;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/aC;->eeA:Landroid/support/v7/widget/cw;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeA:Landroid/support/v7/widget/cw;

    iput-object p1, v0, Landroid/support/v7/widget/cw;->eoO:Landroid/content/res/ColorStateList;

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeA:Landroid/support/v7/widget/cw;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/cw;->eoN:Z

    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/aC;->dAi()V

    return-void

    :cond_1
    iput-object v0, p0, Landroid/support/v7/widget/aC;->eeA:Landroid/support/v7/widget/cw;

    goto :goto_0
.end method

.method dAg(Landroid/util/AttributeSet;I)V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/b/j;->dWg:[I

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, p2, v2}, Landroid/support/v7/widget/bS;->dFw(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/bS;

    move-result-object v1

    :try_start_0
    sget v0, Landroid/support/v7/b/j;->dWh:I

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/support/v7/b/j;->dWh:I

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/bS;->dFu(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/aC;->eeB:I

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeC:Landroid/support/v7/widget/bF;

    iget-object v2, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Landroid/support/v7/widget/aC;->eeB:I

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/bF;->dDM(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aC;->dAf(Landroid/content/res/ColorStateList;)V

    :cond_0
    sget v0, Landroid/support/v7/b/j;->dWi:I

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    sget v2, Landroid/support/v7/b/j;->dWi:I

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/view/z;->dPR(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    :cond_1
    sget v0, Landroid/support/v7/b/j;->dWj:I

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    sget v2, Landroid/support/v7/b/j;->dWj:I

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/bS;->dFr(II)I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/support/v7/widget/cx;->dHt(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/view/z;->dPQ(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-virtual {v1}, Landroid/support/v7/widget/bS;->dFB()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/support/v7/widget/bS;->dFB()V

    throw v0
.end method

.method dAi()V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Landroid/support/v7/widget/aC;->dAh()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/aC;->dAj(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    iget-object v2, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getDrawableState()[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/bF;->dDJ(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;[I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/aC;->eeA:Landroid/support/v7/widget/cw;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/aC;->eeA:Landroid/support/v7/widget/cw;

    iget-object v2, p0, Landroid/support/v7/widget/aC;->eeE:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getDrawableState()[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/bF;->dDJ(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;[I)V

    goto :goto_0
.end method

.method getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    iget-object v0, v0, Landroid/support/v7/widget/cw;->eoO:Landroid/content/res/ColorStateList;

    :cond_0
    return-object v0
.end method

.method getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    iget-object v0, v0, Landroid/support/v7/widget/cw;->eoQ:Landroid/graphics/PorterDuff$Mode;

    :cond_0
    return-object v0
.end method

.method setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/cw;

    invoke-direct {v0}, Landroid/support/v7/widget/cw;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    iput-object p1, v0, Landroid/support/v7/widget/cw;->eoO:Landroid/content/res/ColorStateList;

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/cw;->eoN:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/aC;->dAi()V

    return-void
.end method

.method setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/cw;

    invoke-direct {v0}, Landroid/support/v7/widget/cw;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    iput-object p1, v0, Landroid/support/v7/widget/cw;->eoQ:Landroid/graphics/PorterDuff$Mode;

    iget-object v0, p0, Landroid/support/v7/widget/aC;->eez:Landroid/support/v7/widget/cw;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/cw;->eoP:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/aC;->dAi()V

    return-void
.end method
