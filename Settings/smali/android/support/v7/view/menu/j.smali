.class public final Landroid/support/v7/view/menu/j;
.super Ljava/lang/Object;
.source "MenuWrapperFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dJR(Landroid/content/Context;Landroid/support/v4/b/a/b;)Landroid/view/SubMenu;
    .locals 1

    new-instance v0, Landroid/support/v7/view/menu/e;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/view/menu/e;-><init>(Landroid/content/Context;Landroid/support/v4/b/a/b;)V

    return-object v0
.end method

.method public static dJS(Landroid/content/Context;Landroid/support/v4/b/a/c;)Landroid/view/MenuItem;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v7/view/menu/r;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/view/menu/r;-><init>(Landroid/content/Context;Landroid/support/v4/b/a/c;)V

    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v7/view/menu/F;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/view/menu/F;-><init>(Landroid/content/Context;Landroid/support/v4/b/a/c;)V

    return-object v0
.end method
