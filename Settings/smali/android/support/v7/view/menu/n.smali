.class Landroid/support/v7/view/menu/n;
.super Landroid/support/v7/view/menu/D;
.source "MenuItemWrapperJB.java"

# interfaces
.implements Landroid/view/ActionProvider$VisibilityListener;


# instance fields
.field esE:Landroid/support/v4/view/al;

.field final synthetic esF:Landroid/support/v7/view/menu/r;


# direct methods
.method public constructor <init>(Landroid/support/v7/view/menu/r;Landroid/content/Context;Landroid/view/ActionProvider;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/n;->esF:Landroid/support/v7/view/menu/r;

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/view/menu/D;-><init>(Landroid/support/v7/view/menu/F;Landroid/content/Context;Landroid/view/ActionProvider;)V

    return-void
.end method


# virtual methods
.method public dJT(Landroid/support/v4/view/al;)V
    .locals 2

    const/4 v0, 0x0

    iput-object p1, p0, Landroid/support/v7/view/menu/n;->esE:Landroid/support/v4/view/al;

    iget-object v1, p0, Landroid/support/v7/view/menu/n;->euz:Landroid/view/ActionProvider;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v1, p0}, Landroid/view/ActionProvider;->setVisibilityListener(Landroid/view/ActionProvider$VisibilityListener;)V

    return-void

    :cond_0
    move-object p0, v0

    goto :goto_0
.end method

.method public dJU(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/n;->euz:Landroid/view/ActionProvider;

    invoke-virtual {v0, p1}, Landroid/view/ActionProvider;->onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public dJV()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/n;->euz:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->overridesItemVisibility()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/n;->euz:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->isVisible()Z

    move-result v0

    return v0
.end method

.method public onActionProviderVisibilityChanged(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/n;->esE:Landroid/support/v4/view/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/n;->esE:Landroid/support/v4/view/al;

    invoke-interface {v0, p1}, Landroid/support/v4/view/al;->onActionProviderVisibilityChanged(Z)V

    :cond_0
    return-void
.end method
