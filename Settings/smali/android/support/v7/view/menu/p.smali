.class public Landroid/support/v7/view/menu/p;
.super Ljava/lang/Object;
.source "MenuBuilder.java"

# interfaces
.implements Landroid/support/v4/b/a/a;


# static fields
.field private static final esV:[I


# instance fields
.field esH:Ljava/lang/CharSequence;

.field private esI:Z

.field private esJ:Z

.field private esK:Z

.field private esL:Z

.field private esM:Z

.field private esN:Landroid/view/ContextMenu$ContextMenuInfo;

.field private esO:Z

.field private final esP:Landroid/content/res/Resources;

.field private esQ:Z

.field private esR:Ljava/util/ArrayList;

.field private esS:Ljava/util/ArrayList;

.field private esT:Landroid/support/v7/view/menu/z;

.field esU:Landroid/view/View;

.field private esW:I

.field esX:Landroid/graphics/drawable/Drawable;

.field private esY:Z

.field private esZ:Landroid/support/v7/view/menu/H;

.field private eta:Z

.field private etb:Ljava/util/ArrayList;

.field private etc:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private etd:Ljava/util/ArrayList;

.field private ete:Z

.field private etf:Ljava/util/ArrayList;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Landroid/support/v7/view/menu/p;->esV:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x4
        0x5
        0x3
        0x2
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Landroid/support/v7/view/menu/p;->esW:I

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esY:Z

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esM:Z

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esO:Z

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esI:Z

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esJ:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->esS:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput-object p1, p0, Landroid/support/v7/view/menu/p;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->esP:Landroid/content/res/Resources;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->etb:Ljava/util/ArrayList;

    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->esK:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->etd:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->etf:Ljava/util/ArrayList;

    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->eta:Z

    invoke-direct {p0, v1}, Landroid/support/v7/view/menu/p;->dKv(Z)V

    return-void
.end method

.method private dKc(IIIILjava/lang/CharSequence;I)Landroid/support/v7/view/menu/z;
    .locals 8

    new-instance v0, Landroid/support/v7/view/menu/z;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Landroid/support/v7/view/menu/z;-><init>(Landroid/support/v7/view/menu/p;IIIILjava/lang/CharSequence;I)V

    return-object v0
.end method

.method private dKf(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p5, :cond_0

    iput-object p5, p0, Landroid/support/v7/view/menu/p;->esU:Landroid/view/View;

    iput-object v1, p0, Landroid/support/v7/view/menu/p;->esH:Ljava/lang/CharSequence;

    iput-object v1, p0, Landroid/support/v7/view/menu/p;->esX:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {p0, v2}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    return-void

    :cond_0
    if-lez p1, :cond_3

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->esH:Ljava/lang/CharSequence;

    :cond_1
    :goto_1
    if-lez p3, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/support/v4/content/a;->eab(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->esX:Landroid/graphics/drawable/Drawable;

    :cond_2
    :goto_2
    iput-object v1, p0, Landroid/support/v7/view/menu/p;->esU:Landroid/view/View;

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_1

    iput-object p2, p0, Landroid/support/v7/view/menu/p;->esH:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_4
    if-eqz p4, :cond_2

    iput-object p4, p0, Landroid/support/v7/view/menu/p;->esX:Landroid/graphics/drawable/Drawable;

    goto :goto_2
.end method

.method private dKm(IZ)V
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    :cond_2
    return-void
.end method

.method private static dKn(I)I
    .locals 2

    const/high16 v0, -0x10000

    and-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x10

    if-ltz v0, :cond_0

    sget-object v1, Landroid/support/v7/view/menu/p;->esV:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "order does not contain a valid category."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v1, Landroid/support/v7/view/menu/p;->esV:[I

    aget v0, v1, v0

    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr v1, p0

    or-int/2addr v0, v1

    return v0
.end method

.method private dKq(Landroid/support/v7/view/menu/h;Landroid/support/v7/view/menu/i;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2, p1}, Landroid/support/v7/view/menu/i;->dCa(Landroid/support/v7/view/menu/h;)Z

    move-result v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/view/menu/i;

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move v0, v2

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_2
    if-nez v2, :cond_4

    invoke-interface {v1, p1}, Landroid/support/v7/view/menu/i;->dCa(Landroid/support/v7/view/menu/h;)Z

    move-result v2

    move v0, v2

    goto :goto_1

    :cond_3
    return v2

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method private dKr(Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKH()V

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/view/menu/i;

    if-nez v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1, p1}, Landroid/support/v7/view/menu/i;->dBX(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKt()V

    return-void
.end method

.method private static dKu(Ljava/util/ArrayList;I)I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->dLm()I

    move-result v0

    if-gt v0, p1, :cond_0

    add-int/lit8 v0, v1, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v2
.end method

.method private dKv(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esP:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esP:Landroid/content/res/Resources;

    sget v1, Landroid/support/v7/b/b;->dOK:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esQ:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(I)Landroid/view/MenuItem;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esP:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v1, v0}, Landroid/support/v7/view/menu/p;->dKF(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esP:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/v7/view/menu/p;->dKF(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/support/v7/view/menu/p;->dKF(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Landroid/support/v7/view/menu/p;->dKF(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 9

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v0, 0x0

    invoke-virtual {v4, p4, p5, p6, v0}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    :goto_0
    and-int/lit8 v0, p7, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v7/view/menu/p;->removeGroup(I)V

    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    new-instance v6, Landroid/content/Intent;

    iget v1, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-gez v1, :cond_3

    move-object v1, p6

    :goto_2
    invoke-direct {v6, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    new-instance v1, Landroid/content/ComponentName;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3, v1}, Landroid/support/v7/view/menu/p;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz p8, :cond_1

    iget v6, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-ltz v6, :cond_1

    iget v0, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    aput-object v1, p8, v0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    :cond_3
    iget v1, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    aget-object v1, p5, v1

    goto :goto_2

    :cond_4
    return v3
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esP:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v1, v0}, Landroid/support/v7/view/menu/p;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esP:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/v7/view/menu/p;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 3

    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/support/v7/view/menu/p;->dKF(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    new-instance v1, Landroid/support/v7/view/menu/h;

    iget-object v2, p0, Landroid/support/v7/view/menu/p;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p0, v0}, Landroid/support/v7/view/menu/h;-><init>(Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/z;->dLp(Landroid/support/v7/view/menu/h;)V

    return-object v1
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Landroid/support/v7/view/menu/p;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esT:Landroid/support/v7/view/menu/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esT:Landroid/support/v7/view/menu/z;

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dJL(Landroid/support/v7/view/menu/z;)Z

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    return-void
.end method

.method public clearHeader()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->esX:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->esH:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v7/view/menu/p;->esU:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    return-void
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    return-void
.end method

.method public dJJ()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->esQ:Z

    return v0
.end method

.method public dJK()Landroid/support/v7/view/menu/p;
    .locals 0

    return-object p0
.end method

.method public dJL(Landroid/support/v7/view/menu/z;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->esT:Landroid/support/v7/view/menu/z;

    if-eq v1, p1, :cond_1

    :cond_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKH()V

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/view/menu/i;

    if-nez v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move v0, v2

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    invoke-interface {v1, p0, p1}, Landroid/support/v7/view/menu/i;->dBY(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKt()V

    if-eqz v0, :cond_4

    iput-object v4, p0, Landroid/support/v7/view/menu/p;->esT:Landroid/support/v7/view/menu/z;

    :cond_4
    return v0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public dJM(Landroid/support/v7/view/menu/H;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/p;->esZ:Landroid/support/v7/view/menu/H;

    return-void
.end method

.method dJN()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->ete:Z

    return v0
.end method

.method public dJP(Landroid/support/v7/view/menu/z;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKH()V

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/view/menu/i;

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move v0, v2

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    invoke-interface {v1, p0, p1}, Landroid/support/v7/view/menu/i;->dBZ(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKt()V

    if-eqz v0, :cond_3

    iput-object p1, p0, Landroid/support/v7/view/menu/p;->esT:Landroid/support/v7/view/menu/z;

    :cond_3
    return v0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method dJQ(Landroid/support/v7/view/menu/p;Landroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esZ:Landroid/support/v7/view/menu/H;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esZ:Landroid/support/v7/view/menu/H;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/view/menu/H;->dCe(Landroid/support/v7/view/menu/p;Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dJW()Ljava/util/ArrayList;
    .locals 5

    const/4 v2, 0x0

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->esK:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etb:Ljava/util/ArrayList;

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Landroid/support/v7/view/menu/p;->etb:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iput-boolean v2, p0, Landroid/support/v7/view/menu/p;->esK:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->eta:Z

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etb:Ljava/util/ArrayList;

    return-object v0
.end method

.method public dJX()Landroid/support/v7/view/menu/z;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esT:Landroid/support/v7/view/menu/z;

    return-object v0
.end method

.method public dJY(Landroid/view/MenuItem;Landroid/support/v7/view/menu/i;I)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    check-cast p1, Landroid/support/v7/view/menu/z;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return v2

    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->dLo()Z

    move-result v3

    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->dJs()Landroid/support/v4/view/ai;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/support/v4/view/ai;->hasSubMenu()Z

    move-result v0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->dLd()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->expandActionView()Z

    move-result v0

    or-int/2addr v0, v3

    if-eqz v0, :cond_2

    invoke-virtual {p0, v5}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    :cond_2
    :goto_1
    return v0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->hasSubMenu()Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz v1, :cond_9

    :cond_5
    and-int/lit8 v0, p3, 0x4

    if-nez v0, :cond_6

    invoke-virtual {p0, v2}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    :cond_6
    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->hasSubMenu()Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Landroid/support/v7/view/menu/h;

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p0, p1}, Landroid/support/v7/view/menu/h;-><init>(Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)V

    invoke-virtual {p1, v0}, Landroid/support/v7/view/menu/z;->dLp(Landroid/support/v7/view/menu/h;)V

    :cond_7
    invoke-virtual {p1}, Landroid/support/v7/view/menu/z;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/h;

    if-eqz v1, :cond_8

    invoke-virtual {v4, v0}, Landroid/support/v4/view/ai;->dLD(Landroid/view/SubMenu;)V

    :cond_8
    invoke-direct {p0, v0, p2}, Landroid/support/v7/view/menu/p;->dKq(Landroid/support/v7/view/menu/h;Landroid/support/v7/view/menu/i;)Z

    move-result v0

    or-int/2addr v0, v3

    if-nez v0, :cond_2

    invoke-virtual {p0, v5}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    goto :goto_1

    :cond_9
    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_a

    invoke-virtual {p0, v5}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    move v0, v3

    goto :goto_1

    :cond_a
    move v0, v3

    goto :goto_1
.end method

.method dJZ(ILandroid/view/KeyEvent;)Landroid/support/v7/view/menu/z;
    .locals 12

    const/4 v11, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Landroid/support/v7/view/menu/p;->esS:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0, v4, p1, p2}, Landroid/support/v7/view/menu/p;->dKG(Ljava/util/List;ILandroid/view/KeyEvent;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v11

    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v5

    new-instance v6, Landroid/view/KeyCharacterMap$KeyData;

    invoke-direct {v6}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    invoke-virtual {p2, v6}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v0, 0x1

    if-ne v7, v0, :cond_1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dJN()Z

    move-result v8

    move v2, v3

    :goto_0
    if-ge v2, v7, :cond_7

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    if-eqz v8, :cond_3

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getAlphabeticShortcut()C

    move-result v1

    :goto_1
    iget-object v9, v6, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    aget-char v9, v9, v3

    if-ne v1, v9, :cond_4

    and-int/lit8 v9, v5, 0x2

    if-nez v9, :cond_4

    :cond_2
    return-object v0

    :cond_3
    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getNumericShortcut()C

    move-result v1

    goto :goto_1

    :cond_4
    iget-object v9, v6, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    const/4 v10, 0x2

    aget-char v9, v9, v10

    if-ne v1, v9, :cond_5

    and-int/lit8 v9, v5, 0x2

    if-nez v9, :cond_2

    :cond_5
    if-eqz v8, :cond_6

    const/16 v9, 0x8

    if-ne v1, v9, :cond_6

    const/16 v1, 0x43

    if-eq p1, v1, :cond_2

    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_7
    return-object v11
.end method

.method public final dKA(Z)V
    .locals 3

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->esJ:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esJ:Z

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/view/menu/i;

    if-nez v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1, p0, p1}, Landroid/support/v7/view/menu/i;->dCc(Landroid/support/v7/view/menu/p;Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esJ:Z

    return-void
.end method

.method dKB(Landroid/support/v7/view/menu/z;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->eta:Z

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    return-void
.end method

.method public dKC()V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dJW()Ljava/util/ArrayList;

    move-result-object v4

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->eta:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/view/menu/i;

    if-nez v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move v0, v2

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/support/v7/view/menu/i;->dCb()Z

    move-result v0

    or-int/2addr v0, v2

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_4

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v3

    :goto_2
    if-ge v1, v2, :cond_5

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->dKZ()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Landroid/support/v7/view/menu/p;->etd:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    iget-object v5, p0, Landroid/support/v7/view/menu/p;->etf:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etf:Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dJW()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_5
    iput-boolean v3, p0, Landroid/support/v7/view/menu/p;->eta:Z

    return-void
.end method

.method public dKD(I)I
    .locals 3

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getItemId()I

    move-result v0

    if-ne v0, p1, :cond_0

    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public dKE(Landroid/support/v7/view/menu/i;Landroid/content/Context;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p1, p2, p0}, Landroid/support/v7/view/menu/i;->dBW(Landroid/content/Context;Landroid/support/v7/view/menu/p;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->eta:Z

    return-void
.end method

.method protected dKF(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 7

    invoke-static {p3}, Landroid/support/v7/view/menu/p;->dKn(I)I

    move-result v4

    iget v6, p0, Landroid/support/v7/view/menu/p;->esW:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/view/menu/p;->dKc(IIIILjava/lang/CharSequence;I)Landroid/support/v7/view/menu/z;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->esN:Landroid/view/ContextMenu$ContextMenuInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/view/menu/p;->esN:Landroid/view/ContextMenu$ContextMenuInfo;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/z;->dLe(Landroid/view/ContextMenu$ContextMenuInfo;)V

    :cond_0
    iget-object v1, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-static {v2, v4}, Landroid/support/v7/view/menu/p;->dKu(Ljava/util/ArrayList;I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    return-object v0
.end method

.method dKG(Ljava/util/List;ILandroid/view/KeyEvent;)V
    .locals 12

    const v11, 0x1100f

    const/16 v10, 0x43

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dJN()Z

    move-result v5

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getModifiers()I

    move-result v6

    new-instance v7, Landroid/view/KeyCharacterMap$KeyData;

    invoke-direct {v7}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    invoke-virtual {p3, v7}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eq p2, v10, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v2

    :goto_0
    if-ge v4, v8, :cond_8

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->hasSubMenu()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    check-cast v1, Landroid/support/v7/view/menu/p;

    invoke-virtual {v1, p1, p2, p3}, Landroid/support/v7/view/menu/p;->dKG(Ljava/util/List;ILandroid/view/KeyEvent;)V

    :cond_1
    if-eqz v5, :cond_4

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getAlphabeticShortcut()C

    move-result v1

    move v3, v1

    :goto_1
    if-eqz v5, :cond_5

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getAlphabeticModifiers()I

    move-result v1

    :goto_2
    and-int v9, v6, v11

    and-int/2addr v1, v11

    if-ne v9, v1, :cond_6

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_3

    if-eqz v3, :cond_3

    iget-object v1, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    aget-char v1, v1, v2

    if-eq v3, v1, :cond_2

    iget-object v1, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    const/4 v9, 0x2

    aget-char v1, v1, v9

    if-ne v3, v1, :cond_7

    :cond_2
    :goto_4
    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getNumericShortcut()C

    move-result v1

    move v3, v1

    goto :goto_1

    :cond_5
    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getNumericModifiers()I

    move-result v1

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    if-eqz v5, :cond_3

    const/16 v1, 0x8

    if-ne v3, v1, :cond_3

    if-ne p2, v10, :cond_3

    goto :goto_4

    :cond_8
    return-void
.end method

.method public dKH()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->esY:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esY:Z

    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->esM:Z

    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->esO:Z

    :cond_0
    return-void
.end method

.method protected dKa(I)Landroid/support/v7/view/menu/p;
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/view/menu/p;->dKf(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method public dKb(Z)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->esY:Z

    if-nez v0, :cond_2

    if-eqz p1, :cond_0

    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->esK:Z

    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->eta:Z

    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v7/view/menu/p;->dKr(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->esM:Z

    if-eqz p1, :cond_1

    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->esO:Z

    goto :goto_0
.end method

.method public dKd(Landroid/support/v7/view/menu/i;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/view/menu/i;

    if-eqz v1, :cond_1

    if-ne v1, p1, :cond_0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/view/menu/p;->etc:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method dKe(Landroid/support/v7/view/menu/z;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/menu/p;->esK:Z

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    return-void
.end method

.method public dKg(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/view/menu/p;->dKl(II)I

    move-result v0

    return v0
.end method

.method protected dKh(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/view/menu/p;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    move-object v0, p0

    move v3, v1

    move-object v4, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/view/menu/p;->dKf(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method protected dKi(Ljava/lang/CharSequence;)Landroid/support/v7/view/menu/p;
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, v1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/view/menu/p;->dKf(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method dKj(Landroid/view/MenuItem;)V
    .locals 6

    const/4 v2, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v4

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKH()V

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_3

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getGroupId()I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->dKX()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->isCheckable()Z

    move-result v1

    if-eqz v1, :cond_0

    if-ne v0, p1, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/z;->dLf(Z)V

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKt()V

    return-void
.end method

.method public dKk()Ljava/util/ArrayList;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKC()V

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etf:Ljava/util/ArrayList;

    return-object v0
.end method

.method public dKl(II)I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->size()I

    move-result v1

    if-gez p2, :cond_0

    move p2, v0

    :cond_0
    :goto_0
    if-ge p2, v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_1

    return p2

    :cond_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    return v0
.end method

.method dKo()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->esI:Z

    return v0
.end method

.method public dKp(Landroid/view/MenuItem;I)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Landroid/support/v7/view/menu/p;->dJY(Landroid/view/MenuItem;Landroid/support/v7/view/menu/i;I)Z

    move-result v0

    return v0
.end method

.method protected dKs(Landroid/view/View;)Landroid/support/v7/view/menu/p;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    move-object v0, p0

    move v3, v1

    move-object v4, v2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/view/menu/p;->dKf(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method public dKt()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->esY:Z

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->esM:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Landroid/support/v7/view/menu/p;->esM:Z

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->esO:Z

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    :cond_0
    return-void
.end method

.method protected dKw(I)Landroid/support/v7/view/menu/p;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    move-object v0, p0

    move v3, p1

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/view/menu/p;->dKf(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method public dKx()Ljava/util/ArrayList;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->dKC()V

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->etd:Ljava/util/ArrayList;

    return-object v0
.end method

.method public dKy()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esH:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public dKz()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esZ:Landroid/support/v7/view/menu/H;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esZ:Landroid/support/v7/view/menu/H;

    invoke-interface {v0, p0}, Landroid/support/v7/view/menu/H;->dCd(Landroid/support/v7/view/menu/p;)V

    :cond_0
    return-void
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-object v4
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    return-object v0
.end method

.method getResources()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esP:Landroid/content/res/Resources;

    return-object v0
.end method

.method public hasVisibleItems()Z
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Landroid/support/v7/view/menu/p;->esL:Z

    if-eqz v0, :cond_0

    return v4

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    return v4

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return v2
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/view/menu/p;->dJZ(ILandroid/view/KeyEvent;)Landroid/support/v7/view/menu/z;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public performIdentifierAction(II)Z
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v7/view/menu/p;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Landroid/support/v7/view/menu/p;->dKp(Landroid/view/MenuItem;I)Z

    move-result v0

    return v0
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/view/menu/p;->dJZ(ILandroid/view/KeyEvent;)Landroid/support/v7/view/menu/z;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1, p3}, Landroid/support/v7/view/menu/p;->dKp(Landroid/view/MenuItem;I)Z

    move-result v0

    :cond_0
    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    :cond_1
    return v0
.end method

.method public removeGroup(I)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Landroid/support/v7/view/menu/p;->dKg(I)I

    move-result v3

    if-ltz v3, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v4, v0, v3

    move v0, v1

    :goto_0
    add-int/lit8 v2, v0, 0x1

    if-ge v0, v4, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-direct {p0, v3, v1}, Landroid/support/v7/view/menu/p;->dKm(IZ)V

    move v0, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    :cond_1
    return-void
.end method

.method public removeItem(I)V
    .locals 2

    invoke-virtual {p0, p1}, Landroid/support/v7/view/menu/p;->dKD(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Landroid/support/v7/view/menu/p;->dKm(IZ)V

    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0, p3}, Landroid/support/v7/view/menu/z;->dLi(Z)V

    invoke-virtual {v0, p2}, Landroid/support/v7/view/menu/z;->setCheckable(Z)Landroid/view/MenuItem;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0, p2}, Landroid/support/v7/view/menu/z;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setGroupVisible(IZ)V
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/z;->getGroupId()I

    move-result v5

    if-ne v5, p1, :cond_2

    invoke-virtual {v0, p2}, Landroid/support/v7/view/menu/z;->dLj(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public setQwertyMode(Z)V
    .locals 1

    iput-boolean p1, p0, Landroid/support/v7/view/menu/p;->ete:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/p;->dKb(Z)V

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/p;->esR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
