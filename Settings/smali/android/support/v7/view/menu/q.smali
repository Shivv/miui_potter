.class public abstract Landroid/support/v7/view/menu/q;
.super Ljava/lang/Object;
.source "BaseMenuPresenter.java"

# interfaces
.implements Landroid/support/v7/view/menu/i;


# instance fields
.field private etg:Landroid/support/v7/view/menu/c;

.field private eth:I

.field protected eti:Landroid/support/v7/view/menu/p;

.field protected etj:Landroid/view/LayoutInflater;

.field protected etk:Landroid/view/LayoutInflater;

.field private etl:I

.field protected etm:Landroid/support/v7/view/menu/d;

.field protected etn:Landroid/content/Context;

.field protected mContext:Landroid/content/Context;

.field private mId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/view/menu/q;->etn:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/menu/q;->etk:Landroid/view/LayoutInflater;

    iput p2, p0, Landroid/support/v7/view/menu/q;->etl:I

    iput p3, p0, Landroid/support/v7/view/menu/q;->eth:I

    return-void
.end method


# virtual methods
.method public dBV(Landroid/support/v7/view/menu/c;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/q;->etg:Landroid/support/v7/view/menu/c;

    return-void
.end method

.method public dBW(Landroid/content/Context;Landroid/support/v7/view/menu/p;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/view/menu/q;->mContext:Landroid/content/Context;

    iget-object v0, p0, Landroid/support/v7/view/menu/q;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/menu/q;->etj:Landroid/view/LayoutInflater;

    iput-object p2, p0, Landroid/support/v7/view/menu/q;->eti:Landroid/support/v7/view/menu/p;

    return-void
.end method

.method public dBX(Z)V
    .locals 11

    const/4 v4, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/q;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Landroid/support/v7/view/menu/q;->eti:Landroid/support/v7/view/menu/p;

    if-eqz v1, :cond_7

    iget-object v1, p0, Landroid/support/v7/view/menu/q;->eti:Landroid/support/v7/view/menu/p;

    invoke-virtual {v1}, Landroid/support/v7/view/menu/p;->dKC()V

    iget-object v1, p0, Landroid/support/v7/view/menu/q;->eti:Landroid/support/v7/view/menu/p;

    invoke-virtual {v1}, Landroid/support/v7/view/menu/p;->dJW()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v7, v6

    move v5, v6

    :goto_0
    if-ge v7, v9, :cond_4

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/view/menu/z;

    invoke-virtual {p0, v5, v1}, Landroid/support/v7/view/menu/q;->dGh(ILandroid/support/v7/view/menu/z;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v2, v3, Landroid/support/v7/view/menu/y;

    if-eqz v2, :cond_3

    move-object v2, v3

    check-cast v2, Landroid/support/v7/view/menu/y;

    invoke-interface {v2}, Landroid/support/v7/view/menu/y;->getItemData()Landroid/support/v7/view/menu/z;

    move-result-object v2

    :goto_1
    invoke-virtual {p0, v1, v3, v0}, Landroid/support/v7/view/menu/q;->dGe(Landroid/support/v7/view/menu/z;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    if-eq v1, v2, :cond_1

    invoke-virtual {v10, v6}, Landroid/view/View;->setPressed(Z)V

    invoke-virtual {v10}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    :cond_1
    if-eq v10, v3, :cond_2

    invoke-virtual {p0, v10, v5}, Landroid/support/v7/view/menu/q;->dKL(Landroid/view/View;I)V

    :cond_2
    add-int/lit8 v1, v5, 0x1

    :goto_2
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v5, v1

    goto :goto_0

    :cond_3
    move-object v2, v4

    goto :goto_1

    :cond_4
    :goto_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v5, v1, :cond_5

    invoke-virtual {p0, v0, v5}, Landroid/support/v7/view/menu/q;->dGo(Landroid/view/ViewGroup;I)Z

    move-result v1

    if-nez v1, :cond_4

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_5
    return-void

    :cond_6
    move v1, v5

    goto :goto_2

    :cond_7
    move v5, v6

    goto :goto_3
.end method

.method public dBY(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dBZ(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dCa(Landroid/support/v7/view/menu/h;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/q;->etg:Landroid/support/v7/view/menu/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/q;->etg:Landroid/support/v7/view/menu/c;

    invoke-interface {v0, p1}, Landroid/support/v7/view/menu/c;->dCC(Landroid/support/v7/view/menu/p;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public dCb()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dCc(Landroid/support/v7/view/menu/p;Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/q;->etg:Landroid/support/v7/view/menu/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/q;->etg:Landroid/support/v7/view/menu/c;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/view/menu/c;->dCD(Landroid/support/v7/view/menu/p;Z)V

    :cond_0
    return-void
.end method

.method public dGe(Landroid/support/v7/view/menu/z;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    instance-of v0, p2, Landroid/support/v7/view/menu/y;

    if-eqz v0, :cond_0

    check-cast p2, Landroid/support/v7/view/menu/y;

    move-object v0, p2

    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/support/v7/view/menu/q;->dGi(Landroid/support/v7/view/menu/z;Landroid/support/v7/view/menu/y;)V

    check-cast v0, Landroid/view/View;

    return-object v0

    :cond_0
    invoke-virtual {p0, p3}, Landroid/support/v7/view/menu/q;->dKI(Landroid/view/ViewGroup;)Landroid/support/v7/view/menu/y;

    move-result-object v0

    goto :goto_0
.end method

.method public dGh(ILandroid/support/v7/view/menu/z;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract dGi(Landroid/support/v7/view/menu/z;Landroid/support/v7/view/menu/y;)V
.end method

.method protected dGo(Landroid/view/ViewGroup;I)Z
    .locals 1

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public dKI(Landroid/view/ViewGroup;)Landroid/support/v7/view/menu/y;
    .locals 3

    iget-object v0, p0, Landroid/support/v7/view/menu/q;->etk:Landroid/view/LayoutInflater;

    iget v1, p0, Landroid/support/v7/view/menu/q;->eth:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/y;

    return-object v0
.end method

.method public dKJ(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/view/menu/q;->mId:I

    return-void
.end method

.method public dKK()Landroid/support/v7/view/menu/c;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/q;->etg:Landroid/support/v7/view/menu/c;

    return-object v0
.end method

.method protected dKL(Landroid/view/View;I)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/q;->etm:Landroid/support/v7/view/menu/d;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method
