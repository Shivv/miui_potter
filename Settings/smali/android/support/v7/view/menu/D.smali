.class Landroid/support/v7/view/menu/D;
.super Landroid/support/v4/view/ai;
.source "MenuItemWrapperICS.java"


# instance fields
.field final synthetic euA:Landroid/support/v7/view/menu/F;

.field final euz:Landroid/view/ActionProvider;


# direct methods
.method public constructor <init>(Landroid/support/v7/view/menu/F;Landroid/content/Context;Landroid/view/ActionProvider;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/D;->euA:Landroid/support/v7/view/menu/F;

    invoke-direct {p0, p2}, Landroid/support/v4/view/ai;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Landroid/support/v7/view/menu/D;->euz:Landroid/view/ActionProvider;

    return-void
.end method


# virtual methods
.method public dLB()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/D;->euz:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->onPerformDefaultAction()Z

    move-result v0

    return v0
.end method

.method public dLC()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/D;->euz:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->onCreateActionView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public dLD(Landroid/view/SubMenu;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/view/menu/D;->euz:Landroid/view/ActionProvider;

    iget-object v1, p0, Landroid/support/v7/view/menu/D;->euA:Landroid/support/v7/view/menu/F;

    invoke-virtual {v1, p1}, Landroid/support/v7/view/menu/F;->dJH(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ActionProvider;->onPrepareSubMenu(Landroid/view/SubMenu;)V

    return-void
.end method

.method public hasSubMenu()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/D;->euz:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->hasSubMenu()Z

    move-result v0

    return v0
.end method
