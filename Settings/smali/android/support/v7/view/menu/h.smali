.class public Landroid/support/v7/view/menu/h;
.super Landroid/support/v7/view/menu/p;
.source "SubMenuBuilder.java"

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field private esA:Landroid/support/v7/view/menu/p;

.field private esB:Landroid/support/v7/view/menu/z;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/view/menu/p;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    iput-object p3, p0, Landroid/support/v7/view/menu/h;->esB:Landroid/support/v7/view/menu/z;

    return-void
.end method


# virtual methods
.method public dJJ()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/p;->dJJ()Z

    move-result v0

    return v0
.end method

.method public dJK()Landroid/support/v7/view/menu/p;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/p;->dJK()Landroid/support/v7/view/menu/p;

    move-result-object v0

    return-object v0
.end method

.method public dJL(Landroid/support/v7/view/menu/z;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/p;->dJL(Landroid/support/v7/view/menu/z;)Z

    move-result v0

    return v0
.end method

.method public dJM(Landroid/support/v7/view/menu/H;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/p;->dJM(Landroid/support/v7/view/menu/H;)V

    return-void
.end method

.method public dJN()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/p;->dJN()Z

    move-result v0

    return v0
.end method

.method public dJO()Landroid/view/Menu;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    return-object v0
.end method

.method public dJP(Landroid/support/v7/view/menu/z;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/p;->dJP(Landroid/support/v7/view/menu/z;)Z

    move-result v0

    return v0
.end method

.method dJQ(Landroid/support/v7/view/menu/p;Landroid/view/MenuItem;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/support/v7/view/menu/p;->dJQ(Landroid/support/v7/view/menu/p;Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/view/menu/p;->dJQ(Landroid/support/v7/view/menu/p;Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItem()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esB:Landroid/support/v7/view/menu/z;

    return-object v0
.end method

.method public setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/view/menu/p;->dKw(I)Landroid/support/v7/view/menu/p;

    move-result-object v0

    check-cast v0, Landroid/view/SubMenu;

    return-object v0
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/view/menu/p;->dKh(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/view/menu/p;

    move-result-object v0

    check-cast v0, Landroid/view/SubMenu;

    return-object v0
.end method

.method public setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/view/menu/p;->dKa(I)Landroid/support/v7/view/menu/p;

    move-result-object v0

    check-cast v0, Landroid/view/SubMenu;

    return-object v0
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/view/menu/p;->dKi(Ljava/lang/CharSequence;)Landroid/support/v7/view/menu/p;

    move-result-object v0

    check-cast v0, Landroid/view/SubMenu;

    return-object v0
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/view/menu/p;->dKs(Landroid/view/View;)Landroid/support/v7/view/menu/p;

    move-result-object v0

    check-cast v0, Landroid/view/SubMenu;

    return-object v0
.end method

.method public setIcon(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esB:Landroid/support/v7/view/menu/z;

    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/z;->setIcon(I)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esB:Landroid/support/v7/view/menu/z;

    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/z;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setQwertyMode(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/h;->esA:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/p;->setQwertyMode(Z)V

    return-void
.end method
