.class final Landroid/support/v7/view/menu/C;
.super Landroid/support/v7/view/menu/x;
.source "CascadingMenuPopup.java"

# interfaces
.implements Landroid/support/v7/view/menu/i;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;


# instance fields
.field private eua:I

.field private eub:Landroid/view/View;

.field private euc:I

.field private final eud:Z

.field eue:Landroid/view/View;

.field private final euf:Landroid/view/View$OnAttachStateChangeListener;

.field private final eug:Landroid/support/v7/widget/bD;

.field private euh:Z

.field private final eui:I

.field private euj:I

.field euk:Z

.field private eul:I

.field private eum:Landroid/support/v7/view/menu/c;

.field private eun:I

.field private euo:Landroid/widget/PopupWindow$OnDismissListener;

.field private final eup:I

.field private euq:Z

.field private final eur:Ljava/util/List;

.field private final eus:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private eut:Landroid/view/ViewTreeObserver;

.field final euu:Ljava/util/List;

.field private final euv:I

.field private euw:Z

.field final eux:Landroid/os/Handler;

.field private euy:Z

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;IIZ)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v7/view/menu/x;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/menu/C;->eur:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    new-instance v0, Landroid/support/v7/view/menu/k;

    invoke-direct {v0, p0}, Landroid/support/v7/view/menu/k;-><init>(Landroid/support/v7/view/menu/C;)V

    iput-object v0, p0, Landroid/support/v7/view/menu/C;->eus:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    new-instance v0, Landroid/support/v7/view/menu/t;

    invoke-direct {v0, p0}, Landroid/support/v7/view/menu/t;-><init>(Landroid/support/v7/view/menu/C;)V

    iput-object v0, p0, Landroid/support/v7/view/menu/C;->euf:Landroid/view/View$OnAttachStateChangeListener;

    new-instance v0, Landroid/support/v7/view/menu/O;

    invoke-direct {v0, p0}, Landroid/support/v7/view/menu/O;-><init>(Landroid/support/v7/view/menu/C;)V

    iput-object v0, p0, Landroid/support/v7/view/menu/C;->eug:Landroid/support/v7/widget/bD;

    iput v1, p0, Landroid/support/v7/view/menu/C;->eua:I

    iput v1, p0, Landroid/support/v7/view/menu/C;->euj:I

    iput-object p1, p0, Landroid/support/v7/view/menu/C;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/support/v7/view/menu/C;->eub:Landroid/view/View;

    iput p3, p0, Landroid/support/v7/view/menu/C;->eup:I

    iput p4, p0, Landroid/support/v7/view/menu/C;->euv:I

    iput-boolean p5, p0, Landroid/support/v7/view/menu/C;->eud:Z

    iput-boolean v1, p0, Landroid/support/v7/view/menu/C;->euh:Z

    invoke-direct {p0}, Landroid/support/v7/view/menu/C;->dLz()I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/menu/C;->eun:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Landroid/support/v7/b/d;->dOU:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/menu/C;->eui:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/menu/C;->eux:Landroid/os/Handler;

    return-void
.end method

.method static synthetic dLA(Landroid/support/v7/view/menu/C;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eus:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method

.method static synthetic dLr(Landroid/support/v7/view/menu/C;)Landroid/view/ViewTreeObserver;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eut:Landroid/view/ViewTreeObserver;

    return-object v0
.end method

.method private dLs(I)I
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/u;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getLocationOnScreen([I)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v3, p0, Landroid/support/v7/view/menu/C;->eue:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    iget v3, p0, Landroid/support/v7/view/menu/C;->eun:I

    if-ne v3, v5, :cond_1

    aget v1, v1, v4

    invoke-virtual {v0}, Landroid/widget/ListView;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iget v1, v2, Landroid/graphics/Rect;->right:I

    if-le v0, v1, :cond_0

    return v4

    :cond_0
    return v5

    :cond_1
    aget v0, v1, v4

    sub-int/2addr v0, p1

    if-gez v0, :cond_2

    return v5

    :cond_2
    return v4
.end method

.method static synthetic dLt(Landroid/support/v7/view/menu/C;Landroid/view/ViewTreeObserver;)Landroid/view/ViewTreeObserver;
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/C;->eut:Landroid/view/ViewTreeObserver;

    return-object p1
.end method

.method private dLu(Landroid/support/v7/view/menu/u;Landroid/support/v7/view/menu/p;)Landroid/view/View;
    .locals 9

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    iget-object v0, p1, Landroid/support/v7/view/menu/u;->etq:Landroid/support/v7/view/menu/p;

    invoke-direct {p0, v0, p2}, Landroid/support/v7/view/menu/C;->dLy(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/p;)Landroid/view/MenuItem;

    move-result-object v4

    if-nez v4, :cond_0

    return-object v8

    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/view/menu/u;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/L;

    :goto_0
    invoke-virtual {v0}, Landroid/support/v7/view/menu/L;->getCount()I

    move-result v6

    :goto_1
    if-ge v2, v6, :cond_6

    invoke-virtual {v0, v2}, Landroid/support/v7/view/menu/L;->getItem(I)Landroid/support/v7/view/menu/z;

    move-result-object v7

    if-ne v4, v7, :cond_2

    move v0, v2

    :goto_2
    if-ne v0, v3, :cond_3

    return-object v8

    :cond_1
    check-cast v0, Landroid/support/v7/view/menu/L;

    move v1, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    add-int/2addr v0, v1

    invoke-virtual {v5}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    if-ltz v0, :cond_4

    invoke-virtual {v5}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_5

    :cond_4
    return-object v8

    :cond_5
    invoke-virtual {v5, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_6
    move v0, v3

    goto :goto_2
.end method

.method private dLv()Landroid/support/v7/widget/cF;
    .locals 5

    new-instance v0, Landroid/support/v7/widget/cF;

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->mContext:Landroid/content/Context;

    iget v2, p0, Landroid/support/v7/view/menu/C;->eup:I

    iget v3, p0, Landroid/support/v7/view/menu/C;->euv:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, Landroid/support/v7/widget/cF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->eug:Landroid/support/v7/widget/bD;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->setHoverListener(Landroid/support/v7/widget/bD;)V

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/cF;->dAM(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/cF;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->eub:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->dAD(Landroid/view/View;)V

    iget v1, p0, Landroid/support/v7/view/menu/C;->euj:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->dAz(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->dAG(Z)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->dAE(I)V

    return-object v0
.end method

.method private dLw(Landroid/support/v7/view/menu/p;)V
    .locals 13

    const/4 v11, 0x2

    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    new-instance v0, Landroid/support/v7/view/menu/L;

    iget-boolean v3, p0, Landroid/support/v7/view/menu/C;->eud:Z

    invoke-direct {v0, p1, v8, v3}, Landroid/support/v7/view/menu/L;-><init>(Landroid/support/v7/view/menu/p;Landroid/view/LayoutInflater;Z)V

    invoke-virtual {p0}, Landroid/support/v7/view/menu/C;->dAs()Z

    move-result v3

    if-nez v3, :cond_2

    iget-boolean v3, p0, Landroid/support/v7/view/menu/C;->euh:Z

    if-eqz v3, :cond_2

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/L;->setForceShowIcon(Z)V

    :cond_0
    :goto_0
    iget-object v3, p0, Landroid/support/v7/view/menu/C;->mContext:Landroid/content/Context;

    iget v4, p0, Landroid/support/v7/view/menu/C;->eui:I

    invoke-static {v0, v6, v3, v4}, Landroid/support/v7/view/menu/C;->dKO(Landroid/widget/ListAdapter;Landroid/view/ViewGroup;Landroid/content/Context;I)I

    move-result v9

    invoke-direct {p0}, Landroid/support/v7/view/menu/C;->dLv()Landroid/support/v7/widget/cF;

    move-result-object v10

    invoke-virtual {v10, v0}, Landroid/support/v7/widget/cF;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v10, v9}, Landroid/support/v7/widget/cF;->dAp(I)V

    iget v0, p0, Landroid/support/v7/view/menu/C;->euj:I

    invoke-virtual {v10, v0}, Landroid/support/v7/widget/cF;->dAz(I)V

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    iget-object v3, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    invoke-direct {p0, v0, p1}, Landroid/support/v7/view/menu/C;->dLu(Landroid/support/v7/view/menu/u;Landroid/support/v7/view/menu/p;)Landroid/view/View;

    move-result-object v3

    move-object v5, v3

    move-object v7, v0

    :goto_1
    if-eqz v5, :cond_9

    invoke-virtual {v10, v2}, Landroid/support/v7/widget/cF;->dHM(Z)V

    invoke-virtual {v10, v6}, Landroid/support/v7/widget/cF;->dHN(Ljava/lang/Object;)V

    invoke-direct {p0, v9}, Landroid/support/v7/view/menu/C;->dLs(I)I

    move-result v3

    if-ne v3, v1, :cond_4

    move v0, v1

    :goto_2
    iput v3, p0, Landroid/support/v7/view/menu/C;->eun:I

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1a

    if-lt v3, v4, :cond_5

    invoke-virtual {v10, v5}, Landroid/support/v7/widget/cF;->dAD(Landroid/view/View;)V

    move v3, v2

    move v4, v2

    :goto_3
    iget v11, p0, Landroid/support/v7/view/menu/C;->euj:I

    and-int/lit8 v11, v11, 0x5

    const/4 v12, 0x5

    if-ne v11, v12, :cond_7

    if-eqz v0, :cond_6

    add-int v0, v4, v9

    :goto_4
    invoke-virtual {v10, v0}, Landroid/support/v7/widget/cF;->dAF(I)V

    invoke-virtual {v10, v1}, Landroid/support/v7/widget/cF;->dAq(Z)V

    invoke-virtual {v10, v3}, Landroid/support/v7/widget/cF;->dAu(I)V

    :goto_5
    new-instance v0, Landroid/support/v7/view/menu/u;

    iget v1, p0, Landroid/support/v7/view/menu/C;->eun:I

    invoke-direct {v0, v10, p1, v1}, Landroid/support/v7/view/menu/u;-><init>(Landroid/support/v7/widget/cF;Landroid/support/v7/view/menu/p;I)V

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v10}, Landroid/support/v7/widget/cF;->show()V

    invoke-virtual {v10}, Landroid/support/v7/widget/cF;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    if-nez v7, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/view/menu/C;->euy:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/support/v7/view/menu/p;->dKy()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    sget v0, Landroid/support/v7/b/g;->dQM:I

    invoke-virtual {v8, v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    invoke-virtual {p1}, Landroid/support/v7/view/menu/p;->dKy()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v0, v6, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    invoke-virtual {v10}, Landroid/support/v7/widget/cF;->show()V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/view/menu/C;->dAs()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Landroid/support/v7/view/menu/x;->dKN(Landroid/support/v7/view/menu/p;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/support/v7/view/menu/L;->setForceShowIcon(Z)V

    goto/16 :goto_0

    :cond_3
    move-object v5, v6

    move-object v7, v6

    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    new-array v3, v11, [I

    iget-object v4, p0, Landroid/support/v7/view/menu/C;->eub:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    new-array v11, v11, [I

    invoke-virtual {v5, v11}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v4, v11, v2

    aget v12, v3, v2

    sub-int/2addr v4, v12

    aget v11, v11, v1

    aget v3, v3, v1

    sub-int v3, v11, v3

    goto/16 :goto_3

    :cond_6
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v0

    sub-int v0, v4, v0

    goto/16 :goto_4

    :cond_7
    if-eqz v0, :cond_8

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v0, v4

    goto/16 :goto_4

    :cond_8
    sub-int v0, v4, v9

    goto/16 :goto_4

    :cond_9
    iget-boolean v0, p0, Landroid/support/v7/view/menu/C;->euw:Z

    if-eqz v0, :cond_a

    iget v0, p0, Landroid/support/v7/view/menu/C;->eul:I

    invoke-virtual {v10, v0}, Landroid/support/v7/widget/cF;->dAF(I)V

    :cond_a
    iget-boolean v0, p0, Landroid/support/v7/view/menu/C;->euq:Z

    if-eqz v0, :cond_b

    iget v0, p0, Landroid/support/v7/view/menu/C;->euc:I

    invoke-virtual {v10, v0}, Landroid/support/v7/widget/cF;->dAu(I)V

    :cond_b
    invoke-virtual {p0}, Landroid/support/v7/view/menu/C;->dKP()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/support/v7/widget/cF;->dAB(Landroid/graphics/Rect;)V

    goto/16 :goto_5
.end method

.method private dLx(Landroid/support/v7/view/menu/p;)I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget-object v0, v0, Landroid/support/v7/view/menu/u;->etq:Landroid/support/v7/view/menu/p;

    if-ne p1, v0, :cond_0

    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method private dLy(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/p;)Landroid/view/MenuItem;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/support/v7/view/menu/p;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Landroid/support/v7/view/menu/p;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v3

    if-ne p2, v3, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private dLz()I
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->eub:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method


# virtual methods
.method public dAs()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget-object v0, v0, Landroid/support/v7/view/menu/u;->ets:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->dAs()Z

    move-result v0

    :cond_0
    return v0
.end method

.method public dBV(Landroid/support/v7/view/menu/c;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/C;->eum:Landroid/support/v7/view/menu/c;

    return-void
.end method

.method public dBX(Z)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/u;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/view/menu/C;->dKQ(Landroid/widget/ListAdapter;)Landroid/support/v7/view/menu/L;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/view/menu/L;->notifyDataSetChanged()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public dCa(Landroid/support/v7/view/menu/h;)Z
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget-object v2, v0, Landroid/support/v7/view/menu/u;->etq:Landroid/support/v7/view/menu/p;

    if-ne p1, v2, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/view/menu/u;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    return v3

    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1}, Landroid/support/v7/view/menu/C;->dJx(Landroid/support/v7/view/menu/p;)V

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eum:Landroid/support/v7/view/menu/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eum:Landroid/support/v7/view/menu/c;

    invoke-interface {v0, p1}, Landroid/support/v7/view/menu/c;->dCC(Landroid/support/v7/view/menu/p;)Z

    :cond_2
    return v3

    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method public dCb()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dCc(Landroid/support/v7/view/menu/p;Z)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/support/v7/view/menu/C;->dLx(Landroid/support/v7/view/menu/p;)I

    move-result v1

    if-gez v1, :cond_0

    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    iget-object v2, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget-object v0, v0, Landroid/support/v7/view/menu/u;->etq:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, v3}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget-object v1, v0, Landroid/support/v7/view/menu/u;->etq:Landroid/support/v7/view/menu/p;

    invoke-virtual {v1, p0}, Landroid/support/v7/view/menu/p;->dKd(Landroid/support/v7/view/menu/i;)V

    iget-boolean v1, p0, Landroid/support/v7/view/menu/C;->euk:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Landroid/support/v7/view/menu/u;->ets:Landroid/support/v7/widget/cF;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/cF;->dHL(Ljava/lang/Object;)V

    iget-object v1, v0, Landroid/support/v7/view/menu/u;->ets:Landroid/support/v7/widget/cF;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/cF;->dAy(I)V

    :cond_2
    iget-object v0, v0, Landroid/support/v7/view/menu/u;->ets:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->dismiss()V

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_7

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    add-int/lit8 v2, v1, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget v0, v0, Landroid/support/v7/view/menu/u;->etr:I

    iput v0, p0, Landroid/support/v7/view/menu/C;->eun:I

    :goto_0
    if-nez v1, :cond_8

    invoke-virtual {p0}, Landroid/support/v7/view/menu/C;->dismiss()V

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eum:Landroid/support/v7/view/menu/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eum:Landroid/support/v7/view/menu/c;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Landroid/support/v7/view/menu/c;->dCD(Landroid/support/v7/view/menu/p;Z)V

    :cond_3
    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eut:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eut:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eut:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->eus:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_4
    iput-object v4, p0, Landroid/support/v7/view/menu/C;->eut:Landroid/view/ViewTreeObserver;

    :cond_5
    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eue:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->euf:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euo:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    :cond_6
    :goto_1
    return-void

    :cond_7
    invoke-direct {p0}, Landroid/support/v7/view/menu/C;->dLz()I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/menu/C;->eun:I

    goto :goto_0

    :cond_8
    if-eqz p2, :cond_6

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget-object v0, v0, Landroid/support/v7/view/menu/u;->etq:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, v3}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    goto :goto_1
.end method

.method public dJA(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eub:Landroid/view/View;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Landroid/support/v7/view/menu/C;->eub:Landroid/view/View;

    iget v0, p0, Landroid/support/v7/view/menu/C;->eua:I

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->eub:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/view/aj;->dRE(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/menu/C;->euj:I

    :cond_0
    return-void
.end method

.method public dJu(I)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/menu/C;->euq:Z

    iput p1, p0, Landroid/support/v7/view/menu/C;->euc:I

    return-void
.end method

.method public dJv(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/view/menu/C;->euy:Z

    return-void
.end method

.method public dJw(I)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/menu/C;->euw:Z

    iput p1, p0, Landroid/support/v7/view/menu/C;->eul:I

    return-void
.end method

.method public dJx(Landroid/support/v7/view/menu/p;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->mContext:Landroid/content/Context;

    invoke-virtual {p1, p0, v0}, Landroid/support/v7/view/menu/p;->dKE(Landroid/support/v7/view/menu/i;Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/support/v7/view/menu/C;->dAs()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v7/view/menu/C;->dLw(Landroid/support/v7/view/menu/p;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eur:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected dKR()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dismiss()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    new-array v2, v1, [Landroid/support/v7/view/menu/u;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/v7/view/menu/u;

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_1

    aget-object v2, v0, v1

    iget-object v3, v2, Landroid/support/v7/view/menu/u;->ets:Landroid/support/v7/widget/cF;

    invoke-virtual {v3}, Landroid/support/v7/widget/cF;->dAs()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, v2, Landroid/support/v7/view/menu/u;->ets:Landroid/support/v7/widget/cF;

    invoke-virtual {v2}, Landroid/support/v7/widget/cF;->dismiss()V

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/u;->getListView()Landroid/widget/ListView;

    move-result-object v0

    goto :goto_0
.end method

.method public onDismiss()V
    .locals 6

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_2

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget-object v5, v0, Landroid/support/v7/view/menu/u;->ets:Landroid/support/v7/widget/cF;

    invoke-virtual {v5}, Landroid/support/v7/widget/cF;->dAs()Z

    move-result v5

    if-nez v5, :cond_1

    :goto_1
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/support/v7/view/menu/u;->etq:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, v3}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/16 v0, 0x52

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/C;->dismiss()V

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setForceShowIcon(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/view/menu/C;->euh:Z

    return-void
.end method

.method public setGravity(I)V
    .locals 1

    iget v0, p0, Landroid/support/v7/view/menu/C;->eua:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Landroid/support/v7/view/menu/C;->eua:I

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eub:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/aj;->dRE(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/menu/C;->euj:I

    :cond_0
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/C;->euo:Landroid/widget/PopupWindow$OnDismissListener;

    return-void
.end method

.method public show()V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/view/menu/C;->dAs()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eur:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/p;

    invoke-direct {p0, v0}, Landroid/support/v7/view/menu/C;->dLw(Landroid/support/v7/view/menu/p;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eur:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eub:Landroid/view/View;

    iput-object v0, p0, Landroid/support/v7/view/menu/C;->eue:Landroid/view/View;

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eue:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eut:Landroid/view/ViewTreeObserver;

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Landroid/support/v7/view/menu/C;->eue:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/view/menu/C;->eut:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eut:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->eus:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/view/menu/C;->eue:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/view/menu/C;->euf:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
