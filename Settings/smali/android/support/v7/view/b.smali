.class public Landroid/support/v7/view/b;
.super Landroid/view/MenuInflater;
.source "SupportMenuInflater.java"


# static fields
.field static final evC:[Ljava/lang/Class;

.field static final evD:[Ljava/lang/Class;


# instance fields
.field private evE:Ljava/lang/Object;

.field final evF:[Ljava/lang/Object;

.field final evG:[Ljava/lang/Object;

.field mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/content/Context;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Landroid/support/v7/view/b;->evD:[Ljava/lang/Class;

    sget-object v0, Landroid/support/v7/view/b;->evD:[Ljava/lang/Class;

    sput-object v0, Landroid/support/v7/view/b;->evC:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Landroid/support/v7/view/b;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Landroid/support/v7/view/b;->evF:[Ljava/lang/Object;

    iget-object v0, p0, Landroid/support/v7/view/b;->evF:[Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v7/view/b;->evG:[Ljava/lang/Object;

    return-void
.end method

.method private dMd(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    return-object p1

    :cond_0
    instance-of v0, p1, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/content/ContextWrapper;

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/view/b;->dMd(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_1
    return-object p1
.end method

.method private dMe(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    .locals 9

    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x0

    new-instance v7, Landroid/support/v7/view/f;

    invoke-direct {v7, p0, p3}, Landroid/support/v7/view/f;-><init>(Landroid/support/v7/view/b;Landroid/view/Menu;)V

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    :goto_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "menu"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    :cond_0
    move v4, v6

    move v2, v6

    move v1, v0

    move-object v0, v5

    :goto_1
    if-nez v4, :cond_b

    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_2
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto :goto_1

    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Expecting menu, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    if-eq v0, v3, :cond_0

    goto :goto_0

    :pswitch_0
    if-nez v2, :cond_1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v8, "group"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v7, p2}, Landroid/support/v7/view/f;->dMi(Landroid/util/AttributeSet;)V

    move v1, v2

    :goto_3
    move v2, v1

    goto :goto_2

    :cond_4
    const-string/jumbo v8, "item"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v7, p2}, Landroid/support/v7/view/f;->dMm(Landroid/util/AttributeSet;)V

    move v1, v2

    goto :goto_3

    :cond_5
    const-string/jumbo v8, "menu"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v7}, Landroid/support/v7/view/f;->dMo()Landroid/view/SubMenu;

    move-result-object v1

    invoke-direct {p0, p1, p2, v1}, Landroid/support/v7/view/b;->dMe(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V

    move v1, v2

    goto :goto_3

    :cond_6
    move-object v0, v1

    move v1, v3

    goto :goto_3

    :pswitch_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_7

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    move v0, v4

    move-object v1, v5

    move v2, v6

    :goto_4
    move v4, v0

    move-object v0, v1

    goto :goto_2

    :cond_7
    const-string/jumbo v8, "group"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-virtual {v7}, Landroid/support/v7/view/f;->dMp()V

    move-object v1, v0

    move v0, v4

    goto :goto_4

    :cond_8
    const-string/jumbo v8, "item"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-virtual {v7}, Landroid/support/v7/view/f;->dMn()Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, v7, Landroid/support/v7/view/f;->evW:Landroid/support/v4/view/ai;

    if-eqz v1, :cond_9

    iget-object v1, v7, Landroid/support/v7/view/f;->evW:Landroid/support/v4/view/ai;

    invoke-virtual {v1}, Landroid/support/v4/view/ai;->hasSubMenu()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v7}, Landroid/support/v7/view/f;->dMo()Landroid/view/SubMenu;

    move-object v1, v0

    move v0, v4

    goto :goto_4

    :cond_9
    invoke-virtual {v7}, Landroid/support/v7/view/f;->dMq()V

    move-object v1, v0

    move v0, v4

    goto :goto_4

    :cond_a
    const-string/jumbo v8, "menu"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    move-object v1, v0

    move v0, v3

    goto :goto_4

    :pswitch_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Unexpected end of document"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    return-void

    :cond_c
    move-object v1, v0

    move v0, v4

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method dMf()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/b;->evE:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/b;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/support/v7/view/b;->dMd(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/b;->evE:Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/b;->evE:Ljava/lang/Object;

    return-object v0
.end method

.method public inflate(ILandroid/view/Menu;)V
    .locals 4

    const/4 v1, 0x0

    instance-of v0, p2, Landroid/support/v4/b/a/a;

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/support/v7/view/b;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    invoke-static {v1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    invoke-direct {p0, v1, v0, p2}, Landroid/support/v7/view/b;->dMe(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Landroid/view/InflateException;

    const-string/jumbo v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_2
    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    new-instance v2, Landroid/view/InflateException;

    const-string/jumbo v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method
