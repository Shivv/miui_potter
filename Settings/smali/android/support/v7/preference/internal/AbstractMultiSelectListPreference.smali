.class public abstract Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;
.super Landroid/support/v7/preference/DialogPreference;
.source "AbstractMultiSelectListPreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method


# virtual methods
.method public abstract djW()[Ljava/lang/CharSequence;
.end method

.method public abstract djX(Ljava/util/Set;)V
.end method

.method public abstract djY()Ljava/util/Set;
.end method

.method public abstract djZ()[Ljava/lang/CharSequence;
.end method
