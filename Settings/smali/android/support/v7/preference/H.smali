.class final Landroid/support/v7/preference/H;
.super Landroid/support/v4/view/d;
.source "PreferenceRecyclerViewAccessibilityDelegate.java"


# instance fields
.field final synthetic dNS:Landroid/support/v7/preference/E;


# direct methods
.method constructor <init>(Landroid/support/v7/preference/E;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/H;->dNS:Landroid/support/v7/preference/E;

    invoke-direct {p0}, Landroid/support/v4/view/d;-><init>()V

    return-void
.end method


# virtual methods
.method public ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/preference/H;->dNS:Landroid/support/v7/preference/E;

    iget-object v0, v0, Landroid/support/v7/preference/E;->dNC:Landroid/support/v4/view/d;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/d;->ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V

    iget-object v0, p0, Landroid/support/v7/preference/H;->dNS:Landroid/support/v7/preference/E;

    iget-object v0, v0, Landroid/support/v7/preference/E;->dNB:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->dpz(Landroid/view/View;)I

    move-result v1

    iget-object v0, p0, Landroid/support/v7/preference/H;->dNS:Landroid/support/v7/preference/E;

    iget-object v0, v0, Landroid/support/v7/preference/E;->dNB:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/b;

    move-result-object v0

    instance-of v2, v0, Landroid/support/v7/preference/y;

    if-nez v2, :cond_0

    return-void

    :cond_0
    check-cast v0, Landroid/support/v7/preference/y;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/y;->getItem(I)Landroid/support/v7/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {v0, p2}, Landroid/support/v7/preference/Preference;->dkz(Landroid/support/v4/view/a/a;)V

    return-void
.end method

.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/H;->dNS:Landroid/support/v7/preference/E;

    iget-object v0, v0, Landroid/support/v7/preference/E;->dNC:Landroid/support/v4/view/d;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/view/d;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method
