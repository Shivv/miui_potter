.class final Landroid/support/v7/preference/B;
.super Landroid/support/v7/c/b;
.source "PreferenceGroupAdapter.java"


# instance fields
.field final synthetic dNu:Ljava/util/List;

.field final synthetic dNv:Ljava/util/List;

.field final synthetic dNw:Landroid/support/v7/preference/r;

.field final synthetic dNx:Landroid/support/v7/preference/y;


# direct methods
.method constructor <init>(Landroid/support/v7/preference/y;Ljava/util/List;Ljava/util/List;Landroid/support/v7/preference/r;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/B;->dNx:Landroid/support/v7/preference/y;

    iput-object p2, p0, Landroid/support/v7/preference/B;->dNv:Ljava/util/List;

    iput-object p3, p0, Landroid/support/v7/preference/B;->dNu:Ljava/util/List;

    iput-object p4, p0, Landroid/support/v7/preference/B;->dNw:Landroid/support/v7/preference/r;

    invoke-direct {p0}, Landroid/support/v7/c/b;-><init>()V

    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 3

    iget-object v2, p0, Landroid/support/v7/preference/B;->dNw:Landroid/support/v7/preference/r;

    iget-object v0, p0, Landroid/support/v7/preference/B;->dNv:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    iget-object v1, p0, Landroid/support/v7/preference/B;->dNu:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/preference/Preference;

    invoke-virtual {v2, v0, v1}, Landroid/support/v7/preference/r;->dmf(Landroid/support/v7/preference/Preference;Landroid/support/v7/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public areItemsTheSame(II)Z
    .locals 3

    iget-object v2, p0, Landroid/support/v7/preference/B;->dNw:Landroid/support/v7/preference/r;

    iget-object v0, p0, Landroid/support/v7/preference/B;->dNv:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    iget-object v1, p0, Landroid/support/v7/preference/B;->dNu:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/preference/Preference;

    invoke-virtual {v2, v0, v1}, Landroid/support/v7/preference/r;->dmg(Landroid/support/v7/preference/Preference;Landroid/support/v7/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public getNewListSize()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/B;->dNu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/B;->dNv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
