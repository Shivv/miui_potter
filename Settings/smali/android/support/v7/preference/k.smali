.class public Landroid/support/v7/preference/k;
.super Ljava/lang/Object;
.source "PreferenceManager.java"


# instance fields
.field private dMA:Landroid/content/SharedPreferences;

.field private dMB:J

.field private dMC:I

.field private dMD:Landroid/support/v7/preference/PreferenceScreen;

.field private dME:Landroid/support/v7/preference/p;

.field private dMF:I

.field private dMG:Landroid/support/v7/preference/j;

.field private dMH:Z

.field private dMI:Landroid/support/v7/preference/q;

.field private dMJ:Landroid/support/v7/preference/r;

.field private dMK:Landroid/support/v7/preference/o;

.field private dML:Ljava/lang/String;

.field private dMM:Landroid/content/SharedPreferences$Editor;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/support/v7/preference/k;->dMB:J

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/preference/k;->dMC:I

    iput-object p1, p0, Landroid/support/v7/preference/k;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/support/v7/preference/k;->dlX(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/k;->dlL(Ljava/lang/String;)V

    return-void
.end method

.method private dlW(Z)V
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMM:Landroid/content/SharedPreferences$Editor;

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/support/v4/content/j;->getInstance()Landroid/support/v4/content/j;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/k;->dMM:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/j;->eaF(Landroid/content/SharedPreferences$Editor;)V

    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/preference/k;->dMH:Z

    return-void
.end method

.method private static dlX(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_preferences"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public dlF(Landroid/support/v7/preference/Preference;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/k;->dME:Landroid/support/v7/preference/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/k;->dME:Landroid/support/v7/preference/p;

    invoke-interface {v0, p1}, Landroid/support/v7/preference/p;->bwx(Landroid/support/v7/preference/Preference;)V

    :cond_0
    return-void
.end method

.method public dlG(Landroid/support/v7/preference/PreferenceScreen;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMD:Landroid/support/v7/preference/PreferenceScreen;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMD:Landroid/support/v7/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMD:Landroid/support/v7/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/support/v7/preference/PreferenceScreen;->iN()V

    :cond_0
    iput-object p1, p0, Landroid/support/v7/preference/k;->dMD:Landroid/support/v7/preference/PreferenceScreen;

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public dlH(Landroid/support/v7/preference/o;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/k;->dMK:Landroid/support/v7/preference/o;

    return-void
.end method

.method dlI()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/k;->dMH:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public dlJ(Landroid/support/v7/preference/q;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/k;->dMI:Landroid/support/v7/preference/q;

    return-void
.end method

.method public dlK()Landroid/support/v7/preference/r;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMJ:Landroid/support/v7/preference/r;

    return-object v0
.end method

.method public dlL(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/preference/k;->dML:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/preference/k;->dMA:Landroid/content/SharedPreferences;

    return-void
.end method

.method public dlM(Landroid/content/Context;ILandroid/support/v7/preference/PreferenceScreen;)Landroid/support/v7/preference/PreferenceScreen;
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v7/preference/k;->dlW(Z)V

    new-instance v0, Landroid/support/v7/preference/I;

    invoke-direct {v0, p1, p0}, Landroid/support/v7/preference/I;-><init>(Landroid/content/Context;Landroid/support/v7/preference/k;)V

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/preference/I;->dnd(ILandroid/support/v7/preference/PreferenceGroup;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/PreferenceScreen;

    invoke-virtual {v0, p0}, Landroid/support/v7/preference/PreferenceScreen;->cpS(Landroid/support/v7/preference/k;)V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Landroid/support/v7/preference/k;->dlW(Z)V

    return-object v0
.end method

.method public dlN(Landroid/content/Context;)Landroid/support/v7/preference/PreferenceScreen;
    .locals 2

    new-instance v0, Landroid/support/v7/preference/PreferenceScreen;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Landroid/support/v7/preference/PreferenceScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {v0, p0}, Landroid/support/v7/preference/PreferenceScreen;->cpS(Landroid/support/v7/preference/k;)V

    return-object v0
.end method

.method public dlO()Landroid/support/v7/preference/PreferenceScreen;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMD:Landroid/support/v7/preference/PreferenceScreen;

    return-object v0
.end method

.method public dlP(Landroid/support/v7/preference/r;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/k;->dMJ:Landroid/support/v7/preference/r;

    return-void
.end method

.method public dlQ()Landroid/support/v7/preference/q;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMI:Landroid/support/v7/preference/q;

    return-object v0
.end method

.method public dlR()Landroid/support/v7/preference/j;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMG:Landroid/support/v7/preference/j;

    return-object v0
.end method

.method public dlS(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMD:Landroid/support/v7/preference/PreferenceScreen;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/k;->dMD:Landroid/support/v7/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method dlT()Landroid/content/SharedPreferences$Editor;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMG:Landroid/support/v7/preference/j;

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/preference/k;->dMH:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMM:Landroid/content/SharedPreferences$Editor;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/preference/k;->dlZ()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/preference/k;->dMM:Landroid/content/SharedPreferences$Editor;

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/k;->dMM:Landroid/content/SharedPreferences$Editor;

    return-object v0

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/preference/k;->dlZ()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method public dlU(Landroid/support/v7/preference/p;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/k;->dME:Landroid/support/v7/preference/p;

    return-void
.end method

.method public dlV()Landroid/support/v7/preference/o;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/k;->dMK:Landroid/support/v7/preference/o;

    return-object v0
.end method

.method dlY()J
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Landroid/support/v7/preference/k;->dMB:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Landroid/support/v7/preference/k;->dMB:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dlZ()Landroid/content/SharedPreferences;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/preference/k;->dlR()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/k;->dMA:Landroid/content/SharedPreferences;

    if-nez v0, :cond_1

    iget v0, p0, Landroid/support/v7/preference/k;->dMC:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Landroid/support/v7/preference/k;->mContext:Landroid/content/Context;

    :goto_0
    iget-object v1, p0, Landroid/support/v7/preference/k;->dML:Ljava/lang/String;

    iget v2, p0, Landroid/support/v7/preference/k;->dMF:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/preference/k;->dMA:Landroid/content/SharedPreferences;

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/k;->dMA:Landroid/content/SharedPreferences;

    return-object v0

    :pswitch_0
    iget-object v0, p0, Landroid/support/v7/preference/k;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/a;->eac(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/k;->mContext:Landroid/content/Context;

    return-object v0
.end method
