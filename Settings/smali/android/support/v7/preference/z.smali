.class Landroid/support/v7/preference/z;
.super Ljava/lang/Object;
.source "PreferenceGroupAdapter.java"


# instance fields
.field private dNr:I

.field private dNs:I

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/support/v7/preference/z;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p1, Landroid/support/v7/preference/z;->dNr:I

    iput v0, p0, Landroid/support/v7/preference/z;->dNr:I

    iget v0, p1, Landroid/support/v7/preference/z;->dNs:I

    iput v0, p0, Landroid/support/v7/preference/z;->dNs:I

    iget-object v0, p1, Landroid/support/v7/preference/z;->name:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/v7/preference/z;->name:Ljava/lang/String;

    return-void
.end method

.method static synthetic dmE(Landroid/support/v7/preference/z;I)I
    .locals 0

    iput p1, p0, Landroid/support/v7/preference/z;->dNr:I

    return p1
.end method

.method static synthetic dmF(Landroid/support/v7/preference/z;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/z;->name:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic dmG(Landroid/support/v7/preference/z;)I
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/z;->dNr:I

    return v0
.end method

.method static synthetic dmH(Landroid/support/v7/preference/z;I)I
    .locals 0

    iput p1, p0, Landroid/support/v7/preference/z;->dNs:I

    return p1
.end method

.method static synthetic dmI(Landroid/support/v7/preference/z;)I
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/z;->dNs:I

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Landroid/support/v7/preference/z;

    if-nez v1, :cond_0

    return v0

    :cond_0
    check-cast p1, Landroid/support/v7/preference/z;

    iget v1, p0, Landroid/support/v7/preference/z;->dNr:I

    iget v2, p1, Landroid/support/v7/preference/z;->dNr:I

    if-ne v1, v2, :cond_1

    iget v1, p0, Landroid/support/v7/preference/z;->dNs:I

    iget v2, p1, Landroid/support/v7/preference/z;->dNs:I

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/preference/z;->name:Ljava/lang/String;

    iget-object v1, p1, Landroid/support/v7/preference/z;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    :cond_1
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Landroid/support/v7/preference/z;->dNr:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Landroid/support/v7/preference/z;->dNs:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroid/support/v7/preference/z;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
