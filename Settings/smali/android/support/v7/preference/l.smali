.class public Landroid/support/v7/preference/l;
.super Landroid/support/v7/widget/p;
.source "PreferenceViewHolder.java"


# instance fields
.field private dMN:Z

.field private final dMO:Landroid/util/SparseArray;

.field private dMP:Z


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 6

    const v5, 0x102003e

    const v4, 0x1020016

    const v3, 0x1020010

    const v2, 0x1020006

    invoke-direct {p0, p1}, Landroid/support/v7/widget/p;-><init>(Landroid/view/View;)V

    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Landroid/support/v7/preference/l;->dMO:Landroid/util/SparseArray;

    iget-object v0, p0, Landroid/support/v7/preference/l;->dMO:Landroid/util/SparseArray;

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/preference/l;->dMO:Landroid/util/SparseArray;

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/preference/l;->dMO:Landroid/util/SparseArray;

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/preference/l;->dMO:Landroid/util/SparseArray;

    sget v1, Landroid/support/v7/preference/b;->dIb:I

    sget v2, Landroid/support/v7/preference/b;->dIb:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/preference/l;->dMO:Landroid/util/SparseArray;

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public dma(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/preference/l;->dMO:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/l;->itemView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/preference/l;->dMO:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_1
    return-object v0
.end method

.method public dmb(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/preference/l;->dMN:Z

    return-void
.end method

.method public dmc(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/preference/l;->dMP:Z

    return-void
.end method

.method public dmd()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/l;->dMP:Z

    return v0
.end method

.method public dme()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/l;->dMN:Z

    return v0
.end method
