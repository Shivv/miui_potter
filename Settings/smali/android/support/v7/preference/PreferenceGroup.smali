.class public abstract Landroid/support/v7/preference/PreferenceGroup;
.super Landroid/support/v7/preference/Preference;
.source "PreferenceGroup.java"


# instance fields
.field private dMm:I

.field private final dMn:Landroid/os/Handler;

.field private dMo:Ljava/util/List;

.field private final dMp:Landroid/support/v4/a/a;

.field private dMq:Z

.field private dMr:Z

.field private final dMs:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/preference/PreferenceGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/preference/PreferenceGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-boolean v3, p0, Landroid/support/v7/preference/PreferenceGroup;->dMr:Z

    iput v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMm:I

    iput-boolean v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMq:Z

    new-instance v0, Landroid/support/v4/a/a;

    invoke-direct {v0}, Landroid/support/v4/a/a;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMp:Landroid/support/v4/a/a;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMn:Landroid/os/Handler;

    new-instance v0, Landroid/support/v7/preference/n;

    invoke-direct {v0, p0}, Landroid/support/v7/preference/n;-><init>(Landroid/support/v7/preference/PreferenceGroup;)V

    iput-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMs:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMo:Ljava/util/List;

    sget-object v0, Landroid/support/v7/preference/d;->dJG:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Landroid/support/v7/preference/d;->dJH:I

    sget v2, Landroid/support/v7/preference/d;->dJH:I

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/PreferenceGroup;->dMr:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private dli(Landroid/support/v7/preference/Preference;)Z
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->onPrepareForRemoval()V

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->dkA()Landroid/support/v7/preference/PreferenceGroup;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/Preference;->dku(Landroid/support/v7/preference/PreferenceGroup;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Landroid/support/v7/preference/PreferenceGroup;->dMp:Landroid/support/v4/a/a;

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/a/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v7/preference/PreferenceGroup;->dMn:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v7/preference/PreferenceGroup;->dMs:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Landroid/support/v7/preference/PreferenceGroup;->dMn:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v7/preference/PreferenceGroup;->dMs:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    iget-boolean v1, p0, Landroid/support/v7/preference/PreferenceGroup;->dMq:Z

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->iN()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic dlm(Landroid/support/v7/preference/PreferenceGroup;)Landroid/support/v4/a/a;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMp:Landroid/support/v4/a/a;

    return-object v0
.end method


# virtual methods
.method public dkQ(Z)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v7/preference/Preference;->dkQ(Z)V

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dln()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/PreferenceGroup;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Landroid/support/v7/preference/Preference;->dkx(Landroid/support/v7/preference/Preference;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected dkU(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v7/preference/Preference;->dkU(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dln()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/PreferenceGroup;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/support/v7/preference/Preference;->dkU(Landroid/os/Bundle;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected dkV(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v7/preference/Preference;->dkV(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dln()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/PreferenceGroup;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/support/v7/preference/Preference;->dkV(Landroid/os/Bundle;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected dlc()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method dle()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMo:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dlf(I)Landroid/support/v7/preference/Preference;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    return-object v0
.end method

.method public dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dln()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    invoke-virtual {p0, v1}, Landroid/support/v7/preference/PreferenceGroup;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v0

    :cond_1
    instance-of v3, v0, Landroid/support/v7/preference/PreferenceGroup;

    if-eqz v3, :cond_2

    check-cast v0, Landroid/support/v7/preference/PreferenceGroup;

    invoke-virtual {v0, p1}, Landroid/support/v7/preference/PreferenceGroup;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_2

    return-object v0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return-object v4
.end method

.method public dlh(Landroid/support/v7/preference/Preference;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/preference/PreferenceGroup;->im(Landroid/support/v7/preference/Preference;)Z

    return-void
.end method

.method protected dlj(Landroid/support/v7/preference/Preference;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->shouldDisableDependents()Z

    move-result v0

    invoke-virtual {p1, p0, v0}, Landroid/support/v7/preference/Preference;->dkx(Landroid/support/v7/preference/Preference;Z)V

    const/4 v0, 0x1

    return v0
.end method

.method public dlk()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMq:Z

    return v0
.end method

.method public dll(Landroid/support/v7/preference/Preference;)Z
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/preference/PreferenceGroup;->dli(Landroid/support/v7/preference/Preference;)Z

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dkH()V

    return v0
.end method

.method public dln()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public dlo(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/preference/PreferenceGroup;->dMr:Z

    return-void
.end method

.method public iM()V
    .locals 3

    invoke-super {p0}, Landroid/support/v7/preference/Preference;->iM()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMq:Z

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dln()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/PreferenceGroup;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/preference/Preference;->iM()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public iN()V
    .locals 3

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/support/v7/preference/Preference;->iN()V

    iput-boolean v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMq:Z

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dln()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/PreferenceGroup;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/preference/Preference;->iN()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public im(Landroid/support/v7/preference/Preference;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v5

    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getOrder()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMr:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMm:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Landroid/support/v7/preference/PreferenceGroup;->dMm:I

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/Preference;->setOrder(I)V

    :cond_1
    instance-of v0, p1, Landroid/support/v7/preference/PreferenceGroup;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Landroid/support/v7/preference/PreferenceGroup;

    iget-boolean v1, p0, Landroid/support/v7/preference/PreferenceGroup;->dMr:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceGroup;->dlo(Z)V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMo:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_3

    mul-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, -0x1

    :cond_3
    invoke-virtual {p0, p1}, Landroid/support/v7/preference/PreferenceGroup;->dlj(Landroid/support/v7/preference/Preference;)Z

    move-result v1

    if-nez v1, :cond_4

    return v2

    :cond_4
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Landroid/support/v7/preference/PreferenceGroup;->dMo:Ljava/util/List;

    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dky()Landroid/support/v7/preference/k;

    move-result-object v2

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMp:Landroid/support/v4/a/a;

    invoke-virtual {v0, v3}, Landroid/support/v4/a/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMp:Landroid/support/v4/a/a;

    invoke-virtual {v0, v3}, Landroid/support/v4/a/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v4, p0, Landroid/support/v7/preference/PreferenceGroup;->dMp:Landroid/support/v4/a/a;

    invoke-virtual {v4, v3}, Landroid/support/v4/a/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0, v1}, Landroid/support/v7/preference/Preference;->dkg(Landroid/support/v7/preference/k;J)V

    invoke-virtual {p1, p0}, Landroid/support/v7/preference/Preference;->dku(Landroid/support/v7/preference/PreferenceGroup;)V

    iget-boolean v0, p0, Landroid/support/v7/preference/PreferenceGroup;->dMq:Z

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->iM()V

    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dkH()V

    return v5

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_6
    invoke-virtual {v2}, Landroid/support/v7/preference/k;->dlY()J

    move-result-wide v0

    goto :goto_0
.end method

.method public removeAll()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Landroid/support/v7/preference/PreferenceGroup;->dMo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    invoke-direct {p0, v0}, Landroid/support/v7/preference/PreferenceGroup;->dli(Landroid/support/v7/preference/Preference;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    monitor-exit p0

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceGroup;->dkH()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
