.class public Landroid/support/v13/app/g;
.super Ljava/lang/Object;
.source "FragmentCompat.java"


# static fields
.field static final dCm:Landroid/support/v13/app/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v13/app/b;

    invoke-direct {v0}, Landroid/support/v13/app/b;-><init>()V

    sput-object v0, Landroid/support/v13/app/g;->dCm:Landroid/support/v13/app/e;

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    new-instance v0, Landroid/support/v13/app/c;

    invoke-direct {v0}, Landroid/support/v13/app/c;-><init>()V

    sput-object v0, Landroid/support/v13/app/g;->dCm:Landroid/support/v13/app/e;

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_2

    new-instance v0, Landroid/support/v13/app/f;

    invoke-direct {v0}, Landroid/support/v13/app/f;-><init>()V

    sput-object v0, Landroid/support/v13/app/g;->dCm:Landroid/support/v13/app/e;

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/support/v13/app/i;

    invoke-direct {v0}, Landroid/support/v13/app/i;-><init>()V

    sput-object v0, Landroid/support/v13/app/g;->dCm:Landroid/support/v13/app/e;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static diZ(Landroid/app/Fragment;Z)V
    .locals 1

    sget-object v0, Landroid/support/v13/app/g;->dCm:Landroid/support/v13/app/e;

    invoke-interface {v0, p0, p1}, Landroid/support/v13/app/e;->diY(Landroid/app/Fragment;Z)V

    return-void
.end method
