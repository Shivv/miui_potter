.class public final Landroid/support/v4/media/m;
.super Ljava/lang/Object;
.source "MediaDescriptionCompat.java"


# instance fields
.field private eRa:Ljava/lang/String;

.field private eRb:Landroid/net/Uri;

.field private eRc:Landroid/net/Uri;

.field private eRd:Ljava/lang/CharSequence;

.field private eRe:Landroid/graphics/Bitmap;

.field private eRf:Ljava/lang/CharSequence;

.field private mExtras:Landroid/os/Bundle;

.field private mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Landroid/support/v4/media/MediaDescriptionCompat;
    .locals 9

    new-instance v0, Landroid/support/v4/media/MediaDescriptionCompat;

    iget-object v1, p0, Landroid/support/v4/media/m;->eRa:Ljava/lang/String;

    iget-object v2, p0, Landroid/support/v4/media/m;->mTitle:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/v4/media/m;->eRd:Ljava/lang/CharSequence;

    iget-object v4, p0, Landroid/support/v4/media/m;->eRf:Ljava/lang/CharSequence;

    iget-object v5, p0, Landroid/support/v4/media/m;->eRe:Landroid/graphics/Bitmap;

    iget-object v6, p0, Landroid/support/v4/media/m;->eRc:Landroid/net/Uri;

    iget-object v7, p0, Landroid/support/v4/media/m;->mExtras:Landroid/os/Bundle;

    iget-object v8, p0, Landroid/support/v4/media/m;->eRb:Landroid/net/Uri;

    invoke-direct/range {v0 .. v8}, Landroid/support/v4/media/MediaDescriptionCompat;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/net/Uri;Landroid/os/Bundle;Landroid/net/Uri;)V

    return-object v0
.end method

.method public eli(Landroid/graphics/Bitmap;)Landroid/support/v4/media/m;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/m;->eRe:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method public elj(Landroid/net/Uri;)Landroid/support/v4/media/m;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/m;->eRb:Landroid/net/Uri;

    return-object p0
.end method

.method public elk(Landroid/os/Bundle;)Landroid/support/v4/media/m;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/m;->mExtras:Landroid/os/Bundle;

    return-object p0
.end method

.method public ell(Ljava/lang/CharSequence;)Landroid/support/v4/media/m;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/m;->eRf:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public elm(Ljava/lang/String;)Landroid/support/v4/media/m;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/m;->eRa:Ljava/lang/String;

    return-object p0
.end method

.method public eln(Landroid/net/Uri;)Landroid/support/v4/media/m;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/m;->eRc:Landroid/net/Uri;

    return-object p0
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)Landroid/support/v4/media/m;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/m;->eRd:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/support/v4/media/m;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/m;->mTitle:Ljava/lang/CharSequence;

    return-object p0
.end method
