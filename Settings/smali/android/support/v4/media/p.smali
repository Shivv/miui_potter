.class Landroid/support/v4/media/p;
.super Ljava/lang/Object;
.source "MediaBrowserCompat.java"

# interfaces
.implements Landroid/support/v4/media/t;


# instance fields
.field final synthetic eRu:Landroid/support/v4/media/j;


# direct methods
.method constructor <init>(Landroid/support/v4/media/j;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/p;->eRu:Landroid/support/v4/media/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public elB(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/media/p;->eRu:Landroid/support/v4/media/j;

    invoke-virtual {v0, v1}, Landroid/support/v4/media/j;->elh(Landroid/support/v4/media/MediaBrowserCompat$MediaItem;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v0, Landroid/support/v4/media/MediaBrowserCompat$MediaItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/MediaBrowserCompat$MediaItem;

    invoke-virtual {p1}, Landroid/os/Parcel;->recycle()V

    iget-object v1, p0, Landroid/support/v4/media/p;->eRu:Landroid/support/v4/media/j;

    invoke-virtual {v1, v0}, Landroid/support/v4/media/j;->elh(Landroid/support/v4/media/MediaBrowserCompat$MediaItem;)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/p;->eRu:Landroid/support/v4/media/j;

    invoke-virtual {v0, p1}, Landroid/support/v4/media/j;->onError(Ljava/lang/String;)V

    return-void
.end method
