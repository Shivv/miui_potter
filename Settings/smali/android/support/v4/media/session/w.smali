.class Landroid/support/v4/media/session/w;
.super Landroid/media/session/MediaController$Callback;
.source "MediaControllerCompatApi21.java"


# instance fields
.field protected final eQK:Landroid/support/v4/media/session/x;


# direct methods
.method public constructor <init>(Landroid/support/v4/media/session/x;)V
    .locals 0

    invoke-direct {p0}, Landroid/media/session/MediaController$Callback;-><init>()V

    iput-object p1, p0, Landroid/support/v4/media/session/w;->eQK:Landroid/support/v4/media/session/x;

    return-void
.end method


# virtual methods
.method public onAudioInfoChanged(Landroid/media/session/MediaController$PlaybackInfo;)V
    .locals 6

    iget-object v0, p0, Landroid/support/v4/media/session/w;->eQK:Landroid/support/v4/media/session/x;

    invoke-virtual {p1}, Landroid/media/session/MediaController$PlaybackInfo;->getPlaybackType()I

    move-result v1

    invoke-static {p1}, Landroid/support/v4/media/session/n;->ekG(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {p1}, Landroid/media/session/MediaController$PlaybackInfo;->getVolumeControl()I

    move-result v3

    invoke-virtual {p1}, Landroid/media/session/MediaController$PlaybackInfo;->getMaxVolume()I

    move-result v4

    invoke-virtual {p1}, Landroid/media/session/MediaController$PlaybackInfo;->getCurrentVolume()I

    move-result v5

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/media/session/x;->ekJ(IIIII)V

    return-void
.end method

.method public onExtrasChanged(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/session/w;->eQK:Landroid/support/v4/media/session/x;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/x;->onExtrasChanged(Landroid/os/Bundle;)V

    return-void
.end method

.method public onMetadataChanged(Landroid/media/MediaMetadata;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/session/w;->eQK:Landroid/support/v4/media/session/x;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/x;->ekL(Ljava/lang/Object;)V

    return-void
.end method

.method public onPlaybackStateChanged(Landroid/media/session/PlaybackState;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/session/w;->eQK:Landroid/support/v4/media/session/x;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/x;->ekK(Ljava/lang/Object;)V

    return-void
.end method

.method public onQueueChanged(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/session/w;->eQK:Landroid/support/v4/media/session/x;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/x;->onQueueChanged(Ljava/util/List;)V

    return-void
.end method

.method public onQueueTitleChanged(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/session/w;->eQK:Landroid/support/v4/media/session/x;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/x;->onQueueTitleChanged(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onSessionDestroyed()V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/session/w;->eQK:Landroid/support/v4/media/session/x;

    invoke-interface {v0}, Landroid/support/v4/media/session/x;->onSessionDestroyed()V

    return-void
.end method

.method public onSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/session/w;->eQK:Landroid/support/v4/media/session/x;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/x;->onSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method
