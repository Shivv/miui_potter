.class Landroid/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21$ExtraBinderRequestResultReceiver;
.super Landroid/os/ResultReceiver;
.source "MediaControllerCompat.java"


# instance fields
.field private eQk:Ljava/lang/ref/WeakReference;


# virtual methods
.method protected onReceiveResult(ILandroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21$ExtraBinderRequestResultReceiver;->eQk:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/r;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v1, "android.support.v4.media.session.EXTRA_BINDER"

    invoke-static {p2, v1}, Landroid/support/v4/app/W;->edS(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/media/session/v;->asInterface(Landroid/os/IBinder;)Landroid/support/v4/media/session/i;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/media/session/r;->ekN(Landroid/support/v4/media/session/r;Landroid/support/v4/media/session/i;)Landroid/support/v4/media/session/i;

    invoke-static {v0}, Landroid/support/v4/media/session/r;->ekM(Landroid/support/v4/media/session/r;)V

    return-void
.end method
