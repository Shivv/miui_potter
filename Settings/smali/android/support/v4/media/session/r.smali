.class Landroid/support/v4/media/session/r;
.super Ljava/lang/Object;
.source "MediaControllerCompat.java"


# instance fields
.field private final eQB:Ljava/util/List;

.field private eQC:Ljava/util/HashMap;

.field private eQD:Landroid/support/v4/media/session/i;


# direct methods
.method static synthetic ekM(Landroid/support/v4/media/session/r;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/media/session/r;->ekO()V

    return-void
.end method

.method static synthetic ekN(Landroid/support/v4/media/session/r;Landroid/support/v4/media/session/i;)Landroid/support/v4/media/session/i;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/media/session/r;->eQD:Landroid/support/v4/media/session/i;

    return-object p1
.end method

.method private ekO()V
    .locals 5

    iget-object v0, p0, Landroid/support/v4/media/session/r;->eQD:Landroid/support/v4/media/session/i;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Landroid/support/v4/media/session/r;->eQB:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/media/session/r;->eQB:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/c;

    new-instance v3, Landroid/support/v4/media/session/A;

    invoke-direct {v3, v0}, Landroid/support/v4/media/session/A;-><init>(Landroid/support/v4/media/session/c;)V

    iget-object v4, p0, Landroid/support/v4/media/session/r;->eQC:Ljava/util/HashMap;

    invoke-virtual {v4, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x1

    iput-boolean v4, v0, Landroid/support/v4/media/session/c;->eQi:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v4, p0, Landroid/support/v4/media/session/r;->eQD:Landroid/support/v4/media/session/i;

    invoke-interface {v4, v3}, Landroid/support/v4/media/session/i;->ekD(Landroid/support/v4/media/session/y;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v0}, Landroid/support/v4/media/session/c;->ejs()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string/jumbo v2, "MediaControllerCompat"

    const-string/jumbo v3, "Dead object in registerCallback."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    iget-object v0, p0, Landroid/support/v4/media/session/r;->eQB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v1

    return-void
.end method
