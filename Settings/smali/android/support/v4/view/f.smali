.class Landroid/support/v4/view/f;
.super Landroid/support/v4/view/e;
.source "AccessibilityDelegateCompat.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/view/e;-><init>()V

    return-void
.end method


# virtual methods
.method public dOa(Landroid/support/v4/view/d;)Landroid/view/View$AccessibilityDelegate;
    .locals 1

    new-instance v0, Landroid/support/v4/view/h;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/view/h;-><init>(Landroid/support/v4/view/f;Landroid/support/v4/view/d;)V

    return-object v0
.end method

.method public dOb(Landroid/view/View$AccessibilityDelegate;Landroid/view/View;)Landroid/support/v4/view/a/b;
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View$AccessibilityDelegate;->getAccessibilityNodeProvider(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/support/v4/view/a/b;

    invoke-direct {v1, v0}, Landroid/support/v4/view/a/b;-><init>(Ljava/lang/Object;)V

    return-object v1

    :cond_0
    return-object v1
.end method

.method public dOc(Landroid/view/View$AccessibilityDelegate;Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    invoke-virtual {p1, p2, p3, p4}, Landroid/view/View$AccessibilityDelegate;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method
