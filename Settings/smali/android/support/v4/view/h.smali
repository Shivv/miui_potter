.class final Landroid/support/v4/view/h;
.super Landroid/view/View$AccessibilityDelegate;
.source "AccessibilityDelegateCompat.java"


# instance fields
.field final synthetic exG:Landroid/support/v4/view/f;

.field final synthetic exH:Landroid/support/v4/view/d;


# direct methods
.method constructor <init>(Landroid/support/v4/view/f;Landroid/support/v4/view/d;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/view/h;->exG:Landroid/support/v4/view/f;

    iput-object p2, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/d;->dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public getAccessibilityNodeProvider(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-virtual {v1, p1}, Landroid/support/v4/view/d;->getAccessibilityNodeProvider(Landroid/view/View;)Landroid/support/v4/view/a/b;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/view/a/b;->dNE()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeProvider;

    :cond_0
    return-object v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/d;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-static {p2}, Landroid/support/v4/view/a/a;->dND(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/support/v4/view/a/a;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/view/d;->ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V

    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/d;->onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/view/d;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/view/d;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public sendAccessibilityEvent(Landroid/view/View;I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/d;->sendAccessibilityEvent(Landroid/view/View;I)V

    return-void
.end method

.method public sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/view/h;->exH:Landroid/support/v4/view/d;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/d;->sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method
