.class Landroid/support/v4/view/ac;
.super Ljava/lang/Object;
.source "ViewParentCompat.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dRc(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
    .locals 1

    instance-of v0, p1, Landroid/support/v4/view/as;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v4/view/as;

    invoke-interface {p1, p2, p3, p4}, Landroid/support/v4/view/as;->onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public dRd(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
    .locals 1

    instance-of v0, p1, Landroid/support/v4/view/as;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v4/view/as;

    invoke-interface {p1, p2, p3, p4, p5}, Landroid/support/v4/view/as;->onNestedFling(Landroid/view/View;FFZ)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public dRe(Landroid/view/ViewParent;Landroid/view/View;FF)Z
    .locals 1

    instance-of v0, p1, Landroid/support/v4/view/as;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v4/view/as;

    invoke-interface {p1, p2, p3, p4}, Landroid/support/v4/view/as;->onNestedPreFling(Landroid/view/View;FF)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public dRf(Landroid/view/ViewParent;Landroid/view/View;IIII)V
    .locals 6

    instance-of v0, p1, Landroid/support/v4/view/as;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/support/v4/view/as;

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/view/as;->onNestedScroll(Landroid/view/View;IIII)V

    :cond_0
    return-void
.end method

.method public dRg(Landroid/view/ViewParent;Landroid/view/View;II[I)V
    .locals 1

    instance-of v0, p1, Landroid/support/v4/view/as;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v4/view/as;

    invoke-interface {p1, p2, p3, p4, p5}, Landroid/support/v4/view/as;->onNestedPreScroll(Landroid/view/View;II[I)V

    :cond_0
    return-void
.end method

.method public dRh(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z
    .locals 1

    instance-of v0, p1, Landroid/support/v4/view/as;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v4/view/as;

    invoke-interface {p1, p2, p3, p4}, Landroid/support/v4/view/as;->onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public dRi(Landroid/view/ViewParent;Landroid/view/View;)V
    .locals 1

    instance-of v0, p1, Landroid/support/v4/view/as;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v4/view/as;

    invoke-interface {p1, p2}, Landroid/support/v4/view/as;->onStopNestedScroll(Landroid/view/View;)V

    :cond_0
    return-void
.end method
