.class public final Landroid/support/v4/view/aa;
.super Ljava/lang/Object;
.source "MenuItemCompat.java"


# static fields
.field static final eAn:Landroid/support/v4/view/T;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/view/ag;

    invoke-direct {v0}, Landroid/support/v4/view/ag;-><init>()V

    sput-object v0, Landroid/support/v4/view/aa;->eAn:Landroid/support/v4/view/T;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/support/v4/view/af;

    invoke-direct {v0}, Landroid/support/v4/view/af;-><init>()V

    sput-object v0, Landroid/support/v4/view/aa;->eAn:Landroid/support/v4/view/T;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dRr(Landroid/view/MenuItem;Ljava/lang/CharSequence;)V
    .locals 1

    instance-of v0, p0, Landroid/support/v4/b/a/c;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/support/v4/b/a/c;

    invoke-interface {p0, p1}, Landroid/support/v4/b/a/c;->setContentDescription(Ljava/lang/CharSequence;)Landroid/support/v4/b/a/c;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/support/v4/view/aa;->eAn:Landroid/support/v4/view/T;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/T;->dQW(Landroid/view/MenuItem;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static dRs(Landroid/view/MenuItem;Landroid/content/res/ColorStateList;)V
    .locals 1

    instance-of v0, p0, Landroid/support/v4/b/a/c;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/support/v4/b/a/c;

    invoke-interface {p0, p1}, Landroid/support/v4/b/a/c;->setIconTintList(Landroid/content/res/ColorStateList;)Landroid/view/MenuItem;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/support/v4/view/aa;->eAn:Landroid/support/v4/view/T;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/T;->dQX(Landroid/view/MenuItem;Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method public static dRt(Landroid/view/MenuItem;Ljava/lang/CharSequence;)V
    .locals 1

    instance-of v0, p0, Landroid/support/v4/b/a/c;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/support/v4/b/a/c;

    invoke-interface {p0, p1}, Landroid/support/v4/b/a/c;->setTooltipText(Ljava/lang/CharSequence;)Landroid/support/v4/b/a/c;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/support/v4/view/aa;->eAn:Landroid/support/v4/view/T;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/T;->dQZ(Landroid/view/MenuItem;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static dRu(Landroid/view/MenuItem;Landroid/support/v4/view/ai;)Landroid/view/MenuItem;
    .locals 2

    instance-of v0, p0, Landroid/support/v4/b/a/c;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/support/v4/b/a/c;

    invoke-interface {p0, p1}, Landroid/support/v4/b/a/c;->dJt(Landroid/support/v4/view/ai;)Landroid/support/v4/b/a/c;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "MenuItemCompat"

    const-string/jumbo v1, "setActionProvider: item does not implement SupportMenuItem; ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method public static dRv(Landroid/view/MenuItem;CI)V
    .locals 1

    instance-of v0, p0, Landroid/support/v4/b/a/c;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/support/v4/b/a/c;

    invoke-interface {p0, p1, p2}, Landroid/support/v4/b/a/c;->setAlphabeticShortcut(CI)Landroid/view/MenuItem;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/support/v4/view/aa;->eAn:Landroid/support/v4/view/T;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/view/T;->dRb(Landroid/view/MenuItem;CI)V

    goto :goto_0
.end method

.method public static dRw(Landroid/view/MenuItem;CI)V
    .locals 1

    instance-of v0, p0, Landroid/support/v4/b/a/c;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/support/v4/b/a/c;

    invoke-interface {p0, p1, p2}, Landroid/support/v4/b/a/c;->setNumericShortcut(CI)Landroid/view/MenuItem;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/support/v4/view/aa;->eAn:Landroid/support/v4/view/T;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/view/T;->dQY(Landroid/view/MenuItem;CI)V

    goto :goto_0
.end method

.method public static dRx(Landroid/view/MenuItem;Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    instance-of v0, p0, Landroid/support/v4/b/a/c;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/support/v4/b/a/c;

    invoke-interface {p0, p1}, Landroid/support/v4/b/a/c;->setIconTintMode(Landroid/graphics/PorterDuff$Mode;)Landroid/view/MenuItem;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/support/v4/view/aa;->eAn:Landroid/support/v4/view/T;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/T;->dRa(Landroid/view/MenuItem;Landroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method
