.class public final Landroid/support/v4/h/b;
.super Ljava/lang/Object;
.source "BidiFormatter.java"


# instance fields
.field private eTj:Z

.field private eTk:Landroid/support/v4/h/c;

.field private mFlags:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/h/l;->eoj(Ljava/util/Locale;)Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v4/h/b;->enW(Z)V

    return-void
.end method

.method private enW(Z)V
    .locals 1

    iput-boolean p1, p0, Landroid/support/v4/h/b;->eTj:Z

    invoke-static {}, Landroid/support/v4/h/l;->eot()Landroid/support/v4/h/c;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/h/b;->eTk:Landroid/support/v4/h/c;

    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v4/h/b;->mFlags:I

    return-void
.end method

.method private static enX(Z)Landroid/support/v4/h/l;
    .locals 1

    if-eqz p0, :cond_0

    invoke-static {}, Landroid/support/v4/h/l;->eol()Landroid/support/v4/h/l;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Landroid/support/v4/h/l;->-get0()Landroid/support/v4/h/l;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public build()Landroid/support/v4/h/l;
    .locals 5

    iget v0, p0, Landroid/support/v4/h/b;->mFlags:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v4/h/b;->eTk:Landroid/support/v4/h/c;

    invoke-static {}, Landroid/support/v4/h/l;->eot()Landroid/support/v4/h/c;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/h/b;->eTj:Z

    invoke-static {v0}, Landroid/support/v4/h/b;->enX(Z)Landroid/support/v4/h/l;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v4/h/l;

    iget-boolean v1, p0, Landroid/support/v4/h/b;->eTj:Z

    iget v2, p0, Landroid/support/v4/h/b;->mFlags:I

    iget-object v3, p0, Landroid/support/v4/h/b;->eTk:Landroid/support/v4/h/c;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/support/v4/h/l;-><init>(ZILandroid/support/v4/h/c;Landroid/support/v4/h/l;)V

    return-object v0
.end method
