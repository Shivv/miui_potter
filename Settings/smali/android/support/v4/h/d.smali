.class abstract Landroid/support/v4/h/d;
.super Ljava/lang/Object;
.source "TextDirectionHeuristicsCompat.java"

# interfaces
.implements Landroid/support/v4/h/c;


# instance fields
.field private final eTl:Landroid/support/v4/h/f;


# direct methods
.method public constructor <init>(Landroid/support/v4/h/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v4/h/d;->eTl:Landroid/support/v4/h/f;

    return-void
.end method

.method private eoa(Ljava/lang/CharSequence;II)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/h/d;->eTl:Landroid/support/v4/h/f;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/h/f;->eob(Ljava/lang/CharSequence;II)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Landroid/support/v4/h/d;->enZ()Z

    move-result v0

    return v0

    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    const/4 v0, 0x0

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public enY(Ljava/lang/CharSequence;II)Z
    .locals 1

    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    if-ltz p3, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    sub-int/2addr v0, p3

    if-lt v0, p2, :cond_0

    iget-object v0, p0, Landroid/support/v4/h/d;->eTl:Landroid/support/v4/h/f;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/h/d;->enZ()Z

    move-result v0

    return v0

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/h/d;->eoa(Ljava/lang/CharSequence;II)Z

    move-result v0

    return v0
.end method

.method protected abstract enZ()Z
.end method
