.class public final Landroid/support/v4/h/l;
.super Ljava/lang/Object;
.source "BidiFormatter.java"


# static fields
.field private static final eTA:Landroid/support/v4/h/l;

.field private static final eTB:Landroid/support/v4/h/l;

.field private static eTD:Landroid/support/v4/h/c;

.field private static final eTE:Ljava/lang/String;

.field private static final eTG:Ljava/lang/String;


# instance fields
.field private final eTC:Landroid/support/v4/h/c;

.field private final eTF:Z

.field private final mFlags:I


# direct methods
.method static synthetic -get0()Landroid/support/v4/h/l;
    .locals 1

    sget-object v0, Landroid/support/v4/h/l;->eTA:Landroid/support/v4/h/l;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    sget-object v0, Landroid/support/v4/h/k;->eTu:Landroid/support/v4/h/c;

    sput-object v0, Landroid/support/v4/h/l;->eTD:Landroid/support/v4/h/c;

    const/16 v0, 0x200e

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v4/h/l;->eTG:Ljava/lang/String;

    const/16 v0, 0x200f

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v4/h/l;->eTE:Ljava/lang/String;

    new-instance v0, Landroid/support/v4/h/l;

    sget-object v1, Landroid/support/v4/h/l;->eTD:Landroid/support/v4/h/c;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v3, v1}, Landroid/support/v4/h/l;-><init>(ZILandroid/support/v4/h/c;)V

    sput-object v0, Landroid/support/v4/h/l;->eTA:Landroid/support/v4/h/l;

    new-instance v0, Landroid/support/v4/h/l;

    sget-object v1, Landroid/support/v4/h/l;->eTD:Landroid/support/v4/h/c;

    const/4 v2, 0x1

    invoke-direct {v0, v2, v3, v1}, Landroid/support/v4/h/l;-><init>(ZILandroid/support/v4/h/c;)V

    sput-object v0, Landroid/support/v4/h/l;->eTB:Landroid/support/v4/h/l;

    return-void
.end method

.method private constructor <init>(ZILandroid/support/v4/h/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Landroid/support/v4/h/l;->eTF:Z

    iput p2, p0, Landroid/support/v4/h/l;->mFlags:I

    iput-object p3, p0, Landroid/support/v4/h/l;->eTC:Landroid/support/v4/h/c;

    return-void
.end method

.method synthetic constructor <init>(ZILandroid/support/v4/h/c;Landroid/support/v4/h/l;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/h/l;-><init>(ZILandroid/support/v4/h/c;)V

    return-void
.end method

.method static synthetic eoj(Ljava/util/Locale;)Z
    .locals 1

    invoke-static {p0}, Landroid/support/v4/h/l;->eom(Ljava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method private static eok(Ljava/lang/CharSequence;)I
    .locals 2

    new-instance v0, Landroid/support/v4/h/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v4/h/a;-><init>(Ljava/lang/CharSequence;Z)V

    invoke-virtual {v0}, Landroid/support/v4/h/a;->enT()I

    move-result v0

    return v0
.end method

.method static synthetic eol()Landroid/support/v4/h/l;
    .locals 1

    sget-object v0, Landroid/support/v4/h/l;->eTB:Landroid/support/v4/h/l;

    return-object v0
.end method

.method private static eom(Ljava/util/Locale;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-static {p0}, Landroid/support/v4/h/g;->eod(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private eon(Ljava/lang/CharSequence;Landroid/support/v4/h/c;)Ljava/lang/String;
    .locals 3

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-interface {p2, p1, v1, v0}, Landroid/support/v4/h/c;->enY(Ljava/lang/CharSequence;II)Z

    move-result v0

    iget-boolean v1, p0, Landroid/support/v4/h/l;->eTF:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/support/v4/h/l;->eok(Ljava/lang/CharSequence;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_0
    sget-object v0, Landroid/support/v4/h/l;->eTG:Ljava/lang/String;

    return-object v0

    :cond_1
    iget-boolean v1, p0, Landroid/support/v4/h/l;->eTF:Z

    if-eqz v1, :cond_3

    if-eqz v0, :cond_2

    invoke-static {p1}, Landroid/support/v4/h/l;->eok(Ljava/lang/CharSequence;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    :cond_2
    sget-object v0, Landroid/support/v4/h/l;->eTE:Ljava/lang/String;

    return-object v0

    :cond_3
    const-string/jumbo v0, ""

    return-object v0
.end method

.method private eoo(Ljava/lang/CharSequence;Landroid/support/v4/h/c;)Ljava/lang/String;
    .locals 3

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-interface {p2, p1, v1, v0}, Landroid/support/v4/h/c;->enY(Ljava/lang/CharSequence;II)Z

    move-result v0

    iget-boolean v1, p0, Landroid/support/v4/h/l;->eTF:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/support/v4/h/l;->eop(Ljava/lang/CharSequence;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_0
    sget-object v0, Landroid/support/v4/h/l;->eTG:Ljava/lang/String;

    return-object v0

    :cond_1
    iget-boolean v1, p0, Landroid/support/v4/h/l;->eTF:Z

    if-eqz v1, :cond_3

    if-eqz v0, :cond_2

    invoke-static {p1}, Landroid/support/v4/h/l;->eop(Ljava/lang/CharSequence;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    :cond_2
    sget-object v0, Landroid/support/v4/h/l;->eTE:Ljava/lang/String;

    return-object v0

    :cond_3
    const-string/jumbo v0, ""

    return-object v0
.end method

.method private static eop(Ljava/lang/CharSequence;)I
    .locals 2

    new-instance v0, Landroid/support/v4/h/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v4/h/a;-><init>(Ljava/lang/CharSequence;Z)V

    invoke-virtual {v0}, Landroid/support/v4/h/a;->enQ()I

    move-result v0

    return v0
.end method

.method static synthetic eot()Landroid/support/v4/h/c;
    .locals 1

    sget-object v0, Landroid/support/v4/h/l;->eTD:Landroid/support/v4/h/c;

    return-object v0
.end method

.method public static getInstance()Landroid/support/v4/h/l;
    .locals 1

    new-instance v0, Landroid/support/v4/h/b;

    invoke-direct {v0}, Landroid/support/v4/h/b;-><init>()V

    invoke-virtual {v0}, Landroid/support/v4/h/b;->build()Landroid/support/v4/h/l;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public eoq(Ljava/lang/CharSequence;Landroid/support/v4/h/c;Z)Ljava/lang/CharSequence;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-interface {p2, p1, v1, v0}, Landroid/support/v4/h/c;->enY(Ljava/lang/CharSequence;II)Z

    move-result v1

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/support/v4/h/l;->eor()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    if-eqz v1, :cond_3

    sget-object v0, Landroid/support/v4/h/k;->eTz:Landroid/support/v4/h/c;

    :goto_0
    invoke-direct {p0, p1, v0}, Landroid/support/v4/h/l;->eoo(Ljava/lang/CharSequence;Landroid/support/v4/h/c;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    iget-boolean v0, p0, Landroid/support/v4/h/l;->eTF:Z

    if-eq v1, v0, :cond_5

    if-eqz v1, :cond_4

    const/16 v0, 0x202b

    :goto_1
    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 v0, 0x202c

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :goto_2
    if-eqz p3, :cond_2

    if-eqz v1, :cond_6

    sget-object v0, Landroid/support/v4/h/k;->eTz:Landroid/support/v4/h/c;

    :goto_3
    invoke-direct {p0, p1, v0}, Landroid/support/v4/h/l;->eon(Ljava/lang/CharSequence;Landroid/support/v4/h/c;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    return-object v2

    :cond_3
    sget-object v0, Landroid/support/v4/h/k;->eTv:Landroid/support/v4/h/c;

    goto :goto_0

    :cond_4
    const/16 v0, 0x202a

    goto :goto_1

    :cond_5
    invoke-virtual {v2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    :cond_6
    sget-object v0, Landroid/support/v4/h/k;->eTv:Landroid/support/v4/h/c;

    goto :goto_3
.end method

.method public eor()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v4/h/l;->mFlags:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public eos(Ljava/lang/CharSequence;Landroid/support/v4/h/c;)Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/h/l;->eoq(Ljava/lang/CharSequence;Landroid/support/v4/h/c;Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
