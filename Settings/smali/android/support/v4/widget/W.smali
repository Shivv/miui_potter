.class public final Landroid/support/v4/widget/W;
.super Ljava/lang/Object;
.source "EdgeEffectCompat.java"


# static fields
.field private static final eFu:Landroid/support/v4/widget/B;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/widget/v;

    invoke-direct {v0}, Landroid/support/v4/widget/v;-><init>()V

    sput-object v0, Landroid/support/v4/widget/W;->eFu:Landroid/support/v4/widget/B;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/support/v4/widget/B;

    invoke-direct {v0}, Landroid/support/v4/widget/B;-><init>()V

    sput-object v0, Landroid/support/v4/widget/W;->eFu:Landroid/support/v4/widget/B;

    goto :goto_0
.end method

.method public static dXq(Landroid/widget/EdgeEffect;FF)V
    .locals 1

    sget-object v0, Landroid/support/v4/widget/W;->eFu:Landroid/support/v4/widget/B;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/widget/B;->dUW(Landroid/widget/EdgeEffect;FF)V

    return-void
.end method
