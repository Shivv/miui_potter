.class final Landroid/support/v4/widget/ah;
.super Ljava/lang/Object;
.source "SwipeRefreshLayout.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic eGa:Landroid/support/v4/widget/SwipeRefreshLayout;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/I;->setAlpha(I)V

    iget-object v0, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0}, Landroid/support/v4/widget/I;->start()V

    iget-object v0, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGt:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGN:Landroid/support/v4/widget/aI;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGN:Landroid/support/v4/widget/aI;

    invoke-interface {v0}, Landroid/support/v4/widget/aI;->dZq()V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, v1, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1}, Landroid/support/v4/widget/T;->getTop()I

    move-result v1

    iput v1, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/ah;->eGa:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYc()V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
