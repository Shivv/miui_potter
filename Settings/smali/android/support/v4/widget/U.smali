.class final Landroid/support/v4/widget/U;
.super Ljava/lang/Object;
.source "CircularProgressDrawable.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic eFr:Landroid/support/v4/widget/R;

.field final synthetic eFs:Landroid/support/v4/widget/I;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/I;Landroid/support/v4/widget/R;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/widget/U;->eFs:Landroid/support/v4/widget/I;

    iput-object p2, p0, Landroid/support/v4/widget/U;->eFr:Landroid/support/v4/widget/R;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 5

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v0, p0, Landroid/support/v4/widget/U;->eFs:Landroid/support/v4/widget/I;

    iget-object v1, p0, Landroid/support/v4/widget/U;->eFr:Landroid/support/v4/widget/R;

    const/4 v2, 0x1

    invoke-static {v0, v3, v1, v2}, Landroid/support/v4/widget/I;->dVr(Landroid/support/v4/widget/I;FLandroid/support/v4/widget/R;Z)V

    iget-object v0, p0, Landroid/support/v4/widget/U;->eFr:Landroid/support/v4/widget/R;

    invoke-virtual {v0}, Landroid/support/v4/widget/R;->dWD()V

    iget-object v0, p0, Landroid/support/v4/widget/U;->eFr:Landroid/support/v4/widget/R;

    invoke-virtual {v0}, Landroid/support/v4/widget/R;->dWF()V

    iget-object v0, p0, Landroid/support/v4/widget/U;->eFs:Landroid/support/v4/widget/I;

    invoke-static {v0}, Landroid/support/v4/widget/I;->dVq(Landroid/support/v4/widget/I;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/U;->eFs:Landroid/support/v4/widget/I;

    invoke-static {v0, v4}, Landroid/support/v4/widget/I;->dVG(Landroid/support/v4/widget/I;Z)Z

    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    const-wide/16 v0, 0x534

    invoke-virtual {p1, v0, v1}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object v0, p0, Landroid/support/v4/widget/U;->eFr:Landroid/support/v4/widget/R;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/R;->dWn(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/U;->eFs:Landroid/support/v4/widget/I;

    iget-object v1, p0, Landroid/support/v4/widget/U;->eFs:Landroid/support/v4/widget/I;

    invoke-static {v1}, Landroid/support/v4/widget/I;->dVI(Landroid/support/v4/widget/I;)F

    move-result v1

    add-float/2addr v1, v3

    invoke-static {v0, v1}, Landroid/support/v4/widget/I;->dVz(Landroid/support/v4/widget/I;F)F

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/U;->eFs:Landroid/support/v4/widget/I;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/widget/I;->dVz(Landroid/support/v4/widget/I;F)F

    return-void
.end method
