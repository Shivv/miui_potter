.class public Landroid/support/v4/widget/SwipeRefreshLayout;
.super Landroid/view/ViewGroup;
.source "SwipeRefreshLayout.java"

# interfaces
.implements Landroid/support/v4/view/as;
.implements Landroid/support/v4/view/b;


# static fields
.field static final CIRCLE_DIAMETER:I = 0x28

.field static final CIRCLE_DIAMETER_LARGE:I = 0x38

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final eGo:[I


# instance fields
.field private eGA:Landroid/view/animation/Animation;

.field eGB:F

.field private eGC:Landroid/view/animation/Animation;

.field private eGD:Z

.field private eGE:Z

.field private eGF:Landroid/view/View;

.field private final eGG:[I

.field private final eGH:Landroid/support/v4/view/Z;

.field private eGI:I

.field eGJ:Z

.field private final eGK:Landroid/view/animation/Animation;

.field eGL:I

.field private final eGM:Landroid/view/animation/Animation;

.field eGN:Landroid/support/v4/widget/aI;

.field eGO:Landroid/support/v4/widget/I;

.field private eGP:F

.field protected eGc:I

.field private eGd:I

.field private final eGe:Landroid/support/v4/view/a;

.field private eGf:F

.field private eGg:Z

.field private eGh:Landroid/view/animation/Animation;

.field eGi:Landroid/support/v4/widget/T;

.field protected eGj:I

.field private eGk:Landroid/view/animation/Animation;

.field private eGl:Landroid/view/animation/Animation;

.field private eGm:F

.field eGn:Z

.field private final eGp:Landroid/view/animation/DecelerateInterpolator;

.field private eGq:I

.field private eGr:F

.field private eGs:I

.field eGt:Z

.field eGu:Z

.field eGv:I

.field private eGw:Landroid/support/v4/widget/as;

.field private eGx:I

.field private final eGy:[I

.field private eGz:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->LOG_TAG:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x101000e

    const/4 v2, 0x0

    aput v1, v0, v2

    sput-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGo:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v2, 0x2

    const/4 v1, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGP:F

    new-array v0, v2, [I

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGy:[I

    new-array v0, v2, [I

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGG:[I

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGx:I

    new-instance v0, Landroid/support/v4/widget/ah;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/ah;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGz:Landroid/view/animation/Animation$AnimationListener;

    new-instance v0, Landroid/support/v4/widget/y;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/y;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGM:Landroid/view/animation/Animation;

    new-instance v0, Landroid/support/v4/widget/G;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/G;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGK:Landroid/view/animation/Animation;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGq:I

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGd:I

    invoke-virtual {p0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setWillNotDraw(Z)V

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGp:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x42200000    # 40.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGI:I

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYg()V

    invoke-virtual {p0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setChildrenDrawingOrderEnabled(Z)V

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42800000    # 64.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGv:I

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGv:I

    int-to-float v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGP:F

    new-instance v0, Landroid/support/v4/view/Z;

    invoke-direct {v0, p0}, Landroid/support/v4/view/Z;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGH:Landroid/support/v4/view/Z;

    new-instance v0, Landroid/support/v4/view/a;

    invoke-direct {v0, p0}, Landroid/support/v4/view/a;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    invoke-virtual {p0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setNestedScrollingEnabled(Z)V

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGI:I

    neg-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGj:I

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYd(F)V

    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGo:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private dXQ(Landroid/view/MotionEvent;)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    iget v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    if-ne v2, v3, :cond_1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    :cond_1
    return-void
.end method

.method private dXR(ILandroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    iput p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGc:I

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGM:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGM:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGM:Landroid/view/animation/Animation;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGp:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0, p2}, Landroid/support/v4/widget/T;->dXl(Landroid/view/animation/Animation$AnimationListener;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->clearAnimation()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGM:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private dXT()V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1, v2}, Landroid/view/View;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iput-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private dXU(F)V
    .locals 2

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGf:F

    sub-float v0, p1, v0

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGq:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGE:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGf:F

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGq:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGm:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGE:Z

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    const/16 v1, 0x4c

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/I;->setAlpha(I)V

    :cond_0
    return-void
.end method

.method private dXV(F)V
    .locals 14

    const/high16 v6, 0x40800000    # 4.0f

    const v13, 0x3f4ccccd    # 0.8f

    const/4 v12, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v10, 0x3f800000    # 1.0f

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/I;->dVD(Z)V

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGP:F

    div-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v10, v0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-double v2, v1

    const-wide v4, 0x3fd999999999999aL    # 0.4

    sub-double/2addr v2, v4

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    double-to-float v0, v2

    const/high16 v2, 0x40a00000    # 5.0f

    mul-float/2addr v0, v2

    const/high16 v2, 0x40400000    # 3.0f

    div-float v2, v0, v2

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGP:F

    sub-float v3, v0, v3

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGJ:Z

    if-eqz v0, :cond_4

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGv:I

    iget v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGj:I

    sub-int/2addr v0, v4

    :goto_0
    int-to-float v0, v0

    mul-float v4, v0, v11

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    div-float/2addr v3, v0

    invoke-static {v12, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    div-float v4, v3, v6

    float-to-double v4, v4

    div-float/2addr v3, v6

    float-to-double v6, v3

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    sub-double/2addr v4, v6

    double-to-float v3, v4

    mul-float/2addr v3, v11

    mul-float v4, v0, v3

    mul-float/2addr v4, v11

    iget v5, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGj:I

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    float-to-int v0, v0

    add-int/2addr v0, v5

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1}, Landroid/support/v4/widget/T;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/support/v4/widget/T;->setVisibility(I)V

    :cond_0
    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGn:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1, v10}, Landroid/support/v4/widget/T;->setScaleX(F)V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1, v10}, Landroid/support/v4/widget/T;->setScaleY(F)V

    :cond_1
    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGn:Z

    if-eqz v1, :cond_2

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGP:F

    div-float v1, p1, v1

    invoke-static {v10, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setAnimationProgress(F)V

    :cond_2
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGP:F

    cmpg-float v1, p1, v1

    if-gez v1, :cond_5

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v1}, Landroid/support/v4/widget/I;->getAlpha()I

    move-result v1

    const/16 v4, 0x4c

    if-le v1, v4, :cond_3

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGh:Landroid/view/animation/Animation;

    invoke-direct {p0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXW(Landroid/view/animation/Animation;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYb()V

    :cond_3
    :goto_1
    mul-float v1, v2, v13

    iget-object v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-static {v13, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-virtual {v4, v12, v1}, Landroid/support/v4/widget/I;->dVF(FF)V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-static {v10, v2}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-virtual {v1, v4}, Landroid/support/v4/widget/I;->dVE(F)V

    const v1, 0x3ecccccd    # 0.4f

    mul-float/2addr v1, v2

    const/high16 v2, -0x41800000    # -0.25f

    add-float/2addr v1, v2

    mul-float v2, v3, v11

    add-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/I;->dVu(F)V

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setTargetOffsetTopAndBottom(I)V

    return-void

    :cond_4
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGv:I

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v1}, Landroid/support/v4/widget/I;->getAlpha()I

    move-result v1

    const/16 v4, 0xff

    if-ge v1, v4, :cond_3

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGl:Landroid/view/animation/Animation;

    invoke-direct {p0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXW(Landroid/view/animation/Animation;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYh()V

    goto :goto_1
.end method

.method private dXW(Landroid/view/animation/Animation;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private dXX(Landroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->setVisibility(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/I;->setAlpha(I)V

    :cond_0
    new-instance v0, Landroid/support/v4/widget/af;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/af;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGA:Landroid/view/animation/Animation;

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGA:Landroid/view/animation/Animation;

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGd:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/T;->dXl(Landroid/view/animation/Animation$AnimationListener;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->clearAnimation()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGA:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private dXY(ILandroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    iput p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGc:I

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->getScaleX()F

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGB:F

    new-instance v0, Landroid/support/v4/widget/k;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/k;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGC:Landroid/view/animation/Animation;

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGC:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0, p2}, Landroid/support/v4/widget/T;->dXl(Landroid/view/animation/Animation$AnimationListener;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->clearAnimation()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGC:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private dXZ(ILandroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGn:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXY(ILandroid/view/animation/Animation$AnimationListener;)V

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGc:I

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGK:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGK:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGK:Landroid/view/animation/Animation;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGp:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    if-eqz p2, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0, p2}, Landroid/support/v4/widget/T;->dXl(Landroid/view/animation/Animation$AnimationListener;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->clearAnimation()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGK:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private dYa(II)Landroid/view/animation/Animation;
    .locals 4

    new-instance v0, Landroid/support/v4/widget/au;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/v4/widget/au;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;II)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/T;->dXl(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1}, Landroid/support/v4/widget/T;->clearAnimation()V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/T;->startAnimation(Landroid/view/animation/Animation;)V

    return-object v0
.end method

.method private dYb()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0}, Landroid/support/v4/widget/I;->getAlpha()I

    move-result v0

    const/16 v1, 0x4c

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYa(II)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGh:Landroid/view/animation/Animation;

    return-void
.end method

.method private dYe(F)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGP:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    invoke-direct {p0, v3, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(ZZ)V

    :goto_0
    return-void

    :cond_0
    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0, v1, v1}, Landroid/support/v4/widget/I;->dVF(FF)V

    const/4 v0, 0x0

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGn:Z

    if-nez v1, :cond_1

    new-instance v0, Landroid/support/v4/widget/aC;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/aC;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    :cond_1
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    invoke-direct {p0, v1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXZ(ILandroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/I;->dVD(Z)V

    goto :goto_0
.end method

.method private dYg()V
    .locals 3

    new-instance v0, Landroid/support/v4/widget/T;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, -0x50506

    invoke-direct {v0, v1, v2}, Landroid/support/v4/widget/T;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    new-instance v0, Landroid/support/v4/widget/I;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/I;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/I;->dVA(I)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->setVisibility(I)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private dYh()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0}, Landroid/support/v4/widget/I;->getAlpha()I

    move-result v0

    const/16 v1, 0xff

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYa(II)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGl:Landroid/view/animation/Animation;

    return-void
.end method

.method private setColorViewAlpha(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/I;->setAlpha(I)V

    return-void
.end method

.method private setRefreshing(ZZ)V
    .locals 2

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    if-eq v0, p1, :cond_0

    iput-boolean p2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGt:Z

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXT()V

    iput-boolean p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGz:Landroid/view/animation/Animation$AnimationListener;

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXR(ILandroid/view/animation/Animation$AnimationListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGz:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXS(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method


# virtual methods
.method dXS(Landroid/view/animation/Animation$AnimationListener;)V
    .locals 4

    new-instance v0, Landroid/support/v4/widget/A;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/A;-><init>(Landroid/support/v4/widget/SwipeRefreshLayout;)V

    iput-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGk:Landroid/view/animation/Animation;

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGk:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/T;->dXl(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->clearAnimation()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGk:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method dYc()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->clearAnimation()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0}, Landroid/support/v4/widget/I;->stop()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->setVisibility(I)V

    const/16 v0, 0xff

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorViewAlpha(I)V

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setAnimationProgress(F)V

    :goto_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->getTop()I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    return-void

    :cond_0
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGj:I

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setTargetOffsetTopAndBottom(I)V

    goto :goto_0
.end method

.method dYd(F)V
    .locals 3

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGc:I

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGj:I

    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGc:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1}, Landroid/support/v4/widget/T;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setTargetOffsetTopAndBottom(I)V

    return-void
.end method

.method public dYf()Z
    .locals 2

    const/4 v1, -0x1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGw:Landroid/support/v4/widget/as;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGw:Landroid/support/v4/widget/as;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    invoke-interface {v0, p0, v1}, Landroid/support/v4/widget/as;->dYx(Landroid/support/v4/widget/SwipeRefreshLayout;Landroid/view/View;)Z

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    check-cast v0, Landroid/widget/ListView;

    invoke-static {v0, v1}, Landroid/support/v4/widget/K;->dVK(Landroid/widget/ListView;I)Z

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedFling(FFZ)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/view/a;->dispatchNestedFling(FFZ)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedPreFling(FF)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/a;->dispatchNestedPreFling(FF)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedPreScroll(II[I[I)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/view/a;->dispatchNestedPreScroll(II[I[I)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedScroll(IIII[I)Z
    .locals 6

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/a;->dispatchNestedScroll(IIII[I)Z

    move-result v0

    return v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGx:I

    if-gez v0, :cond_0

    return p2

    :cond_0
    add-int/lit8 v0, p1, -0x1

    if-ne p2, v0, :cond_1

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGx:I

    return v0

    :cond_1
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGx:I

    if-lt p2, v0, :cond_2

    add-int/lit8 v0, p2, 0x1

    return v0

    :cond_2
    return p2
.end method

.method public getNestedScrollAxes()I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGH:Landroid/support/v4/view/Z;

    invoke-virtual {v0}, Landroid/support/v4/view/Z;->getNestedScrollAxes()I

    move-result v0

    return v0
.end method

.method public getProgressCircleDiameter()I
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGI:I

    return v0
.end method

.method public getProgressViewEndOffset()I
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGv:I

    return v0
.end method

.method public getProgressViewStartOffset()I
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGj:I

    return v0
.end method

.method public hasNestedScrollingParent()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    invoke-virtual {v0}, Landroid/support/v4/view/a;->hasNestedScrollingParent()Z

    move-result v0

    return v0
.end method

.method public isNestedScrollingEnabled()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    invoke-virtual {v0}, Landroid/support/v4/view/a;->isNestedScrollingEnabled()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYc()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXT()V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGg:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGg:Z

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGg:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYf()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGD:Z

    if-eqz v1, :cond_2

    :cond_1
    return v2

    :cond_2
    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGE:Z

    return v0

    :pswitch_1
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGj:I

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1}, Landroid/support/v4/widget/T;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setTargetOffsetTopAndBottom(I)V

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGE:Z

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_3

    return v2

    :cond_3
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGf:F

    goto :goto_0

    :pswitch_2
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    if-ne v0, v3, :cond_4

    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "Got ACTION_MOVE event but don\'t have an active pointer id."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_4
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_5

    return v2

    :cond_5
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXU(F)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXQ(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_4
    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGE:Z

    iput v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    if-nez v2, :cond_1

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXT()V

    :cond_1
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    if-nez v2, :cond_2

    return-void

    :cond_2
    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v5

    sub-int v5, v0, v5

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v6

    sub-int/2addr v1, v6

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v1, v6

    add-int/2addr v5, v3

    add-int/2addr v1, v4

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/view/View;->layout(IIII)V

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v1}, Landroid/support/v4/widget/T;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v2}, Landroid/support/v4/widget/T;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    div-int/lit8 v4, v0, 0x2

    div-int/lit8 v5, v1, 0x2

    sub-int/2addr v4, v5

    iget v5, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    add-int/2addr v1, v2

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/support/v4/widget/T;->layout(IIII)V

    return-void
.end method

.method public onMeasure(II)V
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXT()V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGI:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGI:I

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/T;->measure(II)V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGx:I

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    if-ne v1, v2, :cond_3

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGx:I

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onNestedFling(Landroid/view/View;FFZ)Z
    .locals 1

    invoke-virtual {p0, p2, p3, p4}, Landroid/support/v4/widget/SwipeRefreshLayout;->dispatchNestedFling(FFZ)Z

    move-result v0

    return v0
.end method

.method public onNestedPreFling(Landroid/view/View;FF)Z
    .locals 1

    invoke-virtual {p0, p2, p3}, Landroid/support/v4/widget/SwipeRefreshLayout;->dispatchNestedPreFling(FF)Z

    move-result v0

    return v0
.end method

.method public onNestedPreScroll(Landroid/view/View;II[I)V
    .locals 6

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-lez p3, :cond_0

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    int-to-float v0, p3

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    float-to-int v0, v0

    sub-int v0, p3, v0

    aput v0, p4, v5

    iput v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    :goto_0
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXV(F)V

    :cond_0
    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGJ:Z

    if-eqz v0, :cond_1

    if-lez p3, :cond_1

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    aget v0, p4, v5

    sub-int v0, p3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGy:[I

    aget v1, p4, v4

    sub-int v1, p2, v1

    aget v2, p4, v5

    sub-int v2, p3, v2

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->dispatchNestedPreScroll(II[I[I)Z

    move-result v1

    if-eqz v1, :cond_2

    aget v1, p4, v4

    aget v2, v0, v4

    add-int/2addr v1, v2

    aput v1, p4, v4

    aget v1, p4, v5

    aget v0, v0, v5

    add-int/2addr v0, v1

    aput v0, p4, v5

    :cond_2
    return-void

    :cond_3
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    int-to-float v1, p3

    sub-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    aput p3, p4, v5

    goto :goto_0
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
    .locals 6

    iget-object v5, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGG:[I

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->dispatchNestedScroll(IIII[I)Z

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGG:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    add-int/2addr v0, p5

    if-gez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYf()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXV(F)V

    :cond_0
    return-void
.end method

.method public onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGH:Landroid/support/v4/view/Z;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/view/Z;->onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V

    and-int/lit8 v0, p3, 0x2

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->startNestedScroll(I)Z

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGD:Z

    return-void
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGg:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onStopNestedScroll(Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGH:Landroid/support/v4/view/Z;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/Z;->onStopNestedScroll(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGD:Z

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYe(F)V

    iput v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGr:F

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->stopNestedScroll()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v4, -0x1

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGg:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGg:Z

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGg:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYf()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGD:Z

    if-eqz v1, :cond_2

    :cond_1
    return v2

    :cond_2
    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGE:Z

    goto :goto_0

    :pswitch_2
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_4

    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "Got ACTION_MOVE event but have an invalid active pointer id."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_4
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXU(F)V

    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGE:Z

    if-eqz v1, :cond_3

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGm:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_5

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXV(F)V

    goto :goto_0

    :cond_5
    return v2

    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    if-gez v0, :cond_6

    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "Got ACTION_POINTER_DOWN event but have an invalid action index."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_6
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXQ(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_5
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_7

    sget-object v0, Landroid/support/v4/widget/SwipeRefreshLayout;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "Got ACTION_UP event but don\'t have an active pointer id."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_7
    iget-boolean v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGE:Z

    if-eqz v1, :cond_8

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGm:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGE:Z

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYe(F)V

    :cond_8
    iput v4, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGs:I

    return v2

    :pswitch_6
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/AbsListView;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGF:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPq(Landroid/view/View;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0
.end method

.method setAnimationProgress(F)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/T;->setScaleX(F)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/T;->setScaleY(F)V

    return-void
.end method

.method public varargs setColorScheme([I)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    return-void
.end method

.method public varargs setColorSchemeColors([I)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXT()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/I;->setColorSchemeColors([I)V

    return-void
.end method

.method public varargs setColorSchemeResources([I)V
    .locals 4

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    array-length v0, p1

    new-array v2, v0, [I

    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    aget v3, p1, v0

    invoke-static {v1, v3}, Landroid/support/v4/content/a;->eae(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    return-void
.end method

.method public setDistanceToTriggerSync(I)V
    .locals 1

    int-to-float v0, p1

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGP:F

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYc()V

    :cond_0
    return-void
.end method

.method public setNestedScrollingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/a;->setNestedScrollingEnabled(Z)V

    return-void
.end method

.method public setOnChildScrollUpCallback(Landroid/support/v4/widget/as;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGw:Landroid/support/v4/widget/as;

    return-void
.end method

.method public setOnRefreshListener(Landroid/support/v4/widget/aI;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGN:Landroid/support/v4/widget/aI;

    return-void
.end method

.method public setProgressBackgroundColor(I)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setProgressBackgroundColorSchemeResource(I)V

    return-void
.end method

.method public setProgressBackgroundColorSchemeColor(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/T;->setBackgroundColor(I)V

    return-void
.end method

.method public setProgressBackgroundColorSchemeResource(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/content/a;->eae(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setProgressBackgroundColorSchemeColor(I)V

    return-void
.end method

.method public setProgressViewEndTarget(ZI)V
    .locals 1

    iput p2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGv:I

    iput-boolean p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGn:Z

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->invalidate()V

    return-void
.end method

.method public setProgressViewOffset(ZII)V
    .locals 1

    iput-boolean p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGn:Z

    iput p2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGj:I

    iput p3, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGv:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGJ:Z

    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dYc()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    return-void
.end method

.method public setRefreshing(Z)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGu:Z

    iget-boolean v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGJ:Z

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGv:I

    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGj:I

    add-int/2addr v0, v1

    :goto_0
    iget v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setTargetOffsetTopAndBottom(I)V

    iput-boolean v2, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGt:Z

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGz:Landroid/view/animation/Animation$AnimationListener;

    invoke-direct {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->dXX(Landroid/view/animation/Animation$AnimationListener;)V

    :goto_1
    return-void

    :cond_0
    iget v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGv:I

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(ZZ)V

    goto :goto_1
.end method

.method public setSize(I)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    if-nez p1, :cond_1

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42600000    # 56.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGI:I

    :goto_0
    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/I;->dVA(I)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    iget-object v1, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGO:Landroid/support/v4/widget/I;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/T;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_1
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42200000    # 40.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGI:I

    goto :goto_0
.end method

.method setTargetOffsetTopAndBottom(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->bringToFront()V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-static {v0, p1}, Landroid/support/v4/view/z;->dPz(Landroid/view/View;I)V

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGi:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->getTop()I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGL:I

    return-void
.end method

.method public startNestedScroll(I)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/a;->startNestedScroll(I)Z

    move-result v0

    return v0
.end method

.method public stopNestedScroll()V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/SwipeRefreshLayout;->eGe:Landroid/support/v4/view/a;

    invoke-virtual {v0}, Landroid/support/v4/view/a;->stopNestedScroll()V

    return-void
.end method
