.class public Landroid/support/v4/widget/I;
.super Landroid/graphics/drawable/Drawable;
.source "CircularProgressDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# static fields
.field private static final eDG:Landroid/view/animation/Interpolator;

.field private static final eDI:[I

.field private static final eDJ:Landroid/view/animation/Interpolator;


# instance fields
.field private eDD:Z

.field private eDE:F

.field private eDF:F

.field private eDH:Landroid/content/res/Resources;

.field private eDK:Landroid/animation/Animator;

.field private final eDL:Landroid/support/v4/widget/R;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Landroid/support/v4/widget/I;->eDG:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/support/v4/view/b/b;

    invoke-direct {v0}, Landroid/support/v4/view/b/b;-><init>()V

    sput-object v0, Landroid/support/v4/widget/I;->eDJ:Landroid/view/animation/Interpolator;

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/high16 v1, -0x1000000

    const/4 v2, 0x0

    aput v1, v0, v2

    sput-object v0, Landroid/support/v4/widget/I;->eDI:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    invoke-static {p1}, Landroid/support/v4/a/m;->dTe(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/I;->eDH:Landroid/content/res/Resources;

    new-instance v0, Landroid/support/v4/widget/R;

    invoke-direct {v0}, Landroid/support/v4/widget/R;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    sget-object v1, Landroid/support/v4/widget/I;->eDI:[I

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/R;->dWG([I)V

    const/high16 v0, 0x40200000    # 2.5f

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/I;->dVy(F)V

    invoke-direct {p0}, Landroid/support/v4/widget/I;->dVt()V

    return-void
.end method

.method private dVB(FLandroid/support/v4/widget/R;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/I;->dVx(FLandroid/support/v4/widget/R;)V

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWx()F

    move-result v0

    const v1, 0x3f4ccccd    # 0.8f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWm()F

    move-result v1

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWu()F

    move-result v2

    const v3, 0x3c23d70a    # 0.01f

    sub-float/2addr v2, v3

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWm()F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    invoke-virtual {p2, v1}, Landroid/support/v4/widget/R;->dWs(F)V

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWu()F

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/widget/R;->dWA(F)V

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWx()F

    move-result v1

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWx()F

    move-result v2

    sub-float/2addr v0, v2

    mul-float/2addr v0, p1

    add-float/2addr v0, v1

    invoke-virtual {p2, v0}, Landroid/support/v4/widget/R;->dWw(F)V

    return-void
.end method

.method private dVC(F)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/I;->eDE:F

    return-void
.end method

.method static synthetic dVG(Landroid/support/v4/widget/I;Z)Z
    .locals 0

    iput-boolean p1, p0, Landroid/support/v4/widget/I;->eDD:Z

    return p1
.end method

.method private dVH(FFFF)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    iget-object v1, p0, Landroid/support/v4/widget/I;->eDH:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float v2, p2, v1

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/R;->dWr(F)V

    mul-float v2, p1, v1

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/R;->dWI(F)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/R;->dWB(I)V

    mul-float v2, p3, v1

    mul-float/2addr v1, p4

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/widget/R;->dWp(FF)V

    return-void
.end method

.method static synthetic dVI(Landroid/support/v4/widget/I;)F
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/I;->eDF:F

    return v0
.end method

.method static synthetic dVq(Landroid/support/v4/widget/I;)Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/widget/I;->eDD:Z

    return v0
.end method

.method static synthetic dVr(Landroid/support/v4/widget/I;FLandroid/support/v4/widget/R;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/I;->dVs(FLandroid/support/v4/widget/R;Z)V

    return-void
.end method

.method private dVs(FLandroid/support/v4/widget/R;Z)V
    .locals 7

    const/high16 v6, 0x3f800000    # 1.0f

    const v5, 0x3c23d70a    # 0.01f

    const v4, 0x3f4a3d71    # 0.79f

    const/high16 v1, 0x3f000000    # 0.5f

    iget-boolean v0, p0, Landroid/support/v4/widget/I;->eDD:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/I;->dVB(FLandroid/support/v4/widget/R;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    cmpl-float v0, p1, v6

    if-nez v0, :cond_2

    if-eqz p3, :cond_0

    :cond_2
    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWx()F

    move-result v2

    cmpg-float v0, p1, v1

    if-gez v0, :cond_3

    div-float v0, p1, v1

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWm()F

    move-result v1

    sget-object v3, Landroid/support/v4/widget/I;->eDJ:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v4

    add-float/2addr v0, v5

    add-float/2addr v0, v1

    :goto_1
    const v3, 0x3e570a3c    # 0.20999998f

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    iget v3, p0, Landroid/support/v4/widget/I;->eDF:F

    add-float/2addr v3, p1

    const/high16 v4, 0x43580000    # 216.0f

    mul-float/2addr v3, v4

    invoke-virtual {p2, v1}, Landroid/support/v4/widget/R;->dWs(F)V

    invoke-virtual {p2, v0}, Landroid/support/v4/widget/R;->dWA(F)V

    invoke-virtual {p2, v2}, Landroid/support/v4/widget/R;->dWw(F)V

    invoke-direct {p0, v3}, Landroid/support/v4/widget/I;->dVC(F)V

    goto :goto_0

    :cond_3
    sub-float v0, p1, v1

    div-float v1, v0, v1

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWm()F

    move-result v0

    add-float/2addr v0, v4

    sget-object v3, Landroid/support/v4/widget/I;->eDJ:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    sub-float v1, v6, v1

    mul-float/2addr v1, v4

    add-float/2addr v1, v5

    sub-float v1, v0, v1

    goto :goto_1
.end method

.method private dVt()V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    new-instance v2, Landroid/support/v4/widget/q;

    invoke-direct {v2, p0, v0}, Landroid/support/v4/widget/q;-><init>(Landroid/support/v4/widget/I;Landroid/support/v4/widget/R;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    sget-object v2, Landroid/support/v4/widget/I;->eDG:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v2, Landroid/support/v4/widget/U;

    invoke-direct {v2, p0, v0}, Landroid/support/v4/widget/U;-><init>(Landroid/support/v4/widget/I;Landroid/support/v4/widget/R;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iput-object v1, p0, Landroid/support/v4/widget/I;->eDK:Landroid/animation/Animator;

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private dVv(FII)I
    .locals 8

    shr-int/lit8 v0, p2, 0x18

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, p2, 0x10

    and-int/lit16 v1, v1, 0xff

    shr-int/lit8 v2, p2, 0x8

    and-int/lit16 v2, v2, 0xff

    and-int/lit16 v3, p2, 0xff

    shr-int/lit8 v4, p3, 0x18

    and-int/lit16 v4, v4, 0xff

    shr-int/lit8 v5, p3, 0x10

    and-int/lit16 v5, v5, 0xff

    shr-int/lit8 v6, p3, 0x8

    and-int/lit16 v6, v6, 0xff

    and-int/lit16 v7, p3, 0xff

    sub-int/2addr v4, v0

    int-to-float v4, v4

    mul-float/2addr v4, p1

    float-to-int v4, v4

    add-int/2addr v0, v4

    shl-int/lit8 v0, v0, 0x18

    sub-int v4, v5, v1

    int-to-float v4, v4

    mul-float/2addr v4, p1

    float-to-int v4, v4

    add-int/2addr v1, v4

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    sub-int v1, v6, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v1, v2

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    sub-int v1, v7, v3

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v1, v3

    or-int/2addr v0, v1

    return v0
.end method

.method static synthetic dVw(Landroid/support/v4/widget/I;FLandroid/support/v4/widget/R;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/I;->dVx(FLandroid/support/v4/widget/R;)V

    return-void
.end method

.method private dVx(FLandroid/support/v4/widget/R;)V
    .locals 3

    const/high16 v1, 0x3f400000    # 0.75f

    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    sub-float v0, p1, v1

    const/high16 v1, 0x3e800000    # 0.25f

    div-float/2addr v0, v1

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWq()I

    move-result v1

    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWy()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/widget/I;->dVv(FII)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/widget/R;->setColor(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/support/v4/widget/R;->dWq()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/widget/R;->setColor(I)V

    goto :goto_0
.end method

.method static synthetic dVz(Landroid/support/v4/widget/I;F)F
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/I;->eDF:F

    return p1
.end method


# virtual methods
.method public dVA(I)V
    .locals 4

    if-nez p1, :cond_0

    const/high16 v0, 0x41400000    # 12.0f

    const/high16 v1, 0x40c00000    # 6.0f

    const/high16 v2, 0x41300000    # 11.0f

    const/high16 v3, 0x40400000    # 3.0f

    invoke-direct {p0, v2, v3, v0, v1}, Landroid/support/v4/widget/I;->dVH(FFFF)V

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void

    :cond_0
    const/high16 v0, 0x41200000    # 10.0f

    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v2, 0x40f00000    # 7.5f

    const/high16 v3, 0x40200000    # 2.5f

    invoke-direct {p0, v2, v3, v0, v1}, Landroid/support/v4/widget/I;->dVH(FFFF)V

    goto :goto_0
.end method

.method public dVD(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/R;->dWn(Z)V

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method

.method public dVE(F)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/R;->dWz(F)V

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method

.method public dVF(FF)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/R;->dWs(F)V

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, p2}, Landroid/support/v4/widget/R;->dWA(F)V

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method

.method public dVu(F)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/R;->dWw(F)V

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method

.method public dVy(F)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/R;->dWr(F)V

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v1, p0, Landroid/support/v4/widget/I;->eDE:F

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v3

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v1, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/widget/R;->dWv(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public getAlpha()I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0}, Landroid/support/v4/widget/R;->getAlpha()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public isRunning()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDK:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    return v0
.end method

.method public setAlpha(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/R;->setAlpha(I)V

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/R;->setColorFilter(Landroid/graphics/ColorFilter;)V

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method

.method public varargs setColorSchemeColors([I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/R;->dWG([I)V

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/R;->dWB(I)V

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method

.method public start()V
    .locals 4

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDK:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0}, Landroid/support/v4/widget/R;->dWD()V

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0}, Landroid/support/v4/widget/R;->dWH()F

    move-result v0

    iget-object v1, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v1}, Landroid/support/v4/widget/R;->dWC()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/I;->eDD:Z

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDK:Landroid/animation/Animator;

    const-wide/16 v2, 0x29a

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDK:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/R;->dWB(I)V

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0}, Landroid/support/v4/widget/R;->dWo()V

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDK:Landroid/animation/Animator;

    const-wide/16 v2, 0x534

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDK:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDK:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/widget/I;->dVC(F)V

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/R;->dWn(Z)V

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/R;->dWB(I)V

    iget-object v0, p0, Landroid/support/v4/widget/I;->eDL:Landroid/support/v4/widget/R;

    invoke-virtual {v0}, Landroid/support/v4/widget/R;->dWo()V

    invoke-virtual {p0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method
