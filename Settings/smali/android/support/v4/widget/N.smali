.class public abstract Landroid/support/v4/widget/N;
.super Landroid/widget/BaseAdapter;
.source "CursorAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Landroid/support/v4/widget/ai;


# instance fields
.field protected eDN:Landroid/support/v4/widget/aq;

.field protected eDO:Landroid/widget/FilterQueryProvider;

.field protected eDP:I

.field protected eDQ:Landroid/database/DataSetObserver;

.field protected eDR:Landroid/support/v4/widget/S;

.field protected eDS:Z

.field protected eDT:Landroid/database/Cursor;

.field protected eDU:Z

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/widget/N;->dVO(Landroid/content/Context;Landroid/database/Cursor;I)V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public dBG(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 1

    if-nez p1, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public dBJ(Landroid/database/Cursor;)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v4/widget/N;->dVM(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method

.method public dBK(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDO:Landroid/widget/FilterQueryProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDO:Landroid/widget/FilterQueryProvider;

    invoke-interface {v0, p1}, Landroid/widget/FilterQueryProvider;->runQuery(Ljava/lang/CharSequence;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    return-object v0
.end method

.method public abstract dBL(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end method

.method public abstract dBO(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public dUR(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v4/widget/N;->dBO(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public dVM(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    if-ne p1, v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDR:Landroid/support/v4/widget/S;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDR:Landroid/support/v4/widget/S;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    iget-object v1, p0, Landroid/support/v4/widget/N;->eDQ:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDQ:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_2
    iput-object p1, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    if-eqz p1, :cond_5

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDR:Landroid/support/v4/widget/S;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDR:Landroid/support/v4/widget/S;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_3
    iget-object v1, p0, Landroid/support/v4/widget/N;->eDQ:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDQ:Landroid/database/DataSetObserver;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_4
    const-string/jumbo v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/N;->eDP:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v4/widget/N;->eDU:Z

    invoke-virtual {p0}, Landroid/support/v4/widget/N;->notifyDataSetChanged()V

    :goto_0
    return-object v0

    :cond_5
    const/4 v1, -0x1

    iput v1, p0, Landroid/support/v4/widget/N;->eDP:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/widget/N;->eDU:Z

    invoke-virtual {p0}, Landroid/support/v4/widget/N;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method protected dVN()V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/widget/N;->eDS:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v4/widget/N;->eDU:Z

    :cond_0
    return-void
.end method

.method dVO(Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v3, 0x0

    and-int/lit8 v2, p3, 0x1

    if-ne v2, v0, :cond_2

    or-int/lit8 p3, p3, 0x2

    iput-boolean v0, p0, Landroid/support/v4/widget/N;->eDS:Z

    :goto_0
    if-eqz p2, :cond_3

    :goto_1
    iput-object p2, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    iput-boolean v0, p0, Landroid/support/v4/widget/N;->eDU:Z

    iput-object p1, p0, Landroid/support/v4/widget/N;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_4

    const-string/jumbo v1, "_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    :goto_2
    iput v1, p0, Landroid/support/v4/widget/N;->eDP:I

    and-int/lit8 v1, p3, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    new-instance v1, Landroid/support/v4/widget/S;

    invoke-direct {v1, p0}, Landroid/support/v4/widget/S;-><init>(Landroid/support/v4/widget/N;)V

    iput-object v1, p0, Landroid/support/v4/widget/N;->eDR:Landroid/support/v4/widget/S;

    new-instance v1, Landroid/support/v4/widget/x;

    invoke-direct {v1, p0}, Landroid/support/v4/widget/x;-><init>(Landroid/support/v4/widget/N;)V

    iput-object v1, p0, Landroid/support/v4/widget/N;->eDQ:Landroid/database/DataSetObserver;

    :goto_3
    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDR:Landroid/support/v4/widget/S;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDR:Landroid/support/v4/widget/S;

    invoke-interface {p2, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/N;->eDQ:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDQ:Landroid/database/DataSetObserver;

    invoke-interface {p2, v0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    return-void

    :cond_2
    iput-boolean v1, p0, Landroid/support/v4/widget/N;->eDS:Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    const/4 v1, -0x1

    goto :goto_2

    :cond_5
    iput-object v3, p0, Landroid/support/v4/widget/N;->eDR:Landroid/support/v4/widget/S;

    iput-object v3, p0, Landroid/support/v4/widget/N;->eDQ:Landroid/database/DataSetObserver;

    goto :goto_3
.end method

.method public dVP()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/widget/N;->eDU:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Landroid/support/v4/widget/N;->eDU:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    if-nez p2, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->mContext:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v1, p3}, Landroid/support/v4/widget/N;->dUR(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/N;->mContext:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-virtual {p0, p2, v0, v1}, Landroid/support/v4/widget/N;->dBL(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    return-object p2

    :cond_1
    return-object v1
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDN:Landroid/support/v4/widget/aq;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/widget/aq;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/aq;-><init>(Landroid/support/v4/widget/ai;)V

    iput-object v0, p0, Landroid/support/v4/widget/N;->eDN:Landroid/support/v4/widget/aq;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/N;->eDN:Landroid/support/v4/widget/aq;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Landroid/support/v4/widget/N;->eDU:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    return-object v0

    :cond_0
    return-object v1
.end method

.method public getItemId(I)J
    .locals 4

    const-wide/16 v2, 0x0

    iget-boolean v0, p0, Landroid/support/v4/widget/N;->eDU:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    iget v1, p0, Landroid/support/v4/widget/N;->eDP:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0

    :cond_0
    return-wide v2

    :cond_1
    return-wide v2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-boolean v0, p0, Landroid/support/v4/widget/N;->eDU:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "this should only be called when the cursor is valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "couldn\'t move cursor to position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    iget-object v0, p0, Landroid/support/v4/widget/N;->mContext:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v1, p3}, Landroid/support/v4/widget/N;->dBO(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_2
    iget-object v0, p0, Landroid/support/v4/widget/N;->mContext:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/widget/N;->eDT:Landroid/database/Cursor;

    invoke-virtual {p0, p2, v0, v1}, Landroid/support/v4/widget/N;->dBL(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    return-object p2
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
