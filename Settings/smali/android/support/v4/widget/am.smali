.class Landroid/support/v4/widget/am;
.super Ljava/lang/Object;
.source "AutoScrollHelper.java"


# instance fields
.field private eGQ:J

.field private eGR:I

.field private eGS:J

.field private eGT:F

.field private eGU:F

.field private eGV:I

.field private eGW:J

.field private eGX:I

.field private eGY:F

.field private eGZ:I

.field private eHa:I


# direct methods
.method constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Landroid/support/v4/widget/am;->eGS:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v4/widget/am;->eGW:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/support/v4/widget/am;->eGQ:J

    iput v2, p0, Landroid/support/v4/widget/am;->eGX:I

    iput v2, p0, Landroid/support/v4/widget/am;->eGZ:I

    return-void
.end method

.method private dYo(F)F
    .locals 2

    const/high16 v0, -0x3f800000    # -4.0f

    mul-float/2addr v0, p1

    mul-float/2addr v0, p1

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    return v0
.end method

.method private dYt(J)F
    .locals 7

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    iget-wide v0, p0, Landroid/support/v4/widget/am;->eGS:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    return v4

    :cond_0
    iget-wide v0, p0, Landroid/support/v4/widget/am;->eGW:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    iget-wide v0, p0, Landroid/support/v4/widget/am;->eGW:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    :cond_1
    iget-wide v0, p0, Landroid/support/v4/widget/am;->eGS:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    iget v1, p0, Landroid/support/v4/widget/am;->eHa:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0, v4, v5}, Landroid/support/v4/widget/ag;->dXN(FFF)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    return v0

    :cond_2
    iget-wide v0, p0, Landroid/support/v4/widget/am;->eGW:J

    sub-long v0, p1, v0

    iget v2, p0, Landroid/support/v4/widget/am;->eGY:F

    sub-float v2, v5, v2

    iget v3, p0, Landroid/support/v4/widget/am;->eGY:F

    long-to-float v0, v0

    iget v1, p0, Landroid/support/v4/widget/am;->eGR:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0, v4, v5}, Landroid/support/v4/widget/ag;->dXN(FFF)F

    move-result v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    return v0
.end method


# virtual methods
.method public dYj()I
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/am;->eGX:I

    return v0
.end method

.method public dYk()I
    .locals 2

    iget v0, p0, Landroid/support/v4/widget/am;->eGT:F

    iget v1, p0, Landroid/support/v4/widget/am;->eGT:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public dYl()V
    .locals 6

    iget-wide v0, p0, Landroid/support/v4/widget/am;->eGQ:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Cannot compute scroll delta before calling start()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/am;->dYt(J)F

    move-result v2

    invoke-direct {p0, v2}, Landroid/support/v4/widget/am;->dYo(F)F

    move-result v2

    iget-wide v4, p0, Landroid/support/v4/widget/am;->eGQ:J

    sub-long v4, v0, v4

    iput-wide v0, p0, Landroid/support/v4/widget/am;->eGQ:J

    long-to-float v0, v4

    mul-float/2addr v0, v2

    iget v1, p0, Landroid/support/v4/widget/am;->eGT:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/am;->eGX:I

    long-to-float v0, v4

    mul-float/2addr v0, v2

    iget v1, p0, Landroid/support/v4/widget/am;->eGU:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/widget/am;->eGZ:I

    return-void
.end method

.method public dYm()I
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/am;->eGZ:I

    return v0
.end method

.method public dYn(FF)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/am;->eGT:F

    iput p2, p0, Landroid/support/v4/widget/am;->eGU:F

    return-void
.end method

.method public dYp()Z
    .locals 8

    const/4 v0, 0x0

    iget-wide v2, p0, Landroid/support/v4/widget/am;->eGW:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Landroid/support/v4/widget/am;->eGW:J

    iget v1, p0, Landroid/support/v4/widget/am;->eGR:I

    int-to-long v6, v1

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public dYq(I)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/am;->eGV:I

    return-void
.end method

.method public dYr(I)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/am;->eHa:I

    return-void
.end method

.method public dYs()I
    .locals 2

    iget v0, p0, Landroid/support/v4/widget/am;->eGU:F

    iget v1, p0, Landroid/support/v4/widget/am;->eGU:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public dYu()V
    .locals 5

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/support/v4/widget/am;->eGS:J

    sub-long v2, v0, v2

    long-to-int v2, v2

    iget v3, p0, Landroid/support/v4/widget/am;->eGV:I

    const/4 v4, 0x0

    invoke-static {v2, v4, v3}, Landroid/support/v4/widget/ag;->dXD(III)I

    move-result v2

    iput v2, p0, Landroid/support/v4/widget/am;->eGR:I

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/am;->dYt(J)F

    move-result v2

    iput v2, p0, Landroid/support/v4/widget/am;->eGY:F

    iput-wide v0, p0, Landroid/support/v4/widget/am;->eGW:J

    return-void
.end method

.method public start()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/widget/am;->eGS:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v4/widget/am;->eGW:J

    iget-wide v0, p0, Landroid/support/v4/widget/am;->eGS:J

    iput-wide v0, p0, Landroid/support/v4/widget/am;->eGQ:J

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/v4/widget/am;->eGY:F

    iput v2, p0, Landroid/support/v4/widget/am;->eGX:I

    iput v2, p0, Landroid/support/v4/widget/am;->eGZ:I

    return-void
.end method
