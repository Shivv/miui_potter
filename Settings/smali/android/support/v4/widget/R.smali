.class Landroid/support/v4/widget/R;
.super Ljava/lang/Object;
.source "CircularProgressDrawable.java"


# instance fields
.field eEA:F

.field eEB:Landroid/graphics/Path;

.field final eEC:Landroid/graphics/Paint;

.field final eED:Landroid/graphics/Paint;

.field final eEE:Landroid/graphics/Paint;

.field eEF:I

.field eEG:I

.field eEH:F

.field eEI:F

.field eEJ:I

.field eEK:I

.field eEL:F

.field eEr:F

.field eEs:F

.field eEt:F

.field final eEu:Landroid/graphics/RectF;

.field eEv:Z

.field eEw:F

.field eEx:[I

.field eEy:I

.field eEz:F


# direct methods
.method constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/R;->eEu:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/R;->eEE:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/R;->eEC:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/R;->eED:Landroid/graphics/Paint;

    iput v1, p0, Landroid/support/v4/widget/R;->eEH:F

    iput v1, p0, Landroid/support/v4/widget/R;->eEL:F

    iput v1, p0, Landroid/support/v4/widget/R;->eEs:F

    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Landroid/support/v4/widget/R;->eEI:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/v4/widget/R;->eEr:F

    const/16 v0, 0xff

    iput v0, p0, Landroid/support/v4/widget/R;->eEG:I

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEE:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEE:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEE:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEC:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEC:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eED:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method


# virtual methods
.method dWA(F)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/R;->eEL:F

    return-void
.end method

.method dWB(I)V
    .locals 2

    iput p1, p0, Landroid/support/v4/widget/R;->eEF:I

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEx:[I

    iget v1, p0, Landroid/support/v4/widget/R;->eEF:I

    aget v0, v0, v1

    iput v0, p0, Landroid/support/v4/widget/R;->eEJ:I

    return-void
.end method

.method dWC()F
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/R;->eEH:F

    return v0
.end method

.method dWD()V
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/R;->eEH:F

    iput v0, p0, Landroid/support/v4/widget/R;->eEz:F

    iget v0, p0, Landroid/support/v4/widget/R;->eEL:F

    iput v0, p0, Landroid/support/v4/widget/R;->eEw:F

    iget v0, p0, Landroid/support/v4/widget/R;->eEs:F

    iput v0, p0, Landroid/support/v4/widget/R;->eEt:F

    return-void
.end method

.method dWE()I
    .locals 2

    iget v0, p0, Landroid/support/v4/widget/R;->eEF:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Landroid/support/v4/widget/R;->eEx:[I

    array-length v1, v1

    rem-int/2addr v0, v1

    return v0
.end method

.method dWF()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v4/widget/R;->dWE()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/R;->dWB(I)V

    return-void
.end method

.method dWG([I)V
    .locals 1

    iput-object p1, p0, Landroid/support/v4/widget/R;->eEx:[I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/R;->dWB(I)V

    return-void
.end method

.method dWH()F
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/R;->eEL:F

    return v0
.end method

.method dWI(F)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/R;->eEA:F

    return-void
.end method

.method dWm()F
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/R;->eEz:F

    return v0
.end method

.method dWn(Z)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/widget/R;->eEv:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Landroid/support/v4/widget/R;->eEv:Z

    :cond_0
    return-void
.end method

.method dWo()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/widget/R;->eEz:F

    iput v0, p0, Landroid/support/v4/widget/R;->eEw:F

    iput v0, p0, Landroid/support/v4/widget/R;->eEt:F

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/R;->dWs(F)V

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/R;->dWA(F)V

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/R;->dWw(F)V

    return-void
.end method

.method dWp(FF)V
    .locals 1

    float-to-int v0, p1

    iput v0, p0, Landroid/support/v4/widget/R;->eEy:I

    float-to-int v0, p2

    iput v0, p0, Landroid/support/v4/widget/R;->eEK:I

    return-void
.end method

.method dWq()I
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEx:[I

    iget v1, p0, Landroid/support/v4/widget/R;->eEF:I

    aget v0, v0, v1

    return v0
.end method

.method dWr(F)V
    .locals 1

    iput p1, p0, Landroid/support/v4/widget/R;->eEI:F

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEE:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method dWs(F)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/R;->eEH:F

    return-void
.end method

.method dWt(Landroid/graphics/Canvas;FFLandroid/graphics/RectF;)V
    .locals 7

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    iget-boolean v0, p0, Landroid/support/v4/widget/R;->eEv:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    if-nez v0, :cond_1

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    :goto_0
    invoke-virtual {p4}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p4}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    div-float/2addr v0, v6

    iget v1, p0, Landroid/support/v4/widget/R;->eEy:I

    int-to-float v1, v1

    iget v2, p0, Landroid/support/v4/widget/R;->eEr:F

    mul-float/2addr v1, v2

    div-float/2addr v1, v6

    iget-object v2, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    invoke-virtual {v2, v5, v5}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v2, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    iget v3, p0, Landroid/support/v4/widget/R;->eEy:I

    int-to-float v3, v3

    iget v4, p0, Landroid/support/v4/widget/R;->eEr:F

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    iget v3, p0, Landroid/support/v4/widget/R;->eEy:I

    int-to-float v3, v3

    iget v4, p0, Landroid/support/v4/widget/R;->eEr:F

    mul-float/2addr v3, v4

    div-float/2addr v3, v6

    iget v4, p0, Landroid/support/v4/widget/R;->eEK:I

    int-to-float v4, v4

    iget v5, p0, Landroid/support/v4/widget/R;->eEr:F

    mul-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    invoke-virtual {p4}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    add-float/2addr v0, v3

    sub-float/2addr v0, v1

    invoke-virtual {p4}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iget v3, p0, Landroid/support/v4/widget/R;->eEI:F

    div-float/2addr v3, v6

    add-float/2addr v1, v3

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->offset(FF)V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEC:Landroid/graphics/Paint;

    iget v1, p0, Landroid/support/v4/widget/R;->eEJ:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEC:Landroid/graphics/Paint;

    iget v1, p0, Landroid/support/v4/widget/R;->eEG:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    add-float v0, p2, p3

    invoke-virtual {p4}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {p4}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    iget-object v1, p0, Landroid/support/v4/widget/R;->eEC:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/R;->eEB:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    goto :goto_0
.end method

.method dWu()F
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/R;->eEw:F

    return v0
.end method

.method dWv(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 8

    const/high16 v6, 0x43b40000    # 360.0f

    const/high16 v7, 0x40000000    # 2.0f

    iget-object v1, p0, Landroid/support/v4/widget/R;->eEu:Landroid/graphics/RectF;

    iget v0, p0, Landroid/support/v4/widget/R;->eEA:F

    iget v2, p0, Landroid/support/v4/widget/R;->eEI:F

    div-float/2addr v2, v7

    add-float/2addr v0, v2

    iget v2, p0, Landroid/support/v4/widget/R;->eEA:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v7

    iget v2, p0, Landroid/support/v4/widget/R;->eEy:I

    int-to-float v2, v2

    iget v3, p0, Landroid/support/v4/widget/R;->eEr:F

    mul-float/2addr v2, v3

    div-float/2addr v2, v7

    iget v3, p0, Landroid/support/v4/widget/R;->eEI:F

    div-float/2addr v3, v7

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    sub-float/2addr v0, v2

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    iget v0, p0, Landroid/support/v4/widget/R;->eEH:F

    iget v2, p0, Landroid/support/v4/widget/R;->eEs:F

    add-float/2addr v0, v2

    mul-float v2, v0, v6

    iget v0, p0, Landroid/support/v4/widget/R;->eEL:F

    iget v3, p0, Landroid/support/v4/widget/R;->eEs:F

    add-float/2addr v0, v3

    mul-float/2addr v0, v6

    sub-float v3, v0, v2

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEE:Landroid/graphics/Paint;

    iget v4, p0, Landroid/support/v4/widget/R;->eEJ:I

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEE:Landroid/graphics/Paint;

    iget v4, p0, Landroid/support/v4/widget/R;->eEG:I

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    iget v0, p0, Landroid/support/v4/widget/R;->eEI:F

    div-float/2addr v0, v7

    invoke-virtual {v1, v0, v0}, Landroid/graphics/RectF;->inset(FF)V

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v6

    div-float/2addr v6, v7

    iget-object v7, p0, Landroid/support/v4/widget/R;->eED:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    neg-float v4, v0

    neg-float v0, v0

    invoke-virtual {v1, v4, v0}, Landroid/graphics/RectF;->inset(FF)V

    iget-object v5, p0, Landroid/support/v4/widget/R;->eEE:Landroid/graphics/Paint;

    const/4 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    invoke-virtual {p0, p1, v2, v3, v1}, Landroid/support/v4/widget/R;->dWt(Landroid/graphics/Canvas;FFLandroid/graphics/RectF;)V

    return-void
.end method

.method dWw(F)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/R;->eEs:F

    return-void
.end method

.method dWx()F
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/R;->eEt:F

    return v0
.end method

.method dWy()I
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEx:[I

    invoke-virtual {p0}, Landroid/support/v4/widget/R;->dWE()I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method dWz(F)V
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/R;->eEr:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    iput p1, p0, Landroid/support/v4/widget/R;->eEr:F

    :cond_0
    return-void
.end method

.method getAlpha()I
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/R;->eEG:I

    return v0
.end method

.method setAlpha(I)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/R;->eEG:I

    return-void
.end method

.method setColor(I)V
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/R;->eEJ:I

    return-void
.end method

.method setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/R;->eEE:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    return-void
.end method
