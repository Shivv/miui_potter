.class Landroid/support/v4/widget/ad;
.super Landroid/graphics/drawable/shapes/OvalShape;
.source "CircleImageView.java"


# instance fields
.field private eFA:Landroid/graphics/RadialGradient;

.field final synthetic eFy:Landroid/support/v4/widget/T;

.field private eFz:Landroid/graphics/Paint;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/T;I)V
    .locals 1

    iput-object p1, p0, Landroid/support/v4/widget/ad;->eFy:Landroid/support/v4/widget/T;

    invoke-direct {p0}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/ad;->eFz:Landroid/graphics/Paint;

    iput p2, p1, Landroid/support/v4/widget/T;->eFq:I

    invoke-virtual {p0}, Landroid/support/v4/widget/ad;->rect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v0}, Landroid/support/v4/widget/ad;->dXx(I)V

    return-void
.end method

.method private dXx(I)V
    .locals 7

    new-instance v0, Landroid/graphics/RadialGradient;

    div-int/lit8 v1, p1, 0x2

    int-to-float v1, v1

    div-int/lit8 v2, p1, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Landroid/support/v4/widget/ad;->eFy:Landroid/support/v4/widget/T;

    iget v3, v3, Landroid/support/v4/widget/T;->eFq:I

    int-to-float v3, v3

    const/high16 v4, 0x3d000000    # 0.03125f

    const/4 v5, 0x0

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Landroid/support/v4/widget/ad;->eFA:Landroid/graphics/RadialGradient;

    iget-object v0, p0, Landroid/support/v4/widget/ad;->eFz:Landroid/graphics/Paint;

    iget-object v1, p0, Landroid/support/v4/widget/ad;->eFA:Landroid/graphics/RadialGradient;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6

    iget-object v0, p0, Landroid/support/v4/widget/ad;->eFy:Landroid/support/v4/widget/T;

    invoke-virtual {v0}, Landroid/support/v4/widget/T;->getWidth()I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/widget/ad;->eFy:Landroid/support/v4/widget/T;

    invoke-virtual {v1}, Landroid/support/v4/widget/T;->getHeight()I

    move-result v1

    div-int/lit8 v2, v0, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v1, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Landroid/support/v4/widget/ad;->eFz:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    div-int/lit8 v2, v0, 0x2

    int-to-float v2, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    div-int/lit8 v0, v0, 0x2

    iget-object v3, p0, Landroid/support/v4/widget/ad;->eFy:Landroid/support/v4/widget/T;

    iget v3, v3, Landroid/support/v4/widget/T;->eFq:I

    sub-int/2addr v0, v3

    int-to-float v0, v0

    invoke-virtual {p1, v2, v1, v0, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onResize(FF)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/shapes/OvalShape;->onResize(FF)V

    float-to-int v0, p1

    invoke-direct {p0, v0}, Landroid/support/v4/widget/ad;->dXx(I)V

    return-void
.end method
