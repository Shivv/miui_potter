.class Landroid/support/v4/widget/Z;
.super Ljava/lang/Object;
.source "CompoundButtonCompat.java"


# static fields
.field private static eFw:Z

.field private static eFx:Ljava/lang/reflect/Field;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dXu(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-boolean v0, Landroid/support/v4/widget/Z;->eFw:Z

    if-nez v0, :cond_0

    :try_start_0
    const-class v0, Landroid/widget/CompoundButton;

    const-string/jumbo v1, "mButtonDrawable"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Landroid/support/v4/widget/Z;->eFx:Ljava/lang/reflect/Field;

    sget-object v0, Landroid/support/v4/widget/Z;->eFx:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sput-boolean v4, Landroid/support/v4/widget/Z;->eFw:Z

    :cond_0
    sget-object v0, Landroid/support/v4/widget/Z;->eFx:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_1

    :try_start_1
    sget-object v0, Landroid/support/v4/widget/Z;->eFx:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "CompoundButtonCompat"

    const-string/jumbo v2, "Failed to retrieve mButtonDrawable field"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v1, "CompoundButtonCompat"

    const-string/jumbo v2, "Failed to get button drawable via reflection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sput-object v3, Landroid/support/v4/widget/Z;->eFx:Ljava/lang/reflect/Field;

    :cond_1
    return-object v3
.end method

.method public dXv(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    instance-of v0, p1, Landroid/support/v4/widget/aD;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v4/widget/aD;

    invoke-interface {p1, p2}, Landroid/support/v4/widget/aD;->setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public dXw(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V
    .locals 1

    instance-of v0, p1, Landroid/support/v4/widget/aD;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v4/widget/aD;

    invoke-interface {p1, p2}, Landroid/support/v4/widget/aD;->setSupportButtonTintList(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method
