.class public final Landroid/support/v4/widget/H;
.super Ljava/lang/Object;
.source "TextViewCompat.java"


# static fields
.field static final eDC:Landroid/support/v4/widget/ao;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/support/v4/os/h;->eja()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/v4/widget/aG;

    invoke-direct {v0}, Landroid/support/v4/widget/aG;-><init>()V

    sput-object v0, Landroid/support/v4/widget/H;->eDC:Landroid/support/v4/widget/ao;

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    new-instance v0, Landroid/support/v4/widget/m;

    invoke-direct {v0}, Landroid/support/v4/widget/m;-><init>()V

    sput-object v0, Landroid/support/v4/widget/H;->eDC:Landroid/support/v4/widget/ao;

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_2

    new-instance v0, Landroid/support/v4/widget/P;

    invoke-direct {v0}, Landroid/support/v4/widget/P;-><init>()V

    sput-object v0, Landroid/support/v4/widget/H;->eDC:Landroid/support/v4/widget/ao;

    goto :goto_0

    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_3

    new-instance v0, Landroid/support/v4/widget/ax;

    invoke-direct {v0}, Landroid/support/v4/widget/ax;-><init>()V

    sput-object v0, Landroid/support/v4/widget/H;->eDC:Landroid/support/v4/widget/ao;

    goto :goto_0

    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_4

    new-instance v0, Landroid/support/v4/widget/s;

    invoke-direct {v0}, Landroid/support/v4/widget/s;-><init>()V

    sput-object v0, Landroid/support/v4/widget/H;->eDC:Landroid/support/v4/widget/ao;

    goto :goto_0

    :cond_4
    new-instance v0, Landroid/support/v4/widget/ao;

    invoke-direct {v0}, Landroid/support/v4/widget/ao;-><init>()V

    sput-object v0, Landroid/support/v4/widget/H;->eDC:Landroid/support/v4/widget/ao;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dVp(Landroid/widget/TextView;I)V
    .locals 1

    sget-object v0, Landroid/support/v4/widget/H;->eDC:Landroid/support/v4/widget/ao;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/widget/ao;->dUQ(Landroid/widget/TextView;I)V

    return-void
.end method
