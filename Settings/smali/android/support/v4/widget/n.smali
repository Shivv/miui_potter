.class public abstract Landroid/support/v4/widget/n;
.super Landroid/support/v4/widget/N;
.source "ResourceCursorAdapter.java"


# instance fields
.field private eDb:I

.field private eDc:Landroid/view/LayoutInflater;

.field private eDd:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;Z)V
    .locals 1

    invoke-direct {p0, p1, p3, p4}, Landroid/support/v4/widget/N;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    iput p2, p0, Landroid/support/v4/widget/n;->eDb:I

    iput p2, p0, Landroid/support/v4/widget/n;->eDd:I

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Landroid/support/v4/widget/n;->eDc:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public dBO(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Landroid/support/v4/widget/n;->eDc:Landroid/view/LayoutInflater;

    iget v1, p0, Landroid/support/v4/widget/n;->eDd:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public dUR(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Landroid/support/v4/widget/n;->eDc:Landroid/view/LayoutInflater;

    iget v1, p0, Landroid/support/v4/widget/n;->eDb:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
