.class public final Landroid/support/v4/widget/aH;
.super Ljava/lang/Object;
.source "PopupWindowCompat.java"


# static fields
.field static final eHo:Landroid/support/v4/widget/an;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/widget/t;

    invoke-direct {v0}, Landroid/support/v4/widget/t;-><init>()V

    sput-object v0, Landroid/support/v4/widget/aH;->eHo:Landroid/support/v4/widget/an;

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    new-instance v0, Landroid/support/v4/widget/M;

    invoke-direct {v0}, Landroid/support/v4/widget/M;-><init>()V

    sput-object v0, Landroid/support/v4/widget/aH;->eHo:Landroid/support/v4/widget/an;

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    new-instance v0, Landroid/support/v4/widget/ak;

    invoke-direct {v0}, Landroid/support/v4/widget/ak;-><init>()V

    sput-object v0, Landroid/support/v4/widget/aH;->eHo:Landroid/support/v4/widget/an;

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/support/v4/widget/an;

    invoke-direct {v0}, Landroid/support/v4/widget/an;-><init>()V

    sput-object v0, Landroid/support/v4/widget/aH;->eHo:Landroid/support/v4/widget/an;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dYC(Landroid/widget/PopupWindow;I)V
    .locals 1

    sget-object v0, Landroid/support/v4/widget/aH;->eHo:Landroid/support/v4/widget/an;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/widget/an;->dUU(Landroid/widget/PopupWindow;I)V

    return-void
.end method

.method public static dYD(Landroid/widget/PopupWindow;Z)V
    .locals 1

    sget-object v0, Landroid/support/v4/widget/aH;->eHo:Landroid/support/v4/widget/an;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/widget/an;->dUV(Landroid/widget/PopupWindow;Z)V

    return-void
.end method

.method public static dYE(Landroid/widget/PopupWindow;Landroid/view/View;III)V
    .locals 6

    sget-object v0, Landroid/support/v4/widget/aH;->eHo:Landroid/support/v4/widget/an;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/widget/an;->dYi(Landroid/widget/PopupWindow;Landroid/view/View;III)V

    return-void
.end method
