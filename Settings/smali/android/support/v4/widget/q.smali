.class final Landroid/support/v4/widget/q;
.super Ljava/lang/Object;
.source "CircularProgressDrawable.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic eDf:Landroid/support/v4/widget/R;

.field final synthetic eDg:Landroid/support/v4/widget/I;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/I;Landroid/support/v4/widget/R;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/widget/q;->eDg:Landroid/support/v4/widget/I;

    iput-object p2, p0, Landroid/support/v4/widget/q;->eDf:Landroid/support/v4/widget/R;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Landroid/support/v4/widget/q;->eDg:Landroid/support/v4/widget/I;

    iget-object v2, p0, Landroid/support/v4/widget/q;->eDf:Landroid/support/v4/widget/R;

    invoke-static {v1, v0, v2}, Landroid/support/v4/widget/I;->dVw(Landroid/support/v4/widget/I;FLandroid/support/v4/widget/R;)V

    iget-object v1, p0, Landroid/support/v4/widget/q;->eDg:Landroid/support/v4/widget/I;

    iget-object v2, p0, Landroid/support/v4/widget/q;->eDf:Landroid/support/v4/widget/R;

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Landroid/support/v4/widget/I;->dVr(Landroid/support/v4/widget/I;FLandroid/support/v4/widget/R;Z)V

    iget-object v0, p0, Landroid/support/v4/widget/q;->eDg:Landroid/support/v4/widget/I;

    invoke-virtual {v0}, Landroid/support/v4/widget/I;->invalidateSelf()V

    return-void
.end method
