.class public Landroid/support/v4/a/d;
.super Ljava/lang/Object;
.source "Pools.java"

# interfaces
.implements Landroid/support/v4/a/j;


# instance fields
.field private final eBt:[Ljava/lang/Object;

.field private eBu:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-gtz p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "The max pool size must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/d;->eBt:[Ljava/lang/Object;

    return-void
.end method

.method private dSB(Ljava/lang/Object;)Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget v2, p0, Landroid/support/v4/a/d;->eBu:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Landroid/support/v4/a/d;->eBt:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method


# virtual methods
.method public dSC(Ljava/lang/Object;)Z
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v4/a/d;->dSB(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Already in the pool!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Landroid/support/v4/a/d;->eBu:I

    iget-object v1, p0, Landroid/support/v4/a/d;->eBt:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Landroid/support/v4/a/d;->eBt:[Ljava/lang/Object;

    iget v1, p0, Landroid/support/v4/a/d;->eBu:I

    aput-object p1, v0, v1

    iget v0, p0, Landroid/support/v4/a/d;->eBu:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/a/d;->eBu:I

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public dSD()Ljava/lang/Object;
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Landroid/support/v4/a/d;->eBu:I

    if-lez v0, :cond_0

    iget v0, p0, Landroid/support/v4/a/d;->eBu:I

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Landroid/support/v4/a/d;->eBt:[Ljava/lang/Object;

    aget-object v1, v1, v0

    iget-object v2, p0, Landroid/support/v4/a/d;->eBt:[Ljava/lang/Object;

    aput-object v3, v2, v0

    iget v0, p0, Landroid/support/v4/a/d;->eBu:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v4/a/d;->eBu:I

    return-object v1

    :cond_0
    return-object v3
.end method
