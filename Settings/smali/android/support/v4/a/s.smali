.class final Landroid/support/v4/a/s;
.super Landroid/support/v4/a/l;
.source "ArraySet.java"


# instance fields
.field final synthetic eCb:Landroid/support/v4/a/c;


# direct methods
.method constructor <init>(Landroid/support/v4/a/c;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/a/s;->eCb:Landroid/support/v4/a/c;

    invoke-direct {p0}, Landroid/support/v4/a/l;-><init>()V

    return-void
.end method


# virtual methods
.method protected dSO(II)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/s;->eCb:Landroid/support/v4/a/c;

    iget-object v0, v0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected dSP(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/s;->eCb:Landroid/support/v4/a/c;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/c;->dSA(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected dSQ(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/s;->eCb:Landroid/support/v4/a/c;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/c;->dSA(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected dSU()Ljava/util/Map;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "not a map"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected dSV(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "not a map"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected dSW()V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/s;->eCb:Landroid/support/v4/a/c;

    invoke-virtual {v0}, Landroid/support/v4/a/c;->clear()V

    return-void
.end method

.method protected dSZ()I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/s;->eCb:Landroid/support/v4/a/c;

    iget v0, v0, Landroid/support/v4/a/c;->eBi:I

    return v0
.end method

.method protected dTa(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/s;->eCb:Landroid/support/v4/a/c;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/c;->dSw(I)Ljava/lang/Object;

    return-void
.end method

.method protected dTb(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/s;->eCb:Landroid/support/v4/a/c;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/c;->add(Ljava/lang/Object;)Z

    return-void
.end method
