.class public Landroid/support/v4/a/a;
.super Ljava/lang/Object;
.source "SimpleArrayMap.java"


# static fields
.field static eAX:I

.field static eAY:[Ljava/lang/Object;

.field static eAZ:[Ljava/lang/Object;

.field static eBa:I


# instance fields
.field eAW:I

.field eBb:[I

.field eBc:[Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroid/support/v4/a/t;->eCc:[I

    iput-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    sget-object v0, Landroid/support/v4/a/t;->eCe:[Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/a/a;->eAW:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    sget-object v0, Landroid/support/v4/a/t;->eCc:[I

    iput-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    sget-object v0, Landroid/support/v4/a/t;->eCe:[Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    :goto_0
    iput v1, p0, Landroid/support/v4/a/a;->eAW:I

    return-void

    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v4/a/a;->dSb(I)V

    goto :goto_0
.end method

.method private static dSa([III)I
    .locals 1

    :try_start_0
    invoke-static {p0, p1, p2}, Landroid/support/v4/a/t;->dTs([III)I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0
.end method

.method private dSb(I)V
    .locals 4

    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    const-class v1, Landroid/support/v4/a/u;

    monitor-enter v1

    :try_start_0
    sget-object v0, Landroid/support/v4/a/a;->eAZ:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    sget-object v2, Landroid/support/v4/a/a;->eAZ:[Ljava/lang/Object;

    iput-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    const/4 v0, 0x0

    aget-object v0, v2, v0

    check-cast v0, [Ljava/lang/Object;

    sput-object v0, Landroid/support/v4/a/a;->eAZ:[Ljava/lang/Object;

    const/4 v0, 0x1

    aget-object v0, v2, v0

    check-cast v0, [I

    iput-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    const/4 v0, 0x0

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    sget v0, Landroid/support/v4/a/a;->eBa:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Landroid/support/v4/a/a;->eBa:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :cond_0
    monitor-exit v1

    :cond_1
    new-array v0, p1, [I

    iput-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    shl-int/lit8 v0, p1, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    const-class v1, Landroid/support/v4/a/u;

    monitor-enter v1

    :try_start_1
    sget-object v0, Landroid/support/v4/a/a;->eAY:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    sget-object v2, Landroid/support/v4/a/a;->eAY:[Ljava/lang/Object;

    iput-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    const/4 v0, 0x0

    aget-object v0, v2, v0

    check-cast v0, [Ljava/lang/Object;

    sput-object v0, Landroid/support/v4/a/a;->eAY:[Ljava/lang/Object;

    const/4 v0, 0x1

    aget-object v0, v2, v0

    check-cast v0, [I

    iput-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    const/4 v0, 0x0

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    sget v0, Landroid/support/v4/a/a;->eAX:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Landroid/support/v4/a/a;->eAX:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v1

    return-void

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static dSd([I[Ljava/lang/Object;I)V
    .locals 4

    const/16 v2, 0xa

    const/4 v3, 0x2

    array-length v0, p0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    const-class v1, Landroid/support/v4/a/u;

    monitor-enter v1

    :try_start_0
    sget v0, Landroid/support/v4/a/a;->eBa:I

    if-ge v0, v2, :cond_1

    sget-object v0, Landroid/support/v4/a/a;->eAZ:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, p1, v2

    const/4 v0, 0x1

    aput-object p0, p1, v0

    shl-int/lit8 v0, p2, 0x1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lt v0, v3, :cond_0

    const/4 v2, 0x0

    aput-object v2, p1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    sput-object p1, Landroid/support/v4/a/a;->eAZ:[Ljava/lang/Object;

    sget v0, Landroid/support/v4/a/a;->eBa:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Landroid/support/v4/a/a;->eBa:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_1
    monitor-exit v1

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    array-length v0, p0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const-class v1, Landroid/support/v4/a/u;

    monitor-enter v1

    :try_start_1
    sget v0, Landroid/support/v4/a/a;->eAX:I

    if-ge v0, v2, :cond_1

    sget-object v0, Landroid/support/v4/a/a;->eAY:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, p1, v2

    const/4 v0, 0x1

    aput-object p0, p1, v0

    shl-int/lit8 v0, p2, 0x1

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-lt v0, v3, :cond_4

    const/4 v2, 0x0

    aput-object v2, p1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_4
    sput-object p1, Landroid/support/v4/a/a;->eAY:[Ljava/lang/Object;

    sget v0, Landroid/support/v4/a/a;->eAX:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Landroid/support/v4/a/a;->eAX:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public clear()V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Landroid/support/v4/a/a;->eAW:I

    if-lez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    iget-object v1, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    iget v2, p0, Landroid/support/v4/a/a;->eAW:I

    sget-object v3, Landroid/support/v4/a/t;->eCc:[I

    iput-object v3, p0, Landroid/support/v4/a/a;->eBb:[I

    sget-object v3, Landroid/support/v4/a/t;->eCe:[Ljava/lang/Object;

    iput-object v3, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    iput v4, p0, Landroid/support/v4/a/a;->eAW:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/a/a;->dSd([I[Ljava/lang/Object;I)V

    :cond_0
    iget v0, p0, Landroid/support/v4/a/a;->eAW:I

    if-lez v0, :cond_1

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_1
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Landroid/support/v4/a/a;->dSi(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Landroid/support/v4/a/a;->dRY(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method dRY(Ljava/lang/Object;)I
    .locals 4

    const/4 v0, 0x1

    iget v1, p0, Landroid/support/v4/a/a;->eAW:I

    mul-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    if-nez p1, :cond_2

    :goto_0
    if-ge v0, v1, :cond_3

    aget-object v3, v2, v0

    if-nez v3, :cond_0

    shr-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x2

    :cond_2
    if-ge v0, v1, :cond_3

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    shr-int/lit8 v0, v0, 0x1

    return v0

    :cond_3
    const/4 v0, -0x1

    return v0
.end method

.method public dRZ(I)Ljava/lang/Object;
    .locals 9

    const/4 v8, 0x0

    const/16 v0, 0x8

    const/4 v1, 0x0

    iget-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v3, p1, 0x1

    add-int/lit8 v3, v3, 0x1

    aget-object v3, v2, v3

    iget v4, p0, Landroid/support/v4/a/a;->eAW:I

    const/4 v2, 0x1

    if-gt v4, v2, :cond_0

    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    iget-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    invoke-static {v0, v2, v4}, Landroid/support/v4/a/a;->dSd([I[Ljava/lang/Object;I)V

    sget-object v0, Landroid/support/v4/a/t;->eCc:[I

    iput-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    sget-object v0, Landroid/support/v4/a/t;->eCe:[Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    move v0, v1

    :goto_0
    iget v1, p0, Landroid/support/v4/a/a;->eAW:I

    if-eq v4, v1, :cond_7

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_0
    add-int/lit8 v2, v4, -0x1

    iget-object v5, p0, Landroid/support/v4/a/a;->eBb:[I

    array-length v5, v5

    if-le v5, v0, :cond_5

    iget v5, p0, Landroid/support/v4/a/a;->eAW:I

    iget-object v6, p0, Landroid/support/v4/a/a;->eBb:[I

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    if-ge v5, v6, :cond_5

    if-le v4, v0, :cond_1

    shr-int/lit8 v0, v4, 0x1

    add-int/2addr v0, v4

    :cond_1
    iget-object v5, p0, Landroid/support/v4/a/a;->eBb:[I

    iget-object v6, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    invoke-direct {p0, v0}, Landroid/support/v4/a/a;->dSb(I)V

    iget v0, p0, Landroid/support/v4/a/a;->eAW:I

    if-eq v4, v0, :cond_2

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_2
    if-lez p1, :cond_3

    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    invoke-static {v5, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v7, p1, 0x1

    invoke-static {v6, v1, v0, v1, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    if-ge p1, v2, :cond_4

    add-int/lit8 v0, p1, 0x1

    iget-object v1, p0, Landroid/support/v4/a/a;->eBb:[I

    sub-int v7, v2, p1

    invoke-static {v5, v0, v1, p1, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, p1, 0x1

    shl-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v5, p1, 0x1

    sub-int v7, v2, p1

    shl-int/lit8 v7, v7, 0x1

    invoke-static {v6, v0, v1, v5, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    if-ge p1, v2, :cond_6

    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    add-int/lit8 v1, p1, 0x1

    iget-object v5, p0, Landroid/support/v4/a/a;->eBb:[I

    sub-int v6, v2, p1

    invoke-static {v0, v1, v5, p1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    add-int/lit8 v1, p1, 0x1

    shl-int/lit8 v1, v1, 0x1

    iget-object v5, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v6, p1, 0x1

    sub-int v7, v2, p1

    shl-int/lit8 v7, v7, 0x1

    invoke-static {v0, v1, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    aput-object v8, v0, v1

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    aput-object v8, v0, v1

    move v0, v2

    goto/16 :goto_0

    :cond_7
    iput v0, p0, Landroid/support/v4/a/a;->eAW:I

    return-object v3
.end method

.method dSc()I
    .locals 5

    const/4 v1, 0x0

    iget v2, p0, Landroid/support/v4/a/a;->eAW:I

    if-nez v2, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    invoke-static {v0, v2, v1}, Landroid/support/v4/a/a;->dSa([III)I

    move-result v3

    if-gez v3, :cond_1

    return v3

    :cond_1
    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v1, v3, 0x1

    aget-object v0, v0, v1

    if-nez v0, :cond_2

    return v3

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    aget v0, v0, v1

    if-nez v0, :cond_4

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v4, v1, 0x1

    aget-object v0, v0, v4

    if-nez v0, :cond_3

    return v1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v3, -0x1

    :goto_1
    if-ltz v0, :cond_6

    iget-object v2, p0, Landroid/support/v4/a/a;->eBb:[I

    aget v2, v2, v0

    if-nez v2, :cond_6

    iget-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    if-nez v2, :cond_5

    return v0

    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_6
    not-int v0, v1

    return v0
.end method

.method public dSe(I)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v1, p1, 0x1

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public dSf(I)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public dSg(I)V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Landroid/support/v4/a/a;->eAW:I

    iget-object v1, p0, Landroid/support/v4/a/a;->eBb:[I

    array-length v1, v1

    if-ge v1, p1, :cond_1

    iget-object v1, p0, Landroid/support/v4/a/a;->eBb:[I

    iget-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    invoke-direct {p0, p1}, Landroid/support/v4/a/a;->dSb(I)V

    iget v3, p0, Landroid/support/v4/a/a;->eAW:I

    if-lez v3, :cond_0

    iget-object v3, p0, Landroid/support/v4/a/a;->eBb:[I

    invoke-static {v1, v5, v3, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v4, v0, 0x1

    invoke-static {v2, v5, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    invoke-static {v1, v2, v0}, Landroid/support/v4/a/a;->dSd([I[Ljava/lang/Object;I)V

    :cond_1
    iget v1, p0, Landroid/support/v4/a/a;->eAW:I

    if-eq v1, v0, :cond_2

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_2
    return-void
.end method

.method dSh(Ljava/lang/Object;I)I
    .locals 5

    iget v2, p0, Landroid/support/v4/a/a;->eAW:I

    if-nez v2, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    invoke-static {v0, v2, p2}, Landroid/support/v4/a/a;->dSa([III)I

    move-result v3

    if-gez v3, :cond_1

    return v3

    :cond_1
    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v1, v3, 0x1

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v3

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    aget v0, v0, v1

    if-ne v0, p2, :cond_4

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v4, v1, 0x1

    aget-object v0, v0, v4

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    return v1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v3, -0x1

    :goto_1
    if-ltz v0, :cond_6

    iget-object v2, p0, Landroid/support/v4/a/a;->eBb:[I

    aget v2, v2, v0

    if-ne v2, p2, :cond_6

    iget-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    return v0

    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_6
    not-int v0, v1

    return v0
.end method

.method public dSi(Ljava/lang/Object;)I
    .locals 1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/a/a;->dSc()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/a/a;->dSh(Ljava/lang/Object;I)I

    move-result v0

    goto :goto_0
.end method

.method public dSj(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 3

    shl-int/lit8 v0, p1, 0x1

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    aget-object v1, v1, v0

    iget-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    aput-object p2, v2, v0

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_0

    return v5

    :cond_0
    instance-of v0, p1, Landroid/support/v4/a/a;

    if-eqz v0, :cond_6

    check-cast p1, Landroid/support/v4/a/a;

    invoke-virtual {p0}, Landroid/support/v4/a/a;->size()I

    move-result v0

    invoke-virtual {p1}, Landroid/support/v4/a/a;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    return v1

    :cond_1
    move v0, v1

    :goto_0
    :try_start_0
    iget v2, p0, Landroid/support/v4/a/a;->eAW:I

    if-ge v0, v2, :cond_5

    invoke-virtual {p0, v0}, Landroid/support/v4/a/a;->dSf(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0}, Landroid/support/v4/a/a;->dSe(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v2}, Landroid/support/v4/a/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v3, :cond_3

    if-nez v4, :cond_2

    invoke-virtual {p1, v2}, Landroid/support/v4/a/a;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    :cond_2
    return v1

    :cond_3
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_4

    return v1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    return v1

    :catch_1
    move-exception v0

    return v1

    :cond_5
    return v5

    :cond_6
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_c

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0}, Landroid/support/v4/a/a;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v2

    if-eq v0, v2, :cond_7

    return v1

    :cond_7
    move v0, v1

    :goto_1
    :try_start_1
    iget v2, p0, Landroid/support/v4/a/a;->eAW:I

    if-ge v0, v2, :cond_b

    invoke-virtual {p0, v0}, Landroid/support/v4/a/a;->dSf(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0}, Landroid/support/v4/a/a;->dSe(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v3, :cond_9

    if-nez v4, :cond_8

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_a

    :cond_8
    return v1

    :cond_9
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v2

    if-nez v2, :cond_a

    return v1

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_2
    move-exception v0

    return v1

    :catch_3
    move-exception v0

    return v1

    :cond_b
    return v5

    :cond_c
    return v1
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0, p1}, Landroid/support/v4/a/a;->dSi(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    const/4 v1, 0x0

    iget-object v5, p0, Landroid/support/v4/a/a;->eBb:[I

    iget-object v6, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    const/4 v0, 0x1

    iget v7, p0, Landroid/support/v4/a/a;->eAW:I

    move v2, v0

    move v3, v1

    move v4, v1

    :goto_0
    if-ge v3, v7, :cond_1

    aget-object v0, v6, v2

    aget v8, v5, v3

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    xor-int/2addr v0, v8

    add-int/2addr v4, v0

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    return v4
.end method

.method public isEmpty()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v4/a/a;->eAW:I

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    const/4 v8, 0x0

    const/16 v0, 0x8

    const/4 v1, 0x4

    const/4 v4, 0x0

    iget v5, p0, Landroid/support/v4/a/a;->eAW:I

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/a/a;->dSc()I

    move-result v2

    move v3, v4

    :goto_0
    if-ltz v2, :cond_1

    shl-int/lit8 v0, v2, 0x1

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    aget-object v1, v1, v0

    iget-object v2, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    aput-object p2, v2, v0

    return-object v1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {p0, p1, v3}, Landroid/support/v4/a/a;->dSh(Ljava/lang/Object;I)I

    move-result v2

    goto :goto_0

    :cond_1
    not-int v2, v2

    iget-object v6, p0, Landroid/support/v4/a/a;->eBb:[I

    array-length v6, v6

    if-lt v5, v6, :cond_6

    if-lt v5, v0, :cond_3

    shr-int/lit8 v0, v5, 0x1

    add-int/2addr v0, v5

    :cond_2
    :goto_1
    iget-object v1, p0, Landroid/support/v4/a/a;->eBb:[I

    iget-object v6, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    invoke-direct {p0, v0}, Landroid/support/v4/a/a;->dSb(I)V

    iget v0, p0, Landroid/support/v4/a/a;->eAW:I

    if-eq v5, v0, :cond_4

    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_3
    if-ge v5, v1, :cond_2

    move v0, v1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    array-length v0, v0

    if-lez v0, :cond_5

    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    array-length v7, v1

    invoke-static {v1, v4, v0, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    array-length v7, v6

    invoke-static {v6, v4, v0, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v1, v6, v5}, Landroid/support/v4/a/a;->dSd([I[Ljava/lang/Object;I)V

    :cond_6
    if-ge v2, v5, :cond_7

    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    iget-object v1, p0, Landroid/support/v4/a/a;->eBb:[I

    add-int/lit8 v4, v2, 0x1

    sub-int v6, v5, v2

    invoke-static {v0, v2, v1, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    iget-object v4, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    add-int/lit8 v6, v2, 0x1

    shl-int/lit8 v6, v6, 0x1

    iget v7, p0, Landroid/support/v4/a/a;->eAW:I

    sub-int/2addr v7, v2

    shl-int/lit8 v7, v7, 0x1

    invoke-static {v0, v1, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iget v0, p0, Landroid/support/v4/a/a;->eAW:I

    if-ne v5, v0, :cond_8

    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    array-length v0, v0

    if-lt v2, v0, :cond_9

    :cond_8
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    :cond_9
    iget-object v0, p0, Landroid/support/v4/a/a;->eBb:[I

    aput v3, v0, v2

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    aput-object p1, v0, v1

    iget-object v0, p0, Landroid/support/v4/a/a;->eBc:[Ljava/lang/Object;

    shl-int/lit8 v1, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    aput-object p2, v0, v1

    iget v0, p0, Landroid/support/v4/a/a;->eAW:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/a/a;->eAW:I

    return-object v8
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v4/a/a;->dSi(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v4/a/a;->dRZ(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget v0, p0, Landroid/support/v4/a/a;->eAW:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v4/a/a;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "{}"

    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Landroid/support/v4/a/a;->eAW:I

    mul-int/lit8 v2, v2, 0x1c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v2, 0x7b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    iget v2, p0, Landroid/support/v4/a/a;->eAW:I

    if-ge v0, v2, :cond_4

    if-lez v0, :cond_1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p0, v0}, Landroid/support/v4/a/a;->dSf(I)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p0, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_1
    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Landroid/support/v4/a/a;->dSe(I)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p0, :cond_3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-string/jumbo v2, "(this Map)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const-string/jumbo v2, "(this Map)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
