.class public Landroid/support/v4/a/b;
.super Ljava/lang/Object;
.source "SparseArrayCompat.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final eBd:Ljava/lang/Object;


# instance fields
.field private eBe:I

.field private eBf:[I

.field private eBg:[Ljava/lang/Object;

.field private eBh:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/a/b;->eBd:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Landroid/support/v4/a/b;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Landroid/support/v4/a/b;->eBh:Z

    if-nez p1, :cond_0

    sget-object v0, Landroid/support/v4/a/t;->eCc:[I

    iput-object v0, p0, Landroid/support/v4/a/b;->eBf:[I

    sget-object v0, Landroid/support/v4/a/t;->eCe:[Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    :goto_0
    iput v2, p0, Landroid/support/v4/a/b;->eBe:I

    return-void

    :cond_0
    invoke-static {p1}, Landroid/support/v4/a/t;->dTo(I)I

    move-result v0

    new-array v1, v0, [I

    iput-object v1, p0, Landroid/support/v4/a/b;->eBf:[I

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    goto :goto_0
.end method

.method private dSk()V
    .locals 8

    const/4 v2, 0x0

    iget v3, p0, Landroid/support/v4/a/b;->eBe:I

    iget-object v4, p0, Landroid/support/v4/a/b;->eBf:[I

    iget-object v5, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    move v1, v2

    move v0, v2

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v6, v5, v1

    sget-object v7, Landroid/support/v4/a/b;->eBd:Ljava/lang/Object;

    if-eq v6, v7, :cond_1

    if-eq v1, v0, :cond_0

    aget v7, v4, v1

    aput v7, v4, v0

    aput-object v6, v5, v0

    const/4 v6, 0x0

    aput-object v6, v5, v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iput-boolean v2, p0, Landroid/support/v4/a/b;->eBh:Z

    iput v0, p0, Landroid/support/v4/a/b;->eBe:I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 5

    const/4 v1, 0x0

    iget v2, p0, Landroid/support/v4/a/b;->eBe:I

    iget-object v3, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    const/4 v4, 0x0

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Landroid/support/v4/a/b;->eBe:I

    iput-boolean v1, p0, Landroid/support/v4/a/b;->eBh:Z

    return-void
.end method

.method public clone()Landroid/support/v4/a/b;
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/b;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v1, p0, Landroid/support/v4/a/b;->eBf:[I

    invoke-virtual {v1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, Landroid/support/v4/a/b;->eBf:[I

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    iput-object v1, v0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v4/a/b;->clone()Landroid/support/v4/a/b;

    move-result-object v0

    return-object v0
.end method

.method public dSl(I)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/a/b;->eBf:[I

    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    invoke-static {v0, v1, p1}, Landroid/support/v4/a/t;->dTs([III)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, Landroid/support/v4/a/b;->eBd:Ljava/lang/Object;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    sget-object v2, Landroid/support/v4/a/b;->eBd:Ljava/lang/Object;

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/a/b;->eBh:Z

    :cond_0
    return-void
.end method

.method public dSm(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Landroid/support/v4/a/b;->eBf:[I

    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    invoke-static {v0, v1, p1}, Landroid/support/v4/a/t;->dTs([III)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, Landroid/support/v4/a/b;->eBd:Ljava/lang/Object;

    if-ne v1, v2, :cond_1

    :cond_0
    return-object p2

    :cond_1
    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    aget-object v0, v1, v0

    return-object v0
.end method

.method public dSn(ILjava/lang/Object;)V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Landroid/support/v4/a/b;->eBe:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/a/b;->eBf:[I

    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    if-gt p1, v0, :cond_0

    invoke-virtual {p0, p1, p2}, Landroid/support/v4/a/b;->dSr(ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-boolean v0, p0, Landroid/support/v4/a/b;->eBh:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v4/a/b;->eBe:I

    iget-object v1, p0, Landroid/support/v4/a/b;->eBf:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    invoke-direct {p0}, Landroid/support/v4/a/b;->dSk()V

    :cond_1
    iget v0, p0, Landroid/support/v4/a/b;->eBe:I

    iget-object v1, p0, Landroid/support/v4/a/b;->eBf:[I

    array-length v1, v1

    if-lt v0, v1, :cond_2

    add-int/lit8 v1, v0, 0x1

    invoke-static {v1}, Landroid/support/v4/a/t;->dTo(I)I

    move-result v1

    new-array v2, v1, [I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Landroid/support/v4/a/b;->eBf:[I

    iget-object v4, p0, Landroid/support/v4/a/b;->eBf:[I

    array-length v4, v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    iget-object v4, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Landroid/support/v4/a/b;->eBf:[I

    iput-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    :cond_2
    iget-object v1, p0, Landroid/support/v4/a/b;->eBf:[I

    aput p1, v1, v0

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    aput-object p2, v1, v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/a/b;->eBe:I

    return-void
.end method

.method public dSo(I)Ljava/lang/Object;
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/a/b;->eBh:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v4/a/b;->dSk()V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public dSp(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/a/b;->dSm(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public dSq(I)I
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/a/b;->eBh:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v4/a/b;->dSk()V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/b;->eBf:[I

    aget v0, v0, p1

    return v0
.end method

.method public dSr(ILjava/lang/Object;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v4/a/b;->eBf:[I

    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    invoke-static {v0, v1, p1}, Landroid/support/v4/a/t;->dTs([III)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    aput-object p2, v1, v0

    :goto_0
    return-void

    :cond_0
    not-int v0, v0

    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, Landroid/support/v4/a/b;->eBd:Ljava/lang/Object;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Landroid/support/v4/a/b;->eBf:[I

    aput p1, v1, v0

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    aput-object p2, v1, v0

    return-void

    :cond_1
    iget-boolean v1, p0, Landroid/support/v4/a/b;->eBh:Z

    if-eqz v1, :cond_2

    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    iget-object v2, p0, Landroid/support/v4/a/b;->eBf:[I

    array-length v2, v2

    if-lt v1, v2, :cond_2

    invoke-direct {p0}, Landroid/support/v4/a/b;->dSk()V

    iget-object v0, p0, Landroid/support/v4/a/b;->eBf:[I

    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    invoke-static {v0, v1, p1}, Landroid/support/v4/a/t;->dTs([III)I

    move-result v0

    not-int v0, v0

    :cond_2
    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    iget-object v2, p0, Landroid/support/v4/a/b;->eBf:[I

    array-length v2, v2

    if-lt v1, v2, :cond_3

    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Landroid/support/v4/a/t;->dTo(I)I

    move-result v1

    new-array v2, v1, [I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Landroid/support/v4/a/b;->eBf:[I

    iget-object v4, p0, Landroid/support/v4/a/b;->eBf:[I

    array-length v4, v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    iget-object v4, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Landroid/support/v4/a/b;->eBf:[I

    iput-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    :cond_3
    iget v1, p0, Landroid/support/v4/a/b;->eBe:I

    sub-int/2addr v1, v0

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v4/a/b;->eBf:[I

    iget-object v2, p0, Landroid/support/v4/a/b;->eBf:[I

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Landroid/support/v4/a/b;->eBe:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Landroid/support/v4/a/b;->eBe:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iget-object v1, p0, Landroid/support/v4/a/b;->eBf:[I

    aput p1, v1, v0

    iget-object v1, p0, Landroid/support/v4/a/b;->eBg:[Ljava/lang/Object;

    aput-object p2, v1, v0

    iget v0, p0, Landroid/support/v4/a/b;->eBe:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/a/b;->eBe:I

    goto :goto_0
.end method

.method public remove(I)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v4/a/b;->dSl(I)V

    return-void
.end method

.method public size()I
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/a/b;->eBh:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v4/a/b;->dSk()V

    :cond_0
    iget v0, p0, Landroid/support/v4/a/b;->eBe:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v4/a/b;->size()I

    move-result v1

    if-gtz v1, :cond_0

    const-string/jumbo v0, "{}"

    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Landroid/support/v4/a/b;->eBe:I

    mul-int/lit8 v2, v2, 0x1c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v2, 0x7b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    iget v2, p0, Landroid/support/v4/a/b;->eBe:I

    if-ge v0, v2, :cond_3

    if-lez v0, :cond_1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p0, v0}, Landroid/support/v4/a/b;->dSq(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Landroid/support/v4/a/b;->dSo(I)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p0, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-string/jumbo v2, "(this Map)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
