.class abstract Landroid/support/v4/a/l;
.super Ljava/lang/Object;
.source "MapCollections.java"


# instance fields
.field eBP:Landroid/support/v4/a/p;

.field eBQ:Landroid/support/v4/a/r;

.field eBR:Landroid/support/v4/a/f;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dSX(Ljava/util/Map;Ljava/util/Collection;)Z
    .locals 2

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public static dSY(Ljava/util/Map;Ljava/util/Collection;)Z
    .locals 3

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static dTc(Ljava/util/Map;Ljava/util/Collection;)Z
    .locals 3

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static dTd(Ljava/util/Set;Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    instance-of v1, p1, Ljava/util/Set;

    if-eqz v1, :cond_2

    check-cast p1, Ljava/util/Set;

    :try_start_0
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-interface {p0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_1
    return v0

    :catch_0
    move-exception v1

    return v0

    :catch_1
    move-exception v1

    return v0

    :cond_2
    return v0
.end method


# virtual methods
.method public dSM()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/l;->eBQ:Landroid/support/v4/a/r;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/a/r;

    invoke-direct {v0, p0}, Landroid/support/v4/a/r;-><init>(Landroid/support/v4/a/l;)V

    iput-object v0, p0, Landroid/support/v4/a/l;->eBQ:Landroid/support/v4/a/r;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/l;->eBQ:Landroid/support/v4/a/r;

    return-object v0
.end method

.method public dSN(I)[Ljava/lang/Object;
    .locals 4

    invoke-virtual {p0}, Landroid/support/v4/a/l;->dSZ()I

    move-result v1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0, p1}, Landroid/support/v4/a/l;->dSO(II)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method protected abstract dSO(II)Ljava/lang/Object;
.end method

.method protected abstract dSP(Ljava/lang/Object;)I
.end method

.method protected abstract dSQ(Ljava/lang/Object;)I
.end method

.method public dSR([Ljava/lang/Object;I)[Ljava/lang/Object;
    .locals 4

    invoke-virtual {p0}, Landroid/support/v4/a/l;->dSZ()I

    move-result v2

    array-length v0, p1

    if-ge v0, v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1, p2}, Landroid/support/v4/a/l;->dSO(II)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    array-length v1, v0

    if-le v1, v2, :cond_1

    const/4 v1, 0x0

    aput-object v1, v0, v2

    :cond_1
    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public dSS()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/l;->eBP:Landroid/support/v4/a/p;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/a/p;

    invoke-direct {v0, p0}, Landroid/support/v4/a/p;-><init>(Landroid/support/v4/a/l;)V

    iput-object v0, p0, Landroid/support/v4/a/l;->eBP:Landroid/support/v4/a/p;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/l;->eBP:Landroid/support/v4/a/p;

    return-object v0
.end method

.method public dST()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/l;->eBR:Landroid/support/v4/a/f;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/a/f;

    invoke-direct {v0, p0}, Landroid/support/v4/a/f;-><init>(Landroid/support/v4/a/l;)V

    iput-object v0, p0, Landroid/support/v4/a/l;->eBR:Landroid/support/v4/a/f;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/l;->eBR:Landroid/support/v4/a/f;

    return-object v0
.end method

.method protected abstract dSU()Ljava/util/Map;
.end method

.method protected abstract dSV(ILjava/lang/Object;)Ljava/lang/Object;
.end method

.method protected abstract dSW()V
.end method

.method protected abstract dSZ()I
.end method

.method protected abstract dTa(I)V
.end method

.method protected abstract dTb(Ljava/lang/Object;Ljava/lang/Object;)V
.end method
