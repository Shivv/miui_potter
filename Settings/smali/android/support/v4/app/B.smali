.class final Landroid/support/v4/app/B;
.super Ljava/lang/Object;
.source "FragmentTransition.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic eLR:Landroid/support/v4/app/p;

.field final synthetic eLS:Landroid/support/v4/app/p;

.field final synthetic eLT:Landroid/graphics/Rect;

.field final synthetic eLU:Ljava/lang/Object;

.field final synthetic eLV:Ljava/util/ArrayList;

.field final synthetic eLW:Landroid/support/v4/a/u;

.field final synthetic eLX:Landroid/support/v4/app/am;

.field final synthetic eLY:Z

.field final synthetic eLZ:Ljava/lang/Object;

.field final synthetic eMa:Landroid/support/v4/app/z;

.field final synthetic eMb:Ljava/util/ArrayList;

.field final synthetic eMc:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;Ljava/util/ArrayList;Landroid/view/View;Landroid/support/v4/app/p;Landroid/support/v4/app/p;ZLjava/util/ArrayList;Ljava/lang/Object;Landroid/graphics/Rect;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/app/B;->eLX:Landroid/support/v4/app/am;

    iput-object p2, p0, Landroid/support/v4/app/B;->eLW:Landroid/support/v4/a/u;

    iput-object p3, p0, Landroid/support/v4/app/B;->eLZ:Ljava/lang/Object;

    iput-object p4, p0, Landroid/support/v4/app/B;->eMa:Landroid/support/v4/app/z;

    iput-object p5, p0, Landroid/support/v4/app/B;->eLV:Ljava/util/ArrayList;

    iput-object p6, p0, Landroid/support/v4/app/B;->eMc:Landroid/view/View;

    iput-object p7, p0, Landroid/support/v4/app/B;->eLR:Landroid/support/v4/app/p;

    iput-object p8, p0, Landroid/support/v4/app/B;->eLS:Landroid/support/v4/app/p;

    iput-boolean p9, p0, Landroid/support/v4/app/B;->eLY:Z

    iput-object p10, p0, Landroid/support/v4/app/B;->eMb:Ljava/util/ArrayList;

    iput-object p11, p0, Landroid/support/v4/app/B;->eLU:Ljava/lang/Object;

    iput-object p12, p0, Landroid/support/v4/app/B;->eLT:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Landroid/support/v4/app/B;->eLX:Landroid/support/v4/app/am;

    iget-object v1, p0, Landroid/support/v4/app/B;->eLW:Landroid/support/v4/a/u;

    iget-object v2, p0, Landroid/support/v4/app/B;->eLZ:Ljava/lang/Object;

    iget-object v3, p0, Landroid/support/v4/app/B;->eMa:Landroid/support/v4/app/z;

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/app/af;->eeV(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;)Landroid/support/v4/a/u;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/B;->eLV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Landroid/support/v4/app/B;->eLV:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/B;->eMc:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/B;->eLR:Landroid/support/v4/app/p;

    iget-object v2, p0, Landroid/support/v4/app/B;->eLS:Landroid/support/v4/app/p;

    iget-boolean v3, p0, Landroid/support/v4/app/B;->eLY:Z

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v0, v4}, Landroid/support/v4/app/af;->eeN(Landroid/support/v4/app/p;Landroid/support/v4/app/p;ZLandroid/support/v4/a/u;Z)V

    iget-object v1, p0, Landroid/support/v4/app/B;->eLZ:Ljava/lang/Object;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/B;->eLX:Landroid/support/v4/app/am;

    iget-object v2, p0, Landroid/support/v4/app/B;->eLZ:Ljava/lang/Object;

    iget-object v3, p0, Landroid/support/v4/app/B;->eMb:Ljava/util/ArrayList;

    iget-object v4, p0, Landroid/support/v4/app/B;->eLV:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v4/app/am;->efz(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v1, p0, Landroid/support/v4/app/B;->eMa:Landroid/support/v4/app/z;

    iget-object v2, p0, Landroid/support/v4/app/B;->eLU:Ljava/lang/Object;

    iget-boolean v3, p0, Landroid/support/v4/app/B;->eLY:Z

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/app/af;->eeH(Landroid/support/v4/a/u;Landroid/support/v4/app/z;Ljava/lang/Object;Z)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/B;->eLX:Landroid/support/v4/app/am;

    iget-object v2, p0, Landroid/support/v4/app/B;->eLT:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/am;->egl(Landroid/view/View;Landroid/graphics/Rect;)V

    :cond_1
    return-void
.end method
