.class final Landroid/support/v4/app/an;
.super Landroid/support/v4/app/ar;
.source "FragmentManager.java"

# interfaces
.implements Landroid/view/LayoutInflater$Factory2;


# static fields
.field static final eOP:Landroid/view/animation/Interpolator;

.field static final eOQ:Landroid/view/animation/Interpolator;

.field static final ePa:Landroid/view/animation/Interpolator;

.field static ePd:Z

.field static ePj:Ljava/lang/reflect/Field;

.field static final ePl:Landroid/view/animation/Interpolator;


# instance fields
.field eOE:Ljava/util/ArrayList;

.field eOF:Ljava/lang/Runnable;

.field eOG:Landroid/support/v4/app/p;

.field eOH:Landroid/support/v4/app/C;

.field eOI:Landroid/os/Bundle;

.field eOJ:Ljava/lang/String;

.field eOK:Ljava/util/ArrayList;

.field eOL:Z

.field final eOM:Ljava/util/ArrayList;

.field eON:Z

.field eOO:Landroid/support/v4/app/E;

.field eOR:Landroid/support/v4/app/k;

.field eOS:Z

.field eOT:Z

.field eOU:Ljava/util/ArrayList;

.field eOV:Ljava/util/ArrayList;

.field eOW:Ljava/util/ArrayList;

.field eOX:Ljava/util/ArrayList;

.field eOY:Ljava/util/ArrayList;

.field private final eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

.field ePb:Ljava/util/ArrayList;

.field ePc:Landroid/support/v4/app/p;

.field ePe:Landroid/util/SparseArray;

.field ePf:I

.field ePg:I

.field ePh:Ljava/util/ArrayList;

.field ePi:Landroid/util/SparseArray;

.field ePk:Ljava/util/ArrayList;

.field ePm:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/high16 v2, 0x40200000    # 2.5f

    const/high16 v1, 0x3fc00000    # 1.5f

    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    const/4 v0, 0x0

    sput-object v0, Landroid/support/v4/app/an;->ePj:Ljava/lang/reflect/Field;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Landroid/support/v4/app/an;->eOP:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Landroid/support/v4/app/an;->ePl:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Landroid/support/v4/app/an;->ePa:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v1}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Landroid/support/v4/app/an;->eOQ:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/ar;-><init>()V

    iput v1, p0, Landroid/support/v4/app/an;->ePf:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput v1, p0, Landroid/support/v4/app/an;->ePg:I

    iput-object v2, p0, Landroid/support/v4/app/an;->eOI:Landroid/os/Bundle;

    iput-object v2, p0, Landroid/support/v4/app/an;->ePi:Landroid/util/SparseArray;

    new-instance v0, Landroid/support/v4/app/Y;

    invoke-direct {v0, p0}, Landroid/support/v4/app/Y;-><init>(Landroid/support/v4/app/an;)V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOF:Ljava/lang/Runnable;

    return-void
.end method

.method private egA(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7

    const/4 v6, -0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    move v3, v2

    move v4, v0

    :goto_1
    if-ge v3, v4, :cond_5

    iget-object v0, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/l;

    if-eqz p1, :cond_1

    invoke-static {v0}, Landroid/support/v4/app/l;->ebn(Landroid/support/v4/app/l;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/support/v4/app/l;->ebj(Landroid/support/v4/app/l;)Landroid/support/v4/app/al;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-eq v1, v6, :cond_1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/l;->ebk()V

    move v0, v3

    move v1, v4

    :goto_2
    add-int/lit8 v3, v0, 0x1

    move v4, v1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/l;->ebh()Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p1, :cond_3

    invoke-static {v0}, Landroid/support/v4/app/l;->ebj(Landroid/support/v4/app/l;)Landroid/support/v4/app/al;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v1, p1, v2, v5}, Landroid/support/v4/app/al;->efS(Ljava/util/ArrayList;II)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_2
    iget-object v1, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v4, v4, -0x1

    if-eqz p1, :cond_4

    invoke-static {v0}, Landroid/support/v4/app/l;->ebn(Landroid/support/v4/app/l;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    invoke-static {v0}, Landroid/support/v4/app/l;->ebj(Landroid/support/v4/app/l;)Landroid/support/v4/app/al;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-eq v1, v6, :cond_4

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Landroid/support/v4/app/l;->ebk()V

    move v0, v3

    move v1, v4

    goto :goto_2

    :cond_3
    move v0, v3

    move v1, v4

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Landroid/support/v4/app/l;->ebl()V

    move v0, v3

    move v1, v4

    goto :goto_2

    :cond_5
    return-void

    :cond_6
    move v0, v3

    move v1, v4

    goto :goto_2
.end method

.method private egD(Ljava/lang/String;II)Z
    .locals 7

    const/4 v6, 0x1

    invoke-virtual {p0}, Landroid/support/v4/app/an;->ehn()Z

    invoke-direct {p0, v6}, Landroid/support/v4/app/an;->ehb(Z)V

    iget-object v0, p0, Landroid/support/v4/app/an;->ePc:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    if-gez p2, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->ePc:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecs()Landroid/support/v4/app/ar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/ar;->egB()Z

    move-result v0

    if-eqz v0, :cond_0

    return v6

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/an;->ePb:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/an;->eOX:Ljava/util/ArrayList;

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->ein(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v6, p0, Landroid/support/v4/app/an;->eOS:Z

    :try_start_0
    iget-object v1, p0, Landroid/support/v4/app/an;->ePb:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/an;->eOX:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v2}, Landroid/support/v4/app/an;->ehd(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Landroid/support/v4/app/an;->ehL()V

    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/an;->ehm()V

    invoke-direct {p0}, Landroid/support/v4/app/an;->egG()V

    return v0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Landroid/support/v4/app/an;->ehL()V

    throw v0
.end method

.method private egE(Landroid/support/v4/app/p;Landroid/support/v4/app/P;I)V
    .locals 7

    iget-object v4, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v3, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->startViewTransition(Landroid/view/View;)V

    invoke-virtual {p1, p3}, Landroid/support/v4/app/p;->ece(I)V

    iget-object v0, p2, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    iget-object v6, p2, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/p;->eci(Landroid/view/View;)V

    invoke-static {v6}, Landroid/support/v4/app/an;->ehf(Landroid/view/animation/Animation;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v2

    new-instance v0, Landroid/support/v4/app/A;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/A;-><init>(Landroid/support/v4/app/an;Landroid/view/animation/Animation$AnimationListener;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/app/p;)V

    invoke-virtual {v6, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-static {v4, p2}, Landroid/support/v4/app/an;->ehP(Landroid/view/View;Landroid/support/v4/app/P;)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p2, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    iget-object v1, p2, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    invoke-virtual {p1, v1}, Landroid/support/v4/app/p;->ecd(Landroid/animation/Animator;)V

    new-instance v1, Landroid/support/v4/app/j;

    invoke-direct {v1, p0, v3, v4, p1}, Landroid/support/v4/app/j;-><init>(Landroid/support/v4/app/an;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/app/p;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-static {v1, p2}, Landroid/support/v4/app/an;->ehP(Landroid/view/View;Landroid/support/v4/app/P;)V

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method private egG()V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    iget-object v2, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->delete(I)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private egH(Ljava/util/ArrayList;Ljava/util/ArrayList;IILandroid/support/v4/a/c;)I
    .locals 7

    const/4 v3, 0x0

    add-int/lit8 v0, p4, -0x1

    move v4, v0

    move v2, p4

    :goto_0
    if-lt v4, p3, :cond_4

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v0}, Landroid/support/v4/app/al;->egb()Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v4, 0x1

    invoke-virtual {v0, p1, v1, p4}, Landroid/support/v4/app/al;->efS(Ljava/util/ArrayList;II)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    :goto_1
    if-eqz v1, :cond_5

    iget-object v1, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    :cond_0
    new-instance v1, Landroid/support/v4/app/l;

    invoke-direct {v1, v0, v5}, Landroid/support/v4/app/l;-><init>(Landroid/support/v4/app/al;Z)V

    iget-object v6, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v1}, Landroid/support/v4/app/al;->efM(Landroid/support/v4/app/y;)V

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Landroid/support/v4/app/al;->efW()V

    :goto_2
    add-int/lit8 v1, v2, -0x1

    if-eq v4, v1, :cond_1

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_1
    invoke-direct {p0, p5}, Landroid/support/v4/app/an;->eis(Landroid/support/v4/a/c;)V

    move v0, v1

    :goto_3
    add-int/lit8 v1, v4, -0x1

    move v4, v1

    move v2, v0

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v3}, Landroid/support/v4/app/al;->ega(Z)V

    goto :goto_2

    :cond_4
    return v2

    :cond_5
    move v0, v2

    goto :goto_3
.end method

.method private egM(Landroid/support/v4/a/c;)V
    .locals 5

    invoke-virtual {p1}, Landroid/support/v4/a/c;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/support/v4/a/c;->dSy(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    iget-boolean v3, v0, Landroid/support/v4/app/p;->eKj:Z

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ech()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getAlpha()F

    move-result v4

    iput v4, v0, Landroid/support/v4/app/p;->eKX:F

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private egS(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/L;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/app/L;->edz(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    or-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->ecX()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/an;->eOF:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static egW(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V
    .locals 3

    const/4 v2, 0x1

    :goto_0
    if-ge p2, p3, :cond_2

    invoke-virtual {p0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/al;->efR(I)V

    add-int/lit8 v1, p3, -0x1

    if-ne p2, v1, :cond_0

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/support/v4/app/al;->ega(Z)V

    :goto_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v2}, Landroid/support/v4/app/al;->efR(I)V

    invoke-virtual {v0}, Landroid/support/v4/app/al;->efW()V

    goto :goto_2

    :cond_2
    return-void
.end method

.method static egY(Landroid/animation/Animator;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    return v1

    :cond_0
    instance-of v0, p0, Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    check-cast p0, Landroid/animation/ValueAnimator;

    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_4

    const-string/jumbo v3, "alpha"

    aget-object v4, v2, v0

    invoke-virtual {v4}, Landroid/animation/PropertyValuesHolder;->getPropertyName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return v5

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    instance-of v0, p0, Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_4

    check-cast p0, Landroid/animation/AnimatorSet;

    invoke-virtual {p0}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v3

    move v2, v1

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-static {v0}, Landroid/support/v4/app/an;->egY(Landroid/animation/Animator;)Z

    move-result v0

    if-eqz v0, :cond_3

    return v5

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    return v1
.end method

.method static synthetic ego(Landroid/support/v4/app/an;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/an;->ehM()V

    return-void
.end method

.method private egr()V
    .locals 9

    const/4 v3, 0x0

    const/4 v8, 0x0

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    move v6, v3

    :goto_0
    move v7, v3

    :goto_1
    if-ge v7, v6, :cond_4

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/p;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/support/v4/app/p;->ebu()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Landroid/support/v4/app/p;->ecC()I

    move-result v2

    invoke-virtual {v1}, Landroid/support/v4/app/p;->ebu()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/view/animation/Animation;->cancel()V

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    :cond_0
    invoke-virtual {v1, v8}, Landroid/support/v4/app/p;->eci(Landroid/view/View;)V

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    :cond_1
    :goto_2
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    move v6, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/support/v4/app/p;->ebs()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/support/v4/app/p;->ebs()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    goto :goto_2

    :cond_4
    return-void
.end method

.method public static egu(IZ)I
    .locals 1

    const/4 v0, -0x1

    sparse-switch p0, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_1
    if-eqz p1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    :sswitch_2
    if-eqz p1, :cond_2

    const/4 v0, 0x5

    goto :goto_0

    :cond_2
    const/4 v0, 0x6

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch
.end method

.method private ehC(Landroid/support/v4/app/p;)Landroid/support/v4/app/p;
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    :cond_0
    return-object v4

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    iget-object v3, v0, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    if-ne v3, v2, :cond_2

    iget-object v3, v0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v3, :cond_2

    return-object v0

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return-object v4
.end method

.method private ehK(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V
    .locals 9

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    iget-boolean v8, v0, Landroid/support/v4/app/al;->eOn:Z

    iget-object v0, p0, Landroid/support/v4/app/an;->eOK:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOK:Ljava/util/ArrayList;

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOK:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Landroid/support/v4/app/an;->ehO()Landroid/support/v4/app/p;

    move-result-object v1

    move v2, p3

    move-object v3, v1

    move v7, v5

    :goto_1
    if-ge v2, p4, :cond_3

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/an;->eOK:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/al;->efO(Ljava/util/ArrayList;Landroid/support/v4/app/p;)Landroid/support/v4/app/p;

    move-result-object v1

    :goto_2
    if-nez v7, :cond_2

    iget-boolean v0, v0, Landroid/support/v4/app/al;->eOx:Z

    :goto_3
    add-int/lit8 v2, v2, 0x1

    move-object v3, v1

    move v7, v0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/an;->eOK:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/al;->efP(Ljava/util/ArrayList;Landroid/support/v4/app/p;)Landroid/support/v4/app/p;

    move-result-object v1

    goto :goto_2

    :cond_2
    move v0, v6

    goto :goto_3

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/an;->eOK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-nez v8, :cond_4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Landroid/support/v4/app/af;->eeQ(Landroid/support/v4/app/an;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V

    :cond_4
    invoke-static {p1, p2, p3, p4}, Landroid/support/v4/app/an;->egW(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    if-eqz v8, :cond_9

    new-instance v5, Landroid/support/v4/a/c;

    invoke-direct {v5}, Landroid/support/v4/a/c;-><init>()V

    invoke-direct {p0, v5}, Landroid/support/v4/app/an;->eis(Landroid/support/v4/a/c;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/an;->egH(Ljava/util/ArrayList;Ljava/util/ArrayList;IILandroid/support/v4/a/c;)I

    move-result v4

    invoke-direct {p0, v5}, Landroid/support/v4/app/an;->egM(Landroid/support/v4/a/c;)V

    :goto_4
    if-eq v4, p3, :cond_5

    if-eqz v8, :cond_5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, v6

    invoke-static/range {v0 .. v5}, Landroid/support/v4/app/af;->eeQ(Landroid/support/v4/app/an;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    invoke-virtual {p0, v0, v6}, Landroid/support/v4/app/an;->egp(IZ)V

    :cond_5
    :goto_5
    if-ge p3, p4, :cond_7

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    iget v1, v0, Landroid/support/v4/app/al;->eOp:I

    if-ltz v1, :cond_6

    iget v1, v0, Landroid/support/v4/app/al;->eOp:I

    invoke-virtual {p0, v1}, Landroid/support/v4/app/an;->egI(I)V

    const/4 v1, -0x1

    iput v1, v0, Landroid/support/v4/app/al;->eOp:I

    :cond_6
    invoke-virtual {v0}, Landroid/support/v4/app/al;->efU()V

    add-int/lit8 p3, p3, 0x1

    goto :goto_5

    :cond_7
    if-eqz v7, :cond_8

    invoke-virtual {p0}, Landroid/support/v4/app/an;->egZ()V

    :cond_8
    return-void

    :cond_9
    move v4, p4

    goto :goto_4
.end method

.method private ehL()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOS:Z

    iget-object v0, p0, Landroid/support/v4/app/an;->eOX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/support/v4/app/an;->ePb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private ehM()V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    :goto_0
    iget-object v3, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v0, :cond_3

    :goto_1
    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->ecX()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/an;->eOF:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->ecX()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/an;->eOF:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static ehP(Landroid/view/View;Landroid/support/v4/app/P;)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Landroid/support/v4/app/an;->ehT(Landroid/view/View;Landroid/support/v4/app/P;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    if-eqz v0, :cond_3

    iget-object v0, p1, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    new-instance v1, Landroid/support/v4/app/aa;

    invoke-direct {v1, p0}, Landroid/support/v4/app/aa;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p1, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    invoke-static {v0}, Landroid/support/v4/app/an;->ehf(Landroid/view/animation/Animation;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v1, p1, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    new-instance v2, Landroid/support/v4/app/R;

    invoke-direct {v2, p0, v0}, Landroid/support/v4/app/R;-><init>(Landroid/view/View;Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method

.method static ehT(Landroid/view/View;Landroid/support/v4/app/P;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return v0

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getLayerType()I

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, Landroid/support/v4/view/z;->dPJ(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Landroid/support/v4/app/an;->ehy(Landroid/support/v4/app/P;)Z

    move-result v0

    :cond_2
    return v0
.end method

.method public static ehY(I)I
    .locals 1

    const/4 v0, 0x0

    sparse-switch p0, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    const/16 v0, 0x2002

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x1001

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1003

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch
.end method

.method private ehb(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eOS:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "FragmentManager is already executing transactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v1}, Landroid/support/v4/app/C;->ecX()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must be called from main thread of fragment host"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p1, :cond_2

    invoke-direct {p0}, Landroid/support/v4/app/an;->ehu()V

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/an;->ePb:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->ePb:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOX:Ljava/util/ArrayList;

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOS:Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v0, v1}, Landroid/support/v4/app/an;->egA(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v2, p0, Landroid/support/v4/app/an;->eOS:Z

    return-void

    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Landroid/support/v4/app/an;->eOS:Z

    throw v0
.end method

.method private ehd(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Internal error with the back stack records"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-direct {p0, p1, p2}, Landroid/support/v4/app/an;->egA(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v2, v3, :cond_6

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    iget-boolean v0, v0, Landroid/support/v4/app/al;->eOn:Z

    if-nez v0, :cond_9

    if-eq v1, v2, :cond_4

    invoke-direct {p0, p1, p2, v1, v2}, Landroid/support/v4/app/an;->ehK(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    :cond_4
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    :goto_1
    if-ge v1, v3, :cond_5

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    iget-boolean v0, v0, Landroid/support/v4/app/al;->eOn:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    move v0, v1

    :goto_2
    invoke-direct {p0, p1, p2, v2, v0}, Landroid/support/v4/app/an;->ehK(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    add-int/lit8 v1, v0, -0x1

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_3
    add-int/lit8 v2, v0, 0x1

    goto :goto_0

    :cond_6
    if-eq v1, v3, :cond_7

    invoke-direct {p0, p1, p2, v1, v3}, Landroid/support/v4/app/an;->ehK(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    :cond_7
    return-void

    :cond_8
    move v0, v1

    goto :goto_2

    :cond_9
    move v0, v2

    goto :goto_3
.end method

.method private static ehf(Landroid/view/animation/Animation;)Landroid/view/animation/Animation$AnimationListener;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    sget-object v0, Landroid/support/v4/app/an;->ePj:Ljava/lang/reflect/Field;

    if-nez v0, :cond_0

    const-class v0, Landroid/view/animation/Animation;

    const-string/jumbo v2, "mListener"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Landroid/support/v4/app/an;->ePj:Ljava/lang/reflect/Field;

    sget-object v0, Landroid/support/v4/app/an;->ePj:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    :cond_0
    sget-object v0, Landroid/support/v4/app/an;->ePj:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation$AnimationListener;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "FragmentManager"

    const-string/jumbo v3, "Cannot access Animation\'s mListener field"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v2, "FragmentManager"

    const-string/jumbo v3, "No field with the name mListener is found in Animation class"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0
.end method

.method static ehh(Landroid/content/Context;FFFF)Landroid/support/v4/app/P;
    .locals 10

    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    move v1, p1

    move v2, p2

    move v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    sget-object v1, Landroid/support/v4/app/an;->eOP:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p3, p4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    sget-object v1, Landroid/support/v4/app/an;->ePl:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/support/v4/app/P;

    const/4 v1, 0x0

    invoke-direct {v0, v9, v1}, Landroid/support/v4/app/P;-><init>(Landroid/view/animation/Animation;Landroid/support/v4/app/P;)V

    return-object v0
.end method

.method private ehj(I)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOS:Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/app/an;->egp(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v1, p0, Landroid/support/v4/app/an;->eOS:Z

    invoke-virtual {p0}, Landroid/support/v4/app/an;->ehn()Z

    return-void

    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Landroid/support/v4/app/an;->eOS:Z

    throw v0
.end method

.method private ehs(Landroid/support/v4/app/al;ZZZ)V
    .locals 7

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz p2, :cond_4

    invoke-virtual {p1, p4}, Landroid/support/v4/app/al;->ega(Z)V

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p3, :cond_0

    move-object v0, p0

    move v5, v4

    invoke-static/range {v0 .. v5}, Landroid/support/v4/app/af;->eeQ(Landroid/support/v4/app/an;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V

    :cond_0
    if-eqz p4, :cond_1

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    invoke-virtual {p0, v0, v4}, Landroid/support/v4/app/an;->egp(IZ)V

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v3

    :goto_1
    if-ge v1, v2, :cond_6

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_3

    iget-object v4, v0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-boolean v4, v0, Landroid/support/v4/app/p;->eKM:Z

    if-eqz v4, :cond_3

    iget v4, v0, Landroid/support/v4/app/p;->eKq:I

    invoke-virtual {p1, v4}, Landroid/support/v4/app/al;->efN(I)Z

    move-result v4

    if-eqz v4, :cond_3

    iget v4, v0, Landroid/support/v4/app/p;->eKX:F

    cmpl-float v4, v4, v6

    if-lez v4, :cond_2

    iget-object v4, v0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget v5, v0, Landroid/support/v4/app/p;->eKX:F

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    :cond_2
    if-eqz p4, :cond_5

    iput v6, v0, Landroid/support/v4/app/p;->eKX:F

    :cond_3
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Landroid/support/v4/app/al;->efW()V

    goto :goto_0

    :cond_5
    const/high16 v4, -0x40800000    # -1.0f

    iput v4, v0, Landroid/support/v4/app/p;->eKX:F

    iput-boolean v3, v0, Landroid/support/v4/app/p;->eKM:Z

    goto :goto_2

    :cond_6
    return-void
.end method

.method static synthetic eht(Landroid/support/v4/app/an;Landroid/support/v4/app/al;ZZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v4/app/an;->ehs(Landroid/support/v4/app/al;ZZZ)V

    return-void
.end method

.method private ehu()V
    .locals 3

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eOT:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Can not perform this action after onSaveInstanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOJ:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can not perform this action inside of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/app/an;->eOJ:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method static ehx(Landroid/content/Context;FF)Landroid/support/v4/app/P;
    .locals 4

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p1, p2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    sget-object v1, Landroid/support/v4/app/an;->ePl:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v1, Landroid/support/v4/app/P;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Landroid/support/v4/app/P;-><init>(Landroid/view/animation/Animation;Landroid/support/v4/app/P;)V

    return-object v1
.end method

.method static ehy(Landroid/support/v4/app/P;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    instance-of v0, v0, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_0

    return v4

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    instance-of v0, v0, Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    check-cast v0, Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;

    move-result-object v2

    move v0, v1

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Landroid/view/animation/AlphaAnimation;

    if-eqz v3, :cond_1

    return v4

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    invoke-static {v0}, Landroid/support/v4/app/an;->egY(Landroid/animation/Animator;)Z

    move-result v0

    return v0
.end method

.method private eie()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->ePk:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/l;

    invoke-virtual {v0}, Landroid/support/v4/app/l;->ebl()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static eil(Landroid/support/v4/app/E;)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/E;->edn()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v4/app/p;->eKo:Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/E;->edo()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/E;

    invoke-static {v0}, Landroid/support/v4/app/an;->eil(Landroid/support/v4/app/E;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private eir(Ljava/lang/RuntimeException;)V
    .locals 5

    const-string/jumbo v0, "FragmentManager"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "FragmentManager"

    const-string/jumbo v1, "Activity state:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/support/v4/a/q;

    const-string/jumbo v1, "FragmentManager"

    invoke-direct {v0, v1}, Landroid/support/v4/a/q;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    const-string/jumbo v2, "  "

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/support/v4/app/C;->eda(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    throw p1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "FragmentManager"

    const-string/jumbo v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    :try_start_1
    const-string/jumbo v0, "  "

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3, v1, v2}, Landroid/support/v4/app/an;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v1, "FragmentManager"

    const-string/jumbo v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private eis(Landroid/support/v4/a/c;)V
    .locals 8

    const/4 v5, 0x0

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/p;

    iget v0, v1, Landroid/support/v4/app/p;->eKI:I

    if-ge v0, v2, :cond_1

    invoke-virtual {v1}, Landroid/support/v4/app/p;->ebS()I

    move-result v3

    invoke-virtual {v1}, Landroid/support/v4/app/p;->ebJ()I

    move-result v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    iget-object v0, v1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKZ:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKM:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1}, Landroid/support/v4/a/c;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "Active Fragments in "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3, p2, p3, p4}, Landroid/support/v4/app/p;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "Added Fragments:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/p;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/an;->eOU:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "Fragments Created Menus:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOU:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/p;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "Back Stack:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_4

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/al;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {v0, v3, p2, p3, p4}, Landroid/support/v4/app/al;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "Back Stack Indices:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    :goto_4
    if-ge v2, v3, :cond_5

    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mAvailBackStackIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_6
    monitor-exit p0

    iget-object v0, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_7

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "Pending Actions:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_5
    if-ge v1, v2, :cond_7

    iget-object v0, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/L;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "FragmentManager misc state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "  mHost="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "  mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_8

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "  mParent="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_8
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "  mCurState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v0, " mStateSaved="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eOT:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eON:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eOL:Z

    if-eqz v0, :cond_9

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eOL:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    :cond_9
    iget-object v0, p0, Landroid/support/v4/app/an;->eOJ:Ljava/lang/String;

    if-eqz v0, :cond_a

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "  mNoTransactionsBecause="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOJ:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_a
    return-void
.end method

.method public egB()Z
    .locals 3

    invoke-direct {p0}, Landroid/support/v4/app/an;->ehu()V

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/app/an;->egD(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public egC(Landroid/support/v4/app/al;)I
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_3

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget-boolean v1, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v1, :cond_2

    const-string/jumbo v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Setting back stack index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :cond_3
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-boolean v1, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v1, :cond_4

    const-string/jumbo v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Adding back stack index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method egF(Landroid/support/v4/app/p;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/an;->egF(Landroid/support/v4/app/p;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p2, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/as;->eiG(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public egI(I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    :cond_0
    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Freeing back stack index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method egJ()V
    .locals 9

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-eqz v0, :cond_6

    move v1, v2

    move-object v3, v4

    move-object v5, v4

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_5

    iget-boolean v6, v0, Landroid/support/v4/app/p;->eKP:Z

    if-eqz v6, :cond_1

    if-nez v5, :cond_0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, v0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    if-eqz v6, :cond_2

    iget-object v6, v0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    iget v6, v6, Landroid/support/v4/app/p;->eKp:I

    :goto_1
    iput v6, v0, Landroid/support/v4/app/p;->eKK:I

    sget-boolean v6, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v6, :cond_1

    const-string/jumbo v6, "FragmentManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "retainNonConfig: keeping retained "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v6, v0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v6, :cond_3

    iget-object v6, v0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v6}, Landroid/support/v4/app/an;->egJ()V

    iget-object v0, v0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    iget-object v0, v0, Landroid/support/v4/app/an;->eOO:Landroid/support/v4/app/E;

    move-object v6, v0

    :goto_2
    if-nez v3, :cond_4

    if-eqz v6, :cond_4

    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v2

    :goto_3
    if-ge v0, v1, :cond_4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    const/4 v6, -0x1

    goto :goto_1

    :cond_3
    iget-object v0, v0, Landroid/support/v4/app/p;->eKB:Landroid/support/v4/app/E;

    move-object v6, v0

    goto :goto_2

    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_6
    move-object v3, v4

    move-object v5, v4

    :cond_7
    if-nez v5, :cond_8

    if-nez v3, :cond_8

    iput-object v4, p0, Landroid/support/v4/app/an;->eOO:Landroid/support/v4/app/E;

    :goto_4
    return-void

    :cond_8
    new-instance v0, Landroid/support/v4/app/E;

    invoke-direct {v0, v5, v3}, Landroid/support/v4/app/E;-><init>(Ljava/util/List;Ljava/util/List;)V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOO:Landroid/support/v4/app/E;

    goto :goto_4
.end method

.method egK(Landroid/support/v4/app/p;IIIZ)V
    .locals 10

    const/4 v9, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKj:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKR:Z

    if-eqz v0, :cond_1

    :cond_0
    if-le p2, v5, :cond_1

    move p2, v5

    :cond_1
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKN:Z

    if-eqz v0, :cond_2

    iget v0, p1, Landroid/support/v4/app/p;->eKI:I

    if-le p2, v0, :cond_2

    iget v0, p1, Landroid/support/v4/app/p;->eKI:I

    if-nez v0, :cond_4

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebW()Z

    move-result v0

    if-eqz v0, :cond_4

    move p2, v5

    :cond_2
    :goto_0
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKk:Z

    if-eqz v0, :cond_3

    iget v0, p1, Landroid/support/v4/app/p;->eKI:I

    if-ge v0, v9, :cond_3

    if-le p2, v6, :cond_3

    move p2, v6

    :cond_3
    iget v0, p1, Landroid/support/v4/app/p;->eKI:I

    if-gt v0, p2, :cond_23

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKA:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKw:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    return-void

    :cond_4
    iget p2, p1, Landroid/support/v4/app/p;->eKI:I

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebu()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebs()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_7

    :cond_6
    invoke-virtual {p1, v7}, Landroid/support/v4/app/p;->eci(Landroid/view/View;)V

    invoke-virtual {p1, v7}, Landroid/support/v4/app/p;->ecd(Landroid/animation/Animator;)V

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecC()I

    move-result v2

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    :cond_7
    iget v0, p1, Landroid/support/v4/app/p;->eKI:I

    packed-switch v0, :pswitch_data_0

    :cond_8
    :goto_1
    iget v0, p1, Landroid/support/v4/app/p;->eKI:I

    if-eq v0, p2, :cond_9

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "moveToState: Fragment state for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " not updated inline; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "expected state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/support/v4/app/p;->eKI:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput p2, p1, Landroid/support/v4/app/p;->eKI:I

    :cond_9
    return-void

    :pswitch_0
    if-lez p2, :cond_11

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_a

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "moveto CREATED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    if-eqz v0, :cond_c

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    iget-object v1, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v1}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    const-string/jumbo v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    const-string/jumbo v1, "android:target_state"

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/an;->egz(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/p;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    iget-object v0, p1, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    if-eqz v0, :cond_b

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    const-string/jumbo v1, "android:target_req_state"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroid/support/v4/app/p;->eKQ:I

    :cond_b
    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    const-string/jumbo v1, "android:user_visible_hint"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p1, Landroid/support/v4/app/p;->eKx:Z

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKx:Z

    if-nez v0, :cond_c

    iput-boolean v5, p1, Landroid/support/v4/app/p;->eKk:Z

    if-le p2, v6, :cond_c

    move p2, v6

    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    iput-object v0, p1, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    iput-object v0, p1, Landroid/support/v4/app/p;->eKU:Landroid/support/v4/app/p;

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    iget-object v0, v0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    :goto_2
    iput-object v0, p1, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    iget-object v0, p1, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    if-eqz v0, :cond_f

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    iget v1, v1, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p1, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    if-eq v0, v1, :cond_e

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " declared target fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " that does not belong to this FragmentManager!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->ede()Landroid/support/v4/app/an;

    move-result-object v0

    goto :goto_2

    :cond_e
    iget-object v0, p1, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    iget v0, v0, Landroid/support/v4/app/p;->eKI:I

    if-ge v0, v5, :cond_f

    iget-object v1, p1, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    move-object v0, p0

    move v2, v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    :cond_f
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v3}, Landroid/support/v4/app/an;->egN(Landroid/support/v4/app/p;Landroid/content/Context;Z)V

    iput-boolean v3, p1, Landroid/support/v4/app/p;->eKO:Z

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/app/p;->onAttach(Landroid/content/Context;)V

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_10

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    iget-object v0, p1, Landroid/support/v4/app/p;->eKU:Landroid/support/v4/app/p;

    if-nez v0, :cond_1e

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/C;->edj(Landroid/support/v4/app/p;)V

    :goto_3
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v3}, Landroid/support/v4/app/an;->egw(Landroid/support/v4/app/p;Landroid/content/Context;Z)V

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKl:Z

    if-nez v0, :cond_1f

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p0, p1, v0, v3}, Landroid/support/v4/app/an;->eib(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/p;->ecp(Landroid/os/Bundle;)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p0, p1, v0, v3}, Landroid/support/v4/app/an;->eip(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V

    :goto_4
    iput-boolean v3, p1, Landroid/support/v4/app/p;->eKo:Z

    :cond_11
    :pswitch_1
    invoke-virtual {p0, p1}, Landroid/support/v4/app/an;->egy(Landroid/support/v4/app/p;)V

    if-le p2, v5, :cond_19

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_12

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "moveto ACTIVITY_CREATED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKA:Z

    if-nez v0, :cond_17

    iget v0, p1, Landroid/support/v4/app/p;->eKq:I

    if-eqz v0, :cond_37

    iget v0, p1, Landroid/support/v4/app/p;->eKq:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_13

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot create fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " for a container view with no id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->eir(Ljava/lang/RuntimeException;)V

    :cond_13
    iget-object v0, p0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    iget v1, p1, Landroid/support/v4/app/p;->eKq:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/k;->ebe(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_14

    iget-boolean v1, p1, Landroid/support/v4/app/p;->eKr:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_14

    :try_start_0
    invoke-virtual {p1}, Landroid/support/v4/app/p;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, Landroid/support/v4/app/p;->eKq:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_5
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "No view found for id 0x"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v8, p1, Landroid/support/v4/app/p;->eKq:I

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v8, " ("

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ") for fragment "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Landroid/support/v4/app/an;->eir(Ljava/lang/RuntimeException;)V

    :cond_14
    :goto_6
    iput-object v0, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v1}, Landroid/support/v4/app/p;->ebM(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v0, v2}, Landroid/support/v4/app/p;->ebO(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v1, :cond_22

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iput-object v1, p1, Landroid/support/v4/app/p;->eKJ:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    if-eqz v0, :cond_15

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_15
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKZ:Z

    if-eqz v0, :cond_16

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_16
    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/p;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p0, p1, v0, v1, v3}, Landroid/support/v4/app/an;->eif(Landroid/support/v4/app/p;Landroid/view/View;Landroid/os/Bundle;Z)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_21

    iget-object v0, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    if-eqz v0, :cond_20

    :goto_7
    iput-boolean v5, p1, Landroid/support/v4/app/p;->eKM:Z

    :cond_17
    :goto_8
    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/p;->ebv(Landroid/os/Bundle;)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p0, p1, v0, v3}, Landroid/support/v4/app/an;->eik(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_18

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/p;->ecA(Landroid/os/Bundle;)V

    :cond_18
    iput-object v7, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    :cond_19
    :pswitch_2
    const/4 v0, 0x2

    if-le p2, v0, :cond_1a

    iput v6, p1, Landroid/support/v4/app/p;->eKI:I

    :cond_1a
    :pswitch_3
    if-le p2, v6, :cond_1c

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_1b

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "moveto STARTED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1b
    invoke-virtual {p1}, Landroid/support/v4/app/p;->eck()V

    invoke-virtual {p0, p1, v3}, Landroid/support/v4/app/an;->eim(Landroid/support/v4/app/p;Z)V

    :cond_1c
    :pswitch_4
    if-le p2, v9, :cond_8

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_1d

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "moveto RESUMED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1d
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebK()V

    invoke-virtual {p0, p1, v3}, Landroid/support/v4/app/an;->eiq(Landroid/support/v4/app/p;Z)V

    iput-object v7, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    iput-object v7, p1, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    goto/16 :goto_1

    :cond_1e
    iget-object v0, p1, Landroid/support/v4/app/p;->eKU:Landroid/support/v4/app/p;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ebB(Landroid/support/v4/app/p;)V

    goto/16 :goto_3

    :cond_1f
    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/p;->ecl(Landroid/os/Bundle;)V

    iput v5, p1, Landroid/support/v4/app/p;->eKI:I

    goto/16 :goto_4

    :catch_0
    move-exception v1

    const-string/jumbo v1, "unknown"

    goto/16 :goto_5

    :cond_20
    move v5, v3

    goto/16 :goto_7

    :cond_21
    move v5, v3

    goto/16 :goto_7

    :cond_22
    iput-object v7, p1, Landroid/support/v4/app/p;->eKJ:Landroid/view/View;

    goto/16 :goto_8

    :cond_23
    iget v0, p1, Landroid/support/v4/app/p;->eKI:I

    if-le v0, p2, :cond_8

    iget v0, p1, Landroid/support/v4/app/p;->eKI:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    :cond_24
    :goto_9
    :pswitch_5
    if-ge p2, v5, :cond_8

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eON:Z

    if-eqz v0, :cond_25

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebu()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_32

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebu()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v7}, Landroid/support/v4/app/p;->eci(Landroid/view/View;)V

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    :cond_25
    :goto_a
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebu()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_26

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebs()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_33

    :cond_26
    invoke-virtual {p1, p2}, Landroid/support/v4/app/p;->ece(I)V

    move p2, v5

    goto/16 :goto_1

    :pswitch_6
    const/4 v0, 0x5

    if-ge p2, v0, :cond_28

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_27

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "movefrom RESUMED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_27
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebz()V

    invoke-virtual {p0, p1, v3}, Landroid/support/v4/app/an;->egL(Landroid/support/v4/app/p;Z)V

    :cond_28
    :pswitch_7
    if-ge p2, v9, :cond_2a

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_29

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "movefrom STARTED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_29
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecj()V

    invoke-virtual {p0, p1, v3}, Landroid/support/v4/app/an;->eih(Landroid/support/v4/app/p;Z)V

    :cond_2a
    :pswitch_8
    if-ge p2, v6, :cond_2c

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_2b

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "movefrom STOPPED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2b
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecz()V

    :cond_2c
    :pswitch_9
    const/4 v0, 0x2

    if-ge p2, v0, :cond_24

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_2d

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "movefrom ACTIVITY_CREATED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2d
    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_2e

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/C;->edi(Landroid/support/v4/app/p;)Z

    move-result v0

    if-eqz v0, :cond_2e

    iget-object v0, p1, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    if-nez v0, :cond_2e

    invoke-virtual {p0, p1}, Landroid/support/v4/app/an;->ehD(Landroid/support/v4/app/p;)V

    :cond_2e
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecN()V

    invoke-virtual {p0, p1, v3}, Landroid/support/v4/app/an;->eho(Landroid/support/v4/app/p;Z)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_30

    iget-object v0, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    if-eqz v0, :cond_30

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->endViewTransition(Landroid/view/View;)V

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    if-lez v0, :cond_31

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eON:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_31

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_31

    iget v0, p1, Landroid/support/v4/app/p;->eKX:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_31

    invoke-virtual {p0, p1, p3, v3, p4}, Landroid/support/v4/app/an;->ehl(Landroid/support/v4/app/p;IZI)Landroid/support/v4/app/P;

    move-result-object v0

    :goto_b
    const/4 v1, 0x0

    iput v1, p1, Landroid/support/v4/app/p;->eKX:F

    if-eqz v0, :cond_2f

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v4/app/an;->egE(Landroid/support/v4/app/p;Landroid/support/v4/app/P;I)V

    :cond_2f
    iget-object v0, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_30
    iput-object v7, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    iput-object v7, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iput-object v7, p1, Landroid/support/v4/app/p;->eKJ:Landroid/view/View;

    iput-boolean v3, p1, Landroid/support/v4/app/p;->eKw:Z

    goto/16 :goto_9

    :cond_31
    move-object v0, v7

    goto :goto_b

    :cond_32
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebs()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_25

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebs()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {p1, v7}, Landroid/support/v4/app/p;->ecd(Landroid/animation/Animator;)V

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    goto/16 :goto_a

    :cond_33
    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_34

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "movefrom CREATED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_34
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKo:Z

    if-nez v0, :cond_35

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebY()V

    invoke-virtual {p0, p1, v3}, Landroid/support/v4/app/an;->egF(Landroid/support/v4/app/p;Z)V

    :goto_c
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebT()V

    invoke-virtual {p0, p1, v3}, Landroid/support/v4/app/an;->ehB(Landroid/support/v4/app/p;Z)V

    if-nez p5, :cond_8

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKo:Z

    if-nez v0, :cond_36

    invoke-virtual {p0, p1}, Landroid/support/v4/app/an;->egX(Landroid/support/v4/app/p;)V

    goto/16 :goto_1

    :cond_35
    iput v3, p1, Landroid/support/v4/app/p;->eKI:I

    goto :goto_c

    :cond_36
    iput-object v7, p1, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    iput-object v7, p1, Landroid/support/v4/app/p;->eKU:Landroid/support/v4/app/p;

    iput-object v7, p1, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    goto/16 :goto_1

    :cond_37
    move-object v0, v7

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method egL(Landroid/support/v4/app/p;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/an;->egL(Landroid/support/v4/app/p;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p2, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/as;->eiL(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method egN(Landroid/support/v4/app/p;Landroid/content/Context;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v4/app/an;->egN(Landroid/support/v4/app/p;Landroid/content/Context;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p3, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/app/as;->eiN(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public egO(Landroid/support/v4/app/p;)V
    .locals 3

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "show: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKZ:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/support/v4/app/p;->eKZ:Z

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKC:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p1, Landroid/support/v4/app/p;->eKC:Z

    :cond_1
    return-void
.end method

.method egP()Landroid/view/LayoutInflater$Factory2;
    .locals 0

    return-object p0
.end method

.method egQ()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/an;->ehF(Landroid/support/v4/app/p;)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method public egR(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    if-ge v0, v3, :cond_0

    return v2

    :cond_0
    move v1, v2

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ecF(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v3

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return v2
.end method

.method egT(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v4/app/an;->egT(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p3, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/app/as;->eiR(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public egU()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOT:Z

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->ehj(I)V

    return-void
.end method

.method public egV()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->ehj(I)V

    return-void
.end method

.method egX(Landroid/support/v4/app/p;)V
    .locals 3

    iget v0, p1, Landroid/support/v4/app/p;->eKp:I

    if-gez v0, :cond_0

    return-void

    :cond_0
    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Freeing fragment index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    iget v1, p1, Landroid/support/v4/app/p;->eKp:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/C;->edl(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecE()V

    return-void
.end method

.method egZ()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOW:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOW:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOW:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ae;

    invoke-interface {v0}, Landroid/support/v4/app/ae;->onBackStackChanged()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method egn(Landroid/support/v4/app/al;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method egp(IZ)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    if-ne p1, v0, :cond_1

    return-void

    :cond_1
    iput p1, p0, Landroid/support/v4/app/an;->ePg:I

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    move v1, v3

    :goto_0
    if-ge v2, v4, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    invoke-virtual {p0, v0}, Landroid/support/v4/app/an;->egx(Landroid/support/v4/app/p;)V

    iget-object v5, v0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    if-eqz v5, :cond_9

    iget-object v0, v0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->eiE()Z

    move-result v0

    or-int/2addr v0, v1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v4

    move v2, v3

    :goto_2
    if-ge v2, v4, :cond_5

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_4

    iget-boolean v5, v0, Landroid/support/v4/app/p;->eKN:Z

    if-nez v5, :cond_3

    iget-boolean v5, v0, Landroid/support/v4/app/p;->eKR:Z

    if-eqz v5, :cond_4

    :cond_3
    iget-boolean v5, v0, Landroid/support/v4/app/p;->eKM:Z

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_8

    invoke-virtual {p0, v0}, Landroid/support/v4/app/an;->egx(Landroid/support/v4/app/p;)V

    iget-object v5, v0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    if-eqz v5, :cond_8

    iget-object v0, v0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->eiE()Z

    move-result v0

    or-int/2addr v0, v1

    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    if-nez v1, :cond_6

    invoke-virtual {p0}, Landroid/support/v4/app/an;->egQ()V

    :cond_6
    iget-boolean v0, p0, Landroid/support/v4/app/an;->eOL:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    if-eqz v0, :cond_7

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->edd()V

    iput-boolean v3, p0, Landroid/support/v4/app/an;->eOL:Z

    :cond_7
    return-void

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_1
.end method

.method public egq()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOT:Z

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->ehj(I)V

    return-void
.end method

.method public egs(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 7

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v4/app/an;->ePg:I

    if-ge v1, v5, :cond_0

    return v2

    :cond_0
    move v1, v2

    move-object v3, v0

    move v4, v2

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/p;->ecx(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v5

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/an;->eOU:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    :goto_1
    iget-object v0, p0, Landroid/support/v4/app/an;->eOU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, Landroid/support/v4/app/an;->eOU:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v3, :cond_4

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_5

    :cond_4
    invoke-virtual {v0}, Landroid/support/v4/app/p;->onDestroyOptionsMenu()V

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    iput-object v3, p0, Landroid/support/v4/app/an;->eOU:Ljava/util/ArrayList;

    return v4
.end method

.method public egt(Landroid/support/v4/app/p;)V
    .locals 4

    const/4 v3, 0x1

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "attach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKR:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/support/v4/app/p;->eKR:Z

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKj:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment already added: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "add from attach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    iput-boolean v3, p1, Landroid/support/v4/app/p;->eKj:Z

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKt:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKY:Z

    if-eqz v0, :cond_3

    iput-boolean v3, p0, Landroid/support/v4/app/an;->eOL:Z

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method egv()Landroid/os/Parcelable;
    .locals 11

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/an;->eie()V

    invoke-direct {p0}, Landroid/support/v4/app/an;->egr()V

    invoke-virtual {p0}, Landroid/support/v4/app/an;->ehn()Z

    iput-boolean v1, p0, Landroid/support/v4/app/an;->eOT:Z

    iput-object v3, p0, Landroid/support/v4/app/an;->eOO:Landroid/support/v4/app/E;

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    return-object v3

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v6

    new-array v7, v6, [Landroid/support/v4/app/FragmentState;

    move v5, v4

    move v2, v4

    :goto_0
    if-ge v5, v6, :cond_7

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_12

    iget v2, v0, Landroid/support/v4/app/p;->eKp:I

    if-gez v2, :cond_2

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Failure saving state: active "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " has cleared index: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Landroid/support/v4/app/an;->eir(Ljava/lang/RuntimeException;)V

    :cond_2
    new-instance v2, Landroid/support/v4/app/FragmentState;

    invoke-direct {v2, v0}, Landroid/support/v4/app/FragmentState;-><init>(Landroid/support/v4/app/p;)V

    aput-object v2, v7, v5

    iget v8, v0, Landroid/support/v4/app/p;->eKI:I

    if-lez v8, :cond_6

    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    if-nez v8, :cond_6

    invoke-virtual {p0, v0}, Landroid/support/v4/app/an;->eic(Landroid/support/v4/app/p;)Landroid/os/Bundle;

    move-result-object v8

    iput-object v8, v2, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    iget-object v8, v0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    if-eqz v8, :cond_5

    iget-object v8, v0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    iget v8, v8, Landroid/support/v4/app/p;->eKp:I

    if-gez v8, :cond_3

    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Failure saving state: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " has target not in fragment manager: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v8}, Landroid/support/v4/app/an;->eir(Ljava/lang/RuntimeException;)V

    :cond_3
    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    if-nez v8, :cond_4

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    iput-object v8, v2, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    :cond_4
    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    const-string/jumbo v9, "android:target_state"

    iget-object v10, v0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    invoke-virtual {p0, v8, v9, v10}, Landroid/support/v4/app/an;->eit(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/p;)V

    iget v8, v0, Landroid/support/v4/app/p;->eKQ:I

    if-eqz v8, :cond_5

    iget-object v8, v2, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    const-string/jumbo v9, "android:target_req_state"

    iget v10, v0, Landroid/support/v4/app/p;->eKQ:I

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_5
    :goto_1
    sget-boolean v8, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v8, :cond_11

    const-string/jumbo v8, "FragmentManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Saved state of "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v9, ": "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v2, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto/16 :goto_0

    :cond_6
    iget-object v8, v0, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    iput-object v8, v2, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    goto :goto_1

    :cond_7
    if-nez v2, :cond_9

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_8

    const-string/jumbo v0, "FragmentManager"

    const-string/jumbo v1, "saveAllState: no fragments!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    return-object v3

    :cond_9
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_c

    new-array v1, v5, [I

    move v2, v4

    :goto_3
    if-ge v2, v5, :cond_d

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    iget v0, v0, Landroid/support/v4/app/p;->eKp:I

    aput v0, v1, v2

    aget v0, v1, v2

    if-gez v0, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Failure saving state: active "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v8, " has cleared index: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v8, v1, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->eir(Ljava/lang/RuntimeException;)V

    :cond_a
    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_b

    const-string/jumbo v0, "FragmentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "saveAllState: adding fragment #"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v8, ": "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_c
    move-object v1, v3

    :cond_d
    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_f

    new-array v3, v5, [Landroid/support/v4/app/BackStackState;

    move v2, v4

    :goto_4
    if-ge v2, v5, :cond_f

    new-instance v4, Landroid/support/v4/app/BackStackState;

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-direct {v4, v0}, Landroid/support/v4/app/BackStackState;-><init>(Landroid/support/v4/app/al;)V

    aput-object v4, v3, v2

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_e

    const-string/jumbo v0, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "saveAllState: adding back stack #"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_f
    new-instance v0, Landroid/support/v4/app/FragmentManagerState;

    invoke-direct {v0}, Landroid/support/v4/app/FragmentManagerState;-><init>()V

    iput-object v7, v0, Landroid/support/v4/app/FragmentManagerState;->eLF:[Landroid/support/v4/app/FragmentState;

    iput-object v1, v0, Landroid/support/v4/app/FragmentManagerState;->eLC:[I

    iput-object v3, v0, Landroid/support/v4/app/FragmentManagerState;->eLD:[Landroid/support/v4/app/BackStackState;

    iget-object v1, p0, Landroid/support/v4/app/an;->ePc:Landroid/support/v4/app/p;

    if-eqz v1, :cond_10

    iget-object v1, p0, Landroid/support/v4/app/an;->ePc:Landroid/support/v4/app/p;

    iget v1, v1, Landroid/support/v4/app/p;->eKp:I

    iput v1, v0, Landroid/support/v4/app/FragmentManagerState;->eLE:I

    :cond_10
    iget v1, p0, Landroid/support/v4/app/an;->ePf:I

    iput v1, v0, Landroid/support/v4/app/FragmentManagerState;->eLG:I

    invoke-virtual {p0}, Landroid/support/v4/app/an;->egJ()V

    return-object v0

    :cond_11
    move v0, v1

    goto/16 :goto_2

    :cond_12
    move v0, v2

    goto/16 :goto_2
.end method

.method egw(Landroid/support/v4/app/p;Landroid/content/Context;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v4/app/an;->egw(Landroid/support/v4/app/p;Landroid/content/Context;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p3, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/app/as;->eiI(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method egx(Landroid/support/v4/app/p;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget v2, p0, Landroid/support/v4/app/an;->ePg:I

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKN:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebW()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v2, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebJ()I

    move-result v3

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebN()I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Landroid/support/v4/app/an;->ehC(Landroid/support/v4/app/p;)Landroid/support/v4/app/p;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget-object v2, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    if-ge v2, v0, :cond_2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    iget-object v2, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_2
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKM:Z

    if-eqz v0, :cond_4

    iget-object v0, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    iget v0, p1, Landroid/support/v4/app/p;->eKX:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_3

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget v1, p1, Landroid/support/v4/app/p;->eKX:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_3
    iput v6, p1, Landroid/support/v4/app/p;->eKX:F

    iput-boolean v5, p1, Landroid/support/v4/app/p;->eKM:Z

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebJ()I

    move-result v0

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebN()I

    move-result v1

    invoke-virtual {p0, p1, v0, v7, v1}, Landroid/support/v4/app/an;->ehl(Landroid/support/v4/app/p;IZI)Landroid/support/v4/app/P;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-static {v1, v0}, Landroid/support/v4/app/an;->ehP(Landroid/view/View;Landroid/support/v4/app/P;)V

    iget-object v1, v0, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    if-eqz v1, :cond_7

    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v0, v0, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_4
    :goto_1
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKC:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0, p1}, Landroid/support/v4/app/an;->ehq(Landroid/support/v4/app/p;)V

    :cond_5
    return-void

    :cond_6
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_0

    :cond_7
    iget-object v1, v0, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    iget-object v2, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    iget-object v0, v0, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_1
.end method

.method egy(Landroid/support/v4/app/p;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKA:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKD:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/p;->ebM(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v2, v1}, Landroid/support/v4/app/p;->ebO(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iput-object v0, p1, Landroid/support/v4/app/p;->eKJ:Landroid/view/View;

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKZ:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/p;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v1, p1, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p0, p1, v0, v1, v3}, Landroid/support/v4/app/an;->eif(Landroid/support/v4/app/p;Landroid/view/View;Landroid/os/Bundle;Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-object v2, p1, Landroid/support/v4/app/p;->eKJ:Landroid/view/View;

    goto :goto_0
.end method

.method public egz(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/p;
    .locals 5

    const/4 v2, 0x0

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    return-object v2

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-nez v0, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Fragment no longer exists for key "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Landroid/support/v4/app/an;->eir(Ljava/lang/RuntimeException;)V

    :cond_1
    return-object v0
.end method

.method public ehA(I)Landroid/support/v4/app/p;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget v2, v0, Landroid/support/v4/app/p;->eKv:I

    if-ne v2, p1, :cond_0

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_2

    iget v2, v0, Landroid/support/v4/app/p;->eKv:I

    if-ne v2, p1, :cond_2

    return-object v0

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_3
    return-object v3
.end method

.method ehB(Landroid/support/v4/app/p;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/an;->ehB(Landroid/support/v4/app/p;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p2, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/as;->eiH(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method ehD(Landroid/support/v4/app/p;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p1, Landroid/support/v4/app/p;->eKJ:Landroid/view/View;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->ePi:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->ePi:Landroid/util/SparseArray;

    :goto_0
    iget-object v0, p1, Landroid/support/v4/app/p;->eKJ:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/app/an;->ePi:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->ePi:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->ePi:Landroid/util/SparseArray;

    iput-object v0, p1, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    iput-object v2, p0, Landroid/support/v4/app/an;->ePi:Landroid/util/SparseArray;

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/an;->ePi:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_0
.end method

.method public ehE(Landroid/support/v4/app/p;Z)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "add: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v4/app/an;->ehH(Landroid/support/v4/app/p;)V

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKR:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment already added: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    iput-boolean v4, p1, Landroid/support/v4/app/p;->eKj:Z

    iput-boolean v3, p1, Landroid/support/v4/app/p;->eKN:Z

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-nez v0, :cond_2

    iput-boolean v3, p1, Landroid/support/v4/app/p;->eKC:Z

    :cond_2
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKt:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKY:Z

    if-eqz v0, :cond_3

    iput-boolean v4, p0, Landroid/support/v4/app/an;->eOL:Z

    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual {p0, p1}, Landroid/support/v4/app/an;->ehI(Landroid/support/v4/app/p;)V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public ehF(Landroid/support/v4/app/p;)V
    .locals 6

    const/4 v3, 0x0

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKk:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eOS:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/an;->ePm:Z

    return-void

    :cond_0
    iput-boolean v3, p1, Landroid/support/v4/app/p;->eKk:Z

    iget v2, p0, Landroid/support/v4/app/an;->ePg:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    :cond_1
    return-void
.end method

.method public ehG(Landroid/support/v4/app/C;Landroid/support/v4/app/k;Landroid/support/v4/app/p;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    iput-object p2, p0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    iput-object p3, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    return-void
.end method

.method ehH(Landroid/support/v4/app/p;)V
    .locals 3

    iget v0, p1, Landroid/support/v4/app/p;->eKp:I

    if-ltz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Landroid/support/v4/app/an;->ePf:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Landroid/support/v4/app/an;->ePf:I

    iget-object v1, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/p;->ecv(ILandroid/support/v4/app/p;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    iget v1, p1, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Allocated fragment index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method ehI(Landroid/support/v4/app/p;)V
    .locals 6

    const/4 v3, 0x0

    iget v2, p0, Landroid/support/v4/app/an;->ePg:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    return-void
.end method

.method public ehJ()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOT:Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->ehj(I)V

    return-void
.end method

.method public ehN(Landroid/content/res/Configuration;)V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ebV(Landroid/content/res/Configuration;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public ehO()Landroid/support/v4/app/p;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/an;->ePc:Landroid/support/v4/app/p;

    return-object v0
.end method

.method public ehQ(ILandroid/support/v4/app/al;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Setting back stack index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :goto_1
    if-ge v0, p1, :cond_5

    :try_start_1
    iget-object v1, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    :cond_3
    sget-boolean v1, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v1, :cond_4

    const-string/jumbo v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Adding available back stack index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, p0, Landroid/support/v4/app/an;->eOE:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_6

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Adding back stack index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v0, p0, Landroid/support/v4/app/an;->eOV:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ehR(Z)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ecm(Z)V

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public ehS()Z
    .locals 1

    invoke-virtual {p0}, Landroid/support/v4/app/an;->ehn()Z

    move-result v0

    invoke-direct {p0}, Landroid/support/v4/app/an;->eie()V

    return v0
.end method

.method public ehU(Ljava/lang/String;)Landroid/support/v4/app/p;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ecu(Ljava/lang/String;)Landroid/support/v4/app/p;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public ehV(Landroid/support/v4/app/p;)V
    .locals 3

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "hide: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKZ:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/support/v4/app/p;->eKZ:Z

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKC:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p1, Landroid/support/v4/app/p;->eKC:Z

    :cond_1
    return-void
.end method

.method public ehW()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    return-object v0

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public ehX(Landroid/view/Menu;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v4/app/an;->ePg:I

    if-ge v1, v3, :cond_0

    return v0

    :cond_0
    move v1, v0

    move v2, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ebP(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v3

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return v2
.end method

.method public ehZ(Landroid/support/v4/app/p;)V
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    iget v1, p1, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p1, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    if-eq v0, p0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not an active fragment of FragmentManager "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Landroid/support/v4/app/an;->ePc:Landroid/support/v4/app/p;

    return-void
.end method

.method public eha()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eON:Z

    invoke-virtual {p0}, Landroid/support/v4/app/an;->ehn()Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->ehj(I)V

    iput-object v1, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    iput-object v1, p0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    iput-object v1, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    return-void
.end method

.method public ehc(Landroid/support/v4/app/L;Z)V
    .locals 2

    if-nez p2, :cond_0

    invoke-direct {p0}, Landroid/support/v4/app/an;->ehu()V

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Landroid/support/v4/app/an;->eON:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_3

    :cond_1
    if-eqz p2, :cond_2

    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Activity has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    iget-object v0, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/an;->ePh:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Landroid/support/v4/app/an;->ehM()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public ehe()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->ehj(I)V

    return-void
.end method

.method ehg()Landroid/support/v4/app/E;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOO:Landroid/support/v4/app/E;

    invoke-static {v0}, Landroid/support/v4/app/an;->eil(Landroid/support/v4/app/E;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOO:Landroid/support/v4/app/E;

    return-object v0
.end method

.method public ehi()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebw()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method ehk(Landroid/os/Parcelable;Landroid/support/v4/app/E;)V
    .locals 11

    const/4 v2, 0x0

    const/4 v5, 0x0

    if-nez p1, :cond_0

    return-void

    :cond_0
    check-cast p1, Landroid/support/v4/app/FragmentManagerState;

    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->eLF:[Landroid/support/v4/app/FragmentState;

    if-nez v0, :cond_1

    return-void

    :cond_1
    if-eqz p2, :cond_18

    invoke-virtual {p2}, Landroid/support/v4/app/E;->edn()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p2}, Landroid/support/v4/app/E;->edo()Ljava/util/List;

    move-result-object v4

    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    :goto_0
    move v6, v2

    :goto_1
    if-ge v6, v1, :cond_7

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    sget-boolean v3, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v3, :cond_2

    const-string/jumbo v3, "FragmentManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "restoreAllState: re-attaching retained "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v3, v2

    :goto_2
    iget-object v8, p1, Landroid/support/v4/app/FragmentManagerState;->eLF:[Landroid/support/v4/app/FragmentState;

    array-length v8, v8

    if-ge v3, v8, :cond_4

    iget-object v8, p1, Landroid/support/v4/app/FragmentManagerState;->eLF:[Landroid/support/v4/app/FragmentState;

    aget-object v8, v8, v3

    iget v8, v8, Landroid/support/v4/app/FragmentState;->eNK:I

    iget v9, v0, Landroid/support/v4/app/p;->eKp:I

    if-eq v8, v9, :cond_4

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v8, p1, Landroid/support/v4/app/FragmentManagerState;->eLF:[Landroid/support/v4/app/FragmentState;

    array-length v8, v8

    if-ne v3, v8, :cond_5

    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Could not find active fragment with index "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v0, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v8}, Landroid/support/v4/app/an;->eir(Ljava/lang/RuntimeException;)V

    :cond_5
    iget-object v8, p1, Landroid/support/v4/app/FragmentManagerState;->eLF:[Landroid/support/v4/app/FragmentState;

    aget-object v3, v8, v3

    iput-object v0, v3, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iput-object v5, v0, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    iput v2, v0, Landroid/support/v4/app/p;->eKz:I

    iput-boolean v2, v0, Landroid/support/v4/app/p;->eKw:Z

    iput-boolean v2, v0, Landroid/support/v4/app/p;->eKj:Z

    iput-object v5, v0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    iget-object v8, v3, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    if-eqz v8, :cond_6

    iget-object v8, v3, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    iget-object v9, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v9}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v8, v3, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    const-string/jumbo v9, "android:view_state"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v8

    iput-object v8, v0, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    iget-object v3, v3, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    iput-object v3, v0, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    :cond_6
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_1

    :cond_7
    move-object v1, v4

    :goto_3
    new-instance v0, Landroid/util/SparseArray;

    iget-object v3, p1, Landroid/support/v4/app/FragmentManagerState;->eLF:[Landroid/support/v4/app/FragmentState;

    array-length v3, v3

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    move v3, v2

    :goto_4
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->eLF:[Landroid/support/v4/app/FragmentState;

    array-length v0, v0

    if-ge v3, v0, :cond_b

    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->eLF:[Landroid/support/v4/app/FragmentState;

    aget-object v4, v0, v3

    if-eqz v4, :cond_9

    if-eqz v1, :cond_a

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_a

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/E;

    :goto_5
    iget-object v6, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    iget-object v7, p0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    iget-object v8, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v4, v6, v7, v8, v0}, Landroid/support/v4/app/FragmentState;->efp(Landroid/support/v4/app/C;Landroid/support/v4/app/k;Landroid/support/v4/app/p;Landroid/support/v4/app/E;)Landroid/support/v4/app/p;

    move-result-object v0

    sget-boolean v6, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v6, :cond_8

    const-string/jumbo v6, "FragmentManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "restoreAllState: active #"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v6, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    iget v7, v0, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iput-object v5, v4, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    :cond_9
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_a
    move-object v0, v5

    goto :goto_5

    :cond_b
    if-eqz p2, :cond_e

    invoke-virtual {p2}, Landroid/support/v4/app/E;->edn()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_d

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    :goto_6
    move v4, v2

    :goto_7
    if-ge v4, v3, :cond_e

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    iget v1, v0, Landroid/support/v4/app/p;->eKK:I

    if-ltz v1, :cond_c

    iget-object v1, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    iget v7, v0, Landroid/support/v4/app/p;->eKK:I

    invoke-virtual {v1, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/p;

    iput-object v1, v0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    iget-object v1, v0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    if-nez v1, :cond_c

    const-string/jumbo v1, "FragmentManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Re-attaching retained fragment "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " target no longer exists: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v0, v0, Landroid/support/v4/app/p;->eKK:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_7

    :cond_d
    move v3, v2

    goto :goto_6

    :cond_e
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->eLC:[I

    if-eqz v0, :cond_12

    move v1, v2

    :goto_8
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->eLC:[I

    array-length v0, v0

    if-ge v1, v0, :cond_12

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    iget-object v3, p1, Landroid/support/v4/app/FragmentManagerState;->eLC:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-nez v0, :cond_f

    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "No instantiated fragment for index #"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p1, Landroid/support/v4/app/FragmentManagerState;->eLC:[I

    aget v6, v6, v1

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Landroid/support/v4/app/an;->eir(Ljava/lang/RuntimeException;)V

    :cond_f
    const/4 v3, 0x1

    iput-boolean v3, v0, Landroid/support/v4/app/p;->eKj:Z

    sget-boolean v3, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v3, :cond_10

    const-string/jumbo v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "restoreAllState: added #"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    iget-object v3, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iget-object v3, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_12
    iget-object v0, p1, Landroid/support/v4/app/FragmentManagerState;->eLD:[Landroid/support/v4/app/BackStackState;

    if-eqz v0, :cond_15

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->eLD:[Landroid/support/v4/app/BackStackState;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    move v0, v2

    :goto_9
    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->eLD:[Landroid/support/v4/app/BackStackState;

    array-length v1, v1

    if-ge v0, v1, :cond_16

    iget-object v1, p1, Landroid/support/v4/app/FragmentManagerState;->eLD:[Landroid/support/v4/app/BackStackState;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Landroid/support/v4/app/BackStackState;->eiT(Landroid/support/v4/app/an;)Landroid/support/v4/app/al;

    move-result-object v1

    sget-boolean v3, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v3, :cond_13

    const-string/jumbo v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "restoreAllState: back stack #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " (index "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/support/v4/app/al;->eOp:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/support/v4/a/q;

    const-string/jumbo v4, "FragmentManager"

    invoke-direct {v3, v4}, Landroid/support/v4/a/q;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    const-string/jumbo v3, "  "

    invoke-virtual {v1, v3, v4, v2}, Landroid/support/v4/app/al;->efV(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    invoke-virtual {v4}, Ljava/io/PrintWriter;->close()V

    :cond_13
    iget-object v3, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v3, v1, Landroid/support/v4/app/al;->eOp:I

    if-ltz v3, :cond_14

    iget v3, v1, Landroid/support/v4/app/al;->eOp:I

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/an;->ehQ(ILandroid/support/v4/app/al;)V

    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_15
    iput-object v5, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    :cond_16
    iget v0, p1, Landroid/support/v4/app/FragmentManagerState;->eLE:I

    if-ltz v0, :cond_17

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    iget v1, p1, Landroid/support/v4/app/FragmentManagerState;->eLE:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    iput-object v0, p0, Landroid/support/v4/app/an;->ePc:Landroid/support/v4/app/p;

    :cond_17
    iget v0, p1, Landroid/support/v4/app/FragmentManagerState;->eLG:I

    iput v0, p0, Landroid/support/v4/app/an;->ePf:I

    return-void

    :cond_18
    move-object v1, v5

    goto/16 :goto_3
.end method

.method ehl(Landroid/support/v4/app/p;IZI)Landroid/support/v4/app/P;
    .locals 10

    const v9, 0x3f79999a    # 0.975f

    const/4 v0, 0x0

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebS()I

    move-result v1

    invoke-virtual {p1, p2, p3, v1}, Landroid/support/v4/app/p;->ecO(IZI)Landroid/view/animation/Animation;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Landroid/support/v4/app/P;

    invoke-direct {v0, v2, v6}, Landroid/support/v4/app/P;-><init>(Landroid/view/animation/Animation;Landroid/support/v4/app/P;)V

    return-object v0

    :cond_0
    invoke-virtual {p1, p2, p3, v1}, Landroid/support/v4/app/p;->ecy(IZI)Landroid/animation/Animator;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v0, Landroid/support/v4/app/P;

    invoke-direct {v0, v2, v6}, Landroid/support/v4/app/P;-><init>(Landroid/animation/Animator;Landroid/support/v4/app/P;)V

    return-object v0

    :cond_1
    if-eqz v1, :cond_5

    iget-object v2, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v2}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "anim"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :try_start_0
    iget-object v3, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v3}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    if-eqz v3, :cond_2

    new-instance v4, Landroid/support/v4/app/P;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Landroid/support/v4/app/P;-><init>(Landroid/view/animation/Animation;Landroid/support/v4/app/P;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v4

    :cond_2
    const/4 v0, 0x1

    :cond_3
    :goto_0
    if-nez v0, :cond_5

    :try_start_1
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_5

    new-instance v3, Landroid/support/v4/app/P;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Landroid/support/v4/app/P;-><init>(Landroid/animation/Animator;Landroid/support/v4/app/P;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v3

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    if-eqz v2, :cond_4

    throw v0

    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_5

    new-instance v1, Landroid/support/v4/app/P;

    invoke-direct {v1, v0, v6}, Landroid/support/v4/app/P;-><init>(Landroid/view/animation/Animation;Landroid/support/v4/app/P;)V

    return-object v1

    :cond_5
    if-nez p2, :cond_6

    return-object v6

    :cond_6
    invoke-static {p2, p3}, Landroid/support/v4/app/an;->egu(IZ)I

    move-result v0

    if-gez v0, :cond_7

    return-object v6

    :cond_7
    packed-switch v0, :pswitch_data_0

    if-nez p4, :cond_8

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->edk()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->edh()I

    move-result p4

    :cond_8
    if-nez p4, :cond_9

    return-object v6

    :pswitch_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x3f900000    # 1.125f

    invoke-static {v0, v1, v7, v8, v7}, Landroid/support/v4/app/an;->ehh(Landroid/content/Context;FFFF)Landroid/support/v4/app/P;

    move-result-object v0

    return-object v0

    :pswitch_1
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7, v9, v7, v8}, Landroid/support/v4/app/an;->ehh(Landroid/content/Context;FFFF)Landroid/support/v4/app/P;

    move-result-object v0

    return-object v0

    :pswitch_2
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v9, v7, v8, v7}, Landroid/support/v4/app/an;->ehh(Landroid/content/Context;FFFF)Landroid/support/v4/app/P;

    move-result-object v0

    return-object v0

    :pswitch_3
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x3f89999a    # 1.075f

    invoke-static {v0, v7, v1, v7, v8}, Landroid/support/v4/app/an;->ehh(Landroid/content/Context;FFFF)Landroid/support/v4/app/P;

    move-result-object v0

    return-object v0

    :pswitch_4
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v8, v7}, Landroid/support/v4/app/an;->ehx(Landroid/content/Context;FF)Landroid/support/v4/app/P;

    move-result-object v0

    return-object v0

    :pswitch_5
    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7, v8}, Landroid/support/v4/app/an;->ehx(Landroid/content/Context;FF)Landroid/support/v4/app/P;

    move-result-object v0

    return-object v0

    :cond_9
    return-object v6

    :catch_2
    move-exception v3

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method ehm()V
    .locals 5

    const/4 v2, 0x0

    iget-boolean v0, p0, Landroid/support/v4/app/an;->ePm:Z

    if-eqz v0, :cond_2

    move v1, v2

    move v3, v2

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v4, v0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    if-eqz v4, :cond_0

    iget-object v0, v0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->eiE()Z

    move-result v0

    or-int/2addr v3, v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez v3, :cond_2

    iput-boolean v2, p0, Landroid/support/v4/app/an;->ePm:Z

    invoke-virtual {p0}, Landroid/support/v4/app/an;->egQ()V

    :cond_2
    return-void
.end method

.method public ehn()Z
    .locals 4

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Landroid/support/v4/app/an;->ehb(Z)V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Landroid/support/v4/app/an;->ePb:Ljava/util/ArrayList;

    iget-object v3, p0, Landroid/support/v4/app/an;->eOX:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Landroid/support/v4/app/an;->egS(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-boolean v1, p0, Landroid/support/v4/app/an;->eOS:Z

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->ePb:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/an;->eOX:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v2}, Landroid/support/v4/app/an;->ehd(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Landroid/support/v4/app/an;->ehL()V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Landroid/support/v4/app/an;->ehL()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/an;->ehm()V

    invoke-direct {p0}, Landroid/support/v4/app/an;->egG()V

    return v0
.end method

.method eho(Landroid/support/v4/app/p;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/an;->eho(Landroid/support/v4/app/p;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p2, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/as;->eiJ(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public ehp(Landroid/view/Menu;)V
    .locals 2

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ebH(Landroid/view/Menu;)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method ehq(Landroid/support/v4/app/p;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebJ()I

    move-result v0

    iget-boolean v2, p1, Landroid/support/v4/app/p;->eKZ:Z

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebN()I

    move-result v3

    invoke-virtual {p0, p1, v0, v2, v3}, Landroid/support/v4/app/an;->ehl(Landroid/support/v4/app/p;IZI)Landroid/support/v4/app/P;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v2, v0, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    if-eqz v2, :cond_4

    iget-object v2, v0, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    iget-object v3, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    iget-boolean v2, p1, Landroid/support/v4/app/p;->eKZ:Z

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebx()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, v1}, Landroid/support/v4/app/p;->ect(Z)V

    :goto_0
    iget-object v2, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-static {v2, v0}, Landroid/support/v4/app/an;->ehP(Landroid/view/View;Landroid/support/v4/app/P;)V

    iget-object v0, v0, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    :cond_0
    :goto_1
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKj:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKt:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKY:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOL:Z

    :cond_1
    iput-boolean v1, p1, Landroid/support/v4/app/p;->eKC:Z

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKZ:Z

    invoke-virtual {p1, v0}, Landroid/support/v4/app/p;->ecL(Z)V

    return-void

    :cond_2
    iget-object v2, p1, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    iget-object v3, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->startViewTransition(Landroid/view/View;)V

    iget-object v4, v0, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    new-instance v5, Landroid/support/v4/app/V;

    invoke-direct {v5, p0, v2, v3, p1}, Landroid/support/v4/app/V;-><init>(Landroid/support/v4/app/an;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/app/p;)V

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    :cond_3
    iget-object v2, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_5

    iget-object v2, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-static {v2, v0}, Landroid/support/v4/app/an;->ehP(Landroid/view/View;Landroid/support/v4/app/P;)V

    iget-object v2, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    iget-object v3, v0, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    invoke-virtual {v2, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, v0, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->start()V

    :cond_5
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKZ:Z

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebx()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    :goto_2
    iget-object v2, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebx()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/support/v4/app/p;->ect(Z)V

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public ehr(Landroid/support/v4/app/p;)V
    .locals 4

    const/4 v3, 0x1

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "remove: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " nesting="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/support/v4/app/p;->eKz:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebW()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-boolean v1, p1, Landroid/support/v4/app/p;->eKR:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_3

    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKt:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKY:Z

    if-eqz v0, :cond_2

    iput-boolean v3, p0, Landroid/support/v4/app/an;->eOL:Z

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/support/v4/app/p;->eKj:Z

    iput-boolean v3, p1, Landroid/support/v4/app/p;->eKN:Z

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public ehv(Landroid/support/v4/app/p;)V
    .locals 4

    const/4 v3, 0x1

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "detach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKR:Z

    if-nez v0, :cond_3

    iput-boolean v3, p1, Landroid/support/v4/app/p;->eKR:Z

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKj:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "remove from detach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKt:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKY:Z

    if-eqz v0, :cond_2

    iput-boolean v3, p0, Landroid/support/v4/app/an;->eOL:Z

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/support/v4/app/p;->eKj:Z

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public ehw()Landroid/support/v4/app/N;
    .locals 1

    new-instance v0, Landroid/support/v4/app/al;

    invoke-direct {v0, p0}, Landroid/support/v4/app/al;-><init>(Landroid/support/v4/app/an;)V

    return-object v0
.end method

.method public ehz()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->ehj(I)V

    return-void
.end method

.method public eia()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOT:Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->ehj(I)V

    return-void
.end method

.method eib(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v4/app/an;->eib(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p3, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/app/as;->eiQ(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method eic(Landroid/support/v4/app/p;)Landroid/os/Bundle;
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOI:Landroid/os/Bundle;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/an;->eOI:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOI:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/p;->ecK(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOI:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v2}, Landroid/support/v4/app/an;->egT(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOI:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Landroid/support/v4/app/an;->eOI:Landroid/os/Bundle;

    iput-object v1, p0, Landroid/support/v4/app/an;->eOI:Landroid/os/Bundle;

    :goto_0
    iget-object v1, p1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/an;->ehD(Landroid/support/v4/app/p;)V

    :cond_1
    iget-object v1, p1, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    if-eqz v1, :cond_3

    if-nez v0, :cond_2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_2
    const-string/jumbo v1, "android:view_state"

    iget-object v2, p1, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_3
    iget-boolean v1, p1, Landroid/support/v4/app/p;->eKx:Z

    if-nez v1, :cond_5

    if-nez v0, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_4
    const-string/jumbo v1, "android:user_visible_hint"

    iget-boolean v2, p1, Landroid/support/v4/app/p;->eKx:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_5
    return-object v0

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method public eid(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    if-ge v0, v3, :cond_0

    return v2

    :cond_0
    move v1, v2

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ecb(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v3

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return v2
.end method

.method eif(Landroid/support/v4/app/p;Landroid/view/View;Landroid/os/Bundle;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, p3, v1}, Landroid/support/v4/app/an;->eif(Landroid/support/v4/app/p;Landroid/view/View;Landroid/os/Bundle;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p4, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/v4/app/as;->eiF(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;Landroid/view/View;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public eig()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/app/an;->eOT:Z

    return v0
.end method

.method eih(Landroid/support/v4/app/p;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/an;->eih(Landroid/support/v4/app/p;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p2, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/as;->eiP(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public eii(Ljava/lang/String;)Landroid/support/v4/app/p;
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_2

    iget-object v2, v0, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    return-object v0

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_3
    return-object v3
.end method

.method public eij()V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Landroid/support/v4/app/an;->eOO:Landroid/support/v4/app/E;

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOT:Z

    iget-object v1, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecG()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method eik(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v4/app/an;->eik(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p3, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/app/as;->eiM(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method eim(Landroid/support/v4/app/p;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/an;->eim(Landroid/support/v4/app/p;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p2, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/as;->eiS(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method ein(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;II)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    return v3

    :cond_0
    if-nez p3, :cond_3

    if-gez p4, :cond_3

    and-int/lit8 v0, p5, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_1

    return v3

    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return v4

    :cond_3
    const/4 v0, -0x1

    if-nez p3, :cond_4

    if-ltz p4, :cond_6

    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_5

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    if-eqz p3, :cond_8

    invoke-virtual {v0}, Landroid/support/v4/app/al;->efQ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_5
    if-gez v1, :cond_a

    return v3

    :cond_6
    move v1, v0

    :cond_7
    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_d

    return v3

    :cond_8
    if-ltz p4, :cond_9

    iget v0, v0, Landroid/support/v4/app/al;->eOp:I

    if-eq p4, v0, :cond_5

    :cond_9
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_a
    and-int/lit8 v0, p5, 0x1

    if-eqz v0, :cond_7

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_7

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    if-eqz p3, :cond_b

    invoke-virtual {v0}, Landroid/support/v4/app/al;->efQ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_b
    if-ltz p4, :cond_7

    iget v0, v0, Landroid/support/v4/app/al;->eOp:I

    if-ne p4, v0, :cond_7

    :cond_c
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_d
    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-le v2, v1, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOY:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2
.end method

.method eio(I)Z
    .locals 1

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    if-lt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method eip(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v4/app/an;->eip(Landroid/support/v4/app/p;Landroid/os/Bundle;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p3, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v4/app/as;->eiK(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method eiq(Landroid/support/v4/app/p;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecQ()Landroid/support/v4/app/ar;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/app/an;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v4/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/an;->eiq(Landroid/support/v4/app/p;Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/an;->eOZ:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/i;

    if-eqz p2, :cond_2

    iget-object v1, v0, Landroid/support/v4/a/i;->eBO:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/a/i;->eBN:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/as;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/as;->eiO(Landroid/support/v4/app/ar;Landroid/support/v4/app/p;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public eit(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/p;)V
    .locals 3

    iget v0, p3, Landroid/support/v4/app/p;->eKp:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not currently in the FragmentManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->eir(Ljava/lang/RuntimeException;)V

    :cond_0
    iget v0, p3, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public eiu()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/an;->eOT:Z

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Landroid/support/v4/app/an;->ehj(I)V

    return-void
.end method

.method public eiv(Z)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ebX(Z)V

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 11

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string/jumbo v0, "fragment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object v4

    :cond_0
    const-string/jumbo v0, "class"

    invoke-interface {p4, v4, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/support/v4/app/n;->eKe:[I

    invoke-virtual {p3, p4, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    if-nez v0, :cond_10

    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    :goto_0
    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6}, Landroid/support/v4/app/p;->ebC(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    return-object v4

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    :goto_1
    if-ne v1, v5, :cond_3

    if-ne v7, v5, :cond_3

    if-nez v8, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": Must specify unique android:id, android:tag, or have a parent with an id for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    if-eq v7, v5, :cond_7

    invoke-virtual {p0, v7}, Landroid/support/v4/app/an;->ehA(I)Landroid/support/v4/app/p;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_4

    if-eqz v8, :cond_4

    invoke-virtual {p0, v8}, Landroid/support/v4/app/an;->eii(Ljava/lang/String;)Landroid/support/v4/app/p;

    move-result-object v0

    :cond_4
    if-nez v0, :cond_5

    if-eq v1, v5, :cond_5

    invoke-virtual {p0, v1}, Landroid/support/v4/app/an;->ehA(I)Landroid/support/v4/app/p;

    move-result-object v0

    :cond_5
    sget-boolean v5, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v5, :cond_6

    const-string/jumbo v5, "FragmentManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "onCreateView: id=0x"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " fname="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " existing="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    if-nez v0, :cond_9

    iget-object v0, p0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    invoke-virtual {v0, p3, v6, v4}, Landroid/support/v4/app/k;->ebg(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/p;

    move-result-object v4

    iput-boolean v2, v4, Landroid/support/v4/app/p;->eKA:Z

    if-eqz v7, :cond_8

    move v0, v7

    :goto_3
    iput v0, v4, Landroid/support/v4/app/p;->eKv:I

    iput v1, v4, Landroid/support/v4/app/p;->eKq:I

    iput-object v8, v4, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    iput-boolean v2, v4, Landroid/support/v4/app/p;->eKw:Z

    iput-object p0, v4, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    iput-object v0, v4, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, v4, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {v4, v0, p4, v1}, Landroid/support/v4/app/p;->ecn(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    invoke-virtual {p0, v4, v2}, Landroid/support/v4/app/an;->ehE(Landroid/support/v4/app/p;Z)V

    move-object v1, v4

    :goto_4
    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    if-ge v0, v2, :cond_b

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKA:Z

    if-eqz v0, :cond_b

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    :goto_5
    iget-object v0, v1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not create a view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move-object v0, v4

    goto/16 :goto_2

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    iget-boolean v4, v0, Landroid/support/v4/app/p;->eKw:Z

    if-eqz v4, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": Duplicate id 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", tag "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", or parent id 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " with another fragment for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    iput-boolean v2, v0, Landroid/support/v4/app/p;->eKw:Z

    iget-object v1, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    iput-object v1, v0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    iget-boolean v1, v0, Landroid/support/v4/app/p;->eKo:Z

    if-nez v1, :cond_f

    iget-object v1, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v1}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, v0, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {v0, v1, p4, v4}, Landroid/support/v4/app/p;->ecn(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    move-object v1, v0

    goto/16 :goto_4

    :cond_b
    invoke-virtual {p0, v1}, Landroid/support/v4/app/an;->ehI(Landroid/support/v4/app/p;)V

    goto/16 :goto_5

    :cond_c
    if-eqz v7, :cond_d

    iget-object v0, v1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    :cond_d
    iget-object v0, v1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_e

    iget-object v0, v1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_e
    iget-object v0, v1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    return-object v0

    :cond_f
    move-object v1, v0

    goto/16 :goto_4

    :cond_10
    move-object v6, v0

    goto/16 :goto_0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Landroid/support/v4/app/an;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string/jumbo v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/an;->eOG:Landroid/support/v4/app/p;

    invoke-static {v1, v0}, Landroid/support/v4/a/k;->dSL(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    :goto_0
    const-string/jumbo v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-static {v1, v0}, Landroid/support/v4/a/k;->dSL(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method
