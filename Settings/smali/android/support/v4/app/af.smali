.class Landroid/support/v4/app/af;
.super Ljava/lang/Object;
.source "FragmentTransition.java"


# static fields
.field private static final eNu:[I

.field private static final eNv:Landroid/support/v4/app/am;

.field private static final eNw:Landroid/support/v4/app/am;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Landroid/support/v4/app/af;->eNu:[I

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/app/ah;

    invoke-direct {v0}, Landroid/support/v4/app/ah;-><init>()V

    :goto_0
    sput-object v0, Landroid/support/v4/app/af;->eNw:Landroid/support/v4/app/am;

    invoke-static {}, Landroid/support/v4/app/af;->eeX()Landroid/support/v4/app/am;

    move-result-object v0

    sput-object v0, Landroid/support/v4/app/af;->eNv:Landroid/support/v4/app/am;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3
        0x0
        0x1
        0x5
        0x4
        0x7
        0x6
        0x9
        0x8
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static eeC(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;)Landroid/support/v4/a/u;
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p3, Landroid/support/v4/app/z;->eLH:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ech()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/a/u;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    if-nez p2, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/a/u;->clear()V

    return-object v3

    :cond_1
    if-eqz v1, :cond_0

    new-instance v4, Landroid/support/v4/a/u;

    invoke-direct {v4}, Landroid/support/v4/a/u;-><init>()V

    invoke-virtual {p0, v4, v1}, Landroid/support/v4/app/am;->egd(Ljava/util/Map;Landroid/view/View;)V

    iget-object v2, p3, Landroid/support/v4/app/z;->eLM:Landroid/support/v4/app/al;

    iget-boolean v1, p3, Landroid/support/v4/app/z;->eLL:Z

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebt()Landroid/support/v4/app/av;

    move-result-object v1

    iget-object v0, v2, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    move-object v3, v0

    move-object v0, v1

    :goto_0
    if-eqz v3, :cond_2

    invoke-virtual {v4, v3}, Landroid/support/v4/a/u;->retainAll(Ljava/util/Collection;)Z

    :cond_2
    if-eqz v0, :cond_6

    invoke-virtual {v0, v3, v4}, Landroid/support/v4/app/av;->eiW(Ljava/util/List;Ljava/util/Map;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_7

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-nez v1, :cond_5

    invoke-static {p1, v0}, Landroid/support/v4/app/af;->efd(Landroid/support/v4/a/u;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, v0}, Landroid/support/v4/a/u;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebA()Landroid/support/v4/app/av;

    move-result-object v1

    iget-object v0, v2, Landroid/support/v4/app/al;->eOu:Ljava/util/ArrayList;

    move-object v3, v0

    move-object v0, v1

    goto :goto_0

    :cond_5
    invoke-static {v1}, Landroid/support/v4/view/z;->dPx(Landroid/view/View;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {p1, v0}, Landroid/support/v4/app/af;->efd(Landroid/support/v4/a/u;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v1}, Landroid/support/v4/view/z;->dPx(Landroid/view/View;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_6
    invoke-static {p1, v4}, Landroid/support/v4/app/af;->efi(Landroid/support/v4/a/u;Landroid/support/v4/a/u;)V

    :cond_7
    return-object v4
.end method

.method private static eeD(Landroid/support/v4/app/z;Landroid/util/SparseArray;I)Landroid/support/v4/app/z;
    .locals 0

    if-nez p0, :cond_0

    new-instance p0, Landroid/support/v4/app/z;

    invoke-direct {p0}, Landroid/support/v4/app/z;-><init>()V

    invoke-virtual {p1, p2, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object p0
.end method

.method private static eeE(Landroid/support/v4/app/al;Landroid/support/v4/app/i;Landroid/util/SparseArray;ZZ)V
    .locals 11

    const/4 v10, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p1, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget v9, v1, Landroid/support/v4/app/p;->eKq:I

    if-nez v9, :cond_1

    return-void

    :cond_1
    if-eqz p3, :cond_6

    sget-object v0, Landroid/support/v4/app/af;->eNu:[I

    iget v4, p1, Landroid/support/v4/app/i;->eJJ:I

    aget v0, v0, v4

    :goto_0
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v4, v3

    move v6, v3

    move v7, v3

    move v5, v3

    :goto_1
    invoke-virtual {p2, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/z;

    if-eqz v5, :cond_13

    invoke-static {v0, p2, v9}, Landroid/support/v4/app/af;->eeD(Landroid/support/v4/app/z;Landroid/util/SparseArray;I)Landroid/support/v4/app/z;

    move-result-object v8

    iput-object v1, v8, Landroid/support/v4/app/z;->eLH:Landroid/support/v4/app/p;

    iput-boolean p3, v8, Landroid/support/v4/app/z;->eLL:Z

    iput-object p0, v8, Landroid/support/v4/app/z;->eLM:Landroid/support/v4/app/al;

    :goto_2
    if-nez p4, :cond_3

    if-eqz v4, :cond_3

    if-eqz v8, :cond_2

    iget-object v0, v8, Landroid/support/v4/app/z;->eLI:Landroid/support/v4/app/p;

    if-ne v0, v1, :cond_2

    iput-object v10, v8, Landroid/support/v4/app/z;->eLI:Landroid/support/v4/app/p;

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iget v4, v1, Landroid/support/v4/app/p;->eKI:I

    if-ge v4, v2, :cond_3

    iget v4, v0, Landroid/support/v4/app/an;->ePg:I

    if-lt v4, v2, :cond_3

    iget-boolean v4, p0, Landroid/support/v4/app/al;->eOn:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    invoke-virtual {v0, v1}, Landroid/support/v4/app/an;->ehH(Landroid/support/v4/app/p;)V

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    :cond_3
    if-eqz v6, :cond_12

    if-eqz v8, :cond_4

    iget-object v0, v8, Landroid/support/v4/app/z;->eLI:Landroid/support/v4/app/p;

    if-nez v0, :cond_12

    :cond_4
    invoke-static {v8, p2, v9}, Landroid/support/v4/app/af;->eeD(Landroid/support/v4/app/z;Landroid/util/SparseArray;I)Landroid/support/v4/app/z;

    move-result-object v0

    iput-object v1, v0, Landroid/support/v4/app/z;->eLI:Landroid/support/v4/app/p;

    iput-boolean p3, v0, Landroid/support/v4/app/z;->eLJ:Z

    iput-object p0, v0, Landroid/support/v4/app/z;->eLK:Landroid/support/v4/app/al;

    :goto_3
    if-nez p4, :cond_5

    if-eqz v7, :cond_5

    if-eqz v0, :cond_5

    iget-object v2, v0, Landroid/support/v4/app/z;->eLH:Landroid/support/v4/app/p;

    if-ne v2, v1, :cond_5

    iput-object v10, v0, Landroid/support/v4/app/z;->eLH:Landroid/support/v4/app/p;

    :cond_5
    return-void

    :cond_6
    iget v0, p1, Landroid/support/v4/app/i;->eJJ:I

    goto :goto_0

    :pswitch_1
    if-eqz p4, :cond_8

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKC:Z

    if-eqz v0, :cond_7

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKZ:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKj:Z

    :goto_4
    move v4, v2

    move v6, v3

    move v7, v3

    move v5, v0

    goto :goto_1

    :cond_7
    move v0, v3

    goto :goto_4

    :cond_8
    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKZ:Z

    goto :goto_4

    :pswitch_2
    if-eqz p4, :cond_9

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKM:Z

    :goto_5
    move v4, v2

    move v6, v3

    move v7, v3

    move v5, v0

    goto :goto_1

    :cond_9
    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKj:Z

    if-nez v0, :cond_a

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKZ:Z

    xor-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_a
    move v0, v3

    goto :goto_5

    :pswitch_3
    if-eqz p4, :cond_c

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKC:Z

    if-eqz v0, :cond_b

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKj:Z

    if-eqz v0, :cond_b

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKZ:Z

    :goto_6
    move v4, v3

    move v6, v0

    move v7, v2

    move v5, v3

    goto/16 :goto_1

    :cond_b
    move v0, v3

    goto :goto_6

    :cond_c
    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKj:Z

    if-eqz v0, :cond_d

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKZ:Z

    xor-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_d
    move v0, v3

    goto :goto_6

    :pswitch_4
    if-eqz p4, :cond_10

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKj:Z

    if-nez v0, :cond_f

    iget-object v0, v1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_f

    iget-object v0, v1, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_f

    iget v0, v1, Landroid/support/v4/app/p;->eKX:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_e

    move v0, v2

    :goto_7
    move v4, v3

    move v6, v0

    move v7, v2

    move v5, v3

    goto/16 :goto_1

    :cond_e
    move v0, v3

    goto :goto_7

    :cond_f
    move v0, v3

    goto :goto_7

    :cond_10
    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKj:Z

    if-eqz v0, :cond_11

    iget-boolean v0, v1, Landroid/support/v4/app/p;->eKZ:Z

    xor-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_11
    move v0, v3

    goto :goto_7

    :cond_12
    move-object v0, v8

    goto/16 :goto_3

    :cond_13
    move-object v8, v0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method private static eeF(Landroid/support/v4/app/am;Landroid/support/v4/app/p;Z)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebE()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/am;->efx(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecr()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private static eeG(Landroid/support/v4/app/am;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Landroid/support/v4/app/p;Z)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_1

    invoke-virtual {p4}, Landroid/support/v4/app/p;->ecM()Z

    move-result v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0, p2, p1, p3}, Landroid/support/v4/app/am;->efB(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {p4}, Landroid/support/v4/app/p;->ebQ()Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p2, p1, p3}, Landroid/support/v4/app/am;->efs(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic eeH(Landroid/support/v4/a/u;Landroid/support/v4/app/z;Ljava/lang/Object;Z)Landroid/view/View;
    .locals 1

    invoke-static {p0, p1, p2, p3}, Landroid/support/v4/app/af;->eeP(Landroid/support/v4/a/u;Landroid/support/v4/app/z;Ljava/lang/Object;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static eeI(ILjava/util/ArrayList;Ljava/util/ArrayList;II)Landroid/support/v4/a/u;
    .locals 9

    new-instance v7, Landroid/support/v4/a/u;

    invoke-direct {v7}, Landroid/support/v4/a/u;-><init>()V

    add-int/lit8 v0, p4, -0x1

    move v6, v0

    :goto_0
    if-lt v6, p3, :cond_4

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {v0, p0}, Landroid/support/v4/app/al;->efN(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    add-int/lit8 v0, v6, -0x1

    move v6, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, v0, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, v0, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-eqz v1, :cond_2

    iget-object v1, v0, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    iget-object v0, v0, Landroid/support/v4/app/al;->eOu:Ljava/util/ArrayList;

    move-object v3, v0

    move-object v4, v1

    :goto_1
    const/4 v0, 0x0

    move v5, v0

    :goto_2
    if-ge v5, v8, :cond_0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1}, Landroid/support/v4/a/u;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v7, v0, v2}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_2
    iget-object v1, v0, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    iget-object v0, v0, Landroid/support/v4/app/al;->eOu:Ljava/util/ArrayList;

    move-object v3, v1

    move-object v4, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v7, v0, v1}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    return-object v7
.end method

.method private static eeJ(Landroid/support/v4/app/am;Landroid/view/ViewGroup;Landroid/support/v4/app/p;Landroid/view/View;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V
    .locals 9

    new-instance v0, Landroid/support/v4/app/q;

    move-object v1, p5

    move-object v2, p0

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    move-object v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Landroid/support/v4/app/q;-><init>(Ljava/lang/Object;Landroid/support/v4/app/am;Landroid/view/View;Landroid/support/v4/app/p;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Object;)V

    invoke-static {p1, v0}, Landroid/support/v4/app/w;->ecS(Landroid/view/View;Ljava/lang/Runnable;)Landroid/support/v4/app/w;

    return-void
.end method

.method private static eeK(Landroid/support/v4/app/am;Landroid/support/v4/app/p;Z)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecw()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/am;->efx(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecB()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private static eeL(Landroid/support/v4/app/am;Ljava/util/List;)Z
    .locals 4

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/support/v4/app/am;->efF(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private static eeM(Landroid/support/v4/app/am;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/a/u;Landroid/support/v4/app/z;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 18

    move-object/from16 v0, p4

    iget-object v12, v0, Landroid/support/v4/app/z;->eLH:Landroid/support/v4/app/p;

    move-object/from16 v0, p4

    iget-object v13, v0, Landroid/support/v4/app/z;->eLI:Landroid/support/v4/app/p;

    if-eqz v12, :cond_0

    if-nez v13, :cond_1

    :cond_0
    const/4 v3, 0x0

    return-object v3

    :cond_1
    move-object/from16 v0, p4

    iget-boolean v14, v0, Landroid/support/v4/app/z;->eLL:Z

    invoke-virtual/range {p3 .. p3}, Landroid/support/v4/a/u;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-static {v0, v1, v3, v2}, Landroid/support/v4/app/af;->eeW(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;)Landroid/support/v4/a/u;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Landroid/support/v4/a/u;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    :goto_1
    if-nez p7, :cond_4

    if-nez p8, :cond_4

    if-nez v4, :cond_4

    const/4 v3, 0x0

    return-object v3

    :cond_2
    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v14}, Landroid/support/v4/app/af;->eeZ(Landroid/support/v4/app/am;Landroid/support/v4/app/p;Landroid/support/v4/app/p;Z)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    :cond_3
    invoke-virtual {v6}, Landroid/support/v4/a/u;->values()Ljava/util/Collection;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object v4, v3

    goto :goto_1

    :cond_4
    const/4 v3, 0x1

    invoke-static {v12, v13, v14, v6, v3}, Landroid/support/v4/app/af;->eeO(Landroid/support/v4/app/p;Landroid/support/v4/app/p;ZLandroid/support/v4/a/u;Z)V

    if-eqz v4, :cond_6

    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-virtual {v0, v4, v1, v2}, Landroid/support/v4/app/am;->efr(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;)V

    move-object/from16 v0, p4

    iget-boolean v7, v0, Landroid/support/v4/app/z;->eLJ:Z

    move-object/from16 v0, p4

    iget-object v8, v0, Landroid/support/v4/app/z;->eLK:Landroid/support/v4/app/al;

    move-object/from16 v3, p0

    move-object/from16 v5, p8

    invoke-static/range {v3 .. v8}, Landroid/support/v4/app/af;->efc(Landroid/support/v4/app/am;Ljava/lang/Object;Ljava/lang/Object;Landroid/support/v4/a/u;ZLandroid/support/v4/app/al;)V

    if-eqz p7, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/am;->efE(Ljava/lang/Object;Landroid/graphics/Rect;)V

    :cond_5
    :goto_2
    new-instance v5, Landroid/support/v4/app/B;

    move-object/from16 v6, p0

    move-object/from16 v7, p3

    move-object v8, v4

    move-object/from16 v9, p4

    move-object/from16 v10, p6

    move-object/from16 v11, p2

    move-object/from16 v15, p5

    move-object/from16 v16, p7

    invoke-direct/range {v5 .. v17}, Landroid/support/v4/app/B;-><init>(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;Ljava/util/ArrayList;Landroid/view/View;Landroid/support/v4/app/p;Landroid/support/v4/app/p;ZLjava/util/ArrayList;Ljava/lang/Object;Landroid/graphics/Rect;)V

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Landroid/support/v4/app/w;->ecS(Landroid/view/View;Ljava/lang/Runnable;)Landroid/support/v4/app/w;

    return-object v4

    :cond_6
    const/16 v17, 0x0

    goto :goto_2
.end method

.method static synthetic eeN(Landroid/support/v4/app/p;Landroid/support/v4/app/p;ZLandroid/support/v4/a/u;Z)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Landroid/support/v4/app/af;->eeO(Landroid/support/v4/app/p;Landroid/support/v4/app/p;ZLandroid/support/v4/a/u;Z)V

    return-void
.end method

.method private static eeO(Landroid/support/v4/app/p;Landroid/support/v4/app/p;ZLandroid/support/v4/a/u;Z)V
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ebA()Landroid/support/v4/app/av;

    move-result-object v1

    move-object v3, v1

    :goto_0
    if-eqz v3, :cond_3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    if-nez p3, :cond_1

    move v1, v0

    :goto_1
    move v2, v0

    :goto_2
    if-ge v2, v1, :cond_2

    invoke-virtual {p3, v2}, Landroid/support/v4/a/u;->dSf(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p3, v2}, Landroid/support/v4/a/u;->dSe(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebA()Landroid/support/v4/app/av;

    move-result-object v1

    move-object v3, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Landroid/support/v4/a/u;->size()I

    move-result v1

    goto :goto_1

    :cond_2
    if-eqz p4, :cond_4

    invoke-virtual {v3, v5, v4, v6}, Landroid/support/v4/app/av;->eiU(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    :cond_3
    :goto_3
    return-void

    :cond_4
    invoke-virtual {v3, v5, v4, v6}, Landroid/support/v4/app/av;->eiV(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    goto :goto_3
.end method

.method private static eeP(Landroid/support/v4/a/u;Landroid/support/v4/app/z;Ljava/lang/Object;Z)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p1, Landroid/support/v4/app/z;->eLM:Landroid/support/v4/app/al;

    if-eqz p2, :cond_1

    if-eqz p0, :cond_1

    iget-object v1, v0, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, v0, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    if-eqz p3, :cond_0

    iget-object v0, v0, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    :cond_0
    iget-object v0, v0, Landroid/support/v4/app/al;->eOu:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method static eeQ(Landroid/support/v4/app/an;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V
    .locals 7

    const/4 v2, 0x0

    iget v0, p0, Landroid/support/v4/app/an;->ePg:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    move v3, p3

    :goto_0
    if-ge v3, p4, :cond_2

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0, v4, p5}, Landroid/support/v4/app/af;->efh(Landroid/support/v4/app/al;Landroid/util/SparseArray;Z)V

    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    invoke-static {v0, v4, p5}, Landroid/support/v4/app/af;->eeU(Landroid/support/v4/app/al;Landroid/util/SparseArray;Z)V

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-eqz v0, :cond_4

    new-instance v3, Landroid/view/View;

    iget-object v0, p0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_4

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-static {v2, p1, p2, p3, p4}, Landroid/support/v4/app/af;->eeI(ILjava/util/ArrayList;Ljava/util/ArrayList;II)Landroid/support/v4/a/u;

    move-result-object v6

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/z;

    if-eqz p5, :cond_3

    invoke-static {p0, v2, v0, v3, v6}, Landroid/support/v4/app/af;->eeS(Landroid/support/v4/app/an;ILandroid/support/v4/app/z;Landroid/view/View;Landroid/support/v4/a/u;)V

    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    invoke-static {p0, v2, v0, v3, v6}, Landroid/support/v4/app/af;->eeR(Landroid/support/v4/app/an;ILandroid/support/v4/app/z;Landroid/view/View;Landroid/support/v4/a/u;)V

    goto :goto_3

    :cond_4
    return-void
.end method

.method private static eeR(Landroid/support/v4/app/an;ILandroid/support/v4/app/z;Landroid/view/View;Landroid/support/v4/a/u;)V
    .locals 29

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    invoke-virtual {v1}, Landroid/support/v4/app/k;->ebf()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/k;->ebe(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move-object v2, v1

    :cond_0
    if-nez v2, :cond_1

    return-void

    :cond_1
    move-object/from16 v0, p2

    iget-object v13, v0, Landroid/support/v4/app/z;->eLH:Landroid/support/v4/app/p;

    move-object/from16 v0, p2

    iget-object v10, v0, Landroid/support/v4/app/z;->eLI:Landroid/support/v4/app/p;

    invoke-static {v10, v13}, Landroid/support/v4/app/af;->efj(Landroid/support/v4/app/p;Landroid/support/v4/app/p;)Landroid/support/v4/app/am;

    move-result-object v1

    if-nez v1, :cond_2

    return-void

    :cond_2
    move-object/from16 v0, p2

    iget-boolean v3, v0, Landroid/support/v4/app/z;->eLL:Z

    move-object/from16 v0, p2

    iget-boolean v4, v0, Landroid/support/v4/app/z;->eLJ:Z

    invoke-static {v1, v13, v3}, Landroid/support/v4/app/af;->eeK(Landroid/support/v4/app/am;Landroid/support/v4/app/p;Z)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v1, v10, v4}, Landroid/support/v4/app/af;->eeF(Landroid/support/v4/app/am;Landroid/support/v4/app/p;Z)Ljava/lang/Object;

    move-result-object v9

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p2

    invoke-static/range {v1 .. v9}, Landroid/support/v4/app/af;->eeM(Landroid/support/v4/app/am;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/a/u;Landroid/support/v4/app/z;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    if-nez v8, :cond_3

    if-nez v12, :cond_3

    if-nez v9, :cond_3

    return-void

    :cond_3
    move-object/from16 v0, p3

    invoke-static {v1, v9, v10, v6, v0}, Landroid/support/v4/app/af;->efe(Landroid/support/v4/app/am;Ljava/lang/Object;Landroid/support/v4/app/p;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v19

    if-eqz v19, :cond_4

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_4
    const/4 v11, 0x0

    :goto_0
    move-object/from16 v0, p3

    invoke-virtual {v1, v8, v0}, Landroid/support/v4/app/am;->efC(Ljava/lang/Object;Landroid/view/View;)V

    move-object/from16 v0, p2

    iget-boolean v14, v0, Landroid/support/v4/app/z;->eLL:Z

    move-object v9, v1

    move-object v10, v8

    invoke-static/range {v9 .. v14}, Landroid/support/v4/app/af;->eeG(Landroid/support/v4/app/am;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Landroid/support/v4/app/p;Z)Ljava/lang/Object;

    move-result-object v15

    if-eqz v15, :cond_5

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    move-object v14, v1

    move-object/from16 v16, v8

    move-object/from16 v18, v11

    move-object/from16 v20, v12

    move-object/from16 v21, v7

    invoke-virtual/range {v14 .. v21}, Landroid/support/v4/app/am;->efG(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V

    move-object/from16 v20, v1

    move-object/from16 v21, v2

    move-object/from16 v22, v13

    move-object/from16 v23, p3

    move-object/from16 v24, v7

    move-object/from16 v25, v8

    move-object/from16 v26, v17

    move-object/from16 v27, v11

    move-object/from16 v28, v19

    invoke-static/range {v20 .. v28}, Landroid/support/v4/app/af;->eeJ(Landroid/support/v4/app/am;Landroid/view/ViewGroup;Landroid/support/v4/app/p;Landroid/view/View;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v7, v0}, Landroid/support/v4/app/am;->egh(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/Map;)V

    invoke-virtual {v1, v2, v15}, Landroid/support/v4/app/am;->efA(Landroid/view/ViewGroup;Ljava/lang/Object;)V

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v7, v0}, Landroid/support/v4/app/am;->egc(Landroid/view/ViewGroup;Ljava/util/ArrayList;Ljava/util/Map;)V

    :cond_5
    return-void

    :cond_6
    move-object v11, v9

    goto :goto_0
.end method

.method private static eeS(Landroid/support/v4/app/an;ILandroid/support/v4/app/z;Landroid/view/View;Landroid/support/v4/a/u;)V
    .locals 23

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    invoke-virtual {v2}, Landroid/support/v4/app/k;->ebf()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/support/v4/app/k;->ebe(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    move-object v3, v2

    :cond_0
    if-nez v3, :cond_1

    return-void

    :cond_1
    move-object/from16 v0, p2

    iget-object v15, v0, Landroid/support/v4/app/z;->eLH:Landroid/support/v4/app/p;

    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/v4/app/z;->eLI:Landroid/support/v4/app/p;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v15}, Landroid/support/v4/app/af;->efj(Landroid/support/v4/app/p;Landroid/support/v4/app/p;)Landroid/support/v4/app/am;

    move-result-object v2

    if-nez v2, :cond_2

    return-void

    :cond_2
    move-object/from16 v0, p2

    iget-boolean v0, v0, Landroid/support/v4/app/z;->eLL:Z

    move/from16 v16, v0

    move-object/from16 v0, p2

    iget-boolean v4, v0, Landroid/support/v4/app/z;->eLJ:Z

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move/from16 v0, v16

    invoke-static {v2, v15, v0}, Landroid/support/v4/app/af;->eeK(Landroid/support/v4/app/am;Landroid/support/v4/app/p;Z)Ljava/lang/Object;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-static {v2, v0, v4}, Landroid/support/v4/app/af;->eeF(Landroid/support/v4/app/am;Landroid/support/v4/app/p;Z)Ljava/lang/Object;

    move-result-object v10

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p2

    invoke-static/range {v2 .. v10}, Landroid/support/v4/app/af;->efb(Landroid/support/v4/app/am;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/a/u;Landroid/support/v4/app/z;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    if-nez v9, :cond_3

    if-nez v14, :cond_3

    if-nez v10, :cond_3

    return-void

    :cond_3
    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-static {v2, v10, v0, v7, v1}, Landroid/support/v4/app/af;->efe(Landroid/support/v4/app/am;Ljava/lang/Object;Landroid/support/v4/app/p;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v20

    move-object/from16 v0, p3

    invoke-static {v2, v9, v15, v8, v0}, Landroid/support/v4/app/af;->efe(Landroid/support/v4/app/am;Ljava/lang/Object;Landroid/support/v4/app/p;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v18

    const/4 v4, 0x4

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Landroid/support/v4/app/af;->efg(Ljava/util/ArrayList;I)V

    move-object v11, v2

    move-object v12, v9

    move-object v13, v10

    invoke-static/range {v11 .. v16}, Landroid/support/v4/app/af;->eeG(Landroid/support/v4/app/am;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Landroid/support/v4/app/p;Z)Ljava/lang/Object;

    move-result-object v16

    if-eqz v16, :cond_4

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-static {v2, v10, v0, v1}, Landroid/support/v4/app/af;->eeT(Landroid/support/v4/app/am;Ljava/lang/Object;Landroid/support/v4/app/p;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v8}, Landroid/support/v4/app/am;->ege(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    move-object v15, v2

    move-object/from16 v17, v9

    move-object/from16 v19, v10

    move-object/from16 v21, v14

    move-object/from16 v22, v8

    invoke-virtual/range {v15 .. v22}, Landroid/support/v4/app/am;->efG(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/app/am;->efA(Landroid/view/ViewGroup;Ljava/lang/Object;)V

    move-object v5, v2

    move-object v6, v3

    move-object v9, v4

    move-object/from16 v10, p4

    invoke-virtual/range {v5 .. v10}, Landroid/support/v4/app/am;->egf(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Map;)V

    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-static {v0, v3}, Landroid/support/v4/app/af;->efg(Ljava/util/ArrayList;I)V

    invoke-virtual {v2, v14, v7, v8}, Landroid/support/v4/app/am;->efz(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_4
    return-void
.end method

.method private static eeT(Landroid/support/v4/app/am;Ljava/lang/Object;Landroid/support/v4/app/p;Ljava/util/ArrayList;)V
    .locals 2

    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v0, p2, Landroid/support/v4/app/p;->eKj:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p2, Landroid/support/v4/app/p;->eKZ:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p2, Landroid/support/v4/app/p;->eKC:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v4/app/p;->ect(Z)V

    invoke-virtual {p2}, Landroid/support/v4/app/p;->ech()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Landroid/support/v4/app/am;->efw(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;)V

    iget-object v0, p2, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    new-instance v1, Landroid/support/v4/app/aq;

    invoke-direct {v1, p3}, Landroid/support/v4/app/aq;-><init>(Ljava/util/ArrayList;)V

    invoke-static {v0, v1}, Landroid/support/v4/app/w;->ecS(Landroid/view/View;Ljava/lang/Runnable;)Landroid/support/v4/app/w;

    :cond_0
    return-void
.end method

.method public static eeU(Landroid/support/v4/app/al;Landroid/util/SparseArray;Z)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    invoke-static {p0, v0, p1, v2, p2}, Landroid/support/v4/app/af;->eeE(Landroid/support/v4/app/al;Landroid/support/v4/app/i;Landroid/util/SparseArray;ZZ)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic eeV(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;)Landroid/support/v4/a/u;
    .locals 1

    invoke-static {p0, p1, p2, p3}, Landroid/support/v4/app/af;->eeC(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;)Landroid/support/v4/a/u;

    move-result-object v0

    return-object v0
.end method

.method private static eeW(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;)Landroid/support/v4/a/u;
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/support/v4/a/u;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/a/u;->clear()V

    return-object v1

    :cond_1
    iget-object v0, p3, Landroid/support/v4/app/z;->eLI:Landroid/support/v4/app/p;

    new-instance v4, Landroid/support/v4/a/u;

    invoke-direct {v4}, Landroid/support/v4/a/u;-><init>()V

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ech()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Landroid/support/v4/app/am;->egd(Ljava/util/Map;Landroid/view/View;)V

    iget-object v2, p3, Landroid/support/v4/app/z;->eLK:Landroid/support/v4/app/al;

    iget-boolean v1, p3, Landroid/support/v4/app/z;->eLJ:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebA()Landroid/support/v4/app/av;

    move-result-object v1

    iget-object v0, v2, Landroid/support/v4/app/al;->eOu:Ljava/util/ArrayList;

    move-object v2, v0

    move-object v0, v1

    :goto_0
    invoke-virtual {v4, v2}, Landroid/support/v4/a/u;->retainAll(Ljava/util/Collection;)Z

    if-eqz v0, :cond_5

    invoke-virtual {v0, v2, v4}, Landroid/support/v4/app/av;->eiW(Ljava/util/List;Ljava/util/Map;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_6

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-nez v1, :cond_4

    invoke-virtual {p1, v0}, Landroid/support/v4/a/u;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebt()Landroid/support/v4/app/av;

    move-result-object v1

    iget-object v0, v2, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    move-object v2, v0

    move-object v0, v1

    goto :goto_0

    :cond_4
    invoke-static {v1}, Landroid/support/v4/view/z;->dPx(Landroid/view/View;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p1, v0}, Landroid/support/v4/a/u;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1}, Landroid/support/v4/view/z;->dPx(Landroid/view/View;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    invoke-virtual {v4}, Landroid/support/v4/a/u;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/a/u;->retainAll(Ljava/util/Collection;)Z

    :cond_6
    return-object v4
.end method

.method private static eeX()Landroid/support/v4/app/am;
    .locals 2

    :try_start_0
    const-string/jumbo v0, "android.support.transition.FragmentTransitionSupport"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/am;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic eeY(Ljava/util/ArrayList;I)V
    .locals 0

    invoke-static {p0, p1}, Landroid/support/v4/app/af;->efg(Ljava/util/ArrayList;I)V

    return-void
.end method

.method private static eeZ(Landroid/support/v4/app/am;Landroid/support/v4/app/p;Landroid/support/v4/app/p;Z)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return-object v0

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p2}, Landroid/support/v4/app/p;->ebR()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/am;->efx(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/am;->efv(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecg()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic efa(Landroid/support/v4/app/am;Ljava/lang/Object;Landroid/support/v4/app/p;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;
    .locals 1

    invoke-static {p0, p1, p2, p3, p4}, Landroid/support/v4/app/af;->efe(Landroid/support/v4/app/am;Ljava/lang/Object;Landroid/support/v4/app/p;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private static efb(Landroid/support/v4/app/am;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/a/u;Landroid/support/v4/app/z;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    move-object/from16 v0, p4

    iget-object v10, v0, Landroid/support/v4/app/z;->eLH:Landroid/support/v4/app/p;

    move-object/from16 v0, p4

    iget-object v12, v0, Landroid/support/v4/app/z;->eLI:Landroid/support/v4/app/p;

    if-eqz v10, :cond_0

    invoke-virtual {v10}, Landroid/support/v4/app/p;->ech()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v10, :cond_1

    if-nez v12, :cond_2

    :cond_1
    const/4 v2, 0x0

    return-object v2

    :cond_2
    move-object/from16 v0, p4

    iget-boolean v13, v0, Landroid/support/v4/app/z;->eLL:Z

    invoke-virtual/range {p3 .. p3}, Landroid/support/v4/a/u;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {p0, v0, v2, v1}, Landroid/support/v4/app/af;->eeW(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;)Landroid/support/v4/a/u;

    move-result-object v5

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {p0, v0, v2, v1}, Landroid/support/v4/app/af;->eeC(Landroid/support/v4/app/am;Landroid/support/v4/a/u;Ljava/lang/Object;Landroid/support/v4/app/z;)Landroid/support/v4/a/u;

    move-result-object v8

    invoke-virtual/range {p3 .. p3}, Landroid/support/v4/a/u;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Landroid/support/v4/a/u;->clear()V

    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Landroid/support/v4/a/u;->clear()V

    :cond_4
    :goto_1
    if-nez p7, :cond_7

    if-nez p8, :cond_7

    if-nez v3, :cond_7

    const/4 v2, 0x0

    return-object v2

    :cond_5
    invoke-static {p0, v10, v12, v13}, Landroid/support/v4/app/af;->eeZ(Landroid/support/v4/app/am;Landroid/support/v4/app/p;Landroid/support/v4/app/p;Z)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    :cond_6
    invoke-virtual/range {p3 .. p3}, Landroid/support/v4/a/u;->keySet()Ljava/util/Set;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-static {v0, v5, v3}, Landroid/support/v4/app/af;->eff(Ljava/util/ArrayList;Landroid/support/v4/a/u;Ljava/util/Collection;)V

    invoke-virtual/range {p3 .. p3}, Landroid/support/v4/a/u;->values()Ljava/util/Collection;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-static {v0, v8, v3}, Landroid/support/v4/app/af;->eff(Ljava/util/ArrayList;Landroid/support/v4/a/u;Ljava/util/Collection;)V

    move-object v3, v2

    goto :goto_1

    :cond_7
    const/4 v2, 0x1

    invoke-static {v10, v12, v13, v5, v2}, Landroid/support/v4/app/af;->eeO(Landroid/support/v4/app/p;Landroid/support/v4/app/p;ZLandroid/support/v4/a/u;Z)V

    if-eqz v3, :cond_9

    move-object/from16 v0, p6

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-virtual {p0, v3, v0, v1}, Landroid/support/v4/app/am;->efr(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;)V

    move-object/from16 v0, p4

    iget-boolean v6, v0, Landroid/support/v4/app/z;->eLJ:Z

    move-object/from16 v0, p4

    iget-object v7, v0, Landroid/support/v4/app/z;->eLK:Landroid/support/v4/app/al;

    move-object v2, p0

    move-object/from16 v4, p8

    invoke-static/range {v2 .. v7}, Landroid/support/v4/app/af;->efc(Landroid/support/v4/app/am;Ljava/lang/Object;Ljava/lang/Object;Landroid/support/v4/a/u;ZLandroid/support/v4/app/al;)V

    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p4

    move-object/from16 v1, p7

    invoke-static {v8, v0, v1, v13}, Landroid/support/v4/app/af;->eeP(Landroid/support/v4/a/u;Landroid/support/v4/app/z;Ljava/lang/Object;Z)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_8

    move-object/from16 v0, p7

    invoke-virtual {p0, v0, v11}, Landroid/support/v4/app/am;->efE(Ljava/lang/Object;Landroid/graphics/Rect;)V

    :cond_8
    :goto_2
    new-instance v4, Landroid/support/v4/app/v;

    move-object v5, v10

    move-object v6, v12

    move v7, v13

    move-object v10, p0

    invoke-direct/range {v4 .. v11}, Landroid/support/v4/app/v;-><init>(Landroid/support/v4/app/p;Landroid/support/v4/app/p;ZLandroid/support/v4/a/u;Landroid/view/View;Landroid/support/v4/app/am;Landroid/graphics/Rect;)V

    invoke-static {p1, v4}, Landroid/support/v4/app/w;->ecS(Landroid/view/View;Ljava/lang/Runnable;)Landroid/support/v4/app/w;

    return-object v3

    :cond_9
    const/4 v11, 0x0

    const/4 v9, 0x0

    goto :goto_2
.end method

.method private static efc(Landroid/support/v4/app/am;Ljava/lang/Object;Ljava/lang/Object;Landroid/support/v4/a/u;ZLandroid/support/v4/app/al;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p5, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p5, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    if-eqz p4, :cond_1

    iget-object v0, p5, Landroid/support/v4/app/al;->eOu:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-virtual {p3, v0}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/app/am;->efq(Ljava/lang/Object;Landroid/view/View;)V

    if-eqz p2, :cond_0

    invoke-virtual {p0, p2, v0}, Landroid/support/v4/app/am;->efq(Ljava/lang/Object;Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p5, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private static efd(Landroid/support/v4/a/u;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Landroid/support/v4/a/u;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Landroid/support/v4/a/u;->dSe(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v4/a/u;->dSf(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private static efe(Landroid/support/v4/app/am;Ljava/lang/Object;Landroid/support/v4/app/p;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Landroid/support/v4/app/p;->ech()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/am;->egg(Ljava/util/ArrayList;Landroid/view/View;)V

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/app/am;->efD(Ljava/lang/Object;Ljava/util/ArrayList;)V

    :cond_2
    return-object v0
.end method

.method private static eff(Ljava/util/ArrayList;Landroid/support/v4/a/u;Ljava/util/Collection;)V
    .locals 3

    invoke-virtual {p1}, Landroid/support/v4/a/u;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p1, v1}, Landroid/support/v4/a/u;->dSe(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPx(Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static efg(Ljava/util/ArrayList;I)V
    .locals 2

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static efh(Landroid/support/v4/app/al;Landroid/util/SparseArray;Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iget-object v0, v0, Landroid/support/v4/app/an;->eOR:Landroid/support/v4/app/k;

    invoke-virtual {v0}, Landroid/support/v4/app/k;->ebf()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    const/4 v2, 0x1

    invoke-static {p0, v0, p1, v2, p2}, Landroid/support/v4/app/af;->eeE(Landroid/support/v4/app/al;Landroid/support/v4/app/i;Landroid/util/SparseArray;ZZ)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static efi(Landroid/support/v4/a/u;Landroid/support/v4/a/u;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v4/a/u;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p0, v1}, Landroid/support/v4/a/u;->dSe(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/v4/a/u;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/support/v4/a/u;->dRZ(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static efj(Landroid/support/v4/app/p;Landroid/support/v4/app/p;)Landroid/support/v4/app/am;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ecr()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebE()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebR()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecB()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecw()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {p1}, Landroid/support/v4/app/p;->ecg()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    return-object v2

    :cond_6
    sget-object v1, Landroid/support/v4/app/af;->eNw:Landroid/support/v4/app/am;

    if-eqz v1, :cond_7

    sget-object v1, Landroid/support/v4/app/af;->eNw:Landroid/support/v4/app/am;

    invoke-static {v1, v0}, Landroid/support/v4/app/af;->eeL(Landroid/support/v4/app/am;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v0, Landroid/support/v4/app/af;->eNw:Landroid/support/v4/app/am;

    return-object v0

    :cond_7
    sget-object v1, Landroid/support/v4/app/af;->eNv:Landroid/support/v4/app/am;

    if-eqz v1, :cond_8

    sget-object v1, Landroid/support/v4/app/af;->eNv:Landroid/support/v4/app/am;

    invoke-static {v1, v0}, Landroid/support/v4/app/af;->eeL(Landroid/support/v4/app/am;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Landroid/support/v4/app/af;->eNv:Landroid/support/v4/app/am;

    return-object v0

    :cond_8
    sget-object v0, Landroid/support/v4/app/af;->eNw:Landroid/support/v4/app/am;

    if-nez v0, :cond_9

    sget-object v0, Landroid/support/v4/app/af;->eNv:Landroid/support/v4/app/am;

    if-eqz v0, :cond_a

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid Transition types"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    return-object v2
.end method
