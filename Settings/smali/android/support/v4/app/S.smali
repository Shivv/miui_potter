.class public Landroid/support/v4/app/S;
.super Landroid/support/v4/app/M;
.source "FragmentActivity.java"


# instance fields
.field final eMV:Landroid/os/Handler;

.field eMW:Z

.field eMX:Z

.field eMY:Z

.field eMZ:Landroid/support/v4/a/b;

.field eNa:Z

.field final eNb:Landroid/support/v4/app/Z;

.field eNc:I

.field eNd:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/support/v4/app/M;-><init>()V

    new-instance v0, Landroid/support/v4/app/s;

    invoke-direct {v0, p0}, Landroid/support/v4/app/s;-><init>(Landroid/support/v4/app/S;)V

    iput-object v0, p0, Landroid/support/v4/app/S;->eMV:Landroid/os/Handler;

    new-instance v0, Landroid/support/v4/app/I;

    invoke-direct {v0, p0}, Landroid/support/v4/app/I;-><init>(Landroid/support/v4/app/S;)V

    invoke-static {v0}, Landroid/support/v4/app/Z;->eeA(Landroid/support/v4/app/C;)Landroid/support/v4/app/Z;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    iput-boolean v1, p0, Landroid/support/v4/app/S;->eMW:Z

    iput-boolean v1, p0, Landroid/support/v4/app/S;->eNa:Z

    return-void
.end method

.method private static edL(Landroid/support/v4/app/ar;Landroid/arch/lifecycle/Lifecycle$State;)V
    .locals 3

    invoke-virtual {p0}, Landroid/support/v4/app/ar;->ehW()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/support/v4/app/p;->eKL:Landroid/arch/lifecycle/b;

    invoke-virtual {v2, p1}, Landroid/arch/lifecycle/b;->epS(Landroid/arch/lifecycle/Lifecycle$State;)V

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebI()Landroid/support/v4/app/ar;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/app/S;->edL(Landroid/support/v4/app/ar;Landroid/arch/lifecycle/Lifecycle$State;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/app/M;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "Local FragmentActivity "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, " State:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mCreated="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v1, p0, Landroid/support/v4/app/S;->eMX:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v1, "mResumed="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v1, p0, Landroid/support/v4/app/S;->eNd:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v1, " mStopped="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v1, p0, Landroid/support/v4/app/S;->eMW:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v1, " mReallyStopped="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v1, p0, Landroid/support/v4/app/S;->eNa:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    iget-object v1, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v1, v0, p2, p3, p4}, Landroid/support/v4/app/Z;->eel(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eem()Landroid/support/v4/app/ar;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/app/ar;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method public edD()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method edE(Z)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Landroid/support/v4/app/S;->eNa:Z

    if-nez v0, :cond_1

    iput-boolean v1, p0, Landroid/support/v4/app/S;->eNa:Z

    iput-boolean p1, p0, Landroid/support/v4/app/S;->eMY:Z

    iget-object v0, p0, Landroid/support/v4/app/S;->eMV:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/support/v4/app/S;->edM()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->edW()V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Z;->edY(Z)V

    goto :goto_0
.end method

.method public edF()V
    .locals 0

    invoke-virtual {p0}, Landroid/support/v4/app/S;->invalidateOptionsMenu()V

    return-void
.end method

.method public edG()Landroid/support/v4/app/ar;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eem()Landroid/support/v4/app/ar;

    move-result-object v0

    return-object v0
.end method

.method protected edH(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0, v0, p1, p2}, Landroid/support/v4/app/M;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected edI()V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eey()V

    return-void
.end method

.method public edJ(Landroid/support/v4/app/p;)V
    .locals 0

    return-void
.end method

.method final edK(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/app/Z;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method edM()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    iget-boolean v1, p0, Landroid/support/v4/app/S;->eMY:Z

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Z;->edY(Z)V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->ees()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eep()V

    shr-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_2

    add-int/lit8 v1, v0, -0x1

    iget-object v0, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/b;->dSp(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v2, v1}, Landroid/support/v4/a/b;->remove(I)V

    if-nez v0, :cond_0

    const-string/jumbo v0, "FragmentActivity"

    const-string/jumbo v1, "Activity result delivered for unknown Fragment."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Z;->edZ(Ljava/lang/String;)Landroid/support/v4/app/p;

    move-result-object v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "FragmentActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Activity result no fragment exists for who: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const v0, 0xffff

    and-int/2addr v0, p1

    invoke-virtual {v1, v0, p2, p3}, Landroid/support/v4/app/p;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/M;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 4

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eem()Landroid/support/v4/app/ar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/ar;->eig()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x19

    if-gt v2, v3, :cond_0

    return-void

    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/ar;->egB()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/M;->onBackPressed()V

    :cond_2
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/M;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Z;->eex(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Z;->eek(Landroid/support/v4/app/p;)V

    invoke-super {p0, p1}, Landroid/support/v4/app/M;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/support/v4/app/S;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/J;

    if-eqz v0, :cond_0

    iget-object v3, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    iget-object v4, v0, Landroid/support/v4/app/J;->eMv:Landroid/support/v4/a/a;

    invoke-virtual {v3, v4}, Landroid/support/v4/app/Z;->eee(Landroid/support/v4/a/a;)V

    :cond_0
    if-eqz p1, :cond_2

    const-string/jumbo v3, "android:support:fragments"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    if-eqz v0, :cond_4

    iget-object v0, v0, Landroid/support/v4/app/J;->eMu:Landroid/support/v4/app/E;

    :goto_0
    invoke-virtual {v4, v3, v0}, Landroid/support/v4/app/Z;->eev(Landroid/os/Parcelable;Landroid/support/v4/app/E;)V

    const-string/jumbo v0, "android:support:next_request_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "android:support:next_request_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/S;->eNc:I

    const-string/jumbo v0, "android:support:request_indicies"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    const-string/jumbo v0, "android:support:request_fragment_who"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_1

    if-nez v3, :cond_5

    :cond_1
    const-string/jumbo v0, "FragmentActivity"

    const-string/jumbo v1, "Invalid requestCode mapping in savedInstanceState."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    if-nez v0, :cond_3

    new-instance v0, Landroid/support/v4/a/b;

    invoke-direct {v0}, Landroid/support/v4/a/b;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    iput v2, p0, Landroid/support/v4/app/S;->eNc:I

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eeh()V

    return-void

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    array-length v0, v1

    array-length v4, v3

    if-ne v0, v4, :cond_1

    new-instance v0, Landroid/support/v4/a/b;

    array-length v4, v1

    invoke-direct {v0, v4}, Landroid/support/v4/a/b;-><init>(I)V

    iput-object v0, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    move v0, v2

    :goto_1
    array-length v4, v1

    if-ge v0, v4, :cond_2

    iget-object v4, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    aget v5, v1, v0

    aget-object v6, v3, v0

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/a/b;->dSr(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 3

    if-nez p1, :cond_0

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/M;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    iget-object v1, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {p0}, Landroid/support/v4/app/S;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Landroid/support/v4/app/Z;->eef(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/M;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/app/M;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/M;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/M;->onDestroy()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/S;->edE(Z)V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eeo()V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eeb()V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/M;->onLowMemory()V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eeu()V

    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/M;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    return v0

    :sswitch_0
    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/Z;->eej(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :sswitch_1
    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/Z;->eec(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Z;->eez(Z)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/M;->onNewIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eep()V

    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/M;->onPanelClosed(ILandroid/view/Menu;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/Z;->edV(Landroid/view/Menu;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    const/4 v1, 0x2

    invoke-super {p0}, Landroid/support/v4/app/M;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/S;->eNd:Z

    iget-object v0, p0, Landroid/support/v4/app/S;->eMV:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/S;->eMV:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/support/v4/app/S;->edI()V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eeg()V

    return-void
.end method

.method public onPictureInPictureModeChanged(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Z;->edU(Z)V

    return-void
.end method

.method protected onPostResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/M;->onPostResume()V

    iget-object v0, p0, Landroid/support/v4/app/S;->eMV:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Landroid/support/v4/app/S;->edI()V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eew()Z

    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 2

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p0, p2, p3}, Landroid/support/v4/app/S;->edH(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    iget-object v1, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v1, p3}, Landroid/support/v4/app/Z;->eed(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/M;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 4

    const v3, 0xffff

    shr-int/lit8 v0, p1, 0x10

    and-int/2addr v0, v3

    if-eqz v0, :cond_1

    add-int/lit8 v1, v0, -0x1

    iget-object v0, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/b;->dSp(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v2, v1}, Landroid/support/v4/a/b;->remove(I)V

    if-nez v0, :cond_0

    const-string/jumbo v0, "FragmentActivity"

    const-string/jumbo v1, "Activity result delivered for unknown Fragment."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Z;->edZ(Ljava/lang/String;)Landroid/support/v4/app/p;

    move-result-object v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "FragmentActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Activity result no fragment exists for who: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    and-int v0, p1, v3

    invoke-virtual {v1, v0, p2, p3}, Landroid/support/v4/app/p;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/M;->onResume()V

    iget-object v0, p0, Landroid/support/v4/app/S;->eMV:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/S;->eNd:Z

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eew()Z

    return-void
.end method

.method public final onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Landroid/support/v4/app/S;->eMW:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/S;->edE(Z)V

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/S;->edD()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v1}, Landroid/support/v4/app/Z;->eer()Landroid/support/v4/app/E;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v2}, Landroid/support/v4/app/Z;->eea()Landroid/support/v4/a/a;

    move-result-object v2

    if-nez v1, :cond_1

    if-nez v2, :cond_1

    if-nez v0, :cond_1

    return-object v3

    :cond_1
    new-instance v3, Landroid/support/v4/app/J;

    invoke-direct {v3}, Landroid/support/v4/app/J;-><init>()V

    iput-object v0, v3, Landroid/support/v4/app/J;->eMw:Ljava/lang/Object;

    iput-object v1, v3, Landroid/support/v4/app/J;->eMu:Landroid/support/v4/app/E;

    iput-object v2, v3, Landroid/support/v4/app/J;->eMv:Landroid/support/v4/a/a;

    return-object v3
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    const/4 v0, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/M;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/support/v4/app/S;->edG()Landroid/support/v4/app/ar;

    move-result-object v1

    sget-object v2, Landroid/arch/lifecycle/Lifecycle$State;->eWx:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-static {v1, v2}, Landroid/support/v4/app/S;->edL(Landroid/support/v4/app/ar;Landroid/arch/lifecycle/Lifecycle$State;)V

    iget-object v1, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v1}, Landroid/support/v4/app/Z;->een()Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "android:support:fragments"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v1}, Landroid/support/v4/a/b;->size()I

    move-result v1

    if-lez v1, :cond_2

    const-string/jumbo v1, "android:support:next_request_index"

    iget v2, p0, Landroid/support/v4/app/S;->eNc:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v1}, Landroid/support/v4/a/b;->size()I

    move-result v1

    new-array v2, v1, [I

    iget-object v1, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v1}, Landroid/support/v4/a/b;->size()I

    move-result v1

    new-array v3, v1, [Ljava/lang/String;

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v0}, Landroid/support/v4/a/b;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/b;->dSq(I)I

    move-result v0

    aput v0, v2, v1

    iget-object v0, p0, Landroid/support/v4/app/S;->eMZ:Landroid/support/v4/a/b;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/b;->dSo(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "android:support:request_indicies"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string/jumbo v0, "android:support:request_fragment_who"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method protected onStart()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/support/v4/app/M;->onStart()V

    iput-boolean v0, p0, Landroid/support/v4/app/S;->eMW:Z

    iput-boolean v0, p0, Landroid/support/v4/app/S;->eNa:Z

    iget-object v0, p0, Landroid/support/v4/app/S;->eMV:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-boolean v0, p0, Landroid/support/v4/app/S;->eMX:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Landroid/support/v4/app/S;->eMX:Z

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eet()V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eep()V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eew()Z

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->edW()V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->edX()V

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eeq()V

    return-void
.end method

.method public onStateNotSaved()V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eep()V

    return-void
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/support/v4/app/M;->onStop()V

    iput-boolean v2, p0, Landroid/support/v4/app/S;->eMW:Z

    invoke-virtual {p0}, Landroid/support/v4/app/S;->edG()Landroid/support/v4/app/ar;

    move-result-object v0

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$State;->eWx:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-static {v0, v1}, Landroid/support/v4/app/S;->edL(Landroid/support/v4/app/ar;Landroid/arch/lifecycle/Lifecycle$State;)V

    iget-object v0, p0, Landroid/support/v4/app/S;->eMV:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Landroid/support/v4/app/S;->eNb:Landroid/support/v4/app/Z;

    invoke-virtual {v0}, Landroid/support/v4/app/Z;->eei()V

    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/app/S;->eMQ:Z

    if-nez v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    invoke-static {p2}, Landroid/support/v4/app/S;->eeB(I)V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/M;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public bridge synthetic startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/M;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    return-void
.end method

.method public bridge synthetic startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    .locals 0

    invoke-super/range {p0 .. p6}, Landroid/support/v4/app/M;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    return-void
.end method

.method public bridge synthetic startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V
    .locals 0

    invoke-super/range {p0 .. p7}, Landroid/support/v4/app/M;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V

    return-void
.end method
