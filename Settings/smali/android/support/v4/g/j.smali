.class final Landroid/support/v4/g/j;
.super Ljava/lang/Object;
.source "FontsContractCompat.java"

# interfaces
.implements Landroid/support/v4/g/e;


# instance fields
.field final synthetic eSL:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/g/j;->eSL:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public enH(Landroid/graphics/Typeface;)V
    .locals 4

    invoke-static {}, Landroid/support/v4/g/i;->-get0()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-static {}, Landroid/support/v4/g/i;->enD()Landroid/support/v4/a/a;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v4/g/j;->eSL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/support/v4/a/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {}, Landroid/support/v4/g/i;->enD()Landroid/support/v4/a/a;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v4/g/j;->eSL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/support/v4/a/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/g/e;

    invoke-interface {v1, p1}, Landroid/support/v4/g/e;->enl(Ljava/lang/Object;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    return-void
.end method

.method public bridge synthetic enl(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Typeface;

    invoke-virtual {p0, p1}, Landroid/support/v4/g/j;->enH(Landroid/graphics/Typeface;)V

    return-void
.end method
