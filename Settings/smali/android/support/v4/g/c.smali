.class final Landroid/support/v4/g/c;
.super Ljava/lang/Object;
.source "FontsContractCompat.java"

# interfaces
.implements Landroid/support/v4/g/e;


# instance fields
.field final synthetic eSt:Ljava/lang/ref/WeakReference;

.field final synthetic eSu:Landroid/widget/TextView;

.field final synthetic eSv:I


# direct methods
.method constructor <init>(Ljava/lang/ref/WeakReference;Landroid/widget/TextView;I)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/g/c;->eSt:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Landroid/support/v4/g/c;->eSu:Landroid/widget/TextView;

    iput p3, p0, Landroid/support/v4/g/c;->eSv:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic enl(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Typeface;

    invoke-virtual {p0, p1}, Landroid/support/v4/g/c;->enm(Landroid/graphics/Typeface;)V

    return-void
.end method

.method public enm(Landroid/graphics/Typeface;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/g/c;->eSt:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/g/c;->eSu:Landroid/widget/TextView;

    iget v1, p0, Landroid/support/v4/g/c;->eSv:I

    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_0
    return-void
.end method
