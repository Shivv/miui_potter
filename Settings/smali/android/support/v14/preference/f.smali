.class Landroid/support/v14/preference/f;
.super Landroid/support/v7/widget/d;
.source "PreferenceFragment.java"


# instance fields
.field private dEY:Z

.field final synthetic dEZ:Landroid/support/v14/preference/b;

.field private dFa:Landroid/graphics/drawable/Drawable;

.field private dFb:I


# direct methods
.method private constructor <init>(Landroid/support/v14/preference/b;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v14/preference/f;->dEZ:Landroid/support/v14/preference/b;

    invoke-direct {p0}, Landroid/support/v7/widget/d;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v14/preference/f;->dEY:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v14/preference/b;Landroid/support/v14/preference/f;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v14/preference/f;-><init>(Landroid/support/v14/preference/b;)V

    return-void
.end method

.method private djH(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p2, p1}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    instance-of v2, v0, Landroid/support/v7/preference/l;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/support/v7/preference/l;

    invoke-virtual {v0}, Landroid/support/v7/preference/l;->dme()Z

    move-result v0

    :goto_0
    if-nez v0, :cond_1

    return v1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Landroid/support/v14/preference/f;->dEY:Z

    invoke-virtual {p2, p1}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    instance-of v2, v0, Landroid/support/v7/preference/l;

    if-eqz v2, :cond_3

    check-cast v0, Landroid/support/v7/preference/l;

    invoke-virtual {v0}, Landroid/support/v7/preference/l;->dmd()Z

    move-result v0

    :cond_2
    :goto_1
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public Cm(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v14/preference/f;->dFa:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v2

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_2

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {p0, v4, p2}, Landroid/support/v14/preference/f;->djH(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v5

    iget-object v5, p0, Landroid/support/v14/preference/f;->dFa:Landroid/graphics/drawable/Drawable;

    iget v6, p0, Landroid/support/v14/preference/f;->dFb:I

    add-int/2addr v6, v4

    invoke-virtual {v5, v1, v4, v3, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v4, p0, Landroid/support/v14/preference/f;->dFa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public cdd(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V
    .locals 1

    invoke-direct {p0, p2, p3}, Landroid/support/v14/preference/f;->djH(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v14/preference/f;->dFb:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    :cond_0
    return-void
.end method

.method public djE(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v14/preference/f;->dEY:Z

    return-void
.end method

.method public djF(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Landroid/support/v14/preference/f;->dFb:I

    :goto_0
    iput-object p1, p0, Landroid/support/v14/preference/f;->dFa:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Landroid/support/v14/preference/f;->dEZ:Landroid/support/v14/preference/b;

    invoke-static {v0}, Landroid/support/v14/preference/b;->djw(Landroid/support/v14/preference/b;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->dqJ()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v14/preference/f;->dFb:I

    goto :goto_0
.end method

.method public djG(I)V
    .locals 1

    iput p1, p0, Landroid/support/v14/preference/f;->dFb:I

    iget-object v0, p0, Landroid/support/v14/preference/f;->dEZ:Landroid/support/v14/preference/b;

    invoke-static {v0}, Landroid/support/v14/preference/b;->djw(Landroid/support/v14/preference/b;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->dqJ()V

    return-void
.end method
