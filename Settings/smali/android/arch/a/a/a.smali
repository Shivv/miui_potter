.class abstract Landroid/arch/a/a/a;
.super Ljava/lang/Object;
.source "SafeIterableMap.java"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field eWK:Landroid/arch/a/a/e;

.field eWL:Landroid/arch/a/a/e;


# direct methods
.method constructor <init>(Landroid/arch/a/a/e;Landroid/arch/a/a/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Landroid/arch/a/a/a;->eWK:Landroid/arch/a/a/e;

    iput-object p1, p0, Landroid/arch/a/a/a;->eWL:Landroid/arch/a/a/e;

    return-void
.end method

.method private eql()Landroid/arch/a/a/e;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/arch/a/a/a;->eWL:Landroid/arch/a/a/e;

    iget-object v1, p0, Landroid/arch/a/a/a;->eWK:Landroid/arch/a/a/e;

    if-ne v0, v1, :cond_1

    :cond_0
    return-object v2

    :cond_1
    iget-object v0, p0, Landroid/arch/a/a/a;->eWK:Landroid/arch/a/a/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/arch/a/a/a;->eWL:Landroid/arch/a/a/e;

    invoke-virtual {p0, v0}, Landroid/arch/a/a/a;->eqm(Landroid/arch/a/a/e;)Landroid/arch/a/a/e;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract eqm(Landroid/arch/a/a/e;)Landroid/arch/a/a/e;
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Landroid/arch/a/a/a;->eWL:Landroid/arch/a/a/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Landroid/arch/a/a/a;->next()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/util/Map$Entry;
    .locals 2

    iget-object v0, p0, Landroid/arch/a/a/a;->eWL:Landroid/arch/a/a/e;

    invoke-direct {p0}, Landroid/arch/a/a/a;->eql()Landroid/arch/a/a/e;

    move-result-object v1

    iput-object v1, p0, Landroid/arch/a/a/a;->eWL:Landroid/arch/a/a/e;

    return-object v0
.end method
