.class public Landroid/arch/lifecycle/b;
.super Landroid/arch/lifecycle/h;
.source "LifecycleRegistry.java"


# instance fields
.field private eWk:Z

.field private eWl:Ljava/util/ArrayList;

.field private eWm:Landroid/arch/a/a/c;

.field private eWn:I

.field private eWo:Z

.field private eWp:Landroid/arch/lifecycle/Lifecycle$State;

.field private final eWq:Landroid/arch/lifecycle/f;


# direct methods
.method public constructor <init>(Landroid/arch/lifecycle/f;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/arch/lifecycle/h;-><init>()V

    new-instance v0, Landroid/arch/a/a/c;

    invoke-direct {v0}, Landroid/arch/a/a/c;-><init>()V

    iput-object v0, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    iput v1, p0, Landroid/arch/lifecycle/b;->eWn:I

    iput-boolean v1, p0, Landroid/arch/lifecycle/b;->eWk:Z

    iput-boolean v1, p0, Landroid/arch/lifecycle/b;->eWo:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/arch/lifecycle/b;->eWl:Ljava/util/ArrayList;

    iput-object p1, p0, Landroid/arch/lifecycle/b;->eWq:Landroid/arch/lifecycle/f;

    sget-object v0, Landroid/arch/lifecycle/Lifecycle$State;->eWw:Landroid/arch/lifecycle/Lifecycle$State;

    iput-object v0, p0, Landroid/arch/lifecycle/b;->eWp:Landroid/arch/lifecycle/Lifecycle$State;

    return-void
.end method

.method private epT(Landroid/arch/lifecycle/Lifecycle$State;)V
    .locals 1

    iget-object v0, p0, Landroid/arch/lifecycle/b;->eWl:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private epU()V
    .locals 5

    iget-object v0, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    invoke-virtual {v0}, Landroid/arch/a/a/c;->eqr()Landroid/arch/a/a/h;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-boolean v0, p0, Landroid/arch/lifecycle/b;->eWo:Z

    if-nez v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/arch/lifecycle/e;

    :goto_0
    iget-object v3, v1, Landroid/arch/lifecycle/e;->eWt:Landroid/arch/lifecycle/Lifecycle$State;

    iget-object v4, p0, Landroid/arch/lifecycle/b;->eWp:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-virtual {v3, v4}, Landroid/arch/lifecycle/Lifecycle$State;->compareTo(Ljava/lang/Enum;)I

    move-result v3

    if-gez v3, :cond_0

    iget-boolean v3, p0, Landroid/arch/lifecycle/b;->eWo:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/arch/a/a/c;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, Landroid/arch/lifecycle/e;->eWt:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-direct {p0, v3}, Landroid/arch/lifecycle/b;->epT(Landroid/arch/lifecycle/Lifecycle$State;)V

    iget-object v3, p0, Landroid/arch/lifecycle/b;->eWq:Landroid/arch/lifecycle/f;

    iget-object v4, v1, Landroid/arch/lifecycle/e;->eWt:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-static {v4}, Landroid/arch/lifecycle/b;->epX(Landroid/arch/lifecycle/Lifecycle$State;)Landroid/arch/lifecycle/Lifecycle$Event;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/arch/lifecycle/e;->eqj(Landroid/arch/lifecycle/f;Landroid/arch/lifecycle/Lifecycle$Event;)V

    invoke-direct {p0}, Landroid/arch/lifecycle/b;->epW()V

    goto :goto_0
.end method

.method private epV()V
    .locals 5

    iget-object v0, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    invoke-virtual {v0}, Landroid/arch/a/a/c;->eqn()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-boolean v0, p0, Landroid/arch/lifecycle/b;->eWo:Z

    if-nez v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/arch/lifecycle/e;

    :goto_0
    iget-object v3, v1, Landroid/arch/lifecycle/e;->eWt:Landroid/arch/lifecycle/Lifecycle$State;

    iget-object v4, p0, Landroid/arch/lifecycle/b;->eWp:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-virtual {v3, v4}, Landroid/arch/lifecycle/Lifecycle$State;->compareTo(Ljava/lang/Enum;)I

    move-result v3

    if-lez v3, :cond_0

    iget-boolean v3, p0, Landroid/arch/lifecycle/b;->eWo:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/arch/a/a/c;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, Landroid/arch/lifecycle/e;->eWt:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-static {v3}, Landroid/arch/lifecycle/b;->epY(Landroid/arch/lifecycle/Lifecycle$State;)Landroid/arch/lifecycle/Lifecycle$Event;

    move-result-object v3

    invoke-static {v3}, Landroid/arch/lifecycle/b;->eqb(Landroid/arch/lifecycle/Lifecycle$Event;)Landroid/arch/lifecycle/Lifecycle$State;

    move-result-object v4

    invoke-direct {p0, v4}, Landroid/arch/lifecycle/b;->epT(Landroid/arch/lifecycle/Lifecycle$State;)V

    iget-object v4, p0, Landroid/arch/lifecycle/b;->eWq:Landroid/arch/lifecycle/f;

    invoke-virtual {v1, v4, v3}, Landroid/arch/lifecycle/e;->eqj(Landroid/arch/lifecycle/f;Landroid/arch/lifecycle/Lifecycle$Event;)V

    invoke-direct {p0}, Landroid/arch/lifecycle/b;->epW()V

    goto :goto_0
.end method

.method private epW()V
    .locals 2

    iget-object v0, p0, Landroid/arch/lifecycle/b;->eWl:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/arch/lifecycle/b;->eWl:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method private static epX(Landroid/arch/lifecycle/Lifecycle$State;)Landroid/arch/lifecycle/Lifecycle$Event;
    .locals 3

    sget-object v0, Landroid/arch/lifecycle/i;->eWJ:[I

    invoke-virtual {p0}, Landroid/arch/lifecycle/Lifecycle$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected state value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWB:Landroid/arch/lifecycle/Lifecycle$Event;

    return-object v0

    :pswitch_1
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWF:Landroid/arch/lifecycle/Lifecycle$Event;

    return-object v0

    :pswitch_2
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWE:Landroid/arch/lifecycle/Lifecycle$Event;

    return-object v0

    :pswitch_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private static epY(Landroid/arch/lifecycle/Lifecycle$State;)Landroid/arch/lifecycle/Lifecycle$Event;
    .locals 3

    sget-object v0, Landroid/arch/lifecycle/i;->eWJ:[I

    invoke-virtual {p0}, Landroid/arch/lifecycle/Lifecycle$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected state value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_1
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWA:Landroid/arch/lifecycle/Lifecycle$Event;

    return-object v0

    :pswitch_2
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWG:Landroid/arch/lifecycle/Lifecycle$Event;

    return-object v0

    :pswitch_3
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWC:Landroid/arch/lifecycle/Lifecycle$Event;

    return-object v0

    :pswitch_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private eqa()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    invoke-virtual {v0}, Landroid/arch/a/a/c;->size()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    invoke-virtual {v0}, Landroid/arch/a/a/c;->eqq()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/arch/lifecycle/e;

    iget-object v3, v0, Landroid/arch/lifecycle/e;->eWt:Landroid/arch/lifecycle/Lifecycle$State;

    iget-object v0, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    invoke-virtual {v0}, Landroid/arch/a/a/c;->eqp()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/arch/lifecycle/e;

    iget-object v0, v0, Landroid/arch/lifecycle/e;->eWt:Landroid/arch/lifecycle/Lifecycle$State;

    if-eq v3, v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    return v1

    :cond_2
    iget-object v3, p0, Landroid/arch/lifecycle/b;->eWp:Landroid/arch/lifecycle/Lifecycle$State;

    if-ne v3, v0, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static eqb(Landroid/arch/lifecycle/Lifecycle$Event;)Landroid/arch/lifecycle/Lifecycle$State;
    .locals 3

    sget-object v0, Landroid/arch/lifecycle/i;->eWI:[I

    invoke-virtual {p0}, Landroid/arch/lifecycle/Lifecycle$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected event value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$State;->eWx:Landroid/arch/lifecycle/Lifecycle$State;

    return-object v0

    :pswitch_2
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$State;->eWu:Landroid/arch/lifecycle/Lifecycle$State;

    return-object v0

    :pswitch_3
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$State;->eWz:Landroid/arch/lifecycle/Lifecycle$State;

    return-object v0

    :pswitch_4
    sget-object v0, Landroid/arch/lifecycle/Lifecycle$State;->eWy:Landroid/arch/lifecycle/Lifecycle$State;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method private eqc()V
    .locals 3

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-direct {p0}, Landroid/arch/lifecycle/b;->eqa()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Landroid/arch/lifecycle/b;->eWo:Z

    return-void

    :cond_1
    iput-boolean v2, p0, Landroid/arch/lifecycle/b;->eWo:Z

    iget-object v1, p0, Landroid/arch/lifecycle/b;->eWp:Landroid/arch/lifecycle/Lifecycle$State;

    iget-object v0, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    invoke-virtual {v0}, Landroid/arch/a/a/c;->eqq()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/arch/lifecycle/e;

    iget-object v0, v0, Landroid/arch/lifecycle/e;->eWt:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-virtual {v1, v0}, Landroid/arch/lifecycle/Lifecycle$State;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_2

    :goto_1
    iget-object v0, p0, Landroid/arch/lifecycle/b;->eWm:Landroid/arch/a/a/c;

    invoke-virtual {v0}, Landroid/arch/a/a/c;->eqp()Ljava/util/Map$Entry;

    move-result-object v0

    iget-boolean v1, p0, Landroid/arch/lifecycle/b;->eWo:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/arch/lifecycle/b;->eWp:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/arch/lifecycle/e;

    iget-object v0, v0, Landroid/arch/lifecycle/e;->eWt:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-virtual {v1, v0}, Landroid/arch/lifecycle/Lifecycle$State;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Landroid/arch/lifecycle/b;->epU()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Landroid/arch/lifecycle/b;->epV()V

    goto :goto_1
.end method

.method static eqd(Landroid/arch/lifecycle/Lifecycle$State;Landroid/arch/lifecycle/Lifecycle$State;)Landroid/arch/lifecycle/Lifecycle$State;
    .locals 1

    if-nez p1, :cond_1

    :goto_0
    move-object p1, p0

    :cond_0
    return-object p1

    :cond_1
    invoke-virtual {p1, p0}, Landroid/arch/lifecycle/Lifecycle$State;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public epS(Landroid/arch/lifecycle/Lifecycle$State;)V
    .locals 0

    iput-object p1, p0, Landroid/arch/lifecycle/b;->eWp:Landroid/arch/lifecycle/Lifecycle$State;

    return-void
.end method

.method public epZ(Landroid/arch/lifecycle/Lifecycle$Event;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/arch/lifecycle/b;->eqb(Landroid/arch/lifecycle/Lifecycle$Event;)Landroid/arch/lifecycle/Lifecycle$State;

    move-result-object v0

    iput-object v0, p0, Landroid/arch/lifecycle/b;->eWp:Landroid/arch/lifecycle/Lifecycle$State;

    iget-boolean v0, p0, Landroid/arch/lifecycle/b;->eWk:Z

    if-eqz v0, :cond_1

    :cond_0
    iput-boolean v2, p0, Landroid/arch/lifecycle/b;->eWo:Z

    return-void

    :cond_1
    iget v0, p0, Landroid/arch/lifecycle/b;->eWn:I

    if-nez v0, :cond_0

    iput-boolean v2, p0, Landroid/arch/lifecycle/b;->eWk:Z

    invoke-direct {p0}, Landroid/arch/lifecycle/b;->eqc()V

    iput-boolean v1, p0, Landroid/arch/lifecycle/b;->eWk:Z

    return-void
.end method
