.class public Landroid/arch/lifecycle/d;
.super Landroid/app/Fragment;
.source "ReportFragment.java"


# instance fields
.field private eWr:Landroid/arch/lifecycle/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method private eqe(Landroid/arch/lifecycle/c;)V
    .locals 0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Landroid/arch/lifecycle/c;->onCreate()V

    goto :goto_0
.end method

.method private eqf(Landroid/arch/lifecycle/c;)V
    .locals 0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Landroid/arch/lifecycle/c;->onStart()V

    goto :goto_0
.end method

.method private eqg(Landroid/arch/lifecycle/Lifecycle$Event;)V
    .locals 1

    invoke-virtual {p0}, Landroid/arch/lifecycle/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Landroid/arch/lifecycle/a;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/arch/lifecycle/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/arch/lifecycle/a;

    invoke-interface {v0}, Landroid/arch/lifecycle/a;->getLifecycle()Landroid/arch/lifecycle/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/b;->epZ(Landroid/arch/lifecycle/Lifecycle$Event;)V

    goto :goto_0
.end method

.method private eqh(Landroid/arch/lifecycle/c;)V
    .locals 0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Landroid/arch/lifecycle/c;->onResume()V

    goto :goto_0
.end method

.method public static eqi(Landroid/app/Activity;)V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "android.arch.lifecycle.LifecycleDispatcher.report_fragment_tag"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    new-instance v2, Landroid/arch/lifecycle/d;

    invoke-direct {v2}, Landroid/arch/lifecycle/d;-><init>()V

    const-string/jumbo v3, "android.arch.lifecycle.LifecycleDispatcher.report_fragment_tag"

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/arch/lifecycle/d;->eWr:Landroid/arch/lifecycle/c;

    invoke-direct {p0, v0}, Landroid/arch/lifecycle/d;->eqe(Landroid/arch/lifecycle/c;)V

    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWB:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-direct {p0, v0}, Landroid/arch/lifecycle/d;->eqg(Landroid/arch/lifecycle/Lifecycle$Event;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWA:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-direct {p0, v0}, Landroid/arch/lifecycle/d;->eqg(Landroid/arch/lifecycle/Lifecycle$Event;)V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/arch/lifecycle/d;->eWr:Landroid/arch/lifecycle/c;

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWC:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-direct {p0, v0}, Landroid/arch/lifecycle/d;->eqg(Landroid/arch/lifecycle/Lifecycle$Event;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v0, p0, Landroid/arch/lifecycle/d;->eWr:Landroid/arch/lifecycle/c;

    invoke-direct {p0, v0}, Landroid/arch/lifecycle/d;->eqh(Landroid/arch/lifecycle/c;)V

    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWE:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-direct {p0, v0}, Landroid/arch/lifecycle/d;->eqg(Landroid/arch/lifecycle/Lifecycle$Event;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    iget-object v0, p0, Landroid/arch/lifecycle/d;->eWr:Landroid/arch/lifecycle/c;

    invoke-direct {p0, v0}, Landroid/arch/lifecycle/d;->eqf(Landroid/arch/lifecycle/c;)V

    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWF:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-direct {p0, v0}, Landroid/arch/lifecycle/d;->eqg(Landroid/arch/lifecycle/Lifecycle$Event;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWG:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-direct {p0, v0}, Landroid/arch/lifecycle/d;->eqg(Landroid/arch/lifecycle/Lifecycle$Event;)V

    return-void
.end method
