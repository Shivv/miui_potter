.class public final enum Landroid/arch/lifecycle/Lifecycle$Event;
.super Ljava/lang/Enum;
.source "Lifecycle.java"


# static fields
.field public static final enum eWA:Landroid/arch/lifecycle/Lifecycle$Event;

.field public static final enum eWB:Landroid/arch/lifecycle/Lifecycle$Event;

.field public static final enum eWC:Landroid/arch/lifecycle/Lifecycle$Event;

.field private static final synthetic eWD:[Landroid/arch/lifecycle/Lifecycle$Event;

.field public static final enum eWE:Landroid/arch/lifecycle/Lifecycle$Event;

.field public static final enum eWF:Landroid/arch/lifecycle/Lifecycle$Event;

.field public static final enum eWG:Landroid/arch/lifecycle/Lifecycle$Event;

.field public static final enum eWH:Landroid/arch/lifecycle/Lifecycle$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/arch/lifecycle/Lifecycle$Event;

    const-string/jumbo v1, "ON_CREATE"

    invoke-direct {v0, v1, v3}, Landroid/arch/lifecycle/Lifecycle$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWB:Landroid/arch/lifecycle/Lifecycle$Event;

    new-instance v0, Landroid/arch/lifecycle/Lifecycle$Event;

    const-string/jumbo v1, "ON_START"

    invoke-direct {v0, v1, v4}, Landroid/arch/lifecycle/Lifecycle$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWF:Landroid/arch/lifecycle/Lifecycle$Event;

    new-instance v0, Landroid/arch/lifecycle/Lifecycle$Event;

    const-string/jumbo v1, "ON_RESUME"

    invoke-direct {v0, v1, v5}, Landroid/arch/lifecycle/Lifecycle$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWE:Landroid/arch/lifecycle/Lifecycle$Event;

    new-instance v0, Landroid/arch/lifecycle/Lifecycle$Event;

    const-string/jumbo v1, "ON_PAUSE"

    invoke-direct {v0, v1, v6}, Landroid/arch/lifecycle/Lifecycle$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWC:Landroid/arch/lifecycle/Lifecycle$Event;

    new-instance v0, Landroid/arch/lifecycle/Lifecycle$Event;

    const-string/jumbo v1, "ON_STOP"

    invoke-direct {v0, v1, v7}, Landroid/arch/lifecycle/Lifecycle$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWG:Landroid/arch/lifecycle/Lifecycle$Event;

    new-instance v0, Landroid/arch/lifecycle/Lifecycle$Event;

    const-string/jumbo v1, "ON_DESTROY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Landroid/arch/lifecycle/Lifecycle$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWA:Landroid/arch/lifecycle/Lifecycle$Event;

    new-instance v0, Landroid/arch/lifecycle/Lifecycle$Event;

    const-string/jumbo v1, "ON_ANY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Landroid/arch/lifecycle/Lifecycle$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWH:Landroid/arch/lifecycle/Lifecycle$Event;

    const/4 v0, 0x7

    new-array v0, v0, [Landroid/arch/lifecycle/Lifecycle$Event;

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWB:Landroid/arch/lifecycle/Lifecycle$Event;

    aput-object v1, v0, v3

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWF:Landroid/arch/lifecycle/Lifecycle$Event;

    aput-object v1, v0, v4

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWE:Landroid/arch/lifecycle/Lifecycle$Event;

    aput-object v1, v0, v5

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWC:Landroid/arch/lifecycle/Lifecycle$Event;

    aput-object v1, v0, v6

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWG:Landroid/arch/lifecycle/Lifecycle$Event;

    aput-object v1, v0, v7

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWA:Landroid/arch/lifecycle/Lifecycle$Event;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWH:Landroid/arch/lifecycle/Lifecycle$Event;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWD:[Landroid/arch/lifecycle/Lifecycle$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/arch/lifecycle/Lifecycle$Event;
    .locals 1

    const-class v0, Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/arch/lifecycle/Lifecycle$Event;

    return-object v0
.end method

.method public static values()[Landroid/arch/lifecycle/Lifecycle$Event;
    .locals 1

    sget-object v0, Landroid/arch/lifecycle/Lifecycle$Event;->eWD:[Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/arch/lifecycle/Lifecycle$Event;

    return-object v0
.end method
