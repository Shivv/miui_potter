.class public Lorg/apache/thrift/protocol/e;
.super Lorg/apache/thrift/protocol/a;
.source "TBinaryProtocol.java"


# static fields
.field private static final eXL:Lorg/apache/thrift/protocol/c;


# instance fields
.field protected eXI:Z

.field private eXJ:[B

.field private eXK:[B

.field private eXM:[B

.field private eXN:[B

.field private eXO:[B

.field private eXP:[B

.field private eXQ:[B

.field private eXR:[B

.field protected readLength_:I

.field protected strictRead_:Z

.field protected strictWrite_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/thrift/protocol/c;

    invoke-direct {v0}, Lorg/apache/thrift/protocol/c;-><init>()V

    sput-object v0, Lorg/apache/thrift/protocol/e;->eXL:Lorg/apache/thrift/protocol/c;

    return-void
.end method

.method public constructor <init>(Lorg/apache/thrift/transport/a;ZZ)V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lorg/apache/thrift/protocol/a;-><init>(Lorg/apache/thrift/transport/a;)V

    iput-boolean v0, p0, Lorg/apache/thrift/protocol/e;->strictRead_:Z

    iput-boolean v1, p0, Lorg/apache/thrift/protocol/e;->strictWrite_:Z

    iput-boolean v0, p0, Lorg/apache/thrift/protocol/e;->eXI:Z

    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/thrift/protocol/e;->eXO:[B

    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/thrift/protocol/e;->eXR:[B

    new-array v0, v3, [B

    iput-object v0, p0, Lorg/apache/thrift/protocol/e;->eXQ:[B

    new-array v0, v4, [B

    iput-object v0, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    new-array v0, v1, [B

    iput-object v0, p0, Lorg/apache/thrift/protocol/e;->eXN:[B

    new-array v0, v2, [B

    iput-object v0, p0, Lorg/apache/thrift/protocol/e;->eXJ:[B

    new-array v0, v3, [B

    iput-object v0, p0, Lorg/apache/thrift/protocol/e;->eXP:[B

    new-array v0, v4, [B

    iput-object v0, p0, Lorg/apache/thrift/protocol/e;->eXK:[B

    iput-boolean p2, p0, Lorg/apache/thrift/protocol/e;->strictRead_:Z

    iput-boolean p3, p0, Lorg/apache/thrift/protocol/e;->strictWrite_:Z

    return-void
.end method

.method private esh([BII)I
    .locals 1

    invoke-virtual {p0, p3}, Lorg/apache/thrift/protocol/e;->esi(I)V

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/thrift/transport/a;->erk([BII)I

    move-result v0

    return v0
.end method


# virtual methods
.method public erA()V
    .locals 0

    return-void
.end method

.method public erB()Lorg/apache/thrift/protocol/h;
    .locals 3

    new-instance v0, Lorg/apache/thrift/protocol/h;

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esb()B

    move-result v1

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esf()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lorg/apache/thrift/protocol/h;-><init>(BI)V

    return-object v0
.end method

.method public erC()Ljava/nio/ByteBuffer;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esf()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->esi(I)V

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v1}, Lorg/apache/thrift/transport/a;->erl()I

    move-result v1

    if-ge v1, v0, :cond_0

    new-array v1, v0, [B

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Lorg/apache/thrift/transport/a;->erk([BII)I

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v1}, Lorg/apache/thrift/transport/a;->erm()[B

    move-result-object v1

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2}, Lorg/apache/thrift/transport/a;->ero()I

    move-result v2

    invoke-static {v1, v2, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2, v0}, Lorg/apache/thrift/transport/a;->erp(I)V

    return-object v1
.end method

.method public erD()V
    .locals 0

    return-void
.end method

.method public erE(S)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXR:[B

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXR:[B

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXR:[B

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v3, v2}, Lorg/apache/thrift/transport/a;->ern([BII)V

    return-void
.end method

.method public erF(J)V
    .locals 9

    const/16 v7, 0x8

    const/4 v6, 0x0

    const-wide/16 v4, 0xff

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    const/16 v1, 0x38

    shr-long v2, p1, v1

    and-long/2addr v2, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    int-to-byte v1, v1

    aput-byte v1, v0, v6

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    const/16 v1, 0x30

    shr-long v2, p1, v1

    and-long/2addr v2, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    const/16 v1, 0x28

    shr-long v2, p1, v1

    and-long/2addr v2, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x2

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    const/16 v1, 0x20

    shr-long v2, p1, v1

    and-long/2addr v2, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x3

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    const/16 v1, 0x18

    shr-long v2, p1, v1

    and-long/2addr v2, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x4

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    const/16 v1, 0x10

    shr-long v2, p1, v1

    and-long/2addr v2, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x5

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    shr-long v2, p1, v7

    and-long/2addr v2, v4

    long-to-int v1, v2

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x6

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    and-long v2, v4, p1

    long-to-int v1, v2

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x7

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXM:[B

    invoke-virtual {v0, v1, v6, v7}, Lorg/apache/thrift/transport/a;->ern([BII)V

    return-void
.end method

.method public erG()D
    .locals 2

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->erw()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public erH(D)V
    .locals 3

    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/apache/thrift/protocol/e;->erF(J)V

    return-void
.end method

.method public erJ(Ljava/nio/ByteBuffer;)V
    .locals 5

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erW(I)V

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v1, v2, v3, v0}, Lorg/apache/thrift/transport/a;->ern([BII)V

    return-void
.end method

.method public erK()V
    .locals 0

    return-void
.end method

.method public erL()V
    .locals 0

    return-void
.end method

.method public erM()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esb()B

    move-result v1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public erN()V
    .locals 0

    return-void
.end method

.method public erO(B)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXO:[B

    int-to-byte v1, p1

    aput-byte v1, v0, v3

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXO:[B

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v3, v2}, Lorg/apache/thrift/transport/a;->ern([BII)V

    return-void
.end method

.method public erP()V
    .locals 0

    return-void
.end method

.method public erQ()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erO(B)V

    return-void
.end method

.method public erR()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esf()I

    move-result v0

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v1}, Lorg/apache/thrift/transport/a;->erl()I

    move-result v1

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->esj(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2}, Lorg/apache/thrift/transport/a;->erm()[B

    move-result-object v2

    iget-object v3, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v3}, Lorg/apache/thrift/transport/a;->ero()I

    move-result v3

    const-string/jumbo v4, "UTF-8"

    invoke-direct {v1, v2, v3, v0, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2, v0}, Lorg/apache/thrift/transport/a;->erp(I)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v0, Lorg/apache/thrift/TException;

    const-string/jumbo v1, "JVM DOES NOT SUPPORT UTF-8"

    invoke-direct {v0, v1}, Lorg/apache/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public erS()Lorg/apache/thrift/protocol/c;
    .locals 1

    sget-object v0, Lorg/apache/thrift/protocol/e;->eXL:Lorg/apache/thrift/protocol/c;

    return-object v0
.end method

.method public erT(Lorg/apache/thrift/protocol/i;)V
    .locals 1

    iget-byte v0, p1, Lorg/apache/thrift/protocol/i;->type:B

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erO(B)V

    iget-short v0, p1, Lorg/apache/thrift/protocol/i;->eXX:S

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erE(S)V

    return-void
.end method

.method public erU()V
    .locals 0

    return-void
.end method

.method public erV()Lorg/apache/thrift/protocol/d;
    .locals 4

    new-instance v0, Lorg/apache/thrift/protocol/d;

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esb()B

    move-result v1

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esb()B

    move-result v2

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esf()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/thrift/protocol/d;-><init>(BBI)V

    return-object v0
.end method

.method public erW(I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXQ:[B

    shr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXQ:[B

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXQ:[B

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x2

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXQ:[B

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    int-to-byte v1, v1

    const/4 v2, 0x3

    aput-byte v1, v0, v2

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXQ:[B

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v3, v2}, Lorg/apache/thrift/transport/a;->ern([BII)V

    return-void
.end method

.method public erX()Lorg/apache/thrift/protocol/g;
    .locals 3

    new-instance v0, Lorg/apache/thrift/protocol/g;

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esb()B

    move-result v1

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esf()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lorg/apache/thrift/protocol/g;-><init>(BI)V

    return-object v0
.end method

.method public erY(Lorg/apache/thrift/protocol/h;)V
    .locals 1

    iget-byte v0, p1, Lorg/apache/thrift/protocol/h;->eXV:B

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erO(B)V

    iget v0, p1, Lorg/apache/thrift/protocol/h;->eXW:I

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erW(I)V

    return-void
.end method

.method public erZ()V
    .locals 0

    return-void
.end method

.method public eru(Z)V
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erO(B)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public erv()V
    .locals 0

    return-void
.end method

.method public erw()J
    .locals 8

    const/4 v0, 0x0

    const/16 v7, 0x8

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXK:[B

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2}, Lorg/apache/thrift/transport/a;->erl()I

    move-result v2

    if-ge v2, v7, :cond_0

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXK:[B

    invoke-direct {p0, v2, v0, v7}, Lorg/apache/thrift/protocol/e;->esh([BII)I

    :goto_0
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x30

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x2

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x28

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x3

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x20

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x4

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x18

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x5

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    const/16 v6, 0x10

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x6

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    shl-long/2addr v4, v7

    or-long/2addr v2, v4

    add-int/lit8 v0, v0, 0x7

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-long v0, v0

    or-long/2addr v0, v2

    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v0}, Lorg/apache/thrift/transport/a;->erm()[B

    move-result-object v1

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v0}, Lorg/apache/thrift/transport/a;->ero()I

    move-result v0

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2, v7}, Lorg/apache/thrift/transport/a;->erp(I)V

    goto :goto_0
.end method

.method public erx(Lorg/apache/thrift/protocol/c;)V
    .locals 0

    return-void
.end method

.method public ery(Ljava/lang/String;)V
    .locals 4

    :try_start_0
    const-string/jumbo v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Lorg/apache/thrift/protocol/e;->erW(I)V

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lorg/apache/thrift/transport/a;->ern([BII)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lorg/apache/thrift/TException;

    const-string/jumbo v1, "JVM DOES NOT SUPPORT UTF-8"

    invoke-direct {v0, v1}, Lorg/apache/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public erz()S
    .locals 4

    const/4 v0, 0x0

    const/4 v3, 0x2

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXJ:[B

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2}, Lorg/apache/thrift/transport/a;->erl()I

    move-result v2

    if-ge v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXJ:[B

    invoke-direct {p0, v2, v0, v3}, Lorg/apache/thrift/protocol/e;->esh([BII)I

    :goto_0
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    add-int/lit8 v0, v0, 0x1

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v2

    int-to-short v0, v0

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v0}, Lorg/apache/thrift/transport/a;->erm()[B

    move-result-object v1

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v0}, Lorg/apache/thrift/transport/a;->ero()I

    move-result v0

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2, v3}, Lorg/apache/thrift/transport/a;->erp(I)V

    goto :goto_0
.end method

.method public esa()Lorg/apache/thrift/protocol/i;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->esb()B

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/e;->erz()S

    move-result v0

    :cond_0
    new-instance v2, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v3, ""

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    return-object v2
.end method

.method public esb()B
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v0}, Lorg/apache/thrift/transport/a;->erl()I

    move-result v0

    if-ge v0, v2, :cond_0

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXN:[B

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/thrift/protocol/e;->esh([BII)I

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXN:[B

    aget-byte v0, v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v0}, Lorg/apache/thrift/transport/a;->erm()[B

    move-result-object v0

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v1}, Lorg/apache/thrift/transport/a;->ero()I

    move-result v1

    aget-byte v0, v0, v1

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v1, v2}, Lorg/apache/thrift/transport/a;->erp(I)V

    return v0
.end method

.method public esc(Lorg/apache/thrift/protocol/d;)V
    .locals 1

    iget-byte v0, p1, Lorg/apache/thrift/protocol/d;->eXH:B

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erO(B)V

    iget-byte v0, p1, Lorg/apache/thrift/protocol/d;->eXF:B

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erO(B)V

    iget v0, p1, Lorg/apache/thrift/protocol/d;->eXG:I

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erW(I)V

    return-void
.end method

.method public esd(Lorg/apache/thrift/protocol/g;)V
    .locals 1

    iget-byte v0, p1, Lorg/apache/thrift/protocol/g;->eXT:B

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erO(B)V

    iget v0, p1, Lorg/apache/thrift/protocol/g;->eXU:I

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/e;->erW(I)V

    return-void
.end method

.method public ese()V
    .locals 0

    return-void
.end method

.method public esf()I
    .locals 4

    const/4 v0, 0x0

    const/4 v3, 0x4

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXP:[B

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2}, Lorg/apache/thrift/transport/a;->erl()I

    move-result v2

    if-ge v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXP:[B

    invoke-direct {p0, v2, v0, v3}, Lorg/apache/thrift/protocol/e;->esh([BII)I

    :goto_0
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    add-int/lit8 v3, v0, 0x2

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x3

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v2

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v0}, Lorg/apache/thrift/transport/a;->erm()[B

    move-result-object v1

    iget-object v0, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v0}, Lorg/apache/thrift/transport/a;->ero()I

    move-result v0

    iget-object v2, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2, v3}, Lorg/apache/thrift/transport/a;->erp(I)V

    goto :goto_0
.end method

.method public esg(I)V
    .locals 1

    iput p1, p0, Lorg/apache/thrift/protocol/e;->readLength_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/thrift/protocol/e;->eXI:Z

    return-void
.end method

.method protected esi(I)V
    .locals 3

    if-ltz p1, :cond_1

    iget-boolean v0, p0, Lorg/apache/thrift/protocol/e;->eXI:Z

    if-nez v0, :cond_2

    :cond_0
    return-void

    :cond_1
    new-instance v0, Lorg/apache/thrift/TException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Negative length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget v0, p0, Lorg/apache/thrift/protocol/e;->readLength_:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/thrift/protocol/e;->readLength_:I

    iget v0, p0, Lorg/apache/thrift/protocol/e;->readLength_:I

    if-gez v0, :cond_0

    new-instance v0, Lorg/apache/thrift/TException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Message length exceeded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public esj(I)Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/thrift/protocol/e;->esi(I)V

    new-array v0, p1, [B

    iget-object v1, p0, Lorg/apache/thrift/protocol/e;->eXz:Lorg/apache/thrift/transport/a;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p1}, Lorg/apache/thrift/transport/a;->erk([BII)I

    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v0, Lorg/apache/thrift/TException;

    const-string/jumbo v1, "JVM DOES NOT SUPPORT UTF-8"

    invoke-direct {v0, v1}, Lorg/apache/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
