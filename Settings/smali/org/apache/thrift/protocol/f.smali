.class public Lorg/apache/thrift/protocol/f;
.super Ljava/lang/Object;
.source "TProtocolUtil.java"


# static fields
.field private static eXS:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x7fffffff

    sput v0, Lorg/apache/thrift/protocol/f;->eXS:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static esk(Lorg/apache/thrift/protocol/a;B)V
    .locals 1

    sget v0, Lorg/apache/thrift/protocol/f;->eXS:I

    invoke-static {p0, p1, v0}, Lorg/apache/thrift/protocol/f;->esl(Lorg/apache/thrift/protocol/a;BI)V

    return-void
.end method

.method public static esl(Lorg/apache/thrift/protocol/a;BI)V
    .locals 4

    const/4 v0, 0x0

    if-lez p2, :cond_0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :cond_0
    new-instance v0, Lorg/apache/thrift/TException;

    const-string/jumbo v1, "Maximum skip depth exceeded"

    invoke-direct {v0, v1}, Lorg/apache/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erM()Z

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->esb()B

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erz()S

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->esf()I

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erw()J

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erG()D

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erC()Ljava/nio/ByteBuffer;

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erS()Lorg/apache/thrift/protocol/c;

    :goto_1
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->esa()Lorg/apache/thrift/protocol/i;

    move-result-object v0

    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eqz v1, :cond_1

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    add-int/lit8 v1, p2, -0x1

    invoke-static {p0, v0, v1}, Lorg/apache/thrift/protocol/f;->esl(Lorg/apache/thrift/protocol/a;BI)V

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erA()V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erU()V

    goto :goto_0

    :pswitch_9
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erV()Lorg/apache/thrift/protocol/d;

    move-result-object v1

    :goto_2
    iget v2, v1, Lorg/apache/thrift/protocol/d;->eXG:I

    if-lt v0, v2, :cond_2

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erP()V

    goto :goto_0

    :cond_2
    iget-byte v2, v1, Lorg/apache/thrift/protocol/d;->eXH:B

    add-int/lit8 v3, p2, -0x1

    invoke-static {p0, v2, v3}, Lorg/apache/thrift/protocol/f;->esl(Lorg/apache/thrift/protocol/a;BI)V

    iget-byte v2, v1, Lorg/apache/thrift/protocol/d;->eXF:B

    add-int/lit8 v3, p2, -0x1

    invoke-static {p0, v2, v3}, Lorg/apache/thrift/protocol/f;->esl(Lorg/apache/thrift/protocol/a;BI)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_a
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erB()Lorg/apache/thrift/protocol/h;

    move-result-object v1

    :goto_3
    iget v2, v1, Lorg/apache/thrift/protocol/h;->eXW:I

    if-lt v0, v2, :cond_3

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erK()V

    goto :goto_0

    :cond_3
    iget-byte v2, v1, Lorg/apache/thrift/protocol/h;->eXV:B

    add-int/lit8 v3, p2, -0x1

    invoke-static {p0, v2, v3}, Lorg/apache/thrift/protocol/f;->esl(Lorg/apache/thrift/protocol/a;BI)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :pswitch_b
    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erX()Lorg/apache/thrift/protocol/g;

    move-result-object v1

    :goto_4
    iget v2, v1, Lorg/apache/thrift/protocol/g;->eXU:I

    if-lt v0, v2, :cond_4

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/a;->erv()V

    goto :goto_0

    :cond_4
    iget-byte v2, v1, Lorg/apache/thrift/protocol/g;->eXT:B

    add-int/lit8 v3, p2, -0x1

    invoke-static {p0, v2, v3}, Lorg/apache/thrift/protocol/f;->esl(Lorg/apache/thrift/protocol/a;BI)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
