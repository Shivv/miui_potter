.class public Lorg/apache/thrift/protocol/b;
.super Lorg/apache/thrift/protocol/e;
.source "XmPushTBinaryProtocol.java"


# static fields
.field private static eXA:I

.field private static eXB:I

.field private static eXC:I

.field private static eXD:I

.field private static eXE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x2710

    sput v0, Lorg/apache/thrift/protocol/b;->eXB:I

    sput v0, Lorg/apache/thrift/protocol/b;->eXA:I

    sput v0, Lorg/apache/thrift/protocol/b;->eXD:I

    const/high16 v0, 0xa00000

    sput v0, Lorg/apache/thrift/protocol/b;->eXC:I

    const/high16 v0, 0x6400000

    sput v0, Lorg/apache/thrift/protocol/b;->eXE:I

    return-void
.end method

.method public constructor <init>(Lorg/apache/thrift/transport/a;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/thrift/protocol/e;-><init>(Lorg/apache/thrift/transport/a;ZZ)V

    return-void
.end method


# virtual methods
.method public erB()Lorg/apache/thrift/protocol/h;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/b;->esb()B

    move-result v0

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/b;->esf()I

    move-result v1

    sget v2, Lorg/apache/thrift/protocol/b;->eXD:I

    if-gt v1, v2, :cond_0

    new-instance v2, Lorg/apache/thrift/protocol/h;

    invoke-direct {v2, v0, v1}, Lorg/apache/thrift/protocol/h;-><init>(BI)V

    return-object v2

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Thrift set size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " out of range!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v2, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method public erC()Ljava/nio/ByteBuffer;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/b;->esf()I

    move-result v0

    sget v1, Lorg/apache/thrift/protocol/b;->eXE:I

    if-gt v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/b;->esi(I)V

    iget-object v1, p0, Lorg/apache/thrift/protocol/b;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v1}, Lorg/apache/thrift/transport/a;->erl()I

    move-result v1

    if-ge v1, v0, :cond_1

    new-array v1, v0, [B

    iget-object v2, p0, Lorg/apache/thrift/protocol/b;->eXz:Lorg/apache/thrift/transport/a;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Lorg/apache/thrift/transport/a;->erk([BII)I

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v1, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Thrift binary size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " out of range!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x3

    invoke-direct {v1, v2, v0}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(ILjava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lorg/apache/thrift/protocol/b;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v1}, Lorg/apache/thrift/transport/a;->erm()[B

    move-result-object v1

    iget-object v2, p0, Lorg/apache/thrift/protocol/b;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2}, Lorg/apache/thrift/transport/a;->ero()I

    move-result v2

    invoke-static {v1, v2, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/thrift/protocol/b;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2, v0}, Lorg/apache/thrift/transport/a;->erp(I)V

    return-object v1
.end method

.method public erR()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/b;->esf()I

    move-result v0

    sget v1, Lorg/apache/thrift/protocol/b;->eXC:I

    if-gt v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/thrift/protocol/b;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v1}, Lorg/apache/thrift/transport/a;->erl()I

    move-result v1

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v0}, Lorg/apache/thrift/protocol/b;->esj(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v1, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Thrift string size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " out of range!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x3

    invoke-direct {v1, v2, v0}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(ILjava/lang/String;)V

    throw v1

    :cond_1
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/thrift/protocol/b;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2}, Lorg/apache/thrift/transport/a;->erm()[B

    move-result-object v2

    iget-object v3, p0, Lorg/apache/thrift/protocol/b;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v3}, Lorg/apache/thrift/transport/a;->ero()I

    move-result v3

    const-string/jumbo v4, "UTF-8"

    invoke-direct {v1, v2, v3, v0, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iget-object v2, p0, Lorg/apache/thrift/protocol/b;->eXz:Lorg/apache/thrift/transport/a;

    invoke-virtual {v2, v0}, Lorg/apache/thrift/transport/a;->erp(I)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v0, Lorg/apache/thrift/TException;

    const-string/jumbo v1, "JVM DOES NOT SUPPORT UTF-8"

    invoke-direct {v0, v1}, Lorg/apache/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public erV()Lorg/apache/thrift/protocol/d;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/b;->esb()B

    move-result v0

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/b;->esb()B

    move-result v1

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/b;->esf()I

    move-result v2

    sget v3, Lorg/apache/thrift/protocol/b;->eXB:I

    if-gt v2, v3, :cond_0

    new-instance v3, Lorg/apache/thrift/protocol/d;

    invoke-direct {v3, v0, v1, v2}, Lorg/apache/thrift/protocol/d;-><init>(BBI)V

    return-object v3

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Thrift map size "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " out of range!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v2, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method public erX()Lorg/apache/thrift/protocol/g;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/b;->esb()B

    move-result v0

    invoke-virtual {p0}, Lorg/apache/thrift/protocol/b;->esf()I

    move-result v1

    sget v2, Lorg/apache/thrift/protocol/b;->eXA:I

    if-gt v1, v2, :cond_0

    new-instance v2, Lorg/apache/thrift/protocol/g;

    invoke-direct {v2, v0, v1}, Lorg/apache/thrift/protocol/g;-><init>(BI)V

    return-object v2

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Thrift list size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " out of range!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v2, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(ILjava/lang/String;)V

    throw v0
.end method
