.class public Lorg/apache/thrift/f;
.super Ljava/lang/Object;
.source "TSerializer.java"


# instance fields
.field private eYc:Lorg/apache/thrift/protocol/a;

.field private final eYd:Ljava/io/ByteArrayOutputStream;

.field private final eYe:Lorg/apache/thrift/transport/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    new-instance v0, Lorg/apache/thrift/protocol/TBinaryProtocol$Factory;

    invoke-direct {v0}, Lorg/apache/thrift/protocol/TBinaryProtocol$Factory;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/thrift/f;-><init>(Lorg/apache/thrift/protocol/TProtocolFactory;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/thrift/protocol/TProtocolFactory;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lorg/apache/thrift/f;->eYd:Ljava/io/ByteArrayOutputStream;

    new-instance v0, Lorg/apache/thrift/transport/b;

    iget-object v1, p0, Lorg/apache/thrift/f;->eYd:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Lorg/apache/thrift/transport/b;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lorg/apache/thrift/f;->eYe:Lorg/apache/thrift/transport/b;

    iget-object v0, p0, Lorg/apache/thrift/f;->eYe:Lorg/apache/thrift/transport/b;

    invoke-interface {p1, v0}, Lorg/apache/thrift/protocol/TProtocolFactory;->ert(Lorg/apache/thrift/transport/a;)Lorg/apache/thrift/protocol/a;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/thrift/f;->eYc:Lorg/apache/thrift/protocol/a;

    return-void
.end method


# virtual methods
.method public esH(Lorg/apache/thrift/TBase;)[B
    .locals 1

    iget-object v0, p0, Lorg/apache/thrift/f;->eYd:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    iget-object v0, p0, Lorg/apache/thrift/f;->eYc:Lorg/apache/thrift/protocol/a;

    invoke-interface {p1, v0}, Lorg/apache/thrift/TBase;->cTu(Lorg/apache/thrift/protocol/a;)V

    iget-object v0, p0, Lorg/apache/thrift/f;->eYd:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
