.class public Lorg/apache/thrift/transport/c;
.super Lorg/apache/thrift/transport/a;
.source "TMemoryBuffer.java"


# instance fields
.field private eXu:Lorg/apache/thrift/a;

.field private eXv:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Lorg/apache/thrift/transport/a;-><init>()V

    new-instance v0, Lorg/apache/thrift/a;

    invoke-direct {v0, p1}, Lorg/apache/thrift/a;-><init>(I)V

    iput-object v0, p0, Lorg/apache/thrift/transport/c;->eXu:Lorg/apache/thrift/a;

    return-void
.end method


# virtual methods
.method public ern([BII)V
    .locals 1

    iget-object v0, p0, Lorg/apache/thrift/transport/c;->eXu:Lorg/apache/thrift/a;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/thrift/a;->write([BII)V

    return-void
.end method

.method public erq()I
    .locals 1

    iget-object v0, p0, Lorg/apache/thrift/transport/c;->eXu:Lorg/apache/thrift/a;

    invoke-virtual {v0}, Lorg/apache/thrift/a;->size()I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 3

    iget-object v0, p0, Lorg/apache/thrift/transport/c;->eXu:Lorg/apache/thrift/a;

    invoke-virtual {v0}, Lorg/apache/thrift/a;->get()[B

    move-result-object v0

    iget-object v1, p0, Lorg/apache/thrift/transport/c;->eXu:Lorg/apache/thrift/a;

    invoke-virtual {v1}, Lorg/apache/thrift/a;->esn()I

    move-result v1

    iget v2, p0, Lorg/apache/thrift/transport/c;->eXv:I

    sub-int/2addr v1, v2

    if-gt p3, v1, :cond_0

    :goto_0
    if-gtz p3, :cond_1

    :goto_1
    return p3

    :cond_0
    iget-object v1, p0, Lorg/apache/thrift/transport/c;->eXu:Lorg/apache/thrift/a;

    invoke-virtual {v1}, Lorg/apache/thrift/a;->esn()I

    move-result v1

    iget v2, p0, Lorg/apache/thrift/transport/c;->eXv:I

    sub-int p3, v1, v2

    goto :goto_0

    :cond_1
    iget v1, p0, Lorg/apache/thrift/transport/c;->eXv:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lorg/apache/thrift/transport/c;->eXv:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/thrift/transport/c;->eXv:I

    goto :goto_1
.end method
