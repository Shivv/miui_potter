.class public abstract Lmiui/external/a;
.super Landroid/content/ContextWrapper;
.source "SourceFile"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# instance fields
.field private eWY:Lmiui/external/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public eqs()Lmiui/external/b;
    .locals 1

    iget-object v0, p0, Lmiui/external/a;->eWY:Lmiui/external/b;

    return-object v0
.end method

.method eqt(Lmiui/external/b;)V
    .locals 0

    iput-object p1, p0, Lmiui/external/a;->eWY:Lmiui/external/b;

    invoke-virtual {p0, p1}, Lmiui/external/a;->attachBaseContext(Landroid/content/Context;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lmiui/external/a;->eWY:Lmiui/external/b;

    invoke-virtual {v0, p1}, Lmiui/external/b;->eqv(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate()V
    .locals 1

    iget-object v0, p0, Lmiui/external/a;->eWY:Lmiui/external/b;

    invoke-virtual {v0}, Lmiui/external/b;->j()V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    iget-object v0, p0, Lmiui/external/a;->eWY:Lmiui/external/b;

    invoke-virtual {v0}, Lmiui/external/b;->k()V

    return-void
.end method

.method public onTerminate()V
    .locals 1

    iget-object v0, p0, Lmiui/external/a;->eWY:Lmiui/external/b;

    invoke-virtual {v0}, Lmiui/external/b;->l()V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    iget-object v0, p0, Lmiui/external/a;->eWY:Lmiui/external/b;

    invoke-virtual {v0, p1}, Lmiui/external/b;->equ(I)V

    return-void
.end method

.method public registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V
    .locals 1

    iget-object v0, p0, Lmiui/external/a;->eWY:Lmiui/external/b;

    invoke-virtual {v0, p1}, Lmiui/external/b;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    return-void
.end method

.method public unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V
    .locals 1

    iget-object v0, p0, Lmiui/external/a;->eWY:Lmiui/external/b;

    invoke-virtual {v0, p1}, Lmiui/external/b;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    return-void
.end method
