.class final Lcom/android/settingslib/u;
.super Ljava/lang/Object;
.source "MiuiRestrictedSwitchPreference.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic cQr:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field final synthetic cQs:Landroid/view/View;

.field final synthetic cQt:Lmiui/widget/SlidingButton;


# direct methods
.method constructor <init>(Lcom/android/settingslib/MiuiRestrictedSwitchPreference;Landroid/view/View;Lmiui/widget/SlidingButton;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/u;->cQr:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object p2, p0, Lcom/android/settingslib/u;->cQs:Landroid/view/View;

    iput-object p3, p0, Lcom/android/settingslib/u;->cQt:Lmiui/widget/SlidingButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/u;->cQs:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/u;->cQr:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settingslib/u;->cQr:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, v1, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cQo:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    iget-object v0, p0, Lcom/android/settingslib/u;->cQr:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isChecked()Z

    move-result v0

    if-eq p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/u;->cQt:Lmiui/widget/SlidingButton;

    xor-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    :cond_1
    return-void
.end method
