.class public Lcom/android/settingslib/RestrictedPreference;
.super Lcom/android/settingslib/TwoTargetPreference;
.source "RestrictedPreference.java"


# instance fields
.field cPI:Lcom/android/settingslib/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settingslib/RestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    sget v0, Landroid/support/v7/preference/a;->dHP:I

    const v1, 0x101008e

    invoke-static {p1, v0, v1}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settingslib/RestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settingslib/RestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settingslib/TwoTargetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Lcom/android/settingslib/m;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/m;-><init>(Landroid/content/Context;Landroid/support/v7/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settingslib/RestrictedPreference;->cPI:Lcom/android/settingslib/m;

    return-void
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settingslib/TwoTargetPreference;->al(Landroid/support/v7/preference/l;)V

    iget-object v0, p0, Lcom/android/settingslib/RestrictedPreference;->cPI:Lcom/android/settingslib/m;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/m;->cpW(Landroid/support/v7/preference/l;)V

    sget v0, Lcom/android/settingslib/g;->cLd:I

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/RestrictedPreference;->cpT()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected ayU()I
    .locals 1

    sget v0, Lcom/android/settingslib/h;->cLk:I

    return v0
.end method

.method protected ayV()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settingslib/RestrictedPreference;->cpT()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected cpS(Landroid/support/v7/preference/k;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/RestrictedPreference;->cPI:Lcom/android/settingslib/m;

    invoke-virtual {v0}, Lcom/android/settingslib/m;->cpY()V

    invoke-super {p0, p1}, Lcom/android/settingslib/TwoTargetPreference;->cpS(Landroid/support/v7/preference/k;)V

    return-void
.end method

.method public cpT()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/RestrictedPreference;->cPI:Lcom/android/settingslib/m;

    invoke-virtual {v0}, Lcom/android/settingslib/m;->cpZ()Z

    move-result v0

    return v0
.end method

.method public performClick()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/RestrictedPreference;->cPI:Lcom/android/settingslib/m;

    invoke-virtual {v0}, Lcom/android/settingslib/m;->performClick()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/android/settingslib/TwoTargetPreference;->performClick()V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/RestrictedPreference;->cpT()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/RestrictedPreference;->cPI:Lcom/android/settingslib/m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/m;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settingslib/TwoTargetPreference;->setEnabled(Z)V

    return-void
.end method
