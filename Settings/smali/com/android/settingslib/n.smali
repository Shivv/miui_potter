.class public Lcom/android/settingslib/n;
.super Ljava/lang/Object;
.source "RestrictedLockUtils.java"


# static fields
.field public static final cPO:Lcom/android/settingslib/n;


# instance fields
.field public cPP:Landroid/content/ComponentName;

.field public userId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settingslib/n;

    invoke-direct {v0}, Lcom/android/settingslib/n;-><init>()V

    sput-object v0, Lcom/android/settingslib/n;->cPO:Lcom/android/settingslib/n;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    const/16 v0, -0x2710

    iput v0, p0, Lcom/android/settingslib/n;->userId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/ComponentName;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    const/16 v0, -0x2710

    iput v0, p0, Lcom/android/settingslib/n;->userId:I

    iput-object p1, p0, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    iput p2, p0, Lcom/android/settingslib/n;->userId:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_0

    return v3

    :cond_0
    instance-of v0, p1, Lcom/android/settingslib/n;

    if-nez v0, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/android/settingslib/n;

    iget v0, p0, Lcom/android/settingslib/n;->userId:I

    iget v1, p1, Lcom/android/settingslib/n;->userId:I

    if-eq v0, v1, :cond_2

    return v2

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    if-nez v0, :cond_4

    :cond_3
    return v3

    :cond_4
    iget-object v0, p0, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    iget-object v1, p1, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_5
    return v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "EnforcedAdmin{component="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/settingslib/n;->userId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
