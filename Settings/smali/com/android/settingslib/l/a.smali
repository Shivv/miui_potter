.class public Lcom/android/settingslib/l/a;
.super Ljava/lang/Object;
.source "DisplayDensityUtils.java"


# static fields
.field public static final cJX:I

.field private static final cJZ:I

.field private static final cKb:[I

.field private static final cKc:[I


# instance fields
.field private final cJY:I

.field private final cKa:I

.field private final cKd:[Ljava/lang/String;

.field private final cKe:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget v0, Lcom/android/settingslib/i;->cMQ:I

    sput v0, Lcom/android/settingslib/l/a;->cJX:I

    sget v0, Lcom/android/settingslib/i;->cMP:I

    sput v0, Lcom/android/settingslib/l/a;->cJZ:I

    new-array v0, v3, [I

    sget v1, Lcom/android/settingslib/i;->cMT:I

    aput v1, v0, v2

    sput-object v0, Lcom/android/settingslib/l/a;->cKc:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    sget v1, Lcom/android/settingslib/i;->cMS:I

    aput v1, v0, v2

    sget v1, Lcom/android/settingslib/i;->cMU:I

    aput v1, v0, v3

    sget v1, Lcom/android/settingslib/i;->cMR:I

    const/4 v2, 0x2

    aput v1, v0, v2

    sput-object v0, Lcom/android/settingslib/l/a;->cKb:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 15

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/settingslib/l/a;->cpI(I)I

    move-result v6

    if-gtz v6, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/l/a;->cKd:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/l/a;->cKe:[I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settingslib/l/a;->cKa:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settingslib/l/a;->cJY:I

    return-void

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    iget v8, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/4 v0, -0x1

    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    mul-int/lit16 v1, v1, 0xa0

    div-int/lit16 v1, v1, 0x140

    int-to-float v1, v1

    int-to-float v2, v6

    div-float/2addr v1, v2

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v9

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v9, v1

    const v2, 0x3db851ec    # 0.09f

    div-float/2addr v1, v2

    const/4 v2, 0x0

    sget-object v3, Lcom/android/settingslib/l/a;->cKb:[I

    array-length v3, v3

    int-to-float v3, v3

    invoke-static {v1, v2, v3}, Landroid/util/MathUtils;->constrain(FFF)F

    move-result v1

    float-to-int v10, v1

    const/4 v1, 0x0

    sget-object v2, Lcom/android/settingslib/l/a;->cKc:[I

    array-length v2, v2

    int-to-float v2, v2

    const v3, 0x3fd55553

    invoke-static {v3, v1, v2}, Landroid/util/MathUtils;->constrain(FFF)F

    move-result v1

    float-to-int v4, v1

    add-int/lit8 v1, v4, 0x1

    add-int/2addr v1, v10

    new-array v3, v1, [Ljava/lang/String;

    array-length v1, v3

    new-array v2, v1, [I

    const/4 v1, 0x0

    if-lez v4, :cond_2

    int-to-float v5, v4

    const v11, 0x3e199998    # 0.14999998f

    div-float/2addr v11, v5

    add-int/lit8 v4, v4, -0x1

    :goto_0
    if-ltz v4, :cond_2

    int-to-float v5, v6

    add-int/lit8 v12, v4, 0x1

    int-to-float v12, v12

    mul-float/2addr v12, v11

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float v12, v13, v12

    mul-float/2addr v5, v12

    float-to-int v5, v5

    and-int/lit8 v5, v5, -0x2

    if-ne v8, v5, :cond_1

    move v0, v1

    :cond_1
    sget-object v12, Lcom/android/settingslib/l/a;->cKc:[I

    aget v12, v12, v4

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v3, v1

    aput v5, v2, v1

    add-int/lit8 v5, v1, 0x1

    add-int/lit8 v1, v4, -0x1

    move v4, v1

    move v1, v5

    goto :goto_0

    :cond_2
    if-ne v8, v6, :cond_3

    move v0, v1

    :cond_3
    aput v6, v2, v1

    sget v4, Lcom/android/settingslib/l/a;->cJX:I

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    if-lez v10, :cond_5

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v4, v9, v4

    int-to-float v5, v10

    div-float v9, v4, v5

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v10, :cond_5

    int-to-float v5, v6

    add-int/lit8 v11, v4, 0x1

    int-to-float v11, v11

    mul-float/2addr v11, v9

    const/high16 v12, 0x3f800000    # 1.0f

    add-float/2addr v11, v12

    mul-float/2addr v5, v11

    float-to-int v5, v5

    and-int/lit8 v5, v5, -0x2

    if-ne v8, v5, :cond_4

    move v0, v1

    :cond_4
    aput v5, v2, v1

    sget-object v5, Lcom/android/settingslib/l/a;->cKb:[I

    aget v5, v5, v4

    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    add-int/lit8 v5, v1, 0x1

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v5

    goto :goto_1

    :cond_5
    if-ltz v0, :cond_6

    move-object v1, v2

    move-object v2, v3

    :goto_2
    iput v6, p0, Lcom/android/settingslib/l/a;->cKa:I

    iput v0, p0, Lcom/android/settingslib/l/a;->cJY:I

    iput-object v2, p0, Lcom/android/settingslib/l/a;->cKd:[Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settingslib/l/a;->cKe:[I

    return-void

    :cond_6
    array-length v0, v2

    add-int/lit8 v0, v0, 0x1

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    aput v8, v2, v1

    invoke-static {v3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sget v3, Lcom/android/settingslib/l/a;->cJZ:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v8, 0x0

    aput-object v5, v4, v8

    invoke-virtual {v7, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    move v14, v1

    move-object v1, v2

    move-object v2, v0

    move v0, v14

    goto :goto_2
.end method

.method public static cpH(I)V
    .locals 2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    new-instance v1, Lcom/android/settingslib/l/-$Lambda$7BaaBm9YJncNvrLLdpRG1kcEqmE;

    invoke-direct {v1, p0, v0}, Lcom/android/settingslib/l/-$Lambda$7BaaBm9YJncNvrLLdpRG1kcEqmE;-><init>(II)V

    invoke-static {v1}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static cpI(I)I
    .locals 1

    :try_start_0
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/IWindowManager;->getInitialDisplayDensity(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    return v0
.end method

.method public static cpJ(II)V
    .locals 2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    new-instance v1, Lcom/android/settingslib/l/-$Lambda$7BaaBm9YJncNvrLLdpRG1kcEqmE$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/android/settingslib/l/-$Lambda$7BaaBm9YJncNvrLLdpRG1kcEqmE$1;-><init>(III)V

    invoke-static {v1}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic cpK(III)V
    .locals 2

    :try_start_0
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2}, Landroid/view/IWindowManager;->setForcedDisplayDensityForUser(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "DisplayDensityUtils"

    const-string/jumbo v1, "Unable to save forced display density setting"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic cpO(II)V
    .locals 2

    :try_start_0
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Landroid/view/IWindowManager;->clearForcedDisplayDensityForUser(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "DisplayDensityUtils"

    const-string/jumbo v1, "Unable to clear forced display density setting"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public cpG()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/l/a;->cKd:[Ljava/lang/String;

    return-object v0
.end method

.method public cpL()I
    .locals 1

    iget v0, p0, Lcom/android/settingslib/l/a;->cKa:I

    return v0
.end method

.method public cpM()[I
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/l/a;->cKe:[I

    return-object v0
.end method

.method public cpN()I
    .locals 1

    iget v0, p0, Lcom/android/settingslib/l/a;->cJY:I

    return v0
.end method
