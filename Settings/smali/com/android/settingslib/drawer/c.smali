.class Lcom/android/settingslib/drawer/c;
.super Landroid/os/AsyncTask;
.source "SettingsDrawerActivity.java"


# instance fields
.field private final czj:Lcom/android/settingslib/drawer/e;

.field final synthetic czk:Lcom/android/settingslib/drawer/a;


# direct methods
.method public constructor <init>(Lcom/android/settingslib/drawer/a;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settingslib/drawer/c;->czk:Lcom/android/settingslib/drawer/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-static {p1}, Lcom/android/settingslib/drawer/e;->cdW(Landroid/content/Context;)Lcom/android/settingslib/drawer/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/drawer/c;->czj:Lcom/android/settingslib/drawer/e;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settingslib/drawer/c;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/drawer/c;->czj:Lcom/android/settingslib/drawer/e;

    iget-object v1, p0, Lcom/android/settingslib/drawer/c;->czk:Lcom/android/settingslib/drawer/a;

    iget-object v2, p0, Lcom/android/settingslib/drawer/c;->czk:Lcom/android/settingslib/drawer/a;

    invoke-virtual {v2}, Lcom/android/settingslib/drawer/a;->cdU()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/drawer/e;->cdV(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settingslib/drawer/c;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/drawer/c;->czj:Lcom/android/settingslib/drawer/e;

    invoke-static {}, Lcom/android/settingslib/drawer/a;->-get0()Landroid/util/ArraySet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/drawer/e;->cea(Ljava/util/Set;)V

    iget-object v0, p0, Lcom/android/settingslib/drawer/c;->czk:Lcom/android/settingslib/drawer/a;

    invoke-static {v0}, Lcom/android/settingslib/drawer/a;->cdR(Lcom/android/settingslib/drawer/a;)V

    return-void
.end method
