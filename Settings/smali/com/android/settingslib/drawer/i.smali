.class public Lcom/android/settingslib/drawer/i;
.super Ljava/lang/Object;
.source "UserAdapter.java"


# instance fields
.field private final czC:Ljava/lang/String;

.field private final czD:Landroid/os/UserHandle;

.field private final mIcon:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/os/UserHandle;Landroid/os/UserManager;Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/drawer/i;->czD:Landroid/os/UserHandle;

    iget-object v0, p0, Lcom/android/settingslib/drawer/i;->czD:Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v0, Lcom/android/settingslib/i;->cMB:I

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/drawer/i;->czC:Ljava/lang/String;

    const v0, 0x108033b

    invoke-virtual {p3, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-static {p3, v0}, Lcom/android/settingslib/drawer/i;->cer(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/drawer/i;->mIcon:Landroid/graphics/drawable/Drawable;

    return-void

    :cond_0
    iget-object v1, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settingslib/drawer/i;->czC:Ljava/lang/String;

    iget v1, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {p2, v1}, Landroid/os/UserManager;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p2, v1}, Landroid/os/UserManager;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v1, v0}, Lcom/android/internal/util/UserIcons;->getDefaultUserIcon(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private static cer(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 2

    new-instance v0, Lcom/android/settingslib/e/a;

    invoke-static {p0}, Lcom/android/settingslib/e/a;->cnN(Landroid/content/Context;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/android/settingslib/e/a;-><init>(I)V

    invoke-virtual {v0, p1}, Lcom/android/settingslib/e/a;->cnO(Landroid/graphics/drawable/Drawable;)Lcom/android/settingslib/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/e/a;->cnJ()Lcom/android/settingslib/e/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic ces(Lcom/android/settingslib/drawer/i;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/drawer/i;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic cet(Lcom/android/settingslib/drawer/i;)Landroid/os/UserHandle;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/drawer/i;->czD:Landroid/os/UserHandle;

    return-object v0
.end method
