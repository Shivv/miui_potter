.class final Lcom/android/settingslib/wifi/g;
.super Landroid/content/BroadcastReceiver;
.source "WifiTracker.java"


# instance fields
.field final synthetic cCC:Lcom/android/settingslib/wifi/a;


# direct methods
.method constructor <init>(Lcom/android/settingslib/wifi/a;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-static {v1, v3}, Lcom/android/settingslib/wifi/a;->cgu(Lcom/android/settingslib/wifi/a;Z)Z

    :cond_0
    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    const-string/jumbo v1, "wifi_state"

    const/4 v2, 0x4

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settingslib/wifi/a;->cgZ(Lcom/android/settingslib/wifi/a;I)V

    iget-object v0, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgO(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/c;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/settingslib/wifi/c;->sendEmptyMessage(I)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string/jumbo v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgO(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/c;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/settingslib/wifi/c;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_4
    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    iget-object v1, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-static {v1}, Lcom/android/settingslib/wifi/a;->cgJ(Lcom/android/settingslib/wifi/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    iget-object v1, v1, Lcom/android/settingslib/wifi/a;->mMainHandler:Lcom/android/settingslib/wifi/WifiTracker$MainHandler;

    invoke-virtual {v1, v3}, Lcom/android/settingslib/wifi/WifiTracker$MainHandler;->sendEmptyMessage(I)Z

    iget-object v1, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-static {v1}, Lcom/android/settingslib/wifi/a;->cgO(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/c;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Lcom/android/settingslib/wifi/c;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    iget-object v0, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgO(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/c;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/settingslib/wifi/c;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_5
    const-string/jumbo v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgr(Lcom/android/settingslib/wifi/a;)Landroid/net/ConnectivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-static {v1}, Lcom/android/settingslib/wifi/a;->cgU(Lcom/android/settingslib/wifi/a;)Landroid/net/wifi/WifiManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/wifi/g;->cCC:Lcom/android/settingslib/wifi/a;

    invoke-static {v1}, Lcom/android/settingslib/wifi/a;->cgO(Lcom/android/settingslib/wifi/a;)Lcom/android/settingslib/wifi/c;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Lcom/android/settingslib/wifi/c;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0
.end method
