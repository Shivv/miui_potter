.class public Lcom/android/settingslib/wifi/l;
.super Ljava/lang/Object;
.source "WifiTrackerFactory.java"


# static fields
.field private static cDh:Lcom/android/settingslib/wifi/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cir(Landroid/content/Context;Lcom/android/settingslib/wifi/e;Landroid/os/Looper;ZZZ)Lcom/android/settingslib/wifi/a;
    .locals 7

    sget-object v0, Lcom/android/settingslib/wifi/l;->cDh:Lcom/android/settingslib/wifi/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settingslib/wifi/l;->cDh:Lcom/android/settingslib/wifi/a;

    return-object v0

    :cond_0
    new-instance v0, Lcom/android/settingslib/wifi/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/settingslib/wifi/a;-><init>(Landroid/content/Context;Lcom/android/settingslib/wifi/e;Landroid/os/Looper;ZZZ)V

    return-object v0
.end method

.method public static setTestingWifiTracker(Lcom/android/settingslib/wifi/a;)V
    .locals 0

    sput-object p0, Lcom/android/settingslib/wifi/l;->cDh:Lcom/android/settingslib/wifi/a;

    return-void
.end method
