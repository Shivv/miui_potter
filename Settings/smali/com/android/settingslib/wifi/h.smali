.class final Lcom/android/settingslib/wifi/h;
.super Landroid/net/wifi/WifiNetworkScoreCache$CacheListener;
.source "WifiTracker.java"


# instance fields
.field final synthetic cCD:Lcom/android/settingslib/wifi/a;


# direct methods
.method constructor <init>(Lcom/android/settingslib/wifi/a;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/wifi/h;->cCD:Lcom/android/settingslib/wifi/a;

    invoke-direct {p0, p2}, Landroid/net/wifi/WifiNetworkScoreCache$CacheListener;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public networkCacheUpdated(Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/wifi/h;->cCD:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgs(Lcom/android/settingslib/wifi/a;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/h;->cCD:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgX(Lcom/android/settingslib/wifi/a;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v1

    return-void

    :cond_0
    monitor-exit v1

    const-string/jumbo v0, "WifiTracker"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "WifiTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Score cache was updated with networks: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/wifi/h;->cCD:Lcom/android/settingslib/wifi/a;

    invoke-static {v0}, Lcom/android/settingslib/wifi/a;->cgR(Lcom/android/settingslib/wifi/a;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
