.class public Lcom/android/settingslib/b/b;
.super Ljava/lang/Object;
.source "ApplicationsState.java"


# instance fields
.field cAF:Ljava/util/ArrayList;

.field cAG:Ljava/util/Comparator;

.field final synthetic cAH:Lcom/android/settingslib/b/a;

.field cAI:Z

.field final cAJ:Ljava/lang/Object;

.field cAK:Lcom/android/settingslib/b/i;

.field cAL:Z

.field cAM:Z

.field final cAN:Lcom/android/settingslib/b/f;

.field cAO:Z

.field cAP:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/android/settingslib/b/a;Lcom/android/settingslib/b/f;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/b/b;->cAJ:Ljava/lang/Object;

    iput-object p2, p0, Lcom/android/settingslib/b/b;->cAN:Lcom/android/settingslib/b/f;

    return-void
.end method


# virtual methods
.method public cfs()Ljava/util/ArrayList;
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v1, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v2, v2, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method cft()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    invoke-static {v0}, Lcom/android/settingslib/b/a;->cfr(Lcom/android/settingslib/b/a;)V

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public cfu(Lcom/android/settingslib/b/i;Ljava/util/Comparator;Z)Ljava/util/ArrayList;
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/settingslib/b/b;->cAJ:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v2, v0, Lcom/android/settingslib/b/a;->cAh:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAh:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/b/b;->cAI:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/b/b;->cAL:Z

    iput-object p1, p0, Lcom/android/settingslib/b/b;->cAK:Lcom/android/settingslib/b/i;

    iput-object p2, p0, Lcom/android/settingslib/b/b;->cAG:Ljava/util/Comparator;

    iput-boolean p3, p0, Lcom/android/settingslib/b/b;->cAM:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/b/b;->cAP:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/android/settingslib/b/d;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/android/settingslib/b/d;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v3, v3, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    invoke-virtual {v3, v0}, Lcom/android/settingslib/b/d;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v1

    return-object v4

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public cfv()V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v1, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/android/settingslib/b/b;->cAO:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/b/b;->cAO:Z

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/android/settingslib/b/a;->cAf:Z

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    invoke-virtual {v0}, Lcom/android/settingslib/b/a;->ceT()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method cfw()V
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settingslib/b/b;->cAJ:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, Lcom/android/settingslib/b/b;->cAI:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/settingslib/b/b;->cAK:Lcom/android/settingslib/b/i;

    iget-object v3, p0, Lcom/android/settingslib/b/b;->cAG:Ljava/util/Comparator;

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/settingslib/b/b;->cAI:Z

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/settingslib/b/b;->cAK:Lcom/android/settingslib/b/i;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/settingslib/b/b;->cAG:Ljava/util/Comparator;

    iget-boolean v4, p0, Lcom/android/settingslib/b/b;->cAM:Z

    if-eqz v4, :cond_1

    const/4 v4, -0x2

    invoke-static {v4}, Landroid/os/Process;->setThreadPriority(I)V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/settingslib/b/b;->cAM:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit v1

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    invoke-interface {v2, v1}, Lcom/android/settingslib/b/i;->init(Landroid/content/Context;)V

    :cond_2
    iget-object v1, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v5, v5, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    if-eqz v0, :cond_5

    if-eqz v2, :cond_3

    invoke-interface {v2, v0}, Lcom/android/settingslib/b/i;->zN(Lcom/android/settingslib/b/h;)Z

    move-result v6

    if-eqz v6, :cond_5

    :cond_3
    iget-object v6, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v6, v6, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v6

    if-eqz v3, :cond_4

    :try_start_3
    iget-object v7, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v7, v7, Lcom/android/settingslib/b/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v7}, Lcom/android/settingslib/b/h;->cfE(Landroid/content/Context;)V

    :cond_4
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    monitor-exit v6

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_6
    if-eqz v3, :cond_7

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v0

    :try_start_4
    invoke-static {v5, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    monitor-exit v0

    :cond_7
    iget-object v1, p0, Lcom/android/settingslib/b/b;->cAJ:Ljava/lang/Object;

    monitor-enter v1

    :try_start_5
    iget-boolean v0, p0, Lcom/android/settingslib/b/b;->cAI:Z

    if-nez v0, :cond_8

    iput-object v5, p0, Lcom/android/settingslib/b/b;->cAF:Ljava/util/ArrayList;

    iget-boolean v0, p0, Lcom/android/settingslib/b/b;->cAL:Z

    if-nez v0, :cond_9

    iput-object v5, p0, Lcom/android/settingslib/b/b;->cAP:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAJ:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    :cond_8
    :goto_1
    monitor-exit v1

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    return-void

    :catchall_3
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_9
    :try_start_6
    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p0}, Lcom/android/settingslib/b/c;->hasMessages(ILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p0}, Lcom/android/settingslib/b/c;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v2, v2, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    invoke-virtual {v2, v0}, Lcom/android/settingslib/b/c;->sendMessage(Landroid/os/Message;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public cfx(Lcom/android/settingslib/b/i;Ljava/util/Comparator;)Ljava/util/ArrayList;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settingslib/b/b;->cfu(Lcom/android/settingslib/b/i;Ljava/util/Comparator;Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public cfy()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settingslib/b/b;->pause()V

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v1, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czU:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAP:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAP:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAF:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAF:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/settingslib/b/b;->cft()V

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    invoke-virtual {v0}, Lcom/android/settingslib/b/a;->ceU()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public pause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v1, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/android/settingslib/b/b;->cAO:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settingslib/b/b;->cAO:Z

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/android/settingslib/b/a;->cAf:Z

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p0}, Lcom/android/settingslib/b/d;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settingslib/b/b;->cAH:Lcom/android/settingslib/b/a;

    invoke-virtual {v0}, Lcom/android/settingslib/b/a;->ceL()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
