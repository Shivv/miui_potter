.class public Lcom/android/settingslib/b/H;
.super Ljava/lang/Object;
.source "StorageStatsSource.java"


# instance fields
.field private cBD:Landroid/app/usage/StorageStatsManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, Landroid/app/usage/StorageStatsManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/StorageStatsManager;

    iput-object v0, p0, Lcom/android/settingslib/b/H;->cBD:Landroid/app/usage/StorageStatsManager;

    return-void
.end method


# virtual methods
.method public cfM(Ljava/lang/String;Landroid/os/UserHandle;)Lcom/android/settingslib/b/I;
    .locals 2

    new-instance v0, Lcom/android/settingslib/b/I;

    iget-object v1, p0, Lcom/android/settingslib/b/H;->cBD:Landroid/app/usage/StorageStatsManager;

    invoke-virtual {v1, p1, p2}, Landroid/app/usage/StorageStatsManager;->queryExternalStatsForUser(Ljava/lang/String;Landroid/os/UserHandle;)Landroid/app/usage/ExternalStorageStats;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settingslib/b/I;-><init>(Landroid/app/usage/ExternalStorageStats;)V

    return-object v0
.end method

.method public cfN(Ljava/lang/String;I)J
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/b/H;->cBD:Landroid/app/usage/StorageStatsManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/usage/StorageStatsManager;->getCacheQuotaBytes(Ljava/lang/String;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public cfO(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)Lcom/android/settingslib/b/J;
    .locals 2

    new-instance v0, Lcom/android/settingslib/b/K;

    iget-object v1, p0, Lcom/android/settingslib/b/H;->cBD:Landroid/app/usage/StorageStatsManager;

    invoke-virtual {v1, p1, p2, p3}, Landroid/app/usage/StorageStatsManager;->queryStatsForPackage(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)Landroid/app/usage/StorageStats;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settingslib/b/K;-><init>(Landroid/app/usage/StorageStats;)V

    return-object v0
.end method
