.class final Lcom/android/settingslib/b/n;
.super Ljava/lang/Object;
.source "ApplicationsState.java"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cfJ(Lcom/android/settingslib/b/h;Lcom/android/settingslib/b/h;)I
    .locals 4

    iget-wide v0, p1, Lcom/android/settingslib/b/h;->cBm:J

    iget-wide v2, p2, Lcom/android/settingslib/b/h;->cBm:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-wide v0, p1, Lcom/android/settingslib/b/h;->cBm:J

    iget-wide v2, p2, Lcom/android/settingslib/b/h;->cBm:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_1
    sget-object v0, Lcom/android/settingslib/b/a;->cAa:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settingslib/b/h;

    check-cast p2, Lcom/android/settingslib/b/h;

    invoke-virtual {p0, p1, p2}, Lcom/android/settingslib/b/n;->cfJ(Lcom/android/settingslib/b/h;Lcom/android/settingslib/b/h;)I

    move-result v0

    return v0
.end method
