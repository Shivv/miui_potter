.class Lcom/android/settingslib/b/d;
.super Landroid/os/Handler;
.source "ApplicationsState.java"


# instance fields
.field final synthetic cAR:Lcom/android/settingslib/b/a;

.field cAS:Z

.field final cAT:Landroid/content/pm/IPackageStatsObserver$Stub;


# direct methods
.method constructor <init>(Lcom/android/settingslib/b/a;Landroid/os/Looper;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Lcom/android/settingslib/b/G;

    invoke-direct {v0, p0}, Lcom/android/settingslib/b/G;-><init>(Lcom/android/settingslib/b/d;)V

    iput-object v0, p0, Lcom/android/settingslib/b/d;->cAT:Landroid/content/pm/IPackageStatsObserver$Stub;

    return-void
.end method


# virtual methods
.method synthetic cfz()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAk:Landroid/app/usage/StorageStatsManager;

    iget-object v1, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->czY:Ljava/util/UUID;

    iget-object v2, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v2, v2, Lcom/android/settingslib/b/a;->czP:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget v3, v3, Lcom/android/settingslib/b/a;->cAC:I

    invoke-static {v3}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/usage/StorageStatsManager;->queryStatsForPackage(Ljava/util/UUID;Ljava/lang/String;Landroid/os/UserHandle;)Landroid/app/usage/StorageStats;

    move-result-object v0

    new-instance v1, Landroid/content/pm/PackageStats;

    iget-object v2, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v2, v2, Lcom/android/settingslib/b/a;->czP:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget v3, v3, Lcom/android/settingslib/b/a;->cAC:I

    invoke-direct {v1, v2, v3}, Landroid/content/pm/PackageStats;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/app/usage/StorageStats;->getCodeBytes()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/content/pm/PackageStats;->codeSize:J

    invoke-virtual {v0}, Landroid/app/usage/StorageStats;->getDataBytes()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/content/pm/PackageStats;->dataSize:J

    invoke-virtual {v0}, Landroid/app/usage/StorageStats;->getCacheBytes()J

    move-result-wide v2

    iput-wide v2, v1, Landroid/content/pm/PackageStats;->cacheSize:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAT:Landroid/content/pm/IPackageStatsObserver$Stub;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/IPackageStatsObserver$Stub;->onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "ApplicationsState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Failed to query stats: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAT:Landroid/content/pm/IPackageStatsObserver$Stub;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/IPackageStatsObserver$Stub;->onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 13

    const/4 v8, 0x5

    const/4 v7, 0x6

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v3, v0, Lcom/android/settingslib/b/a;->cAh:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_16

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->cAh:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->cAh:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v0

    :goto_0
    monitor-exit v3

    if-eqz v1, :cond_0

    move v3, v4

    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfw()V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_2
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v5, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v5

    move v2, v4

    :goto_3
    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    if-ge v2, v7, :cond_3

    iget-boolean v0, p0, Lcom/android/settingslib/b/d;->cAS:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settingslib/b/d;->cAS:Z

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x6

    invoke-virtual {v0, v3, v1}, Lcom/android/settingslib/b/c;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/b/c;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAc:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v6

    iget-object v1, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v1, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_15

    add-int/lit8 v1, v2, 0x1

    iget-object v2, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    invoke-static {v2, v0}, Lcom/android/settingslib/b/a;->ceK(Lcom/android/settingslib/b/a;Landroid/content/pm/ApplicationInfo;)Lcom/android/settingslib/b/h;

    move v3, v1

    :goto_4
    if-eqz v6, :cond_2

    iget-object v1, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    if-ltz v1, :cond_2

    iget-object v1, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v1, v1, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/b/h;

    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v6, 0x800000

    and-int/2addr v2, v6

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v2, v2, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    move v2, v3

    goto/16 :goto_3

    :cond_3
    monitor-exit v5

    if-lt v2, v7, :cond_4

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/settingslib/b/d;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/settingslib/b/c;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/settingslib/b/c;->sendEmptyMessage(I)Z

    :cond_5
    invoke-virtual {p0, v7}, Lcom/android/settingslib/b/d;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    :pswitch_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czQ:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getHomeActivities(Ljava/util/List;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v3, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v3

    :try_start_2
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v5

    :goto_5
    if-ge v4, v5, :cond_8

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/b/h;

    if-eqz v1, :cond_6

    const/4 v7, 0x1

    iput-boolean v7, v1, Lcom/android/settingslib/b/h;->cBk:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_6

    :catchall_2
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_8
    monitor-exit v3

    invoke-virtual {p0, v8}, Lcom/android/settingslib/b/d;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string/jumbo v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    move v2, v4

    :goto_7
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czQ:Landroid/content/pm/PackageManager;

    const v1, 0xc0200

    invoke-virtual {v0, v5, v1, v6}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v7

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v8, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v8

    :try_start_3
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    move v3, v4

    :goto_8
    if-ge v3, v9, :cond_a

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/b/h;

    if-eqz v1, :cond_9

    const/4 v10, 0x1

    iput-boolean v10, v1, Lcom/android/settingslib/b/h;->cBo:Z

    :goto_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_8

    :cond_9
    const-string/jumbo v1, "ApplicationsState"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Cannot find pkg: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " on user "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_9

    :catchall_3
    move-exception v0

    monitor-exit v8

    throw v0

    :cond_a
    monitor-exit v8

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    :cond_b
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/android/settingslib/b/c;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/android/settingslib/b/c;->sendEmptyMessage(I)Z

    :cond_c
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/settingslib/b/d;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    :pswitch_4
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/settingslib/b/d;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    :pswitch_5
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v1, v0, Lcom/android/settingslib/b/a;->cAo:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_4
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czP:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    if-eqz v0, :cond_d

    monitor-exit v1

    return-void

    :cond_d
    :try_start_5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    :goto_a
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_13

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->czK:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iget-object v5, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v6, 0x800000

    and-int/2addr v5, v6

    if-eqz v5, :cond_12

    iget-wide v6, v0, Lcom/android/settingslib/b/h;->cBc:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_e

    iget-boolean v5, v0, Lcom/android/settingslib/b/h;->cBn:Z

    if-eqz v5, :cond_12

    :cond_e
    iget-wide v4, v0, Lcom/android/settingslib/b/h;->cBg:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_f

    iget-wide v4, v0, Lcom/android/settingslib/b/h;->cBg:J

    const-wide/16 v6, 0x4e20

    sub-long v6, v2, v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_11

    :cond_f
    iget-boolean v4, p0, Lcom/android/settingslib/b/d;->cAS:Z

    if-nez v4, :cond_10

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/settingslib/b/d;->cAS:Z

    iget-object v4, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v4, v4, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x6

    invoke-virtual {v4, v6, v5}, Lcom/android/settingslib/b/c;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v5, v5, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    invoke-virtual {v5, v4}, Lcom/android/settingslib/b/c;->sendMessage(Landroid/os/Message;)Z

    :cond_10
    iput-wide v2, v0, Lcom/android/settingslib/b/h;->cBg:J

    iget-object v2, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v3, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->storageUuid:Ljava/util/UUID;

    iput-object v3, v2, Lcom/android/settingslib/b/a;->czY:Ljava/util/UUID;

    iget-object v2, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v3, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v3, v2, Lcom/android/settingslib/b/a;->czP:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    iput v0, v2, Lcom/android/settingslib/b/a;->cAC:I

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAA:Lcom/android/settingslib/b/d;

    new-instance v2, Lcom/android/settingslib/b/-$Lambda$VTxsQnI4iclqa9Kn3eNREve6DLY;

    invoke-direct {v2, p0}, Lcom/android/settingslib/b/-$Lambda$VTxsQnI4iclqa9Kn3eNREve6DLY;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/d;->post(Ljava/lang/Runnable;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    :cond_11
    monitor-exit v1

    return-void

    :cond_12
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_a

    :cond_13
    :try_start_6
    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/c;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/android/settingslib/b/c;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settingslib/b/d;->cAS:Z

    iget-object v0, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v0, v0, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v0, v3, v2}, Lcom/android/settingslib/b/c;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settingslib/b/d;->cAR:Lcom/android/settingslib/b/a;

    iget-object v2, v2, Lcom/android/settingslib/b/a;->cAs:Lcom/android/settingslib/b/c;

    invoke-virtual {v2, v0}, Lcom/android/settingslib/b/c;->sendMessage(Landroid/os/Message;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    :cond_14
    monitor-exit v1

    goto/16 :goto_2

    :catchall_4
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_15
    move v3, v2

    goto/16 :goto_4

    :cond_16
    move-object v1, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
