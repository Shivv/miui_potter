.class final Lcom/android/settingslib/b/s;
.super Ljava/lang/Object;
.source "ApplicationsState.java"

# interfaces
.implements Lcom/android/settingslib/b/i;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zN(Lcom/android/settingslib/b/h;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-static {v0}, Lcom/android/settingslib/b/L;->cfT(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_2

    return v2

    :cond_2
    iget-boolean v0, p1, Lcom/android/settingslib/b/h;->cBo:Z

    if-eqz v0, :cond_3

    return v2

    :cond_3
    iget-object v0, p1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lcom/android/settingslib/b/h;->cBk:Z

    if-eqz v0, :cond_4

    return v2

    :cond_4
    return v1
.end method

.method public zO()V
    .locals 0

    return-void
.end method
