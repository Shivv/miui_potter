.class public Lcom/android/settingslib/b/k;
.super Ljava/lang/Object;
.source "ApplicationsState.java"

# interfaces
.implements Lcom/android/settingslib/b/i;


# instance fields
.field private final cBu:Lcom/android/settingslib/b/i;

.field private final cBv:Lcom/android/settingslib/b/i;


# direct methods
.method public constructor <init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/b/k;->cBv:Lcom/android/settingslib/b/i;

    iput-object p2, p0, Lcom/android/settingslib/b/k;->cBu:Lcom/android/settingslib/b/i;

    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/b/k;->cBv:Lcom/android/settingslib/b/i;

    invoke-interface {v0, p1}, Lcom/android/settingslib/b/i;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settingslib/b/k;->cBu:Lcom/android/settingslib/b/i;

    invoke-interface {v0, p1}, Lcom/android/settingslib/b/i;->init(Landroid/content/Context;)V

    return-void
.end method

.method public zN(Lcom/android/settingslib/b/h;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/b/k;->cBv:Lcom/android/settingslib/b/i;

    invoke-interface {v0, p1}, Lcom/android/settingslib/b/i;->zN(Lcom/android/settingslib/b/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/b/k;->cBu:Lcom/android/settingslib/b/i;

    invoke-interface {v0, p1}, Lcom/android/settingslib/b/i;->zN(Lcom/android/settingslib/b/h;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public zO()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/b/k;->cBv:Lcom/android/settingslib/b/i;

    invoke-interface {v0}, Lcom/android/settingslib/b/i;->zO()V

    iget-object v0, p0, Lcom/android/settingslib/b/k;->cBu:Lcom/android/settingslib/b/i;

    invoke-interface {v0}, Lcom/android/settingslib/b/i;->zO()V

    return-void
.end method
