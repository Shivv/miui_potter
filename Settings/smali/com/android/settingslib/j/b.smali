.class final Lcom/android/settingslib/j/b;
.super Ljava/lang/Object;
.source "ZoneGetter.java"


# instance fields
.field public final cJK:[Ljava/lang/CharSequence;

.field public final cJL:[Ljava/util/TimeZone;

.field public final cJM:I

.field public final cJN:Ljava/util/Set;

.field public final cJO:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-static {p1}, Lcom/android/settingslib/j/a;->cpv(Landroid/content/Context;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/j/b;->cJM:I

    iget v0, p0, Lcom/android/settingslib/j/b;->cJM:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settingslib/j/b;->cJO:[Ljava/lang/String;

    iget v0, p0, Lcom/android/settingslib/j/b;->cJM:I

    new-array v0, v0, [Ljava/util/TimeZone;

    iput-object v0, p0, Lcom/android/settingslib/j/b;->cJL:[Ljava/util/TimeZone;

    iget v0, p0, Lcom/android/settingslib/j/b;->cJM:I

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/android/settingslib/j/b;->cJK:[Ljava/lang/CharSequence;

    move v1, v2

    :goto_0
    iget v0, p0, Lcom/android/settingslib/j/b;->cJM:I

    if-ge v1, v0, :cond_0

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v6, p0, Lcom/android/settingslib/j/b;->cJO:[Ljava/lang/String;

    aput-object v0, v6, v1

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    iget-object v6, p0, Lcom/android/settingslib/j/b;->cJL:[Ljava/util/TimeZone;

    aput-object v0, v6, v1

    iget-object v6, p0, Lcom/android/settingslib/j/b;->cJK:[Ljava/lang/CharSequence;

    invoke-static {p1, v3, v0, v4}, Lcom/android/settingslib/j/a;->cpr(Landroid/content/Context;Ljava/util/Locale;Ljava/util/TimeZone;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v6, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/j/b;->cJN:Ljava/util/Set;

    invoke-static {v3}, Llibcore/icu/TimeZoneNames;->forLocale(Ljava/util/Locale;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    :goto_1
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget-object v4, p0, Lcom/android/settingslib/j/b;->cJN:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method
