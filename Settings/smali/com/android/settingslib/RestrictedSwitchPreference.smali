.class public Lcom/android/settingslib/RestrictedSwitchPreference;
.super Landroid/support/v14/preference/SwitchPreference;
.source "RestrictedSwitchPreference.java"


# instance fields
.field cQO:Lcom/android/settingslib/m;

.field cQP:Z

.field cQQ:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settingslib/RestrictedSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    sget v0, Landroid/support/v7/preference/a;->dIa:I

    const v1, 0x101036d

    invoke-static {p1, v0, v1}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settingslib/RestrictedSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settingslib/RestrictedSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v14/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-boolean v1, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQP:Z

    iput-object v2, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQQ:Ljava/lang/String;

    sget v0, Lcom/android/settingslib/h;->cLm:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/RestrictedSwitchPreference;->dkS(I)V

    new-instance v0, Lcom/android/settingslib/m;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/m;-><init>(Landroid/content/Context;Landroid/support/v7/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQO:Lcom/android/settingslib/m;

    if-eqz p2, :cond_1

    sget-object v0, Lcom/android/settingslib/j;->cOH:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    sget v0, Lcom/android/settingslib/j;->cOJ:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v4, v0, Landroid/util/TypedValue;->type:I

    const/16 v5, 0x12

    if-ne v4, v5, :cond_5

    iget v0, v0, Landroid/util/TypedValue;->data:I

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQP:Z

    :cond_0
    sget v0, Lcom/android/settingslib/j;->cOI:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-eqz v0, :cond_6

    iget v3, v0, Landroid/util/TypedValue;->type:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6

    iget v3, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v3, :cond_7

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_8

    move-object v0, v2

    :goto_2
    iput-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQQ:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQQ:Ljava/lang/String;

    if-nez v0, :cond_2

    sget v0, Lcom/android/settingslib/i;->cMu:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQQ:Ljava/lang/String;

    :cond_2
    iget-boolean v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQP:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/android/settingslib/h;->cLl:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/RestrictedSwitchPreference;->setLayoutResource(I)V

    invoke-virtual {p0, v1}, Lcom/android/settingslib/RestrictedSwitchPreference;->csi(Z)V

    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move-object v0, v2

    goto :goto_1

    :cond_7
    iget-object v0, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_8
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 5

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v14/preference/SwitchPreference;->al(Landroid/support/v7/preference/l;)V

    iget-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQO:Lcom/android/settingslib/m;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/m;->cpW(Landroid/support/v7/preference/l;)V

    sget v0, Lcom/android/settingslib/g;->cLd:I

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x1020040

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v4

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/RestrictedSwitchPreference;->csj()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/android/settingslib/RestrictedSwitchPreference;->csj()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQP:Z

    if-eqz v0, :cond_6

    sget v0, Lcom/android/settingslib/g;->cKQ:I

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settingslib/RestrictedSwitchPreference;->csj()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v2, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQQ:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settingslib/RestrictedSwitchPreference;->csj()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQQ:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method protected cpS(Landroid/support/v7/preference/k;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQO:Lcom/android/settingslib/m;

    invoke-virtual {v0}, Lcom/android/settingslib/m;->cpY()V

    invoke-super {p0, p1}, Landroid/support/v14/preference/SwitchPreference;->cpS(Landroid/support/v7/preference/k;)V

    return-void
.end method

.method public csi(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQO:Lcom/android/settingslib/m;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/m;->cpX(Z)V

    return-void
.end method

.method public csj()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQO:Lcom/android/settingslib/m;

    invoke-virtual {v0}, Lcom/android/settingslib/m;->cpZ()Z

    move-result v0

    return v0
.end method

.method public performClick()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQO:Lcom/android/settingslib/m;

    invoke-virtual {v0}, Lcom/android/settingslib/m;->performClick()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v14/preference/SwitchPreference;->performClick()V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/RestrictedSwitchPreference;->csj()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/RestrictedSwitchPreference;->cQO:Lcom/android/settingslib/m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/m;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v14/preference/SwitchPreference;->setEnabled(Z)V

    return-void
.end method
