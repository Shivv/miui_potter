.class public Lcom/android/settingslib/o;
.super Ljava/lang/Object;
.source "MiuiRestrictedPreferenceHelper.java"


# instance fields
.field private cPT:Z

.field private cPU:Z

.field private cPV:Ljava/lang/String;

.field private cPW:Lcom/android/settingslib/n;

.field private final cPX:Landroid/preference/Preference;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/settingslib/o;->cPV:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/settingslib/o;->cPU:Z

    iput-object p1, p0, Lcom/android/settingslib/o;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settingslib/o;->cPX:Landroid/preference/Preference;

    if-eqz p3, :cond_4

    sget-object v0, Lcom/android/settingslib/j;->cOE:[I

    invoke-virtual {p1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    sget v0, Lcom/android/settingslib/j;->cOG:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v4, v0, Landroid/util/TypedValue;->type:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    iget v4, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v4, :cond_1

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/android/settingslib/o;->cPV:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settingslib/o;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/settingslib/o;->cPV:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    invoke-static {v0, v4, v5}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v1, p0, Lcom/android/settingslib/o;->cPV:Ljava/lang/String;

    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, v0, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    sget v0, Lcom/android/settingslib/j;->cOF:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    if-eqz v0, :cond_4

    iget v1, v0, Landroid/util/TypedValue;->type:I

    const/16 v3, 0x12

    if-ne v1, v3, :cond_6

    iget v0, v0, Landroid/util/TypedValue;->data:I

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/android/settingslib/o;->cPU:Z

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public cqg(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/o;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    return-void
.end method

.method public cqh(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/o;->cPU:Z

    return-void
.end method

.method public cqi()V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/o;->cPV:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/o;->cPV:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settingslib/o;->cqg(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public cqj()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settingslib/o;->cPT:Z

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/android/settingslib/o;->cPT:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/settingslib/o;->cPU:Z

    if-eqz v0, :cond_1

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/android/settingslib/i;->cMv:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/settingslib/o;->cPT:Z

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public performClick()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/settingslib/o;->cPT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/o;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settingslib/o;->cPW:Lcom/android/settingslib/n;

    invoke-static {v0, v1}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setDisabledByAdmin(Lcom/android/settingslib/n;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    iput-object p1, p0, Lcom/android/settingslib/o;->cPW:Lcom/android/settingslib/n;

    iget-boolean v3, p0, Lcom/android/settingslib/o;->cPT:Z

    if-eq v3, v0, :cond_1

    iput-boolean v0, p0, Lcom/android/settingslib/o;->cPT:Z

    :goto_1
    iget-object v2, p0, Lcom/android/settingslib/o;->cPX:Landroid/preference/Preference;

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
