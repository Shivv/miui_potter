.class public Lcom/android/settingslib/D;
.super Ljava/lang/Object;
.source "NetworkPolicyEditor.java"


# instance fields
.field private cQG:Ljava/util/ArrayList;

.field private cQH:Landroid/net/NetworkPolicyManager;


# direct methods
.method public constructor <init>(Landroid/net/NetworkPolicyManager;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/D;->cQG:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkPolicyManager;

    iput-object v0, p0, Lcom/android/settingslib/D;->cQH:Landroid/net/NetworkPolicyManager;

    return-void
.end method

.method private static crO(Landroid/net/NetworkTemplate;)Landroid/net/NetworkTemplate;
    .locals 4

    const/4 v2, 0x0

    if-nez p0, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {p0}, Landroid/net/NetworkTemplate;->getNetworkId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/wifi/WifiInfo;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/net/NetworkTemplate;

    invoke-virtual {p0}, Landroid/net/NetworkTemplate;->getMatchRule()I

    move-result v2

    invoke-virtual {p0}, Landroid/net/NetworkTemplate;->getSubscriberId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Landroid/net/NetworkTemplate;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    return-object v2
.end method

.method private static crR(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;
    .locals 14

    const/4 v13, 0x1

    const-wide/16 v4, -0x1

    invoke-virtual {p0}, Landroid/net/NetworkTemplate;->getMatchRule()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-static {}, Landroid/util/RecurrenceRule;->buildNever()Landroid/util/RecurrenceRule;

    move-result-object v3

    const/4 v12, 0x0

    :goto_0
    new-instance v1, Landroid/net/NetworkPolicy;

    move-object v2, p0

    move-wide v6, v4

    move-wide v8, v4

    move-wide v10, v4

    invoke-direct/range {v1 .. v13}, Landroid/net/NetworkPolicy;-><init>(Landroid/net/NetworkTemplate;Landroid/util/RecurrenceRule;JJJJZZ)V

    return-object v1

    :cond_0
    invoke-static {}, Ljava/time/ZonedDateTime;->now()Ljava/time/ZonedDateTime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/time/ZonedDateTime;->getDayOfMonth()I

    move-result v0

    invoke-static {}, Ljava/time/ZoneId;->systemDefault()Ljava/time/ZoneId;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/RecurrenceRule;->buildRecurringMonthly(ILjava/time/ZoneId;)Landroid/util/RecurrenceRule;

    move-result-object v3

    move v12, v13

    goto :goto_0
.end method

.method private csc(Landroid/net/NetworkTemplate;J)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crX(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    iput-wide p2, v0, Landroid/net/NetworkPolicy;->warningBytes:J

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/net/NetworkPolicy;->inferred:Z

    invoke-virtual {v0}, Landroid/net/NetworkPolicy;->clearSnooze()V

    invoke-virtual {p0}, Lcom/android/settingslib/D;->csd()V

    return-void
.end method


# virtual methods
.method public crP(Landroid/net/NetworkTemplate;J)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crS(Landroid/net/NetworkTemplate;)J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-lez v0, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/D;->csc(Landroid/net/NetworkTemplate;J)V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crX(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    iput-wide p2, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/net/NetworkPolicy;->inferred:Z

    invoke-virtual {v0}, Landroid/net/NetworkPolicy;->clearSnooze()V

    invoke-virtual {p0}, Lcom/android/settingslib/D;->csd()V

    return-void
.end method

.method public crQ(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/android/settingslib/D;->crO(Landroid/net/NetworkTemplate;)Landroid/net/NetworkTemplate;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    return-object v0
.end method

.method public crS(Landroid/net/NetworkTemplate;)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v0, v0, Landroid/net/NetworkPolicy;->warningBytes:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public crT(Landroid/net/NetworkTemplate;J)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crW(Landroid/net/NetworkTemplate;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    :goto_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/D;->csc(Landroid/net/NetworkTemplate;J)V

    return-void

    :cond_0
    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    goto :goto_0
.end method

.method public crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/D;->cQG:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkPolicy;

    iget-object v2, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    invoke-virtual {v2, p1}, Landroid/net/NetworkTemplate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public crV([Landroid/net/NetworkPolicy;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/D;->cQH:Landroid/net/NetworkPolicyManager;

    invoke-virtual {v0, p1}, Landroid/net/NetworkPolicyManager;->setNetworkPolicies([Landroid/net/NetworkPolicy;)V

    return-void
.end method

.method public crW(Landroid/net/NetworkTemplate;)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v0, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public crX(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/android/settingslib/D;->crR(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/D;->cQG:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method public crY(Landroid/net/NetworkTemplate;ILjava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crX(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    invoke-static {p3}, Ljava/time/ZoneId;->of(Ljava/lang/String;)Ljava/time/ZoneId;

    move-result-object v1

    invoke-static {p2, v1}, Landroid/net/NetworkPolicy;->buildRule(ILjava/time/ZoneId;)Landroid/util/RecurrenceRule;

    move-result-object v1

    iput-object v1, v0, Landroid/net/NetworkPolicy;->cycleRule:Landroid/util/RecurrenceRule;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/net/NetworkPolicy;->inferred:Z

    invoke-virtual {v0}, Landroid/net/NetworkPolicy;->clearSnooze()V

    invoke-virtual {p0}, Lcom/android/settingslib/D;->csd()V

    return-void
.end method

.method public crZ(Landroid/net/NetworkTemplate;)I
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/net/NetworkPolicy;->cycleRule:Landroid/util/RecurrenceRule;

    invoke-virtual {v1}, Landroid/util/RecurrenceRule;->isMonthly()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/net/NetworkPolicy;->cycleRule:Landroid/util/RecurrenceRule;

    iget-object v0, v0, Landroid/util/RecurrenceRule;->start:Ljava/time/ZonedDateTime;

    invoke-virtual {v0}, Ljava/time/ZonedDateTime;->getDayOfMonth()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public csa(Landroid/net/NetworkTemplate;Z)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v2

    if-eqz p2, :cond_3

    if-nez v2, :cond_2

    invoke-static {p1}, Lcom/android/settingslib/D;->crR(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v2

    iput-boolean v1, v2, Landroid/net/NetworkPolicy;->metered:Z

    iput-boolean v0, v2, Landroid/net/NetworkPolicy;->inferred:Z

    iget-object v0, p0, Lcom/android/settingslib/D;->cQG:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/android/settingslib/D;->crO(Landroid/net/NetworkTemplate;)Landroid/net/NetworkTemplate;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/android/settingslib/D;->cQG:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settingslib/D;->csd()V

    :cond_1
    return-void

    :cond_2
    iget-boolean v3, v2, Landroid/net/NetworkPolicy;->metered:Z

    if-nez v3, :cond_0

    iput-boolean v1, v2, Landroid/net/NetworkPolicy;->metered:Z

    iput-boolean v0, v2, Landroid/net/NetworkPolicy;->inferred:Z

    move v0, v1

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_0

    iget-boolean v3, v2, Landroid/net/NetworkPolicy;->metered:Z

    if-eqz v3, :cond_0

    iput-boolean v0, v2, Landroid/net/NetworkPolicy;->metered:Z

    iput-boolean v0, v2, Landroid/net/NetworkPolicy;->inferred:Z

    move v0, v1

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method public csb()V
    .locals 10

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-wide/16 v8, -0x1

    iget-object v2, p0, Lcom/android/settingslib/D;->cQH:Landroid/net/NetworkPolicyManager;

    invoke-virtual {v2}, Landroid/net/NetworkPolicyManager;->getNetworkPolicies()[Landroid/net/NetworkPolicy;

    move-result-object v3

    iget-object v2, p0, Lcom/android/settingslib/D;->cQG:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    iget-wide v6, v5, Landroid/net/NetworkPolicy;->limitBytes:J

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    iput-wide v8, v5, Landroid/net/NetworkPolicy;->limitBytes:J

    move v0, v1

    :cond_0
    iget-wide v6, v5, Landroid/net/NetworkPolicy;->warningBytes:J

    cmp-long v6, v6, v8

    if-gez v6, :cond_1

    iput-wide v8, v5, Landroid/net/NetworkPolicy;->warningBytes:J

    move v0, v1

    :cond_1
    iget-object v6, p0, Lcom/android/settingslib/D;->cQG:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settingslib/D;->csd()V

    :cond_3
    return-void
.end method

.method public csd()V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/D;->cQG:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settingslib/D;->cQG:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/net/NetworkPolicy;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/NetworkPolicy;

    new-instance v1, Lcom/android/settingslib/E;

    invoke-direct {v1, p0, v0}, Lcom/android/settingslib/E;-><init>(Lcom/android/settingslib/D;[Landroid/net/NetworkPolicy;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/E;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
