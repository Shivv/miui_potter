.class public Lcom/android/settingslib/graph/BottomLabelLayout;
.super Landroid/widget/LinearLayout;
.source "BottomLabelLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private cmL()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/settingslib/graph/BottomLabelLayout;->getOrientation()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 6

    const/4 v2, 0x1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-direct {p0}, Lcom/android/settingslib/graph/BottomLabelLayout;->cmL()Z

    move-result v3

    const/4 v0, 0x0

    if-nez v3, :cond_2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    if-ne v4, v5, :cond_2

    const/high16 v0, -0x80000000

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    move v1, v2

    :goto_0
    invoke-super {p0, v0, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/graph/BottomLabelLayout;->getMeasuredWidthAndState()I

    move-result v0

    const/high16 v3, -0x1000000

    and-int/2addr v0, v3

    const/high16 v3, 0x1000000

    if-ne v0, v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/settingslib/graph/BottomLabelLayout;->setStacked(Z)V

    move v1, v2

    :cond_0
    if-eqz v1, :cond_1

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    :cond_1
    return-void

    :cond_2
    move v1, v0

    move v0, p1

    goto :goto_0
.end method

.method setStacked(Z)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settingslib/graph/BottomLabelLayout;->setOrientation(I)V

    if-eqz p1, :cond_3

    const v0, 0x800003

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/settingslib/graph/BottomLabelLayout;->setGravity(I)V

    sget v0, Lcom/android/settingslib/g;->cLg:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/graph/BottomLabelLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const/16 v1, 0x8

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const/16 v0, 0x50

    goto :goto_1
.end method
