.class public Lcom/android/settingslib/graph/a;
.super Landroid/graphics/drawable/Drawable;
.source "BatteryMeterDrawableBase.java"


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field protected final cGA:Landroid/graphics/Paint;

.field private final cGB:[I

.field private final cGC:Landroid/graphics/RectF;

.field private cGD:F

.field private cGE:F

.field protected final cGF:Landroid/graphics/Paint;

.field private cGG:I

.field private cGH:F

.field private cGI:Ljava/lang/String;

.field private cGJ:I

.field private final cGK:Landroid/graphics/RectF;

.field private final cGL:I

.field private final cGM:[F

.field private cGN:F

.field private final cGO:Landroid/graphics/Path;

.field private final cGP:Landroid/graphics/Path;

.field private cGQ:I

.field private cGR:F

.field protected final cGS:Landroid/graphics/Paint;

.field private final cGT:Landroid/graphics/Path;

.field private cGU:F

.field private cGV:I

.field private final cGW:[F

.field protected final cGX:Landroid/graphics/Paint;

.field private cGY:Z

.field private cGZ:I

.field private final cGy:Landroid/graphics/RectF;

.field private final cGz:Landroid/graphics/RectF;

.field protected final cHa:Landroid/graphics/Paint;

.field private final cHb:I

.field private final cHc:Landroid/graphics/Path;

.field private final cHd:Landroid/graphics/Path;

.field private cHe:Z

.field protected final cHf:Landroid/graphics/Paint;

.field private cHg:Z

.field private final cHh:I

.field protected final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settingslib/graph/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settingslib/graph/a;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 12

    const/4 v0, -0x1

    const/4 v11, 0x0

    const/4 v1, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGG:I

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGZ:I

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGN:F

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGO:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cHc:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cHd:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGT:Landroid/graphics/Path;

    iput-object p1, p0, Lcom/android/settingslib/graph/a;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v0, Lcom/android/settingslib/a;->cKg:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v3

    sget v0, Lcom/android/settingslib/a;->cKh:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v4

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->length()I

    move-result v5

    mul-int/lit8 v0, v5, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_1

    iget-object v6, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    mul-int/lit8 v7, v0, 0x2

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    aput v8, v6, v7

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getType(I)I

    move-result v6

    if-ne v6, v10, :cond_0

    iget-object v6, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    mul-int/lit8 v7, v0, 0x2

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getThemeAttributeId(II)I

    move-result v8

    invoke-static {p1, v8}, Lcom/android/settingslib/v;->cqJ(Landroid/content/Context;I)I

    move-result v8

    aput v8, v6, v7

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    mul-int/lit8 v7, v0, 0x2

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v8

    aput v8, v6, v7

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    sget v0, Lcom/android/settingslib/i;->cLC:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGI:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/graph/a;->cHh:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settingslib/f;->cKM:I

    invoke-virtual {v0, v1, v9, v9}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGH:F

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settingslib/f;->cKN:I

    invoke-virtual {v0, v1, v9, v9}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGE:F

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settingslib/f;->cKO:I

    invoke-virtual {v0, v1, v9, v9}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGR:F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cHf:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHf:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHf:Landroid/graphics/Paint;

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHf:Landroid/graphics/Paint;

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHf:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGA:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGA:Landroid/graphics/Paint;

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGA:Landroid/graphics/Paint;

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGA:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGF:Landroid/graphics/Paint;

    const-string/jumbo v0, "sans-serif-condensed"

    invoke-static {v0, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGF:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGF:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGS:Landroid/graphics/Paint;

    const-string/jumbo v0, "sans-serif"

    invoke-static {v0, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGS:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGS:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    array-length v0, v0

    if-le v0, v9, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGS:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    aget v1, v1, v9

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/graph/a;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settingslib/c;->cKr:I

    invoke-static {v0, v1}, Lcom/android/settingslib/v;->cqy(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGV:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v9}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cHa:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHa:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settingslib/c;->cKp:I

    invoke-static {v1, v3}, Lcom/android/settingslib/v;->cqy(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget v0, Lcom/android/settingslib/a;->cKf:I

    invoke-static {v2, v0}, Lcom/android/settingslib/graph/a;->cmy(Landroid/content/res/Resources;I)[F

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGW:[F

    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cHa:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGX:Landroid/graphics/Paint;

    sget v0, Lcom/android/settingslib/a;->cKi:I

    invoke-static {v2, v0}, Lcom/android/settingslib/graph/a;->cmy(Landroid/content/res/Resources;I)[F

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/graph/a;->cGM:[F

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settingslib/d;->cKu:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/graph/a;->cHb:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settingslib/d;->cKt:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGL:I

    return-void
.end method

.method private cmu(I)I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/settingslib/graph/a;->cHg:Z

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGZ:I

    return v0

    :cond_0
    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    aget v2, v1, v0

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    add-int/lit8 v3, v0, 0x1

    aget v1, v1, v3

    if-gt p1, v2, :cond_2

    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cGB:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x2

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGZ:I

    return v0

    :cond_1
    return v1

    :cond_2
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_3
    return v1
.end method

.method private static cmy(Landroid/content/res/Resources;I)[F
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v4

    move v0, v1

    move v2, v1

    move v3, v1

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    aget v5, v4, v0

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v5, v0, 0x1

    aget v5, v4, v5

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    array-length v0, v4

    new-array v0, v0, [F

    :goto_1
    array-length v5, v4

    if-ge v1, v5, :cond_1

    aget v5, v4, v1

    int-to-float v5, v5

    int-to-float v6, v3

    div-float/2addr v5, v6

    aput v5, v0, v1

    add-int/lit8 v5, v1, 0x1

    add-int/lit8 v6, v1, 0x1

    aget v6, v4, v6

    int-to-float v6, v6

    int-to-float v7, v2

    div-float/2addr v6, v7

    aput v6, v0, v5

    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public cms()I
    .locals 1

    iget v0, p0, Lcom/android/settingslib/graph/a;->cHh:I

    return v0
.end method

.method public cmt(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/graph/a;->cHe:Z

    invoke-virtual {p0}, Lcom/android/settingslib/graph/a;->cmv()V

    return-void
.end method

.method protected cmv()V
    .locals 4

    new-instance v0, Lcom/android/settingslib/graph/-$Lambda$MrwEU-vyaTwkHywoAWWEwS4wcZo;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/android/settingslib/graph/-$Lambda$MrwEU-vyaTwkHywoAWWEwS4wcZo;-><init>(BLjava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/android/settingslib/graph/a;->unscheduleSelf(Ljava/lang/Runnable;)V

    new-instance v0, Lcom/android/settingslib/graph/-$Lambda$MrwEU-vyaTwkHywoAWWEwS4wcZo;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/android/settingslib/graph/-$Lambda$MrwEU-vyaTwkHywoAWWEwS4wcZo;-><init>(BLjava/lang/Object;)V

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/settingslib/graph/a;->scheduleSelf(Ljava/lang/Runnable;J)V

    return-void
.end method

.method synthetic cmw()V
    .locals 0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method synthetic cmx()V
    .locals 0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 11

    iget v9, p0, Lcom/android/settingslib/graph/a;->cGG:I

    const/4 v0, -0x1

    if-ne v9, v0, :cond_0

    return-void

    :cond_0
    int-to-float v0, v9

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v1, v0, v1

    iget v2, p0, Lcom/android/settingslib/graph/a;->cGQ:I

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGQ:I

    int-to-float v0, v0

    const v3, 0x3f27b961

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iget v3, p0, Lcom/android/settingslib/graph/a;->cGJ:I

    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    int-to-float v4, v2

    iget v5, p0, Lcom/android/settingslib/graph/a;->cGH:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    const/4 v6, 0x0

    const/4 v7, 0x0

    int-to-float v8, v0

    int-to-float v10, v2

    invoke-virtual {v5, v6, v7, v8, v10}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    int-to-float v3, v3

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/graphics/RectF;->offset(FF)V

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    int-to-float v6, v0

    const/high16 v7, 0x3e800000    # 0.25f

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    int-to-float v0, v0

    const/high16 v8, 0x3e800000    # 0.25f

    mul-float/2addr v0, v8

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v7, v0

    iget-object v7, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    int-to-float v8, v4

    add-float/2addr v7, v8

    invoke-virtual {v3, v5, v6, v0, v7}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/android/settingslib/graph/a;->cGE:F

    add-float/2addr v3, v5

    iput v3, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v5, p0, Lcom/android/settingslib/graph/a;->cGE:F

    add-float/2addr v3, v5

    iput v3, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lcom/android/settingslib/graph/a;->cGR:F

    sub-float/2addr v3, v5

    iput v3, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->top:F

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, p0, Lcom/android/settingslib/graph/a;->cGE:F

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/android/settingslib/graph/a;->cGE:F

    add-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/android/settingslib/graph/a;->cGR:F

    sub-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/android/settingslib/graph/a;->cGR:F

    sub-float/2addr v3, v4

    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGA:Landroid/graphics/Paint;

    iget-boolean v0, p0, Lcom/android/settingslib/graph/a;->cGY:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGV:I

    :goto_0
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/16 v0, 0x60

    if-lt v9, v0, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move v7, v0

    :goto_2
    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGK:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-boolean v0, p0, Lcom/android/settingslib/graph/a;->cGY:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    const/high16 v4, 0x40c00000    # 6.0f

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    const/high16 v6, 0x41200000    # 10.0f

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    cmpl-float v5, v5, v0

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    cmpl-float v5, v5, v1

    if-eqz v5, :cond_5

    :cond_1
    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    invoke-virtual {v5, v0, v1, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGO:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGO:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGW:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGW:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v0, 0x2

    :goto_3
    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGW:[F

    array-length v1, v1

    if-ge v0, v1, :cond_9

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGO:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGW:[F

    aget v4, v4, v0

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGW:[F

    add-int/lit8 v6, v0, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v0, 0x2

    goto :goto_3

    :cond_2
    invoke-direct {p0, v9}, Lcom/android/settingslib/graph/a;->cmu(I)I

    move-result v0

    goto/16 :goto_0

    :cond_3
    iget v0, p0, Lcom/android/settingslib/graph/a;->cHh:I

    if-gt v9, v0, :cond_14

    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v0, v4, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v1

    move v7, v0

    goto/16 :goto_2

    :cond_5
    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    cmpl-float v5, v5, v3

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    cmpl-float v5, v5, v4

    if-nez v5, :cond_1

    :goto_4
    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v7

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v3

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const v1, 0x3e99999a    # 0.3f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_a

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGO:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cHa:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_6
    :goto_5
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    iget-boolean v3, p0, Lcom/android/settingslib/graph/a;->cGY:Z

    if-nez v3, :cond_7

    iget-boolean v3, p0, Lcom/android/settingslib/graph/a;->cHg:Z

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_7

    iget v3, p0, Lcom/android/settingslib/graph/a;->cHh:I

    if-le v9, v3, :cond_7

    iget-boolean v3, p0, Lcom/android/settingslib/graph/a;->cHe:Z

    if-eqz v3, :cond_7

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGF:Landroid/graphics/Paint;

    invoke-direct {p0, v9}, Lcom/android/settingslib/graph/a;->cmu(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGF:Landroid/graphics/Paint;

    int-to-float v2, v2

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGG:I

    const/16 v3, 0x64

    if-ne v0, v3, :cond_10

    const v0, 0x3ec28f5c    # 0.38f

    :goto_6
    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGF:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float v0, v0

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGD:F

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGJ:I

    int-to-float v0, v0

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v4, v0, v2

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGQ:I

    int-to-float v0, v0

    iget v2, p0, Lcom/android/settingslib/graph/a;->cGD:F

    add-float/2addr v0, v2

    const v2, 0x3ef0a3d7    # 0.47f

    mul-float v5, v0, v2

    cmpl-float v0, v7, v5

    if-lez v0, :cond_11

    const/4 v8, 0x1

    :goto_7
    if-nez v8, :cond_13

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGT:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGF:Landroid/graphics/Paint;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v6, p0, Lcom/android/settingslib/graph/a;->cGT:Landroid/graphics/Path;

    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextPath(Ljava/lang/String;IIFFLandroid/graphics/Path;)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cGT:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$Op;->DIFFERENCE:Landroid/graphics/Path$Op;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    move v0, v8

    :cond_7
    :goto_8
    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cHf:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iput v7, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cHd:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cHd:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    sget-object v6, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cHd:Landroid/graphics/Path;

    sget-object v6, Landroid/graphics/Path$Op;->INTERSECT:Landroid/graphics/Path$Op;

    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGA:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-boolean v2, p0, Lcom/android/settingslib/graph/a;->cGY:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/android/settingslib/graph/a;->cHg:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_8

    iget v2, p0, Lcom/android/settingslib/graph/a;->cHh:I

    if-gt v9, v2, :cond_12

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGJ:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/android/settingslib/graph/a;->cGQ:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/settingslib/graph/a;->cGU:F

    add-float/2addr v1, v2

    const v2, 0x3ef5c28f    # 0.48f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/android/settingslib/graph/a;->cGI:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGS:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_8
    :goto_9
    return-void

    :cond_9
    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGO:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGW:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGW:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGC:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_4

    :cond_a
    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGO:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$Op;->DIFFERENCE:Landroid/graphics/Path$Op;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    goto/16 :goto_5

    :cond_b
    iget-boolean v0, p0, Lcom/android/settingslib/graph/a;->cHg:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v3, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float/2addr v4, v0

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    sub-float/2addr v5, v0

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget-object v6, p0, Lcom/android/settingslib/graph/a;->cGz:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    sub-float v0, v6, v0

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    sub-float v0, v5, v0

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    cmpl-float v5, v5, v1

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    cmpl-float v5, v5, v3

    if-eqz v5, :cond_d

    :cond_c
    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    invoke-virtual {v5, v1, v3, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHc:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHc:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGM:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGM:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v0, 0x2

    :goto_a
    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGM:[F

    array-length v1, v1

    if-ge v0, v1, :cond_e

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cHc:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGM:[F

    aget v4, v4, v0

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGM:[F

    add-int/lit8 v6, v0, 0x1

    aget v5, v5, v6

    iget-object v6, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v0, 0x2

    goto :goto_a

    :cond_d
    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    cmpl-float v5, v5, v4

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    cmpl-float v5, v5, v0

    if-nez v5, :cond_c

    :goto_b
    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v7

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v3

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const v1, 0x3e99999a    # 0.3f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_f

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHc:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGX:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_5

    :cond_e
    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHc:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGM:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/android/settingslib/graph/a;->cGM:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/android/settingslib/graph/a;->cGy:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_b

    :cond_f
    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGP:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/a;->cHc:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$Op;->DIFFERENCE:Landroid/graphics/Path$Op;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    goto/16 :goto_5

    :cond_10
    const/high16 v0, 0x3f000000    # 0.5f

    goto/16 :goto_6

    :cond_11
    const/4 v8, 0x0

    goto/16 :goto_7

    :cond_12
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGF:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v4, v5, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_9

    :cond_13
    move v0, v8

    goto/16 :goto_8

    :cond_14
    move v0, v1

    goto/16 :goto_1
.end method

.method public getBatteryLevel()I
    .locals 1

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGG:I

    return v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget v0, p0, Lcom/android/settingslib/graph/a;->cGL:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget v0, p0, Lcom/android/settingslib/graph/a;->cHb:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setBatteryLevel(I)V
    .locals 0

    iput p1, p0, Lcom/android/settingslib/graph/a;->cGG:I

    invoke-virtual {p0}, Lcom/android/settingslib/graph/a;->cmv()V

    return-void
.end method

.method public setBounds(IIII)V
    .locals 3

    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sub-int v0, p4, p2

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGQ:I

    sub-int v0, p3, p1

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGJ:I

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGS:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/settingslib/graph/a;->cGQ:I

    int-to-float v1, v1

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGS:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float v0, v0

    iput v0, p0, Lcom/android/settingslib/graph/a;->cGU:F

    return-void
.end method

.method public setCharging(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/graph/a;->cGY:Z

    invoke-virtual {p0}, Lcom/android/settingslib/graph/a;->cmv()V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHf:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGA:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGS:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cHa:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    iget-object v0, p0, Lcom/android/settingslib/graph/a;->cGX:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    return-void
.end method
