.class public Lcom/android/settingslib/graph/UsageGraph;
.super Landroid/view/View;
.source "UsageGraph.java"


# instance fields
.field private cHi:F

.field private cHj:F

.field private final cHk:Landroid/graphics/Paint;

.field private final cHl:Landroid/util/SparseIntArray;

.field private cHm:F

.field private final cHn:Landroid/graphics/Paint;

.field private final cHo:I

.field private cHp:Z

.field private cHq:I

.field private final cHr:Landroid/util/SparseIntArray;

.field private cHs:I

.field private final cHt:Landroid/graphics/Path;

.field private final cHu:I

.field private cHv:I

.field private cHw:Z

.field private final cHx:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v2, -0x1

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHr:Landroid/util/SparseIntArray;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    iput v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHj:F

    iput v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHi:F

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHm:F

    iput v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHq:I

    iput v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHs:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget v1, Lcom/android/settingslib/d;->cKB:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHo:I

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/CornerPathEffect;

    iget v3, p0, Lcom/android/settingslib/graph/UsageGraph;->cHo:I

    int-to-float v3, v3

    invoke-direct {v2, v3}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    sget v2, Lcom/android/settingslib/d;->cKC:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHx:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHx:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHk:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHk:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v1, Lcom/android/settingslib/d;->cKA:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    sget v2, Lcom/android/settingslib/d;->cKz:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/settingslib/graph/UsageGraph;->cHk:Landroid/graphics/Paint;

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, p0, Lcom/android/settingslib/graph/UsageGraph;->cHk:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/DashPathEffect;

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v1, v5, v6

    aput v2, v5, v7

    const/4 v1, 0x0

    invoke-direct {v4, v5, v1}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHk:Landroid/graphics/Paint;

    sget v2, Lcom/android/settingslib/c;->cKs:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget v1, Lcom/android/settingslib/d;->cKy:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHu:I

    return-void
.end method

.method private cmA(II)Z
    .locals 2

    sub-int v0, p2, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHo:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cmB()V
    .locals 9

    const/4 v1, 0x0

    iget-object v8, p0, Lcom/android/settingslib/graph/UsageGraph;->cHx:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/LinearGradient;

    invoke-virtual {p0}, Lcom/android/settingslib/graph/UsageGraph;->getHeight()I

    move-result v2

    int-to-float v4, v2

    iget v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHv:I

    const v3, 0x3e4ccccd    # 0.2f

    invoke-direct {p0, v2, v3}, Lcom/android/settingslib/graph/UsageGraph;->cmJ(IF)I

    move-result v5

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    const/4 v6, 0x0

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    return-void
.end method

.method private cmC()V
    .locals 8

    const/4 v0, 0x0

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/android/settingslib/graph/UsageGraph;->getWidth()I

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    move v1, v2

    move v3, v0

    :goto_0
    iget-object v4, p0, Lcom/android/settingslib/graph/UsageGraph;->cHr:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    if-ge v0, v4, :cond_4

    iget-object v4, p0, Lcom/android/settingslib/graph/UsageGraph;->cHr:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v4

    iget-object v5, p0, Lcom/android/settingslib/graph/UsageGraph;->cHr:Landroid/util/SparseIntArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v5

    if-ne v5, v2, :cond_2

    iget-object v4, p0, Lcom/android/settingslib/graph/UsageGraph;->cHr:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_1

    if-eq v1, v2, :cond_1

    iget-object v4, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    :cond_1
    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v1, v4, v2}, Landroid/util/SparseIntArray;->put(II)V

    move v1, v2

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    int-to-float v3, v4

    invoke-direct {p0, v3}, Lcom/android/settingslib/graph/UsageGraph;->cmF(F)I

    move-result v4

    int-to-float v3, v5

    invoke-direct {p0, v3}, Lcom/android/settingslib/graph/UsageGraph;->cmI(F)I

    move-result v3

    iget-object v5, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v5}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    if-lez v5, :cond_3

    iget-object v5, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    iget-object v6, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v6}, Landroid/util/SparseIntArray;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v5

    iget-object v6, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    iget-object v7, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v7}, Landroid/util/SparseIntArray;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v6

    if-eq v6, v2, :cond_3

    invoke-direct {p0, v5, v4}, Lcom/android/settingslib/graph/UsageGraph;->cmA(II)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_3

    invoke-direct {p0, v6, v3}, Lcom/android/settingslib/graph/UsageGraph;->cmA(II)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_3

    move v1, v3

    move v3, v4

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v5, v4, v3}, Landroid/util/SparseIntArray;->put(II)V

    move v3, v4

    goto :goto_1

    :cond_4
    return-void
.end method

.method private cmD(Landroid/graphics/Canvas;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v3}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method private cmE(Landroid/graphics/Canvas;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v2, v0

    iget-boolean v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHw:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHk:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method private cmF(F)I
    .locals 2

    iget v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHj:F

    div-float v0, p1, v0

    invoke-virtual {p0}, Lcom/android/settingslib/graph/UsageGraph;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private cmI(F)I
    .locals 3

    invoke-virtual {p0}, Lcom/android/settingslib/graph/UsageGraph;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHi:F

    div-float v1, p1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v1, v2, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private cmJ(IF)I
    .locals 2

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p2

    float-to-int v0, v0

    shl-int/lit8 v0, v0, 0x18

    const v1, 0xffffff

    or-int/2addr v0, v1

    and-int/2addr v0, p1

    return v0
.end method

.method private cmK(Landroid/graphics/Canvas;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    int-to-float v1, v0

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v4}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {v3, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/android/settingslib/graph/UsageGraph;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    invoke-virtual {p0}, Lcom/android/settingslib/graph/UsageGraph;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    int-to-float v2, v2

    int-to-float v3, v3

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHt:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHx:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method private cmz(ILandroid/graphics/Canvas;I)V
    .locals 0

    return-void
.end method


# virtual methods
.method cmG()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHr:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    return-void
.end method

.method public cmH(Landroid/util/SparseIntArray;)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHr:Landroid/util/SparseIntArray;

    invoke-virtual {p1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    invoke-virtual {p1, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHr:Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    invoke-direct {p0}, Lcom/android/settingslib/graph/UsageGraph;->cmC()V

    invoke-virtual {p0}, Lcom/android/settingslib/graph/UsageGraph;->postInvalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHm:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHs:I

    invoke-direct {p0, v2, p1, v0}, Lcom/android/settingslib/graph/UsageGraph;->cmz(ILandroid/graphics/Canvas;I)V

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHu:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHm:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHq:I

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settingslib/graph/UsageGraph;->cmz(ILandroid/graphics/Canvas;I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHu:I

    sub-int/2addr v0, v1

    const/4 v1, -0x1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settingslib/graph/UsageGraph;->cmz(ILandroid/graphics/Canvas;I)V

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHl:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHp:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/android/settingslib/graph/UsageGraph;->cmE(Landroid/graphics/Canvas;)V

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/settingslib/graph/UsageGraph;->cmK(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/android/settingslib/graph/UsageGraph;->cmD(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    invoke-direct {p0}, Lcom/android/settingslib/graph/UsageGraph;->cmB()V

    invoke-direct {p0}, Lcom/android/settingslib/graph/UsageGraph;->cmC()V

    return-void
.end method

.method setAccentColor(I)V
    .locals 2

    iput p1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHv:I

    iget-object v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHn:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHv:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-direct {p0}, Lcom/android/settingslib/graph/UsageGraph;->cmB()V

    invoke-virtual {p0}, Lcom/android/settingslib/graph/UsageGraph;->postInvalidate()V

    return-void
.end method

.method setDividerColors(II)V
    .locals 0

    iput p1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHq:I

    iput p2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHs:I

    return-void
.end method

.method setDividerLoc(I)V
    .locals 2

    int-to-float v0, p1

    iget v1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHi:F

    div-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v0, v1, v0

    iput v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHm:F

    return-void
.end method

.method setMax(II)V
    .locals 1

    int-to-float v0, p1

    iput v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHj:F

    int-to-float v0, p2

    iput v0, p0, Lcom/android/settingslib/graph/UsageGraph;->cHi:F

    return-void
.end method

.method setShowProjection(ZZ)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/graph/UsageGraph;->cHp:Z

    iput-boolean p2, p0, Lcom/android/settingslib/graph/UsageGraph;->cHw:Z

    invoke-virtual {p0}, Lcom/android/settingslib/graph/UsageGraph;->postInvalidate()V

    return-void
.end method
