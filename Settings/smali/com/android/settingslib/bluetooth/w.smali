.class Lcom/android/settingslib/bluetooth/w;
.super Lcom/android/settingslib/bluetooth/v;
.source "LocalBluetoothProfileManager.java"


# instance fields
.field final synthetic cEH:Lcom/android/settingslib/bluetooth/e;


# direct methods
.method constructor <init>(Lcom/android/settingslib/bluetooth/e;Lcom/android/settingslib/bluetooth/f;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/w;->cEH:Lcom/android/settingslib/bluetooth/e;

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/bluetooth/v;-><init>(Lcom/android/settingslib/bluetooth/e;Lcom/android/settingslib/bluetooth/f;)V

    return-void
.end method


# virtual methods
.method public ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/w;->cEG:Lcom/android/settingslib/bluetooth/f;

    check-cast v0, Lcom/android/settingslib/bluetooth/y;

    const-string/jumbo v1, "android.bluetooth.pan.extra.LOCAL_ROLE"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, p3, v1}, Lcom/android/settingslib/bluetooth/y;->clh(Landroid/bluetooth/BluetoothDevice;I)V

    invoke-super {p0, p1, p2, p3}, Lcom/android/settingslib/bluetooth/v;->ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method
