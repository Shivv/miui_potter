.class Lcom/android/settingslib/bluetooth/V;
.super Ljava/lang/Object;
.source "BluetoothEventManager.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/I;


# instance fields
.field final synthetic cGh:Lcom/android/settingslib/bluetooth/s;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/s;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/V;->cGh:Lcom/android/settingslib/bluetooth/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/V;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/V;-><init>(Lcom/android/settingslib/bluetooth/s;)V

    return-void
.end method


# virtual methods
.method public ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    const-string/jumbo v1, "android.intent.extra.DOCK_STATE"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/V;->cGh:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lcom/android/settingslib/bluetooth/b;->setVisible(Z)V

    :cond_0
    return-void
.end method
