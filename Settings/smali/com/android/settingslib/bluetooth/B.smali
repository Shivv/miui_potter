.class final Lcom/android/settingslib/bluetooth/B;
.super Ljava/lang/Object;
.source "A2dpSinkProfile.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/f;


# static fields
.field private static cFh:Z

.field static final cFm:[Landroid/os/ParcelUuid;


# instance fields
.field private cFg:Landroid/bluetooth/BluetoothA2dpSink;

.field private cFi:Z

.field private final cFj:Lcom/android/settingslib/bluetooth/d;

.field private final cFk:Lcom/android/settingslib/bluetooth/e;

.field private final cFl:Lcom/android/settingslib/bluetooth/t;


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/settingslib/bluetooth/B;->cFh:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/settingslib/bluetooth/B;->cFh:Z

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/os/ParcelUuid;

    sget-object v1, Landroid/bluetooth/BluetoothUuid;->AudioSource:Landroid/os/ParcelUuid;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Landroid/bluetooth/BluetoothUuid;->AdvAudioDist:Landroid/os/ParcelUuid;

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/settingslib/bluetooth/B;->cFm:[Landroid/os/ParcelUuid;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/B;->cFj:Lcom/android/settingslib/bluetooth/d;

    iput-object p3, p0, Lcom/android/settingslib/bluetooth/B;->cFl:Lcom/android/settingslib/bluetooth/t;

    iput-object p4, p0, Lcom/android/settingslib/bluetooth/B;->cFk:Lcom/android/settingslib/bluetooth/e;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFj:Lcom/android/settingslib/bluetooth/d;

    new-instance v1, Lcom/android/settingslib/bluetooth/af;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/settingslib/bluetooth/af;-><init>(Lcom/android/settingslib/bluetooth/B;Lcom/android/settingslib/bluetooth/af;)V

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/settingslib/bluetooth/d;->cjH(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)V

    return-void
.end method

.method static synthetic clA(Lcom/android/settingslib/bluetooth/B;)Lcom/android/settingslib/bluetooth/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFj:Lcom/android/settingslib/bluetooth/d;

    return-object v0
.end method

.method static synthetic clC(Lcom/android/settingslib/bluetooth/B;)Landroid/bluetooth/BluetoothA2dpSink;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    return-object v0
.end method

.method static synthetic clD(Lcom/android/settingslib/bluetooth/B;)Lcom/android/settingslib/bluetooth/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFk:Lcom/android/settingslib/bluetooth/e;

    return-object v0
.end method

.method static synthetic clx(Lcom/android/settingslib/bluetooth/B;Landroid/bluetooth/BluetoothA2dpSink;)Landroid/bluetooth/BluetoothA2dpSink;
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    return-object p1
.end method

.method static synthetic cly(Lcom/android/settingslib/bluetooth/B;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/bluetooth/B;->cFi:Z

    return p1
.end method

.method static synthetic clz(Lcom/android/settingslib/bluetooth/B;)Lcom/android/settingslib/bluetooth/t;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFl:Lcom/android/settingslib/bluetooth/t;

    return-object v0
.end method


# virtual methods
.method public asi(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/B;->clw()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "A2dpSinkProfile"

    const-string/jumbo v1, "Ignoring Connect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothA2dpSink;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public asj(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothA2dpSink;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public ask(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothA2dpSink;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    return v0
.end method

.method public asl(Landroid/bluetooth/BluetoothClass;)I
    .locals 1

    sget v0, Lcom/android/settingslib/e;->cKE:I

    return v0
.end method

.method public asm(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    sget v0, Lcom/android/settingslib/i;->cMa:I

    return v0
.end method

.method public asn()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public aso(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/B;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-static {v0}, Lcom/android/settingslib/bluetooth/J;->cmk(I)I

    move-result v0

    return v0

    :pswitch_1
    sget v0, Lcom/android/settingslib/i;->cLE:I

    return v0

    :pswitch_2
    sget v0, Lcom/android/settingslib/i;->cLD:I

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public asp()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public asq()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public asr(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public ass()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settingslib/bluetooth/B;->cFi:Z

    return v0
.end method

.method public ast(Landroid/bluetooth/BluetoothDevice;Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    if-nez v0, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method clB()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public clw()Ljava/util/List;
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x3

    filled-new-array {v1, v2, v3}, [I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothA2dpSink;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected finalize()V
    .locals 3

    sget-boolean v0, Lcom/android/settingslib/bluetooth/B;->cFh:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "A2dpSinkProfile"

    const-string/jumbo v1, "finalize()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/B;->cFg:Landroid/bluetooth/BluetoothA2dpSink;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "A2dpSinkProfile"

    const-string/jumbo v2, "Error cleaning up A2DP proxy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "A2DPSink"

    return-object v0
.end method
