.class abstract Lcom/android/settingslib/bluetooth/l;
.super Ljava/lang/Object;
.source "BluetoothDeviceFilter.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/h;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/l;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/l;-><init>()V

    return-void
.end method


# virtual methods
.method public atm(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settingslib/bluetooth/l;->ckj([Landroid/os/ParcelUuid;Landroid/bluetooth/BluetoothClass;)Z

    move-result v0

    return v0
.end method

.method abstract ckj([Landroid/os/ParcelUuid;Landroid/bluetooth/BluetoothClass;)Z
.end method
