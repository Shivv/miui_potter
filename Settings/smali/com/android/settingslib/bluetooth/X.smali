.class final Lcom/android/settingslib/bluetooth/X;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothEventManager.java"


# instance fields
.field final synthetic cGj:Lcom/android/settingslib/bluetooth/s;


# direct methods
.method constructor <init>(Lcom/android/settingslib/bluetooth/s;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/X;->cGj:Lcom/android/settingslib/bluetooth/s;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/X;->cGj:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v2}, Lcom/android/settingslib/bluetooth/s;->ckx(Lcom/android/settingslib/bluetooth/s;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/bluetooth/I;

    if-eqz v1, :cond_0

    invoke-interface {v1, p1, p2, v0}, Lcom/android/settingslib/bluetooth/I;->ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V

    :cond_0
    return-void
.end method
