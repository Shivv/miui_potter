.class final Lcom/android/settingslib/bluetooth/ab;
.super Ljava/lang/Object;
.source "PanProfile.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# instance fields
.field final synthetic cGn:Lcom/android/settingslib/bluetooth/y;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/y;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/ab;->cGn:Lcom/android/settingslib/bluetooth/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/y;Lcom/android/settingslib/bluetooth/ab;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/ab;-><init>(Lcom/android/settingslib/bluetooth/y;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 2

    invoke-static {}, Lcom/android/settingslib/bluetooth/y;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PanProfile"

    const-string/jumbo v1, "Bluetooth service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ab;->cGn:Lcom/android/settingslib/bluetooth/y;

    check-cast p2, Landroid/bluetooth/BluetoothPan;

    invoke-static {v0, p2}, Lcom/android/settingslib/bluetooth/y;->cle(Lcom/android/settingslib/bluetooth/y;Landroid/bluetooth/BluetoothPan;)Landroid/bluetooth/BluetoothPan;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ab;->cGn:Lcom/android/settingslib/bluetooth/y;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settingslib/bluetooth/y;->clf(Lcom/android/settingslib/bluetooth/y;Z)Z

    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 2

    invoke-static {}, Lcom/android/settingslib/bluetooth/y;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PanProfile"

    const-string/jumbo v1, "Bluetooth service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ab;->cGn:Lcom/android/settingslib/bluetooth/y;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settingslib/bluetooth/y;->clf(Lcom/android/settingslib/bluetooth/y;Z)Z

    return-void
.end method
