.class final Lcom/android/settingslib/bluetooth/ad;
.super Ljava/lang/Object;
.source "MapClientProfile.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# instance fields
.field final synthetic cGp:Lcom/android/settingslib/bluetooth/x;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/x;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/x;Lcom/android/settingslib/bluetooth/ad;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/ad;-><init>(Lcom/android/settingslib/bluetooth/x;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 5

    invoke-static {}, Lcom/android/settingslib/bluetooth/x;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MapClientProfile"

    const-string/jumbo v1, "Bluetooth service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    check-cast p2, Landroid/bluetooth/BluetoothMapClient;

    invoke-static {v0, p2}, Lcom/android/settingslib/bluetooth/x;->ckZ(Lcom/android/settingslib/bluetooth/x;Landroid/bluetooth/BluetoothMapClient;)Landroid/bluetooth/BluetoothMapClient;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/x;->cla(Lcom/android/settingslib/bluetooth/x;)Landroid/bluetooth/BluetoothMapClient;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothMapClient;->getConnectedDevices()Ljava/util/List;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    invoke-static {v1}, Lcom/android/settingslib/bluetooth/x;->clb(Lcom/android/settingslib/bluetooth/x;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "MapClientProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MapProfile found new device: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    invoke-static {v1}, Lcom/android/settingslib/bluetooth/x;->clb(Lcom/android/settingslib/bluetooth/x;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    invoke-static {v3}, Lcom/android/settingslib/bluetooth/x;->clc(Lcom/android/settingslib/bluetooth/x;)Lcom/android/settingslib/bluetooth/d;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    invoke-static {v4}, Lcom/android/settingslib/bluetooth/x;->cld(Lcom/android/settingslib/bluetooth/x;)Lcom/android/settingslib/bluetooth/e;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v0}, Lcom/android/settingslib/bluetooth/t;->ckP(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/e;Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v3}, Lcom/android/settingslib/bluetooth/b;->ciG(Lcom/android/settingslib/bluetooth/f;I)V

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjn()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/x;->cld(Lcom/android/settingslib/bluetooth/x;)Lcom/android/settingslib/bluetooth/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/e;->cjX()V

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settingslib/bluetooth/x;->ckY(Lcom/android/settingslib/bluetooth/x;Z)Z

    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public onServiceDisconnected(I)V
    .locals 2

    invoke-static {}, Lcom/android/settingslib/bluetooth/x;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MapClientProfile"

    const-string/jumbo v1, "Bluetooth service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/x;->cld(Lcom/android/settingslib/bluetooth/x;)Lcom/android/settingslib/bluetooth/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/e;->ckf()V

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/ad;->cGp:Lcom/android/settingslib/bluetooth/x;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settingslib/bluetooth/x;->ckY(Lcom/android/settingslib/bluetooth/x;Z)Z

    return-void
.end method
