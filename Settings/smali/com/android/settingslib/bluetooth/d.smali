.class public Lcom/android/settingslib/bluetooth/d;
.super Ljava/lang/Object;
.source "LocalBluetoothAdapter.java"


# static fields
.field private static cDM:Lcom/android/settingslib/bluetooth/d;


# instance fields
.field private cDN:I

.field private final cDO:Landroid/bluetooth/BluetoothAdapter;

.field private cDP:J

.field private cDQ:Lcom/android/settingslib/bluetooth/e;


# direct methods
.method private constructor <init>(Landroid/bluetooth/BluetoothAdapter;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/android/settingslib/bluetooth/d;->cDN:I

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    return-void
.end method

.method static declared-synchronized getInstance()Lcom/android/settingslib/bluetooth/d;
    .locals 3

    const-class v1, Lcom/android/settingslib/bluetooth/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/settingslib/bluetooth/d;->cDM:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/android/settingslib/bluetooth/d;

    invoke-direct {v2, v0}, Lcom/android/settingslib/bluetooth/d;-><init>(Landroid/bluetooth/BluetoothAdapter;)V

    sput-object v2, Lcom/android/settingslib/bluetooth/d;->cDM:Lcom/android/settingslib/bluetooth/d;

    :cond_0
    sget-object v0, Lcom/android/settingslib/bluetooth/d;->cDM:Lcom/android/settingslib/bluetooth/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public cjA(II)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p1, p2}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(II)Z

    move-result v0

    return v0
.end method

.method public cjB()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public cjC(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->setName(Ljava/lang/String;)Z

    return-void
.end method

.method public cjD()I
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getConnectionState()I

    move-result v0

    return v0
.end method

.method public cjE()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    return v0
.end method

.method public cjF()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_0
    return-void
.end method

.method public cjG()I
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getScanMode()I

    move-result v0

    return v0
.end method

.method cjH(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p1, p2, p3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    return-void
.end method

.method public cjI()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized cjJ()I
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/d;->cjQ()Z

    iget v0, p0, Lcom/android/settingslib/bluetooth/d;->cDN:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method cjK(Lcom/android/settingslib/bluetooth/e;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/d;->cDQ:Lcom/android/settingslib/bluetooth/e;

    return-void
.end method

.method public cjL()[Landroid/os/ParcelUuid;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v0

    return-object v0
.end method

.method cjM(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/settingslib/bluetooth/d;->cDN:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iput p1, p0, Lcom/android/settingslib/bluetooth/d;->cDN:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    const/16 v0, 0xc

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDQ:Lcom/android/settingslib/bluetooth/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDQ:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/e;->cjV()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cjN()J
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getDiscoveryEndMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public cjO()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cjP(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(I)Z

    return-void
.end method

.method cjQ()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    iget v1, p0, Lcom/android/settingslib/bluetooth/d;->cDN:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/d;->cjM(I)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public cjR()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    return-void
.end method

.method public cjS()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    return v0
.end method

.method public cjy(Z)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    const-string/jumbo v0, "support_bluetooth_boost"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "bluetooth_boost_value"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v4, :cond_1

    const-string/jumbo v0, "LocalBluetoothAdapter"

    const-string/jumbo v1, "setBluetoothEnabled: boost value error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    move v1, v0

    :goto_1
    if-eqz v1, :cond_5

    if-eqz p1, :cond_4

    const/16 v0, 0xb

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/d;->cjM(I)V

    :goto_3
    return v1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_2

    const-string/jumbo v1, "0x"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    :try_start_0
    const-string/jumbo v1, "LocalBluetoothAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setBluetoothEnabled : boostValue = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    new-instance v1, Landroid/util/BoostFramework;

    invoke-direct {v1}, Landroid/util/BoostFramework;-><init>()V

    if-eqz v1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "BluetoothApp="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/app/ActivityThread;->currentPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/util/BoostFramework;->perfHint(ILjava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "LocalBluetoothAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setBluetoothEnabled: set boost rec = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "LocalBluetoothAdapter"

    const-string/jumbo v1, "setBluetoothEnabled: set boost number format exception"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    move-result v0

    move v1, v0

    goto/16 :goto_1

    :cond_4
    const/16 v0, 0xd

    goto/16 :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/d;->cjQ()Z

    goto/16 :goto_3
.end method

.method public cjz(Z)V
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    if-nez v0, :cond_3

    if-nez p1, :cond_2

    iget-wide v0, p0, Lcom/android/settingslib/bluetooth/d;->cDP:J

    const-wide/32 v2, 0x493e0

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDQ:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/e;->ckb()Lcom/android/settingslib/bluetooth/E;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/E;->clQ()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDQ:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/e;->cjZ()Lcom/android/settingslib/bluetooth/B;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/B;->clB()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settingslib/bluetooth/d;->cDP:J

    :cond_3
    return-void
.end method

.method public getState()I
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/d;->cDO:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    return v0
.end method
