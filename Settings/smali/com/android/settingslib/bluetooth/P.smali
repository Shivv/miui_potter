.class Lcom/android/settingslib/bluetooth/P;
.super Ljava/lang/Object;
.source "BluetoothEventManager.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/I;


# instance fields
.field final synthetic cGb:Lcom/android/settingslib/bluetooth/s;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/s;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/P;->cGb:Lcom/android/settingslib/bluetooth/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/P;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/P;-><init>(Lcom/android/settingslib/bluetooth/s;)V

    return-void
.end method


# virtual methods
.method public ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/P;->cGb:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v1

    if-nez v1, :cond_0

    const-string/jumbo v0, "BluetoothEventManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "received ACTION_DISAPPEARED for an unknown device: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-static {v1}, Lcom/android/settingslib/bluetooth/t;->ckJ(Lcom/android/settingslib/bluetooth/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/P;->cGb:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckF(Lcom/android/settingslib/bluetooth/s;)Ljava/util/Collection;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/P;->cGb:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckF(Lcom/android/settingslib/bluetooth/s;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/a;

    invoke-interface {v0, v1}, Lcom/android/settingslib/bluetooth/a;->aqJ(Lcom/android/settingslib/bluetooth/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    monitor-exit v2

    :cond_2
    return-void
.end method
