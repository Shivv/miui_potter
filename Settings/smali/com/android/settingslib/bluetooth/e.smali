.class public Lcom/android/settingslib/bluetooth/e;
.super Ljava/lang/Object;
.source "LocalBluetoothProfileManager.java"


# instance fields
.field private cDR:Lcom/android/settingslib/bluetooth/x;

.field private final cDS:Lcom/android/settingslib/bluetooth/y;

.field private final cDT:Lcom/android/settingslib/bluetooth/s;

.field private cDU:Lcom/android/settingslib/bluetooth/z;

.field private final cDV:Z

.field private final cDW:Lcom/android/settingslib/bluetooth/A;

.field private final cDX:Lcom/android/settingslib/bluetooth/d;

.field private final cDY:Ljava/util/Map;

.field private cDZ:Lcom/android/settingslib/bluetooth/B;

.field private cEa:Lcom/android/settingslib/bluetooth/C;

.field private final cEb:Z

.field private cEc:Lcom/android/settingslib/bluetooth/D;

.field private cEd:Lcom/android/settingslib/bluetooth/E;

.field private final cEe:Ljava/util/Collection;

.field private final cEf:Lcom/android/settingslib/bluetooth/F;

.field private cEg:Lcom/android/settingslib/bluetooth/G;

.field private final cEh:Lcom/android/settingslib/bluetooth/t;

.field private cEi:Lcom/android/settingslib/bluetooth/H;

.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/s;)V
    .locals 4

    const v1, 0x11200e3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDY:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEe:Ljava/util/Collection;

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iput-object p3, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    iput-object p4, p0, Lcom/android/settingslib/bluetooth/e;->cDT:Lcom/android/settingslib/bluetooth/s;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settingslib/bluetooth/e;->cDV:Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settingslib/bluetooth/e;->cEb:Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/d;->cjK(Lcom/android/settingslib/bluetooth/e;)V

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDT:Lcom/android/settingslib/bluetooth/s;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/s;->ckB(Lcom/android/settingslib/bluetooth/e;)V

    invoke-virtual {p2}, Lcom/android/settingslib/bluetooth/d;->cjL()[Landroid/os/ParcelUuid;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/e;->cjU([Landroid/os/ParcelUuid;)V

    :cond_0
    new-instance v0, Lcom/android/settingslib/bluetooth/A;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, p1, v1, v2, p0}, Lcom/android/settingslib/bluetooth/A;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDW:Lcom/android/settingslib/bluetooth/A;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDW:Lcom/android/settingslib/bluetooth/A;

    const-string/jumbo v1, "HID"

    const-string/jumbo v2, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/android/settingslib/bluetooth/y;

    invoke-direct {v0, p1}, Lcom/android/settingslib/bluetooth/y;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDS:Lcom/android/settingslib/bluetooth/y;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDS:Lcom/android/settingslib/bluetooth/y;

    const-string/jumbo v1, "PAN"

    const-string/jumbo v2, "android.bluetooth.pan.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->ckg(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Adding local MAP profile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/settingslib/bluetooth/e;->cEb:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settingslib/bluetooth/x;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/settingslib/bluetooth/x;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDR:Lcom/android/settingslib/bluetooth/x;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDR:Lcom/android/settingslib/bluetooth/x;

    const-string/jumbo v1, "MAP Client"

    const-string/jumbo v2, "android.bluetooth.mapmce.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Adding local PBAP profile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settingslib/bluetooth/F;

    invoke-direct {v0, p1}, Lcom/android/settingslib/bluetooth/F;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEf:Lcom/android/settingslib/bluetooth/F;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEf:Lcom/android/settingslib/bluetooth/F;

    const-string/jumbo v1, "PBAP Server"

    const-string/jumbo v2, "android.bluetooth.pbap.intent.action.PBAP_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "LocalBluetoothProfileManager construction complete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    new-instance v0, Lcom/android/settingslib/bluetooth/H;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/settingslib/bluetooth/H;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEi:Lcom/android/settingslib/bluetooth/H;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEi:Lcom/android/settingslib/bluetooth/H;

    const-string/jumbo v1, "MAP"

    const-string/jumbo v2, "android.bluetooth.map.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic cjT(Lcom/android/settingslib/bluetooth/e;)Lcom/android/settingslib/bluetooth/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    return-object v0
.end method

.method private cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDT:Lcom/android/settingslib/bluetooth/s;

    new-instance v1, Lcom/android/settingslib/bluetooth/v;

    invoke-direct {v1, p0, p1}, Lcom/android/settingslib/bluetooth/v;-><init>(Lcom/android/settingslib/bluetooth/e;Lcom/android/settingslib/bluetooth/f;)V

    invoke-virtual {v0, p3, v1}, Lcom/android/settingslib/bluetooth/s;->cks(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDY:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic cke(Lcom/android/settingslib/bluetooth/e;)Lcom/android/settingslib/bluetooth/t;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    return-object v0
.end method

.method private ckg(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDT:Lcom/android/settingslib/bluetooth/s;

    new-instance v1, Lcom/android/settingslib/bluetooth/w;

    invoke-direct {v1, p0, p1}, Lcom/android/settingslib/bluetooth/w;-><init>(Lcom/android/settingslib/bluetooth/e;Lcom/android/settingslib/bluetooth/f;)V

    invoke-virtual {v0, p3, v1}, Lcom/android/settingslib/bluetooth/s;->cks(Ljava/lang/String;Lcom/android/settingslib/bluetooth/I;)V

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDY:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method cjU([Landroid/os/ParcelUuid;)V
    .locals 4

    sget-object v0, Landroid/bluetooth/BluetoothUuid;->AudioSource:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEd:Lcom/android/settingslib/bluetooth/E;

    if-nez v0, :cond_0

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Adding local A2DP SRC profile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settingslib/bluetooth/E;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/settingslib/bluetooth/E;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEd:Lcom/android/settingslib/bluetooth/E;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEd:Lcom/android/settingslib/bluetooth/E;

    const-string/jumbo v1, "A2DP"

    const-string/jumbo v2, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->AudioSink:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    if-nez v0, :cond_1

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Adding local A2DP Sink profile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settingslib/bluetooth/B;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/settingslib/bluetooth/B;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    const-string/jumbo v1, "A2DPSink"

    const-string/jumbo v2, "android.bluetooth.a2dp-sink.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree_AG:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/bluetooth/BluetoothUuid;->HSP_AG:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEa:Lcom/android/settingslib/bluetooth/C;

    if-nez v0, :cond_3

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Adding local HEADSET profile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settingslib/bluetooth/C;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/settingslib/bluetooth/C;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEa:Lcom/android/settingslib/bluetooth/C;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEa:Lcom/android/settingslib/bluetooth/C;

    const-string/jumbo v1, "HEADSET"

    const-string/jumbo v2, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_2
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEg:Lcom/android/settingslib/bluetooth/G;

    if-nez v0, :cond_4

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Adding local HfpClient profile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settingslib/bluetooth/G;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/settingslib/bluetooth/G;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEg:Lcom/android/settingslib/bluetooth/G;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEg:Lcom/android/settingslib/bluetooth/G;

    const-string/jumbo v1, "HEADSET_CLIENT"

    const-string/jumbo v2, "android.bluetooth.headsetclient.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_3
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->MNS:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDR:Lcom/android/settingslib/bluetooth/x;

    if-nez v0, :cond_5

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Adding local Map Client profile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settingslib/bluetooth/x;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/settingslib/bluetooth/x;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDR:Lcom/android/settingslib/bluetooth/x;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDR:Lcom/android/settingslib/bluetooth/x;

    const-string/jumbo v1, "MAP Client"

    const-string/jumbo v2, "android.bluetooth.mapmce.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_4
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->ObexObjectPush:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEc:Lcom/android/settingslib/bluetooth/D;

    if-nez v0, :cond_6

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Adding local OPP profile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settingslib/bluetooth/D;

    invoke-direct {v0}, Lcom/android/settingslib/bluetooth/D;-><init>()V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEc:Lcom/android/settingslib/bluetooth/D;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDY:Ljava/util/Map;

    const-string/jumbo v1, "OPP"

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cEc:Lcom/android/settingslib/bluetooth/D;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    :goto_5
    iget-boolean v0, p0, Lcom/android/settingslib/bluetooth/e;->cDV:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDU:Lcom/android/settingslib/bluetooth/z;

    if-nez v0, :cond_7

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Adding local PBAP Client profile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settingslib/bluetooth/z;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/e;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/e;->cEh:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/android/settingslib/bluetooth/z;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDU:Lcom/android/settingslib/bluetooth/z;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDU:Lcom/android/settingslib/bluetooth/z;

    const-string/jumbo v1, "PbapClient"

    const-string/jumbo v2, "android.bluetooth.pbap.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/bluetooth/e;->cjY(Lcom/android/settingslib/bluetooth/f;Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    :goto_6
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDT:Lcom/android/settingslib/bluetooth/s;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/s;->ckD()V

    return-void

    :cond_8
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEd:Lcom/android/settingslib/bluetooth/E;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Warning: A2DP profile was previously added but the UUID is now missing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Warning: A2DP Sink profile was previously added but the UUID is now missing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_a
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEa:Lcom/android/settingslib/bluetooth/C;

    if-eqz v0, :cond_3

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Warning: HEADSET profile was previously added but the UUID is now missing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_b
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEg:Lcom/android/settingslib/bluetooth/G;

    if-eqz v0, :cond_c

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Warning: Hfp Client profile was previously added but the UUID is now missing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_c
    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Handsfree Uuid not found."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_d
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDR:Lcom/android/settingslib/bluetooth/x;

    if-eqz v0, :cond_e

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Warning: MAP Client profile was previously added but the UUID is now missing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_e
    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "MAP Client Uuid not found."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_f
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEc:Lcom/android/settingslib/bluetooth/D;

    if-eqz v0, :cond_6

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Warning: OPP profile was previously added but the UUID is now missing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_10
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDU:Lcom/android/settingslib/bluetooth/z;

    if-eqz v0, :cond_7

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Warning: PBAP Client profile was previously added but the UUID is now missing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6
.end method

.method cjV()V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDX:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjL()[Landroid/os/ParcelUuid;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/bluetooth/e;->cjU([Landroid/os/ParcelUuid;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDT:Lcom/android/settingslib/bluetooth/s;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/s;->cku()Z

    return-void
.end method

.method public cjW()Lcom/android/settingslib/bluetooth/H;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEi:Lcom/android/settingslib/bluetooth/H;

    return-object v0
.end method

.method cjX()V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEe:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/u;

    invoke-interface {v0}, Lcom/android/settingslib/bluetooth/u;->ckV()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public cjZ()Lcom/android/settingslib/bluetooth/B;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/B;->ass()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    return-object v0

    :cond_0
    return-object v1
.end method

.method declared-synchronized cka([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;Ljava/util/Collection;Ljava/util/Collection;ZLandroid/bluetooth/BluetoothDevice;)V
    .locals 4

    const/4 v3, 0x2

    monitor-enter p0

    :try_start_0
    invoke-interface {p4}, Ljava/util/Collection;->clear()V

    invoke-interface {p4, p3}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Current Profiles"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p3}, Ljava/util/Collection;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEa:Lcom/android/settingslib/bluetooth/C;

    if-eqz v0, :cond_3

    sget-object v0, Landroid/bluetooth/BluetoothUuid;->HSP_AG:Landroid/os/ParcelUuid;

    invoke-static {p2, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/bluetooth/BluetoothUuid;->HSP:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree_AG:Landroid/os/ParcelUuid;

    invoke-static {p2, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEa:Lcom/android/settingslib/bluetooth/C;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEa:Lcom/android/settingslib/bluetooth/C;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEg:Lcom/android/settingslib/bluetooth/G;

    if-eqz v0, :cond_4

    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree_AG:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    invoke-static {p2, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEg:Lcom/android/settingslib/bluetooth/G;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEg:Lcom/android/settingslib/bluetooth/G;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_4
    sget-object v0, Lcom/android/settingslib/bluetooth/E;->cFu:[Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->containsAnyUuid([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEd:Lcom/android/settingslib/bluetooth/E;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEd:Lcom/android/settingslib/bluetooth/E;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEd:Lcom/android/settingslib/bluetooth/E;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_5
    sget-object v0, Lcom/android/settingslib/bluetooth/B;->cFm:[Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->containsAnyUuid([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDZ:Lcom/android/settingslib/bluetooth/B;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_6
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->ObexObjectPush:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEc:Lcom/android/settingslib/bluetooth/D;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEc:Lcom/android/settingslib/bluetooth/D;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEc:Lcom/android/settingslib/bluetooth/D;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_7
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Hid:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Hogp:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDW:Lcom/android/settingslib/bluetooth/A;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDW:Lcom/android/settingslib/bluetooth/A;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDW:Lcom/android/settingslib/bluetooth/A;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_9
    if-eqz p5, :cond_a

    const-string/jumbo v0, "LocalBluetoothProfileManager"

    const-string/jumbo v1, "Valid PAN-NAP connection exists."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->NAP:Landroid/os/ParcelUuid;

    invoke-static {p1, v0}, Landroid/bluetooth/BluetoothUuid;->isUuidPresent([Landroid/os/ParcelUuid;Landroid/os/ParcelUuid;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDS:Lcom/android/settingslib/bluetooth/y;

    if-eqz v0, :cond_10

    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDS:Lcom/android/settingslib/bluetooth/y;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDS:Lcom/android/settingslib/bluetooth/y;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_b
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEi:Lcom/android/settingslib/bluetooth/H;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEi:Lcom/android/settingslib/bluetooth/H;

    invoke-virtual {v0, p6}, Lcom/android/settingslib/bluetooth/H;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    if-ne v0, v3, :cond_c

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEi:Lcom/android/settingslib/bluetooth/H;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEi:Lcom/android/settingslib/bluetooth/H;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEi:Lcom/android/settingslib/bluetooth/H;

    const/4 v1, 0x1

    invoke-virtual {v0, p6, v1}, Lcom/android/settingslib/bluetooth/H;->ast(Landroid/bluetooth/BluetoothDevice;Z)V

    :cond_c
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEf:Lcom/android/settingslib/bluetooth/F;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEf:Lcom/android/settingslib/bluetooth/F;

    invoke-virtual {v0, p6}, Lcom/android/settingslib/bluetooth/F;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    if-ne v0, v3, :cond_d

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEf:Lcom/android/settingslib/bluetooth/F;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEf:Lcom/android/settingslib/bluetooth/F;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEf:Lcom/android/settingslib/bluetooth/F;

    const/4 v1, 0x1

    invoke-virtual {v0, p6, v1}, Lcom/android/settingslib/bluetooth/F;->ast(Landroid/bluetooth/BluetoothDevice;Z)V

    :cond_d
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDR:Lcom/android/settingslib/bluetooth/x;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDR:Lcom/android/settingslib/bluetooth/x;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDR:Lcom/android/settingslib/bluetooth/x;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_e
    iget-boolean v0, p0, Lcom/android/settingslib/bluetooth/e;->cDV:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDU:Lcom/android/settingslib/bluetooth/z;

    invoke-interface {p3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDU:Lcom/android/settingslib/bluetooth/z;

    invoke-interface {p4, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_f
    const-string/jumbo v0, "LocalBluetoothProfileManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "New Profiles"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_10
    if-eqz p5, :cond_b

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ckb()Lcom/android/settingslib/bluetooth/E;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEd:Lcom/android/settingslib/bluetooth/E;

    return-object v0
.end method

.method public ckc()Lcom/android/settingslib/bluetooth/F;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEf:Lcom/android/settingslib/bluetooth/F;

    return-object v0
.end method

.method public ckd(Ljava/lang/String;)Lcom/android/settingslib/bluetooth/f;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDY:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    return-object v0
.end method

.method ckf()V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cEe:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/u;

    invoke-interface {v0}, Lcom/android/settingslib/bluetooth/u;->onServiceDisconnected()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public ckh()Lcom/android/settingslib/bluetooth/z;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/e;->cDU:Lcom/android/settingslib/bluetooth/z;

    return-object v0
.end method
