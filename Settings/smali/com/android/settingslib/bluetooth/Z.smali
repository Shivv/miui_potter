.class final Lcom/android/settingslib/bluetooth/Z;
.super Ljava/lang/Object;
.source "HidProfile.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# instance fields
.field final synthetic cGl:Lcom/android/settingslib/bluetooth/A;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/A;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/A;Lcom/android/settingslib/bluetooth/Z;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/Z;-><init>(Lcom/android/settingslib/bluetooth/A;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 5

    invoke-static {}, Lcom/android/settingslib/bluetooth/A;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "HidProfile"

    const-string/jumbo v1, "Bluetooth service connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    check-cast p2, Landroid/bluetooth/BluetoothInputDevice;

    invoke-static {v0, p2}, Lcom/android/settingslib/bluetooth/A;->clt(Lcom/android/settingslib/bluetooth/A;Landroid/bluetooth/BluetoothInputDevice;)Landroid/bluetooth/BluetoothInputDevice;

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/A;->clu(Lcom/android/settingslib/bluetooth/A;)Landroid/bluetooth/BluetoothInputDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothInputDevice;->getConnectedDevices()Ljava/util/List;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    invoke-static {v1}, Lcom/android/settingslib/bluetooth/A;->clq(Lcom/android/settingslib/bluetooth/A;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "HidProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "HidProfile found new device: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    invoke-static {v1}, Lcom/android/settingslib/bluetooth/A;->clq(Lcom/android/settingslib/bluetooth/A;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    invoke-static {v3}, Lcom/android/settingslib/bluetooth/A;->clv(Lcom/android/settingslib/bluetooth/A;)Lcom/android/settingslib/bluetooth/d;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    invoke-static {v4}, Lcom/android/settingslib/bluetooth/A;->cls(Lcom/android/settingslib/bluetooth/A;)Lcom/android/settingslib/bluetooth/e;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v0}, Lcom/android/settingslib/bluetooth/t;->ckP(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/e;Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v3}, Lcom/android/settingslib/bluetooth/b;->ciG(Lcom/android/settingslib/bluetooth/f;I)V

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjn()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settingslib/bluetooth/A;->clp(Lcom/android/settingslib/bluetooth/A;Z)Z

    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public onServiceDisconnected(I)V
    .locals 2

    invoke-static {}, Lcom/android/settingslib/bluetooth/A;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "HidProfile"

    const-string/jumbo v1, "Bluetooth service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/Z;->cGl:Lcom/android/settingslib/bluetooth/A;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settingslib/bluetooth/A;->clp(Lcom/android/settingslib/bluetooth/A;Z)Z

    return-void
.end method
