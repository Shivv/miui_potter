.class Lcom/android/settingslib/bluetooth/U;
.super Ljava/lang/Object;
.source "BluetoothEventManager.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/I;


# instance fields
.field final synthetic cGg:Lcom/android/settingslib/bluetooth/s;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/s;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/U;->cGg:Lcom/android/settingslib/bluetooth/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/s;Lcom/android/settingslib/bluetooth/U;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/U;-><init>(Lcom/android/settingslib/bluetooth/s;)V

    return-void
.end method


# virtual methods
.method public ckW(Landroid/content/Context;Landroid/content/Intent;Landroid/bluetooth/BluetoothDevice;)V
    .locals 2

    if-nez p3, :cond_0

    const-string/jumbo v0, "BluetoothEventManager"

    const-string/jumbo v1, "ACTION_PAIRING_CANCEL with no EXTRA_DEVICE"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/U;->cGg:Lcom/android/settingslib/bluetooth/s;

    invoke-static {v0}, Lcom/android/settingslib/bluetooth/s;->ckv(Lcom/android/settingslib/bluetooth/s;)Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "BluetoothEventManager"

    const-string/jumbo v1, "ACTION_PAIRING_CANCEL with no cached device"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    sget v1, Lcom/android/settingslib/i;->cLU:I

    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v1}, Lcom/android/settingslib/bluetooth/J;->cmj(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_2
    return-void
.end method
