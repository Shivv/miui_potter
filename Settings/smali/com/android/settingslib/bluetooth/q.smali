.class public Lcom/android/settingslib/bluetooth/q;
.super Ljava/lang/Object;
.source "LocalBluetoothManager.java"


# static fields
.field private static cEr:Lcom/android/settingslib/bluetooth/q;


# instance fields
.field private cEn:Landroid/content/Context;

.field private final cEo:Lcom/android/settingslib/bluetooth/s;

.field private final cEp:Lcom/android/settingslib/bluetooth/d;

.field private final cEq:Lcom/android/settingslib/bluetooth/e;

.field private final cEs:Lcom/android/settingslib/bluetooth/t;

.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/bluetooth/d;Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/q;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/q;->cEp:Lcom/android/settingslib/bluetooth/d;

    new-instance v0, Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, p2, p0}, Lcom/android/settingslib/bluetooth/t;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/q;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEs:Lcom/android/settingslib/bluetooth/t;

    new-instance v0, Lcom/android/settingslib/bluetooth/s;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/q;->cEp:Lcom/android/settingslib/bluetooth/d;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/q;->cEs:Lcom/android/settingslib/bluetooth/t;

    invoke-direct {v0, v1, v2, p2}, Lcom/android/settingslib/bluetooth/s;-><init>(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEo:Lcom/android/settingslib/bluetooth/s;

    new-instance v0, Lcom/android/settingslib/bluetooth/e;

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/q;->cEp:Lcom/android/settingslib/bluetooth/d;

    iget-object v2, p0, Lcom/android/settingslib/bluetooth/q;->cEs:Lcom/android/settingslib/bluetooth/t;

    iget-object v3, p0, Lcom/android/settingslib/bluetooth/q;->cEo:Lcom/android/settingslib/bluetooth/s;

    invoke-direct {v0, p2, v1, v2, v3}, Lcom/android/settingslib/bluetooth/e;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/s;)V

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEq:Lcom/android/settingslib/bluetooth/e;

    return-void
.end method

.method public static declared-synchronized ckk(Landroid/content/Context;Lcom/android/settingslib/bluetooth/r;)Lcom/android/settingslib/bluetooth/q;
    .locals 4

    const/4 v2, 0x0

    const-class v1, Lcom/android/settingslib/bluetooth/q;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/settingslib/bluetooth/q;->cEr:Lcom/android/settingslib/bluetooth/q;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/android/settingslib/bluetooth/d;->getInstance()Lcom/android/settingslib/bluetooth/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    monitor-exit v1

    return-object v2

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/android/settingslib/bluetooth/q;

    invoke-direct {v3, v0, v2}, Lcom/android/settingslib/bluetooth/q;-><init>(Lcom/android/settingslib/bluetooth/d;Landroid/content/Context;)V

    sput-object v3, Lcom/android/settingslib/bluetooth/q;->cEr:Lcom/android/settingslib/bluetooth/q;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/android/settingslib/bluetooth/q;->cEr:Lcom/android/settingslib/bluetooth/q;

    invoke-interface {p1, v2, v0}, Lcom/android/settingslib/bluetooth/r;->atw(Landroid/content/Context;Lcom/android/settingslib/bluetooth/q;)V

    :cond_1
    sget-object v0, Lcom/android/settingslib/bluetooth/q;->cEr:Lcom/android/settingslib/bluetooth/q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public ckl()Lcom/android/settingslib/bluetooth/d;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEp:Lcom/android/settingslib/bluetooth/d;

    return-object v0
.end method

.method public ckm()Lcom/android/settingslib/bluetooth/t;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEs:Lcom/android/settingslib/bluetooth/t;

    return-object v0
.end method

.method public ckn()Lcom/android/settingslib/bluetooth/s;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEo:Lcom/android/settingslib/bluetooth/s;

    return-object v0
.end method

.method public cko()Lcom/android/settingslib/bluetooth/e;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEq:Lcom/android/settingslib/bluetooth/e;

    return-object v0
.end method

.method public declared-synchronized ckp(Landroid/content/Context;)V
    .locals 2

    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    const-string/jumbo v0, "LocalBluetoothManager"

    const-string/jumbo v1, "setting foreground activity to non-null context"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/q;->cEn:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEn:Landroid/content/Context;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "LocalBluetoothManager"

    const-string/jumbo v1, "setting foreground activity to null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEn:Landroid/content/Context;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ckq()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEn:Landroid/content/Context;

    return-object v0
.end method

.method public ckr()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/q;->cEn:Landroid/content/Context;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
