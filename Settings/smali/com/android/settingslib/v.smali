.class public Lcom/android/settingslib/v;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static final cQu:[I

.field private static cQv:Ljava/lang/String;

.field private static cQw:[Landroid/content/pm/Signature;

.field private static cQx:Ljava/lang/String;

.field private static cQy:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const v0, 0x10804d2

    const v1, 0x10804d3

    const v2, 0x10804d4

    const v3, 0x10804d5

    const v4, 0x10804d6

    filled-new-array {v0, v1, v2, v3, v4}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settingslib/v;->cQu:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cqA(Landroid/content/res/Resources;Ljava/lang/String;)Z
    .locals 1

    const v0, 0x1040132

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static cqB(Landroid/content/Intent;)I
    .locals 3

    const-string/jumbo v0, "level"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "scale"

    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    return v0
.end method

.method private static cqC(D)Ljava/lang/String;
    .locals 2

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static cqD(Landroid/net/ConnectivityManager;)I
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/net/ConnectivityManager;->getTetherableUsbRegexs()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/net/ConnectivityManager;->getTetherableWifiRegexs()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/net/ConnectivityManager;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    move-result-object v4

    array-length v0, v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    array-length v3, v3

    if-eqz v3, :cond_1

    move v3, v1

    :goto_1
    array-length v4, v4

    if-eqz v4, :cond_2

    :goto_2
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    sget v0, Lcom/android/settingslib/i;->cMW:I

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    sget v0, Lcom/android/settingslib/i;->cMW:I

    return v0

    :cond_4
    if-eqz v3, :cond_5

    if-eqz v1, :cond_5

    sget v0, Lcom/android/settingslib/i;->cMW:I

    return v0

    :cond_5
    if-eqz v3, :cond_6

    sget v0, Lcom/android/settingslib/i;->cNa:I

    return v0

    :cond_6
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    sget v0, Lcom/android/settingslib/i;->cMZ:I

    return v0

    :cond_7
    if-eqz v0, :cond_8

    sget v0, Lcom/android/settingslib/i;->cMY:I

    return v0

    :cond_8
    sget v0, Lcom/android/settingslib/i;->cMX:I

    return v0
.end method

.method public static cqE(DZ)Ljava/lang/String;
    .locals 2

    if-eqz p2, :cond_0

    double-to-float v0, p0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    :goto_0
    invoke-static {v0}, Lcom/android/settingslib/v;->cqH(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    double-to-int v0, p0

    goto :goto_0
.end method

.method public static cqF(JJ)Ljava/lang/String;
    .locals 4

    long-to-double v0, p0

    long-to-double v2, p2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Lcom/android/settingslib/v;->cqC(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static cqG(Landroid/content/pm/PackageInfo;)Landroid/content/pm/Signature;
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object v0, v0, v2

    return-object v0

    :cond_0
    return-object v1
.end method

.method public static cqH(I)Ljava/lang/String;
    .locals 4

    int-to-double v0, p0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Lcom/android/settingslib/v;->cqC(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static cqI(Landroid/content/Context;)I
    .locals 1

    const v0, 0x1010435

    invoke-static {p0, v0}, Lcom/android/settingslib/v;->cqJ(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static cqJ(Landroid/content/Context;I)I
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    new-array v0, v0, [I

    aput p1, v0, v1

    invoke-virtual {p0, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return v1
.end method

.method private static cqK(Landroid/content/pm/PackageManager;)Landroid/content/pm/Signature;
    .locals 2

    :try_start_0
    const-string/jumbo v0, "android"

    const/16 v1, 0x40

    invoke-virtual {p0, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/v;->cqG(Landroid/content/pm/PackageInfo;)Landroid/content/pm/Signature;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return-object v0
.end method

.method public static cqL(Landroid/content/Context;Landroid/content/pm/UserInfo;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v1

    if-eqz v1, :cond_1

    sget v0, Lcom/android/settingslib/i;->cMB:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {p1}, Landroid/content/pm/UserInfo;->isGuest()Z

    move-result v1

    if-eqz v1, :cond_2

    sget v0, Lcom/android/settingslib/i;->cNe:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    if-nez v0, :cond_4

    if-eqz p1, :cond_4

    iget v0, p1, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :cond_3
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settingslib/i;->cMN:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    if-nez p1, :cond_3

    sget v0, Lcom/android/settingslib/i;->cNc:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static cqx(Landroid/content/Context;Landroid/os/UserManager;Landroid/content/pm/UserInfo;)Lcom/android/settingslib/e/a;
    .locals 3

    invoke-static {p0}, Lcom/android/settingslib/e/a;->cnN(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p2}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x108033b

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Lcom/android/settingslib/e/a;

    invoke-direct {v2, v0}, Lcom/android/settingslib/e/a;-><init>(I)V

    invoke-virtual {v2, v1}, Lcom/android/settingslib/e/a;->cnK(Landroid/graphics/Bitmap;)Lcom/android/settingslib/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/e/a;->cnJ()Lcom/android/settingslib/e/a;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v1, p2, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget v1, p2, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {p1, v1}, Landroid/os/UserManager;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v2, Lcom/android/settingslib/e/a;

    invoke-direct {v2, v0}, Lcom/android/settingslib/e/a;-><init>(I)V

    invoke-virtual {v2, v1}, Lcom/android/settingslib/e/a;->cnK(Landroid/graphics/Bitmap;)Lcom/android/settingslib/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/e/a;->cnJ()Lcom/android/settingslib/e/a;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v1, Lcom/android/settingslib/e/a;

    invoke-direct {v1, v0}, Lcom/android/settingslib/e/a;-><init>(I)V

    iget v0, p2, Landroid/content/pm/UserInfo;->id:I

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/internal/util/UserIcons;->getDefaultUserIcon(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/settingslib/e/a;->cnO(Landroid/graphics/drawable/Drawable;)Lcom/android/settingslib/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/e/a;->cnJ()Lcom/android/settingslib/e/a;

    move-result-object v0

    return-object v0
.end method

.method public static cqy(Landroid/content/Context;I)I
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources;->getColorStateList(ILandroid/content/res/Resources$Theme;)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    return v0
.end method

.method public static cqz(Landroid/content/res/Resources;Landroid/content/Intent;)Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, "status"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    sget v0, Lcom/android/settingslib/i;->cLw:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    sget v0, Lcom/android/settingslib/i;->cLy:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    sget v0, Lcom/android/settingslib/i;->cLA:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    sget v0, Lcom/android/settingslib/i;->cLz:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget v0, Lcom/android/settingslib/i;->cLB:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isSystemPackage(Landroid/content/res/Resources;Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInfo;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v3, 0x0

    sget-object v1, Lcom/android/settingslib/v;->cQw:[Landroid/content/pm/Signature;

    if-nez v1, :cond_0

    new-array v1, v0, [Landroid/content/pm/Signature;

    invoke-static {p1}, Lcom/android/settingslib/v;->cqK(Landroid/content/pm/PackageManager;)Landroid/content/pm/Signature;

    move-result-object v2

    aput-object v2, v1, v3

    sput-object v1, Lcom/android/settingslib/v;->cQw:[Landroid/content/pm/Signature;

    :cond_0
    sget-object v1, Lcom/android/settingslib/v;->cQv:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/content/pm/PackageManager;->getPermissionControllerPackageName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/settingslib/v;->cQv:Ljava/lang/String;

    :cond_1
    sget-object v1, Lcom/android/settingslib/v;->cQy:Ljava/lang/String;

    if-nez v1, :cond_2

    invoke-virtual {p1}, Landroid/content/pm/PackageManager;->getServicesSystemSharedLibraryPackageName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/settingslib/v;->cQy:Ljava/lang/String;

    :cond_2
    sget-object v1, Lcom/android/settingslib/v;->cQx:Ljava/lang/String;

    if-nez v1, :cond_3

    invoke-virtual {p1}, Landroid/content/pm/PackageManager;->getSharedSystemSharedLibraryPackageName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/settingslib/v;->cQx:Ljava/lang/String;

    :cond_3
    sget-object v1, Lcom/android/settingslib/v;->cQw:[Landroid/content/pm/Signature;

    aget-object v1, v1, v3

    if-eqz v1, :cond_4

    sget-object v1, Lcom/android/settingslib/v;->cQw:[Landroid/content/pm/Signature;

    aget-object v1, v1, v3

    invoke-static {p2}, Lcom/android/settingslib/v;->cqG(Landroid/content/pm/PackageInfo;)Landroid/content/pm/Signature;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    iget-object v1, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    sget-object v2, Lcom/android/settingslib/v;->cQv:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    sget-object v2, Lcom/android/settingslib/v;->cQy:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    sget-object v2, Lcom/android/settingslib/v;->cQx:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v2, "com.android.printspooler"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v0, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/android/settingslib/v;->cqA(Landroid/content/res/Resources;Ljava/lang/String;)Z

    move-result v0

    :cond_5
    return v0
.end method
