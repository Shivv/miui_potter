.class public Lcom/android/settingslib/MiuiRestrictedPreference;
.super Lcom/android/settingslib/MiuiTwoTargetPreference;
.source "MiuiRestrictedPreference.java"


# instance fields
.field cPQ:Lcom/android/settingslib/o;

.field private cPR:Z

.field private cPS:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settingslib/MiuiTwoTargetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Lcom/android/settingslib/o;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/o;-><init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    sget v0, Lmiui/R$layout;->preference_value:I

    invoke-virtual {p0, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->setLayoutResource(I)V

    iput-boolean v1, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPR:Z

    iput-boolean v1, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPS:Z

    return-void
.end method


# virtual methods
.method protected aBP()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqf()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected ayP()I
    .locals 1

    sget v0, Lcom/android/settingslib/h;->cLk:I

    return v0
.end method

.method public cqa(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->cqh(Z)V

    return-void
.end method

.method public cqb(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPS:Z

    return-void
.end method

.method public cqc(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/android/settingslib/o;->cqg(Ljava/lang/String;I)V

    return-void
.end method

.method public cqd(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1, p2}, Lcom/android/settingslib/o;->cqg(Ljava/lang/String;I)V

    return-void
.end method

.method public cqe(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPR:Z

    return-void
.end method

.method public cqf()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->cqj()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->cqi()V

    invoke-super {p0, p1}, Lcom/android/settingslib/MiuiTwoTargetPreference;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 6

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-super {p0, p1}, Lcom/android/settingslib/MiuiTwoTargetPreference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->onBindView(Landroid/view/View;)V

    sget v0, Lcom/android/settingslib/g;->cLd:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqf()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    sget v0, Lmiui/R$id;->arrow_right:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPR:Z

    if-eqz v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    sget v0, Lmiui/R$id;->value_right:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x1020010

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-boolean v4, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPS:Z

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    if-eqz v1, :cond_2

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    move v0, v3

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public performClick(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->performClick()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/android/settingslib/MiuiTwoTargetPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    :cond_0
    return-void
.end method

.method public setDisabledByAdmin(Lcom/android/settingslib/n;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedPreference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqf()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/MiuiRestrictedPreference;->cPQ:Lcom/android/settingslib/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settingslib/MiuiTwoTargetPreference;->setEnabled(Z)V

    return-void
.end method
