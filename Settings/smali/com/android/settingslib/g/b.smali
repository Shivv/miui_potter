.class public Lcom/android/settingslib/g/b;
.super Ljava/lang/Object;
.source "DataUsageController.java"


# static fields
.field private static final cJb:Ljava/lang/StringBuilder;

.field private static final cJh:Ljava/util/Formatter;

.field private static final cJi:Z


# instance fields
.field private final cJc:Landroid/net/INetworkStatsService;

.field private final cJd:Landroid/net/ConnectivityManager;

.field private final cJe:Landroid/telephony/TelephonyManager;

.field private final cJf:Landroid/net/NetworkPolicyManager;

.field private cJg:Lcom/android/settingslib/g/c;

.field private cJj:Landroid/net/INetworkStatsSession;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string/jumbo v0, "DataUsageController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settingslib/g/b;->cJi:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    sput-object v0, Lcom/android/settingslib/g/b;->cJb:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    sget-object v1, Lcom/android/settingslib/g/b;->cJb:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/settingslib/g/b;->cJh:Ljava/util/Formatter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/g/b;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/g/b;->cJe:Landroid/telephony/TelephonyManager;

    invoke-static {p1}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/g/b;->cJd:Landroid/net/ConnectivityManager;

    const-string/jumbo v0, "netstats"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/net/INetworkStatsService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkStatsService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/g/b;->cJc:Landroid/net/INetworkStatsService;

    iget-object v0, p0, Lcom/android/settingslib/g/b;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/net/NetworkPolicyManager;->from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/g/b;->cJf:Landroid/net/NetworkPolicyManager;

    return-void
.end method

.method private coA(JJ)Ljava/lang/String;
    .locals 9

    sget-object v8, Lcom/android/settingslib/g/b;->cJb:Ljava/lang/StringBuilder;

    monitor-enter v8

    :try_start_0
    sget-object v0, Lcom/android/settingslib/g/b;->cJb:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/settingslib/g/b;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/android/settingslib/g/b;->cJh:Ljava/util/Formatter;

    const v6, 0x10010

    const/4 v7, 0x0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v8

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method private static coB(Landroid/net/NetworkStatsHistory$Entry;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Entry["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "bucketDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/net/NetworkStatsHistory$Entry;->bucketDuration:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",bucketStart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/net/NetworkStatsHistory$Entry;->bucketStart:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",activeTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/net/NetworkStatsHistory$Entry;->activeTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",rxBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",rxPackets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/net/NetworkStatsHistory$Entry;->rxPackets:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",txBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",txPackets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/net/NetworkStatsHistory$Entry;->txPackets:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",operations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/net/NetworkStatsHistory$Entry;->operations:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private coC()Landroid/net/INetworkStatsSession;
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/g/b;->cJj:Landroid/net/INetworkStatsSession;

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/settingslib/g/b;->cJc:Landroid/net/INetworkStatsService;

    invoke-interface {v0}, Landroid/net/INetworkStatsService;->openSession()Landroid/net/INetworkStatsSession;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settingslib/g/b;->cJj:Landroid/net/INetworkStatsSession;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settingslib/g/b;->cJj:Landroid/net/INetworkStatsSession;

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "DataUsageController"

    const-string/jumbo v2, "Failed to open stats session"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v1, "DataUsageController"

    const-string/jumbo v2, "Failed to open stats session"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private coE(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/settingslib/g/b;->cJf:Landroid/net/NetworkPolicyManager;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return-object v5

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/g/b;->cJf:Landroid/net/NetworkPolicyManager;

    invoke-virtual {v0}, Landroid/net/NetworkPolicyManager;->getNetworkPolicies()[Landroid/net/NetworkPolicy;

    move-result-object v1

    if-nez v1, :cond_2

    return-object v5

    :cond_2
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    if-eqz v3, :cond_3

    iget-object v4, v3, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    invoke-virtual {p1, v4}, Landroid/net/NetworkTemplate;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    return-object v3

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    return-object v5
.end method

.method private static coF(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultDataSubscriptionId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private coz(Ljava/lang/String;)Lcom/android/settingslib/g/d;
    .locals 3

    const-string/jumbo v0, "DataUsageController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to get data usage, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public coD()Lcom/android/settingslib/g/d;
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/g/b;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/g/b;->coF(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "no subscriber id"

    invoke-direct {p0, v0}, Lcom/android/settingslib/g/b;->coz(Ljava/lang/String;)Lcom/android/settingslib/g/d;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/NetworkTemplate;->buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settingslib/g/b;->cJe:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getMergedSubscriberIds()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/NetworkTemplate;->normalize(Landroid/net/NetworkTemplate;[Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/g/b;->coG(Landroid/net/NetworkTemplate;)Lcom/android/settingslib/g/d;

    move-result-object v0

    return-object v0
.end method

.method public coG(Landroid/net/NetworkTemplate;)Lcom/android/settingslib/g/d;
    .locals 19

    invoke-direct/range {p0 .. p0}, Lcom/android/settingslib/g/b;->coC()Landroid/net/INetworkStatsSession;

    move-result-object v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "no stats session"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/settingslib/g/b;->coz(Ljava/lang/String;)Lcom/android/settingslib/g/d;

    move-result-object v2

    return-object v2

    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/android/settingslib/g/b;->coE(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v11

    const/16 v3, 0xa

    :try_start_0
    move-object/from16 v0, p1

    invoke-interface {v2, v0, v3}, Landroid/net/INetworkStatsSession;->getHistoryForNetwork(Landroid/net/NetworkTemplate;I)Landroid/net/NetworkStatsHistory;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    if-eqz v11, :cond_2

    invoke-static {v11}, Landroid/net/NetworkPolicyManager;->cycleIterator(Landroid/net/NetworkPolicy;)Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/time/ZonedDateTime;

    invoke-virtual {v4}, Ljava/time/ZonedDateTime;->toInstant()Ljava/time/Instant;

    move-result-object v4

    invoke-virtual {v4}, Ljava/time/Instant;->toEpochMilli()J

    move-result-wide v4

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/time/ZonedDateTime;

    invoke-virtual {v2}, Ljava/time/ZonedDateTime;->toInstant()Ljava/time/Instant;

    move-result-object v2

    invoke-virtual {v2}, Ljava/time/Instant;->toEpochMilli()J

    move-result-wide v6

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/net/NetworkStatsHistory;->getValues(JJJLandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sget-boolean v3, Lcom/android/settingslib/g/b;->cJi:Z

    if-eqz v3, :cond_1

    const-string/jumbo v3, "DataUsageController"

    const-string/jumbo v10, "history call from %s to %s now=%s took %sms: %s"

    const/16 v16, 0x5

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    new-instance v17, Ljava/util/Date;

    move-object/from16 v0, v17

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    const/16 v18, 0x0

    aput-object v17, v16, v18

    new-instance v17, Ljava/util/Date;

    move-object/from16 v0, v17

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    const/16 v18, 0x1

    aput-object v17, v16, v18

    new-instance v17, Ljava/util/Date;

    move-object/from16 v0, v17

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    const/4 v8, 0x2

    aput-object v17, v16, v8

    sub-long v8, v14, v12

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    const/4 v9, 0x3

    aput-object v8, v16, v9

    invoke-static {v2}, Lcom/android/settingslib/g/b;->coB(Landroid/net/NetworkStatsHistory$Entry;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x4

    aput-object v8, v16, v9

    move-object/from16 v0, v16

    invoke-static {v10, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-nez v2, :cond_3

    const-string/jumbo v2, "no entry data"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/settingslib/g/b;->coz(Ljava/lang/String;)Lcom/android/settingslib/g/d;

    move-result-object v2

    return-object v2

    :cond_2
    const-wide v4, 0x90321000L

    sub-long v4, v8, v4

    move-wide v6, v8

    goto :goto_0

    :cond_3
    iget-wide v8, v2, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    iget-wide v2, v2, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    add-long/2addr v2, v8

    new-instance v8, Lcom/android/settingslib/g/d;

    invoke-direct {v8}, Lcom/android/settingslib/g/d;-><init>()V

    iput-wide v4, v8, Lcom/android/settingslib/g/d;->cJn:J

    iput-wide v2, v8, Lcom/android/settingslib/g/d;->cJl:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/android/settingslib/g/b;->coA(JJ)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lcom/android/settingslib/g/d;->cJp:Ljava/lang/String;

    if-eqz v11, :cond_7

    iget-wide v2, v11, Landroid/net/NetworkPolicy;->limitBytes:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    iget-wide v2, v11, Landroid/net/NetworkPolicy;->limitBytes:J

    :goto_1
    iput-wide v2, v8, Lcom/android/settingslib/g/d;->cJk:J

    iget-wide v2, v11, Landroid/net/NetworkPolicy;->warningBytes:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    iget-wide v2, v11, Landroid/net/NetworkPolicy;->warningBytes:J

    :goto_2
    iput-wide v2, v8, Lcom/android/settingslib/g/d;->cJo:J

    :goto_3
    if-eqz v8, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settingslib/g/b;->cJg:Lcom/android/settingslib/g/c;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/settingslib/g/b;->cJg:Lcom/android/settingslib/g/c;

    invoke-interface {v2}, Lcom/android/settingslib/g/c;->coH()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lcom/android/settingslib/g/d;->cJm:Ljava/lang/String;

    :cond_4
    return-object v8

    :cond_5
    const-wide/16 v2, 0x0

    goto :goto_1

    :cond_6
    const-wide/16 v2, 0x0

    goto :goto_2

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/g/b;->coy()J

    move-result-wide v2

    iput-wide v2, v8, Lcom/android/settingslib/g/d;->cJo:J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v2

    const-string/jumbo v2, "remote call failed"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/settingslib/g/b;->coz(Ljava/lang/String;)Lcom/android/settingslib/g/d;

    move-result-object v2

    return-object v2
.end method

.method public coy()J
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/g/b;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e00e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x100000

    mul-long/2addr v0, v2

    return-wide v0
.end method
