.class public Lcom/android/settingslib/g/g;
.super Landroid/content/AsyncTaskLoader;
.source "ChartDataLoader.java"


# instance fields
.field private final cJv:Landroid/os/Bundle;

.field private final cJw:Landroid/net/INetworkStatsSession;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/INetworkStatsSession;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settingslib/g/g;->cJw:Landroid/net/INetworkStatsSession;

    iput-object p3, p0, Lcom/android/settingslib/g/g;->cJv:Landroid/os/Bundle;

    return-void
.end method

.method private coO(Landroid/net/NetworkTemplate;IILandroid/net/NetworkStatsHistory;)Landroid/net/NetworkStatsHistory;
    .locals 6

    iget-object v0, p0, Lcom/android/settingslib/g/g;->cJw:Landroid/net/INetworkStatsSession;

    const/4 v4, 0x0

    const/16 v5, 0xa

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Landroid/net/INetworkStatsSession;->getHistoryForUid(Landroid/net/NetworkTemplate;IIII)Landroid/net/NetworkStatsHistory;

    move-result-object v0

    if-eqz p4, :cond_0

    invoke-virtual {p4, v0}, Landroid/net/NetworkStatsHistory;->recordEntireHistory(Landroid/net/NetworkStatsHistory;)V

    return-object p4

    :cond_0
    return-object v0
.end method

.method public static coP(Landroid/net/NetworkTemplate;Lcom/android/settingslib/AppItem;)Landroid/os/Bundle;
    .locals 1

    const/16 v0, 0xa

    invoke-static {p0, p1, v0}, Lcom/android/settingslib/g/g;->coQ(Landroid/net/NetworkTemplate;Lcom/android/settingslib/AppItem;I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static coQ(Landroid/net/NetworkTemplate;Lcom/android/settingslib/AppItem;I)Landroid/os/Bundle;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "template"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "app"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "fields"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method private coR(Landroid/net/NetworkTemplate;Lcom/android/settingslib/AppItem;I)Lcom/android/settingslib/g/e;
    .locals 10

    const-wide/32 v8, 0x36ee80

    const/4 v1, 0x0

    new-instance v2, Lcom/android/settingslib/g/e;

    invoke-direct {v2}, Lcom/android/settingslib/g/e;-><init>()V

    iget-object v0, p0, Lcom/android/settingslib/g/g;->cJw:Landroid/net/INetworkStatsSession;

    invoke-interface {v0, p1, p3}, Landroid/net/INetworkStatsSession;->getHistoryForNetwork(Landroid/net/NetworkTemplate;I)Landroid/net/NetworkStatsHistory;

    move-result-object v0

    iput-object v0, v2, Lcom/android/settingslib/g/e;->cJs:Landroid/net/NetworkStatsHistory;

    if-eqz p2, :cond_1

    iget-object v0, p2, Lcom/android/settingslib/AppItem;->cQL:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v3

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v4, p2, Lcom/android/settingslib/AppItem;->cQL:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    iget-object v5, v2, Lcom/android/settingslib/g/e;->cJq:Landroid/net/NetworkStatsHistory;

    invoke-direct {p0, p1, v4, v1, v5}, Lcom/android/settingslib/g/g;->coO(Landroid/net/NetworkTemplate;IILandroid/net/NetworkStatsHistory;)Landroid/net/NetworkStatsHistory;

    move-result-object v5

    iput-object v5, v2, Lcom/android/settingslib/g/e;->cJq:Landroid/net/NetworkStatsHistory;

    iget-object v5, v2, Lcom/android/settingslib/g/e;->cJt:Landroid/net/NetworkStatsHistory;

    const/4 v6, 0x1

    invoke-direct {p0, p1, v4, v6, v5}, Lcom/android/settingslib/g/g;->coO(Landroid/net/NetworkTemplate;IILandroid/net/NetworkStatsHistory;)Landroid/net/NetworkStatsHistory;

    move-result-object v4

    iput-object v4, v2, Lcom/android/settingslib/g/e;->cJt:Landroid/net/NetworkStatsHistory;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-lez v3, :cond_2

    new-instance v0, Landroid/net/NetworkStatsHistory;

    iget-object v1, v2, Lcom/android/settingslib/g/e;->cJt:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v1}, Landroid/net/NetworkStatsHistory;->getBucketDuration()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Landroid/net/NetworkStatsHistory;-><init>(J)V

    iput-object v0, v2, Lcom/android/settingslib/g/e;->cJr:Landroid/net/NetworkStatsHistory;

    iget-object v0, v2, Lcom/android/settingslib/g/e;->cJr:Landroid/net/NetworkStatsHistory;

    iget-object v1, v2, Lcom/android/settingslib/g/e;->cJq:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v0, v1}, Landroid/net/NetworkStatsHistory;->recordEntireHistory(Landroid/net/NetworkStatsHistory;)V

    iget-object v0, v2, Lcom/android/settingslib/g/e;->cJr:Landroid/net/NetworkStatsHistory;

    iget-object v1, v2, Lcom/android/settingslib/g/e;->cJt:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v0, v1}, Landroid/net/NetworkStatsHistory;->recordEntireHistory(Landroid/net/NetworkStatsHistory;)V

    :cond_1
    :goto_1
    return-object v2

    :cond_2
    new-instance v0, Landroid/net/NetworkStatsHistory;

    invoke-direct {v0, v8, v9}, Landroid/net/NetworkStatsHistory;-><init>(J)V

    iput-object v0, v2, Lcom/android/settingslib/g/e;->cJq:Landroid/net/NetworkStatsHistory;

    new-instance v0, Landroid/net/NetworkStatsHistory;

    invoke-direct {v0, v8, v9}, Landroid/net/NetworkStatsHistory;-><init>(J)V

    iput-object v0, v2, Lcom/android/settingslib/g/e;->cJt:Landroid/net/NetworkStatsHistory;

    new-instance v0, Landroid/net/NetworkStatsHistory;

    invoke-direct {v0, v8, v9}, Landroid/net/NetworkStatsHistory;-><init>(J)V

    iput-object v0, v2, Lcom/android/settingslib/g/e;->cJr:Landroid/net/NetworkStatsHistory;

    goto :goto_1
.end method


# virtual methods
.method public loadInBackground()Lcom/android/settingslib/g/e;
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/g/g;->cJv:Landroid/os/Bundle;

    const-string/jumbo v1, "template"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkTemplate;

    iget-object v1, p0, Lcom/android/settingslib/g/g;->cJv:Landroid/os/Bundle;

    const-string/jumbo v2, "app"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/AppItem;

    iget-object v2, p0, Lcom/android/settingslib/g/g;->cJv:Landroid/os/Bundle;

    const-string/jumbo v3, "fields"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    :try_start_0
    invoke-direct {p0, v0, v1, v2}, Lcom/android/settingslib/g/g;->coR(Landroid/net/NetworkTemplate;Lcom/android/settingslib/AppItem;I)Lcom/android/settingslib/g/e;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "problem reading network stats"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settingslib/g/g;->loadInBackground()Lcom/android/settingslib/g/e;

    move-result-object v0

    return-object v0
.end method

.method protected onReset()V
    .locals 0

    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onReset()V

    invoke-virtual {p0}, Lcom/android/settingslib/g/g;->cancelLoad()Z

    return-void
.end method

.method protected onStartLoading()V
    .locals 0

    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onStartLoading()V

    invoke-virtual {p0}, Lcom/android/settingslib/g/g;->forceLoad()V

    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onStopLoading()V

    invoke-virtual {p0}, Lcom/android/settingslib/g/g;->cancelLoad()Z

    return-void
.end method
