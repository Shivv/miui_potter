.class public Lcom/android/settings/notification/ConfigureNotificationSettings;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "ConfigureNotificationSettings.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/notification/ConfigureNotificationSettings$1;

    invoke-direct {v0}, Lcom/android/settings/notification/ConfigureNotificationSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/notification/ConfigureNotificationSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method

.method private static fV(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;
    .locals 7

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/android/settings/notification/BadgingNotificationPreferenceController;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/BadgingNotificationPreferenceController;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/android/settings/notification/PulseNotificationPreferenceController;

    invoke-direct {v2, p0}, Lcom/android/settings/notification/PulseNotificationPreferenceController;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

    const-string/jumbo v4, "lock_screen_notifications"

    const-string/jumbo v5, "lock_screen_notifications_profile_header"

    const-string/jumbo v6, "lock_screen_notifications_profile"

    invoke-direct {v3, p0, v4, v5, v6}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1, v2}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-virtual {p1, v3}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    new-instance v4, Lcom/android/settings/gestures/j;

    const-string/jumbo v5, "gesture_swipe_down_fingerprint_notifications"

    invoke-direct {v4, p0, p1, v5}, Lcom/android/settings/gestures/j;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/lang/String;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method static synthetic fW(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;
    .locals 1

    invoke-static {p0, p1}, Lcom/android/settings/notification/ConfigureNotificationSettings;->fV(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "ConfigNotiSettings"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f15002f

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x151

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/notification/ConfigureNotificationSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/settings/notification/ConfigureNotificationSettings;->fV(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
