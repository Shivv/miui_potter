.class final Lcom/android/settings/notification/ZenModeSettings$4;
.super Lcom/android/settings/notification/ZenRuleNameDialog;
.source "ZenModeSettings.java"


# instance fields
.field final synthetic gK:Lcom/android/settings/notification/ZenModeSettings;

.field final synthetic gL:Lcom/android/settings/notification/ZenRuleInfo;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/ZenModeSettings;Landroid/content/Context;Ljava/lang/CharSequence;Lcom/android/settings/notification/ZenRuleInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/ZenModeSettings$4;->gK:Lcom/android/settings/notification/ZenModeSettings;

    iput-object p4, p0, Lcom/android/settings/notification/ZenModeSettings$4;->gL:Lcom/android/settings/notification/ZenRuleInfo;

    invoke-direct {p0, p2, p3}, Lcom/android/settings/notification/ZenRuleNameDialog;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public db(Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Landroid/app/AutomaticZenRule;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings$4;->gL:Lcom/android/settings/notification/ZenRuleInfo;

    iget-object v2, v1, Lcom/android/settings/notification/ZenRuleInfo;->x:Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings$4;->gL:Lcom/android/settings/notification/ZenRuleInfo;

    iget-object v3, v1, Lcom/android/settings/notification/ZenRuleInfo;->t:Landroid/net/Uri;

    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/app/AutomaticZenRule;-><init>(Ljava/lang/String;Landroid/content/ComponentName;Landroid/net/Uri;IZ)V

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings$4;->gK:Lcom/android/settings/notification/ZenModeSettings;

    invoke-virtual {v1, v0}, Lcom/android/settings/notification/ZenModeSettings;->fX(Landroid/app/AutomaticZenRule;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings$4;->gK:Lcom/android/settings/notification/ZenModeSettings;

    iget-object v2, p0, Lcom/android/settings/notification/ZenModeSettings$4;->gK:Lcom/android/settings/notification/ZenModeSettings;

    iget-object v3, p0, Lcom/android/settings/notification/ZenModeSettings$4;->gL:Lcom/android/settings/notification/ZenRuleInfo;

    iget-object v3, v3, Lcom/android/settings/notification/ZenRuleInfo;->y:Ljava/lang/String;

    invoke-static {v2, v3, v6, v0}, Lcom/android/settings/notification/ZenModeSettings;->cn(Lcom/android/settings/notification/ZenModeSettings;Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/settings/notification/ZenModeSettings;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
