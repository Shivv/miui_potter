.class final Lcom/android/settings/notification/VibrateWhenRingPreferenceController$SettingObserver;
.super Landroid/database/ContentObserver;
.source "VibrateWhenRingPreferenceController.java"


# instance fields
.field private final fT:Landroid/net/Uri;

.field private final fU:Landroid/support/v7/preference/Preference;

.field final synthetic fV:Lcom/android/settings/notification/VibrateWhenRingPreferenceController;


# direct methods
.method public constructor <init>(Lcom/android/settings/notification/VibrateWhenRingPreferenceController;Landroid/support/v7/preference/Preference;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/notification/VibrateWhenRingPreferenceController$SettingObserver;->fV:Lcom/android/settings/notification/VibrateWhenRingPreferenceController;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "vibrate_when_ringing"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/VibrateWhenRingPreferenceController$SettingObserver;->fT:Landroid/net/Uri;

    iput-object p2, p0, Lcom/android/settings/notification/VibrateWhenRingPreferenceController$SettingObserver;->fU:Landroid/support/v7/preference/Preference;

    return-void
.end method


# virtual methods
.method public hr(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/VibrateWhenRingPreferenceController$SettingObserver;->fV:Lcom/android/settings/notification/VibrateWhenRingPreferenceController;

    invoke-static {v0}, Lcom/android/settings/notification/VibrateWhenRingPreferenceController;->hq(Lcom/android/settings/notification/VibrateWhenRingPreferenceController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/VibrateWhenRingPreferenceController$SettingObserver;->fT:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/notification/VibrateWhenRingPreferenceController$SettingObserver;->fT:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/VibrateWhenRingPreferenceController$SettingObserver;->fV:Lcom/android/settings/notification/VibrateWhenRingPreferenceController;

    iget-object v1, p0, Lcom/android/settings/notification/VibrateWhenRingPreferenceController$SettingObserver;->fU:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/VibrateWhenRingPreferenceController;->hp(Landroid/support/v7/preference/Preference;)V

    :cond_0
    return-void
.end method
