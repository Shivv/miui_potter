.class public abstract Lcom/android/settings/notification/ZenRuleSelectionDialog;
.super Ljava/lang/Object;
.source "ZenRuleSelectionDialog.java"


# static fields
.field private static final cO:Z

.field private static final cP:Ljava/util/Comparator;


# instance fields
.field private final cQ:Landroid/app/AlertDialog;

.field private cR:Landroid/app/NotificationManager;

.field private final cS:Landroid/content/pm/PackageManager;

.field private final cT:Landroid/widget/LinearLayout;

.field private final cU:Lcom/android/settings/utils/i;

.field private final cV:Lcom/android/settings/utils/j;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cO:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/settings/notification/ZenModeSettings;->eJ:Z

    sput-boolean v0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cO:Z

    new-instance v0, Lcom/android/settings/notification/ZenRuleSelectionDialog$2;

    invoke-direct {v0}, Lcom/android/settings/notification/ZenRuleSelectionDialog$2;-><init>()V

    sput-object v0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cP:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/utils/i;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/notification/ZenRuleSelectionDialog$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/ZenRuleSelectionDialog$1;-><init>(Lcom/android/settings/notification/ZenRuleSelectionDialog;)V

    iput-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cV:Lcom/android/settings/utils/j;

    iput-object p1, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cS:Landroid/content/pm/PackageManager;

    const-string/jumbo v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cR:Landroid/app/NotificationManager;

    iput-object p2, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cU:Lcom/android/settings/utils/i;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d028c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a039e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cT:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cU:Lcom/android/settings/utils/i;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->dT()Lcom/android/settings/notification/ZenRuleInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->dS(Lcom/android/settings/notification/ZenRuleInfo;)V

    invoke-direct {p0}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->dU()Lcom/android/settings/notification/ZenRuleInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->dS(Lcom/android/settings/notification/ZenRuleInfo;)V

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cU:Lcom/android/settings/utils/i;

    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cV:Lcom/android/settings/utils/j;

    invoke-virtual {v0, v2}, Lcom/android/settings/utils/i;->ayI(Lcom/android/settings/utils/j;)V

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cU:Lcom/android/settings/utils/i;

    invoke-virtual {v0}, Lcom/android/settings/utils/i;->ayH()V

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f121699

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/notification/ZenRuleSelectionDialog$3;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/ZenRuleSelectionDialog$3;-><init>(Lcom/android/settings/notification/ZenRuleSelectionDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1203c7

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cQ:Landroid/app/AlertDialog;

    return-void
.end method

.method private dR(Ljava/util/Set;)V
    .locals 2

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/notification/ZenRuleInfo;

    invoke-direct {p0, v0}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->dS(Lcom/android/settings/notification/ZenRuleInfo;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private dS(Lcom/android/settings/notification/ZenRuleInfo;)V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cS:Landroid/content/pm/PackageManager;

    iget-object v1, p1, Lcom/android/settings/notification/ZenRuleInfo;->packageName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d028b

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v3, Lcom/android/settings/notification/ZenRuleSelectionDialog$LoadIconTask;

    const v1, 0x7f0a01f0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-direct {v3, p0, v1}, Lcom/android/settings/notification/ZenRuleSelectionDialog$LoadIconTask;-><init>(Lcom/android/settings/notification/ZenRuleSelectionDialog;Landroid/widget/ImageView;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/pm/ApplicationInfo;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    invoke-virtual {v3, v1}, Lcom/android/settings/notification/ZenRuleSelectionDialog$LoadIconTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const v1, 0x7f0a049a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, p1, Lcom/android/settings/notification/ZenRuleInfo;->title:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v1, p1, Lcom/android/settings/notification/ZenRuleInfo;->u:Z

    if-nez v1, :cond_0

    const v1, 0x7f0a0445

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cS:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    new-instance v1, Lcom/android/settings/notification/ZenRuleSelectionDialog$4;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/notification/ZenRuleSelectionDialog$4;-><init>(Lcom/android/settings/notification/ZenRuleSelectionDialog;Lcom/android/settings/notification/ZenRuleInfo;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cT:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private dT()Lcom/android/settings/notification/ZenRuleInfo;
    .locals 4

    new-instance v0, Landroid/service/notification/ZenModeConfig$EventInfo;

    invoke-direct {v0}, Landroid/service/notification/ZenModeConfig$EventInfo;-><init>()V

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/service/notification/ZenModeConfig$EventInfo;->calendar:Ljava/lang/String;

    const/4 v1, 0x0

    iput v1, v0, Landroid/service/notification/ZenModeConfig$EventInfo;->reply:I

    new-instance v1, Lcom/android/settings/notification/ZenRuleInfo;

    invoke-direct {v1}, Lcom/android/settings/notification/ZenRuleInfo;-><init>()V

    const-string/jumbo v2, "android.settings.ZEN_MODE_EVENT_RULE_SETTINGS"

    iput-object v2, v1, Lcom/android/settings/notification/ZenRuleInfo;->y:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f12168e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/settings/notification/ZenRuleInfo;->title:Ljava/lang/String;

    invoke-static {}, Landroid/service/notification/ZenModeConfig;->getScheduleConditionProvider()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/settings/notification/ZenRuleInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->toEventConditionId(Landroid/service/notification/ZenModeConfig$EventInfo;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->t:Landroid/net/Uri;

    invoke-static {}, Landroid/service/notification/ZenModeConfig;->getEventConditionProvider()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->x:Landroid/content/ComponentName;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->u:Z

    return-object v1
.end method

.method private dU()Lcom/android/settings/notification/ZenRuleInfo;
    .locals 4

    new-instance v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    invoke-direct {v0}, Landroid/service/notification/ZenModeConfig$ScheduleInfo;-><init>()V

    sget-object v1, Landroid/service/notification/ZenModeConfig;->ALL_DAYS:[I

    iput-object v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->days:[I

    const/16 v1, 0x16

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startHour:I

    const/4 v1, 0x7

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endHour:I

    new-instance v1, Lcom/android/settings/notification/ZenRuleInfo;

    invoke-direct {v1}, Lcom/android/settings/notification/ZenRuleInfo;-><init>()V

    const-string/jumbo v2, "android.settings.ZEN_MODE_SCHEDULE_RULE_SETTINGS"

    iput-object v2, v1, Lcom/android/settings/notification/ZenRuleInfo;->y:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f1216e3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/settings/notification/ZenRuleInfo;->title:Ljava/lang/String;

    invoke-static {}, Landroid/service/notification/ZenModeConfig;->getEventConditionProvider()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/settings/notification/ZenRuleInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->toScheduleConditionId(Landroid/service/notification/ZenModeConfig$ScheduleInfo;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->t:Landroid/net/Uri;

    invoke-static {}, Landroid/service/notification/ZenModeConfig;->getScheduleConditionProvider()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->x:Landroid/content/ComponentName;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/android/settings/notification/ZenRuleInfo;->u:Z

    return-object v1
.end method

.method static synthetic dX()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cP:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic dY(Lcom/android/settings/notification/ZenRuleSelectionDialog;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cQ:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic dZ(Lcom/android/settings/notification/ZenRuleSelectionDialog;)Landroid/app/NotificationManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cR:Landroid/app/NotificationManager;

    return-object v0
.end method

.method static synthetic ea(Lcom/android/settings/notification/ZenRuleSelectionDialog;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cS:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic eb(Lcom/android/settings/notification/ZenRuleSelectionDialog;)Lcom/android/settings/utils/i;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cU:Lcom/android/settings/utils/i;

    return-object v0
.end method

.method static synthetic ec(Lcom/android/settings/notification/ZenRuleSelectionDialog;)Lcom/android/settings/utils/j;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cV:Lcom/android/settings/utils/j;

    return-object v0
.end method

.method static synthetic ed(Lcom/android/settings/notification/ZenRuleSelectionDialog;Ljava/util/Set;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->dR(Ljava/util/Set;)V

    return-void
.end method


# virtual methods
.method public abstract dV(Lcom/android/settings/notification/ZenRuleInfo;)V
.end method

.method public abstract dW(Lcom/android/settings/notification/ZenRuleInfo;)V
.end method

.method public show()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog;->cQ:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
