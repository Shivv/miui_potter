.class final Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiSilentSettingsFragment.java"


# instance fields
.field private final bJ:Landroid/net/Uri;

.field private final bK:Landroid/net/Uri;

.field final synthetic bL:Lcom/android/settings/notification/MiuiSilentSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->bL:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-static {p1}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cH(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "zen_mode"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->bK:Landroid/net/Uri;

    const-string/jumbo v0, "zen_mode_config_etag"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->bJ:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public cL()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->bL:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cJ(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->bK:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->bL:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cJ(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->bJ:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method

.method public cM()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->bL:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cJ(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->bL:Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cH(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
