.class public Lcom/android/settings/notification/ChannelNotificationSettings;
.super Lcom/android/settings/notification/NotificationSettingsBase;
.source "ChannelNotificationSettings.java"


# instance fields
.field private aA:Lcom/android/settings/applications/AppHeaderController;

.field private aB:Landroid/preference/Preference;

.field private aC:Lcom/android/settings/notification/NotificationSoundPreference;

.field private ay:Landroid/app/NotificationChannelGroup;

.field private az:Lcom/android/settings/widget/FooterPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;-><init>()V

    return-void
.end method

.method private bb()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0}, Landroid/app/NotificationChannel;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/widget/FooterPreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/widget/FooterPreference;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/FooterPreference;->setOrder(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/FooterPreference;->boV(Z)V

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/FooterPreference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private bc()V
    .locals 3

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->fr(Landroid/util/ArrayMap;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/overlay/a;->aIp(Landroid/content/Context;)Lcom/android/settings/applications/ApplicationFeatureProvider;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/android/settings/applications/ApplicationFeatureProvider;->xy(Landroid/app/Fragment;Landroid/view/View;)Lcom/android/settings/applications/AppHeaderController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aA:Lcom/android/settings/applications/AppHeaderController;

    return-void
.end method

.method private bd(I)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    sparse-switch p1, :sswitch_data_0

    const-string/jumbo v0, ""

    return-object v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f120bc2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const v0, 0x7f120bba

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :sswitch_1
    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120bc0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f120bbf

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120bbe

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f120bbd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f120bb9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->be()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f120bb8

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f120bbc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->be()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f120bbb

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto/16 :goto_0

    :cond_2
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3e8 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_4
    .end sparse-switch
.end method

.method private bf()V
    .locals 1

    const v0, 0x7f1500fe

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->O()V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0}, Landroid/app/NotificationChannel;->canBypassDnd()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->fy(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0}, Landroid/app/NotificationChannel;->getLockscreenVisibility()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->fz(I)V

    invoke-direct {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bj()V

    invoke-direct {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bl()V

    invoke-direct {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bk()V

    invoke-direct {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bi()V

    return-void
.end method

.method private bg(Ljava/lang/CharSequence;)V
    .locals 4

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v2, v2, Lcom/android/settings/notification/NotificationBackend$AppRow;->aI:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->mContext:Landroid/content/Context;

    const v3, 0x7f120bb6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aA:Lcom/android/settings/applications/AppHeaderController;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/settings/applications/AppHeaderController;->setSummary(Ljava/lang/CharSequence;)Lcom/android/settings/applications/AppHeaderController;

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aA:Lcom/android/settings/applications/AppHeaderController;

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/AppHeaderController;->tj(Landroid/app/Activity;Landroid/content/Context;)Lcom/android/settings/applications/LayoutPreference;

    return-void
.end method

.method private bh()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d01f2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/MiuiLayoutPreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/settings/applications/MiuiLayoutPreference;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    const/16 v1, -0x1f4

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/MiuiLayoutPreference;->setOrder(I)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    const-string/jumbo v1, "block"

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/MiuiLayoutPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aG:Z

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->fs(ZLandroid/app/NotificationChannel;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    :cond_0
    const v0, 0x7f1203fb

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->fv(I)V

    return-void
.end method

.method private bi()V
    .locals 8

    const/4 v6, 0x0

    const/4 v3, 0x0

    const-string/jumbo v0, "importance"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aB:Landroid/preference/Preference;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "uid"

    iget v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eb:I

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "hideInfoButton"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "package"

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ec:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "android.provider.extra.CHANNEL_ID"

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aB:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ei:Lcom/android/settingslib/n;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->ft(Landroid/app/NotificationChannel;)Z

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aB:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/android/settings/notification/ChannelImportanceSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getMetricsCategory()I

    move-result v7

    const v4, 0x7f120bc1

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bqs(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ILjava/lang/CharSequence;ZI)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aB:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aB:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->bd(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    move v0, v6

    goto :goto_0
.end method

.method private bj()V
    .locals 0

    return-void
.end method

.method private bk()V
    .locals 2

    const-string/jumbo v0, "ringtone"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/notification/NotificationSoundPreference;

    iput-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aC:Lcom/android/settings/notification/NotificationSoundPreference;

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aC:Lcom/android/settings/notification/NotificationSoundPreference;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getSound()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/NotificationSoundPreference;->fa(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aC:Lcom/android/settings/notification/NotificationSoundPreference;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->ft(Landroid/app/NotificationChannel;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/NotificationSoundPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aC:Lcom/android/settings/notification/NotificationSoundPreference;

    new-instance v1, Lcom/android/settings/notification/ChannelNotificationSettings$3;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/ChannelNotificationSettings$3;-><init>(Lcom/android/settings/notification/ChannelNotificationSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/NotificationSoundPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private bl()V
    .locals 0

    return-void
.end method

.method static synthetic bm(Lcom/android/settings/notification/ChannelNotificationSettings;)Landroid/app/NotificationChannelGroup;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ay:Landroid/app/NotificationChannelGroup;

    return-object v0
.end method

.method static synthetic bn(Lcom/android/settings/notification/ChannelNotificationSettings;Landroid/app/NotificationChannelGroup;)Landroid/app/NotificationChannelGroup;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ay:Landroid/app/NotificationChannelGroup;

    return-object p1
.end method

.method static synthetic bo(Lcom/android/settings/notification/ChannelNotificationSettings;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/ChannelNotificationSettings;->bg(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected O()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "badge"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ei:Lcom/android/settingslib/n;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->aH:Z

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->canShowBadge()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    new-instance v1, Lcom/android/settings/notification/ChannelNotificationSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/ChannelNotificationSettings$2;-><init>(Lcom/android/settings/notification/ChannelNotificationSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method Q(Z)V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ee:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->fw(I)Z

    move-result v3

    invoke-virtual {p0, v0, v3}, Lcom/android/settings/notification/ChannelNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->fw(I)Z

    move-result v3

    invoke-virtual {p0, v0, v3}, Lcom/android/settings/notification/ChannelNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    iget-object v3, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eo:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p0, v5}, Lcom/android/settings/notification/ChannelNotificationSettings;->fw(I)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, v4}, Lcom/android/settings/notification/ChannelNotificationSettings;->fw(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ep:Z

    :goto_1
    invoke-virtual {p0, v3, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    iget-object v3, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {p0, v4}, Lcom/android/settings/notification/ChannelNotificationSettings;->fw(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->fx()Z

    move-result v0

    :goto_2
    invoke-virtual {p0, v3, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->en:Lcom/android/settings/widget/MiuiFooterPreference;

    iget-object v3, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v3

    if-nez v3, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {p0, v0, v2}, Lcom/android/settings/notification/ChannelNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->er:Landroid/preference/Preference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->er:Landroid/preference/Preference;

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->fw(I)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->az:Lcom/android/settings/widget/FooterPreference;

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aB:Landroid/preference/Preference;

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->fw(I)Z

    move-result v3

    invoke-virtual {p0, v0, v3}, Lcom/android/settings/notification/ChannelNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aC:Lcom/android/settings/notification/NotificationSoundPreference;

    invoke-virtual {p0, v5}, Lcom/android/settings/notification/ChannelNotificationSettings;->fw(I)Z

    move-result v3

    invoke-virtual {p0, v0, v3}, Lcom/android/settings/notification/ChannelNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 2

    instance-of v0, p1, Lcom/android/settings/RingtonePreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aC:Lcom/android/settings/notification/NotificationSoundPreference;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aC:Lcom/android/settings/notification/NotificationSoundPreference;

    invoke-virtual {v1}, Lcom/android/settings/notification/NotificationSoundPreference;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/NotificationSoundPreference;->onPrepareRingtonePickerIntent(Landroid/content/Intent;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->startActivityForResult(Landroid/content/Intent;I)V

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/notification/NotificationSettingsBase;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method be()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0}, Landroid/app/NotificationChannel;->getSound()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getSound()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x109

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aC:Lcom/android/settings/notification/NotificationSoundPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aC:Lcom/android/settings/notification/NotificationSoundPreference;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/settings/notification/NotificationSoundPreference;->onActivityResult(IILandroid/content/Intent;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->aB:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/settings/notification/ChannelNotificationSettings;->bd(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->onResume()V

    iget v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eb:I

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ec:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ed:Landroid/content/pm/PackageInfo;

    if-nez v2, :cond_1

    :cond_0
    const-string/jumbo v0, "ChannelSettings"

    const-string/jumbo v1, "Missing package or uid or packageinfo or channel"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_2
    const v2, 0x7f150099

    invoke-virtual {p0, v2}, Lcom/android/settings/notification/ChannelNotificationSettings;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bh()V

    invoke-direct {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bc()V

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->fq()V

    invoke-direct {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bb()V

    const-string/jumbo v2, "miscellaneous"

    iget-object v3, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->fp()V

    iput-boolean v0, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->ee:Z

    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v2}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v2

    if-nez v2, :cond_5

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ChannelNotificationSettings;->Q(Z)V

    return-void

    :cond_4
    invoke-direct {p0}, Lcom/android/settings/notification/ChannelNotificationSettings;->bf()V

    iget-object v2, p0, Lcom/android/settings/notification/ChannelNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v2}, Landroid/app/NotificationChannel;->getGroup()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    new-instance v2, Lcom/android/settings/notification/ChannelNotificationSettings$1;

    invoke-direct {v2, p0}, Lcom/android/settings/notification/ChannelNotificationSettings$1;-><init>(Lcom/android/settings/notification/ChannelNotificationSettings;)V

    new-array v3, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/android/settings/notification/ChannelNotificationSettings$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method
