.class public Lcom/android/settings/notification/SilentModeEditRuleSettings;
.super Lcom/android/settings/notification/SilentModeRuleBaseSettings;
.source "SilentModeEditRuleSettings.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;-><init>()V

    return-void
.end method

.method private dP()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cw:Landroid/service/notification/ZenModeConfig;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig;->automaticRules:Landroid/util/ArrayMap;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ch:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/service/notification/ZenModeConfig$ZenRule;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    invoke-direct {p0, v0}, Lcom/android/settings/notification/SilentModeEditRuleSettings;->dQ(Landroid/service/notification/ZenModeConfig$ZenRule;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeEditRuleSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private dQ(Landroid/service/notification/ZenModeConfig$ZenRule;)Z
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->tryParseScheduleConditionId(Landroid/net/Uri;)Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected cU()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeEditRuleSettings;->dP()Z

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget v0, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startHour:I

    mul-int/lit8 v0, v0, 0x3c

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget v1, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startMinute:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ck:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget v0, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endHour:I

    mul-int/lit8 v0, v0, 0x3c

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget v1, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endMinute:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->bW:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->days:[I

    invoke-static {v0}, Lcom/android/settings/notification/SilentModeUtils;->dO([I)I

    move-result v0

    new-instance v1, Lcom/android/settings/dndmode/e;

    invoke-direct {v1, v0}, Lcom/android/settings/dndmode/e;-><init>(I)V

    iput-object v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->bT:Lcom/android/settings/dndmode/e;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->bT:Lcom/android/settings/dndmode/e;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->bS:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/dndmode/e;->mj(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->bU:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    iput v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ca:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->bY:Ljava/lang/String;

    return-void
.end method

.method public da()Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cQ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ck:I

    div-int/lit8 v1, v1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startHour:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ck:I

    rem-int/lit8 v1, v1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startMinute:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->bW:I

    div-int/lit8 v1, v1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endHour:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->bW:I

    rem-int/lit8 v1, v1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endMinute:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ce:Lcom/android/settings/dndmode/RepeatPreference;

    invoke-virtual {v1}, Lcom/android/settings/dndmode/RepeatPreference;->me()Lcom/android/settings/dndmode/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dndmode/e;->mi()[Z

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/notification/SilentModeUtils;->dK([Z)[I

    move-result-object v1

    iput-object v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->days:[I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    iput-boolean v2, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->exitAtAlarm:Z

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ci:Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    invoke-static {v1}, Landroid/service/notification/ZenModeConfig;->toScheduleConditionId(Landroid/service/notification/ZenModeConfig$ScheduleInfo;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cj:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->zenMode:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->condition:Landroid/service/notification/Condition;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    iput-boolean v2, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->snoozing:Z

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->ch:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cg:Landroid/service/notification/ZenModeConfig$ZenRule;

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cO(Landroid/service/notification/ZenModeConfig$ZenRule;)Landroid/app/AutomaticZenRule;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/SilentModeEditRuleSettings;->cX(Ljava/lang/String;Landroid/app/AutomaticZenRule;)Z

    move-result v0

    return v0
.end method
