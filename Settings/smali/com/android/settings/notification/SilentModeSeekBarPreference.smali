.class public Lcom/android/settings/notification/SilentModeSeekBarPreference;
.super Landroid/preference/Preference;
.source "SilentModeSeekBarPreference.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private dA:Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

.field private dB:Landroid/widget/TextView;

.field private dC:Lmiui/widget/SeekBar;

.field private dD:Landroid/widget/TextView;

.field private dE:Landroid/widget/RelativeLayout;

.field private dF:Ljava/util/List;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private eO(I)I
    .locals 2

    const/16 v1, 0xc

    if-gt p1, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p1, -0x19

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, p1, -0x32

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v0, v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    add-int/lit8 v0, p1, -0x4b

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v0, v1, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private eP(Lmiui/widget/SeekBar;)F
    .locals 4

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {p1}, Lmiui/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    int-to-float v0, v0

    invoke-virtual {p1}, Lmiui/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    sub-float v1, v2, v1

    add-float/2addr v0, v1

    return v0
.end method

.method private eQ(I)I
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x32

    if-gt p1, v1, :cond_1

    div-int/lit8 v0, p1, 0x19

    mul-int/lit8 v0, v0, 0x1e

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x4b

    if-gt p1, v1, :cond_2

    const/16 v0, 0x78

    goto :goto_0

    :cond_2
    const/16 v1, 0x64

    if-gt p1, v1, :cond_0

    const/16 v0, 0x1e0

    goto :goto_0
.end method

.method private eR()V
    .locals 8

    const-wide/16 v2, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iget-object v1, v0, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    if-eqz v1, :cond_1

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->tryParseCountdownConditionId(Landroid/net/Uri;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v0, v4

    :goto_0
    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dE:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dC:Lmiui/widget/SeekBar;

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-direct {p0, v0, v1}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->eS(J)I

    move-result v0

    invoke-virtual {v2, v0}, Lmiui/widget/SeekBar;->setProgress(I)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dE:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dC:Lmiui/widget/SeekBar;

    invoke-virtual {v0, v6}, Lmiui/widget/SeekBar;->setProgress(I)V

    goto :goto_1

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method private eS(J)I
    .locals 7

    const-wide/16 v4, 0xe10

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v4

    if-gtz v2, :cond_1

    const-wide/16 v0, 0x48

    div-long v0, p1, v0

    :cond_0
    :goto_0
    long-to-int v0, v0

    return v0

    :cond_1
    const-wide/16 v2, 0x1c20

    cmp-long v2, p1, v2

    if-gtz v2, :cond_2

    sub-long v0, p1, v4

    const-wide/16 v2, 0x90

    div-long/2addr v0, v2

    const-wide/16 v2, 0x32

    add-long/2addr v0, v2

    goto :goto_0

    :cond_2
    const-wide/16 v2, 0x7080

    cmp-long v2, p1, v2

    if-gtz v2, :cond_0

    sub-long v0, p1, v4

    const-wide/16 v2, 0x360

    div-long/2addr v0, v2

    const-wide/16 v2, 0x4b

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method private eT(J)Ljava/lang/String;
    .locals 9

    const-wide/32 v6, 0x36ee80

    const v4, 0xea60

    const/16 v5, 0xa

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    div-long v2, p1, v6

    long-to-int v1, v2

    rem-long v2, p1, v6

    long-to-int v2, v2

    div-int v3, v2, v4

    rem-int/2addr v2, v4

    div-int/lit16 v2, v2, 0x3e8

    if-lez v1, :cond_1

    if-ge v1, v5, :cond_0

    const-string/jumbo v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-ge v3, v5, :cond_2

    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ge v2, v5, :cond_3

    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private eU()V
    .locals 12

    const-wide/16 v10, 0x3e8

    const-wide/16 v2, 0x0

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iget-object v1, v0, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    if-eqz v1, :cond_1

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->tryParseCountdownConditionId(Landroid/net/Uri;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v0, v4

    :goto_0
    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dE:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dC:Lmiui/widget/SeekBar;

    div-long v4, v0, v10

    invoke-direct {p0, v4, v5}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->eS(J)I

    move-result v3

    invoke-virtual {v2, v3}, Lmiui/widget/SeekBar;->setProgress(I)V

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->eT(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dC:Lmiui/widget/SeekBar;

    invoke-direct {p0, v1}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->eP(Lmiui/widget/SeekBar;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dA:Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

    invoke-virtual {v0, v7}, Lcom/android/settings/notification/SilentModeSeekBarPreference$H;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dA:Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dA:Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

    invoke-virtual {v1, v7}, Lcom/android/settings/notification/SilentModeSeekBarPreference$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v10, v11}, Lcom/android/settings/notification/SilentModeSeekBarPreference$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dE:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dC:Lmiui/widget/SeekBar;

    invoke-virtual {v0, v6}, Lmiui/widget/SeekBar;->setProgress(I)V

    goto :goto_1

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method static synthetic eV(Lcom/android/settings/notification/SilentModeSeekBarPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->eU()V

    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->mContext:Landroid/content/Context;

    const v0, 0x7f0a0497

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SeekBar;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dC:Lmiui/widget/SeekBar;

    const v0, 0x7f0a0498

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dE:Landroid/widget/RelativeLayout;

    const v0, 0x7f0a0386

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dF:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dF:Ljava/util/List;

    const v0, 0x7f0a0052

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dF:Ljava/util/List;

    const v0, 0x7f0a02eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dF:Ljava/util/List;

    const v0, 0x7f0a04bc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dF:Ljava/util/List;

    const v0, 0x7f0a01ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dF:Ljava/util/List;

    const v0, 0x7f0a0166

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dF:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dD:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dD:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06010e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dD:Landroid/widget/TextView;

    const/high16 v1, 0x41400000    # 12.0f

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->eR()V

    new-instance v0, Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/notification/SilentModeSeekBarPreference$H;-><init>(Lcom/android/settings/notification/SilentModeSeekBarPreference;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dA:Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dC:Lmiui/widget/SeekBar;

    invoke-virtual {v0, p0}, Lmiui/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dA:Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

    const-wide/16 v2, 0x32

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/notification/SilentModeSeekBarPreference$H;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f0d01d0

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->setLayoutResource(I)V

    invoke-super {p0, p1}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 5

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dE:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->eO(I)I

    move-result v1

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dF:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dD:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dD:Landroid/widget/TextView;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v0, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dD:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06010d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dF:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dD:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dD:Landroid/widget/TextView;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dD:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06010e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dE:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dB:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dA:Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/SilentModeSeekBarPreference$H;->removeMessages(I)V

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->eO(I)I

    move-result v0

    mul-int/lit8 v1, v0, 0x19

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    mul-int/lit8 v0, v0, 0x19

    invoke-direct {p0, v0}, Lcom/android/settings/notification/SilentModeSeekBarPreference;->eQ(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/MiuiSettings$SilenceMode;->getZenMode(Landroid/content/Context;)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dA:Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

    invoke-virtual {v2, v4}, Lcom/android/settings/notification/SilentModeSeekBarPreference$H;->removeMessages(I)V

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-static {v2, v1, v0}, Landroid/app/ExtraNotificationManager;->startCountDownSilenceMode(Landroid/content/Context;II)V

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeSeekBarPreference;->dA:Lcom/android/settings/notification/SilentModeSeekBarPreference$H;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v4, v2, v3}, Lcom/android/settings/notification/SilentModeSeekBarPreference$H;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method
