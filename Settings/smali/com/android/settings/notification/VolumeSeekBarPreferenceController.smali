.class public abstract Lcom/android/settings/notification/VolumeSeekBarPreferenceController;
.super Lcom/android/settings/notification/AdjustVolumeRestrictedPreferenceController;
.source "VolumeSeekBarPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field protected dm:Lcom/android/settings/notification/VolumeSeekBarPreference;

.field protected dn:Lcom/android/settings/notification/VolumeSeekBarPreference$Callback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/notification/VolumeSeekBarPreference$Callback;Lcom/android/settings/core/lifecycle/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/AdjustVolumeRestrictedPreferenceController;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/notification/VolumeSeekBarPreferenceController;->dn:Lcom/android/settings/notification/VolumeSeekBarPreference$Callback;

    if-eqz p3, :cond_0

    invoke-virtual {p3, p0}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    return-void
.end method


# virtual methods
.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/notification/AdjustVolumeRestrictedPreferenceController;->i(Landroid/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/VolumeSeekBarPreferenceController;->p()Z

    move-result v0

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/VolumeSeekBarPreferenceController;->dm:Lcom/android/settings/notification/VolumeSeekBarPreference;

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/VolumeSeekBarPreferenceController;->dm:Lcom/android/settings/notification/VolumeSeekBarPreference;

    return-void
.end method
