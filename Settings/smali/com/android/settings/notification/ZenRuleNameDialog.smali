.class public abstract Lcom/android/settings/notification/ZenRuleNameDialog;
.super Ljava/lang/Object;
.source "ZenRuleNameDialog.java"


# static fields
.field private static final cr:Z


# instance fields
.field private final cs:Landroid/app/AlertDialog;

.field private final ct:Landroid/widget/EditText;

.field private final cu:Z

.field private final cv:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/settings/notification/ZenModeSettings;->eJ:Z

    sput-boolean v0, Lcom/android/settings/notification/ZenRuleNameDialog;->cr:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->cu:Z

    iput-object p2, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->cv:Ljava/lang/CharSequence;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0d028a

    invoke-virtual {v0, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a039f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->ct:Landroid/widget/EditText;

    iget-boolean v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->cu:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->ct:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->ct:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-boolean v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->cu:Z

    if-eqz v0, :cond_2

    const v0, 0x7f12168f

    :goto_1
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/notification/ZenRuleNameDialog$1;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/ZenRuleNameDialog$1;-><init>(Lcom/android/settings/notification/ZenRuleNameDialog;)V

    const v2, 0x7f120c11

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1203c7

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->cs:Landroid/app/AlertDialog;

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const v0, 0x7f1216bf

    goto :goto_1
.end method

.method private dc()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->ct:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->ct:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic dd(Lcom/android/settings/notification/ZenRuleNameDialog;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->cu:Z

    return v0
.end method

.method static synthetic de(Lcom/android/settings/notification/ZenRuleNameDialog;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->cv:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic df(Lcom/android/settings/notification/ZenRuleNameDialog;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/ZenRuleNameDialog;->dc()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract db(Ljava/lang/String;)V
.end method

.method public show()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleNameDialog;->cs:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
