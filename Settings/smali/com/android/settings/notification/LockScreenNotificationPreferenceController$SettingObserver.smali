.class Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;
.super Landroid/database/ContentObserver;
.source "LockScreenNotificationPreferenceController.java"


# instance fields
.field private final p:Landroid/net/Uri;

.field private final q:Landroid/net/Uri;

.field final synthetic r:Lcom/android/settings/notification/LockScreenNotificationPreferenceController;


# direct methods
.method public constructor <init>(Lcom/android/settings/notification/LockScreenNotificationPreferenceController;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->r:Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "lock_screen_allow_private_notifications"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->p:Landroid/net/Uri;

    const-string/jumbo v0, "lock_screen_show_notifications"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->q:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->p:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->q:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->r:Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

    invoke-static {v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->w(Lcom/android/settings/notification/LockScreenNotificationPreferenceController;)V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->r:Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

    invoke-static {v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->u(Lcom/android/settings/notification/LockScreenNotificationPreferenceController;)I

    move-result v0

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->r:Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

    invoke-static {v0}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->v(Lcom/android/settings/notification/LockScreenNotificationPreferenceController;)V

    :cond_1
    return-void
.end method

.method public x(Landroid/content/ContentResolver;Z)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->p:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController$SettingObserver;->q:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method
