.class public Lcom/android/settings/notification/SettingPref;
.super Ljava/lang/Object;
.source "SettingPref.java"


# instance fields
.field protected final dL:I

.field protected dM:Lcom/android/settings/MiuiDropDownPreference;

.field protected final dN:Ljava/lang/String;

.field protected dO:Lcom/android/settings/MiuiTwoStatePreference;

.field protected final dP:I

.field private final dQ:Landroid/net/Uri;

.field private final dR:[I

.field private final mKey:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(ILjava/lang/String;Ljava/lang/String;I[I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/notification/SettingPref;->dP:I

    iput-object p2, p0, Lcom/android/settings/notification/SettingPref;->mKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/notification/SettingPref;->dN:Ljava/lang/String;

    iput p4, p0, Lcom/android/settings/notification/SettingPref;->dL:I

    iput-object p5, p0, Lcom/android/settings/notification/SettingPref;->dR:[I

    iget v0, p0, Lcom/android/settings/notification/SettingPref;->dP:I

    iget-object v1, p0, Lcom/android/settings/notification/SettingPref;->dN:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/settings/notification/SettingPref;->fi(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SettingPref;->dQ:Landroid/net/Uri;

    return-void
.end method

.method protected static fh(ILandroid/content/ContentResolver;Ljava/lang/String;I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    invoke-static {p1, p2, p3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0

    :pswitch_1
    invoke-static {p1, p2, p3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static fi(ILjava/lang/String;)Landroid/net/Uri;
    .locals 1

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    invoke-static {p1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    :pswitch_1
    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static fk(ILandroid/content/ContentResolver;Ljava/lang/String;I)Z
    .locals 1

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    invoke-static {p1, p2, p3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    return v0

    :pswitch_1
    invoke-static {p1, p2, p3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public fc()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dQ:Landroid/net/Uri;

    return-object v0
.end method

.method public fd(Landroid/content/Context;)V
    .locals 5

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/notification/SettingPref;->dP:I

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/notification/SettingPref;->dN:Ljava/lang/String;

    iget v4, p0, Lcom/android/settings/notification/SettingPref;->dL:I

    invoke-static {v1, v2, v3, v4}, Lcom/android/settings/notification/SettingPref;->fh(ILandroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/notification/SettingPref;->dO:Lcom/android/settings/MiuiTwoStatePreference;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/notification/SettingPref;->dO:Lcom/android/settings/MiuiTwoStatePreference;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Lcom/android/settings/MiuiTwoStatePreference;->setChecked(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dM:Lcom/android/settings/MiuiDropDownPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dM:Lcom/android/settings/MiuiDropDownPreference;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDropDownPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public fe(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected ff(Landroid/content/Context;I)Z
    .locals 3

    iget v0, p0, Lcom/android/settings/notification/SettingPref;->dP:I

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/SettingPref;->dN:Ljava/lang/String;

    invoke-static {v0, v1, v2, p2}, Lcom/android/settings/notification/SettingPref;->fk(ILandroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method protected fg(Landroid/content/res/Resources;I)Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public fj(Lcom/android/settings/MiuiSettingsPreferenceFragment;)Landroid/preference/Preference;
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/SettingPref;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v3}, Lcom/android/settings/notification/SettingPref;->fe(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    move-object v1, v2

    :cond_0
    instance-of v0, v1, Lcom/android/settings/MiuiTwoStatePreference;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Lcom/android/settings/MiuiTwoStatePreference;

    iput-object v0, p0, Lcom/android/settings/notification/SettingPref;->dO:Lcom/android/settings/MiuiTwoStatePreference;

    :cond_1
    :goto_0
    invoke-virtual {p0, v3}, Lcom/android/settings/notification/SettingPref;->fd(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dO:Lcom/android/settings/MiuiTwoStatePreference;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/android/settings/notification/SettingPref$1;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/notification/SettingPref$1;-><init>(Lcom/android/settings/notification/SettingPref;Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dO:Lcom/android/settings/MiuiTwoStatePreference;

    return-object v0

    :cond_2
    instance-of v0, v1, Lcom/android/settings/MiuiDropDownPreference;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/android/settings/MiuiDropDownPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SettingPref;->dM:Lcom/android/settings/MiuiDropDownPreference;

    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dR:[I

    array-length v0, v0

    new-array v4, v0, [Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dR:[I

    array-length v0, v0

    new-array v5, v0, [Ljava/lang/CharSequence;

    const/4 v0, 0x0

    :goto_1
    iget-object v6, p0, Lcom/android/settings/notification/SettingPref;->dR:[I

    array-length v6, v6

    if-ge v0, v6, :cond_3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v7, p0, Lcom/android/settings/notification/SettingPref;->dR:[I

    aget v7, v7, v0

    invoke-virtual {p0, v6, v7}, Lcom/android/settings/notification/SettingPref;->fg(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    iget-object v6, p0, Lcom/android/settings/notification/SettingPref;->dR:[I

    aget v6, v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dM:Lcom/android/settings/MiuiDropDownPreference;

    invoke-virtual {v0, v4}, Lcom/android/settings/MiuiDropDownPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dM:Lcom/android/settings/MiuiDropDownPreference;

    invoke-virtual {v0, v5}, Lcom/android/settings/MiuiDropDownPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dM:Lcom/android/settings/MiuiDropDownPreference;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/android/settings/notification/SettingPref$2;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/notification/SettingPref$2;-><init>(Lcom/android/settings/notification/SettingPref;Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->dM:Lcom/android/settings/MiuiDropDownPreference;

    return-object v0

    :cond_5
    return-object v2
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/SettingPref;->mKey:Ljava/lang/String;

    return-object v0
.end method
