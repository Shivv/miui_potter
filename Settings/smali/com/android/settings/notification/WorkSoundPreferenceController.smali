.class public Lcom/android/settings/notification/WorkSoundPreferenceController;
.super Lcom/android/settings/core/c;
.source "WorkSoundPreferenceController.java"

# interfaces
.implements Landroid/support/v7/preference/f;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field private final dq:Lcom/android/settings/notification/AudioHelper;

.field private dr:I

.field private final ds:Landroid/content/BroadcastReceiver;

.field private final dt:Lcom/android/settings/notification/SoundSettings;

.field private final du:Z

.field private dv:Landroid/support/v7/preference/Preference;

.field private dw:Landroid/support/v7/preference/Preference;

.field private dx:Landroid/support/v7/preference/Preference;

.field private dy:Landroid/support/v7/preference/PreferenceGroup;

.field private dz:Landroid/support/v7/preference/TwoStatePreference;

.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/settings/notification/SoundSettings;Lcom/android/settings/core/lifecycle/a;Lcom/android/settings/notification/AudioHelper;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/notification/WorkSoundPreferenceController$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/WorkSoundPreferenceController$1;-><init>(Lcom/android/settings/notification/WorkSoundPreferenceController;)V

    iput-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->ds:Landroid/content/BroadcastReceiver;

    invoke-static {p1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->mUserManager:Landroid/os/UserManager;

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->bqw(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->du:Z

    iput-object p2, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dt:Lcom/android/settings/notification/SoundSettings;

    iput-object p4, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dq:Lcom/android/settings/notification/AudioHelper;

    if-eqz p3, :cond_0

    invoke-virtual {p3, p0}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    return-void
.end method

.method private eB()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eE()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/media/RingtoneManager;->disableSyncFromParent(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eC()V

    return-void
.end method

.method private eC()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dx:Landroid/support/v7/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dx:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dw:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dv:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eL()V

    return-void
.end method

.method private eD()V
    .locals 3

    const v2, 0x7f121665

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dz:Landroid/support/v7/preference/TwoStatePreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dx:Landroid/support/v7/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dx:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/support/v7/preference/Preference;->dks(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dw:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/support/v7/preference/Preference;->dks(I)V

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dv:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/support/v7/preference/Preference;->dks(I)V

    return-void
.end method

.method private eE()Landroid/content/Context;
    .locals 2

    iget v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dr:I

    const/16 v1, -0x2710

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dq:Lcom/android/settings/notification/AudioHelper;

    iget v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dr:I

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/AudioHelper;->hv(I)Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private eI()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dq:Lcom/android/settings/notification/AudioHelper;

    invoke-virtual {v0}, Lcom/android/settings/notification/AudioHelper;->hs()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private eJ(Landroid/content/Context;I)Ljava/lang/CharSequence;
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dq:Lcom/android/settings/notification/AudioHelper;

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {p1}, Landroid/content/Context;->getUserId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/AudioHelper;->hu(Landroid/os/UserManager;I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->mContext:Landroid/content/Context;

    const v1, 0x7f120a1c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-static {p1, p2}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;Landroid/net/Uri;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private eK()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dy:Landroid/support/v7/preference/PreferenceGroup;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dy:Landroid/support/v7/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/support/v7/preference/PreferenceGroup;->setVisible(Z)V

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dz:Landroid/support/v7/preference/TwoStatePreference;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dy:Landroid/support/v7/preference/PreferenceGroup;

    const-string/jumbo v1, "work_use_personal_sounds"

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceGroup;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/TwoStatePreference;

    iput-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dz:Landroid/support/v7/preference/TwoStatePreference;

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dz:Landroid/support/v7/preference/TwoStatePreference;

    new-instance v1, Lcom/android/settings/notification/-$Lambda$C0Nx_xMOjQuGP4wPPelrS2OPQY4;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/-$Lambda$C0Nx_xMOjQuGP4wPPelrS2OPQY4;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/TwoStatePreference;->dkJ(Landroid/support/v7/preference/f;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dx:Landroid/support/v7/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dw:Landroid/support/v7/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dv:Landroid/support/v7/preference/Preference;

    iget-boolean v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->du:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dx:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/support/v7/preference/Preference;->setVisible(Z)V

    iput-object v2, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dx:Landroid/support/v7/preference/Preference;

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eE()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "sync_parent_sounds"

    iget v2, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dr:I

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eD()V

    :goto_0
    return-void

    :cond_4
    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eC()V

    goto :goto_0
.end method

.method private eL()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eE()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dx:Landroid/support/v7/preference/Preference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dx:Landroid/support/v7/preference/Preference;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eJ(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dw:Landroid/support/v7/preference/Preference;

    const/4 v2, 0x2

    invoke-direct {p0, v0, v2}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eJ(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dv:Landroid/support/v7/preference/Preference;

    const/4 v2, 0x4

    invoke-direct {p0, v0, v2}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eJ(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 2

    const-string/jumbo v0, "sound_work_settings_section"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dy:Landroid/support/v7/preference/PreferenceGroup;

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dy:Landroid/support/v7/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dy:Landroid/support/v7/preference/PreferenceGroup;

    invoke-virtual {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceGroup;->setVisible(Z)V

    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "sound_work_settings_section"

    return-object v0
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dq:Lcom/android/settings/notification/AudioHelper;

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/AudioHelper;->ht(Landroid/os/UserManager;)I

    move-result v0

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eI()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dI(Landroid/support/v7/preference/Preference;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method eA()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eE()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/media/RingtoneManager;->enableSyncFromParent(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eD()V

    return-void
.end method

.method public eF(I)V
    .locals 2

    iget v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dr:I

    const/16 v1, -0x2710

    if-ne v0, v1, :cond_0

    iput p1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dr:I

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eK()V

    :cond_0
    return-void
.end method

.method public eG(I)V
    .locals 2

    iget v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dr:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dq:Lcom/android/settings/notification/AudioHelper;

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/AudioHelper;->ht(Landroid/os/UserManager;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dr:I

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eK()V

    :cond_0
    return-void
.end method

.method public eH(Landroid/support/v7/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    const/4 v1, 0x1

    const-string/jumbo v0, "work_ringtone"

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eE()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eJ(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return v1

    :cond_0
    const-string/jumbo v0, "work_notification_ringtone"

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "work_alarm_ringtone"

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    return v1
.end method

.method synthetic eM(Landroid/support/v7/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dt:Lcom/android/settings/notification/SoundSettings;

    invoke-static {v0}, Lcom/android/settings/notification/WorkSoundPreferenceController$UnifyWorkDialogFragment;->eN(Lcom/android/settings/notification/SoundSettings;)V

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eB()V

    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->ds:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.MANAGED_PROFILE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.MANAGED_PROFILE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->ds:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dq:Lcom/android/settings/notification/AudioHelper;

    iget-object v1, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/AudioHelper;->ht(Landroid/os/UserManager;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/WorkSoundPreferenceController;->dr:I

    invoke-direct {p0}, Lcom/android/settings/notification/WorkSoundPreferenceController;->eK()V

    return-void
.end method
