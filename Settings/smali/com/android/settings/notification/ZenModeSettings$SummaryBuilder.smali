.class public Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;
.super Ljava/lang/Object;
.source "ZenModeSettings.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    return-void
.end method

.method private cx(Landroid/app/NotificationManager$Policy;I)Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p1, Landroid/app/NotificationManager$Policy;->priorityCategories:I

    and-int/2addr v1, p2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private cy(Landroid/app/NotificationManager$Policy;I)Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p1, Landroid/app/NotificationManager$Policy;->suppressedVisualEffects:I

    and-int/2addr v1, p2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method append(Ljava/lang/String;ZI)Ljava/lang/String;
    .locals 4

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iget-object v2, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const v2, 0x7f120886

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    return-object p1
.end method

.method cu()Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->getEnabledAutomaticRulesCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    const v1, 0x7f1216d6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f100047

    invoke-virtual {v1, v3, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method cv(Landroid/app/NotificationManager$Policy;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    const v1, 0x7f121691

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cx(Landroid/app/NotificationManager$Policy;I)Z

    move-result v1

    const v2, 0x7f1216a9

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->prepend(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v3}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cx(Landroid/app/NotificationManager$Policy;I)Z

    move-result v1

    const v2, 0x7f1216bb

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->prepend(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-direct {p0, p1, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cx(Landroid/app/NotificationManager$Policy;I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Landroid/app/NotificationManager$Policy;->priorityMessageSenders:I

    if-nez v1, :cond_2

    const v1, 0x7f121693

    invoke-virtual {p0, v0, v3, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->append(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    const/16 v1, 0x8

    invoke-direct {p0, p1, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cx(Landroid/app/NotificationManager$Policy;I)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, p1, Landroid/app/NotificationManager$Policy;->priorityCallSenders:I

    if-nez v1, :cond_3

    const v1, 0x7f121692

    invoke-virtual {p0, v0, v3, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->append(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    const v1, 0x7f1216d5

    invoke-virtual {p0, v0, v3, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->append(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const v1, 0x7f1216d4

    invoke-virtual {p0, v0, v3, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->append(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const/16 v1, 0x10

    invoke-direct {p0, p1, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cx(Landroid/app/NotificationManager$Policy;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f1216bc

    invoke-virtual {p0, v0, v3, v1}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->append(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method cw(Landroid/app/NotificationManager$Policy;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    const v1, 0x7f121694

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v3}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cy(Landroid/app/NotificationManager$Policy;I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v2}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cy(Landroid/app/NotificationManager$Policy;I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    const v1, 0x7f1216b3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, v3}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cy(Landroid/app/NotificationManager$Policy;I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    const v1, 0x7f1216d3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, v2}, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->cy(Landroid/app/NotificationManager$Policy;I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    const v1, 0x7f1216d0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method getEnabledAutomaticRulesCount()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/NotificationManager;->getAutomaticZenRules()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AutomaticZenRule;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AutomaticZenRule;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v1, v0

    :cond_2
    return v1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method prepend(Ljava/lang/String;ZI)Ljava/lang/String;
    .locals 4

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/settings/notification/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const v2, 0x7f120886

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    return-object p1
.end method
