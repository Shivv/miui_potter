.class final Lcom/android/settings/notification/NotificationSettingsBase$1;
.super Ljava/lang/Object;
.source "NotificationSettingsBase.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic hK:Lcom/android/settings/notification/NotificationSettingsBase;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/NotificationSettingsBase;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/NotificationSettingsBase$1;->hK:Lcom/android/settings/notification/NotificationSettingsBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, -0x3e8

    :goto_0
    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase$1;->hK:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v3, v3, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v3, v0}, Landroid/app/NotificationChannel;->setImportance(I)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase$1;->hK:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v0, v0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/app/NotificationChannel;->lockFields(I)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase$1;->hK:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v0, v0, Lcom/android/settings/notification/NotificationSettingsBase;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase$1;->hK:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v3, v3, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/notification/NotificationSettingsBase$1;->hK:Lcom/android/settings/notification/NotificationSettingsBase;

    iget v4, v4, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I

    iget-object v5, p0, Lcom/android/settings/notification/NotificationSettingsBase$1;->hK:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v5, v5, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0, v3, v4, v5}, Lcom/android/settings/notification/NotificationBackend;->bu(Ljava/lang/String;ILandroid/app/NotificationChannel;)V

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase$1;->hK:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase$1;->hK:Lcom/android/settings/notification/NotificationSettingsBase;

    iget-object v0, v0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v0}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/android/settings/notification/NotificationSettingsBase;->Q(Z)V

    return v1

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method
