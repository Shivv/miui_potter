.class public Lcom/android/settings/notification/ZenModeVoiceActivity;
.super Lcom/android/settings/utils/c;
.source "ZenModeVoiceActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/utils/c;-><init>()V

    return-void
.end method

.method private A(ILandroid/service/notification/Condition;)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    invoke-static {p0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    iget-object v1, p2, Landroid/service/notification/Condition;->id:Landroid/net/Uri;

    const-string/jumbo v2, "ZenModeVoiceActivity"

    invoke-virtual {v0, p1, v1, v2}, Landroid/app/NotificationManager;->setZenMode(ILandroid/net/Uri;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    const-string/jumbo v1, "ZenModeVoiceActivity"

    invoke-virtual {v0, p1, v2, v1}, Landroid/app/NotificationManager;->setZenMode(ILandroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private y(II)Ljava/lang/CharSequence;
    .locals 12

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v0, -0x1

    const/4 v8, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move v1, v0

    move v2, v0

    move v3, v0

    :goto_0
    if-ltz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModeVoiceActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    const v3, 0x7f1216da

    const v2, 0x7f100049

    const v1, 0x7f100048

    const v0, 0x7f1216d9

    move v11, v0

    move v0, v3

    move v3, v2

    move v2, v1

    move v1, v11

    goto :goto_0

    :pswitch_2
    const v1, 0x7f1216db

    move v2, v0

    move v3, v0

    move v11, v0

    move v0, v1

    move v1, v11

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const v0, 0xea60

    mul-int/2addr v0, p2

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "Hm"

    :goto_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-static {v6, v0}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeVoiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/16 v5, 0x3c

    if-ge p2, v5, :cond_3

    new-array v1, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    aput-object v0, v1, v9

    invoke-virtual {v4, v3, p2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string/jumbo v0, "hma"

    goto :goto_1

    :cond_3
    rem-int/lit8 v3, p2, 0x3c

    if-eqz v3, :cond_4

    new-array v2, v9, [Ljava/lang/Object;

    aput-object v0, v2, v8

    invoke-virtual {v4, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    div-int/lit8 v1, p2, 0x3c

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v8

    aput-object v0, v3, v9

    invoke-virtual {v4, v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected z(Landroid/content/Intent;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v2, 0x0

    const-string/jumbo v1, "android.settings.extra.do_not_disturb_mode_enabled"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "android.settings.extra.do_not_disturb_mode_minutes"

    const/4 v3, -0x1

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string/jumbo v1, "android.settings.extra.do_not_disturb_mode_enabled"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    if-lez v3, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {p0, v3, v0}, Landroid/service/notification/ZenModeConfig;->toTimeCondition(Landroid/content/Context;II)Landroid/service/notification/Condition;

    move-result-object v0

    :cond_0
    const/4 v1, 0x3

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/android/settings/notification/ZenModeVoiceActivity;->A(ILandroid/service/notification/Condition;)V

    const-string/jumbo v0, "audio"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModeVoiceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    const/4 v4, 0x5

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v2, v5}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    :cond_1
    invoke-direct {p0, v1, v3}, Lcom/android/settings/notification/ZenModeVoiceActivity;->y(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModeVoiceActivity;->ayo(Ljava/lang/CharSequence;)V

    :goto_1
    return v2

    :cond_2
    const-string/jumbo v0, "ZenModeVoiceActivity"

    const-string/jumbo v1, "Missing extra android.provider.Settings.EXTRA_DO_NOT_DISTURB_MODE_ENABLED"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenModeVoiceActivity;->finish()V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method
