.class public Lcom/android/settings/notification/ZenRuleInfo;
.super Ljava/lang/Object;
.source "ZenRuleInfo.java"


# instance fields
.field public packageName:Ljava/lang/String;

.field public s:Landroid/content/ComponentName;

.field public t:Landroid/net/Uri;

.field public title:Ljava/lang/String;

.field public u:Z

.field public v:Ljava/lang/CharSequence;

.field public w:I

.field public x:Landroid/content/ComponentName;

.field public y:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/notification/ZenRuleInfo;->w:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/notification/ZenRuleInfo;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    :cond_1
    return v1

    :cond_2
    check-cast p1, Lcom/android/settings/notification/ZenRuleInfo;

    iget-boolean v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->u:Z

    iget-boolean v3, p1, Lcom/android/settings/notification/ZenRuleInfo;->u:Z

    if-eq v2, v3, :cond_3

    return v1

    :cond_3
    iget v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->w:I

    iget v3, p1, Lcom/android/settings/notification/ZenRuleInfo;->w:I

    if-eq v2, v3, :cond_4

    return v1

    :cond_4
    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->packageName:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->packageName:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/settings/notification/ZenRuleInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_7

    :cond_5
    return v1

    :cond_6
    iget-object v2, p1, Lcom/android/settings/notification/ZenRuleInfo;->packageName:Ljava/lang/String;

    if-nez v2, :cond_5

    :cond_7
    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->title:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/settings/notification/ZenRuleInfo;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_a

    :cond_8
    return v1

    :cond_9
    iget-object v2, p1, Lcom/android/settings/notification/ZenRuleInfo;->title:Ljava/lang/String;

    if-nez v2, :cond_8

    :cond_a
    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->y:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->y:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/settings/notification/ZenRuleInfo;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_d

    :cond_b
    return v1

    :cond_c
    iget-object v2, p1, Lcom/android/settings/notification/ZenRuleInfo;->y:Ljava/lang/String;

    if-nez v2, :cond_b

    :cond_d
    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->s:Landroid/content/ComponentName;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->s:Landroid/content/ComponentName;

    iget-object v3, p1, Lcom/android/settings/notification/ZenRuleInfo;->s:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_10

    :cond_e
    return v1

    :cond_f
    iget-object v2, p1, Lcom/android/settings/notification/ZenRuleInfo;->s:Landroid/content/ComponentName;

    if-nez v2, :cond_e

    :cond_10
    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->t:Landroid/net/Uri;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->t:Landroid/net/Uri;

    iget-object v3, p1, Lcom/android/settings/notification/ZenRuleInfo;->t:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_13

    :cond_11
    return v1

    :cond_12
    iget-object v2, p1, Lcom/android/settings/notification/ZenRuleInfo;->t:Landroid/net/Uri;

    if-nez v2, :cond_11

    :cond_13
    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->x:Landroid/content/ComponentName;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->x:Landroid/content/ComponentName;

    iget-object v3, p1, Lcom/android/settings/notification/ZenRuleInfo;->x:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_16

    :cond_14
    return v1

    :cond_15
    iget-object v2, p1, Lcom/android/settings/notification/ZenRuleInfo;->x:Landroid/content/ComponentName;

    if-nez v2, :cond_14

    :cond_16
    iget-object v2, p0, Lcom/android/settings/notification/ZenRuleInfo;->v:Ljava/lang/CharSequence;

    if-eqz v2, :cond_18

    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleInfo;->v:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/android/settings/notification/ZenRuleInfo;->v:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_17
    :goto_0
    return v0

    :cond_18
    iget-object v2, p1, Lcom/android/settings/notification/ZenRuleInfo;->v:Ljava/lang/CharSequence;

    if-eqz v2, :cond_17

    move v0, v1

    goto :goto_0
.end method
