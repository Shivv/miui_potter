.class public abstract Lcom/android/settings/notification/NotificationSettingsBase;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "NotificationSettingsBase.java"


# static fields
.field private static final es:Landroid/content/Intent;

.field private static final et:Z


# instance fields
.field protected eb:I

.field protected ec:Ljava/lang/String;

.field protected ed:Landroid/content/pm/PackageInfo;

.field protected ee:Z

.field protected ef:Lcom/android/settings/notification/NotificationBackend;

.field protected eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

.field protected eh:Landroid/app/NotificationChannel;

.field protected ei:Lcom/android/settingslib/n;

.field protected ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field protected ek:Lcom/android/settings/widget/SwitchBar;

.field protected el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field protected em:Lcom/android/settings/applications/MiuiLayoutPreference;

.field protected en:Lcom/android/settings/widget/MiuiFooterPreference;

.field protected eo:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field protected ep:Z

.field protected eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

.field protected er:Landroid/preference/Preference;

.field protected eu:Z

.field protected ev:Landroid/app/NotificationManager;

.field protected ew:Landroid/content/pm/PackageManager;

.field protected ex:Landroid/preference/CheckBoxPreference;

.field protected ey:Landroid/os/UserManager;

.field protected mContext:Landroid/content/Context;

.field protected mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field protected mUserId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "NotifiSettingsBase"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/notification/NotificationSettingsBase;->et:Z

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.category.NOTIFICATION_PREFERENCES"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/android/settings/notification/NotificationSettingsBase;->es:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/notification/NotificationBackend;

    invoke-direct {v0}, Lcom/android/settings/notification/NotificationBackend;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ef:Lcom/android/settings/notification/NotificationBackend;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ee:Z

    return-void
.end method

.method private fA(Landroid/util/ArrayMap;Ljava/util/List;)V
    .locals 5

    sget-boolean v0, Lcom/android/settings/notification/NotificationSettingsBase;->et:Z

    if-eqz v0, :cond_0

    const-string/jumbo v1, "NotifiSettingsBase"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " preference activities"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, " ;_;"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/notification/NotificationBackend$AppRow;

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/android/settings/notification/NotificationSettingsBase;->et:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "NotifiSettingsBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Ignoring notification preference activity ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ") for unknown package "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_3
    iget-object v3, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aL:Landroid/content/Intent;

    if-eqz v3, :cond_4

    sget-boolean v0, Lcom/android/settings/notification/NotificationSettingsBase;->et:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "NotifiSettingsBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Ignoring duplicate notification preference activity ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ") for package "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    new-instance v3, Landroid/content/Intent;

    sget-object v4, Lcom/android/settings/notification/NotificationSettingsBase;->es:Landroid/content/Intent;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v4, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    iput-object v2, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aL:Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aL:Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.extra.CHANNEL_ID"

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    :cond_5
    return-void
.end method

.method private fB(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    .locals 8

    const/4 v0, 0x0

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    return-object v7

    :cond_1
    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ew:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    if-eqz p1, :cond_3

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ew:Landroid/content/pm/PackageManager;

    const/16 v4, 0x40

    invoke-virtual {v0, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v4, "NotifiSettingsBase"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Failed to load package "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return-object v7
.end method

.method private fC()I
    .locals 2

    const/16 v0, -0x3e8

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fE()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fD()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private fD()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "lock_screen_allow_private_notifications"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private fE()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "lock_screen_show_notifications"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private fF()Ljava/util/List;
    .locals 3

    sget-boolean v0, Lcom/android/settings/notification/NotificationSettingsBase;->et:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "NotifiSettingsBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "APP_NOTIFICATION_PREFS_CATEGORY_INTENT is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/settings/notification/NotificationSettingsBase;->es:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ew:Landroid/content/pm/PackageManager;

    sget-object v1, Lcom/android/settings/notification/NotificationSettingsBase;->es:Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private fG(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mUserId:I

    invoke-static {v0, p3, v1}, Lcom/android/settingslib/w;->cqR(Landroid/content/Context;II)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    invoke-direct {v1, p1, p2, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aB(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;)V

    :cond_0
    return-void
.end method

.method private fH()V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string/jumbo v0, "allow_sound"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/NotificationSettingsBase;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ei:Lcom/android/settingslib/n;

    invoke-virtual {v0, v3}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase;->el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/NotificationSettingsBase;->ft(Landroid/app/NotificationChannel;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqw()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v3

    const/4 v4, 0x3

    if-ge v3, v4, :cond_2

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v3}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v3

    const/16 v4, -0x3e8

    if-ne v3, v4, :cond_0

    move v1, v2

    :cond_0
    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    new-instance v1, Lcom/android/settings/notification/NotificationSettingsBase$1;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/NotificationSettingsBase$1;-><init>(Lcom/android/settings/notification/NotificationSettingsBase;)V

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method static synthetic fJ(Lcom/android/settings/notification/NotificationSettingsBase;)I
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fC()I

    move-result v0

    return v0
.end method


# virtual methods
.method abstract O()V
.end method

.method abstract Q(Z)V
.end method

.method protected fI()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mContext:Landroid/content/Context;

    const v1, 0x7f12013f

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method protected fp()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ed:Landroid/content/pm/PackageInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    if-eqz v1, :cond_0

    const v1, 0x7f150076

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/NotificationSettingsBase;->addPreferencesFromResource(I)V

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->canBypassDnd()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/NotificationSettingsBase;->fy(Z)V

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getLockscreenVisibility()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/NotificationSettingsBase;->fz(I)V

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fH()V

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->O()V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ex:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ex:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v2, v2, Lcom/android/settings/notification/NotificationBackend$AppRow;->aF:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v2}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_2
    return-void
.end method

.method protected fq()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aL:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->er:Landroid/preference/Preference;

    if-nez v0, :cond_0

    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->er:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->er:Landroid/preference/Preference;

    const-string/jumbo v1, "app_link"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->er:Landroid/preference/Preference;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOrder(I)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->er:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->aL:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->er:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mContext:Landroid/content/Context;

    const v2, 0x7f12015a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->er:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method protected fr(Landroid/util/ArrayMap;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fF()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/notification/NotificationSettingsBase;->fA(Landroid/util/ArrayMap;Ljava/util/List;)V

    return-void
.end method

.method protected fs(ZLandroid/app/NotificationChannel;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v2, v2, Lcom/android/settings/notification/NotificationBackend$AppRow;->aG:Z

    if-nez v2, :cond_0

    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/app/NotificationChannel;->isBlockableSystem()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p2}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected ft(Landroid/app/NotificationChannel;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->aJ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected fu(Landroid/preference/Preference;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-ne v0, p2, :cond_1

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method protected fv(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "block_desc"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/MiuiFooterPreference;

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->en:Lcom/android/settings/widget/MiuiFooterPreference;

    new-instance v0, Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/widget/MiuiFooterPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->en:Lcom/android/settings/widget/MiuiFooterPreference;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->en:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/MiuiFooterPreference;->setSelectable(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->en:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/MiuiFooterPreference;->setTitle(I)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->en:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/MiuiFooterPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->en:Lcom/android/settings/widget/MiuiFooterPreference;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiFooterPreference;->setOrder(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->en:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method protected fw(I)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v1

    const/16 v2, -0x3e8

    if-ne v1, v2, :cond_0

    return v0

    :cond_0
    if-lt v1, p1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected fx()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ey:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/os/UserManager;->getProfileParent(I)Landroid/content/pm/UserInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, v1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v2, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v1

    or-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method protected fy(Z)V
    .locals 2

    const-string/jumbo v0, "bypass_dnd"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/NotificationSettingsBase;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eo:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eo:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ei:Lcom/android/settingslib/n;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eo:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/NotificationSettingsBase;->ft(Landroid/app/NotificationChannel;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eo:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->cqw()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eo:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eo:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    new-instance v1, Lcom/android/settings/notification/NotificationSettingsBase$2;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/NotificationSettingsBase$2;-><init>(Lcom/android/settings/notification/NotificationSettingsBase;)V

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected fz(I)V
    .locals 6

    const/16 v5, -0x3e8

    const-string/jumbo v0, "visibility_override"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/NotificationSettingsBase;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v2}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aC()V

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fE()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fD()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f120978

    invoke-virtual {p0, v2}, Lcom/android/settings/notification/NotificationSettingsBase;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0xc

    invoke-direct {p0, v2, v3, v4}, Lcom/android/settings/notification/NotificationSettingsBase;->fG(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    :cond_0
    const v2, 0x7f120976

    invoke-virtual {p0, v2}, Lcom/android/settings/notification/NotificationSettingsBase;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x4

    invoke-direct {p0, v2, v3, v4}, Lcom/android/settings/notification/NotificationSettingsBase;->fG(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    const v2, 0x7f120974

    invoke-virtual {p0, v2}, Lcom/android/settings/notification/NotificationSettingsBase;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    if-ne p1, v5, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fC()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setValue(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    const-string/jumbo v1, "%s"

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    new-instance v1, Lcom/android/settings/notification/NotificationSettingsBase$3;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/NotificationSettingsBase$3;-><init>(Lcom/android/settings/notification/NotificationSettingsBase;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ei:Lcom/android/settingslib/n;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    sget-boolean v0, Lcom/android/settings/notification/NotificationSettingsBase;->et:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "NotifiSettingsBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onActivityCreated mCreated="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eu:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eu:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "NotifiSettingsBase"

    const-string/jumbo v1, "onActivityCreated: ignoring duplicate call"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eu:Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    sget-boolean v0, Lcom/android/settings/notification/NotificationSettingsBase;->et:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "NotifiSettingsBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onCreate getIntent()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez v1, :cond_1

    if-nez v2, :cond_1

    const-string/jumbo v0, "NotifiSettingsBase"

    const-string/jumbo v1, "No intent"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fI()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ew:Landroid/content/pm/PackageManager;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "user"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ey:Landroid/os/UserManager;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ev:Landroid/app/NotificationManager;

    if-eqz v2, :cond_4

    const-string/jumbo v0, "package"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "package"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    if-eqz v2, :cond_5

    const-string/jumbo v0, "uid"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "uid"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I

    iget v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I

    if-gez v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ew:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageUid(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    iget v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I

    invoke-direct {p0, v0, v1}, Lcom/android/settings/notification/NotificationSettingsBase;->fB(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ed:Landroid/content/pm/PackageInfo;

    iget v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ed:Landroid/content/pm/PackageInfo;

    if-nez v0, :cond_6

    :cond_3
    const-string/jumbo v0, "NotifiSettingsBase"

    const-string/jumbo v1, "Missing package or uid or packageinfo"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->fI()V

    return-void

    :cond_4
    const-string/jumbo v0, "android.provider.extra.APP_PACKAGE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const-string/jumbo v0, "app_uid"

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I

    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mUserId:I

    return-void

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public onResume()V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ed:Landroid/content/pm/PackageInfo;

    if-nez v2, :cond_1

    :cond_0
    const-string/jumbo v0, "NotifiSettingsBase"

    const-string/jumbo v1, "Missing package or uid or packageinfo"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->finish()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ew:Landroid/content/pm/PackageManager;

    iget-object v5, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ed:Landroid/content/pm/PackageInfo;

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/settings/notification/NotificationBackend;->bA(Landroid/content/Context;Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInfo;)Lcom/android/settings/notification/NotificationBackend$AppRow;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    invoke-virtual {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string/jumbo v3, "android.provider.extra.CHANNEL_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v3, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    iget v4, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eb:I

    const-string/jumbo v5, "android.provider.extra.CHANNEL_ID"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v4, v2}, Lcom/android/settings/notification/NotificationBackend;->bq(Ljava/lang/String;ILjava/lang/String;)Landroid/app/NotificationChannel;

    move-result-object v0

    :cond_2
    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->eh:Landroid/app/NotificationChannel;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    iget v3, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mUserId:I

    invoke-static {v0, v2, v3}, Lcom/android/settingslib/w;->cqT(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ei:Lcom/android/settingslib/n;

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ev:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getNotificationPolicy()Landroid/app/NotificationManager$Policy;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ep:Z

    iget-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ec:Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/notification/NotificationSettingsBase;->mUserId:I

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->cqT(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/NotificationSettingsBase;->ei:Lcom/android/settingslib/n;

    return-void

    :cond_4
    iget v0, v0, Landroid/app/NotificationManager$Policy;->suppressedVisualEffects:I

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0
.end method
