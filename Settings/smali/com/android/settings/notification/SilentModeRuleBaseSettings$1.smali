.class final Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;
.super Ljava/lang/Object;
.source "SilentModeRuleBaseSettings.java"

# interfaces
.implements Lmiui/app/TimePickerDialog$OnTimeSetListener;


# instance fields
.field final synthetic gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/SilentModeRuleBaseSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeSet(Lmiui/widget/TimePicker;II)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    iget-boolean v0, v0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cm:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    mul-int/lit8 v1, p2, 0x3c

    add-int/2addr v1, p3

    iput v1, v0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ck:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    iget-object v0, v0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cl:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    iget v2, v2, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->ck:I

    invoke-static {v1, v2}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cZ(Lcom/android/settings/notification/SilentModeRuleBaseSettings;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    mul-int/lit8 v1, p2, 0x3c

    add-int/2addr v1, p3

    iput v1, v0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bW:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    iget-object v0, v0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bX:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;->gX:Lcom/android/settings/notification/SilentModeRuleBaseSettings;

    iget v2, v2, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->bW:I

    invoke-static {v1, v2}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->cZ(Lcom/android/settings/notification/SilentModeRuleBaseSettings;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto :goto_0
.end method
