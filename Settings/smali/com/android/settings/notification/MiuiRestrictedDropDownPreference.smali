.class public Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;
.super Landroid/preference/ListPreference;
.source "MiuiRestrictedDropDownPreference.java"


# instance fields
.field private final ad:Lcom/android/settingslib/o;

.field private final ae:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private af:Landroid/preference/Preference$OnPreferenceClickListener;

.field private ag:Ljava/util/List;

.field private ah:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;

.field private ai:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ag:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ai:Z

    new-instance v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$1;-><init>(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;)V

    iput-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ae:Landroid/widget/AdapterView$OnItemSelectedListener;

    const v0, 0x7f0d0192

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setLayoutResource(I)V

    const v0, 0x7f0d0190

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->setWidgetLayoutResource(I)V

    new-instance v0, Lcom/android/settingslib/o;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/o;-><init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ad:Lcom/android/settingslib/o;

    return-void
.end method

.method private aD(Ljava/lang/CharSequence;)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_0

    return-object v3

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ag:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    iget-object v2, v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;->an:Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v0

    :cond_2
    return-object v3
.end method

.method private aE(I)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;
    .locals 1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-direct {p0, v0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aD(Ljava/lang/CharSequence;)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    move-result-object v0

    return-object v0
.end method

.method private aG(Ljava/lang/CharSequence;)Z
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ag:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    iget-object v0, v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;->am:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    return v2
.end method

.method private aH()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ai:Z

    return v0
.end method

.method private aI(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ai:Z

    return-void
.end method

.method static synthetic aJ(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ah:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;

    return-object v0
.end method

.method static synthetic aK(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ai:Z

    return v0
.end method

.method static synthetic aL(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ai:Z

    return p1
.end method

.method static synthetic aM(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic aN(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;Ljava/lang/CharSequence;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aG(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method static synthetic aO(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aH()Z

    move-result v0

    return v0
.end method

.method static synthetic aP(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;Ljava/lang/CharSequence;)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aD(Ljava/lang/CharSequence;)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aQ(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;I)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aE(I)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aR(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aI(Z)V

    return-void
.end method


# virtual methods
.method public aA(Landroid/preference/Preference$OnPreferenceClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->af:Landroid/preference/Preference$OnPreferenceClickListener;

    return-void
.end method

.method public aB(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ag:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public aC()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ag:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public aF()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ad:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->cqj()Z

    move-result v0

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0a0420

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ah:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ah:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;

    invoke-virtual {v0, p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;->setPreference(Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;)V

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ad:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ah:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ae:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$ReselectionSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v0, 0x7f0a038d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aF()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public performClick(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->af:Landroid/preference/Preference$OnPreferenceClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->af:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-interface {v0, p0}, Landroid/preference/Preference$OnPreferenceClickListener;->onPreferenceClick(Landroid/preference/Preference;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ad:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->performClick()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ai:Z

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    :cond_1
    return-void
.end method

.method public setDisabledByAdmin(Lcom/android/settingslib/n;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ad:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aF()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->ad:Lcom/android/settingslib/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->setEnabled(Z)V

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;->aD(Ljava/lang/CharSequence;)Lcom/android/settings/notification/MiuiRestrictedDropDownPreference$RestrictedItem;

    move-result-object v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    return-void
.end method
