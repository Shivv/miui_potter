.class final Lcom/android/settings/notification/ZenRuleSelectionDialog$1;
.super Ljava/lang/Object;
.source "ZenRuleSelectionDialog.java"

# interfaces
.implements Lcom/android/settings/utils/j;


# instance fields
.field final synthetic hn:Lcom/android/settings/notification/ZenRuleSelectionDialog;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/ZenRuleSelectionDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog$1;->hn:Lcom/android/settings/notification/ZenRuleSelectionDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hO(Ljava/util/Set;)V
    .locals 6

    invoke-static {}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->-get0()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ZenRuleSelectionDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Services reloaded: count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Ljava/util/TreeSet;

    invoke-static {}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->dX()Ljava/util/Comparator;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ServiceInfo;

    iget-object v3, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog$1;->hn:Lcom/android/settings/notification/ZenRuleSelectionDialog;

    invoke-static {v3}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->ea(Lcom/android/settings/notification/ZenRuleSelectionDialog;)Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/android/settings/notification/ZenModeSettings;->bX(Landroid/content/pm/PackageManager;Landroid/content/pm/ServiceInfo;)Lcom/android/settings/notification/ZenRuleInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v4, v3, Lcom/android/settings/notification/ZenRuleInfo;->s:Landroid/content/ComponentName;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog$1;->hn:Lcom/android/settings/notification/ZenRuleSelectionDialog;

    invoke-static {v4}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->dZ(Lcom/android/settings/notification/ZenRuleSelectionDialog;)Landroid/app/NotificationManager;

    move-result-object v4

    iget-object v5, v3, Lcom/android/settings/notification/ZenRuleInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->isNotificationPolicyAccessGrantedForPackage(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, v3, Lcom/android/settings/notification/ZenRuleInfo;->w:I

    if-lez v4, :cond_2

    iget v4, v3, Lcom/android/settings/notification/ZenRuleInfo;->w:I

    iget-object v5, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog$1;->hn:Lcom/android/settings/notification/ZenRuleSelectionDialog;

    invoke-static {v5}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->dZ(Lcom/android/settings/notification/ZenRuleSelectionDialog;)Landroid/app/NotificationManager;

    move-result-object v5

    invoke-virtual {v0}, Landroid/content/pm/ServiceInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/NotificationManager;->getRuleInstanceCount(Landroid/content/ComponentName;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-lt v4, v0, :cond_1

    :cond_2
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/notification/ZenRuleSelectionDialog$1;->hn:Lcom/android/settings/notification/ZenRuleSelectionDialog;

    invoke-static {v0, v1}, Lcom/android/settings/notification/ZenRuleSelectionDialog;->ed(Lcom/android/settings/notification/ZenRuleSelectionDialog;Ljava/util/Set;)V

    return-void
.end method
