.class public Lcom/android/settings/notification/SilentModeUtils;
.super Ljava/lang/Object;
.source "SilentModeUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dJ(Landroid/content/Context;)Landroid/app/AutomaticZenRule;
    .locals 8

    invoke-static {p0}, Landroid/provider/MiuiSettings$AntiSpam;->getStartTimeForQuietMode(Landroid/content/Context;)I

    move-result v0

    invoke-static {p0}, Landroid/provider/MiuiSettings$AntiSpam;->getEndTimeForQuietMode(Landroid/content/Context;)I

    move-result v1

    invoke-static {p0}, Landroid/provider/MiuiSettings$AntiSpam;->getQuietRepeatType(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Lcom/android/settings/notification/SilentModeUtils;->dN(I)[Z

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/notification/SilentModeUtils;->dK([Z)[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/notification/SilentModeUtils;->dM(II[I)Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;

    move-result-object v3

    const v0, 0x7f120560

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/app/AutomaticZenRule;

    iget-object v2, v3, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->co:Landroid/content/ComponentName;

    iget-object v3, v3, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->cn:Landroid/net/Uri;

    const/4 v4, 0x1

    invoke-static {v4}, Landroid/app/NotificationManager;->zenModeToInterruptionFilter(I)I

    move-result v4

    invoke-static {p0}, Landroid/provider/MiuiSettings$AntiSpam;->isAutoTimerOfQuietModeEnable(Landroid/content/Context;)Z

    move-result v5

    const-wide/16 v6, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/app/AutomaticZenRule;-><init>(Ljava/lang/String;Landroid/content/ComponentName;Landroid/net/Uri;IZJ)V

    return-object v0
.end method

.method public static dK([Z)[I
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_1

    aget-boolean v3, p0, v0

    if-eqz v3, :cond_0

    add-int/lit8 v3, v0, 0x1

    rem-int/lit8 v3, v3, 0x7

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [I

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    move v1, v2

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method public static dL(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;
    .locals 1

    invoke-static {p0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getZenModeConfig()Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    return-object v0
.end method

.method public static dM(II[I)Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;
    .locals 3

    new-instance v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    invoke-direct {v0}, Landroid/service/notification/ZenModeConfig$ScheduleInfo;-><init>()V

    iput-object p2, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->days:[I

    div-int/lit8 v1, p0, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startHour:I

    rem-int/lit8 v1, p0, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startMinute:I

    div-int/lit8 v1, p1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endHour:I

    rem-int/lit8 v1, p1, 0x3c

    iput v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endMinute:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->exitAtAlarm:Z

    new-instance v1, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;

    invoke-direct {v1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;-><init>()V

    const-string/jumbo v2, "SilentModeRuleSettings"

    iput-object v2, v1, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->cp:Ljava/lang/String;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->toScheduleConditionId(Landroid/service/notification/ZenModeConfig$ScheduleInfo;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->cn:Landroid/net/Uri;

    invoke-static {}, Landroid/service/notification/ZenModeConfig;->getScheduleConditionProvider()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, v1, Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;->co:Landroid/content/ComponentName;

    return-object v1
.end method

.method public static dN(I)[Z
    .locals 5

    const/4 v4, 0x7

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-array v1, v4, [Z

    :goto_0
    if-ge v0, v4, :cond_1

    shl-int v2, v3, v0

    and-int/2addr v2, p0

    if-eqz v2, :cond_0

    aput-boolean v3, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static dO([I)I
    .locals 8

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    aget v2, p0, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    add-int/lit8 v1, v1, 0x40

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    int-to-double v2, v1

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    aget v1, p0, v0

    add-int/lit8 v1, v1, -0x2

    int-to-double v6, v1

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    add-double/2addr v2, v4

    double-to-int v1, v2

    goto :goto_1

    :cond_1
    return v1
.end method
