.class public Lcom/android/settings/notification/MiuiSilentSettingsFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiSilentSettingsFragment.java"


# instance fields
.field private bA:Landroid/preference/Preference$OnPreferenceClickListener;

.field private bB:Landroid/preference/CheckBoxPreference;

.field private bC:Landroid/preference/PreferenceScreen;

.field private final bD:Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;

.field private bE:Lmiui/preference/RadioButtonPreferenceCategory;

.field private bF:Lmiui/preference/RadioButtonPreference;

.field private bG:Lcom/android/settings/dndmode/LabelPreference;

.field private bH:Lmiui/preference/RadioButtonPreference;

.field private bI:Landroid/preference/ListPreference;

.field private bt:Landroid/preference/PreferenceCategory;

.field private bu:Landroid/preference/PreferenceCategory;

.field private bv:Landroid/service/notification/ZenModeConfig;

.field private bw:Landroid/os/Handler;

.field private bx:Landroid/preference/CheckBoxPreference;

.field private by:Lmiui/preference/RadioButtonPreference;

.field private bz:Landroid/preference/CheckBoxPreference;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;-><init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bD:Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;

    new-instance v0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$1;-><init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bA:Landroid/preference/Preference$OnPreferenceClickListener;

    return-void
.end method

.method private cD(Landroid/service/notification/ZenModeConfig;)Z
    .locals 2

    iget-object v0, p1, Landroid/service/notification/ZenModeConfig;->automaticRules:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->enabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public static cE(J)Ljava/lang/String;
    .locals 8

    const-wide/32 v6, 0x36ee80

    const/16 v4, 0xa

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    div-long v2, p0, v6

    long-to-int v1, v2

    rem-long v2, p0, v6

    long-to-int v2, v2

    const v3, 0xea60

    div-int/2addr v2, v3

    if-ge v1, v4, :cond_0

    const-string/jumbo v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ge v2, v4, :cond_1

    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private cF()V
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/MiuiSettings$SilenceMode;->getZenMode(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bw:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bE:Lmiui/preference/RadioButtonPreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bF:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Lmiui/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroid/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bC:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bB:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bz:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bx:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bI:Landroid/preference/ListPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bv:Landroid/service/notification/ZenModeConfig;

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030114

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bv:Landroid/service/notification/ZenModeConfig;

    iget-boolean v0, v0, Landroid/service/notification/ZenModeConfig;->allowCalls:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bv:Landroid/service/notification/ZenModeConfig;

    iget v0, v0, Landroid/service/notification/ZenModeConfig;->allowCallsFrom:I

    :goto_1
    iget-object v2, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bI:Landroid/preference/ListPreference;

    aget-object v1, v1, v0

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bI:Landroid/preference/ListPreference;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bv:Landroid/service/notification/ZenModeConfig;

    invoke-direct {p0, v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cD(Landroid/service/notification/ZenModeConfig;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bG:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120ab4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    :goto_2
    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1200e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    iget-object v1, v0, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    if-eqz v1, :cond_1

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v0, v0, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->tryParseCountdownConditionId(Landroid/net/Uri;)J

    move-result-wide v0

    cmp-long v2, v0, v6

    if-eqz v2, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cE(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bw:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bw:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    return-void

    :cond_2
    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bE:Lmiui/preference/RadioButtonPreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bH:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Lmiui/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroid/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bC:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bI:Landroid/preference/ListPreference;

    invoke-virtual {v0, v4}, Landroid/preference/ListPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bz:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bx:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bB:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bE:Lmiui/preference/RadioButtonPreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->by:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Lmiui/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroid/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bC:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_4
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bG:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120ab3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method static synthetic cG(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic cH(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bw:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic cI(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Lmiui/preference/RadioButtonPreferenceCategory;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bE:Lmiui/preference/RadioButtonPreferenceCategory;

    return-object v0
.end method

.method static synthetic cJ(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic cK(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cF()V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v5, -0x3

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f1500de

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/settings/notification/MiuiSilentSettingsFragment$2;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$2;-><init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bw:Landroid/os/Handler;

    const-string/jumbo v0, "silent_mode_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bC:Landroid/preference/PreferenceScreen;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bD:Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->cL()V

    const-string/jumbo v0, "key_silent_mode_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bE:Lmiui/preference/RadioButtonPreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bE:Lmiui/preference/RadioButtonPreferenceCategory;

    const-string/jumbo v3, "key_normal"

    invoke-virtual {v0, v3}, Lmiui/preference/RadioButtonPreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->by:Lmiui/preference/RadioButtonPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->by:Lmiui/preference/RadioButtonPreference;

    iget-object v3, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bA:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v3}, Lmiui/preference/RadioButtonPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bE:Lmiui/preference/RadioButtonPreferenceCategory;

    const-string/jumbo v3, "key_standard"

    invoke-virtual {v0, v3}, Lmiui/preference/RadioButtonPreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bF:Lmiui/preference/RadioButtonPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bF:Lmiui/preference/RadioButtonPreference;

    iget-object v3, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bA:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v3}, Lmiui/preference/RadioButtonPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bE:Lmiui/preference/RadioButtonPreferenceCategory;

    const-string/jumbo v3, "key_total"

    invoke-virtual {v0, v3}, Lmiui/preference/RadioButtonPreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bH:Lmiui/preference/RadioButtonPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bH:Lmiui/preference/RadioButtonPreference;

    iget-object v3, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bA:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v3}, Lmiui/preference/RadioButtonPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo v0, "key_advanced_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    const-string/jumbo v3, "key_vip_list"

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bI:Landroid/preference/ListPreference;

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bI:Landroid/preference/ListPreference;

    new-instance v3, Lcom/android/settings/notification/MiuiSilentSettingsFragment$3;

    invoke-direct {v3, p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$3;-><init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_0
    const-string/jumbo v0, "key_repeat"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bB:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bB:Landroid/preference/CheckBoxPreference;

    new-instance v3, Lcom/android/settings/notification/MiuiSilentSettingsFragment$4;

    invoke-direct {v3, p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$4;-><init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bB:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/provider/MiuiSettings$AntiSpam;->isRepeatedCallActionEnable(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "key_popup_window"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bz:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bz:Landroid/preference/CheckBoxPreference;

    new-instance v3, Lcom/android/settings/notification/MiuiSilentSettingsFragment$5;

    invoke-direct {v3, p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$5;-><init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v3, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bz:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "show_notification"

    invoke-static {v0, v4, v1, v5}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v1, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "key_mute_music"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bx:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bx:Landroid/preference/CheckBoxPreference;

    new-instance v3, Lcom/android/settings/notification/MiuiSilentSettingsFragment$6;

    invoke-direct {v3, p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$6;-><init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bx:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "mute_music"

    invoke-static {v3, v4, v1, v5}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-ne v1, v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "key_advanced_settings2"

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bu:Landroid/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bu:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "key_timing_mute"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bG:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bG:Lcom/android/settings/dndmode/LabelPreference;

    new-instance v1, Lcom/android/settings/notification/MiuiSilentSettingsFragment$7;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$7;-><init>(Lcom/android/settings/notification/MiuiSilentSettingsFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bC:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bE:Lmiui/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cF()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bt:Landroid/preference/PreferenceCategory;

    iget-object v3, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bI:Landroid/preference/ListPreference;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bw:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->bD:Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment$SettingsObserver;->cM()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/notification/MiuiSilentSettingsFragment;->cF()V

    return-void
.end method
