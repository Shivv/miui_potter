.class final Lcom/android/settings/notification/ZenModePrioritySettings$5;
.super Ljava/lang/Object;
.source "ZenModePrioritySettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic ic:Lcom/android/settings/notification/ZenModePrioritySettings;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/ZenModePrioritySettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/ZenModePrioritySettings$5;->ic:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/android/settings/notification/ZenModePrioritySettings$5;->ic:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-static {v0}, Lcom/android/settings/notification/ZenModePrioritySettings;->gV(Lcom/android/settings/notification/ZenModePrioritySettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v5

    :cond_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sget-boolean v1, Lcom/android/settings/notification/ZenModePrioritySettings;->eJ:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "ZenModeSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onPrefChange allowRepeatCallers="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/android/settings/notification/ZenModePrioritySettings$5;->ic:Lcom/android/settings/notification/ZenModePrioritySettings;

    const/16 v2, 0x10

    invoke-static {v1, v0, v2}, Lcom/android/settings/notification/ZenModePrioritySettings;->gX(Lcom/android/settings/notification/ZenModePrioritySettings;ZI)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/notification/ZenModePrioritySettings$5;->ic:Lcom/android/settings/notification/ZenModePrioritySettings;

    iget-object v2, p0, Lcom/android/settings/notification/ZenModePrioritySettings$5;->ic:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-static {v2}, Lcom/android/settings/notification/ZenModePrioritySettings;->gW(Lcom/android/settings/notification/ZenModePrioritySettings;)Landroid/app/NotificationManager$Policy;

    move-result-object v2

    iget v2, v2, Landroid/app/NotificationManager$Policy;->priorityCallSenders:I

    iget-object v3, p0, Lcom/android/settings/notification/ZenModePrioritySettings$5;->ic:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-static {v3}, Lcom/android/settings/notification/ZenModePrioritySettings;->gW(Lcom/android/settings/notification/ZenModePrioritySettings;)Landroid/app/NotificationManager$Policy;

    move-result-object v3

    iget v3, v3, Landroid/app/NotificationManager$Policy;->priorityMessageSenders:I

    iget-object v4, p0, Lcom/android/settings/notification/ZenModePrioritySettings$5;->ic:Lcom/android/settings/notification/ZenModePrioritySettings;

    invoke-static {v4}, Lcom/android/settings/notification/ZenModePrioritySettings;->gW(Lcom/android/settings/notification/ZenModePrioritySettings;)Landroid/app/NotificationManager$Policy;

    move-result-object v4

    iget v4, v4, Landroid/app/NotificationManager$Policy;->suppressedVisualEffects:I

    invoke-static {v1, v0, v2, v3, v4}, Lcom/android/settings/notification/ZenModePrioritySettings;->gY(Lcom/android/settings/notification/ZenModePrioritySettings;IIII)V

    return v5
.end method
