.class final Lcom/android/settings/notification/SilentModeSettingsBase$1;
.super Ljava/lang/Object;
.source "SilentModeSettingsBase.java"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private hN(Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;)Ljava/lang/String;
    .locals 3

    iget-object v1, p1, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;->cE:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v0, v1, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->isValidScheduleConditionId(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v1, Landroid/service/notification/ZenModeConfig$ZenRule;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, v1, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    invoke-static {v0}, Landroid/service/notification/ZenModeConfig;->isValidEventConditionId(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;

    check-cast p2, Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/notification/SilentModeSettingsBase$1;->hM(Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;)I

    move-result v0

    return v0
.end method

.method public hM(Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;)I
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/notification/SilentModeSettingsBase$1;->hN(Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/android/settings/notification/SilentModeSettingsBase$1;->hN(Lcom/android/settings/notification/SilentModeSettingsBase$ZenRuleInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
