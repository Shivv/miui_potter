.class public Lcom/android/settings/notification/AppNotificationSettings;
.super Lcom/android/settings/notification/NotificationSettingsBase;
.source "AppNotificationSettings.java"


# static fields
.field private static final A:Z

.field private static B:Ljava/lang/String;

.field private static C:Ljava/lang/String;


# instance fields
.field private D:Ljava/util/Comparator;

.field private E:Ljava/util/Comparator;

.field private F:Ljava/util/List;

.field private G:Ljava/util/List;

.field private H:Lcom/android/settings/widget/MiuiFooterPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "AppNotificationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/notification/AppNotificationSettings;->A:Z

    const-string/jumbo v0, "categories"

    sput-object v0, Lcom/android/settings/notification/AppNotificationSettings;->C:Ljava/lang/String;

    const-string/jumbo v0, "deleted"

    sput-object v0, Lcom/android/settings/notification/AppNotificationSettings;->B:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/NotificationSettingsBase;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->G:Ljava/util/List;

    new-instance v0, Lcom/android/settings/notification/AppNotificationSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/AppNotificationSettings$1;-><init>(Lcom/android/settings/notification/AppNotificationSettings;)V

    iput-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->D:Ljava/util/Comparator;

    new-instance v0, Lcom/android/settings/notification/AppNotificationSettings$2;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/AppNotificationSettings$2;-><init>(Lcom/android/settings/notification/AppNotificationSettings;)V

    iput-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->E:Ljava/util/Comparator;

    return-void
.end method

.method private K()V
    .locals 3

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/AppNotificationSettings;->fr(Landroid/util/ArrayMap;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getActivity()Landroid/app/Activity;

    return-void
.end method

.method private L(Landroid/app/NotificationChannel;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120bbb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120bc2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :sswitch_1
    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120bf5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :sswitch_2
    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120bbf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :sswitch_3
    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120bbd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :sswitch_4
    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120bb8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3e8 -> :sswitch_0
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
    .end sparse-switch
.end method

.method private M()V
    .locals 9

    const v2, 0x7f120bb2

    const/4 v8, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "AppNotificationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Notification channel group posted twice to settings - old size "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/notification/AppNotificationSettings;->G:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ", new size "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/notification/AppNotificationSettings;->F:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    sget-object v1, Lcom/android/settings/notification/AppNotificationSettings;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->G:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v2, 0x7f120b8f

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aF:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/AppNotificationSettings;->Q(Z)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->F:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationChannelGroup;

    new-instance v5, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/NotificationChannelGroup;->getId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->F:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v8, :cond_4

    const v1, 0x7f120bb3

    :goto_2
    invoke-virtual {v5, v1}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    sget-object v1, Lcom/android/settings/notification/AppNotificationSettings;->C:Ljava/lang/String;

    invoke-virtual {v5, v1}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    :goto_3
    invoke-virtual {v5, v8}, Landroid/preference/PreferenceCategory;->setOrderingAsAdded(Z)V

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->G:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Landroid/app/NotificationChannelGroup;->getChannels()Ljava/util/List;

    move-result-object v6

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->D:Ljava/util/Comparator;

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v1, v3

    :goto_4
    if-ge v1, v7, :cond_3

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationChannel;

    invoke-direct {p0, v5, v0}, Lcom/android/settings/notification/AppNotificationSettings;->N(Landroid/preference/PreferenceCategory;Landroid/app/NotificationChannel;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Landroid/app/NotificationChannelGroup;->getName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/NotificationChannelGroup;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget v2, v2, Lcom/android/settings/notification/NotificationBackend$AppRow;->aE:I

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/NotificationBackend;->bs(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_1

    new-instance v1, Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/settings/widget/MiuiFooterPreference;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {v1, v3}, Lcom/android/settings/widget/MiuiFooterPreference;->setSelectable(Z)V

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    const v5, 0x7f100010

    invoke-virtual {v2, v5, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/MiuiFooterPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {v0, v3}, Lcom/android/settings/widget/MiuiFooterPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    sget-object v1, Lcom/android/settings/notification/AppNotificationSettings;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiFooterPreference;->setKey(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/MiuiFooterPreference;->setOrder(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1
.end method

.method private N(Landroid/preference/PreferenceCategory;Landroid/app/NotificationChannel;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    new-instance v8, Lcom/android/settings/widget/MiuiMasterSwitchPreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ei:Lcom/android/settingslib/n;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aG:Z

    invoke-virtual {p0, v0, p2}, Lcom/android/settings/notification/AppNotificationSettings;->fs(ZLandroid/app/NotificationChannel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/settings/notification/AppNotificationSettings;->ft(Landroid/app/NotificationChannel;)Z

    move-result v0

    :goto_0
    invoke-virtual {v8, v0}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->ayO(Z)V

    invoke-virtual {p2}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/app/NotificationChannel;->getName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v8, v0}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->setChecked(Z)V

    invoke-direct {p0, p2}, Lcom/android/settings/notification/AppNotificationSettings;->L(Landroid/app/NotificationChannel;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "uid"

    iget v4, p0, Lcom/android/settings/notification/AppNotificationSettings;->eb:I

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "hideInfoButton"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "package"

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->ec:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "android.provider.extra.CHANNEL_ID"

    invoke-virtual {p2}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/android/settings/notification/ChannelNotificationSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getMetricsCategory()I

    move-result v7

    const v4, 0x7f120bb1

    move-object v5, v3

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bqs(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ILjava/lang/CharSequence;ZI)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/android/settings/widget/MiuiMasterSwitchPreference;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {p1, v8}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    return-void

    :cond_0
    move v0, v6

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1
.end method

.method static synthetic R(Lcom/android/settings/notification/AppNotificationSettings;)Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->E:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic S(Lcom/android/settings/notification/AppNotificationSettings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->F:Ljava/util/List;

    return-object v0
.end method

.method static synthetic T(Lcom/android/settings/notification/AppNotificationSettings;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/AppNotificationSettings;->F:Ljava/util/List;

    return-object p1
.end method

.method static synthetic U(Lcom/android/settings/notification/AppNotificationSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/AppNotificationSettings;->M()V

    return-void
.end method


# virtual methods
.method O()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "badge"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->ei:Lcom/android/settingslib/n;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eh:Landroid/app/NotificationChannel;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->aH:Z

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    new-instance v1, Lcom/android/settings/notification/AppNotificationSettings$4;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/AppNotificationSettings$4;-><init>(Lcom/android/settings/notification/AppNotificationSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {v1}, Landroid/app/NotificationChannel;->canShowBadge()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    goto :goto_0
.end method

.method protected P()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d01f2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a046d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/SwitchBar;

    iput-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ek:Lcom/android/settings/widget/SwitchBar;

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ek:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchBar;->show()V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ek:Lcom/android/settings/widget/SwitchBar;

    iget-object v2, p0, Lcom/android/settings/notification/AppNotificationSettings;->ei:Lcom/android/settingslib/n;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/SwitchBar;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ek:Lcom/android/settings/widget/SwitchBar;

    iget-object v2, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v2, v2, Lcom/android/settings/notification/NotificationBackend$AppRow;->aF:Z

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/SwitchBar;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ek:Lcom/android/settings/widget/SwitchBar;

    new-instance v2, Lcom/android/settings/notification/AppNotificationSettings$5;

    invoke-direct {v2, p0}, Lcom/android/settings/notification/AppNotificationSettings$5;-><init>(Lcom/android/settings/notification/AppNotificationSettings;)V

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/SwitchBar;->aBx(Lcom/android/settings/widget/t;)V

    new-instance v0, Lcom/android/settings/applications/MiuiLayoutPreference;

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/android/settings/applications/MiuiLayoutPreference;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    const/16 v1, -0x1f4

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/MiuiLayoutPreference;->setOrder(I)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    const-string/jumbo v1, "block"

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/MiuiLayoutPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aF:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->em:Lcom/android/settings/applications/MiuiLayoutPreference;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/AppNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    :cond_0
    const v0, 0x7f12014d

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/AppNotificationSettings;->fv(I)V

    return-void
.end method

.method protected Q(Z)V
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    xor-int/lit8 v3, p1, 0x1

    invoke-virtual {p0, v0, v3}, Lcom/android/settings/notification/AppNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    xor-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/notification/AppNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->en:Lcom/android/settings/widget/MiuiFooterPreference;

    invoke-virtual {p0, v0, p1}, Lcom/android/settings/notification/AppNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ej:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    xor-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/notification/AppNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    iget-boolean v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ee:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->el:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    xor-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/notification/AppNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    iget-object v2, p0, Lcom/android/settings/notification/AppNotificationSettings;->eo:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/AppNotificationSettings;->fw(I)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0, v4}, Lcom/android/settings/notification/AppNotificationSettings;->fw(I)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ep:Z

    :goto_1
    invoke-virtual {p0, v2, v0}, Lcom/android/settings/notification/AppNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eq:Lcom/android/settings/notification/MiuiRestrictedDropDownPreference;

    if-nez p1, :cond_2

    invoke-virtual {p0, v4}, Lcom/android/settings/notification/AppNotificationSettings;->fw(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->fx()Z

    move-result v1

    :cond_2
    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/AppNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->er:Landroid/preference/Preference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->er:Landroid/preference/Preference;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/notification/AppNotificationSettings;->fu(Landroid/preference/Preference;Z)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aG:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aF:Z

    xor-int/lit8 v0, v0, 0x1

    :cond_5
    return-void

    :cond_6
    const/4 v0, 0x1

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x48

    return v0
.end method

.method public onResume()V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/android/settings/notification/NotificationSettingsBase;->onResume()V

    iget v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eb:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ec:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ed:Landroid/content/pm/PackageInfo;

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, "AppNotificationSettings"

    const-string/jumbo v1, "Missing package or uid or packageinfo"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->finish()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->H:Lcom/android/settings/widget/MiuiFooterPreference;

    iput-boolean v3, p0, Lcom/android/settings/notification/AppNotificationSettings;->ee:Z

    :cond_2
    const v0, 0x7f150099

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/AppNotificationSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOrderingAsAdded(Z)V

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->P()V

    invoke-direct {p0}, Lcom/android/settings/notification/AppNotificationSettings;->K()V

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget v2, v2, Lcom/android/settings/notification/NotificationBackend$AppRow;->aE:I

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/notification/NotificationBackend;->bp(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ee:Z

    iget-boolean v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ee:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->ef:Lcom/android/settings/notification/NotificationBackend;

    iget-object v1, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v1, v1, Lcom/android/settings/notification/NotificationBackend$AppRow;->pkg:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget v2, v2, Lcom/android/settings/notification/NotificationBackend$AppRow;->aE:I

    const-string/jumbo v3, "miscellaneous"

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/notification/NotificationBackend;->bq(Ljava/lang/String;ILjava/lang/String;)Landroid/app/NotificationChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eh:Landroid/app/NotificationChannel;

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->fp()V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/notification/AppNotificationSettings;->eg:Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-boolean v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;->aF:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/AppNotificationSettings;->Q(Z)V

    return-void

    :cond_3
    const v0, 0x7f1500fd

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/AppNotificationSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/notification/AppNotificationSettings;->O()V

    new-instance v0, Lcom/android/settings/notification/AppNotificationSettings$3;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/AppNotificationSettings$3;-><init>(Lcom/android/settings/notification/AppNotificationSettings;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/notification/AppNotificationSettings$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
