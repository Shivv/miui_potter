.class Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;
.super Landroid/preference/Preference;
.source "ZenModeSettings.java"


# instance fields
.field final bh:Z

.field private final bi:Landroid/view/View$OnClickListener;

.field final bj:Ljava/lang/String;

.field final bk:Ljava/lang/CharSequence;

.field final synthetic bl:Lcom/android/settings/notification/ZenModeSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/notification/ZenModeSettings;Landroid/content/Context;Ljava/util/Map$Entry;)V
    .locals 10

    const/4 v2, 0x1

    const/4 v9, 0x0

    iput-object p1, p0, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->bl:Lcom/android/settings/notification/ZenModeSettings;

    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference$1;-><init>(Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;)V

    iput-object v0, p0, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->bi:Landroid/view/View$OnClickListener;

    invoke-interface {p3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AutomaticZenRule;

    invoke-virtual {v0}, Landroid/app/AutomaticZenRule;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->bk:Ljava/lang/CharSequence;

    invoke-interface {p3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->bj:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/app/AutomaticZenRule;->getConditionId()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/service/notification/ZenModeConfig;->isValidScheduleConditionId(Landroid/net/Uri;)Z

    move-result v4

    invoke-virtual {v0}, Landroid/app/AutomaticZenRule;->getConditionId()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/service/notification/ZenModeConfig;->isValidEventConditionId(Landroid/net/Uri;)Z

    move-result v1

    if-nez v4, :cond_0

    move v3, v1

    :goto_0
    :try_start_0
    invoke-static {p1}, Lcom/android/settings/notification/ZenModeSettings;->ck(Lcom/android/settings/notification/ZenModeSettings;)Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v0}, Landroid/app/AutomaticZenRule;->getOwner()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    new-instance v6, Lcom/android/settings/notification/ZenModeSettings$LoadIconTask;

    invoke-direct {v6, p1, p0}, Lcom/android/settings/notification/ZenModeSettings$LoadIconTask;-><init>(Lcom/android/settings/notification/ZenModeSettings;Landroid/preference/Preference;)V

    const/4 v7, 0x1

    new-array v7, v7, [Landroid/content/pm/ApplicationInfo;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-virtual {v6, v7}, Lcom/android/settings/notification/ZenModeSettings$LoadIconTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-static {p1}, Lcom/android/settings/notification/ZenModeSettings;->ck(Lcom/android/settings/notification/ZenModeSettings;)Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {p1, v0, v3, v5}, Lcom/android/settings/notification/ZenModeSettings;->co(Lcom/android/settings/notification/ZenModeSettings;Landroid/app/AutomaticZenRule;ZLjava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    iput-boolean v2, p0, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->bh:Z

    invoke-virtual {v0}, Landroid/app/AutomaticZenRule;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v9}, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->setPersistent(Z)V

    if-eqz v4, :cond_1

    const-string/jumbo v1, "android.settings.ZEN_MODE_SCHEDULE_RULE_SETTINGS"

    :goto_1
    invoke-static {p1}, Lcom/android/settings/notification/ZenModeSettings;->cl(Lcom/android/settings/notification/ZenModeSettings;)Lcom/android/settings/utils/i;

    move-result-object v4

    invoke-virtual {v0}, Landroid/app/AutomaticZenRule;->getOwner()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/android/settings/utils/i;->ayG(Landroid/content/ComponentName;)Landroid/content/pm/ServiceInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/notification/ZenModeSettings;->cm(Landroid/content/pm/ServiceInfo;)Landroid/content/ComponentName;

    move-result-object v0

    iget-object v4, p0, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->bj:Ljava/lang/String;

    invoke-static {p1, v1, v0, v4}, Lcom/android/settings/notification/ZenModeSettings;->cn(Lcom/android/settings/notification/ZenModeSettings;Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->setIntent(Landroid/content/Intent;)V

    if-nez v0, :cond_3

    move v0, v3

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->setSelectable(Z)V

    const v0, 0x7f0d028d

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->setWidgetLayoutResource(I)V

    return-void

    :cond_0
    move v3, v2

    goto :goto_0

    :catch_0
    move-exception v0

    const v0, 0x7f0801ed

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->setIcon(I)V

    iput-boolean v9, p0, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->bh:Z

    return-void

    :cond_1
    if-eqz v1, :cond_2

    const-string/jumbo v1, "android.settings.ZEN_MODE_EVENT_RULE_SETTINGS"

    goto :goto_1

    :cond_2
    const-string/jumbo v1, ""

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a012d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/notification/ZenModeSettings$ZenRulePreference;->bi:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
