.class public Lcom/android/settings/WifiCallingSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "WifiCallingSettings.java"

# interfaces
.implements Lcom/android/settings/widget/t;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final ccB:I

.field private ccC:Landroid/preference/ListPreference;

.field private ccD:Landroid/preference/ListPreference;

.field private ccE:Z

.field private ccF:Z

.field private ccG:Landroid/widget/TextView;

.field private ccH:Lcom/android/ims/ImsManager;

.field private ccI:Landroid/content/IntentFilter;

.field private ccJ:Landroid/content/BroadcastReceiver;

.field private ccK:I

.field private ccL:Landroid/widget/Switch;

.field private ccM:Lcom/android/settings/widget/SwitchBar;

.field private ccN:Landroid/preference/Preference;

.field private ccO:Z

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    iput-boolean v0, p0, Lcom/android/settings/WifiCallingSettings;->ccO:Z

    iput-boolean v1, p0, Lcom/android/settings/WifiCallingSettings;->ccE:Z

    iput-boolean v1, p0, Lcom/android/settings/WifiCallingSettings;->ccF:Z

    iput v0, p0, Lcom/android/settings/WifiCallingSettings;->ccB:I

    iput v0, p0, Lcom/android/settings/WifiCallingSettings;->ccK:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    new-instance v0, Lcom/android/settings/kG;

    invoke-direct {v0, p0}, Lcom/android/settings/kG;-><init>(Lcom/android/settings/WifiCallingSettings;)V

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/android/settings/kH;

    invoke-direct {v0, p0}, Lcom/android/settings/kH;-><init>(Lcom/android/settings/WifiCallingSettings;)V

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccJ:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private static bUB(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    const/4 v2, 0x0

    const-class v0, Landroid/telephony/CarrierConfigManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    if-nez v0, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v2

    :cond_1
    const-string/jumbo v1, "wfc_emergency_address_carrier_app_string"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    return-object v2

    :cond_2
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_3

    return-object v2

    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    return-object v1
.end method

.method public static bUC(Landroid/content/Context;I)I
    .locals 4

    const v0, 0x10406c2

    invoke-static {p0}, Lcom/android/ims/ImsManager;->isWfcEnabledByUser(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    packed-switch p1, :pswitch_data_0

    const-string/jumbo v1, "WifiCallingSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unexpected WFC mode value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    const v0, 0x10406a1

    goto :goto_0

    :pswitch_1
    const v0, 0x10406a0

    goto :goto_0

    :pswitch_2
    const v0, 0x10406a2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private bUE(Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "alertTitle"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string/jumbo v2, "alertMessage"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private bUF(Landroid/content/Context;ZII)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    invoke-virtual {p0, p1, p3}, Lcom/android/settings/WifiCallingSettings;->bUD(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    iget-object v2, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/WifiCallingSettings;->ccE:Z

    :goto_0
    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    if-eqz p2, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/WifiCallingSettings;->ccF:Z

    :goto_1
    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-static {p1}, Lcom/android/settings/WifiCallingSettings;->bUB(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    if-eqz p2, :cond_6

    iget-boolean v3, p0, Lcom/android/settings/WifiCallingSettings;->ccE:Z

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Lcom/android/settings/dc;->brY(Landroid/content/Context;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :goto_3
    iget-boolean v1, p0, Lcom/android/settings/WifiCallingSettings;->ccF:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :goto_4
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccN:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :goto_5
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccN:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_5

    :cond_6
    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccN:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_5
.end method

.method private bUG(Landroid/content/Context;Z)V
    .locals 4

    const-string/jumbo v0, "WifiCallingSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateWfcMode("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v0, p2}, Lcom/android/ims/ImsManager;->setWfcSettingForSlot(Z)V

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/ims/ImsManager;->getWfcModeForSlot(Z)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/ims/ImsManager;->getWfcModeForSlot(Z)I

    move-result v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/settings/WifiCallingSettings;->bUF(Landroid/content/Context;ZII)V

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getMetricsCategory()I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/android/settings/core/instrumentation/e;->ajU(Landroid/content/Context;II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getMetricsCategory()I

    move-result v2

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/core/instrumentation/e;->ajU(Landroid/content/Context;II)V

    goto :goto_0
.end method

.method static synthetic bUH(Lcom/android/settings/WifiCallingSettings;)Lcom/android/ims/ImsManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    return-object v0
.end method

.method static synthetic bUI(Lcom/android/settings/WifiCallingSettings;)Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccL:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic bUJ(Lcom/android/settings/WifiCallingSettings;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/WifiCallingSettings;->bUE(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public bUD(Landroid/content/Context;I)I
    .locals 4

    const v0, 0x10406c2

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v1}, Lcom/android/ims/ImsManager;->isWfcEnabledByUserForSlot()Z

    move-result v1

    if-eqz v1, :cond_0

    packed-switch p2, :pswitch_data_0

    const-string/jumbo v1, "WifiCallingSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unexpected WFC mode value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    const v0, 0x10406a1

    goto :goto_0

    :pswitch_1
    const v0, 0x10406a0

    goto :goto_0

    :pswitch_2
    const v0, 0x10406a2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public gG(Landroid/widget/Switch;Z)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "WifiCallingSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onSwitchChanged("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_0

    invoke-direct {p0, v0, v4}, Lcom/android/settings/WifiCallingSettings;->bUG(Landroid/content/Context;Z)V

    return-void

    :cond_0
    invoke-static {v0}, Lcom/android/settings/WifiCallingSettings;->bUB(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v0, "EXTRA_LAUNCH_CARRIER_APP"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1, v5}, Lcom/android/settings/WifiCallingSettings;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0, v5}, Lcom/android/settings/WifiCallingSettings;->bUG(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x69

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    invoke-virtual {v0}, Lcom/android/settings/bL;->bMF()Lcom/android/settings/widget/SwitchBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccM:Lcom/android/settings/widget/SwitchBar;

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccM:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchBar;->getSwitch()Lcom/android/settings/widget/ToggleSwitch;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccL:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccM:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchBar;->show()V

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccG:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccG:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/android/settings/WifiCallingSettings;->bWE(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccG:Landroid/widget/TextView;

    const v1, 0x7f1214dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-ne p1, v4, :cond_0

    const-string/jumbo v1, "WifiCallingSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "WFC emergency address activity result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    invoke-direct {p0, v0, v4}, Lcom/android/settings/WifiCallingSettings;->bUG(Landroid/content/Context;Z)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const v3, 0x7f030126

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15010e

    invoke-virtual {p0, v0}, Lcom/android/settings/WifiCallingSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "wifi_calling_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/WifiCallingSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "wifi_calling_roaming_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/WifiCallingSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "emergency_address_key"

    invoke-virtual {p0, v0}, Lcom/android/settings/WifiCallingSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccN:Landroid/preference/Preference;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccI:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccI:Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.android.ims.REGISTRATION_ERROR"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "carrier_config"

    invoke-virtual {p0, v0}, Lcom/android/settings/WifiCallingSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v2, "editable_wfc_mode_bool"

    invoke-virtual {v0, v2}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/settings/WifiCallingSettings;->ccE:Z

    const-string/jumbo v2, "editable_wfc_roaming_mode_bool"

    invoke-virtual {v0, v2}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/settings/WifiCallingSettings;->ccF:Z

    const-string/jumbo v2, "carrier_wfc_supports_wifi_only_bool"

    invoke-virtual {v0, v2, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    const v1, 0x7f030124

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries(I)V

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setEntryValues(I)V

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    const v1, 0x7f030123

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries(I)V

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setEntryValues(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "EXTRA_PHONE_ID"

    invoke-static {v1}, Lcom/android/settings/dc;->bYO(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/WifiCallingSettings;->ccK:I

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/WifiCallingSettings;->ccK:I

    invoke-static {v0, v1}, Lcom/android/ims/ImsManager;->getInstance(Landroid/content/Context;I)Lcom/android/ims/ImsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccM:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchBar;->aBz()V

    return-void
.end method

.method public onPause()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-boolean v0, p0, Lcom/android/settings/WifiCallingSettings;->ccO:Z

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/android/settings/WifiCallingSettings;->ccO:Z

    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Lcom/android/settings/WifiCallingSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/android/settings/WifiCallingSettings;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccM:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/SwitchBar;->aBy(Lcom/android/settings/widget/t;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccJ:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_2

    iget-object v2, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v2, v3}, Lcom/android/ims/ImsManager;->getWfcModeForSlot(Z)I

    move-result v2

    if-eq v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v2, v0, v3}, Lcom/android/ims/ImsManager;->setWfcModeForSlot(IZ)V

    iget-object v2, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/WifiCallingSettings;->bUD(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getMetricsCategory()I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/android/settings/core/instrumentation/e;->ajU(Landroid/content/Context;II)V

    :cond_0
    iget-boolean v1, p0, Lcom/android/settings/WifiCallingSettings;->ccF:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v1, v4}, Lcom/android/ims/ImsManager;->getWfcModeForSlot(Z)I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v1, v0, v4}, Lcom/android/ims/ImsManager;->setWfcModeForSlot(IZ)V

    :cond_1
    :goto_0
    return v4

    :cond_2
    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v1, v4}, Lcom/android/ims/ImsManager;->getWfcModeForSlot(Z)I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v1, v0, v4}, Lcom/android/ims/ImsManager;->setWfcModeForSlot(IZ)V

    iget-object v1, p0, Lcom/android/settings/WifiCallingSettings;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getMetricsCategory()I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/android/settings/core/instrumentation/e;->ajU(Landroid/content/Context;II)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->isWfcEnabledByUserForSlot()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->isNonTtyOrTtyOnVolteEnabledForSlot()Z

    move-result v0

    :goto_0
    iget-object v3, p0, Lcom/android/settings/WifiCallingSettings;->ccL:Landroid/widget/Switch;

    invoke-virtual {v3, v0}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v3, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v3, v1}, Lcom/android/ims/ImsManager;->getWfcModeForSlot(Z)I

    move-result v3

    iget-object v4, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v4, v7}, Lcom/android/ims/ImsManager;->getWfcModeForSlot(Z)I

    move-result v4

    iget-object v5, p0, Lcom/android/settings/WifiCallingSettings;->ccC:Landroid/preference/ListPreference;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/settings/WifiCallingSettings;->ccD:Landroid/preference/ListPreference;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-direct {p0, v2, v0, v3, v4}, Lcom/android/settings/WifiCallingSettings;->bUF(Landroid/content/Context;ZII)V

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccH:Lcom/android/ims/ImsManager;

    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->isWfcEnabledByPlatformForSlot()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Lcom/android/settings/WifiCallingSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lcom/android/settings/WifiCallingSettings;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v4, 0x20

    invoke-virtual {v0, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccM:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/SwitchBar;->aBx(Lcom/android/settings/widget/t;)V

    iput-boolean v7, p0, Lcom/android/settings/WifiCallingSettings;->ccO:Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/WifiCallingSettings;->ccJ:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/android/settings/WifiCallingSettings;->ccI:Landroid/content/IntentFilter;

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "alertShow"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/android/settings/WifiCallingSettings;->bUE(Landroid/content/Intent;)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method
