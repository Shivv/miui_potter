.class final Lcom/android/settings/dt;
.super Ljava/lang/Object;
.source "KeyguardAdvancedSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic chm:Lcom/android/settings/KeyguardAdvancedSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/KeyguardAdvancedSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dt;->chm:Lcom/android/settings/KeyguardAdvancedSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iget-object v0, p0, Lcom/android/settings/dt;->chm:Lcom/android/settings/KeyguardAdvancedSettings;

    invoke-static {v0}, Lcom/android/settings/KeyguardAdvancedSettings;->beZ(Lcom/android/settings/KeyguardAdvancedSettings;)I

    move-result v0

    if-ne v4, v0, :cond_0

    return v2

    :cond_0
    const v0, 0x7f120974

    if-eq v4, v0, :cond_2

    move v3, v1

    :goto_0
    const v0, 0x7f120978

    if-ne v4, v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v5, p0, Lcom/android/settings/dt;->chm:Lcom/android/settings/KeyguardAdvancedSettings;

    invoke-virtual {v5}, Lcom/android/settings/KeyguardAdvancedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v6, "lock_screen_allow_private_notifications"

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    invoke-static {v5, v6, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/dt;->chm:Lcom/android/settings/KeyguardAdvancedSettings;

    invoke-virtual {v0}, Lcom/android/settings/KeyguardAdvancedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v5, "lock_screen_show_notifications"

    if-eqz v3, :cond_1

    move v2, v1

    :cond_1
    invoke-static {v0, v5, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/dt;->chm:Lcom/android/settings/KeyguardAdvancedSettings;

    invoke-static {v0, v4}, Lcom/android/settings/KeyguardAdvancedSettings;->bfa(Lcom/android/settings/KeyguardAdvancedSettings;I)I

    return v1

    :cond_2
    move v3, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method
