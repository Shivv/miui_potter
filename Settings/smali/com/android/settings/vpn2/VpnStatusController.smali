.class public Lcom/android/settings/vpn2/VpnStatusController;
.super Lcom/android/settings/bA;
.source "VpnStatusController.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private YB:Lcom/android/settings/vpn2/VpnManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bA;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    const-string/jumbo v0, "VpnStatusController"

    iput-object v0, p0, Lcom/android/settings/vpn2/VpnStatusController;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/android/settings/vpn2/VpnManager;

    invoke-direct {v0, p1}, Lcom/android/settings/vpn2/VpnManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/vpn2/VpnStatusController;->YB:Lcom/android/settings/vpn2/VpnManager;

    return-void
.end method

.method private OO(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/vpn2/VpnStatusController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setVpnTitle, status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/vpn2/VpnStatusController;->bRZ:Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/android/settings/vpn2/VpnStatusController;->bRZ:Landroid/widget/TextView;

    const v1, 0x7f121463

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/vpn2/VpnStatusController;->bRZ:Landroid/widget/TextView;

    const v1, 0x7f121464

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/vpn2/VpnStatusController;->bRZ:Landroid/widget/TextView;

    const v1, 0x7f120477

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public pause()V
    .locals 0

    return-void
.end method

.method public wt()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/vpn2/VpnStatusController;->YB:Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {v0}, Lcom/android/settings/vpn2/VpnManager;->OL()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/VpnStatusController;->OO(I)V

    return-void
.end method

.method public wv()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/vpn2/VpnStatusController;->YB:Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {v0}, Lcom/android/settings/vpn2/VpnManager;->OL()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/VpnStatusController;->OO(I)V

    return-void
.end method
