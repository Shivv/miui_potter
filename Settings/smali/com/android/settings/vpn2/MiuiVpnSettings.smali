.class public Lcom/android/settings/vpn2/MiuiVpnSettings;
.super Lcom/android/settings/vpn2/VpnSettings;
.source "MiuiVpnSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private aal:Lcom/android/settings/vpn2/MiuiVpnSettings$ConfigureKeyGuardDialog;

.field private aam:Z

.field private aan:Z

.field private aao:Z

.field private aap:Lcom/android/settings/vpn2/AppPreference;

.field private aaq:Lcom/android/settings/vpn2/LegacyVpnPreference;

.field private aar:Ljava/lang/String;

.field private aas:Z

.field private aat:Landroid/preference/CheckBoxPreference;

.field private aau:Lcom/android/settings/vpn2/VpnManager;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/vpn2/VpnSettings;-><init>()V

    new-instance v0, Lcom/android/settings/vpn2/MiuiVpnSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/vpn2/MiuiVpnSettings$1;-><init>(Lcom/android/settings/vpn2/MiuiVpnSettings;)V

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private QC()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    aget-object v2, v0, v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YI:Landroid/net/IConnectivityManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/net/IConnectivityManager;->getLegacyVpnInfo(I)Lcom/android/internal/net/LegacyVpnInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aau:Lcom/android/settings/vpn2/VpnManager;

    iget-object v0, v0, Lcom/android/internal/net/LegacyVpnInfo;->key:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/android/settings/vpn2/VpnManager;->OK(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiuiVpnSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Error when disconnect vpn"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    instance-of v3, v0, Lcom/android/settings/vpn2/AppPreference;

    if-eqz v3, :cond_1

    check-cast v0, Lcom/android/settings/vpn2/AppPreference;

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QI(Lcom/android/settings/vpn2/AppPreference;)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method private QF()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aaq:Lcom/android/settings/vpn2/LegacyVpnPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aaq:Lcom/android/settings/vpn2/LegacyVpnPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/vpn2/LegacyVpnPreference;->setChecked(Z)V

    :cond_0
    iput-boolean v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aan:Z

    iput-boolean v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aao:Z

    iput-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aaq:Lcom/android/settings/vpn2/LegacyVpnPreference;

    iput-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aap:Lcom/android/settings/vpn2/AppPreference;

    return-void
.end method

.method private QI(Lcom/android/settings/vpn2/AppPreference;)V
    .locals 4

    :try_start_0
    invoke-virtual {p1}, Lcom/android/settings/vpn2/AppPreference;->Pi()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/Activity;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/settings/vpn2/AppPreference;->OR()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiuiVpnSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "VPN provider does not exist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/settings/vpn2/AppPreference;->OR()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private QJ()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aan:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aaq:Lcom/android/settings/vpn2/LegacyVpnPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aaq:Lcom/android/settings/vpn2/LegacyVpnPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/vpn2/LegacyVpnPreference;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method private QK(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aat:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aat:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private QL(Lcom/android/internal/net/VpnProfile;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QF()V

    iput-boolean v3, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aan:Z

    iput-boolean v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aao:Z

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/vpn2/LegacyVpnPreference;

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aaq:Lcom/android/settings/vpn2/LegacyVpnPreference;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aaq:Lcom/android/settings/vpn2/LegacyVpnPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aaq:Lcom/android/settings/vpn2/LegacyVpnPreference;

    invoke-virtual {v0, v3}, Lcom/android/settings/vpn2/LegacyVpnPreference;->setChecked(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/settings/vpn2/MiuiVpnUtils;->PG(Landroid/content/Context;Lcom/android/internal/net/VpnProfile;)V

    iget-object v0, p1, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private QM(Lcom/android/settings/vpn2/AppPreference;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QF()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aan:Z

    iput-boolean v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aao:Z

    iput-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aap:Lcom/android/settings/vpn2/AppPreference;

    return v1
.end method

.method private QN(Z)V
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/vpn2/MiuiVpnUtils;->PE(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/vpn2/LegacyVpnPreference;

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YI:Landroid/net/IConnectivityManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-interface {v1, v3}, Landroid/net/IConnectivityManager;->getLegacyVpnInfo(I)Lcom/android/internal/net/LegacyVpnInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz p1, :cond_2

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/vpn2/LegacyVpnPreference;->Po()Lcom/android/internal/net/VpnProfile;

    move-result-object v0

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/android/internal/net/LegacyVpnInfo;->key:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :catch_0
    move-exception v1

    const-string/jumbo v3, "MiuiVpnSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Error when updateVpnState"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    goto :goto_0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aau:Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/vpn2/VpnManager;->OJ(Lcom/android/internal/net/VpnProfile;Landroid/app/Activity;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return-void

    :catch_1
    move-exception v0

    const-string/jumbo v1, "MiuiVpnSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error when connect vpn"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aau:Lcom/android/settings/vpn2/VpnManager;

    iget-object v1, v1, Lcom/android/internal/net/LegacyVpnInfo;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/settings/vpn2/VpnManager;->OK(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic QO(Lcom/android/settings/vpn2/MiuiVpnSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aam:Z

    return p1
.end method

.method static synthetic QP(Lcom/android/settings/vpn2/MiuiVpnSettings;Lcom/android/settings/vpn2/AppPreference;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QI(Lcom/android/settings/vpn2/AppPreference;)V

    return-void
.end method

.method static synthetic QQ(Lcom/android/settings/vpn2/MiuiVpnSettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QN(Z)V

    return-void
.end method


# virtual methods
.method protected OX()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected OZ()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aar:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QJ()V

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aau:Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {v0}, Lcom/android/settings/vpn2/VpnManager;->OL()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QK(I)V

    return-void
.end method

.method protected QD(Lcom/android/internal/net/VpnProfile;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QE(Lcom/android/internal/net/VpnProfile;Z)V

    return-void
.end method

.method protected QE(Lcom/android/internal/net/VpnProfile;Z)V
    .locals 6

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "profile"

    invoke-virtual {p1}, Lcom/android/internal/net/VpnProfile;->encode()[B

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string/jumbo v0, "profile_key"

    iget-object v1, p1, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "profile_add"

    invoke-virtual {v4, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-class v0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x65

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/vpn2/MiuiVpnSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    return-void
.end method

.method protected QG()V
    .locals 7

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YF:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YK:Landroid/security/KeyStore;

    new-array v2, v2, [I

    invoke-static {v0, v2}, Lcom/android/settings/vpn2/MiuiVpnSettings;->OY(Landroid/security/KeyStore;[I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/net/VpnProfile;

    invoke-virtual {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->findOrCreatePreference(Lcom/android/internal/net/VpnProfile;)Lcom/android/settings/vpn2/LegacyVpnPreference;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/android/settings/vpn2/LegacyVpnPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    new-instance v4, Lcom/android/settings/vpn2/MiuiVpnSettings$3;

    invoke-direct {v4, p0}, Lcom/android/settings/vpn2/MiuiVpnSettings$3;-><init>(Lcom/android/settings/vpn2/MiuiVpnSettings;)V

    invoke-virtual {v3, v4}, Lcom/android/settings/vpn2/LegacyVpnPreference;->Pq(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    iget-object v5, v0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v4, "MiuiVpnSettings"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "show vpn config, key = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/settings/vpn2/MiuiVpnSettings;->OT(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/vpn2/AppVpnInfo;

    invoke-virtual {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->findOrCreatePreference(Lcom/android/settings/vpn2/AppVpnInfo;)Lcom/android/settings/vpn2/AppPreference;

    move-result-object v0

    new-instance v3, Lcom/android/settings/vpn2/MiuiVpnSettings$4;

    invoke-direct {v3, p0}, Lcom/android/settings/vpn2/MiuiVpnSettings$4;-><init>(Lcom/android/settings/vpn2/MiuiVpnSettings;)V

    invoke-virtual {v0, v3}, Lcom/android/settings/vpn2/AppPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "vpn_configure_category"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QF()V

    return-void
.end method

.method public QH(ILandroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QG()V

    :cond_0
    const/16 v0, 0x65

    if-ne p1, v0, :cond_3

    const-string/jumbo v0, "profile"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-string/jumbo v1, "profile_key"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "profile_delete"

    invoke-virtual {p2, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    if-nez v0, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    if-eqz v3, :cond_1

    invoke-static {v1, v0}, Lcom/android/internal/net/VpnProfile;->decode(Ljava/lang/String;[B)Lcom/android/internal/net/VpnProfile;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/vpn2/LegacyVpnPreference;

    if-eqz v2, :cond_4

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aau:Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {v2, v1}, Lcom/android/settings/vpn2/VpnManager;->OK(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YK:Landroid/security/KeyStore;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "VPN_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/security/KeyStore;->delete(Ljava/lang/String;)Z

    const-string/jumbo v0, "MiuiVpnSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "delete vpn config, key = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YK:Landroid/security/KeyStore;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VPN_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v3, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/android/internal/net/VpnProfile;->encode()[B

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v1, v2, v4, v5, v6}, Landroid/security/KeyStore;->put(Ljava/lang/String;[BII)Z

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aau:Lcom/android/settings/vpn2/VpnManager;

    iget-object v2, v3, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/settings/vpn2/VpnManager;->OK(Ljava/lang/String;)V

    sget v1, Lcom/android/settings/vpn2/AppPreference;->YC:I

    invoke-virtual {v0, v1}, Lcom/android/settings/vpn2/LegacyVpnPreference;->OI(I)V

    :goto_1
    iget-object v0, v3, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aar:Ljava/lang/String;

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v3}, Lcom/android/settings/vpn2/MiuiVpnSettings;->findOrCreatePreference(Lcom/android/internal/net/VpnProfile;)Lcom/android/settings/vpn2/LegacyVpnPreference;

    move-result-object v0

    new-instance v1, Lcom/android/settings/vpn2/MiuiVpnSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/vpn2/MiuiVpnSettings$2;-><init>(Lcom/android/settings/vpn2/MiuiVpnSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/vpn2/LegacyVpnPreference;->Pq(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    const-string/jumbo v0, "MiuiVpnSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "add vpn config, key = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v3, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/vpn2/VpnSettings;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QH(ILandroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/vpn2/VpnSettings;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->mUserManager:Landroid/os/UserManager;

    new-instance v0, Lcom/android/settings/vpn2/MiuiVpnSettings$ConfigureKeyGuardDialog;

    invoke-direct {v0, p0, v2}, Lcom/android/settings/vpn2/MiuiVpnSettings$ConfigureKeyGuardDialog;-><init>(Lcom/android/settings/vpn2/MiuiVpnSettings;Lcom/android/settings/vpn2/MiuiVpnSettings$ConfigureKeyGuardDialog;)V

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aal:Lcom/android/settings/vpn2/MiuiVpnSettings$ConfigureKeyGuardDialog;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->mUserManager:Landroid/os/UserManager;

    const-string/jumbo v1, "no_config_vpn"

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aas:Z

    new-instance v0, Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Landroid/preference/PreferenceScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->setHasOptionsMenu(Z)V

    return-void

    :cond_0
    invoke-virtual {p0, v3}, Lcom/android/settings/vpn2/MiuiVpnSettings;->setHasOptionsMenu(Z)V

    new-instance v0, Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/vpn2/VpnManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aau:Lcom/android/settings/vpn2/VpnManager;

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    const-string/jumbo v0, "vpn_enable"

    invoke-virtual {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aat:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "vpn_configure_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.net.vpn.SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "vpn_password_enable"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const v0, 0x7f121441

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    sget v1, Lmiui/R$drawable;->action_button_new_light:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/vpn2/VpnSettings;->onDestroy()V

    iget-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aas:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aal:Lcom/android/settings/vpn2/MiuiVpnSettings$ConfigureKeyGuardDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/vpn2/MiuiVpnSettings$ConfigureKeyGuardDialog;->QR(Z)V

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    const/4 v4, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-ne v0, v4, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/android/internal/net/VpnProfile;

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/android/internal/net/VpnProfile;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v4}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QE(Lcom/android/internal/net/VpnProfile;Z)V

    return v4

    :cond_1
    invoke-super {p0, p1}, Lcom/android/settings/vpn2/VpnSettings;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1

    instance-of v0, p1, Lcom/android/settings/vpn2/LegacyVpnPreference;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/settings/vpn2/LegacyVpnPreference;

    invoke-virtual {v0}, Lcom/android/settings/vpn2/LegacyVpnPreference;->Po()Lcom/android/internal/net/VpnProfile;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QL(Lcom/android/internal/net/VpnProfile;)Z

    :cond_0
    instance-of v0, p1, Lcom/android/settings/vpn2/AppPreference;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/android/settings/vpn2/AppPreference;

    invoke-direct {p0, p1}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QM(Lcom/android/settings/vpn2/AppPreference;)Z

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "vpn_enable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aan:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QN(Z)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/vpn2/VpnSettings;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aao:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aap:Lcom/android/settings/vpn2/AppPreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aap:Lcom/android/settings/vpn2/AppPreference;

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QI(Lcom/android/settings/vpn2/AppPreference;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f12145f

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QC()V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 3

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/android/settings/vpn2/VpnSettings;->onResume()V

    iget-boolean v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aas:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    const v1, 0x7f121477

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "saved_bundle"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :cond_3
    if-eqz v0, :cond_4

    const-string/jumbo v2, "show_dialog"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aam:Z

    :cond_4
    iget-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aal:Lcom/android/settings/vpn2/MiuiVpnSettings$ConfigureKeyGuardDialog;

    iget-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aam:Z

    if-nez v0, :cond_7

    xor-int/lit8 v0, v1, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings$ConfigureKeyGuardDialog;->QR(Z)V

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QG()V

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YL:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aar:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aar:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/vpn2/LegacyVpnPreference;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/android/settings/vpn2/LegacyVpnPreference;->Po()Lcom/android/internal/net/VpnProfile;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->QL(Lcom/android/internal/net/VpnProfile;)Z

    :cond_5
    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_6
    :goto_2
    return-void

    :cond_7
    const/4 v0, 0x1

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/vpn2/MiuiVpnUtils;->PE(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_9
    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "vpn_configure_category"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->YQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/vpn2/VpnSettings;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "show_dialog"

    iget-boolean v1, p0, Lcom/android/settings/vpn2/MiuiVpnSettings;->aam:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "saved_bundle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method
