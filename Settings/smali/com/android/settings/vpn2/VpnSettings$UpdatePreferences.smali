.class Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;
.super Ljava/lang/Object;
.source "VpnSettings.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private YR:Ljava/util/Set;

.field private YS:Ljava/util/Set;

.field private YT:Ljava/util/Map;

.field private YU:Ljava/lang/String;

.field private final YV:Lcom/android/settings/vpn2/VpnSettings;

.field private YW:Ljava/util/List;

.field private YX:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/android/settings/vpn2/VpnSettings;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YX:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YW:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YT:Ljava/util/Map;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YS:Ljava/util/Set;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YR:Ljava/util/Set;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YU:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YV:Lcom/android/settings/vpn2/VpnSettings;

    return-void
.end method


# virtual methods
.method public final Pb(Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YW:Ljava/util/List;

    iput-object p2, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YS:Ljava/util/Set;

    iput-object p3, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YR:Ljava/util/Set;

    return-object p0
.end method

.method public final Pc(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YX:Ljava/util/List;

    iput-object p2, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YT:Ljava/util/Map;

    iput-object p3, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YU:Ljava/lang/String;

    return-object p0
.end method

.method public run()V
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YV:Lcom/android/settings/vpn2/VpnSettings;

    invoke-virtual {v0}, Lcom/android/settings/vpn2/VpnSettings;->canAddPreferences()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YV:Lcom/android/settings/vpn2/VpnSettings;

    invoke-virtual {v0}, Lcom/android/settings/vpn2/VpnSettings;->OX()V

    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    iget-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YX:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/net/VpnProfile;

    iget-object v1, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YV:Lcom/android/settings/vpn2/VpnSettings;

    invoke-virtual {v1, v0}, Lcom/android/settings/vpn2/VpnSettings;->findOrCreatePreference(Lcom/android/internal/net/VpnProfile;)Lcom/android/settings/vpn2/LegacyVpnPreference;

    move-result-object v5

    iget-object v1, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YT:Ljava/util/Map;

    iget-object v6, v0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-interface {v1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YT:Ljava/util/Map;

    iget-object v6, v0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/net/LegacyVpnInfo;

    iget v1, v1, Lcom/android/internal/net/LegacyVpnInfo;->state:I

    invoke-virtual {v5, v1}, Lcom/android/settings/vpn2/LegacyVpnPreference;->OI(I)V

    :goto_1
    iget-object v1, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YU:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YU:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_2
    invoke-virtual {v5, v0}, Lcom/android/settings/vpn2/LegacyVpnPreference;->OH(Z)V

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    sget v1, Lcom/android/settings/vpn2/LegacyVpnPreference;->STATE_NONE:I

    invoke-virtual {v5, v1}, Lcom/android/settings/vpn2/LegacyVpnPreference;->OI(I)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/net/LegacyVpnInfo;

    new-instance v4, Lcom/android/internal/net/VpnProfile;

    iget-object v5, v0, Lcom/android/internal/net/LegacyVpnInfo;->key:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/android/internal/net/VpnProfile;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YV:Lcom/android/settings/vpn2/VpnSettings;

    invoke-virtual {v5, v4}, Lcom/android/settings/vpn2/VpnSettings;->findOrCreatePreference(Lcom/android/internal/net/VpnProfile;)Lcom/android/settings/vpn2/LegacyVpnPreference;

    move-result-object v4

    iget v5, v0, Lcom/android/internal/net/LegacyVpnInfo;->state:I

    invoke-virtual {v4, v5}, Lcom/android/settings/vpn2/LegacyVpnPreference;->OI(I)V

    iget-object v5, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YU:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YU:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/internal/net/LegacyVpnInfo;->key:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_4
    invoke-virtual {v4, v0}, Lcom/android/settings/vpn2/LegacyVpnPreference;->OH(Z)V

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YW:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/vpn2/AppVpnInfo;

    iget-object v2, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YV:Lcom/android/settings/vpn2/VpnSettings;

    invoke-virtual {v2, v0}, Lcom/android/settings/vpn2/VpnSettings;->findOrCreatePreference(Lcom/android/settings/vpn2/AppVpnInfo;)Lcom/android/settings/vpn2/AppPreference;

    move-result-object v2

    iget-object v4, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YS:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Lcom/android/settings/vpn2/AppPreference;->Pg(I)V

    :goto_6
    iget-object v4, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YR:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/android/settings/vpn2/AppPreference;->Ph(Z)V

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_6
    sget v4, Lcom/android/settings/vpn2/AppPreference;->YC:I

    invoke-virtual {v2, v4}, Lcom/android/settings/vpn2/AppPreference;->Pg(I)V

    goto :goto_6

    :cond_7
    iget-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YV:Lcom/android/settings/vpn2/VpnSettings;

    invoke-virtual {v0, v3}, Lcom/android/settings/vpn2/VpnSettings;->setShownPreferences(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/android/settings/vpn2/VpnSettings$UpdatePreferences;->YV:Lcom/android/settings/vpn2/VpnSettings;

    invoke-virtual {v0}, Lcom/android/settings/vpn2/VpnSettings;->OZ()V

    return-void
.end method
