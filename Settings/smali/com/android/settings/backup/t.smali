.class public final Lcom/android/settings/backup/t;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SyncRootProtos.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# instance fields
.field private bitField0_:I

.field private setting_:Lcom/android/settings/backup/SettingProtos$Settings;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHr()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/t;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-direct {p0}, Lcom/android/settings/backup/t;->aIf()V

    return-void
.end method

.method private aIe()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/android/settings/backup/t;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private aIf()V
    .locals 0

    return-void
.end method

.method static synthetic aIi()Lcom/android/settings/backup/t;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/t;->create()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aIj(Lcom/android/settings/backup/t;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/backup/t;->aIe()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/android/settings/backup/t;
    .locals 1

    new-instance v0, Lcom/android/settings/backup/t;

    invoke-direct {v0}, Lcom/android/settings/backup/t;-><init>()V

    return-object v0
.end method


# virtual methods
.method public aHW()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/t;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    return-object v0
.end method

.method public aHY()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/settings/backup/t;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aId(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/t;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/android/settings/backup/t;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    iget v0, p0, Lcom/android/settings/backup/t;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/t;->bitField0_:I

    return-object p0
.end method

.method public aIg(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/t;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHX()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHY()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHW()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/t;->aIh(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/t;

    :cond_1
    return-object p0
.end method

.method public aIh(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/t;
    .locals 2

    iget v0, p0, Lcom/android/settings/backup/t;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/t;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHr()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/t;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-static {v0}, Lcom/android/settings/backup/SettingProtos$Settings;->aHx(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/p;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/backup/p;->aHL(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/backup/p;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/t;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    :goto_0
    iget v0, p0, Lcom/android/settings/backup/t;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/t;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/android/settings/backup/t;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    goto :goto_0
.end method

.method public build()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/android/settings/backup/t;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->build()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 4

    const/4 v0, 0x1

    new-instance v2, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;-><init>(Lcom/android/settings/backup/t;Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)V

    iget v3, p0, Lcom/android/settings/backup/t;->bitField0_:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/backup/t;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-static {v2, v1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aIc(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-static {v2, v0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aIb(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;I)I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/android/settings/backup/t;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHr()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/t;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    iget v0, p0, Lcom/android/settings/backup/t;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/settings/backup/t;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->clear()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->clear()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/android/settings/backup/t;
    .locals 2

    invoke-static {}, Lcom/android/settings/backup/t;->create()Lcom/android/settings/backup/t;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/t;->aIg(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->clone()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->clone()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->clone()Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHX()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/t;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settings/backup/t;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHw()Lcom/android/settings/backup/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->aHY()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/backup/t;->aHW()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/p;->aHL(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/p;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/android/settings/backup/p;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/t;->aId(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/t;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3a -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/t;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/t;->aIg(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/t;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/t;

    move-result-object v0

    return-object v0
.end method
