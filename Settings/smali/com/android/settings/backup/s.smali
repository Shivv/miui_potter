.class Lcom/android/settings/backup/s;
.super Ljava/lang/Object;
.source "SettingsAgent.java"


# instance fields
.field private aUH:Ljava/io/File;

.field private aUI:Ljava/util/HashMap;

.field private aUJ:Lcom/android/settings/backup/c;

.field final synthetic aUK:Lcom/android/settings/backup/q;


# direct methods
.method private constructor <init>(Lcom/android/settings/backup/q;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/backup/q;Lcom/android/settings/backup/s;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/s;-><init>(Lcom/android/settings/backup/q;)V

    return-void
.end method


# virtual methods
.method public aHP()I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    iget-object v1, p0, Lcom/android/settings/backup/s;->aUI:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/c;->aFt(Ljava/util/HashMap;)V

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v0}, Lcom/android/settings/backup/c;->aFs()V

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v0}, Lcom/android/settings/backup/c;->aFr()V

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiui/os/FileUtils;->rm(Ljava/lang/String;)Z

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public aHQ(Landroid/os/ParcelFileDescriptor;)I
    .locals 12

    const/4 v6, 0x0

    const/4 v2, 0x0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v1, v1, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {v1}, Lmiui/app/backup/FullBackupAgent;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v3, "_tmp_attach"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v0, Lcom/android/settings/backup/c;

    iget-object v1, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v1, v1, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-direct {v0, v1}, Lcom/android/settings/backup/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHw()Lcom/android/settings/backup/p;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v0}, Lcom/android/settings/backup/c;->aFq()Ljava/util/Vector;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v3}, Lcom/android/settings/backup/c;->aFp()Ljava/util/Vector;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v4}, Lcom/android/settings/backup/c;->aFm()Ljava/util/Vector;

    move-result-object v4

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :try_start_0
    iget-object v7, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v7, v0}, Lcom/android/settings/backup/c;->aFo(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SystemSetting;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Lcom/android/settings/backup/p;->aHG(Lcom/android/settings/backup/SettingProtos$SystemSetting;)Lcom/android/settings/backup/p;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v7, "Backup:SettingsAgent"

    const-string/jumbo v8, "Cannot load system setting "

    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :try_start_1
    iget-object v5, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v5, v0}, Lcom/android/settings/backup/c;->aFn(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1, v0}, Lcom/android/settings/backup/p;->aHF(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/p;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string/jumbo v5, "Backup:SettingsAgent"

    const-string/jumbo v7, "Cannot load secure setting "

    invoke-static {v5, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_4
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/backup/SettingProtos$LockSetting;

    invoke-virtual {v1, v0}, Lcom/android/settings/backup/p;->aHE(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/p;

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHU()Lcom/android/settings/backup/t;

    move-result-object v0

    invoke-virtual {v1}, Lcom/android/settings/backup/p;->build()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/t;->aId(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/t;

    :try_start_2
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v0}, Lcom/android/settings/backup/t;->build()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->writeTo(Ljava/io/OutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_6
    sget-object v1, Lcom/android/settings/backup/d;->aTS:[Lcom/android/settings/backup/j;

    array-length v2, v1

    move v0, v6

    :goto_3
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v4, v4, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    iget-object v3, v3, Lcom/android/settings/backup/j;->aUl:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lmiui/app/backup/FullBackupAgent;->addAttachedFile(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_7
    throw v0

    :cond_8
    iget-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/c;->aFl(I)Ljava/io/File;

    move-result-object v7

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/c;->aFl(I)Ljava/io/File;

    move-result-object v8

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/c;->aFl(I)Ljava/io/File;

    move-result-object v9

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/c;->aFl(I)Ljava/io/File;

    move-result-object v10

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/c;->aFl(I)Ljava/io/File;

    move-result-object v11

    new-instance v0, Lcom/android/settings/backup/b;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/backup/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    const-string/jumbo v3, "settings_descript.xml"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/b;->aFb(Ljava/io/File;)V

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v0, v0, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/backup/FullBackupAgent;->addAttachedFile(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v0, v0, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/backup/FullBackupAgent;->addAttachedFile(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v0, v0, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/backup/FullBackupAgent;->addAttachedFile(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v0, v0, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {v10}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/backup/FullBackupAgent;->addAttachedFile(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v0, v0, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {v11}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/backup/FullBackupAgent;->addAttachedFile(Ljava/lang/String;)V

    return v6

    :catchall_1
    move-exception v0

    goto/16 :goto_4
.end method

.method public aHR(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)I
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/16 v2, 0x2000

    :try_start_2
    new-array v2, v2, [B

    :goto_0
    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_d
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    :try_start_3
    const-string/jumbo v2, "Backup:SettingsAgent"

    const-string/jumbo v4, "IllegalArgumentException"

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v3, :cond_0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    :cond_1
    :goto_3
    return v6

    :cond_2
    :try_start_6
    iget-object v2, p0, Lcom/android/settings/backup/s;->aUI:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_d
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v3, :cond_3

    :try_start_7
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :cond_3
    :goto_4
    if-eqz v1, :cond_1

    :try_start_8
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    const-string/jumbo v1, "Backup:SettingsAgent"

    const-string/jumbo v2, "IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catch_2
    move-exception v0

    const-string/jumbo v2, "Backup:SettingsAgent"

    const-string/jumbo v3, "IOException"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :catch_3
    move-exception v0

    move-object v1, v2

    :goto_5
    :try_start_9
    const-string/jumbo v3, "Backup:SettingsAgent"

    const-string/jumbo v4, "IOException"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    if-eqz v2, :cond_4

    :try_start_a
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    :cond_4
    :goto_6
    if-eqz v1, :cond_1

    :try_start_b
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_3

    :catch_4
    move-exception v0

    const-string/jumbo v1, "Backup:SettingsAgent"

    const-string/jumbo v2, "IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catch_5
    move-exception v0

    const-string/jumbo v2, "Backup:SettingsAgent"

    const-string/jumbo v3, "IOException"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    :catch_6
    move-exception v0

    const-string/jumbo v2, "Backup:SettingsAgent"

    const-string/jumbo v3, "IOException"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_7
    move-exception v0

    const-string/jumbo v1, "Backup:SettingsAgent"

    const-string/jumbo v2, "IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    :goto_7
    if-eqz v3, :cond_5

    :try_start_c
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    :cond_5
    :goto_8
    if-eqz v1, :cond_6

    :try_start_d
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    :cond_6
    :goto_9
    throw v0

    :catch_8
    move-exception v2

    const-string/jumbo v3, "Backup:SettingsAgent"

    const-string/jumbo v4, "IOException"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    :catch_9
    move-exception v1

    const-string/jumbo v2, "Backup:SettingsAgent"

    const-string/jumbo v3, "IOException"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_7

    :catchall_2
    move-exception v0

    goto :goto_7

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_7

    :catch_a
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto/16 :goto_1

    :catch_b
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    :catch_c
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_5

    :catch_d
    move-exception v0

    move-object v2, v3

    goto :goto_5
.end method

.method public aHS(Landroid/os/ParcelFileDescriptor;)I
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Lcom/android/settings/backup/c;

    iget-object v1, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v1, v1, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-direct {v0, v1}, Lcom/android/settings/backup/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {v1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHV(Ljava/io/InputStream;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->aHW()Lcom/android/settings/backup/SettingProtos$Settings;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    :cond_0
    if-nez v2, :cond_2

    const/4 v0, 0x6

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_1
    return v0

    :cond_2
    :try_start_2
    invoke-virtual {v2}, Lcom/android/settings/backup/SettingProtos$Settings;->aHu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/backup/SettingProtos$SystemSetting;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v4, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v4, v0}, Lcom/android/settings/backup/c;->aFf(Lcom/android/settings/backup/SettingProtos$SystemSetting;)Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    const-string/jumbo v4, "Backup:SettingsAgent"

    const-string/jumbo v5, "Cannot add system setting "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_3
    throw v0

    :cond_4
    :try_start_5
    invoke-virtual {v2}, Lcom/android/settings/backup/SettingProtos$Settings;->aHt()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/backup/SettingProtos$SecureSetting;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v4, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v4, v0}, Lcom/android/settings/backup/c;->aFe(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Landroid/net/Uri;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_7
    const-string/jumbo v4, "Backup:SettingsAgent"

    const-string/jumbo v5, "Cannot add secure setting "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_5
    invoke-virtual {v2}, Lcom/android/settings/backup/SettingProtos$Settings;->aHs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/backup/SettingProtos$LockSetting;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    iget-object v3, p0, Lcom/android/settings/backup/s;->aUJ:Lcom/android/settings/backup/c;

    invoke-virtual {v3, v0}, Lcom/android/settings/backup/c;->aFd(Lcom/android/settings/backup/SettingProtos$LockSetting;)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    :catch_2
    move-exception v0

    :try_start_9
    const-string/jumbo v3, "Backup:SettingsAgent"

    const-string/jumbo v4, "Cannot add lock setting "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/backup/s;->aUI:Ljava/util/HashMap;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/settings/backup/s;->aUK:Lcom/android/settings/backup/q;

    iget-object v1, v1, Lcom/android/settings/backup/q;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {v1}, Lmiui/app/backup/FullBackupAgent;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "_tmp_attach"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_8
    const/4 v0, 0x0

    return v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public aHT(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;)I
    .locals 7

    new-instance v6, Ljava/io/File;

    iget-object v0, p0, Lcom/android/settings/backup/s;->aUH:Ljava/io/File;

    const-string/jumbo v1, "settings_descript.xml"

    invoke-direct {v6, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, Lmiui/app/backup/BackupManager;->DOMAIN_ATTACH:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    move-object v0, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lmiui/app/backup/FullBackupProxy;->backupToTar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;)I

    :cond_0
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    const/4 v0, 0x0

    return v0
.end method
