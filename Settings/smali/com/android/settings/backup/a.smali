.class public Lcom/android/settings/backup/a;
.super Lcom/android/settings/backup/e;
.source "WifiAgent.java"


# direct methods
.method public constructor <init>(Lmiui/app/backup/FullBackupAgent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/e;-><init>(Lmiui/app/backup/FullBackupAgent;)V

    return-void
.end method


# virtual methods
.method public aES(Lmiui/app/backup/BackupMeta;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aET(Landroid/os/ParcelFileDescriptor;)I
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {}, Lcom/android/settings/wifi/l;->getInstance()Lcom/android/settings/wifi/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/wifi/l;->acd()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    :try_start_2
    const-string/jumbo v2, "Backup:WifiAgent"

    const-string/jumbo v3, "IOException"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_0
    :goto_2
    return v4

    :cond_1
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_2
    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string/jumbo v1, "Backup:WifiAgent"

    const-string/jumbo v2, "IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_2
    move-exception v0

    const-string/jumbo v1, "Backup:WifiAgent"

    const-string/jumbo v2, "IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_3

    :try_start_6
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :cond_3
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    const-string/jumbo v2, "Backup:WifiAgent"

    const-string/jumbo v3, "IOException"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public aEU()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aEV(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aEW(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;)I
    .locals 4

    invoke-static {}, Lcom/android/settings/wifi/l;->getInstance()Lcom/android/settings/wifi/l;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/backup/a;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/wifi/l;->ace(Landroid/content/Context;Ljava/io/FileDescriptor;)V

    iget-object v0, p0, Lcom/android/settings/backup/a;->aTU:Lmiui/app/backup/FullBackupAgent;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Lmiui/app/backup/FullBackupAgent;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/settings/wifi/l;->getInstance()Lcom/android/settings/wifi/l;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/backup/a;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/l;->acf(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/backup/a;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-static {v1}, Lcom/android/settings/wifi/a;->getInstance(Landroid/content/Context;)Lcom/android/settings/wifi/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/wifi/a;->aau(Ljava/util/Map;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/backup/a;->aTU:Lmiui/app/backup/FullBackupAgent;

    const-class v3, Lcom/android/settings/wifi/MiuiWifiService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "miui.intent.action.RESTORE_WIFI_CONFIGURATIONS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "wifiConfiguration"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/backup/a;->aTU:Lmiui/app/backup/FullBackupAgent;

    invoke-virtual {v0, v1}, Lmiui/app/backup/FullBackupAgent;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public aEX(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
