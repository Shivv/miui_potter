.class final Lcom/android/settings/backup/z;
.super Ljava/lang/Object;
.source "ToggleBackupSettingFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic aUQ:Lcom/android/settings/backup/ToggleBackupSettingFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/backup/ToggleBackupSettingFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/z;->aUQ:Lcom/android/settings/backup/ToggleBackupSettingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/z;->aUQ:Lcom/android/settings/backup/ToggleBackupSettingFragment;

    invoke-static {v0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aFR(Lcom/android/settings/backup/ToggleBackupSettingFragment;)V

    return v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/backup/z;->aUQ:Lcom/android/settings/backup/ToggleBackupSettingFragment;

    invoke-static {v0, v1}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aFQ(Lcom/android/settings/backup/ToggleBackupSettingFragment;Z)V

    return v1
.end method
