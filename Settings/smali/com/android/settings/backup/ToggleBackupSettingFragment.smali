.class public Lcom/android/settings/backup/ToggleBackupSettingFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "ToggleBackupSettingFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private aUu:Landroid/app/backup/IBackupManager;

.field private aUv:Landroid/app/Dialog;

.field private aUw:Landroid/preference/Preference;

.field protected aUx:Lcom/android/settings/widget/TogglePreference;

.field private aUy:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUy:Z

    return-void
.end method

.method private aFO(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUu:Landroid/app/backup/IBackupManager;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUu:Landroid/app/backup/IBackupManager;

    invoke-interface {v0, p1}, Landroid/app/backup/IBackupManager;->setBackupEnabled(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "ToggleBackupSettingFragment"

    const-string/jumbo v2, "Error communicating with BackupManager"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method private aFP()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "user_full_data_backup_aware"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1207aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUy:Z

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1201e8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUv:Landroid/app/Dialog;

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1201e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic aFQ(Lcom/android/settings/backup/ToggleBackupSettingFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aFO(Z)V

    return-void
.end method

.method static synthetic aFR(Lcom/android/settings/backup/ToggleBackupSettingFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aFP()V

    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x51

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    new-instance v1, Lcom/android/settings/backup/z;

    invoke-direct {v1, p0}, Lcom/android/settings/backup/z;-><init>(Lcom/android/settings/backup/ToggleBackupSettingFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/TogglePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iput-boolean v1, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUy:Z

    invoke-direct {p0, v1}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aFO(Z)V

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/TogglePreference;->setChecked(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    iput-boolean v1, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUy:Z

    invoke-direct {p0, v2}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aFO(Z)V

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/TogglePreference;->setChecked(Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "backup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUu:Landroid/app/backup/IBackupManager;

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    new-instance v1, Lcom/android/settings/backup/y;

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/settings/backup/y;-><init>(Lcom/android/settings/backup/ToggleBackupSettingFragment;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUw:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUw:Landroid/preference/Preference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setPersistent(Z)V

    iget-object v1, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUw:Landroid/preference/Preference;

    const v2, 0x7f0d0243

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setLayoutResource(I)V

    iget-object v1, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUw:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method public onDestroyView()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/TogglePreference;->aCY(Lcom/android/settings/widget/M;)V

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUy:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aFO(Z)V

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/TogglePreference;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUv:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUv:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUv:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iput-object v1, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUv:Landroid/app/Dialog;

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStop()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    new-instance v0, Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/settings/widget/TogglePreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    const v2, 0x7f1207d0

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/TogglePreference;->setTitle(I)V

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/TogglePreference;->setOrder(I)V

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "user_full_data_backup_aware"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUw:Landroid/preference/Preference;

    const v2, 0x7f1207a9

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSummary(I)V

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUu:Landroid/app/backup/IBackupManager;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v2, v0}, Lcom/android/settings/widget/TogglePreference;->setChecked(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-virtual {p0}, Lcom/android/settings/backup/ToggleBackupSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f1201e5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUw:Landroid/preference/Preference;

    const v2, 0x7f1201e4

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUu:Landroid/app/backup/IBackupManager;

    invoke-interface {v0}, Landroid/app/backup/IBackupManager;->isBackupEnabled()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/android/settings/backup/ToggleBackupSettingFragment;->aUx:Lcom/android/settings/widget/TogglePreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/TogglePreference;->setChecked(Z)V

    goto :goto_2
.end method
