.class public final Lcom/android/settings/backup/SettingProtos$LockSetting;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SettingProtos.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# static fields
.field private static final aUB:Lcom/android/settings/backup/SettingProtos$LockSetting;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private guid_:Ljava/lang/Object;

.field private luid_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/Object;

.field private value_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/settings/backup/SettingProtos$LockSetting;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/settings/backup/SettingProtos$LockSetting;-><init>(Z)V

    sput-object v0, Lcom/android/settings/backup/SettingProtos$LockSetting;->aUB:Lcom/android/settings/backup/SettingProtos$LockSetting;

    sget-object v0, Lcom/android/settings/backup/SettingProtos$LockSetting;->aUB:Lcom/android/settings/backup/SettingProtos$LockSetting;

    invoke-direct {v0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHe()V

    return-void
.end method

.method private constructor <init>(Lcom/android/settings/backup/o;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->memoizedIsInitialized:B

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/backup/o;Lcom/android/settings/backup/SettingProtos$LockSetting;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/SettingProtos$LockSetting;-><init>(Lcom/android/settings/backup/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->memoizedIsInitialized:B

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->memoizedSerializedSize:I

    return-void
.end method

.method public static aGS()Lcom/android/settings/backup/o;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/o;->aHq()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public static aGU()Lcom/android/settings/backup/SettingProtos$LockSetting;
    .locals 1

    sget-object v0, Lcom/android/settings/backup/SettingProtos$LockSetting;->aUB:Lcom/android/settings/backup/SettingProtos$LockSetting;

    return-object v0
.end method

.method private aGW()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->guid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->guid_:Ljava/lang/Object;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method private aGY()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->luid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->luid_:Ljava/lang/Object;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method private aGZ()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->name_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->name_:Ljava/lang/Object;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method private aHe()V
    .locals 2

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->guid_:Ljava/lang/Object;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->luid_:Ljava/lang/Object;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->name_:Ljava/lang/Object;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->value_:J

    return-void
.end method

.method public static aHf(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/o;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGS()Lcom/android/settings/backup/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/o;->aHn(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aHg(Lcom/android/settings/backup/SettingProtos$LockSetting;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    return p1
.end method

.method static synthetic aHh(Lcom/android/settings/backup/SettingProtos$LockSetting;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->guid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic aHi(Lcom/android/settings/backup/SettingProtos$LockSetting;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->luid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic aHj(Lcom/android/settings/backup/SettingProtos$LockSetting;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->name_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic aHk(Lcom/android/settings/backup/SettingProtos$LockSetting;J)J
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->value_:J

    return-wide p1
.end method


# virtual methods
.method public aGT()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->name_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->name_:Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public aGV()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->guid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->guid_:Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public aGX()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->luid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->luid_:Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public aHa()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aHb()Z
    .locals 2

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aHc()Z
    .locals 2

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aHd()Z
    .locals 2

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$LockSetting;
    .locals 1

    sget-object v0, Lcom/android/settings/backup/SettingProtos$LockSetting;->aUB:Lcom/android/settings/backup/SettingProtos$LockSetting;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    return v1

    :cond_0
    iget v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGW()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_1
    iget v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGY()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGZ()Lcom/google/protobuf/ByteString;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget-wide v2, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->value_:J

    invoke-static {v5, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeSInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->memoizedSerializedSize:I

    return v0
.end method

.method public getValue()J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->value_:J

    return-wide v0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v0, 0x1

    iget-byte v1, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->memoizedIsInitialized:B

    return v0
.end method

.method public newBuilderForType()Lcom/android/settings/backup/o;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGS()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->newBuilderForType()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/android/settings/backup/o;
    .locals 1

    invoke-static {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aHf(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->toBuilder()Lcom/android/settings/backup/o;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->getSerializedSize()I

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGW()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGY()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGZ()Lcom/google/protobuf/ByteString;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-wide v0, p0, Lcom/android/settings/backup/SettingProtos$LockSetting;->value_:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeSInt64(IJ)V

    :cond_3
    return-void
.end method
