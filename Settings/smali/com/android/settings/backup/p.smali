.class public final Lcom/android/settings/backup/p;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SettingProtos.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# instance fields
.field private bitField0_:I

.field private lock_:Ljava/util/List;

.field private secure_:Ljava/util/List;

.field private system_:Ljava/util/List;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    invoke-direct {p0}, Lcom/android/settings/backup/p;->aHK()V

    return-void
.end method

.method private aHH()V
    .locals 2

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    :cond_0
    return-void
.end method

.method private aHI()V
    .locals 2

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    :cond_0
    return-void
.end method

.method private aHJ()V
    .locals 2

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    :cond_0
    return-void
.end method

.method private aHK()V
    .locals 0

    return-void
.end method

.method static synthetic aHM()Lcom/android/settings/backup/p;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/p;->create()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/android/settings/backup/p;
    .locals 1

    new-instance v0, Lcom/android/settings/backup/p;

    invoke-direct {v0}, Lcom/android/settings/backup/p;-><init>()V

    return-object v0
.end method


# virtual methods
.method public aHE(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/p;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/backup/p;->aHH()V

    iget-object v0, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public aHF(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/p;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/backup/p;->aHI()V

    iget-object v0, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public aHG(Lcom/android/settings/backup/SettingProtos$SystemSetting;)Lcom/android/settings/backup/p;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/backup/p;->aHJ()V

    iget-object v0, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public aHL(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/p;
    .locals 2

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHr()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    :cond_0
    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHA(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHA(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHz(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHz(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    :cond_2
    :goto_1
    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHy(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHy(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    :cond_3
    :goto_2
    return-object p0

    :cond_4
    invoke-direct {p0}, Lcom/android/settings/backup/p;->aHJ()V

    iget-object v0, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHA(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/android/settings/backup/p;->aHI()V

    iget-object v0, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHz(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/android/settings/backup/p;->aHH()V

    iget-object v0, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHy(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public build()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/backup/SettingProtos$Settings;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/android/settings/backup/p;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->build()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 3

    new-instance v0, Lcom/android/settings/backup/SettingProtos$Settings;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/backup/SettingProtos$Settings;-><init>(Lcom/android/settings/backup/p;Lcom/android/settings/backup/SettingProtos$Settings;)V

    iget v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    iget v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    iget v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    :cond_0
    iget-object v1, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHD(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)Ljava/util/List;

    iget v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    iget v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    :cond_1
    iget-object v1, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHC(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)Ljava/util/List;

    iget v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    iget v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/android/settings/backup/p;->bitField0_:I

    :cond_2
    iget-object v1, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/android/settings/backup/SettingProtos$Settings;->aHB(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/android/settings/backup/p;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/p;->system_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/p;->secure_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/p;->lock_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/android/settings/backup/p;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->clear()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->clear()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/android/settings/backup/p;
    .locals 2

    invoke-static {}, Lcom/android/settings/backup/p;->create()Lcom/android/settings/backup/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/p;->aHL(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->clone()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->clone()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->clone()Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->aHr()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/p;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/p;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settings/backup/p;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SystemSetting;->aFS()Lcom/android/settings/backup/m;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/android/settings/backup/m;->buildPartial()Lcom/android/settings/backup/SettingProtos$SystemSetting;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/p;->aHG(Lcom/android/settings/backup/SettingProtos$SystemSetting;)Lcom/android/settings/backup/p;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->aGs()Lcom/android/settings/backup/n;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/android/settings/backup/n;->buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/p;->aHF(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/p;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/android/settings/backup/SettingProtos$LockSetting;->aGS()Lcom/android/settings/backup/o;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/android/settings/backup/o;->buildPartial()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/p;->aHE(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/p;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/p;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/p;->aHL(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/p;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/p;

    move-result-object v0

    return-object v0
.end method
