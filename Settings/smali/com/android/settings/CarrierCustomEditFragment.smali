.class public Lcom/android/settings/CarrierCustomEditFragment;
.super Lcom/android/settings/BaseFragment;
.source "CarrierCustomEditFragment.java"


# instance fields
.field private bNS:Landroid/content/BroadcastReceiver;

.field private bNT:[Landroid/widget/EditText;

.field private bNU:Z

.field private bNV:I

.field bNW:[Ljava/lang/String;

.field private bNX:[Landroid/database/ContentObserver;

.field private bNY:[Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Lcom/android/settings/hW;

    invoke-direct {v0, p0}, Lcom/android/settings/hW;-><init>(Lcom/android/settings/CarrierCustomEditFragment;)V

    iput-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNS:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private bEY()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/CarrierCustomEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/android/settings/CarrierCustomEditFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method private bEZ(ILandroid/view/View;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    const v0, 0x7f0a0407

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNV:I

    if-ne v1, v2, :cond_2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v1, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNT:[Landroid/widget/EditText;

    const v0, 0x7f0a0161

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    aput-object v0, v1, p1

    iget-boolean v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNU:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNT:[Landroid/widget/EditText;

    aget-object v0, v0, p1

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lmiui/telephony/TelephonyManager;->hasIccCard(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/CarrierCustomEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "status_bar_custom_carrier"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNT:[Landroid/widget/EditText;

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNT:[Landroid/widget/EditText;

    aget-object v1, v1, p1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNY:[Landroid/text/TextWatcher;

    new-instance v1, Lcom/android/settings/hX;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/hX;-><init>(Lcom/android/settings/CarrierCustomEditFragment;I)V

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNT:[Landroid/widget/EditText;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNY:[Landroid/text/TextWatcher;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNX:[Landroid/database/ContentObserver;

    new-instance v1, Lcom/android/settings/hY;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2, p1}, Lcom/android/settings/hY;-><init>(Lcom/android/settings/CarrierCustomEditFragment;Landroid/os/Handler;I)V

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNX:[Landroid/database/ContentObserver;

    aget-object v0, v0, p1

    invoke-virtual {v0, v4}, Landroid/database/ContentObserver;->onChange(Z)V

    return-void

    :cond_2
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/settings/CarrierCustomEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f121124

    invoke-virtual {v1, v3, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method static synthetic bFa(Lcom/android/settings/CarrierCustomEditFragment;)[Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNT:[Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic bFb(Lcom/android/settings/CarrierCustomEditFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNU:Z

    return v0
.end method

.method static synthetic bFc(Lcom/android/settings/CarrierCustomEditFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNV:I

    return v0
.end method


# virtual methods
.method public finish()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/CarrierCustomEditFragment;->bEY()V

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/settings/CarrierCustomEditFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNU:Z

    return-void
.end method

.method public onDestroyView()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/CarrierCustomEditFragment;->bEY()V

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNV:I

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/CarrierCustomEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNX:[Landroid/database/ContentObserver;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDestroyView()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const v0, 0x7f0d01de

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    :cond_0
    return-object v1
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/CarrierCustomEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNS:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/CarrierCustomEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNS:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v0

    sget-boolean v2, Lcom/android/settings/g;->bsZ:Z

    if-nez v2, :cond_0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNV:I

    iget v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNV:I

    new-array v0, v0, [Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNT:[Landroid/widget/EditText;

    iget v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNV:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNW:[Ljava/lang/String;

    iget v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNV:I

    new-array v0, v0, [Landroid/database/ContentObserver;

    iput-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNX:[Landroid/database/ContentObserver;

    iget v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNV:I

    new-array v0, v0, [Landroid/text/TextWatcher;

    iput-object v0, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNY:[Landroid/text/TextWatcher;

    const v0, 0x7f0a0113

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v4, v0}, Lcom/android/settings/CarrierCustomEditFragment;->bEZ(ILandroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/settings/CarrierCustomEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v0, 0x7f0a039a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    :goto_0
    iget v3, p0, Lcom/android/settings/CarrierCustomEditFragment;->bNV:I

    if-ge v1, v3, :cond_2

    const v3, 0x7f0d01df

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-direct {p0, v1, v3}, Lcom/android/settings/CarrierCustomEditFragment;->bEZ(ILandroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
