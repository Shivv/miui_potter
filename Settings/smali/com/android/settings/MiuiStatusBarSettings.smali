.class public Lcom/android/settings/MiuiStatusBarSettings;
.super Lcom/android/settings/StatusBarSettingsPreferenceFragment;
.source "MiuiStatusBarSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private caV:Landroid/preference/ListPreference;

.field private caW:Lmiui/preference/ValuePreference;

.field private caX:Landroid/preference/PreferenceScreen;

.field private caY:Z

.field private caZ:Landroid/preference/CheckBoxPreference;

.field private cba:Lcom/android/settings/ShowCarrierChoicePreference;

.field private cbb:Landroid/preference/CheckBoxPreference;

.field private cbc:Landroid/preference/CheckBoxPreference;

.field private cbd:Landroid/preference/ListPreference;

.field private cbe:Landroid/preference/CheckBoxPreference;

.field private cbf:Landroid/preference/PreferenceScreen;

.field private cbg:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/StatusBarSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private bTp(Landroid/content/Context;)I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "status_bar_show_custom_carrier"

    invoke-static {}, Landroid/provider/MiuiSettings$System;->getShowCustomCarrierDefault()I

    move-result v3

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    sget-boolean v2, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-eqz v2, :cond_1

    if-eqz v1, :cond_0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private bTq(Landroid/content/Context;I)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "status_bar_show_custom_carrier"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/MiuiStatusBarSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/StatusBarSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f1500ea

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "status_bar_style"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbd:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbd:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "user_fold"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbg:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbg:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "toggle_sort"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbf:Landroid/preference/PreferenceScreen;

    const-string/jumbo v0, "settings_custom_notification"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caX:Landroid/preference/PreferenceScreen;

    const-string/jumbo v0, "show_notification_icon"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbc:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbc:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "show_network_speed"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbb:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbb:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caY:Z

    const-string/jumbo v0, "custom_carrier"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caW:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caW:Lmiui/preference/ValuePreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    const-string/jumbo v0, "show_carrier"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ShowCarrierChoicePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/ShowCarrierChoicePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-boolean v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caY:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f12113f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/ShowCarrierChoicePreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caW:Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f121127

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    const-string/jumbo v0, "battery_indicator"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caV:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caV:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "toggle_collapse_after_clicked"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbe:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbe:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbe:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/app/MiuiStatusBarManager;->isCollapseAfterClicked(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "expandable_under_keyguard"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caZ:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caZ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caZ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/app/MiuiStatusBarManager;->isExpandableUnderKeyguard(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "settings_status_bar"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/MiuiStatusBarSettings;->caV:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v0, "settings_status_bar"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiStatusBarSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbc:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "show_network_speed"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v2}, Landroid/app/MiuiStatusBarManager;->setShowNetworkSpeed(Landroid/content/Context;Z)V

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string/jumbo v2, "show_carrier"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    invoke-virtual {v3}, Lcom/android/settings/ShowCarrierChoicePreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/android/settings/MiuiStatusBarSettings;->bTq(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    iget-object v3, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    invoke-virtual {v3}, Lcom/android/settings/ShowCarrierChoicePreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v3

    aget-object v0, v3, v0

    invoke-virtual {v2, v0}, Lcom/android/settings/ShowCarrierChoicePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v2, "expandable_under_keyguard"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v2}, Landroid/app/MiuiStatusBarManager;->setExpandableUnderKeyguard(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_3
    const-string/jumbo v2, "toggle_collapse_after_clicked"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v2}, Landroid/app/MiuiStatusBarManager;->setCollapseAfterClicked(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_4
    const-string/jumbo v2, "show_notification_icon"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v0, v2}, Landroid/app/MiuiStatusBarManager;->setShowNotificationIcon(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_5
    const-string/jumbo v2, "user_fold"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "user_fold"

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    :cond_6
    const/4 v0, -0x1

    goto :goto_1

    :cond_7
    const-string/jumbo v2, "status_bar_style"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :try_start_0
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "status_bar_style_type"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v2, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbd:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbd:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v3

    aget-object v0, v3, v0

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_8
    const-string/jumbo v2, "battery_indicator"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_1
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "battery_indicator_style"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v2, p0, Lcom/android/settings/MiuiStatusBarSettings;->caV:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/settings/MiuiStatusBarSettings;->caV:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v3

    aget-object v0, v3, v0

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    goto/16 :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caW:Lmiui/preference/ValuePreference;

    if-ne p2, v0, :cond_2

    const-string/jumbo v2, "com.android.settings.CarrierCustomEditFragment"

    iget-boolean v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caY:Z

    if-eqz v0, :cond_1

    const v5, 0x7f121125

    :goto_0
    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/MiuiStatusBarSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :cond_0
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/android/settings/StatusBarSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    const v5, 0x7f121127

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbf:Landroid/preference/PreferenceScreen;

    if-ne p2, v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lmiui/app/ToggleManager;->isListStyle(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v2, "com.android.settings.ToggleArrangementFragment"

    :goto_2
    const v5, 0x7f121147

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/MiuiStatusBarSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lmiui/app/ToggleManager;->isListStyle(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "from_single_page_settings"

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settings/bo;->bIW(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string/jumbo v2, "com.android.settings.TogglePositionFragment"

    goto :goto_2

    :cond_4
    const-string/jumbo v0, "from_double_page_settings"

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caX:Landroid/preference/PreferenceScreen;

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/bo;->bIV(Landroid/content/Context;)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 10

    const/4 v9, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/StatusBarSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbb:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/app/MiuiStatusBarManager;->isShowNetworkSpeed(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbc:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/app/MiuiStatusBarManager;->isShowNotificationIcon(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbg:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/g;->bfH(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f12112c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    sget-boolean v0, Lcom/android/settings/g;->bsZ:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    new-array v6, v0, [Ljava/lang/String;

    move v0, v2

    :goto_1
    array-length v3, v6

    if-ge v0, v3, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "status_bar_custom_carrier"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7, v9}, Landroid/provider/MiuiSettings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    aget-object v3, v6, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    aget-object v3, v6, v0

    :goto_2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_2

    const-string/jumbo v3, ""

    :goto_3
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v0

    goto :goto_0

    :cond_1
    move-object v3, v4

    goto :goto_2

    :cond_2
    const-string/jumbo v3, " | "

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->caW:Lmiui/preference/ValuePreference;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    iget-object v3, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/settings/MiuiStatusBarSettings;->bTp(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/settings/ShowCarrierChoicePreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/settings/ShowCarrierChoicePreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    iget-object v3, p0, Lcom/android/settings/MiuiStatusBarSettings;->cba:Lcom/android/settings/ShowCarrierChoicePreference;

    invoke-virtual {v3}, Lcom/android/settings/ShowCarrierChoicePreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/ShowCarrierChoicePreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "battery_indicator_style"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_4

    iget-object v3, p0, Lcom/android/settings/MiuiStatusBarSettings;->caV:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_4

    iget-object v3, p0, Lcom/android/settings/MiuiStatusBarSettings;->caV:Landroid/preference/ListPreference;

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v3, p0, Lcom/android/settings/MiuiStatusBarSettings;->caV:Landroid/preference/ListPreference;

    iget-object v4, p0, Lcom/android/settings/MiuiStatusBarSettings;->caV:Landroid/preference/ListPreference;

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v4

    aget-object v0, v4, v0

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbd:Landroid/preference/ListPreference;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbd:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiStatusBarSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lmiui/app/ToggleManager;->isListStyle(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    :goto_4
    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbd:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/MiuiStatusBarSettings;->cbd:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_5
    return-void

    :cond_6
    move v2, v1

    goto :goto_4
.end method
