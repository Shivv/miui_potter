.class Lcom/android/settings/i;
.super Landroid/widget/ArrayAdapter;
.source "MiuiSettings.java"


# instance fields
.field private btZ:Lcom/android/settingslib/c/a;

.field private bua:Ljava/util/HashMap;

.field private bub:Landroid/view/LayoutInflater;

.field private buc:Z

.field private bud:Ljava/util/Locale;

.field private bue:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;Lcom/android/settingslib/c/a;Z)V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object p3, p0, Lcom/android/settings/i;->btZ:Lcom/android/settingslib/c/a;

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/settings/i;->bub:Landroid/view/LayoutInflater;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/i;->bua:Ljava/util/HashMap;

    iput-boolean p4, p0, Lcom/android/settings/i;->buc:Z

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v0, p0, Lcom/android/settings/i;->bud:Ljava/util/Locale;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a0520

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/wifi/M;

    invoke-direct {v2, p1, v4}, Lcom/android/settings/wifi/M;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a0094

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/bluetooth/BluetoothStatusController;

    invoke-direct {v2, p1, v4}, Lcom/android/settings/bluetooth/BluetoothStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a052f

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/f/a;

    invoke-direct {v2, p1, v4}, Lcom/android/settings/f/a;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a0299

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/accounts/XiaomiAccountStatusController;

    invoke-direct {v2, p1, v4}, Lcom/android/settings/accounts/XiaomiAccountStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a01a5

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/display/c;

    invoke-direct {v2, p1, v4}, Lcom/android/settings/display/c;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a04ff

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/vpn2/VpnStatusController;

    invoke-direct {v2, p1, v4}, Lcom/android/settings/vpn2/VpnStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/settings/device/h;->aDV()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a02b6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/device/c;

    invoke-direct {v2, p1, v4}, Lcom/android/settings/device/c;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a0477

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/applications/SystemAppUpdaterStatusController;

    iget-object v3, p0, Lcom/android/settings/i;->bud:Ljava/util/Locale;

    invoke-direct {v2, p1, v4, v3}, Lcom/android/settings/applications/SystemAppUpdaterStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;Ljava/util/Locale;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a02b2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/f/b;

    invoke-direct {v2, p1, v4}, Lcom/android/settings/f/b;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    const-wide/32 v2, 0x7f0a0006

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/android/settings/device/c;

    invoke-direct {v2, p1, v4}, Lcom/android/settings/device/c;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private bgD(Landroid/preference/PreferenceActivity$Header;)I
    .locals 5

    const/4 v4, 0x1

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0a04f7

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    return v4

    :cond_0
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/i;->bgE(J)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    return v0

    :cond_2
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0a02b6

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0a0006

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x2

    return v0

    :cond_4
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/i;->bgF(J)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    return v0

    :cond_5
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0a0477

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    const/4 v0, 0x5

    return v0

    :cond_6
    return v4
.end method

.method private bgH(Lcom/android/settings/j;Landroid/preference/PreferenceActivity$Header;)V
    .locals 8

    const-wide/32 v6, 0x7f0a02b2

    const/4 v4, 0x1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-wide v0, p2, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0a02b6

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-wide v0, p2, Landroid/preference/PreferenceActivity$Header;->id:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    iget-wide v0, p2, Landroid/preference/PreferenceActivity$Header;->id:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3

    iget-wide v0, p2, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0a02a1

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    :cond_3
    iget-object v0, p1, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p1, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p1, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_5
    iget-object v0, p1, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private bgI(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lmiui/R$dimen;->preference_horizontal_extra_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    :cond_0
    iget v1, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method private bgK(Lcom/android/settings/j;Landroid/preference/PreferenceActivity$Header;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const v2, 0x3e99999a    # 0.3f

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-wide v4, p2, Landroid/preference/PreferenceActivity$Header;->id:J

    long-to-int v4, v4

    const v5, 0x7f0a02b2

    if-ne v4, v5, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "no_config_mobile_networks"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v4

    if-eqz v4, :cond_6

    :cond_2
    :goto_0
    iget-object v1, p1, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    iget-object v4, p1, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setAlpha(F)V

    :cond_3
    iget-object v1, p1, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    iget-object v4, p1, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setAlpha(F)V

    :cond_4
    iget-object v1, p1, Lcom/android/settings/j;->buf:Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/android/settings/j;->buf:Landroid/widget/ImageView;

    if-eqz v0, :cond_a

    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_5
    return-void

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    const v5, 0x7f0a052f

    if-ne v4, v5, :cond_b

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "no_config_tethering"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v4

    if-nez v4, :cond_2

    move v0, v1

    goto :goto_0

    :cond_8
    move v1, v3

    goto :goto_1

    :cond_9
    move v1, v3

    goto :goto_2

    :cond_a
    move v2, v3

    goto :goto_3

    :cond_b
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bgE(J)Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/i;->bua:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bgF(J)Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bgG()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/i;->bua:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bP;

    invoke-virtual {v0}, Lcom/android/settings/bP;->bNw()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bA;

    invoke-virtual {v0}, Lcom/android/settings/bA;->wt()V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/i;->notifyDataSetChanged()V

    return-void
.end method

.method public bgJ(Lcom/android/settings/j;Landroid/preference/PreferenceActivity$Header;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/android/settings/j;->buf:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/android/settings/j;->buf:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget-object v0, p2, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    iget-object v0, p2, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    const-string/jumbo v1, "account_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "com.xiaomi"

    const-string/jumbo v1, "com.xiaomi"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p1, Lcom/android/settings/j;->buf:Landroid/widget/ImageView;

    const v1, 0x7f080468

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/settings/i;->btZ:Lcom/android/settingslib/c/a;

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/settingslib/c/a;->cgj(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p1, Lcom/android/settings/j;->buf:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_4
    iget v0, p2, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/settings/j;->buf:Landroid/widget/ImageView;

    iget v1, p2, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/i;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {p0, v0}, Lcom/android/settings/i;->bgD(Landroid/preference/PreferenceActivity$Header;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    invoke-virtual {p0, p1}, Lcom/android/settings/i;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {p0, v0}, Lcom/android/settings/i;->bgD(Landroid/preference/PreferenceActivity$Header;)I

    move-result v4

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/j;

    iget-object v2, v1, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    if-eqz v2, :cond_10

    iget-object v2, v1, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v5, v6, v7}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object v3, v1

    :goto_0
    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0, v3, v0}, Lcom/android/settings/i;->bgJ(Lcom/android/settings/j;Landroid/preference/PreferenceActivity$Header;)V

    invoke-direct {p0, v3, v0}, Lcom/android/settings/i;->bgH(Lcom/android/settings/j;Landroid/preference/PreferenceActivity$Header;)V

    invoke-direct {p0, v3, v0}, Lcom/android/settings/i;->bgK(Lcom/android/settings/j;Landroid/preference/PreferenceActivity$Header;)V

    return-object p2

    :cond_1
    new-instance v2, Lcom/android/settings/j;

    invoke-direct {v2}, Lcom/android/settings/j;-><init>()V

    packed-switch v4, :pswitch_data_1

    iget-object v1, p0, Lcom/android/settings/i;->bub:Landroid/view/LayoutInflater;

    sget v3, Lmiui/R$layout;->preference_value:I

    const/4 v5, 0x0

    invoke-virtual {v1, v3, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_2
    :goto_2
    if-eqz p2, :cond_f

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v3, v2

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/android/settings/i;->bub:Landroid/view/LayoutInflater;

    sget v3, Lmiui/R$layout;->preference_category:I

    const/4 v5, 0x0

    invoke-virtual {v1, v3, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    goto :goto_2

    :pswitch_1
    iget-object v1, p0, Lcom/android/settings/i;->bub:Landroid/view/LayoutInflater;

    sget v3, Lmiui/R$layout;->preference_value:I

    const/4 v5, 0x0

    invoke-virtual {v1, v3, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v1, 0x1020006

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/android/settings/j;->buf:Landroid/widget/ImageView;

    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    const v1, 0x1020010

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    const v1, 0x1020018

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/android/settings/i;->bub:Landroid/view/LayoutInflater;

    const v5, 0x7f0d0139

    invoke-virtual {v3, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v3, 0x7f0a040f

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiui/widget/SlidingButton;

    iput-object v1, v2, Lcom/android/settings/j;->bug:Lmiui/widget/SlidingButton;

    sget v1, Lmiui/R$id;->arrow_right:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/android/settings/i;->bub:Landroid/view/LayoutInflater;

    sget v3, Lmiui/R$layout;->preference_value:I

    const/4 v5, 0x0

    invoke-virtual {v1, v3, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v1, :cond_3

    sget v1, Lmiui/R$id;->arrow_right:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    const v1, 0x1020018

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x1020006

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/android/settings/j;->buf:Landroid/widget/ImageView;

    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    const v1, 0x1020010

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    sget v1, Lmiui/R$id;->value_right:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    goto/16 :goto_2

    :pswitch_3
    iget-object v1, v3, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v3, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v3, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz p2, :cond_0

    sget v1, Lmiui/R$drawable;->preference_category_background_no_title:I

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v1, v3, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz p2, :cond_0

    if-nez p1, :cond_5

    sget v1, Lmiui/R$drawable;->preference_category_background_first:I

    :goto_3
    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    :cond_5
    sget v1, Lmiui/R$drawable;->preference_category_background:I

    goto :goto_3

    :pswitch_4
    iget-object v1, p0, Lcom/android/settings/i;->bua:Ljava/util/HashMap;

    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/bP;

    if-eqz v1, :cond_6

    iget-object v2, v3, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/android/settings/bP;->bNu(Landroid/widget/TextView;)V

    iget-object v2, v3, Lcom/android/settings/j;->bug:Lmiui/widget/SlidingButton;

    invoke-virtual {v1, v2}, Lcom/android/settings/bP;->bNv(Lmiui/widget/SlidingButton;)V

    iget-object v2, v3, Lcom/android/settings/j;->bug:Lmiui/widget/SlidingButton;

    invoke-virtual {v2, v1}, Lmiui/widget/SlidingButton;->setTag(Ljava/lang/Object;)V

    :cond_6
    :pswitch_5
    iget-object v1, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/bA;

    if-eqz v1, :cond_7

    iget-object v2, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/android/settings/bA;->bKV(Landroid/widget/TextView;)V

    iget-object v2, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    :cond_7
    :pswitch_6
    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "system_app"

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, v0, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    iget-object v5, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v6, "."

    const-string/jumbo v7, "_"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v2, v1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "drawable"

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    :cond_8
    :pswitch_7
    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f0a0477

    cmp-long v1, v6, v8

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/bA;

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v5, "updatable_system_app_count"

    const/4 v6, 0x0

    invoke-static {v2, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    iget-object v6, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    if-lez v5, :cond_c

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/settings/i;->bud:Ljava/util/Locale;

    invoke-static {v6}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v6

    int-to-long v8, v5

    invoke-virtual {v6, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    const v5, 0x7f08044b

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v2, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    const/4 v5, -0x1

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    const/high16 v5, 0x41200000    # 10.0f

    const/4 v6, 0x1

    invoke-virtual {v2, v6, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v2, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/android/settings/bA;->bKV(Landroid/widget/TextView;)V

    :cond_9
    :goto_5
    :pswitch_8
    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lmiui/R$attr;->preferenceWithIconBackground:I

    invoke-static {v1, v2}, Lmiui/util/AttributeResolver;->resolveDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz p2, :cond_a

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_a
    iget-object v1, v3, Lcom/android/settings/j;->bui:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/i;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity$Header;->getSummary(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, v3, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v3, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    const/4 v1, 0x2

    if-ne v4, v1, :cond_b

    iget-object v1, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    iget-wide v4, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/bA;

    check-cast v1, Lcom/android/settings/device/c;

    iget-object v2, v3, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    iget-object v4, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v4}, Lcom/android/settings/device/c;->aDl(Landroid/widget/TextView;Landroid/widget/TextView;)V

    :cond_b
    invoke-direct {p0, p2}, Lcom/android/settings/i;->bgI(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_c
    const/16 v2, 0x8

    goto/16 :goto_4

    :cond_d
    iget-object v1, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    if-eqz v1, :cond_9

    iget-object v1, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, v3, Lcom/android/settings/j;->buj:Landroid/widget/TextView;

    const v2, 0x800005

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_5

    :cond_e
    iget-object v1, v3, Lcom/android/settings/j;->buh:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6

    :cond_f
    move-object v3, v2

    goto/16 :goto_0

    :cond_10
    move-object v3, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_6
        :pswitch_8
        :pswitch_5
        :pswitch_4
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/android/settings/i;->getItemViewType(I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/i;->bua:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bP;

    invoke-virtual {v0}, Lcom/android/settings/bP;->pause()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/i;->bue:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bA;

    invoke-virtual {v0}, Lcom/android/settings/bA;->pause()V

    goto :goto_1

    :cond_1
    return-void
.end method
