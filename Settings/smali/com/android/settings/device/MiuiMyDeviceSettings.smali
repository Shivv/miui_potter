.class public Lcom/android/settings/device/MiuiMyDeviceSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiMyDeviceSettings.java"


# static fields
.field private static aTa:Ljava/lang/String;

.field private static aTb:Ljava/lang/String;

.field private static final synthetic aTi:[I


# instance fields
.field private aTc:Lcom/android/settings/device/m;

.field private aTd:Lmiui/preference/ValuePreference;

.field private aTe:Lmiui/preference/ValuePreference;

.field private aTf:Z

.field private aTg:Landroid/telephony/TelephonyManager;

.field private aTh:Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

.field private mRootView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "mab://ab.xiaomi.com/d?url=aHR0cDovL20ubWkuY29tL3Nkaz9waWQ9MTAwJmNpZD0yMDAyNy4wMDAwMiZjbGllbnRfaWQ9MTgwMTAwMDQxMDc4&fallback=https%3a%2f%2fm.mi.com%2f%3f%26client_id%3d180100031058%26masid%3d20027.00002"

    sput-object v0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTb:Ljava/lang/String;

    const-string/jumbo v0, "http://ab.xiaomi.com/d?url=aHR0cDovL20ubWkuY29tL3Nkaz9waWQ9MTAwJmNpZD0yMDAyNy4wMDAwMiZjbGllbnRfaWQ9MTgwMTAwMDQxMDc4&fallback=https%3a%2f%2fm.mi.com%2f%3f%26client_id%3d180100031058%26masid%3d20027.00002"

    sput-object v0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTa:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private aDA()V
    .locals 5

    const-string/jumbo v0, "support_forbid_ads"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "mi_shopping_mall_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->bWK(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lmiui/accounts/ExtraAccountManager;->getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    const-string/jumbo v2, "miId"

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string/jumbo v3, "UTF-8"

    invoke-static {v0, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTg:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "imei"

    invoke-static {v0}, Lcom/android/settings/device/f;->aDy(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "UTF-8"

    invoke-static {v0, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    const-string/jumbo v0, "device"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v3, "UTF-8"

    invoke-static {v2, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "model"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string/jumbo v3, "UTF-8"

    invoke-static {v2, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string/jumbo v2, "langType"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDJ()[I

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTh:Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

    invoke-virtual {v2}, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    const-string/jumbo v0, "version"

    const-string/jumbo v2, "0"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    const-string/jumbo v0, "https://adv.sec.miui.com/myDevClound/devInfoNew"

    new-instance v2, Lcom/android/settings/device/g;

    invoke-direct {v2, p0}, Lcom/android/settings/device/g;-><init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V

    invoke-static {v0, v1, v2}, Lcom/android/settings/device/i;->aEr(Ljava/lang/String;Ljava/util/HashMap;Lcom/android/settings/device/k;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    const-string/jumbo v0, "version"

    const-string/jumbo v2, "0"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :pswitch_1
    const-string/jumbo v0, "version"

    const-string/jumbo v2, "1"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "version"

    const-string/jumbo v2, "2"

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private aDB(Landroid/view/ViewGroup;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/device/h;->aDW(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/android/settings/device/h;->aEb(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v5, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTf:Z

    :cond_0
    iget-boolean v6, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTf:Z

    const v2, 0x7f12059b

    const v3, 0x7f080320

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDz(Landroid/view/ViewGroup;IILjava/lang/String;ZZ)Landroid/view/View;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTf:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    new-instance v1, Lcom/android/settings/device/q;

    invoke-direct {v1, p0}, Lcom/android/settings/device/q;-><init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v7}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v0, v7}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method private aDC(Landroid/preference/PreferenceCategory;Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void
.end method

.method private aDD(Landroid/view/View;Ljava/lang/String;Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const v0, 0x7f0a04ce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p3, :cond_0

    const v0, 0x7f0a04d0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080108

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v3

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, v4, v4, v1, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method private aDE(Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v0, "mi_shopping_mall"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/CustomValuePreference;

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/android/settings/device/l;->aEz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    sput-object v1, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTb:Ljava/lang/String;

    :cond_0
    invoke-static {p1}, Lcom/android/settings/device/l;->aEA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/CustomValuePreference;->setValue(Ljava/lang/String;)V

    :cond_1
    invoke-static {p1}, Lcom/android/settings/device/l;->aEB(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/CustomValuePreference;->aAp(I)V

    :cond_2
    return-void
.end method

.method static synthetic aDF(Lcom/android/settings/device/MiuiMyDeviceSettings;)Lcom/android/settings/device/m;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTc:Lcom/android/settings/device/m;

    return-object v0
.end method

.method static synthetic aDG(Lcom/android/settings/device/MiuiMyDeviceSettings;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTf:Z

    return v0
.end method

.method static synthetic aDH(Lcom/android/settings/device/MiuiMyDeviceSettings;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic aDI(Lcom/android/settings/device/MiuiMyDeviceSettings;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDE(Ljava/lang/String;)V

    return-void
.end method

.method private static synthetic aDJ()[I
    .locals 3

    sget-object v0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTi:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTi:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->values()[Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->aTs:Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

    invoke-virtual {v1}, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->aTt:Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

    invoke-virtual {v1}, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->aTu:Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

    invoke-virtual {v1}, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->aTv:Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

    invoke-virtual {v1}, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    sput-object v0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTi:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method private aDz(Landroid/view/ViewGroup;IILjava/lang/String;ZZ)Landroid/view/View;
    .locals 6

    const v5, 0x7f0a04cb

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0d010a

    invoke-virtual {v0, v3, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0a04cf

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, p2}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v3, p4, p5}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDD(Landroid/view/View;Ljava/lang/String;Z)V

    if-lez p3, :cond_1

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    const v0, 0x7f0a0077

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz p6, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lmiui/R$drawable;->preference_item_bg:I

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lmiui/R$dimen;->preference_horizontal_extra_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    :cond_0
    iget v1, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v3, v1, v2, v0, v4}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v3

    :cond_1
    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public forceEnglish()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const-string v0, "en"

    invoke-static {v0}, Ljava/util/Locale;->forLanguageTag(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->forceEnglish()V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTf:Z

    iget-boolean v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTf:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->setHasOptionsMenu(Z)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/device/h;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/device/h;->aEc()Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTh:Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTg:Landroid/telephony/TelephonyManager;

    const v0, 0x7f150092

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->addPreferencesFromResource(I)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v0, Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;->aTv:Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTh:Lcom/android/settings/device/MiuiAboutPhoneUtils$PhoneConfigurationType;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTc:Lcom/android/settings/device/m;

    invoke-virtual {v0}, Lcom/android/settings/device/m;->aEJ()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/settings/dc;->bYu(Landroid/app/Activity;Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    if-nez v0, :cond_1

    const v0, 0x7f0d0109

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/device/m;->aEI(Landroid/content/Context;)Lcom/android/settings/device/m;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTc:Lcom/android/settings/device/m;

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    const v1, 0x7f0a032f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDB(Landroid/view/ViewGroup;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    invoke-static {}, Lcom/android/settings/E;->blE()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "instruction"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dc;->bYz()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.extra.LICENSE_TYPE"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/dc;->bYt(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    const-string/jumbo v1, "device_name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, ":miui:starting_window_label"

    const-string/jumbo v1, ""

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-class v0, Lcom/android/settings/MiuiDeviceNameEditFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p0

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/device/MiuiMyDeviceSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "mi_shopping_mall"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "provision_about_v85x"

    const-string/jumbo v1, "provision_about_page_goto_mi_shop_v85x"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTb:Ljava/lang/String;

    const-string/jumbo v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTb:Ljava/lang/String;

    const-string/jumbo v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTb:Ljava/lang/String;

    const-string/jumbo v1, "mab://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    sget-object v2, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTb:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/dc;->bYt(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    sget-object v2, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTa:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    :cond_4
    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    const-string/jumbo v0, "model_number"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-static {}, Lcom/android/settings/device/h;->aDO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    const-string/jumbo v0, "device_internal_memory"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTe:Lmiui/preference/ValuePreference;

    const-string/jumbo v0, "device_name"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTd:Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-static {}, Lcom/android/settings/device/h;->aDY()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "credentials"

    invoke-direct {p0, v0, v1}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDC(Landroid/preference/PreferenceCategory;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lcom/android/settings/device/h;->aDX()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "instruction"

    invoke-direct {p0, v0, v1}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDC(Landroid/preference/PreferenceCategory;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->aDA()V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTe:Lmiui/preference/ValuePreference;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/device/h;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/device/h;->aDT()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f120a67

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTe:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v4}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTd:Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/MiuiSettings$System;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTd:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v4}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTd:Lmiui/preference/ValuePreference;

    iget-boolean v1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->aTf:Z

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "provision_about_page_v85x"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blD(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method
