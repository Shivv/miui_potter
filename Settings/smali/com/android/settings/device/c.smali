.class public Lcom/android/settings/device/c;
.super Lcom/android/settings/bA;
.source "DeviceStatusController.java"


# instance fields
.field private aSU:Landroid/graphics/drawable/Drawable;

.field private aSV:Landroid/widget/TextView;

.field private aSW:Lcom/android/settings/device/d;

.field private mSummary:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bA;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    return-void
.end method

.method private aDm(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/device/c;->mSummary:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/device/c;->aSV:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-eqz v1, :cond_0

    move-object p1, v0

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/settings/device/c;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080108

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/device/c;->aSU:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/settings/device/c;->aSU:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/android/settings/device/c;->aSU:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/device/c;->aSU:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/settings/device/c;->aSV:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/device/c;->aSV:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/device/c;->aSU:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0, v0, v2, v0}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/android/settings/device/h;->aDV()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/device/c;->aSV:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/device/c;->aSV:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/android/settings/device/c;->aSV:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v1, p0, Lcom/android/settings/device/c;->mContext:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/android/settings/device/h;->aEb(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p1

    iput-object v0, p0, Lcom/android/settings/device/c;->aSU:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/device/c;->aSV:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic aDo(Lcom/android/settings/device/c;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/device/c;->aDm(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public aDl(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/c;->mSummary:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/android/settings/device/c;->aSV:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/device/c;->aDn()V

    return-void
.end method

.method public aDn()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/device/c;->aSW:Lcom/android/settings/device/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/device/c;->aSW:Lcom/android/settings/device/d;

    invoke-virtual {v0}, Lcom/android/settings/device/d;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/device/c;->aSW:Lcom/android/settings/device/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/device/d;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/settings/device/d;

    invoke-direct {v0, p0}, Lcom/android/settings/device/d;-><init>(Lcom/android/settings/device/c;)V

    iput-object v0, p0, Lcom/android/settings/device/c;->aSW:Lcom/android/settings/device/d;

    iget-object v0, p0, Lcom/android/settings/device/c;->aSW:Lcom/android/settings/device/d;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/device/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/device/c;->aSW:Lcom/android/settings/device/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/device/c;->aSW:Lcom/android/settings/device/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/device/d;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public wt()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/device/c;->aDn()V

    return-void
.end method

.method protected wv()V
    .locals 0

    return-void
.end method
