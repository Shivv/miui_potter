.class public Lcom/android/settings/device/MiuiMyDeviceDetailSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiMyDeviceDetailSettings.java"


# instance fields
.field aSr:I

.field aSs:Landroid/widget/Toast;

.field private aSt:Lmiui/preference/ValuePreference;

.field aSu:[J

.field aSv:Ljava/lang/String;

.field aSw:J

.field aSx:I

.field private aSy:Lcom/android/settings/device/a;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSu:[J

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSx:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSw:J

    return-void
.end method

.method private aDc(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "device_cpu"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {v0, p1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    const-string/jumbo v0, "device_memory"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {v0, p2}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method private aDd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    :try_start_0
    invoke-virtual {v0, p2}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120594

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aDe(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120594

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p2, v2, v1}, Lcom/android/settings/az;->buf(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic aDf(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aDc(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d0142

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const v4, 0x7f120df4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150091

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->addPreferencesFromResource(I)V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->setHasOptionsMenu(Z)V

    const-string/jumbo v0, "model_number"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-static {}, Lcom/android/settings/device/h;->aDO()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    const-string/jumbo v0, "firmware_version"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    sget-object v0, Landroid/os/Build$VERSION;->SECURITY_PATCH:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, "security_patch"

    invoke-direct {p0, v3, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aDd(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string/jumbo v0, "device_miui_version"

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/device/h;->aDP(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aDd(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "device_cpu"

    invoke-virtual {p0, v4}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aDd(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "device_memory"

    invoke-virtual {p0, v4}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aDd(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSy:Lcom/android/settings/device/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/device/a;

    invoke-direct {v0, p0}, Lcom/android/settings/device/a;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSy:Lcom/android/settings/device/a;

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSy:Lcom/android/settings/device/a;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/android/settings/device/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "baseband_version"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_2
    const-string/jumbo v0, "kernel_version"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-static {}, Lcom/android/settings/device/h;->aDQ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    const-string/jumbo v0, "hardware_version"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    const-string/jumbo v2, "ro.miui.cust_hardware"

    const-string/jumbo v3, ""

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_2
    :goto_3
    const-string/jumbo v0, "wifi_type_approval"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f121622

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_4
    const-string/jumbo v0, "device_name"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSt:Lmiui/preference/ValuePreference;

    invoke-static {}, Lcom/android/settings/device/h;->aDR()Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "pre_installed_application"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v3, "security_patch"

    invoke-virtual {p0, v3}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v0, "baseband_version"

    const-string/jumbo v2, "gsm.version.baseband"

    invoke-direct {p0, v0, v2}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aDe(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    const v2, 0x7f121623

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setTitle(I)V

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    goto :goto_4
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x104000a

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0d0253

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSy:Lcom/android/settings/device/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSy:Lcom/android/settings/device/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/device/a;->cancel(Z)Z

    :cond_0
    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 11

    const/4 v10, 0x4

    const/4 v1, 0x0

    const/4 v9, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v0, "device_total_memory"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "device_cpu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "kernel_version"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {}, Lcom/android/settings/device/h;->aDS()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_0
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSv:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSv:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSw:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0xbb8

    cmp-long v0, v4, v6

    if-lez v0, :cond_9

    iget v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSx:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSx:I

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_1
    iget v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSx:I

    if-lez v0, :cond_3

    const v0, 0x7f100039

    const-string/jumbo v4, "device_cpu"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const v0, 0x7f10003a

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget v6, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSx:I

    new-array v7, v9, [Ljava/lang/Object;

    iget v8, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSx:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v0, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_3
    iget v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSx:I

    if-gtz v0, :cond_4

    const-string/jumbo v0, "device_total_memory"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string/jumbo v0, "4636"

    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "android.provider.Telephony.SECRET_CODE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "android_secret_code://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v0, 0x1000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    iput v10, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSx:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSw:J

    :cond_4
    :goto_2
    const-string/jumbo v0, "firmware_version"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSu:[J

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSu:[J

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSu:[J

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v9, v1, v3, v2}, Ljava/lang/System;->arraycopy([JI[JII)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSu:[J

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSu:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    aput-wide v4, v0, v1

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSu:[J

    aget-wide v0, v0, v3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1f4

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android"

    const-class v2, Lcom/android/internal/app/PlatLogoActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_3
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSv:Ljava/lang/String;

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_6
    const-string/jumbo v4, "kernel_version"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const v0, 0x7f100037

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v0, "device_cpu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "284"

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v0, "kernel_version"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    const-string/jumbo v0, "6484"

    goto/16 :goto_1

    :cond_9
    const v0, 0x7f1208f7

    const-string/jumbo v1, "kernel_version"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const v0, 0x7f1208f6

    :cond_a
    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    :cond_b
    const-string/jumbo v1, "device_cpu"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :cond_c
    iput v10, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSx:I

    goto/16 :goto_2

    :catch_0
    move-exception v1

    const-string/jumbo v1, "MiuiMyDeviceDetailSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to start activity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_d
    const-string/jumbo v0, "device_miui_version"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    if-lez v0, :cond_11

    iget v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    iget v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    if-nez v0, :cond_f

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "development"

    invoke-virtual {v0, v1, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "show"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_e
    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f121065

    invoke-static {v0, v1, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "com.android.settings.action.DEV_OPEN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "show"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_3

    :cond_f
    iget v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    if-lez v0, :cond_5

    iget v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_10
    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    new-array v4, v9, [Ljava/lang/Object;

    iget v5, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    const v5, 0x7f100038

    invoke-virtual {v1, v5, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    :cond_11
    iget v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    if-gez v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_12
    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f121064

    invoke-static {v0, v1, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    :cond_13
    const-string/jumbo v0, "wifi_type_approval"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {p0, v9}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->alW(I)V

    goto/16 :goto_3

    :cond_14
    const-string/jumbo v0, "device_name"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, ":miui:starting_window_label"

    const-string/jumbo v1, ""

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-class v0, Lcom/android/settings/MiuiDeviceNameEditFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p0

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    return v9

    :cond_15
    const-string/jumbo v0, "status_info"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_16

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/settings/Settings$PadStatusActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSv:Ljava/lang/String;

    return v9

    :cond_16
    const-string/jumbo v0, "pre_installed_application"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.VIEW_PRE_INSTALLED_APPLICATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/dc;->bYt(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->startActivity(Landroid/content/Intent;)V

    :cond_17
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSv:Ljava/lang/String;

    return v9

    :cond_18
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public onResume()V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/provider/MiuiSettings$System;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSt:Lmiui/preference/ValuePreference;

    invoke-virtual {v3, v2}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSt:Lmiui/preference/ValuePreference;

    invoke-virtual {v2, v0}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSt:Lmiui/preference/ValuePreference;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Lmiui/preference/ValuePreference;->setEnabled(Z)V

    const-string/jumbo v0, "device_total_memory"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/device/h;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/device/h;->aDT()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    const-string/jumbo v0, "device_available_memory"

    invoke-virtual {p0, v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/device/h;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/device/h;->aDU()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v2, "development"

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v2, "show"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    :goto_1
    iput v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSr:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->aSs:Landroid/widget/Toast;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x7

    goto :goto_1
.end method
