.class final Lcom/android/settings/jR;
.super Ljava/lang/Object;
.source "MiuiSecurityTrustedCredentials.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic cqk:Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;

.field final synthetic cql:Lcom/android/settings/bX;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;Lcom/android/settings/bX;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/jR;->cqk:Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;

    iput-object p2, p0, Lcom/android/settings/jR;->cql:Lcom/android/settings/bX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    new-instance v0, Lmiui/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/jR;->cqk:Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/settings/jR;->cql:Lcom/android/settings/bX;

    invoke-static {v1}, Lcom/android/settings/bX;->bPB(Lcom/android/settings/bX;)Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/jR;->cql:Lcom/android/settings/bX;

    invoke-static {v1, v2}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPi(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Lcom/android/settings/bX;)I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/jS;

    iget-object v2, p0, Lcom/android/settings/jR;->cql:Lcom/android/settings/bX;

    invoke-direct {v1, p0, v2, p1}, Lcom/android/settings/jS;-><init>(Lcom/android/settings/jR;Lcom/android/settings/bX;Landroid/content/DialogInterface;)V

    const v2, 0x1040013

    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/jT;

    invoke-direct {v1, p0}, Lcom/android/settings/jT;-><init>(Lcom/android/settings/jR;)V

    const v2, 0x1040009

    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->show()V

    return-void
.end method
