.class Lcom/android/settings/y;
.super Landroid/app/AlertDialog$Builder;
.source "TrustedCredentialsDialogBuilder.java"


# instance fields
.field private final bvW:Lcom/android/settings/A;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/android/settings/z;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/A;

    invoke-direct {v0, p1, p2}, Lcom/android/settings/A;-><init>(Landroid/app/Activity;Lcom/android/settings/z;)V

    iput-object v0, p0, Lcom/android/settings/y;->bvW:Lcom/android/settings/A;

    invoke-direct {p0}, Lcom/android/settings/y;->bjy()V

    return-void
.end method

.method private bjy()V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x10405f8

    invoke-virtual {p0, v0}, Lcom/android/settings/y;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/android/settings/y;->bvW:Lcom/android/settings/A;

    invoke-static {v0}, Lcom/android/settings/A;->bjV(Lcom/android/settings/A;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/y;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f12129b

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/y;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v0, 0x104000a

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/y;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method


# virtual methods
.method public bjw([Lcom/android/settings/p;)Lcom/android/settings/y;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/y;->bvW:Lcom/android/settings/A;

    invoke-virtual {v0, p1}, Lcom/android/settings/A;->bjL([Lcom/android/settings/p;)V

    return-object p0
.end method

.method public bjx(Lcom/android/settings/p;)Lcom/android/settings/y;
    .locals 2

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-array v0, v1, [Lcom/android/settings/p;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/y;->bjw([Lcom/android/settings/p;)Lcom/android/settings/y;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/android/settings/p;

    aput-object p1, v0, v1

    goto :goto_0
.end method

.method public create()Landroid/app/AlertDialog;
    .locals 2

    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/y;->bvW:Lcom/android/settings/A;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    iget-object v1, p0, Lcom/android/settings/y;->bvW:Lcom/android/settings/A;

    invoke-virtual {v1, v0}, Lcom/android/settings/A;->bjM(Landroid/app/AlertDialog;)V

    return-object v0
.end method
