.class Lcom/android/settings/MiuiConfirmLockPasswordInstall$ApplyRomFile;
.super Ljava/io/File;
.source "MiuiConfirmLockPasswordInstall.java"


# instance fields
.field final synthetic this$0:Lcom/android/settings/MiuiConfirmLockPasswordInstall;


# direct methods
.method public constructor <init>(Lcom/android/settings/MiuiConfirmLockPasswordInstall;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiConfirmLockPasswordInstall$ApplyRomFile;->this$0:Lcom/android/settings/MiuiConfirmLockPasswordInstall;

    invoke-direct {p0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getCanonicalPath()Ljava/lang/String;
    .locals 3

    invoke-super {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "/mnt"

    const-string/jumbo v1, "/mnt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    const-string/jumbo v1, "/mnt"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string/jumbo v1, "/storage/emulated/0/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "/storage/emulated/0/"

    const-string/jumbo v2, "/data/media/0/"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string/jumbo v1, "/storage/sdcard0"

    const-string/jumbo v2, "sdcard"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
