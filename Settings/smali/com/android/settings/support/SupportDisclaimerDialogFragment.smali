.class public final Lcom/android/settings/support/SupportDisclaimerDialogFragment;
.super Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;
.source "SupportDisclaimerDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;-><init>()V

    return-void
.end method

.method public static aST(Landroid/accounts/Account;I)Lcom/android/settings/support/SupportDisclaimerDialogFragment;
    .locals 3

    new-instance v0, Lcom/android/settings/support/SupportDisclaimerDialogFragment;

    invoke-direct {v0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "extra_account"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v2, "extra_type"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static aSU(Landroid/text/Spannable;)V
    .locals 7

    const/4 v1, 0x0

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v2, Landroid/text/style/URLSpan;

    invoke-interface {p0, v1, v0, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-interface {p0, v3}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {p0, v3}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-interface {p0, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    new-instance v6, Lcom/android/settings/support/SupportDisclaimerDialogFragment$NoUnderlineUrlSpan;

    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Lcom/android/settings/support/SupportDisclaimerDialogFragment$NoUnderlineUrlSpan;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x21

    invoke-interface {p0, v6, v4, v5, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x20e

    return v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    iget-object v0, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/util/Pair;

    const/16 v3, 0x1e3

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    const/4 v6, 0x0

    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    invoke-virtual {p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v6, [Landroid/util/Pair;

    const/16 v3, 0x1e3

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v2, 0x7f0a044a

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/settings/overlay/a;->aIw(Landroid/content/Context;)Lcom/android/settings/overlay/b;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    xor-int/lit8 v4, v0, 0x1

    invoke-interface {v2, v3, v4}, Lcom/android/settings/overlay/b;->aIA(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    new-array v4, v6, [Landroid/util/Pair;

    const/16 v5, 0x2f8

    invoke-virtual {v0, v1, v5, v4}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    new-array v4, v6, [Landroid/util/Pair;

    const/16 v5, 0x1e4

    invoke-virtual {v0, v1, v5, v4}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    invoke-virtual {p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v0, "extra_account"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    const-string/jumbo v4, "extra_type"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v1, v0, v3}, Lcom/android/settings/overlay/b;->aIB(Landroid/app/Activity;Landroid/accounts/Account;I)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1211f3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0d01f6

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a044b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/settings/overlay/a;->aIw(Landroid/content/Context;)Lcom/android/settings/overlay/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/settings/overlay/b;->aIz()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment;->aSU(Landroid/text/Spannable;)V

    invoke-static {v0, p0}, Lcom/android/settings/support/SupportDisclaimerDialogFragment$SystemInformationSpan;->aSV(Landroid/text/Spannable;Landroid/app/DialogFragment;)Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
