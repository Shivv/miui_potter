.class public Lcom/android/settings/VerticalSeekBar;
.super Landroid/widget/AbsSeekBar;
.source "VerticalSeekBar.java"


# instance fields
.field private bFT:Landroid/graphics/Rect;

.field private bFU:Lcom/android/settings/aK;

.field private final bFV:Ljava/lang/reflect/Method;

.field private bFW:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/VerticalSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/VerticalSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AbsSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFT:Landroid/graphics/Rect;

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    const-string/jumbo v2, "setProgress"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFV:Ljava/lang/reflect/Method;

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private bvQ()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    return-void
.end method

.method private bvT(Landroid/view/MotionEvent;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFW:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFW:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getHeight()I

    move-result v2

    sub-int v3, v2, v0

    iget v4, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingBottom:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingTop:I

    sub-int/2addr v3, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getPaddingBottom()I

    move-result v5

    sub-int v5, v2, v5

    if-le v4, v5, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getMax()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/android/settings/VerticalSeekBar;->setProgressOnMove(F)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getPaddingTop()I

    move-result v1

    if-ge v4, v1, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_2
    iget v1, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingBottom:I

    sub-int v1, v2, v1

    sub-int/2addr v1, v4

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    int-to-float v0, v0

    int-to-float v1, v3

    div-float/2addr v0, v1

    goto :goto_1
.end method

.method private setProgressOnMove(F)V
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getProgress()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/VerticalSeekBar;->bFV:Ljava/lang/reflect/Method;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    float-to-int v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getProgress()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFU:Lcom/android/settings/aK;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFU:Lcom/android/settings/aK;

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getProgress()I

    move-result v1

    invoke-interface {v0, p0, v1, v5}, Lcom/android/settings/aK;->bvU(Lcom/android/settings/VerticalSeekBar;IZ)V

    :cond_0
    return-void

    :catch_0
    move-exception v1

    float-to-int v1, p1

    invoke-virtual {p0, v1}, Lcom/android/settings/VerticalSeekBar;->setProgress(I)V

    goto :goto_0
.end method


# virtual methods
.method bvR()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFU:Lcom/android/settings/aK;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFU:Lcom/android/settings/aK;

    invoke-interface {v0, p0}, Lcom/android/settings/aK;->bvV(Lcom/android/settings/VerticalSeekBar;)V

    :cond_0
    return-void
.end method

.method bvS()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFU:Lcom/android/settings/aK;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFU:Lcom/android/settings/aK;

    invoke-interface {v0, p0}, Lcom/android/settings/aK;->bvW(Lcom/android/settings/VerticalSeekBar;)V

    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0, v1, v1}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    move-result v0

    return v0

    :pswitch_0
    new-instance v0, Landroid/view/KeyEvent;

    const/16 v2, 0x16

    invoke-direct {v0, v3, v2}, Landroid/view/KeyEvent;-><init>(II)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/view/KeyEvent;

    const/16 v2, 0x15

    invoke-direct {v0, v3, v2}, Landroid/view/KeyEvent;-><init>(II)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/view/KeyEvent;

    const/16 v2, 0x14

    invoke-direct {v0, v3, v2}, Landroid/view/KeyEvent;-><init>(II)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Landroid/view/KeyEvent;

    const/16 v2, 0x13

    invoke-direct {v0, v3, v2}, Landroid/view/KeyEvent;-><init>(II)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/AbsSeekBar;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/VerticalSeekBar;->bFT:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/VerticalSeekBar;->bFW:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    sub-int v6, v1, v5

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getProgress()I

    move-result v7

    mul-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getMax()I

    move-result v7

    div-int/2addr v6, v7

    sub-int v6, v1, v6

    iget v7, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingLeft:I

    sub-int v5, v6, v5

    iget v8, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingLeft:I

    add-int/2addr v4, v8

    invoke-virtual {v3, v7, v5, v4, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    const/16 v4, 0x2710

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    iget v4, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingBottom:I

    sub-int v4, v1, v4

    iget v5, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingTop:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingLeft:I

    iget v6, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingBottom:I

    sub-int v6, v1, v6

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getProgress()I

    move-result v7

    mul-int/2addr v4, v7

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getMax()I

    move-result v7

    div-int/2addr v4, v7

    sub-int v4, v6, v4

    iget v6, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingRight:I

    sub-int/2addr v2, v6

    iget v6, p0, Lcom/android/settings/VerticalSeekBar;->mPaddingBottom:I

    sub-int/2addr v1, v6

    invoke-virtual {v3, v5, v4, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onMeasure(II)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/VerticalSeekBar;->setMeasuredDimension(II)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No background!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p2, p1, p3, p4}, Landroid/widget/AbsSeekBar;->onSizeChanged(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v2

    :pswitch_0
    invoke-virtual {p0, v2}, Lcom/android/settings/VerticalSeekBar;->setPressed(Z)V

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->bvR()V

    invoke-direct {p0, p1}, Lcom/android/settings/VerticalSeekBar;->bvT(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/settings/VerticalSeekBar;->bvT(Landroid/view/MotionEvent;)V

    invoke-direct {p0}, Lcom/android/settings/VerticalSeekBar;->bvQ()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/settings/VerticalSeekBar;->bvT(Landroid/view/MotionEvent;)V

    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->bvS()V

    invoke-virtual {p0, v1}, Lcom/android/settings/VerticalSeekBar;->setPressed(Z)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/android/settings/VerticalSeekBar;->bvS()V

    invoke-virtual {p0, v1}, Lcom/android/settings/VerticalSeekBar;->setPressed(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setOnSeekBarChangeListener(Lcom/android/settings/aK;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/VerticalSeekBar;->bFU:Lcom/android/settings/aK;

    return-void
.end method

.method public setThumb(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/VerticalSeekBar;->bFW:Landroid/graphics/drawable/Drawable;

    invoke-super {p0, p1}, Landroid/widget/AbsSeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
