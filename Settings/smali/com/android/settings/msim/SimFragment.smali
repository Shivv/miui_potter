.class public Lcom/android/settings/msim/SimFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "SimFragment.java"


# static fields
.field private static final Hz:[Ljava/lang/String;


# instance fields
.field private HA:Landroid/content/BroadcastReceiver;

.field private HB:Lcom/android/internal/telephony/Phone;

.field private HC:Landroid/content/res/Resources;

.field private HD:Z

.field private HE:Landroid/preference/Preference;

.field private HF:I

.field private HG:Ljava/lang/String;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "data_state"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "service_state"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "operator_name"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string/jumbo v1, "roaming_state"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string/jumbo v1, "network_type"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string/jumbo v1, "latest_area_info"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "number"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string/jumbo v1, "signal_strength"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/msim/SimFragment;->Hz:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/msim/SimFragment;->HB:Lcom/android/internal/telephony/Phone;

    new-instance v0, Lcom/android/settings/msim/c;

    invoke-direct {v0, p0}, Lcom/android/settings/msim/c;-><init>(Lcom/android/settings/msim/SimFragment;)V

    iput-object v0, p0, Lcom/android/settings/msim/SimFragment;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/android/settings/msim/d;

    invoke-direct {v0, p0}, Lcom/android/settings/msim/d;-><init>(Lcom/android/settings/msim/SimFragment;)V

    iput-object v0, p0, Lcom/android/settings/msim/SimFragment;->HA:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private Ai(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settings/msim/SimFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method private Aj(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lcom/android/settings/msim/SimFragment;->HG:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/msim/SimFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/settings/msim/SimFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {v0, p2}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private Ak(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string/jumbo v0, "latest_area_info"

    invoke-direct {p0, v0, p1}, Lcom/android/settings/msim/SimFragment;->Aj(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private Al()V
    .locals 4

    const v3, 0x7f120db4

    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/msim/SimFragment;->HF:I

    invoke-virtual {v1, v2}, Lmiui/telephony/TelephonyManager;->getDataStateForSlot(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    const-string/jumbo v1, "data_state"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/msim/SimFragment;->Aj(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v1, 0x7f120db2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v1, 0x7f120db5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v1, 0x7f120db3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private Am()V
    .locals 2

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/msim/SimFragment;->HF:I

    invoke-virtual {v0, v1}, Lmiui/telephony/TelephonyManager;->getNetworkTypeForSlot(I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/telephony/TelephonyManagerEx;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string/jumbo v1, "network_type"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/msim/SimFragment;->Aj(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HB:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "CDMA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "GSM"

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "CDMA"

    goto :goto_0
.end method

.method private An(Landroid/telephony/ServiceState;)V
    .locals 3

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v2, 0x7f120dc8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    packed-switch v1, :pswitch_data_0

    :goto_0
    const-string/jumbo v1, "service_state"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/msim/SimFragment;->Aj(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "roaming_state"

    iget-object v1, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v2, 0x7f120dc2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/SimFragment;->Aj(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string/jumbo v0, "operator_name"

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getOperatorAlphaLong()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/SimFragment;->Aj(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v1, 0x7f120dc5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v1, 0x7f120dc7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v1, 0x7f120dc6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "roaming_state"

    iget-object v1, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v2, 0x7f120dc3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/SimFragment;->Aj(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic Ap(Lcom/android/settings/msim/SimFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/msim/SimFragment;->Ak(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic Aq(Lcom/android/settings/msim/SimFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/msim/SimFragment;->Al()V

    return-void
.end method

.method static synthetic Ar(Lcom/android/settings/msim/SimFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/msim/SimFragment;->Am()V

    return-void
.end method

.method static synthetic As(Lcom/android/settings/msim/SimFragment;Landroid/telephony/ServiceState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/msim/SimFragment;->An(Landroid/telephony/ServiceState;)V

    return-void
.end method


# virtual methods
.method Ao(Landroid/telephony/SignalStrength;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HE:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HB:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v1, 0x1

    if-eq v1, v0, :cond_0

    const/4 v1, 0x3

    if-ne v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HE:Landroid/preference/Preference;

    check-cast v0, Lmiui/preference/ValuePreference;

    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getDbm()I

    move-result v0

    if-ne v4, v0, :cond_4

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getAsuLevel()I

    move-result v0

    if-ne v4, v0, :cond_3

    :goto_1
    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HE:Landroid/preference/Preference;

    check-cast v0, Lmiui/preference/ValuePreference;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v4, 0x7f120db8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "   "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f120db6

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget v2, Lmiui/R$style;->Theme_Light_Settings_NoTitle:I

    invoke-virtual {p0, v2}, Lcom/android/settings/msim/SimFragment;->setThemeRes(I)V

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2, v1}, Lmiui/telephony/SubscriptionManager;->getSlotId(Landroid/os/Bundle;I)I

    move-result v2

    iput v2, p0, Lcom/android/settings/msim/SimFragment;->HF:I

    const v2, 0x7f1500ec

    invoke-virtual {p0, v2}, Lcom/android/settings/msim/SimFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/android/settings/msim/SimFragment;->HC:Landroid/content/res/Resources;

    const v3, 0x7f120594

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/msim/SimFragment;->HG:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v2

    iget v3, p0, Lcom/android/settings/msim/SimFragment;->HF:I

    invoke-virtual {v2, v3}, Lcom/android/settings/dc;->bsb(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/msim/SimFragment;->HB:Lcom/android/internal/telephony/Phone;

    :cond_0
    const-string/jumbo v2, "signal_strength"

    invoke-virtual {p0, v2}, Lcom/android/settings/msim/SimFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/msim/SimFragment;->HE:Landroid/preference/Preference;

    iget-object v2, p0, Lcom/android/settings/msim/SimFragment;->HB:Lcom/android/internal/telephony/Phone;

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    sget-object v2, Lcom/android/settings/msim/SimFragment;->Hz:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_5

    aget-object v1, v2, v0

    invoke-direct {p0, v1}, Lcom/android/settings/msim/SimFragment;->Ai(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/msim/SimFragment;->HB:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "CDMA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "br"

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v2

    iget v3, p0, Lcom/android/settings/msim/SimFragment;->HF:I

    invoke-virtual {v2, v3}, Lmiui/telephony/TelephonyManager;->getSimCountryIsoForSlot(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/msim/SimFragment;->HD:Z

    :cond_3
    iget-object v1, p0, Lcom/android/settings/msim/SimFragment;->HB:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    const-string/jumbo v1, "number"

    invoke-direct {p0, v1, v0}, Lcom/android/settings/msim/SimFragment;->Aj(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/settings/msim/SimFragment;->HD:Z

    if-nez v0, :cond_5

    const-string/jumbo v0, "latest_area_info"

    invoke-direct {p0, v0}, Lcom/android/settings/msim/SimFragment;->Ai(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public onPause()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HB:Lcom/android/internal/telephony/Phone;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/msim/SimFragment;->HF:I

    iget-object v2, p0, Lcom/android/settings/msim/SimFragment;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiui/telephony/TelephonyManager;->listenForSlot(ILandroid/telephony/PhoneStateListener;I)V

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/msim/SimFragment;->HD:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/msim/SimFragment;->HA:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/msim/SimFragment;->HB:Lcom/android/internal/telephony/Phone;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/msim/SimFragment;->HF:I

    iget-object v2, p0, Lcom/android/settings/msim/SimFragment;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x141

    invoke-virtual {v0, v1, v2, v3}, Lmiui/telephony/TelephonyManager;->listenForSlot(ILandroid/telephony/PhoneStateListener;I)V

    iget-boolean v0, p0, Lcom/android/settings/msim/SimFragment;->HD:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/msim/SimFragment;->HA:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.cellbroadcastreceiver.CB_AREA_INFO_RECEIVED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "android.permission.RECEIVE_EMERGENCY_BROADCAST"

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.cellbroadcastreceiver.GET_LATEST_CB_AREA_INFO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/msim/SimFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string/jumbo v3, "android.permission.RECEIVE_EMERGENCY_BROADCAST"

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/Activity;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
