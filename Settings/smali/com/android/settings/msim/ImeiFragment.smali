.class public Lcom/android/settings/msim/ImeiFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "ImeiFragment.java"


# static fields
.field private static final Ht:[Ljava/lang/String;


# instance fields
.field private Hu:Lcom/android/internal/telephony/Phone;

.field private Hv:Landroid/content/res/Resources;

.field private Hw:I

.field private Hx:Landroid/telephony/TelephonyManager;

.field private Hy:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "imei"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "imei_sv"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "prl_version"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string/jumbo v1, "min_number"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string/jumbo v1, "meid_number"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string/jumbo v1, "icc_id"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/msim/ImeiFragment;->Ht:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    return-void
.end method

.method private Ag(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settings/msim/ImeiFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/msim/ImeiFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method private Ah(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lcom/android/settings/msim/ImeiFragment;->Hy:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/msim/ImeiFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/settings/msim/ImeiFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {v0, p2}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget v1, Lmiui/R$style;->Theme_Light_Settings_NoTitle:I

    invoke-virtual {p0, v1}, Lcom/android/settings/msim/ImeiFragment;->setThemeRes(I)V

    invoke-virtual {p0}, Lcom/android/settings/msim/ImeiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1, v0}, Lmiui/telephony/SubscriptionManager;->getSlotId(Landroid/os/Bundle;I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hw:I

    invoke-virtual {p0}, Lcom/android/settings/msim/ImeiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hx:Landroid/telephony/TelephonyManager;

    const v1, 0x7f1500eb

    invoke-virtual {p0, v1}, Lcom/android/settings/msim/ImeiFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/msim/ImeiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hv:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hv:Landroid/content/res/Resources;

    const v2, 0x7f120594

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hy:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/msim/ImeiFragment;->Hw:I

    invoke-virtual {v1, v2}, Lcom/android/settings/dc;->bsb(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    :cond_0
    iget-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/msim/ImeiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    sget-object v1, Lcom/android/settings/msim/ImeiFragment;->Ht:[Ljava/lang/String;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    invoke-direct {p0, v3}, Lcom/android/settings/msim/ImeiFragment;->Ag(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "CDMA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "meid_number"

    iget-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getMeid()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/ImeiFragment;->Ah(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "min_number"

    iget-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getCdmaMin()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/ImeiFragment;->Ah(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/msim/ImeiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "min_number"

    invoke-virtual {p0, v0}, Lcom/android/settings/msim/ImeiFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const v1, 0x7f121158

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    :cond_3
    const-string/jumbo v0, "prl_version"

    iget-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getCdmaPrlVersion()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/ImeiFragment;->Ah(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "imei_sv"

    invoke-direct {p0, v0}, Lcom/android/settings/msim/ImeiFragment;->Ag(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getLteOnCdmaMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    const-string/jumbo v0, "icc_id"

    iget-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getIccSerialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/ImeiFragment;->Ah(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "imei"

    iget-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getImei()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/ImeiFragment;->Ah(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    const-string/jumbo v0, "imei"

    invoke-direct {p0, v0}, Lcom/android/settings/msim/ImeiFragment;->Ag(Ljava/lang/String;)V

    const-string/jumbo v0, "icc_id"

    invoke-direct {p0, v0}, Lcom/android/settings/msim/ImeiFragment;->Ag(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    const-string/jumbo v0, "imei"

    iget-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hu:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/ImeiFragment;->Ah(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "imei_sv"

    iget-object v1, p0, Lcom/android/settings/msim/ImeiFragment;->Hx:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceSoftwareVersion()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/msim/ImeiFragment;->Ah(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "prl_version"

    invoke-direct {p0, v0}, Lcom/android/settings/msim/ImeiFragment;->Ag(Ljava/lang/String;)V

    const-string/jumbo v0, "meid_number"

    invoke-direct {p0, v0}, Lcom/android/settings/msim/ImeiFragment;->Ag(Ljava/lang/String;)V

    const-string/jumbo v0, "min_number"

    invoke-direct {p0, v0}, Lcom/android/settings/msim/ImeiFragment;->Ag(Ljava/lang/String;)V

    const-string/jumbo v0, "icc_id"

    invoke-direct {p0, v0}, Lcom/android/settings/msim/ImeiFragment;->Ag(Ljava/lang/String;)V

    goto :goto_1
.end method
