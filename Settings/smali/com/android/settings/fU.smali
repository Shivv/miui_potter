.class final Lcom/android/settings/fU;
.super Ljava/lang/Object;
.source "MiuiKeyguardSettingsUtils.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic clb:Landroid/app/Activity;

.field final synthetic clc:Ljava/lang/String;

.field final synthetic cld:Landroid/security/MiuiLockPatternUtils;

.field final synthetic cle:Landroid/content/Intent;


# direct methods
.method constructor <init>(Landroid/app/Activity;Ljava/lang/String;Landroid/security/MiuiLockPatternUtils;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fU;->clb:Landroid/app/Activity;

    iput-object p2, p0, Lcom/android/settings/fU;->clc:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/fU;->cld:Landroid/security/MiuiLockPatternUtils;

    iput-object p4, p0, Lcom/android/settings/fU;->cle:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 6

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    if-eqz v0, :cond_3

    const-string/jumbo v1, "booleanResult"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_2

    new-instance v0, Lcom/android/settings/bM;

    iget-object v1, p0, Lcom/android/settings/fU;->clb:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/bM;->bNe()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/settings/bM;->bNd()Z

    move-result v2

    iget-object v3, p0, Lcom/android/settings/fU;->clc:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/fU;->clb:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/fU;->clc:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    iget-object v3, p0, Lcom/android/settings/fU;->cld:Landroid/security/MiuiLockPatternUtils;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/android/settings/bn;->bFj(Lcom/android/internal/widget/LockPatternUtils;IZ)V

    iget-object v3, p0, Lcom/android/settings/fU;->cld:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v3}, Landroid/security/MiuiLockPatternUtils;->clearLockoutAttemptDeadline()V

    iget-object v3, p0, Lcom/android/settings/fU;->clb:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "face_unlock_has_feature"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/MiuiSettings$Secure;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/fU;->clb:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "face_unlock_has_feature"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/MiuiSettings$Secure;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    iget-object v3, p0, Lcom/android/settings/fU;->clb:Landroid/app/Activity;

    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "face_unlock_release"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_1
    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    new-instance v2, Lcom/android/settings/fV;

    iget-object v3, p0, Lcom/android/settings/fU;->clb:Landroid/app/Activity;

    iget-object v4, p0, Lcom/android/settings/fU;->cle:Landroid/content/Intent;

    invoke-direct {v2, p0, v3, v4}, Lcom/android/settings/fV;-><init>(Lcom/android/settings/fU;Landroid/app/Activity;Landroid/content/Intent;)V

    invoke-static {v1, v0, v2}, Lcom/android/settings/bn;->bFK(Ljava/util/List;Lcom/android/settings/bM;Lcom/android/settings/bb;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/fU;->clb:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    iget-object v0, p0, Lcom/android/settings/fU;->cle:Landroid/content/Intent;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/fU;->clb:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/settings/fU;->cle:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return-void
.end method
