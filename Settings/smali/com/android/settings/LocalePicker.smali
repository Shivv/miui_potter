.class public Lcom/android/settings/LocalePicker;
.super Lcom/android/settings/BaseListFragment;
.source "LocalePicker.java"

# interfaces
.implements Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;
.implements Lcom/android/settings/d;


# instance fields
.field private cbB:Lcom/android/settings/LocalePicker$MyDialogFragment;

.field cbC:Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;

.field private cbD:Ljava/util/Locale;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BaseListFragment;-><init>()V

    invoke-virtual {p0, p0}, Lcom/android/settings/LocalePicker;->bTD(Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;)V

    return-void
.end method

.method static synthetic bTG(Lcom/android/settings/LocalePicker;)Ljava/util/Locale;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LocalePicker;->cbD:Ljava/util/Locale;

    return-object v0
.end method


# virtual methods
.method public Lx(I)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bTD(Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/LocalePicker;->cbC:Lcom/android/internal/app/LocalePicker$LocaleSelectionListener;

    return-void
.end method

.method protected bTE(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/LocalePicker;->cbB:Lcom/android/settings/LocalePicker$MyDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/LocalePicker;->cbB:Lcom/android/settings/LocalePicker$MyDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/LocalePicker$MyDialogFragment;->bXi()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/LocalePicker;->cbB:Lcom/android/settings/LocalePicker$MyDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/LocalePicker$MyDialogFragment;->dismiss()V

    :cond_0
    iput-object v1, p0, Lcom/android/settings/LocalePicker;->cbB:Lcom/android/settings/LocalePicker$MyDialogFragment;

    return-void
.end method

.method protected bTF(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/LocalePicker;->cbB:Lcom/android/settings/LocalePicker$MyDialogFragment;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "LocalePicker"

    const-string/jumbo v1, "Old dialog fragment not null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/settings/LocalePicker$MyDialogFragment;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/LocalePicker$MyDialogFragment;-><init>(Lcom/android/settings/d;I)V

    iput-object v0, p0, Lcom/android/settings/LocalePicker;->cbB:Lcom/android/settings/LocalePicker$MyDialogFragment;

    iget-object v0, p0, Lcom/android/settings/LocalePicker;->cbB:Lcom/android/settings/LocalePicker$MyDialogFragment;

    invoke-virtual {p0}, Lcom/android/settings/LocalePicker;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/LocalePicker$MyDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "locale"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "locale"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iput-object v0, p0, Lcom/android/settings/LocalePicker;->cbD:Ljava/util/Locale;

    :cond_0
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/LocalePicker;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/android/settings/kx;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/kx;-><init>(Lcom/android/settings/LocalePicker;I)V

    const v2, 0x7f1207cd

    invoke-static {v0, v2, v1}, Lcom/android/settings/aq;->brb(Landroid/content/Context;ILjava/lang/Runnable;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/BaseListFragment;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-static {p2, v1, v0, v2}, Lcom/android/settings/aq;->brg(Landroid/view/ViewGroup;Landroid/view/View;Landroid/widget/ListView;Z)V

    return-object v1
.end method

.method public onLocaleSelected(Ljava/util/Locale;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/LocalePicker;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bry(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/android/settings/LocalePicker;->cbD:Ljava/util/Locale;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/LocalePicker;->bTF(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/LocalePicker;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    invoke-static {p1}, Lcom/android/internal/app/LocalePicker;->updateLocale(Ljava/util/Locale;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/LocalePicker;->cbD:Ljava/util/Locale;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "locale"

    iget-object v1, p0, Lcom/android/settings/LocalePicker;->cbD:Ljava/util/Locale;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-void
.end method
