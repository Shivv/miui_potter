.class public Lcom/android/settings/datetime/c;
.super Ljava/lang/Object;
.source "ZonePickerHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final aBI:Ljava/text/Collator;

.field final synthetic aBJ:Lcom/android/settings/datetime/a;


# direct methods
.method public constructor <init>(Lcom/android/settings/datetime/a;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/datetime/c;->aBJ:Lcom/android/settings/datetime/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datetime/c;->aBI:Ljava/text/Collator;

    return-void
.end method


# virtual methods
.method public apv(Lcom/android/settings/datetime/n;Lcom/android/settings/datetime/n;)I
    .locals 3

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_1
    if-nez p2, :cond_2

    const/4 v0, 0x1

    return v0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/datetime/c;->aBI:Ljava/text/Collator;

    invoke-virtual {p1}, Lcom/android/settings/datetime/n;->apL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/settings/datetime/n;->apL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/datetime/n;

    check-cast p2, Lcom/android/settings/datetime/n;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/datetime/c;->apv(Lcom/android/settings/datetime/n;Lcom/android/settings/datetime/n;)I

    move-result v0

    return v0
.end method
