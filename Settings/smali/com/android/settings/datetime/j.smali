.class public Lcom/android/settings/datetime/j;
.super Lcom/android/settings/core/e;
.source "TimeZonePreferenceController.java"


# instance fields
.field private final aBS:Lcom/android/settings/datetime/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/datetime/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/datetime/j;->aBS:Lcom/android/settings/datetime/d;

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 5

    instance-of v0, p1, Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lmiui/preference/ValuePreference;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    iget-object v4, p0, Lcom/android/settings/datetime/j;->mContext:Landroid/content/Context;

    invoke-static {v4, v1, v2, v3}, Lcom/android/settingslib/j/a;->cpr(Landroid/content/Context;Ljava/util/Locale;Ljava/util/TimeZone;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/icu/text/TimeZoneNames;->getInstance(Ljava/util/Locale;)Landroid/icu/text/TimeZoneNames;

    move-result-object v1

    invoke-static {v1, v2, v3}, Lcom/android/settingslib/j/a;->cpu(Landroid/icu/text/TimeZoneNames;Ljava/util/TimeZone;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/datetime/j;->aBS:Lcom/android/settings/datetime/d;

    invoke-virtual {v0}, Lcom/android/settings/datetime/d;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/datetime/j;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->isTimeChangeDisallow(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/datetime/j;->getTimeZoneOffsetAndName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method getTimeZoneOffsetAndName()Ljava/lang/CharSequence;
    .locals 3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/datetime/j;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/android/settingslib/j/a;->cpy(Landroid/content/Context;Ljava/util/TimeZone;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "timezone"

    return-object v0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
