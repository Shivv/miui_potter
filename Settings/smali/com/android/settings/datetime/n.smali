.class public Lcom/android/settings/datetime/n;
.super Ljava/lang/Object;
.source "TimeZoneObj.java"


# instance fields
.field private aBW:Ljava/lang/String;

.field private aBX:Ljava/lang/String;

.field private aBY:Ljava/lang/String;

.field private aBZ:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/datetime/n;->aBW:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/datetime/n;->aBX:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/datetime/n;->aBY:Ljava/lang/String;

    iput p4, p0, Lcom/android/settings/datetime/n;->aBZ:I

    return-void
.end method


# virtual methods
.method public apK()I
    .locals 1

    iget v0, p0, Lcom/android/settings/datetime/n;->aBZ:I

    return v0
.end method

.method public apL()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/n;->aBW:Ljava/lang/String;

    return-object v0
.end method

.method public apM()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/n;->aBX:Ljava/lang/String;

    return-object v0
.end method

.method public apN()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/n;->aBY:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, " timezoneID = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/datetime/n;->aBY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " cityName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/datetime/n;->aBW:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " gmtName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/datetime/n;->aBX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " offset = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/datetime/n;->aBZ:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
