.class public Lcom/android/settings/datetime/q;
.super Lcom/android/settings/core/e;
.source "TimeFormatPreferenceController.java"


# instance fields
.field private final aCn:Ljava/util/Calendar;

.field private final aCo:Z

.field private final aCp:Lcom/android/settings/datetime/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/datetime/g;Z)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-boolean p3, p0, Lcom/android/settings/datetime/q;->aCo:Z

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datetime/q;->aCn:Ljava/util/Calendar;

    iput-object p2, p0, Lcom/android/settings/datetime/q;->aCp:Lcom/android/settings/datetime/g;

    return-void
.end method

.method private aqa()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/q;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private aqb(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/datetime/q;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "time_12_24"

    if-eqz p1, :cond_0

    const-string/jumbo v0, "24"

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void

    :cond_0
    const-string/jumbo v0, "12"

    goto :goto_0
.end method

.method private aqc(Z)V
    .locals 3

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v0, "android.intent.action.TIME_SET"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v2, "android.intent.extra.TIME_PREF_24_HOUR_FORMAT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/datetime/q;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 7

    const/4 v5, 0x0

    instance-of v0, p1, Landroid/preference/TwoStatePreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    move-object v0, p1

    check-cast v0, Landroid/preference/TwoStatePreference;

    invoke-direct {p0}, Lcom/android/settings/datetime/q;->aqa()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/datetime/q;->aCn:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    iget-object v0, p0, Lcom/android/settings/datetime/q;->aCn:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0xb

    const/16 v3, 0x1f

    const/16 v4, 0xd

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    iget-object v0, p0, Lcom/android/settings/datetime/q;->aCn:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/datetime/q;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public fm(Landroid/preference/Preference;)Z
    .locals 2

    instance-of v0, p1, Landroid/preference/TwoStatePreference;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "24 hour"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    check-cast p1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/datetime/q;->aqb(Z)V

    invoke-direct {p0, v0}, Lcom/android/settings/datetime/q;->aqc(Z)V

    iget-object v0, p0, Lcom/android/settings/datetime/q;->aCp:Lcom/android/settings/datetime/g;

    iget-object v1, p0, Lcom/android/settings/datetime/q;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/android/settings/datetime/g;->apz(Landroid/content/Context;)V

    const/4 v0, 0x1

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "24 hour"

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/datetime/q;->aCo:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
