.class final Lcom/android/settings/datetime/r;
.super Ljava/lang/Object;
.source "MiuiZonePickerSettings.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic aCq:Lcom/android/settings/datetime/MiuiZonePickerSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/datetime/MiuiZonePickerSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/datetime/r;->aCq:Lcom/android/settings/datetime/MiuiZonePickerSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datetime/r;->aCq:Lcom/android/settings/datetime/MiuiZonePickerSettings;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apV(Lcom/android/settings/datetime/MiuiZonePickerSettings;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/datetime/r;->aCq:Lcom/android/settings/datetime/MiuiZonePickerSettings;

    iget-object v1, p0, Lcom/android/settings/datetime/r;->aCq:Lcom/android/settings/datetime/MiuiZonePickerSettings;

    invoke-static {v1}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->apS(Lcom/android/settings/datetime/MiuiZonePickerSettings;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/datetime/MiuiZonePickerSettings;->onQueryTextSubmit(Ljava/lang/String;)V

    return-void
.end method
