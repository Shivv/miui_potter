.class public Lcom/android/settings/datetime/p;
.super Lcom/android/settings/core/e;
.source "AutoTimePreferenceController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final aCm:Lcom/android/settings/datetime/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/datetime/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/datetime/p;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/datetime/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/datetime/p;->aCm:Lcom/android/settings/datetime/g;

    return-void
.end method

.method private apZ()Lcom/android/settingslib/n;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datetime/p;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/w;->cqX(Landroid/content/Context;)Lcom/android/settingslib/n;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 2

    instance-of v0, p1, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-direct {p0}, Lcom/android/settings/datetime/p;->apZ()Lcom/android/settingslib/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    move-object v0, p1

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p0}, Lcom/android/settings/datetime/p;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/datetime/p;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->isTimeChangeDisallow(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public isEnabled()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/datetime/p;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "auto_time"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "auto_time"

    return-object v0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sget-object v2, Lcom/android/settings/datetime/p;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "auto_time enabled changed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/settings/datetime/p;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "auto_time"

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/datetime/p;->aCm:Lcom/android/settings/datetime/g;

    iget-object v2, p0, Lcom/android/settings/datetime/p;->mContext:Landroid/content/Context;

    invoke-interface {v0, v2}, Lcom/android/settings/datetime/g;->apz(Landroid/content/Context;)V

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
