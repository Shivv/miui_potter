.class public Lcom/android/settings/display/ScreenColorPreference;
.super Landroid/preference/Preference;
.source "ScreenColorPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private TV:Lcom/android/settings/display/ScreenColorBitMapView;

.field private TW:Landroid/widget/TextView;

.field private TX:I

.field private TY:Landroid/widget/TextView;

.field private TZ:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/display/ScreenColorPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/display/ScreenColorPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const v0, 0x7f0d01a0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ScreenColorPreference;->setLayoutResource(I)V

    return-void
.end method

.method private Lp(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const v0, 0x7f060122

    :goto_0
    invoke-static {}, Landroid/app/AppGlobals;->getInitialApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Application;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v1, p0, Lcom/android/settings/display/ScreenColorPreference;->TW:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v1, p0, Lcom/android/settings/display/ScreenColorPreference;->TZ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0600ae

    goto :goto_0
.end method


# virtual methods
.method public Lo(IZ)V
    .locals 5

    const v4, 0x7f0800c4

    const v3, 0x7f0800c3

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_color_level"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TW:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TW:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TW:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TZ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TZ:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TZ:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x3

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TW:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TW:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_6
    if-ne p1, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TZ:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TZ:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a02b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    const v0, 0x7f0a0109

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TW:Landroid/widget/TextView;

    const v0, 0x7f0a050f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TZ:Landroid/widget/TextView;

    const v0, 0x7f0a03ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/ScreenColorBitMapView;

    iput-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    new-instance v1, Lcom/android/settings/display/E;

    invoke-direct {v1, p0}, Lcom/android/settings/display/E;-><init>(Lcom/android/settings/display/ScreenColorPreference;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/display/ScreenColorBitMapView;->NA(Lcom/android/settings/display/r;)V

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_color_level"

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/display/ScreenColorPreference;->Lo(IZ)V

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenColorPreference;->isEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/display/ScreenColorPreference;->Lp(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TW:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TZ:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v2, 0x1

    const/high16 v3, 0x43160000    # 150.0f

    iget v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TX:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/display/ScreenColorPreference;->Lo(IZ)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TX:I

    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TY:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/display/ScreenColorPreference;->Lo(IZ)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    iget-object v1, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    invoke-virtual {v1}, Lcom/android/settings/display/ScreenColorBitMapView;->NB()F

    move-result v1

    iget-object v2, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    invoke-virtual {v2}, Lcom/android/settings/display/ScreenColorBitMapView;->NB()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/display/ScreenColorBitMapView;->NC(FF)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TZ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, v2, v2}, Lcom/android/settings/display/ScreenColorPreference;->Lo(IZ)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    iget-object v1, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    invoke-virtual {v1}, Lcom/android/settings/display/ScreenColorBitMapView;->NB()F

    move-result v1

    sub-float/2addr v1, v3

    iget-object v2, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    invoke-virtual {v2}, Lcom/android/settings/display/ScreenColorBitMapView;->NB()F

    move-result v2

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/display/ScreenColorBitMapView;->NC(FF)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TW:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/display/ScreenColorPreference;->Lo(IZ)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    iget-object v1, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    invoke-virtual {v1}, Lcom/android/settings/display/ScreenColorBitMapView;->NB()F

    move-result v1

    add-float/2addr v1, v3

    iget-object v2, p0, Lcom/android/settings/display/ScreenColorPreference;->TV:Lcom/android/settings/display/ScreenColorBitMapView;

    invoke-virtual {v2}, Lcom/android/settings/display/ScreenColorBitMapView;->NB()F

    move-result v2

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/display/ScreenColorBitMapView;->NC(FF)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a0109 -> :sswitch_2
        0x7f0a02b8 -> :sswitch_0
        0x7f0a050f -> :sswitch_1
    .end sparse-switch
.end method

.method public onParentChanged(Landroid/preference/Preference;Z)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/preference/Preference;->onParentChanged(Landroid/preference/Preference;Z)V

    xor-int/lit8 v0, p2, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/display/ScreenColorPreference;->Lp(Z)V

    return-void
.end method
