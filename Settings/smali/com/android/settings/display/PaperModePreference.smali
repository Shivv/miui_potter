.class public Lcom/android/settings/display/PaperModePreference;
.super Lmiui/preference/RadioButtonPreference;
.source "PaperModePreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private Xk:Lcom/android/settings/display/u;

.field private Xl:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/display/PaperModePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/display/PaperModePreference;->Xk:Lcom/android/settings/display/u;

    const v0, 0x7f0d0157

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PaperModePreference;->setWidgetLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public Ov(Lcom/android/settings/display/u;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/PaperModePreference;->Xk:Lcom/android/settings/display/u;

    return-void
.end method

.method public Ow(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/display/PaperModePreference;->Xl:Z

    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 4

    invoke-super {p0, p1}, Lmiui/preference/RadioButtonPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a0133

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v1, p0, Lcom/android/settings/display/PaperModePreference;->Xl:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/display/PaperModePreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/display/PaperModePreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/PaperModePreference;->Xk:Lcom/android/settings/display/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PaperModePreference;->Xk:Lcom/android/settings/display/u;

    invoke-interface {v0, p0}, Lcom/android/settings/display/u;->Mw(Lmiui/preference/RadioButtonPreference;)V

    :cond_0
    return-void
.end method
