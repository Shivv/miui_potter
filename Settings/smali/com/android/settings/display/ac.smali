.class final Lcom/android/settings/display/ac;
.super Lmiui/os/AsyncTaskWithProgress;
.source "PaperModeSetAppsFragment.java"


# instance fields
.field private final Yk:Ljava/util/Comparator;

.field private Yl:Ljava/util/List;

.field private Ym:Ljava/util/List;

.field final synthetic Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

.field final synthetic Yo:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/android/settings/display/PaperModeSetAppsFragment;Landroid/app/FragmentManager;Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/display/ac;->Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

    iput-object p3, p0, Lcom/android/settings/display/ac;->Yo:Ljava/util/List;

    invoke-direct {p0, p2}, Lmiui/os/AsyncTaskWithProgress;-><init>(Landroid/app/FragmentManager;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/display/ac;->Yl:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/display/ac;->Ym:Ljava/util/List;

    new-instance v0, Lcom/android/settings/display/ad;

    invoke-direct {v0, p0}, Lcom/android/settings/display/ad;-><init>(Lcom/android/settings/display/ac;)V

    iput-object v0, p0, Lcom/android/settings/display/ac;->Yk:Ljava/util/Comparator;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/settings/display/ac;->Yl:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/display/ac;->Ym:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/display/ac;->Yo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v1, p0, Lcom/android/settings/display/ac;->Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

    invoke-static {v1}, Lcom/android/settings/display/PaperModeSetAppsFragment;->Oc(Lcom/android/settings/display/PaperModeSetAppsFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/display/ac;->Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

    invoke-static {v1}, Lcom/android/settings/display/PaperModeSetAppsFragment;->Oc(Lcom/android/settings/display/PaperModeSetAppsFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/display/ac;->Yl:Ljava/util/List;

    new-instance v3, Lcom/android/settings/display/t;

    iget-object v4, p0, Lcom/android/settings/display/ac;->Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

    iget-object v5, p0, Lcom/android/settings/display/ac;->Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

    invoke-static {v5}, Lcom/android/settings/display/PaperModeSetAppsFragment;->Oa(Lcom/android/settings/display/PaperModeSetAppsFragment;)Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v3, v4, v5, v0, v6}, Lcom/android/settings/display/t;-><init>(Lcom/android/settings/display/PaperModeSetAppsFragment;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Z)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/android/settings/display/PaperModeSetAppsFragment;->-get0()Ljava/util/HashSet;

    move-result-object v1

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/display/ac;->Ym:Ljava/util/List;

    new-instance v3, Lcom/android/settings/display/t;

    iget-object v4, p0, Lcom/android/settings/display/ac;->Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

    iget-object v5, p0, Lcom/android/settings/display/ac;->Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

    invoke-static {v5}, Lcom/android/settings/display/PaperModeSetAppsFragment;->Oa(Lcom/android/settings/display/PaperModeSetAppsFragment;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v4, v5, v0, v7}, Lcom/android/settings/display/t;-><init>(Lcom/android/settings/display/PaperModeSetAppsFragment;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Z)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/display/ac;->Yl:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/display/ac;->Yk:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/android/settings/display/ac;->Ym:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/display/ac;->Yk:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 3

    invoke-super {p0, p1}, Lmiui/os/AsyncTaskWithProgress;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/display/ac;->Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

    iget-object v1, p0, Lcom/android/settings/display/ac;->Yl:Ljava/util/List;

    iget-object v2, p0, Lcom/android/settings/display/ac;->Ym:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/android/settings/display/PaperModeSetAppsFragment;->Of(Lcom/android/settings/display/PaperModeSetAppsFragment;Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settings/display/ac;->Yn:Lcom/android/settings/display/PaperModeSetAppsFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/display/PaperModeSetAppsFragment;->Od(Lcom/android/settings/display/PaperModeSetAppsFragment;Lmiui/os/AsyncTaskWithProgress;)Lmiui/os/AsyncTaskWithProgress;

    return-void
.end method
