.class Lcom/android/settings/display/g;
.super Landroid/preference/CheckBoxPreference;
.source "MonochromeModeSetAppFragment.java"


# instance fields
.field final synthetic TU:Lcom/android/settings/display/MonochromeModeSetAppFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/display/MonochromeModeSetAppFragment;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Z)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/display/g;->TU:Lcom/android/settings/display/MonochromeModeSetAppFragment;

    invoke-direct {p0, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->Lj(Lcom/android/settings/display/MonochromeModeSetAppFragment;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/g;->setTitle(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->Lj(Lcom/android/settings/display/MonochromeModeSetAppFragment;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/g;->setIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/display/g;->setKey(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/g;->setPersistent(Z)V

    invoke-virtual {p0, p4}, Lcom/android/settings/display/g;->setChecked(Z)V

    invoke-virtual {p0, p1}, Lcom/android/settings/display/g;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method
