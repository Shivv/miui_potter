.class public Lcom/android/settings/display/ScreenZoomSettings;
.super Lcom/android/settings/PreviewSeekBarPreferenceFragment;
.source "ScreenZoomSettings.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private UK:I

.field private UL:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/display/M;

    invoke-direct {v0}, Lcom/android/settings/display/M;-><init>()V

    sput-object v0, Lcom/android/settings/display/ScreenZoomSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected Mt(Landroid/content/res/Configuration;I)Landroid/content/res/Configuration;
    .locals 2

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iget-object v1, p0, Lcom/android/settings/display/ScreenZoomSettings;->UL:[I

    aget v1, v1, p2

    iput v1, v0, Landroid/content/res/Configuration;->densityDpi:I

    return-object v0
.end method

.method protected commit()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/display/ScreenZoomSettings;->UL:[I

    iget v1, p0, Lcom/android/settings/display/ScreenZoomSettings;->bQs:I

    aget v0, v0, v1

    iget v1, p0, Lcom/android/settings/display/ScreenZoomSettings;->UK:I

    if-ne v0, v1, :cond_0

    invoke-static {v2}, Lcom/android/settingslib/l/a;->cpH(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v2, v0}, Lcom/android/settingslib/l/a;->cpJ(II)V

    goto :goto_0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x153

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d01a2

    iput v0, p0, Lcom/android/settings/display/ScreenZoomSettings;->bQo:I

    const v0, 0x7f0d01a3

    const v1, 0x7f0d01a4

    const v2, 0x7f0d01a6

    filled-new-array {v0, v1, v2}, [I

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/ScreenZoomSettings;->bQp:[I

    new-instance v0, Lcom/android/settingslib/l/a;

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenZoomSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settingslib/l/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settingslib/l/a;->cpN()I

    move-result v1

    if-gez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenZoomSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    new-array v1, v4, [I

    aput v0, v1, v3

    iput-object v1, p0, Lcom/android/settings/display/ScreenZoomSettings;->UL:[I

    new-array v1, v4, [Ljava/lang/String;

    sget v2, Lcom/android/settingslib/l/a;->cJX:I

    invoke-virtual {p0, v2}, Lcom/android/settings/display/ScreenZoomSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    iput-object v1, p0, Lcom/android/settings/display/ScreenZoomSettings;->bQq:[Ljava/lang/String;

    iput v3, p0, Lcom/android/settings/display/ScreenZoomSettings;->bQr:I

    iput v0, p0, Lcom/android/settings/display/ScreenZoomSettings;->UK:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/android/settingslib/l/a;->cpM()[I

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/display/ScreenZoomSettings;->UL:[I

    invoke-virtual {v0}, Lcom/android/settingslib/l/a;->cpG()[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/display/ScreenZoomSettings;->bQq:[Ljava/lang/String;

    iput v1, p0, Lcom/android/settings/display/ScreenZoomSettings;->bQr:I

    invoke-virtual {v0}, Lcom/android/settingslib/l/a;->cpL()I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/ScreenZoomSettings;->UK:I

    goto :goto_0
.end method
