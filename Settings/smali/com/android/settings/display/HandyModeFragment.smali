.class public Lcom/android/settings/display/HandyModeFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "HandyModeFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field TD:Ljava/util/ArrayList;

.field TE:Landroid/preference/CheckBoxPreference;

.field TF:Lmiui/util/HandyModeUtils;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/display/HandyModeFragment;->TD:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public KW(F)V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/display/HandyModeFragment;->TD:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/HandyModeFragment;->TD:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0}, Lmiui/preference/RadioButtonPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    cmpl-float v1, v1, p1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Lmiui/preference/RadioButtonPreference;->setChecked(Z)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    return-void
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/display/HandyModeFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lmiui/util/HandyModeUtils;->getInstance(Landroid/content/Context;)Lmiui/util/HandyModeUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/HandyModeFragment;->TF:Lmiui/util/HandyModeUtils;

    const v0, 0x7f150061

    invoke-virtual {p0, v0}, Lcom/android/settings/display/HandyModeFragment;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "handy_mode_state"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/HandyModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/display/HandyModeFragment;->TE:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/display/HandyModeFragment;->TE:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/display/HandyModeFragment;->TE:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/display/HandyModeFragment;->TF:Lmiui/util/HandyModeUtils;

    invoke-virtual {v2}, Lmiui/util/HandyModeUtils;->isHandyModeEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f03007d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f03007e

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v3

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    iget-object v7, p0, Lcom/android/settings/display/HandyModeFragment;->TF:Lmiui/util/HandyModeUtils;

    invoke-virtual {v7, v6}, Lmiui/util/HandyModeUtils;->isValidSize(F)Z

    move-result v6

    if-nez v6, :cond_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v6, Lmiui/preference/RadioButtonPreference;

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v6, v7}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    aget-object v7, v3, v0

    invoke-virtual {v6, v7}, Lmiui/preference/RadioButtonPreference;->setTitle(Ljava/lang/CharSequence;)V

    aget-object v7, v4, v0

    invoke-virtual {v6, v7}, Lmiui/preference/RadioButtonPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Lmiui/preference/RadioButtonPreference;->setPersistent(Z)V

    invoke-virtual {v6, p0}, Lmiui/preference/RadioButtonPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v7, p0, Lcom/android/settings/display/HandyModeFragment;->TD:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/HandyModeFragment;->TF:Lmiui/util/HandyModeUtils;

    invoke-virtual {v0}, Lmiui/util/HandyModeUtils;->getSize()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/HandyModeFragment;->KW(F)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/display/HandyModeFragment;->TF:Lmiui/util/HandyModeUtils;

    invoke-virtual {v1, v0}, Lmiui/util/HandyModeUtils;->setHandyModeStateToSettings(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/HandyModeFragment;->KW(F)V

    iget-object v1, p0, Lcom/android/settings/display/HandyModeFragment;->TF:Lmiui/util/HandyModeUtils;

    invoke-virtual {v1, v0}, Lmiui/util/HandyModeUtils;->setSize(F)V

    const/4 v0, 0x1

    return v0
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStart()V

    invoke-virtual {p0}, Lcom/android/settings/display/HandyModeFragment;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    const v1, 0x7f1207e2

    invoke-virtual {v0, v1}, Lmiui/app/ActionBar;->setTitle(I)V

    return-void
.end method
