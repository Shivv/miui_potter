.class public Lcom/android/settings/display/NightDisplaySettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "NightDisplaySettings.java"

# interfaces
.implements Lcom/android/internal/app/NightDisplayController$Callback;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mActivatedPreference:Landroid/preference/TwoStatePreference;

.field private mAutoModePreference:Lcom/android/settings/MiuiListPreference;

.field private mController:Lcom/android/internal/app/NightDisplayController;

.field private mEndTimePreference:Landroid/preference/Preference;

.field private mStartTimePreference:Landroid/preference/Preference;

.field private mTimeFormatter:Ljava/text/DateFormat;


# direct methods
.method static synthetic -get0(Lcom/android/settings/display/NightDisplaySettings;)Lcom/android/internal/app/NightDisplayController;
    .locals 1
    .param p0, "-this"    # Lcom/android/settings/display/NightDisplaySettings;

    .prologue
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/settings/display/NightDisplaySettings;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/settings/display/NightDisplaySettings;
    .param p1, "dialogId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/settings/display/NightDisplaySettings;->showDialog(I)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private getFormattedTimeString(Ljava/time/LocalTime;)Ljava/lang/String;
    .locals 4
    .param p1, "localTime"    # Ljava/time/LocalTime;

    .prologue
    const/4 v3, 0x0

    .line 167
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 168
    .local v0, "c":Ljava/util/Calendar;
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mTimeFormatter:Ljava/text/DateFormat;

    invoke-virtual {v1}, Ljava/text/DateFormat;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 169
    invoke-virtual {p1}, Ljava/time/LocalTime;->getHour()I

    move-result v1

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 170
    invoke-virtual {p1}, Ljava/time/LocalTime;->getMinute()I

    move-result v1

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    .line 171
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 172
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 173
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mTimeFormatter:Ljava/text/DateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private showDialog(I)V
    .locals 7
    .param p1, "dialogId"    # I

    .prologue
    .line 131
    if-nez p1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0}, Lcom/android/internal/app/NightDisplayController;->getCustomStartTime()Ljava/time/LocalTime;

    move-result-object v6

    .line 137
    .local v6, "initialTime":Ljava/time/LocalTime;
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/display/NightDisplaySettings;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 138
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    .line 139
    .local v5, "use24HourFormat":Z
    new-instance v0, Landroid/app/TimePickerDialog;

    new-instance v2, Lcom/android/settings/display/NightDisplaySettings$3;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/display/NightDisplaySettings$3;-><init>(Lcom/android/settings/display/NightDisplaySettings;I)V

    .line 149
    invoke-virtual {v6}, Ljava/time/LocalTime;->getHour()I

    move-result v3

    invoke-virtual {v6}, Ljava/time/LocalTime;->getMinute()I

    move-result v4

    .line 139
    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    invoke-virtual {v0}, Landroid/app/TimePickerDialog;->show()V

    .line 150
    return-void

    .line 134
    .end local v1    # "context":Landroid/content/Context;
    .end local v5    # "use24HourFormat":Z
    .end local v6    # "initialTime":Ljava/time/LocalTime;
    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0}, Lcom/android/internal/app/NightDisplayController;->getCustomEndTime()Ljava/time/LocalTime;

    move-result-object v6

    .restart local v6    # "initialTime":Ljava/time/LocalTime;
    goto :goto_0
.end method


# virtual methods
.method public onActivated(Z)V
    .locals 1
    .param p1, "activated"    # Z

    .prologue
    .line 154
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mActivatedPreference:Landroid/preference/TwoStatePreference;

    invoke-virtual {v0, p1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    .line 155
    return-void
.end method

.method public onAutoModeChanged(I)V
    .locals 3
    .param p1, "autoMode"    # I

    .prologue
    .line 159
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mAutoModePreference:Lcom/android/settings/MiuiListPreference;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/MiuiListPreference;->setValue(Ljava/lang/String;)V

    .line 161
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    .line 162
    .local v0, "showCustomSchedule":Z
    :goto_0
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mStartTimePreference:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 163
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mEndTimePreference:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 164
    return-void

    .line 161
    .end local v0    # "showCustomSchedule":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "showCustomSchedule":Z
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 61
    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 64
    # THE XML LAYOUT
    const v1, 0x7f150097

    invoke-virtual {p0, v1}, Lcom/android/settings/display/NightDisplaySettings;->addPreferencesFromResource(I)V

    .line 65
    const-string/jumbo v1, "night_display_auto_mode"

    invoke-virtual {p0, v1}, Lcom/android/settings/display/NightDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/MiuiListPreference;

    iput-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mAutoModePreference:Lcom/android/settings/MiuiListPreference;

    .line 66
    const-string/jumbo v1, "night_display_start_time"

    invoke-virtual {p0, v1}, Lcom/android/settings/display/NightDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mStartTimePreference:Landroid/preference/Preference;

    .line 67
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mStartTimePreference:Landroid/preference/Preference;

    new-instance v2, Lcom/android/settings/display/NightDisplaySettings$1;

    invoke-direct {v2, p0}, Lcom/android/settings/display/NightDisplaySettings$1;-><init>(Lcom/android/settings/display/NightDisplaySettings;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 75
    const-string/jumbo v1, "night_display_end_time"

    invoke-virtual {p0, v1}, Lcom/android/settings/display/NightDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mEndTimePreference:Landroid/preference/Preference;

    .line 76
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mEndTimePreference:Landroid/preference/Preference;

    new-instance v2, Lcom/android/settings/display/NightDisplaySettings$2;

    invoke-direct {v2, p0}, Lcom/android/settings/display/NightDisplaySettings$2;-><init>(Lcom/android/settings/display/NightDisplaySettings;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 84
    const-string/jumbo v1, "night_display_activated"

    invoke-virtual {p0, v1}, Lcom/android/settings/display/NightDisplaySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/TwoStatePreference;

    iput-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mActivatedPreference:Landroid/preference/TwoStatePreference;

    .line 86
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mAutoModePreference:Lcom/android/settings/MiuiListPreference;

    new-array v2, v7, [Ljava/lang/CharSequence;

    .line 87
    # screen_paper_mode_turn_off
    const v3, 0x7f120ea1

    invoke-virtual {p0, v3}, Lcom/android/settings/display/NightDisplaySettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 88
    # screen_paper_mode_time_title
    const v3, 0x7f120e9f

    invoke-virtual {p0, v3}, Lcom/android/settings/display/NightDisplaySettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    .line 89
    # paper_mode_auto_twilight_title
    const v3, 0x7f120c6e

    invoke-virtual {p0, v3}, Lcom/android/settings/display/NightDisplaySettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 86
    invoke-virtual {v1, v2}, Lcom/android/settings/MiuiListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 91
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mAutoModePreference:Lcom/android/settings/MiuiListPreference;

    new-array v2, v7, [Ljava/lang/CharSequence;

    .line 92
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 93
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    .line 94
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 91
    invoke-virtual {v1, v2}, Lcom/android/settings/MiuiListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 96
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mAutoModePreference:Lcom/android/settings/MiuiListPreference;

    invoke-virtual {v1, p0}, Lcom/android/settings/MiuiListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 97
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mActivatedPreference:Landroid/preference/TwoStatePreference;

    invoke-virtual {v1, p0}, Landroid/preference/TwoStatePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 99
    invoke-virtual {p0}, Lcom/android/settings/display/NightDisplaySettings;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 100
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/android/internal/app/NightDisplayController;

    invoke-direct {v1, v0}, Lcom/android/internal/app/NightDisplayController;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    .line 102
    invoke-static {v0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mTimeFormatter:Ljava/text/DateFormat;

    .line 103
    iget-object v1, p0, Lcom/android/settings/display/NightDisplaySettings;->mTimeFormatter:Ljava/text/DateFormat;

    const-string/jumbo v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 104
    return-void
.end method

.method public onCustomEndTimeChanged(Ljava/time/LocalTime;)V
    .locals 2
    .param p1, "endTime"    # Ljava/time/LocalTime;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mEndTimePreference:Landroid/preference/Preference;

    invoke-direct {p0, p1}, Lcom/android/settings/display/NightDisplaySettings;->getFormattedTimeString(Ljava/time/LocalTime;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 184
    return-void
.end method

.method public onCustomStartTimeChanged(Ljava/time/LocalTime;)V
    .locals 2
    .param p1, "startTime"    # Ljava/time/LocalTime;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mStartTimePreference:Landroid/preference/Preference;

    invoke-direct {p0, p1}, Lcom/android/settings/display/NightDisplaySettings;->getFormattedTimeString(Ljava/time/LocalTime;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 179
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mAutoModePreference:Lcom/android/settings/MiuiListPreference;

    if-ne p1, v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/internal/app/NightDisplayController;->setAutoMode(I)Z

    move-result v0

    return v0

    .line 190
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mActivatedPreference:Landroid/preference/TwoStatePreference;

    if-ne p1, v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/internal/app/NightDisplayController;->setActivated(Z)Z

    move-result v0

    return v0

    .line 193
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStart()V

    .line 111
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0, p0}, Lcom/android/internal/app/NightDisplayController;->setListener(Lcom/android/internal/app/NightDisplayController$Callback;)V

    .line 114
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0}, Lcom/android/internal/app/NightDisplayController;->isActivated()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/NightDisplaySettings;->onActivated(Z)V

    .line 115
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0}, Lcom/android/internal/app/NightDisplayController;->getAutoMode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/NightDisplaySettings;->onAutoModeChanged(I)V

    .line 116
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0}, Lcom/android/internal/app/NightDisplayController;->getCustomStartTime()Ljava/time/LocalTime;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/NightDisplaySettings;->onCustomStartTimeChanged(Ljava/time/LocalTime;)V

    .line 117
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0}, Lcom/android/internal/app/NightDisplayController;->getCustomEndTime()Ljava/time/LocalTime;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/NightDisplaySettings;->onCustomEndTimeChanged(Ljava/time/LocalTime;)V

    .line 118
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    invoke-virtual {v0}, Lcom/android/internal/app/NightDisplayController;->getColorMode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/NightDisplaySettings;->onDisplayColorModeChanged(I)V

    .line 119
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStop()V

    .line 126
    iget-object v0, p0, Lcom/android/settings/display/NightDisplaySettings;->mController:Lcom/android/internal/app/NightDisplayController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/internal/app/NightDisplayController;->setListener(Lcom/android/internal/app/NightDisplayController$Callback;)V

    .line 127
    return-void
.end method
