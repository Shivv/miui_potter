.class public Lcom/android/settings/display/BrightnessFragment;
.super Lcom/android/settings/BaseFragment;
.source "BrightnessFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final Vt:I

.field private static final Vu:I

.field public static final Vw:Z


# instance fields
.field private VA:Lcom/android/settings/display/p;

.field private VB:Landroid/widget/LinearLayout;

.field private VC:Landroid/database/ContentObserver;

.field private VD:Landroid/database/ContentObserver;

.field private VE:I

.field private VF:I

.field private VG:I

.field private VH:Landroid/app/Dialog;

.field private VI:Lmiui/widget/SeekBar;

.field private VJ:Lmiui/widget/SeekBar;

.field private VK:Landroid/view/View;

.field private VL:I

.field private VM:I

.field private VN:Z

.field private VO:Landroid/database/ContentObserver;

.field private VP:F

.field private VQ:F

.field private VR:F

.field private VS:Landroid/os/IPowerManager;

.field private VT:Z

.field private VU:I

.field private VV:Lmiui/widget/SeekBar;

.field private VW:Landroid/view/View;

.field private VX:Lmiui/widget/SlidingButton;

.field private VY:Landroid/widget/TextView;

.field private VZ:Landroid/view/View;

.field private final Vs:I

.field private final Vv:I

.field private Vx:Z

.field private Vy:Z

.field private Vz:Lcom/android/settings/display/q;

.field private Wa:Landroid/widget/Toast;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11070018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/settings/display/BrightnessFragment;->Vu:I

    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11070015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/settings/display/BrightnessFragment;->Vt:I

    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    sput-boolean v0, Lcom/android/settings/display/BrightnessFragment;->Vw:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VE:I

    const/16 v0, 0xff

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vs:I

    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vv:I

    new-instance v0, Lcom/android/settings/display/W;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/display/W;-><init>(Lcom/android/settings/display/BrightnessFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VD:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/settings/display/X;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/display/X;-><init>(Lcom/android/settings/display/BrightnessFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VC:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/settings/display/Y;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/display/Y;-><init>(Lcom/android/settings/display/BrightnessFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VO:Landroid/database/ContentObserver;

    return-void
.end method

.method private Nd()I
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nf()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_auto_brightness_adj"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const v1, 0x461c4000    # 10000.0f

    mul-float/2addr v0, v1

    :goto_0
    float-to-int v0, v0

    return v0

    :cond_0
    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VE:I

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_brightness"

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    int-to-float v0, v0

    :goto_1
    iget v1, p0, Lcom/android/settings/display/BrightnessFragment;->VU:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/android/settings/display/BrightnessFragment;->VP:F

    div-float/2addr v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VE:I

    int-to-float v0, v0

    goto :goto_1
.end method

.method private Ne()Ljava/lang/String;
    .locals 5

    const-string/jumbo v1, "/sys/class/leds/lcd-backlight/brightness"

    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x11090019

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    new-instance v3, Ljava/io/File;

    aget-object v4, v2, v0

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    aget-object v0, v2, v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private Nf()I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_brightness_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private Nh()V
    .locals 5

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "night_light_level"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_0

    and-int/lit16 v1, v0, 0xff

    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x11070015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x11070014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-static {v1, v2, v3}, Landroid/util/MathUtils;->constrain(III)I

    move-result v1

    iput v1, p0, Lcom/android/settings/display/BrightnessFragment;->VF:I

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11070018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x11070017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/util/MathUtils;->constrain(III)I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VG:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11070016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VF:I

    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11070019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VG:I

    goto :goto_0
.end method

.method private Ni()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nd()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/widget/SeekBar;->setProgress(I)V

    return-void
.end method

.method private Nj()V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nf()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    invoke-virtual {v1, v0}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Nk()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->VT:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vx:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VL:I

    invoke-direct {p0, v0}, Lcom/android/settings/display/BrightnessFragment;->Nm(I)V

    :cond_1
    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VM:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/settings/display/BrightnessFragment;->Nl(IZ)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->VT:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VE:I

    return-void
.end method

.method private Nl(IZ)V
    .locals 3

    :try_start_0
    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vy:Z

    if-eqz v0, :cond_1

    int-to-float v0, p1

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    const v1, 0x461c4000    # 10000.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VS:Landroid/os/IPowerManager;

    invoke-interface {v1, v0}, Landroid/os/IPowerManager;->setTemporaryScreenAutoBrightnessAdjustmentSettingOverride(F)V

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_auto_brightness_adj"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    int-to-float v0, p1

    iget v1, p0, Lcom/android/settings/display/BrightnessFragment;->VP:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/android/settings/display/BrightnessFragment;->VU:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    if-eqz p2, :cond_2

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/settings/display/BrightnessFragment;->VE:I

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_brightness"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VE:I

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VS:Landroid/os/IPowerManager;

    invoke-interface {v1, v0}, Landroid/os/IPowerManager;->setTemporaryScreenBrightnessSettingOverride(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private Nm(I)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vy:Z

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_brightness_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Nn()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VW:Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/settings/display/BrightnessFragment;->Vy:Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VZ:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VW:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic Nq(Lcom/android/settings/display/BrightnessFragment;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic Nr(Lcom/android/settings/display/BrightnessFragment;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VP:F

    return v0
.end method

.method static synthetic Ns(Lcom/android/settings/display/BrightnessFragment;)Landroid/os/IPowerManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VS:Landroid/os/IPowerManager;

    return-object v0
.end method

.method static synthetic Nt(Lcom/android/settings/display/BrightnessFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VU:I

    return v0
.end method

.method static synthetic Nu(Lcom/android/settings/display/BrightnessFragment;)Lmiui/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    return-object v0
.end method

.method static synthetic Nv(Lcom/android/settings/display/BrightnessFragment;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/display/BrightnessFragment;->VE:I

    return p1
.end method

.method static synthetic Nw(Lcom/android/settings/display/BrightnessFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/display/BrightnessFragment;->VN:Z

    return p1
.end method

.method static synthetic Nx(Lcom/android/settings/display/BrightnessFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Ni()V

    return-void
.end method

.method static synthetic Ny(Lcom/android/settings/display/BrightnessFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nj()V

    return-void
.end method


# virtual methods
.method public Ng()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VS:Landroid/os/IPowerManager;

    const-string/jumbo v2, "getScreenBrightnessReal"

    const-class v3, Ljava/lang/Integer;

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3, v4}, Lmiui/util/ReflectionUtils;->tryCallMethod(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Object;)Lmiui/util/ObjectReference;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_0
    return v0
.end method

.method public No(J)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VA:Lcom/android/settings/display/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VA:Lcom/android/settings/display/p;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/display/p;->Nz(J)V

    :cond_0
    return-void
.end method

.method public Np()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VA:Lcom/android/settings/display/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VA:Lcom/android/settings/display/p;

    invoke-virtual {v0}, Lcom/android/settings/display/p;->stopWatching()V

    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nd()I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VM:I

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    iget v3, p0, Lcom/android/settings/display/BrightnessFragment;->VM:I

    invoke-virtual {v0, v3}, Lmiui/widget/SeekBar;->setProgress(I)V

    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vx:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VB:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    invoke-virtual {v0, p0}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nf()I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VL:I

    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VL:I

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vy:Z

    iget-object v3, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VL:I

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nn()V

    :goto_2
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "screen_auto_brightness_adj"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/display/BrightnessFragment;->VD:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "screen_brightness"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/display/BrightnessFragment;->VD:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "screen_brightness_mode"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/display/BrightnessFragment;->VC:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_NIGHT_LIGHT_ADJ:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nh()V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VI:Lmiui/widget/SeekBar;

    iget v3, p0, Lcom/android/settings/display/BrightnessFragment;->VF:I

    sget v4, Lcom/android/settings/display/BrightnessFragment;->Vt:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/android/settings/display/BrightnessFragment;->VQ:F

    div-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v0, v3}, Lmiui/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VJ:Lmiui/widget/SeekBar;

    iget v3, p0, Lcom/android/settings/display/BrightnessFragment;->VG:I

    sget v4, Lcom/android/settings/display/BrightnessFragment;->Vu:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/android/settings/display/BrightnessFragment;->VR:F

    div-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v0, v3}, Lmiui/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VO:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "screen_paper_mode_enabled"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/display/BrightnessFragment;->VO:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f120b8a

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->Wa:Landroid/widget/Toast;

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    invoke-virtual {v0, v2}, Lmiui/widget/SlidingButton;->setEnabled(Z)V

    goto/16 :goto_2
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    invoke-virtual {p0, v0}, Lcom/android/settings/display/BrightnessFragment;->onClick(Landroid/view/View;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a00a1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    invoke-virtual {v1}, Lmiui/widget/SlidingButton;->isChecked()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    invoke-virtual {v0}, Lmiui/widget/SlidingButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    sget-boolean v2, Lcom/android/settings/display/BrightnessFragment;->Vw:Z

    if-eqz v2, :cond_1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->Ng()I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "screen_brightness"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->Np()V

    :cond_1
    :goto_1
    invoke-direct {p0, v0}, Lcom/android/settings/display/BrightnessFragment;->Nm(I)V

    sget-boolean v0, Lcom/android/settings/display/BrightnessFragment;->Vw:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nd()I

    move-result v2

    invoke-virtual {v0, v2}, Lmiui/widget/SeekBar;->setProgress(I)V

    :cond_2
    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment;->VY:Landroid/widget/TextView;

    if-eqz v1, :cond_5

    const v0, 0x7f12038b

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nn()V

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const-wide/16 v2, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/android/settings/display/BrightnessFragment;->No(J)V

    goto :goto_1

    :cond_5
    const v0, 0x7f12038a

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_NIGHT_LIGHT_ADJ:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "BrightnessFragment"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lcom/android/settings/display/q;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/android/settings/display/q;-><init>(Lcom/android/settings/display/BrightnessFragment;Landroid/os/Looper;Lcom/android/settings/display/q;)V

    iput-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->Vz:Lcom/android/settings/display/q;

    :cond_0
    const-string/jumbo v0, "power"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VS:Landroid/os/IPowerManager;

    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110a0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vx:Z

    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11070005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VU:I

    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VU:I

    rsub-int v0, v0, 0xff

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v0, v1

    const v1, 0x461c4000    # 10000.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VP:F

    sget-boolean v0, Lcom/android/settings/display/BrightnessFragment;->Vw:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settings/display/p;

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Ne()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/display/p;-><init>(Lcom/android/settings/display/BrightnessFragment;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VA:Lcom/android/settings/display/p;

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->VT:Z

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VD:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VC:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    invoke-virtual {v1, v2}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VB:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-boolean v1, Lmiui/os/DeviceFeature;->SUPPORT_NIGHT_LIGHT_ADJ:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VO:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vz:Lcom/android/settings/display/q;

    invoke-virtual {v0}, Lcom/android/settings/display/q;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    sget-boolean v0, Lcom/android/settings/display/BrightnessFragment;->Vw:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nf()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VA:Lcom/android/settings/display/p;

    invoke-virtual {v0}, Lcom/android/settings/display/p;->stopWatching()V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onPause()V

    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    if-ne p1, v2, :cond_1

    invoke-direct {p0, p2, v1}, Lcom/android/settings/display/BrightnessFragment;->Nl(IZ)V

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vz:Lcom/android/settings/display/q;

    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment;->Vz:Lcom/android/settings/display/q;

    iget v3, p0, Lcom/android/settings/display/BrightnessFragment;->VG:I

    shl-int/lit8 v3, v3, 0x8

    iget v4, p0, Lcom/android/settings/display/BrightnessFragment;->VF:I

    or-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/display/q;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment;->VI:Lmiui/widget/SeekBar;

    if-ne p1, v2, :cond_2

    int-to-float v2, p2

    iget v3, p0, Lcom/android/settings/display/BrightnessFragment;->VQ:F

    mul-float/2addr v2, v3

    sget v3, Lcom/android/settings/display/BrightnessFragment;->Vt:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/android/settings/display/BrightnessFragment;->VF:I

    if-eq v3, v2, :cond_3

    iput v2, p0, Lcom/android/settings/display/BrightnessFragment;->VF:I

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment;->VJ:Lmiui/widget/SeekBar;

    if-ne p1, v2, :cond_3

    int-to-float v2, p2

    iget v3, p0, Lcom/android/settings/display/BrightnessFragment;->VR:F

    mul-float/2addr v2, v3

    sget v3, Lcom/android/settings/display/BrightnessFragment;->Vu:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/android/settings/display/BrightnessFragment;->VG:I

    if-eq v3, v2, :cond_3

    iput v2, p0, Lcom/android/settings/display/BrightnessFragment;->VG:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onResume()V

    sget-boolean v0, Lcom/android/settings/display/BrightnessFragment;->Vw:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nf()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/display/BrightnessFragment;->No(J)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VH:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VH:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/settings/display/BrightnessFragment$SavedState;

    invoke-direct {v1}, Lcom/android/settings/display/BrightnessFragment$SavedState;-><init>()V

    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    invoke-virtual {v2}, Lmiui/widget/SlidingButton;->isChecked()Z

    move-result v2

    iput-boolean v2, v1, Lcom/android/settings/display/BrightnessFragment$SavedState;->automatic:Z

    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    invoke-virtual {v2}, Lmiui/widget/SeekBar;->getProgress()I

    move-result v2

    iput v2, v1, Lcom/android/settings/display/BrightnessFragment$SavedState;->progress:I

    iget v2, p0, Lcom/android/settings/display/BrightnessFragment;->VL:I

    if-ne v2, v0, :cond_1

    :goto_0
    iput-boolean v0, v1, Lcom/android/settings/display/BrightnessFragment$SavedState;->oldAutomatic:Z

    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VM:I

    iput v0, v1, Lcom/android/settings/display/BrightnessFragment$SavedState;->oldProgress:I

    iget v0, p0, Lcom/android/settings/display/BrightnessFragment;->VE:I

    iput v0, v1, Lcom/android/settings/display/BrightnessFragment$SavedState;->curBrightness:I

    const-string/jumbo v0, "save_state"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nk()V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    if-ne p1, v0, :cond_0

    sget-boolean v0, Lcom/android/settings/display/BrightnessFragment;->Vw:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nf()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->Np()V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VS:Landroid/os/IPowerManager;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-interface {v0, v1}, Landroid/os/IPowerManager;->setTemporaryScreenAutoBrightnessAdjustmentSettingOverride(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    if-ne p1, v0, :cond_2

    :try_start_0
    sget-boolean v0, Lcom/android/settings/display/BrightnessFragment;->Vw:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->Nf()I

    move-result v0

    if-ne v0, v4, :cond_0

    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/display/BrightnessFragment;->No(J)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VS:Landroid/os/IPowerManager;

    const/high16 v1, -0x40000000    # -2.0f

    invoke-interface {v0, v1}, Landroid/os/IPowerManager;->setTemporaryScreenAutoBrightnessAdjustmentSettingOverride(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    invoke-virtual {v0}, Lmiui/widget/SeekBar;->getProgress()I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/android/settings/display/BrightnessFragment;->Nl(IZ)V

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VI:Lmiui/widget/SeekBar;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VJ:Lmiui/widget/SeekBar;

    if-ne p1, v0, :cond_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->Vz:Lcom/android/settings/display/q;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->Vz:Lcom/android/settings/display/q;

    iget v2, p0, Lcom/android/settings/display/BrightnessFragment;->VG:I

    shl-int/lit8 v2, v2, 0x8

    iget v3, p0, Lcom/android/settings/display/BrightnessFragment;->VF:I

    or-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v4, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/display/q;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    invoke-virtual {v0}, Lmiui/widget/SeekBar;->getProgress()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->Wa:Landroid/widget/Toast;

    const v1, 0x7f120b8a

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->Wa:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->VN:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VJ:Lmiui/widget/SeekBar;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->Wa:Landroid/widget/Toast;

    const v1, 0x7f120b88

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->Wa:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f0a03d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SeekBar;

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lmiui/widget/SeekBar;->setMax(I)V

    const v0, 0x7f0a00a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VB:Landroid/widget/LinearLayout;

    const v0, 0x7f0a007d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SlidingButton;

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VX:Lmiui/widget/SlidingButton;

    const v0, 0x7f0a003b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VY:Landroid/widget/TextView;

    const v0, 0x7f0a03d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VW:Landroid/view/View;

    const v0, 0x7f0a003c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VZ:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VV:Lmiui/widget/SeekBar;

    invoke-virtual {v0, p0}, Lmiui/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_NIGHT_LIGHT_ADJ:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a02d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VK:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VK:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0a02d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SeekBar;

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VI:Lmiui/widget/SeekBar;

    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11070014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sget v1, Lcom/android/settings/display/BrightnessFragment;->Vt:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, v2

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VI:Lmiui/widget/SeekBar;

    invoke-virtual {v1}, Lmiui/widget/SeekBar;->getMax()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VQ:F

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VI:Lmiui/widget/SeekBar;

    invoke-virtual {v0, p0}, Lmiui/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const v0, 0x7f0a02d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SeekBar;

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VJ:Lmiui/widget/SeekBar;

    invoke-virtual {p0}, Lcom/android/settings/display/BrightnessFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11070017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sget v1, Lcom/android/settings/display/BrightnessFragment;->Vu:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, v2

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->VJ:Lmiui/widget/SeekBar;

    invoke-virtual {v1}, Lmiui/widget/SeekBar;->getMax()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->VR:F

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VJ:Lmiui/widget/SeekBar;

    invoke-virtual {v0, p0}, Lmiui/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_0
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VH:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->VH:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "save_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/BrightnessFragment$SavedState;

    iget v1, v0, Lcom/android/settings/display/BrightnessFragment$SavedState;->oldProgress:I

    iput v1, p0, Lcom/android/settings/display/BrightnessFragment;->VM:I

    iget-boolean v1, v0, Lcom/android/settings/display/BrightnessFragment$SavedState;->oldAutomatic:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    iput v1, p0, Lcom/android/settings/display/BrightnessFragment;->VL:I

    iget v1, v0, Lcom/android/settings/display/BrightnessFragment$SavedState;->curBrightness:I

    iput v1, p0, Lcom/android/settings/display/BrightnessFragment;->VE:I

    iget-boolean v1, v0, Lcom/android/settings/display/BrightnessFragment$SavedState;->automatic:Z

    if-eqz v1, :cond_2

    :goto_1
    invoke-direct {p0, v2}, Lcom/android/settings/display/BrightnessFragment;->Nm(I)V

    iget v0, v0, Lcom/android/settings/display/BrightnessFragment$SavedState;->progress:I

    invoke-direct {p0, v0, v3}, Lcom/android/settings/display/BrightnessFragment;->Nl(IZ)V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d012f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
