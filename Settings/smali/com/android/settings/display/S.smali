.class final Lcom/android/settings/display/S;
.super Ljava/lang/Object;
.source "HandyModeGuideView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic Yb:Lcom/android/settings/display/R;


# direct methods
.method constructor <init>(Lcom/android/settings/display/R;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget v1, v0, Lcom/android/settings/display/HandyModeGuideView;->Vo:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/android/settings/display/HandyModeGuideView;->Vo:I

    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vo:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    const/4 v1, 0x0

    iput v1, v0, Lcom/android/settings/display/HandyModeGuideView;->Vo:I

    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v1, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v1, v1, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-boolean v1, v1, Lcom/android/settings/display/HandyModeGuideView;->Vn:Z

    xor-int/lit8 v1, v1, 0x1

    iput-boolean v1, v0, Lcom/android/settings/display/HandyModeGuideView;->Vn:Z

    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-boolean v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vn:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v1, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v1, v1, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-boolean v1, v1, Lcom/android/settings/display/HandyModeGuideView;->Vl:Z

    xor-int/lit8 v1, v1, 0x1

    iput-boolean v1, v0, Lcom/android/settings/display/HandyModeGuideView;->Vl:Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vo:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v1, v1, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    invoke-virtual {v1}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v1, v1, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    invoke-virtual {v1}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v1, v1, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    invoke-virtual {v1}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v1, v1, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    invoke-virtual {v1}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v0, v0, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    iget-object v0, v0, Lcom/android/settings/display/HandyModeGuideView;->Vj:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/settings/display/S;->Yb:Lcom/android/settings/display/R;

    iget-object v1, v1, Lcom/android/settings/display/R;->Ya:Lcom/android/settings/display/HandyModeGuideView;

    invoke-virtual {v1}, Lcom/android/settings/display/HandyModeGuideView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
