.class public Lcom/android/settings/display/c;
.super Lcom/android/settings/bA;
.source "FontStatusController.java"


# instance fields
.field private Tq:Lcom/android/settings/display/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bA;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    return-void
.end method

.method private KO(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/c;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/c;->bRZ:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/c;->bRZ:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/c;->bSa:Lcom/android/settings/bB;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/c;->bSa:Lcom/android/settings/bB;

    invoke-virtual {v0, p1}, Lcom/android/settings/bB;->bKW(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method static synthetic KP(Lcom/android/settings/display/c;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/c;->KO(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public pause()V
    .locals 0

    return-void
.end method

.method public wt()V
    .locals 0

    return-void
.end method

.method public wv()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/c;->bRZ:Landroid/widget/TextView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/c;->bSa:Lcom/android/settings/bB;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/c;->Tq:Lcom/android/settings/display/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/c;->Tq:Lcom/android/settings/display/d;

    invoke-virtual {v0}, Lcom/android/settings/display/d;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/c;->Tq:Lcom/android/settings/display/d;

    invoke-virtual {v0}, Lcom/android/settings/display/d;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    :cond_1
    new-instance v0, Lcom/android/settings/display/d;

    invoke-direct {v0, p0}, Lcom/android/settings/display/d;-><init>(Lcom/android/settings/display/c;)V

    iput-object v0, p0, Lcom/android/settings/display/c;->Tq:Lcom/android/settings/display/d;

    iget-object v0, p0, Lcom/android/settings/display/c;->Tq:Lcom/android/settings/display/d;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/display/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    return-void
.end method
