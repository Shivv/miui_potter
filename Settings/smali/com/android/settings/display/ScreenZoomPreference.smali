.class public Lcom/android/settings/display/ScreenZoomPreference;
.super Lcom/android/settings/MiuiValuePreference;
.source "ScreenZoomPreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/MiuiValuePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/android/settings/display/ScreenZoomPreference;->getFragment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "com.android.settings.display.ScreenZoomSettings"

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ScreenZoomPreference;->setFragment(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/android/settingslib/l/a;

    invoke-direct {v0, p1}, Lcom/android/settingslib/l/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settingslib/l/a;->cpN()I

    move-result v1

    if-gez v1, :cond_2

    invoke-virtual {p0, v2}, Lcom/android/settings/display/ScreenZoomPreference;->setEnabled(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/display/ScreenZoomPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/android/settingslib/l/a;->cpG()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/settingslib/l/a;->cpN()I

    move-result v0

    aget-object v0, v1, v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/ScreenZoomPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
