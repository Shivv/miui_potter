.class Lcom/android/settings/display/b;
.super Landroid/widget/BaseAdapter;
.source "FluencyModeListPreference.java"


# instance fields
.field private To:Landroid/view/LayoutInflater;

.field final synthetic Tp:Lcom/android/settings/display/FluencyModeListPreference;

.field private mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/settings/display/FluencyModeListPreference;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/display/b;->Tp:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/android/settings/display/b;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/display/b;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/b;->To:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/b;->Tp:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-virtual {v0}, Lcom/android/settings/display/FluencyModeListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/b;->Tp:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-static {v0}, Lcom/android/settings/display/FluencyModeListPreference;->KI(Lcom/android/settings/display/FluencyModeListPreference;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    return-object p2

    :cond_1
    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/b;->To:Landroid/view/LayoutInflater;

    const v1, 0x7f0d00c1

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_2
    const v0, 0x7f0a04be

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    const v1, 0x7f0a04bf

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/display/b;->Tp:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-static {v3}, Lcom/android/settings/display/FluencyModeListPreference;->KI(Lcom/android/settings/display/FluencyModeListPreference;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/settings/display/b;->Tp:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-static {v3}, Lcom/android/settings/display/FluencyModeListPreference;->KJ(Lcom/android/settings/display/FluencyModeListPreference;)I

    move-result v3

    if-ne p1, v3, :cond_3

    const/4 v2, 0x1

    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/b;->Tp:Lcom/android/settings/display/FluencyModeListPreference;

    invoke-static {v0}, Lcom/android/settings/display/FluencyModeListPreference;->KK(Lcom/android/settings/display/FluencyModeListPreference;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method
