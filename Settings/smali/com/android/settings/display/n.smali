.class Lcom/android/settings/display/n;
.super Landroid/database/ContentObserver;
.source "ScreenEffectFragment.java"


# instance fields
.field private final Vc:Landroid/net/Uri;

.field private final Vd:Landroid/net/Uri;

.field final synthetic Ve:Lcom/android/settings/display/ScreenEffectFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/display/ScreenEffectFragment;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    new-instance v0, Landroid/os/Handler;

    invoke-static {p1}, Lcom/android/settings/display/ScreenEffectFragment;->MK(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "screen_paper_mode_enabled"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/n;->Vc:Landroid/net/Uri;

    const-string/jumbo v0, "screen_paper_mode"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/n;->Vd:Landroid/net/Uri;

    return-void
.end method

.method private MP()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-static {v1}, Lcom/android/settings/display/ScreenEffectFragment;->MM(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_paper_mode"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private MQ()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-static {v0}, Lcom/android/settings/display/ScreenEffectFragment;->MM(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_paper_mode_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public MR()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-static {v0}, Lcom/android/settings/display/ScreenEffectFragment;->MM(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/n;->Vc:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-static {v0}, Lcom/android/settings/display/ScreenEffectFragment;->MM(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/n;->Vd:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public MS()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-static {v0}, Lcom/android/settings/display/ScreenEffectFragment;->MM(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onChange(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/display/n;->MQ()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/display/n;->MP()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/ScreenEffectFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/ScreenEffectFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-static {v0}, Lcom/android/settings/display/ScreenEffectFragment;->ML(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/ScreenEffectFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-virtual {v0}, Lcom/android/settings/display/ScreenEffectFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/display/n;->Ve:Lcom/android/settings/display/ScreenEffectFragment;

    invoke-static {v0}, Lcom/android/settings/display/ScreenEffectFragment;->ML(Lcom/android/settings/display/ScreenEffectFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    goto :goto_0
.end method
