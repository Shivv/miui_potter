.class Lcom/android/settings/display/t;
.super Landroid/preference/CheckBoxPreference;
.source "PaperModeSetAppsFragment.java"


# instance fields
.field final synthetic WK:Lcom/android/settings/display/PaperModeSetAppsFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/display/PaperModeSetAppsFragment;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Z)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/display/t;->WK:Lcom/android/settings/display/PaperModeSetAppsFragment;

    invoke-direct {p0, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/display/PaperModeSetAppsFragment;->Ob(Lcom/android/settings/display/PaperModeSetAppsFragment;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/t;->setTitle(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/android/settings/display/PaperModeSetAppsFragment;->Ob(Lcom/android/settings/display/PaperModeSetAppsFragment;)Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/t;->setIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/display/t;->setKey(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/t;->setPersistent(Z)V

    invoke-virtual {p0, p4}, Lcom/android/settings/display/t;->setChecked(Z)V

    invoke-virtual {p0, p1}, Lcom/android/settings/display/t;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method
