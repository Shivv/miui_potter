.class final Lcom/android/settings/display/ah;
.super Ljava/lang/Object;
.source "ResolutionListPreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic Yv:Lcom/android/settings/display/ResolutionListPreference;

.field final synthetic Yw:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/settings/display/ResolutionListPreference;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/ah;->Yv:Lcom/android/settings/display/ResolutionListPreference;

    iput-object p2, p0, Lcom/android/settings/display/ah;->Yw:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/ah;->Yv:Lcom/android/settings/display/ResolutionListPreference;

    invoke-static {v0, p2}, Lcom/android/settings/display/ResolutionListPreference;->OC(Lcom/android/settings/display/ResolutionListPreference;I)I

    iget-object v0, p0, Lcom/android/settings/display/ah;->Yv:Lcom/android/settings/display/ResolutionListPreference;

    invoke-virtual {v0}, Lcom/android/settings/display/ResolutionListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/ah;->Yw:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/display/ah;->Yw:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/display/ah;->Yv:Lcom/android/settings/display/ResolutionListPreference;

    invoke-virtual {v2}, Lcom/android/settings/display/ResolutionListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v2

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
