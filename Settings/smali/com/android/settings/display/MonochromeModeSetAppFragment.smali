.class public Lcom/android/settings/display/MonochromeModeSetAppFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MonochromeModeSetAppFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final TO:Ljava/util/HashSet;


# instance fields
.field private TP:Landroid/preference/PreferenceCategory;

.field private TQ:Landroid/preference/PreferenceCategory;

.field private TR:Landroid/content/BroadcastReceiver;

.field private TS:Ljava/util/HashMap;

.field private TT:Lmiui/os/AsyncTaskWithProgress;

.field private mContext:Landroid/content/Context;

.field private mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static synthetic -get0()Ljava/util/HashSet;
    .locals 1

    sget-object v0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TO:Ljava/util/HashSet;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/android/settings/display/l;->UJ:Ljava/util/HashSet;

    sput-object v0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TO:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/display/B;

    invoke-direct {v0, p0}, Lcom/android/settings/display/B;-><init>(Lcom/android/settings/display/MonochromeModeSetAppFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TR:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private Lg()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "screen_monochrome_mode_white_list"

    invoke-static {v1, v2}, Landroid/provider/MiuiSettings$ScreenEffect;->getScreenModePkgList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TS:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TT:Lmiui/os/AsyncTaskWithProgress;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/settings/display/C;

    invoke-virtual {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/android/settings/display/C;-><init>(Lcom/android/settings/display/MonochromeModeSetAppFragment;Landroid/app/FragmentManager;Ljava/util/List;)V

    iput-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TT:Lmiui/os/AsyncTaskWithProgress;

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TT:Lmiui/os/AsyncTaskWithProgress;

    const v1, 0x7f120e93

    invoke-virtual {v0, v1}, Lmiui/os/AsyncTaskWithProgress;->setMessage(I)Lmiui/os/AsyncTaskWithProgress;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmiui/os/AsyncTaskWithProgress;->setCancelable(Z)Lmiui/os/AsyncTaskWithProgress;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lmiui/os/AsyncTaskWithProgress;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method private Lh(Ljava/util/List;Ljava/util/List;)V
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TP:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TP:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TP:Landroid/preference/PreferenceCategory;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TQ:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TQ:Landroid/preference/PreferenceCategory;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method static synthetic Li(Lcom/android/settings/display/MonochromeModeSetAppFragment;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic Lj(Lcom/android/settings/display/MonochromeModeSetAppFragment;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic Lk(Lcom/android/settings/display/MonochromeModeSetAppFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TS:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic Ll(Lcom/android/settings/display/MonochromeModeSetAppFragment;Lmiui/os/AsyncTaskWithProgress;)Lmiui/os/AsyncTaskWithProgress;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TT:Lmiui/os/AsyncTaskWithProgress;

    return-object p1
.end method

.method static synthetic Lm(Lcom/android/settings/display/MonochromeModeSetAppFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->Lg()V

    return-void
.end method

.method static synthetic Ln(Lcom/android/settings/display/MonochromeModeSetAppFragment;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->Lh(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/display/MonochromeModeSetAppFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->mPackageManager:Landroid/content/pm/PackageManager;

    const v0, 0x7f15008c

    invoke-virtual {p0, v0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "monochrome_mode_pkg_list"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TP:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "monochrome_mode_off_pkg_list"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TQ:Landroid/preference/PreferenceCategory;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TR:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TR:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TT:Lmiui/os/AsyncTaskWithProgress;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TT:Lmiui/os/AsyncTaskWithProgress;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/os/AsyncTaskWithProgress;->cancel(Z)Z

    iput-object v2, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TT:Lmiui/os/AsyncTaskWithProgress;

    :cond_0
    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    instance-of v1, p1, Lcom/android/settings/display/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TS:Ljava/util/HashMap;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {v1, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/display/MonochromeModeSetAppFragment;->TS:Ljava/util/HashMap;

    const-string/jumbo v2, "screen_monochrome_mode_white_list"

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$ScreenEffect;->setScreenModePkgList(Landroid/content/Context;Ljava/util/HashMap;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->Lg()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/display/MonochromeModeSetAppFragment;->Lg()V

    return-void
.end method
