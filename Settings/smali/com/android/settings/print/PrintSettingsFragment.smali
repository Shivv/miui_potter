.class public Lcom/android/settings/print/PrintSettingsFragment;
.super Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;
.source "PrintSettingsFragment.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;


# instance fields
.field private bfi:Landroid/preference/PreferenceCategory;

.field private bfj:Landroid/widget/Button;

.field private bfk:Lcom/android/settings/print/d;

.field private bfl:Landroid/preference/PreferenceCategory;

.field private bfm:Lcom/android/settings/print/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/android/settings/print/-$Lambda$5syiqLuAdYwxmn3uilGCBf-wFq4;->$INST$0:Lcom/android/settings/print/-$Lambda$5syiqLuAdYwxmn3uilGCBf-wFq4;

    sput-object v0, Lcom/android/settings/print/PrintSettingsFragment;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    new-instance v0, Lcom/android/settings/print/q;

    invoke-direct {v0}, Lcom/android/settings/print/q;-><init>()V

    sput-object v0, Lcom/android/settings/print/PrintSettingsFragment;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private static aTA(Landroid/print/PrintJobInfo;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/print/PrintJobInfo;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private aTB()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "EXTRA_PRINT_SERVICE_COMPONENT_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "EXTRA_PRINT_SERVICE_COMPONENT_NAME"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/print/PrintSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->performClick(Landroid/preference/PreferenceScreen;)V

    :cond_1
    return-void
.end method

.method static synthetic aTC(Landroid/app/Activity;Lcom/android/settings/dashboard/C;)Lcom/android/settings/dashboard/D;
    .locals 2

    new-instance v0, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;

    new-instance v1, Lcom/android/settings/print/f;

    invoke-direct {v1, p0}, Lcom/android/settings/print/f;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, p1, v1}, Lcom/android/settings/print/PrintSettingsFragment$PrintSummaryProvider;-><init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;Lcom/android/settings/print/f;)V

    return-object v0
.end method

.method static synthetic aTD(Lcom/android/settings/print/PrintSettingsFragment;)Landroid/preference/PreferenceCategory;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfi:Landroid/preference/PreferenceCategory;

    return-object v0
.end method

.method static synthetic aTE(Lcom/android/settings/print/PrintSettingsFragment;)Landroid/preference/PreferenceCategory;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfl:Landroid/preference/PreferenceCategory;

    return-object v0
.end method

.method static synthetic aTF(Lcom/android/settings/print/PrintSettingsFragment;)Landroid/content/Context;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->bWz()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aTG(Lcom/android/settings/print/PrintSettingsFragment;)Landroid/preference/Preference;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/print/PrintSettingsFragment;->aTz()Landroid/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aTH(Landroid/print/PrintJobInfo;)Z
    .locals 1

    invoke-static {p0}, Lcom/android/settings/print/PrintSettingsFragment;->aTA(Landroid/print/PrintJobInfo;)Z

    move-result v0

    return v0
.end method

.method private aTy()Landroid/content/Intent;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "print_service_search_uri"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1
.end method

.method private aTz()Landroid/preference/Preference;
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/print/PrintSettingsFragment;->aTy()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    new-instance v1, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v2, 0x7f120d0e

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(I)V

    const v2, 0x7f080202

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIcon(I)V

    const v2, 0x7ffffffe

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOrder(I)V

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setPersistent(Z)V

    return-object v1
.end method


# virtual methods
.method protected Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d0168

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected aq()I
    .locals 1

    const v0, 0x7f120810

    return v0
.end method

.method protected ayh()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "android.settings.ACTION_PRINT_SETTINGS"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x50

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfj:Landroid/widget/Button;

    if-ne v0, p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/print/PrintSettingsFragment;->aTy()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/settings/print/PrintSettingsFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "PrintSettingsFragment"

    const-string/jumbo v2, "Unable to start activity"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->bWP()V

    const v0, 0x7f1500a9

    invoke-virtual {p0, v0}, Lcom/android/settings/print/PrintSettingsFragment;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "print_jobs_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/print/PrintSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfi:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "print_services_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/print/PrintSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfl:Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfi:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    new-instance v0, Lcom/android/settings/print/d;

    invoke-direct {v0, p0, v4}, Lcom/android/settings/print/d;-><init>(Lcom/android/settings/print/PrintSettingsFragment;Lcom/android/settings/print/d;)V

    iput-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfk:Lcom/android/settings/print/d;

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfk:Lcom/android/settings/print/d;

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v4, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    new-instance v0, Lcom/android/settings/print/c;

    invoke-direct {v0, p0, v4}, Lcom/android/settings/print/c;-><init>(Lcom/android/settings/print/PrintSettingsFragment;Lcom/android/settings/print/c;)V

    iput-object v0, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfm:Lcom/android/settings/print/c;

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfm:Lcom/android/settings/print/c;

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v4, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-object v1
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->onStart()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/print/PrintSettingsFragment;->setHasOptionsMenu(Z)V

    invoke-direct {p0}, Lcom/android/settings/print/PrintSettingsFragment;->aTB()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->onStop()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/android/settings/print/PrintSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0d00a8

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v1, 0x7f0a028e

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f120d12

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/android/settings/print/PrintSettingsFragment;->aTy()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    const v1, 0x7f0a0039

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfj:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfj:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/settings/print/PrintSettingsFragment;->bfj:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lcom/android/settings/print/PrintSettingsFragment;->bWE(Landroid/view/View;)V

    return-void
.end method
