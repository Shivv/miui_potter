.class Lcom/android/settings/print/b;
.super Landroid/content/Loader;
.source "PrintServiceSettingsFragment.java"


# instance fields
.field private bfg:Landroid/print/PrinterDiscoverySession;

.field private final bfh:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/content/Loader;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/print/b;->bfh:Ljava/util/Map;

    return-void
.end method

.method private aTu()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->isPrinterDiscoveryStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->stopPrinterDiscovery()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private aTw()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/print/b;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "print"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrintManager;

    invoke-virtual {v0}, Landroid/print/PrintManager;->createPrinterDiscoverySession()Landroid/print/PrinterDiscoverySession;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    iget-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    new-instance v1, Lcom/android/settings/print/i;

    invoke-direct {v1, p0}, Lcom/android/settings/print/i;-><init>(Lcom/android/settings/print/b;)V

    invoke-virtual {v0, v1}, Landroid/print/PrinterDiscoverySession;->setOnPrintersChangeListener(Landroid/print/PrinterDiscoverySession$OnPrintersChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0, v2}, Landroid/print/PrinterDiscoverySession;->startPrinterDiscovery(Ljava/util/List;)V

    return-void
.end method

.method static synthetic aTx(Lcom/android/settings/print/b;)Landroid/print/PrinterDiscoverySession;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    return-object v0
.end method


# virtual methods
.method public aTv(Ljava/util/List;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/print/b;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/content/Loader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/settings/print/b;->aTv(Ljava/util/List;)V

    return-void
.end method

.method protected onAbandon()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/print/b;->onStopLoading()V

    return-void
.end method

.method protected onCancelLoad()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/print/b;->aTu()Z

    move-result v0

    return v0
.end method

.method protected onForceLoad()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/print/b;->aTw()V

    return-void
.end method

.method protected onReset()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/print/b;->onStopLoading()V

    iget-object v0, p0, Lcom/android/settings/print/b;->bfh:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->destroy()V

    iput-object v1, p0, Lcom/android/settings/print/b;->bfg:Landroid/print/PrinterDiscoverySession;

    :cond_0
    return-void
.end method

.method protected onStartLoading()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/print/b;->bfh:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/print/b;->bfh:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/print/b;->aTv(Ljava/util/List;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/print/b;->onForceLoad()V

    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/print/b;->onCancelLoad()Z

    return-void
.end method
