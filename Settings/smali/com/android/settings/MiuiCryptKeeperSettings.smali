.class public Lcom/android/settings/MiuiCryptKeeperSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiCryptKeeperSettings.java"


# instance fields
.field private bQZ:Landroid/app/AlertDialog;

.field private bRa:Landroid/app/AlertDialog;

.field private bRb:Landroid/widget/Button;

.field private bRc:Landroid/view/View$OnClickListener;

.field private bRd:Landroid/content/IntentFilter;

.field private bRe:Landroid/content/BroadcastReceiver;

.field private bRf:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/iU;

    invoke-direct {v0, p0}, Lcom/android/settings/iU;-><init>(Lcom/android/settings/MiuiCryptKeeperSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRe:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/settings/iV;

    invoke-direct {v0, p0}, Lcom/android/settings/iV;-><init>(Lcom/android/settings/MiuiCryptKeeperSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRc:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic bJA(Lcom/android/settings/MiuiCryptKeeperSettings;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRb:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic bJB(Lcom/android/settings/MiuiCryptKeeperSettings;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRf:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bJC(Lcom/android/settings/MiuiCryptKeeperSettings;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bQZ:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic bJD(Lcom/android/settings/MiuiCryptKeeperSettings;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiCryptKeeperSettings;->bJx(I)Z

    move-result v0

    return v0
.end method

.method private bJx(I)Z
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/android/settings/MiuiCryptKeeperSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Lcom/android/settings/cx;

    invoke-virtual {p0}, Lcom/android/settings/MiuiCryptKeeperSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v2, v0, p0}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    invoke-virtual {v2}, Lcom/android/settings/cx;->bSR()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    invoke-direct {p0, v4, v0}, Lcom/android/settings/MiuiCryptKeeperSettings;->bJy(ILjava/lang/String;)V

    return v4

    :cond_0
    invoke-virtual {v2}, Lcom/android/settings/cx;->bSR()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v0

    const/high16 v3, 0x10000

    if-ne v0, v3, :cond_1

    const v0, 0x7f1209a7

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, p1, v0, v4}, Lcom/android/settings/cx;->bSQ(ILjava/lang/CharSequence;Z)Z

    move-result v0

    return v0

    :cond_1
    const v0, 0x7f1209a4

    goto :goto_0
.end method

.method private bJy(ILjava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/MiuiCryptKeeperSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1204ab

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1204b0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/iW;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/iW;-><init>(Lcom/android/settings/MiuiCryptKeeperSettings;ILjava/lang/String;)V

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRa:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRa:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method static synthetic bJz(Lcom/android/settings/MiuiCryptKeeperSettings;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bQZ:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/MiuiCryptKeeperSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const v0, 0x7f0d0080

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiCryptKeeperSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiCryptKeeperSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/android/internal/widget/LockPatternUtils;->isDeviceEncryptionEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    sget-boolean v2, Lmiui/os/Build;->HAS_CUST_PARTITION:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const-string/jumbo v2, "crypt_keeper_decrypt_methods_summary"

    invoke-virtual {p0, v2}, Lcom/android/settings/MiuiCryptKeeperSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-eqz v2, :cond_0

    const v3, 0x7f1204a6

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    :cond_0
    const-string/jumbo v2, "android.app.action.START_ENCRYPTION"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "device_policy"

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getStorageEncryptionStatus()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v1, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x37

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    if-ne p2, v1, :cond_1

    if-eqz p3, :cond_1

    const-string/jumbo v0, "type"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string/jumbo v0, "password"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/android/settings/MiuiCryptKeeperSettings;->bJy(ILjava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRa:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRa:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRa:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bQZ:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bQZ:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bQZ:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    iput-object v1, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRa:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bQZ:Landroid/app/AlertDialog;

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d0077

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiCryptKeeperSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRe:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiCryptKeeperSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRe:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRd:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRd:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRd:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const v0, 0x7f0a020c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRb:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRb:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRc:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRb:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    const v0, 0x7f0a0512

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/MiuiCryptKeeperSettings;->bRf:Landroid/widget/TextView;

    return-void
.end method
