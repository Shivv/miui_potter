.class public Lcom/android/settings/bF;
.super Landroid/widget/ArrayAdapter;
.source "MiuiAppPicker.java"


# instance fields
.field private final bSV:Landroid/view/LayoutInflater;

.field private final bSW:Ljava/util/List;

.field final synthetic bSX:Lcom/android/settings/MiuiAppPicker;


# direct methods
.method public constructor <init>(Lcom/android/settings/MiuiAppPicker;Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/android/settings/bF;->bSX:Lcom/android/settings/MiuiAppPicker;

    invoke-direct {p0, p2, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bF;->bSW:Ljava/util/List;

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/settings/bF;->bSV:Landroid/view/LayoutInflater;

    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget v4, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v5, 0x3e8

    if-ne v4, v5, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v4, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x2

    if-nez v4, :cond_2

    const-string/jumbo v4, "user"

    sget-object v5, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_2
    new-instance v4, Lcom/android/settings/bE;

    invoke-direct {v4, p1}, Lcom/android/settings/bE;-><init>(Lcom/android/settings/MiuiAppPicker;)V

    iput-object v0, v4, Lcom/android/settings/bE;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v4, Lcom/android/settings/bE;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1}, Lcom/android/settings/MiuiAppPicker;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/android/settings/bE;->bST:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/settings/bF;->bSW:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/bF;->bSW:Ljava/util/List;

    invoke-static {}, Lcom/android/settings/MiuiAppPicker;->bMf()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v0, Lcom/android/settings/bE;

    invoke-direct {v0, p1}, Lcom/android/settings/bE;-><init>(Lcom/android/settings/MiuiAppPicker;)V

    const v1, 0x7f120b8c

    invoke-virtual {p2, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lcom/android/settings/bE;->bST:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/bF;->bSW:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/bF;->bSW:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/android/settings/bF;->addAll(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x8

    iget-object v0, p0, Lcom/android/settings/bF;->bSV:Landroid/view/LayoutInflater;

    invoke-static {v0, p2}, Lcom/android/settings/applications/AppViewHolder;->sm(Landroid/view/LayoutInflater;Landroid/view/View;)Lcom/android/settings/applications/AppViewHolder;

    move-result-object v1

    iget-object v2, v1, Lcom/android/settings/applications/AppViewHolder;->uk:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/android/settings/bF;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bE;

    iget-object v3, v1, Lcom/android/settings/applications/AppViewHolder;->ul:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/android/settings/bE;->bST:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v0, Lcom/android/settings/bE;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/android/settings/applications/AppViewHolder;->um:Landroid/widget/ImageView;

    iget-object v4, v0, Lcom/android/settings/bE;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v5, p0, Lcom/android/settings/bF;->bSX:Lcom/android/settings/MiuiAppPicker;

    invoke-virtual {v5}, Lcom/android/settings/MiuiAppPicker;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, v1, Lcom/android/settings/applications/AppViewHolder;->um:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, v1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/android/settings/bE;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, v1, Lcom/android/settings/applications/AppViewHolder;->un:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    return-object v2

    :cond_0
    iget-object v0, v1, Lcom/android/settings/applications/AppViewHolder;->um:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, v1, Lcom/android/settings/applications/AppViewHolder;->um:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, v1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
