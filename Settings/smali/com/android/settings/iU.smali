.class final Lcom/android/settings/iU;
.super Landroid/content/BroadcastReceiver;
.source "MiuiCryptKeeperSettings.java"


# instance fields
.field final synthetic coY:Lcom/android/settings/MiuiCryptKeeperSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiCryptKeeperSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/iU;->coY:Lcom/android/settings/MiuiCryptKeeperSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "level"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v3, "plugged"

    invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string/jumbo v3, "invalid_charger"

    invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string/jumbo v3, "vold.pfe"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "activated"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    const/16 v3, 0x50

    if-lt v2, v3, :cond_2

    move v3, v0

    :goto_0
    and-int/lit8 v2, v4, 0x7

    if-eqz v2, :cond_4

    if-nez v5, :cond_3

    :goto_1
    iget-object v2, p0, Lcom/android/settings/iU;->coY:Lcom/android/settings/MiuiCryptKeeperSettings;

    invoke-static {v2}, Lcom/android/settings/MiuiCryptKeeperSettings;->bJA(Lcom/android/settings/MiuiCryptKeeperSettings;)Landroid/widget/Button;

    move-result-object v4

    if-eqz v3, :cond_5

    if-eqz v0, :cond_5

    xor-int/lit8 v2, v6, 0x1

    :goto_2
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/iU;->coY:Lcom/android/settings/MiuiCryptKeeperSettings;

    invoke-static {v2}, Lcom/android/settings/MiuiCryptKeeperSettings;->bJB(Lcom/android/settings/MiuiCryptKeeperSettings;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v3, :cond_6

    const v2, 0x7f1204b8

    :goto_3
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/iU;->coY:Lcom/android/settings/MiuiCryptKeeperSettings;

    invoke-static {v2}, Lcom/android/settings/MiuiCryptKeeperSettings;->bJB(Lcom/android/settings/MiuiCryptKeeperSettings;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    move v3, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_2

    :cond_6
    const v2, 0x7f1204b2

    goto :goto_3
.end method
