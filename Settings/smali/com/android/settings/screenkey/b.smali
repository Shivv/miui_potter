.class Lcom/android/settings/screenkey/b;
.super Landroid/widget/BaseAdapter;
.source "ScreenKeySettings.java"


# instance fields
.field final synthetic aVg:Lcom/android/settings/screenkey/ScreenKeySettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/screenkey/ScreenKeySettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/screenkey/b;->aVg:Lcom/android/settings/screenkey/ScreenKeySettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/screenkey/ScreenKeySettings;Lcom/android/settings/screenkey/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/screenkey/b;-><init>(Lcom/android/settings/screenkey/ScreenKeySettings;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/screenkey/b;->aVg:Lcom/android/settings/screenkey/ScreenKeySettings;

    invoke-static {v0}, Lcom/android/settings/screenkey/ScreenKeySettings;->aIZ(Lcom/android/settings/screenkey/ScreenKeySettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/screenkey/b;->aVg:Lcom/android/settings/screenkey/ScreenKeySettings;

    invoke-static {v0}, Lcom/android/settings/screenkey/ScreenKeySettings;->aIZ(Lcom/android/settings/screenkey/ScreenKeySettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/screenkey/b;->aVg:Lcom/android/settings/screenkey/ScreenKeySettings;

    invoke-static {v0}, Lcom/android/settings/screenkey/ScreenKeySettings;->aIZ(Lcom/android/settings/screenkey/ScreenKeySettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/screenkey/b;->aVg:Lcom/android/settings/screenkey/ScreenKeySettings;

    invoke-static {v0}, Lcom/android/settings/screenkey/ScreenKeySettings;->aIZ(Lcom/android/settings/screenkey/ScreenKeySettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, p0, Lcom/android/settings/screenkey/b;->aVg:Lcom/android/settings/screenkey/ScreenKeySettings;

    invoke-virtual {v0}, Lcom/android/settings/screenkey/ScreenKeySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0d01dd

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a0155

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/screenkey/b;->aVg:Lcom/android/settings/screenkey/ScreenKeySettings;

    invoke-static {v3}, Lcom/android/settings/screenkey/ScreenKeySettings;->aJa(Lcom/android/settings/screenkey/ScreenKeySettings;)Landroid/widget/SortableListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SortableListView;->getListenerForStartingSort()Landroid/view/View$OnTouchListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0a03ac

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/android/settings/screenkey/c;->aJc(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0a03ab

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1}, Lcom/android/settings/screenkey/c;->aJd(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object v2
.end method
