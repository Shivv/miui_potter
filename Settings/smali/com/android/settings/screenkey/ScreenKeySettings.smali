.class public Lcom/android/settings/screenkey/ScreenKeySettings;
.super Lcom/android/settings/StatusBarSettingsPreferenceFragment;
.source "ScreenKeySettings.java"

# interfaces
.implements Landroid/widget/SortableListView$OnOrderChangedListener;


# instance fields
.field private aVa:Lcom/android/settings/screenkey/b;

.field private aVb:Ljava/util/ArrayList;

.field private aVc:Landroid/widget/SortableListView;

.field private aVd:Lcom/android/settings/screenkey/c;

.field private aVe:Landroid/preference/PreferenceGroup;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/StatusBarSettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVb:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic aIY(Lcom/android/settings/screenkey/ScreenKeySettings;)Lcom/android/settings/screenkey/b;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVa:Lcom/android/settings/screenkey/b;

    return-object v0
.end method

.method static synthetic aIZ(Lcom/android/settings/screenkey/ScreenKeySettings;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVb:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic aJa(Lcom/android/settings/screenkey/ScreenKeySettings;)Landroid/widget/SortableListView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVc:Landroid/widget/SortableListView;

    return-object v0
.end method

.method static synthetic aJb(Lcom/android/settings/screenkey/ScreenKeySettings;Landroid/widget/SortableListView;)Landroid/widget/SortableListView;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVc:Landroid/widget/SortableListView;

    return-object p1
.end method


# virtual methods
.method public OnOrderChanged(II)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVb:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVb:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVb:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVd:Lcom/android/settings/screenkey/c;

    iget-object v1, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVb:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/settings/screenkey/c;->aJe(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVa:Lcom/android/settings/screenkey/b;

    invoke-virtual {v0}, Lcom/android/settings/screenkey/b;->notifyDataSetChanged()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVb:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/screenkey/ScreenKeySettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/StatusBarSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f1500e9

    invoke-virtual {p0, v0}, Lcom/android/settings/screenkey/ScreenKeySettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/screenkey/ScreenKeySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->getScreenKeyOrder(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVb:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/settings/screenkey/c;

    invoke-virtual {p0}, Lcom/android/settings/screenkey/ScreenKeySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/screenkey/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVd:Lcom/android/settings/screenkey/c;

    iget-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVd:Lcom/android/settings/screenkey/c;

    iget-object v1, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVb:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/settings/screenkey/c;->aJe(Ljava/util/ArrayList;)V

    new-instance v0, Lcom/android/settings/screenkey/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/screenkey/b;-><init>(Lcom/android/settings/screenkey/ScreenKeySettings;Lcom/android/settings/screenkey/b;)V

    iput-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVa:Lcom/android/settings/screenkey/b;

    const-string/jumbo v0, "key_position_custom"

    invoke-virtual {p0, v0}, Lcom/android/settings/screenkey/ScreenKeySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVe:Landroid/preference/PreferenceGroup;

    iget-object v0, p0, Lcom/android/settings/screenkey/ScreenKeySettings;->aVe:Landroid/preference/PreferenceGroup;

    new-instance v1, Lcom/android/settings/screenkey/a;

    invoke-virtual {p0}, Lcom/android/settings/screenkey/ScreenKeySettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/settings/screenkey/a;-><init>(Lcom/android/settings/screenkey/ScreenKeySettings;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addItemFromInflater(Landroid/preference/Preference;)V

    return-void
.end method
