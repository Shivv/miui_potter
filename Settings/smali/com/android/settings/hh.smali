.class final Lcom/android/settings/hh;
.super Ljava/lang/Object;
.source "MutedVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# instance fields
.field final synthetic cmJ:Lcom/android/settings/MutedVideoView;


# direct methods
.method constructor <init>(Lcom/android/settings/MutedVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hh;->cmJ:Lcom/android/settings/MutedVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/android/settings/hh;->cmJ:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAv(Lcom/android/settings/MutedVideoView;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/hh;->cmJ:Lcom/android/settings/MutedVideoView;

    invoke-static {v0, v3}, Lcom/android/settings/MutedVideoView;->bAM(Lcom/android/settings/MutedVideoView;I)I

    iget-object v0, p0, Lcom/android/settings/hh;->cmJ:Lcom/android/settings/MutedVideoView;

    invoke-static {v0, v3}, Lcom/android/settings/MutedVideoView;->bAQ(Lcom/android/settings/MutedVideoView;I)I

    iget-object v0, p0, Lcom/android/settings/hh;->cmJ:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/hh;->cmJ:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/hh;->cmJ:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAz(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/hh;->cmJ:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAz(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/hh;->cmJ:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAx(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v0

    if-eqz v0, :cond_1

    return v4

    :cond_1
    return v4
.end method
