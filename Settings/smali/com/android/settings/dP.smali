.class final Lcom/android/settings/dP;
.super Ljava/lang/Object;
.source "TrustedCredentialsDialogBuilder.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic chO:Lcom/android/settings/A;


# direct methods
.method constructor <init>(Lcom/android/settings/A;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dP;->chO:Lcom/android/settings/A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dP;->chO:Lcom/android/settings/A;

    invoke-static {v0}, Lcom/android/settings/A;->bjV(Lcom/android/settings/A;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/dP;->chO:Lcom/android/settings/A;

    invoke-static {v0}, Lcom/android/settings/A;->bjV(Lcom/android/settings/A;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/dP;->chO:Lcom/android/settings/A;

    invoke-static {v1}, Lcom/android/settings/A;->bjT(Lcom/android/settings/A;)Landroid/view/View;

    move-result-object v1

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p0, Lcom/android/settings/dP;->chO:Lcom/android/settings/A;

    invoke-static {v0}, Lcom/android/settings/A;->bjT(Lcom/android/settings/A;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/dP;->chO:Lcom/android/settings/A;

    invoke-static {v1}, Lcom/android/settings/A;->bjS(Lcom/android/settings/A;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x10c000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method
