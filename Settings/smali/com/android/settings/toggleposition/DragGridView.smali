.class public Lcom/android/settings/toggleposition/DragGridView;
.super Landroid/widget/GridView;
.source "DragGridView.java"


# instance fields
.field private bjX:I

.field private bjY:I

.field private bjZ:I

.field private bkA:I

.field private bkB:Landroid/os/Vibrator;

.field private bkC:Landroid/view/WindowManager$LayoutParams;

.field private bkD:Landroid/view/WindowManager;

.field private bkE:I

.field private bkF:I

.field private bka:Z

.field private bkb:I

.field private bkc:I

.field bkd:Ljava/lang/Runnable;

.field private bke:I

.field private bkf:I

.field private bkg:Lcom/android/settings/toggleposition/b;

.field private bkh:Landroid/graphics/Bitmap;

.field private bki:Landroid/widget/ImageView;

.field private bkj:J

.field private bkk:I

.field private bkl:I

.field private bkm:Landroid/os/Handler;

.field private bkn:I

.field private bko:Z

.field private bkp:I

.field private bkq:Ljava/lang/Runnable;

.field private bkr:I

.field private bks:Z

.field private bkt:I

.field private bku:I

.field private bkv:I

.field private bkw:I

.field private bkx:Landroid/view/View;

.field private bky:I

.field private bkz:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/toggleposition/DragGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/toggleposition/DragGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkj:J

    iput-boolean v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bko:Z

    iput v4, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    iput v4, p0, Lcom/android/settings/toggleposition/DragGridView;->bkk:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    iput-boolean v3, p0, Lcom/android/settings/toggleposition/DragGridView;->bka:Z

    iput v3, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkm:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/toggleposition/e;

    invoke-direct {v0, p0}, Lcom/android/settings/toggleposition/e;-><init>(Lcom/android/settings/toggleposition/DragGridView;)V

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkq:Ljava/lang/Runnable;

    iput v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkb:I

    iput v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bjY:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bjX:I

    const/4 v0, -0x2

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bjZ:I

    new-instance v0, Lcom/android/settings/toggleposition/f;

    invoke-direct {v0, p0}, Lcom/android/settings/toggleposition/f;-><init>(Lcom/android/settings/toggleposition/DragGridView;)V

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkd:Ljava/lang/Runnable;

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    iget v3, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/settings/toggleposition/DragGridView;->setPaddingRelative(IIII)V

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/DragGridView;->setVerticalSpacing(I)V

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/DragGridView;->setHorizontalSpacing(I)V

    const-string/jumbo v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkB:Landroid/os/Vibrator;

    const-string/jumbo v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkD:Landroid/view/WindowManager;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070225

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bky:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060115

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkz:I

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkA:I

    iget-boolean v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bks:Z

    if-nez v0, :cond_0

    iput v4, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    :cond_0
    return-void
.end method

.method static synthetic aYA(Lcom/android/settings/toggleposition/DragGridView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/toggleposition/DragGridView;->bko:Z

    return p1
.end method

.method static synthetic aYB(Lcom/android/settings/toggleposition/DragGridView;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/toggleposition/DragGridView;->aYg(II)V

    return-void
.end method

.method static synthetic aYC(Lcom/android/settings/toggleposition/DragGridView;Landroid/graphics/Bitmap;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/toggleposition/DragGridView;->aYi(Landroid/graphics/Bitmap;II)V

    return-void
.end method

.method private aYg(II)V
    .locals 7

    const/4 v0, 0x0

    const/4 v3, 0x0

    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    if-eqz v0, :cond_3

    :goto_0
    if-ge p1, p2, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/DragGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, p1, 0x1

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    rem-int/2addr v0, v2

    if-nez v0, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v0, v2

    int-to-float v2, v0

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/toggleposition/DragGridView;->aYj(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v2, v0

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/toggleposition/DragGridView;->aYj(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    :goto_2
    if-le p1, p2, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/DragGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_4

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    add-int/2addr v0, p1

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    rem-int/2addr v0, v2

    if-nez v0, :cond_5

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v0, v2

    int-to-float v2, v0

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v4, v0

    move-object v0, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/toggleposition/DragGridView;->aYj(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_3
    add-int/lit8 p1, p1, -0x1

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v2, v0

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/toggleposition/DragGridView;->aYj(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lcom/android/settings/toggleposition/h;

    invoke-direct {v1, p0}, Lcom/android/settings/toggleposition/h;-><init>(Lcom/android/settings/toggleposition/DragGridView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method private static aYh(III)I
    .locals 1

    if-ge p0, p1, :cond_0

    return p1

    :cond_0
    if-lt p0, p2, :cond_1

    add-int/lit8 v0, p2, -0x1

    return v0

    :cond_1
    return p0
.end method

.method private aYi(Landroid/graphics/Bitmap;II)V
    .locals 4

    const/4 v3, -0x2

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkv:I

    sub-int v1, p2, v1

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkt:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkw:I

    sub-int v1, p3, v1

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bku:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bky:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x3f666666    # 0.9f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x18

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bki:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bki:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bki:Landroid/widget/ImageView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkD:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bki:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private aYj(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;
    .locals 6

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string/jumbo v0, "translationX"

    new-array v1, v3, [F

    aput p2, v1, v4

    aput p3, v1, v5

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-string/jumbo v1, "translationY"

    new-array v2, v3, [F

    aput p4, v2, v4

    aput p5, v2, v5

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    return-object v2
.end method

.method private aYk(Landroid/view/View;II)Z
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_0

    return v3

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    if-lt p2, v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    if-le p2, v0, :cond_2

    :cond_1
    return v3

    :cond_2
    if-lt p3, v1, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v1

    if-le p3, v0, :cond_4

    :cond_3
    return v3

    :cond_4
    const/4 v0, 0x1

    return v0
.end method

.method private aYl(II)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkv:I

    sub-int v1, p1, v1

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkt:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkw:I

    sub-int v1, p2, v1

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bku:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bky:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkD:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bki:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkC:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0, p1, p2}, Lcom/android/settings/toggleposition/DragGridView;->aYn(II)V

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkw:I

    sub-int v0, p2, v0

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkb:I

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bjX:I

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/toggleposition/DragGridView;->aYo()V

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bjX:I

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkb:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkd:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/DragGridView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkw:I

    sub-int v0, p2, v0

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkb:I

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bjZ:I

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/toggleposition/DragGridView;->aYo()V

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bjZ:I

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkb:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkd:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/DragGridView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bjY:I

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkb:I

    invoke-direct {p0}, Lcom/android/settings/toggleposition/DragGridView;->aYo()V

    goto :goto_0
.end method

.method private aYm()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/DragGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bjY:I

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkb:I

    invoke-direct {p0}, Lcom/android/settings/toggleposition/DragGridView;->aYo()V

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkg:Lcom/android/settings/toggleposition/b;

    invoke-interface {v0, v2}, Lcom/android/settings/toggleposition/b;->aYG(I)V

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkg:Lcom/android/settings/toggleposition/b;

    invoke-interface {v0}, Lcom/android/settings/toggleposition/b;->aYE()V

    invoke-direct {p0}, Lcom/android/settings/toggleposition/DragGridView;->aYp()V

    iput v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkk:I

    iput v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    return-void
.end method

.method private aYn(II)V
    .locals 3

    const/4 v1, -0x1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/toggleposition/DragGridView;->pointToPosition(II)I

    move-result v0

    invoke-direct {p0}, Lcom/android/settings/toggleposition/DragGridView;->getSortItemPosition()I

    move-result v2

    if-ne v0, v2, :cond_0

    move v0, v1

    :cond_0
    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    if-eq v0, v2, :cond_2

    if-eq v0, v1, :cond_2

    iget-boolean v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bka:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkg:Lcom/android/settings/toggleposition/b;

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    invoke-interface {v1, v2, v0}, Lcom/android/settings/toggleposition/b;->aYF(II)V

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/android/settings/toggleposition/DragGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkg:Lcom/android/settings/toggleposition/b;

    invoke-interface {v1, v0}, Lcom/android/settings/toggleposition/b;->aYG(I)V

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/android/settings/toggleposition/g;

    invoke-direct {v2, p0, v1, v0}, Lcom/android/settings/toggleposition/g;-><init>(Lcom/android/settings/toggleposition/DragGridView;Landroid/view/ViewTreeObserver;I)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    :cond_2
    return-void
.end method

.method private aYo()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkm:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkd:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private aYp()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bki:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkD:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bki:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bki:Landroid/widget/ImageView;

    :cond_0
    return-void
.end method

.method static synthetic aYq(Lcom/android/settings/toggleposition/DragGridView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bjY:I

    return v0
.end method

.method static synthetic aYr(Lcom/android/settings/toggleposition/DragGridView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkb:I

    return v0
.end method

.method static synthetic aYs(Lcom/android/settings/toggleposition/DragGridView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bke:I

    return v0
.end method

.method static synthetic aYt(Lcom/android/settings/toggleposition/DragGridView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkf:I

    return v0
.end method

.method static synthetic aYu(Lcom/android/settings/toggleposition/DragGridView;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkh:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic aYv(Lcom/android/settings/toggleposition/DragGridView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    return v0
.end method

.method static synthetic aYw(Lcom/android/settings/toggleposition/DragGridView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    return-object v0
.end method

.method static synthetic aYx(Lcom/android/settings/toggleposition/DragGridView;)Landroid/os/Vibrator;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkB:Landroid/os/Vibrator;

    return-object v0
.end method

.method static synthetic aYy(Lcom/android/settings/toggleposition/DragGridView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/toggleposition/DragGridView;->bka:Z

    return p1
.end method

.method static synthetic aYz(Lcom/android/settings/toggleposition/DragGridView;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    return p1
.end method

.method private getSortItemPosition()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v2, -0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/GridView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bke:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkf:I

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bke:I

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkf:I

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/toggleposition/DragGridView;->pointToPosition(II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkk:I

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkk:I

    invoke-direct {p0}, Lcom/android/settings/toggleposition/DragGridView;->getSortItemPosition()I

    move-result v1

    if-ne v0, v1, :cond_1

    iput v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkk:I

    :cond_1
    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkk:I

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    if-ne v0, v2, :cond_2

    invoke-super {p0, p1}, Landroid/widget/GridView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkm:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkq:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkj:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkl:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/DragGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkf:I

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkw:I

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bke:I

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkv:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkf:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bku:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bke:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkt:I

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkh:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    goto/16 :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-direct {p0, v2, v0, v1}, Lcom/android/settings/toggleposition/DragGridView;->aYk(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkf:I

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkA:I

    if-le v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bko:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkm:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkq:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkm:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkq:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkm:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkd:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/android/settings/toggleposition/DragGridView;->aYm()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bko:Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    const/4 v1, 0x0

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/widget/GridView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0, v6}, Lcom/android/settings/toggleposition/DragGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getChildCount()I

    move-result v0

    int-to-double v2, v0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v2, v4

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    int-to-double v4, v0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v13, v2

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkz:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    move v7, v6

    :goto_0
    if-gt v7, v13, :cond_0

    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v0

    mul-int/2addr v0, v7

    int-to-float v0, v0

    invoke-virtual {v12}, Landroid/view/View;->getY()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    mul-int/2addr v2, v7

    int-to-float v2, v2

    add-float/2addr v2, v0

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    invoke-virtual {v12}, Landroid/view/View;->getWidth()I

    move-result v3

    mul-int/2addr v0, v3

    iget v3, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    iget v4, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    int-to-float v3, v0

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_0
    move v0, v6

    :goto_1
    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    if-gt v0, v2, :cond_1

    invoke-virtual {v12}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    mul-int/2addr v3, v0

    add-int/2addr v2, v3

    int-to-float v7, v2

    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v2

    mul-int/2addr v2, v13

    iget v3, p0, Lcom/android/settings/toggleposition/DragGridView;->bkp:I

    mul-int/2addr v3, v13

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v12}, Landroid/view/View;->getY()F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    sub-float v10, v2, v3

    move-object v6, p1

    move v8, v1

    move v9, v7

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bko:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/GridView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    const/4 v0, 0x1

    const/4 v3, 0x0

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkc:I

    if-lez v1, :cond_3

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkc:I

    div-int v1, v2, v1

    if-lez v1, :cond_1

    :goto_0
    if-eq v1, v0, :cond_4

    iget v3, p0, Lcom/android/settings/toggleposition/DragGridView;->bkc:I

    mul-int/2addr v3, v1

    add-int/lit8 v4, v1, -0x1

    iget v5, p0, Lcom/android/settings/toggleposition/DragGridView;->bkn:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    if-le v3, v2, :cond_0

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :cond_1
    :goto_1
    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/GridView;->onMeasure(II)V

    return-void

    :cond_3
    const/4 v0, 0x2

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    iget-boolean v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bko:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bki:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkE:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkF:I

    iget v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bkE:I

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkv:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/android/settings/toggleposition/DragGridView;->bkv:I

    add-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lcom/android/settings/toggleposition/DragGridView;->aYh(III)I

    move-result v0

    iget v1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkF:I

    iget v2, p0, Lcom/android/settings/toggleposition/DragGridView;->bkw:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/DragGridView;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/android/settings/toggleposition/DragGridView;->bkx:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/android/settings/toggleposition/DragGridView;->bkw:I

    add-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Lcom/android/settings/toggleposition/DragGridView;->aYh(III)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/toggleposition/DragGridView;->aYl(II)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/settings/toggleposition/DragGridView;->aYm()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bko:Z

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/GridView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    instance-of v0, p1, Lcom/android/settings/toggleposition/b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/settings/toggleposition/b;

    iput-object p1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkg:Lcom/android/settings/toggleposition/b;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "the adapter must be implements DragGridAdapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setColumnWidth(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/GridView;->setColumnWidth(I)V

    iput p1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkc:I

    return-void
.end method

.method public setDragResponseMS(J)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkj:J

    return-void
.end method

.method public setHorizontalSpacing(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    iput p1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkn:I

    return-void
.end method

.method public setNumColumns(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/GridView;->setNumColumns(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/toggleposition/DragGridView;->bks:Z

    iput p1, p0, Lcom/android/settings/toggleposition/DragGridView;->bkr:I

    return-void
.end method
