.class public Lcom/android/settings/sound/VolumeSeekBarPreference;
.super Lcom/android/settings/MiuiSeekBarPreference;
.source "VolumeSeekBarPreference.java"


# instance fields
.field private MA:Lcom/android/settings/sound/b;

.field private MB:I

.field private MC:Z

.field private Mz:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/MiuiSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MB:I

    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->bzk(I)V

    return-void
.end method


# virtual methods
.method public GC(Lcom/android/settings/sound/b;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MA:Lcom/android/settings/sound/b;

    return-void
.end method

.method public GD()Lcom/android/settings/sound/b;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MA:Lcom/android/settings/sound/b;

    return-object v0
.end method

.method public GE()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->Mz:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->Mz:Landroid/view/View;

    const v1, 0x1020006

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->refreshDrawableState()V

    :cond_0
    return-void
.end method

.method public GF()I
    .locals 1

    iget v0, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MB:I

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSeekBarPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a03d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    const v1, 0x1020016

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getPaddingStart()I

    move-result v2

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingEnd()I

    move-result v4

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setPaddingRelative(IIII)V

    const v1, 0x1020006

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/settings/sound/VolumeStreamStateView;

    iget v2, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MB:I

    invoke-virtual {v1, v2}, Lcom/android/settings/sound/VolumeStreamStateView;->setStream(I)V

    invoke-virtual {v1}, Lcom/android/settings/sound/VolumeStreamStateView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getAlpha()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MA:Lcom/android/settings/sound/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MA:Lcom/android/settings/sound/b;

    invoke-virtual {v1, v0}, Lcom/android/settings/sound/b;->Gt(Landroid/widget/SeekBar;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->cqf()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settings/sound/e;

    invoke-direct {v0, p0}, Lcom/android/settings/sound/e;-><init>(Lcom/android/settings/sound/VolumeSeekBarPreference;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0d0154

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->Mz:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->Mz:Landroid/view/View;

    return-object v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->bzm(IZ)V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MC:Z

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MC:Z

    return-void
.end method

.method public setStream(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/sound/VolumeSeekBarPreference;->MB:I

    return-void
.end method
