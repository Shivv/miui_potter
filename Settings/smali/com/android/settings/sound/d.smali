.class public Lcom/android/settings/sound/d;
.super Ljava/lang/Object;
.source "RingerVolumeFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Ljava/lang/Runnable;
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# instance fields
.field private MP:Landroid/media/AudioManager;

.field private MQ:Landroid/net/Uri;

.field private MR:D

.field private MS:Landroid/os/Handler;

.field private MT:I

.field private MU:I

.field private MV:Landroid/media/MediaPlayer;

.field private MW:I

.field private MX:Z

.field private MY:Lmiui/widget/SeekBar;

.field private MZ:I

.field private Na:I

.field private Nb:Landroid/database/ContentObserver;

.field final synthetic Nc:Lcom/android/settings/sound/RingerVolumeFragment;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/settings/sound/RingerVolumeFragment;Landroid/content/Context;Lmiui/widget/SeekBar;I)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/sound/d;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;Landroid/content/Context;Lmiui/widget/SeekBar;ILandroid/net/Uri;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/sound/RingerVolumeFragment;Landroid/content/Context;Lmiui/widget/SeekBar;ILandroid/net/Uri;)V
    .locals 2

    const/4 v1, -0x1

    iput-object p1, p0, Lcom/android/settings/sound/d;->Nc:Lcom/android/settings/sound/RingerVolumeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/sound/d;->MS:Landroid/os/Handler;

    iput v1, p0, Lcom/android/settings/sound/d;->MT:I

    iput v1, p0, Lcom/android/settings/sound/d;->Na:I

    new-instance v0, Lcom/android/settings/sound/f;

    iget-object v1, p0, Lcom/android/settings/sound/d;->MS:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/sound/f;-><init>(Lcom/android/settings/sound/d;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/sound/d;->Nb:Landroid/database/ContentObserver;

    iput-object p2, p0, Lcom/android/settings/sound/d;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "audio"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    iput p4, p0, Lcom/android/settings/sound/d;->MZ:I

    iput-object p3, p0, Lcom/android/settings/sound/d;->MY:Lmiui/widget/SeekBar;

    iget-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    iget v1, p0, Lcom/android/settings/sound/d;->MZ:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMinVolume(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/sound/d;->MW:I

    iget-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    iget v1, p0, Lcom/android/settings/sound/d;->MZ:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/sound/d;->MU:I

    invoke-direct {p0, p3, p5}, Lcom/android/settings/sound/d;->GU(Lmiui/widget/SeekBar;Landroid/net/Uri;)V

    return-void
.end method

.method private GR()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/sound/d;->MX:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/sound/d;->MX:Z

    :cond_1
    return-void
.end method

.method private GU(Lmiui/widget/SeekBar;Landroid/net/Uri;)V
    .locals 4

    const/16 v0, 0x64

    invoke-virtual {p1, v0}, Lmiui/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    iget v1, p0, Lcom/android/settings/sound/d;->MZ:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double v0, v2, v0

    iput-wide v0, p0, Lcom/android/settings/sound/d;->MR:D

    iget-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    iget v1, p0, Lcom/android/settings/sound/d;->MZ:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/sound/d;->GS(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lmiui/widget/SeekBar;->setProgress(I)V

    invoke-virtual {p1, p0}, Lmiui/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Settings$System;->VOLUME_SETTINGS:[Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/sound/d;->MZ:I

    aget-object v1, v1, v2

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/sound/d;->Nb:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    if-nez p2, :cond_1

    iget v0, p0, Lcom/android/settings/sound/d;->MZ:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    sget-object p2, Landroid/provider/Settings$System;->DEFAULT_RINGTONE_URI:Landroid/net/Uri;

    :cond_0
    :goto_0
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/android/settings/sound/d;->Nc:Lcom/android/settings/sound/RingerVolumeFragment;

    invoke-static {v0}, Lcom/android/settings/sound/RingerVolumeFragment;->GO(Lcom/android/settings/sound/RingerVolumeFragment;)Landroid/net/Uri;

    move-result-object p2

    :cond_1
    iput-object p2, p0, Lcom/android/settings/sound/d;->MQ:Landroid/net/Uri;

    return-void

    :cond_2
    iget v0, p0, Lcom/android/settings/sound/d;->MZ:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    sget-object p2, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/settings/sound/d;->MZ:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    sget-object p2, Landroid/provider/Settings$System;->DEFAULT_ALARM_ALERT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method static synthetic Hb(Lcom/android/settings/sound/d;)Landroid/media/AudioManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic Hc(Lcom/android/settings/sound/d;)Lmiui/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/d;->MY:Lmiui/widget/SeekBar;

    return-object v0
.end method

.method static synthetic Hd(Lcom/android/settings/sound/d;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/sound/d;->MZ:I

    return v0
.end method

.method static synthetic He(Lcom/android/settings/sound/d;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/sound/d;->MT:I

    return p1
.end method

.method static synthetic Hf(Lcom/android/settings/sound/d;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    return-object p1
.end method


# virtual methods
.method GS(I)I
    .locals 4

    int-to-double v0, p1

    iget-wide v2, p0, Lcom/android/settings/sound/d;->MR:D

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iget-object v1, p0, Lcom/android/settings/sound/d;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "ringer_volume_progress"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    sget-object v2, Landroid/provider/Settings$System;->VOLUME_SETTINGS:[Ljava/lang/String;

    iget v3, p0, Lcom/android/settings/sound/d;->MZ:I

    aget-object v2, v2, v3

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/settings/sound/d;->GT(I)I

    move-result v2

    if-ne p1, v2, :cond_0

    return v1

    :cond_0
    return v0
.end method

.method GT(I)I
    .locals 6

    int-to-double v0, p1

    iget-wide v2, p0, Lcom/android/settings/sound/d;->MR:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/android/settings/sound/d;->MR:D

    div-double/2addr v0, v2

    double-to-int v0, v0

    if-lez p1, :cond_1

    if-nez v0, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x64

    if-ge p1, v1, :cond_0

    iget v1, p0, Lcom/android/settings/sound/d;->MU:I

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public GV()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method GW(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/sound/d;->GY(I)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MS:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MS:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public GX()V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lcom/android/settings/sound/d;->MZ:I

    invoke-static {v0}, Lcom/android/settings/az;->bug(I)I

    move-result v1

    iget v0, p0, Lcom/android/settings/sound/d;->MZ:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    iget v2, p0, Lcom/android/settings/sound/d;->MZ:I

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getDevicesForStream(I)I

    move-result v0

    and-int/lit8 v0, v0, 0xc

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/d;->Nc:Lcom/android/settings/sound/RingerVolumeFragment;

    invoke-virtual {v0}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v2, "config_safe_media_volume_index"

    const-string/jumbo v3, "integer"

    const-string/jumbo v4, "android"

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/sound/d;->Nc:Lcom/android/settings/sound/RingerVolumeFragment;

    invoke-virtual {v2}, Lcom/android/settings/sound/RingerVolumeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-le v1, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    iget v2, p0, Lcom/android/settings/sound/d;->MZ:I

    invoke-virtual {v1, v2, v0, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    iput v0, p0, Lcom/android/settings/sound/d;->MT:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method GY(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/sound/d;->GT(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/sound/d;->MT:I

    return-void
.end method

.method public GZ()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/sound/d;->Nc:Lcom/android/settings/sound/RingerVolumeFragment;

    invoke-virtual {v0, p0}, Lcom/android/settings/sound/RingerVolumeFragment;->GJ(Lcom/android/settings/sound/d;)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MQ:Landroid/net/Uri;

    if-nez v0, :cond_0

    iput-object v4, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    return-void

    :cond_0
    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/android/settings/sound/g;

    invoke-direct {v1, p0}, Lcom/android/settings/sound/g;-><init>(Lcom/android/settings/sound/d;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/android/settings/sound/d;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/sound/d;->MQ:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/android/settings/sound/d;->MZ:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/sound/d;->MX:Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iput-object v4, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    goto :goto_0

    :catch_1
    move-exception v0

    iput-object v4, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    goto :goto_0
.end method

.method public Ha()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/sound/d;->MV:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/sound/d;->GR()V

    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/d;->GR()V

    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6

    if-nez p3, :cond_0

    return-void

    :cond_0
    int-to-double v0, p2

    iget-wide v2, p0, Lcom/android/settings/sound/d;->MR:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/android/settings/sound/d;->MR:D

    div-double/2addr v0, v2

    iget v2, p0, Lcom/android/settings/sound/d;->MW:I

    int-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    iget v0, p0, Lcom/android/settings/sound/d;->MW:I

    invoke-virtual {p0, v0}, Lcom/android/settings/sound/d;->GS(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/sound/d;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "ringer_volume_progress"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Landroid/provider/Settings$System;->VOLUME_SETTINGS:[Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/sound/d;->MZ:I

    aget-object v1, v1, v2

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-virtual {p0, p2}, Lcom/android/settings/sound/d;->GW(I)V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/sound/d;->GV()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/sound/d;->GZ()V

    :cond_0
    return-void
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/sound/d;->MP:Landroid/media/AudioManager;

    iget v1, p0, Lcom/android/settings/sound/d;->MZ:I

    iget v2, p0, Lcom/android/settings/sound/d;->MT:I

    const/16 v3, 0x400

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method

.method public stop()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/sound/d;->Ha()V

    iget-object v0, p0, Lcom/android/settings/sound/d;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/d;->Nb:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/sound/d;->MY:Lmiui/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method
