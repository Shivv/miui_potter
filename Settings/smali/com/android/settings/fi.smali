.class final Lcom/android/settings/fi;
.super Ljava/lang/Object;
.source "ToggleAnimHelper.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field cjZ:Z

.field final synthetic cka:Lcom/android/settings/ap;

.field final synthetic ckb:Landroid/widget/ImageView;

.field final synthetic ckc:I


# direct methods
.method constructor <init>(Lcom/android/settings/ap;Landroid/widget/ImageView;I)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/fi;->cka:Lcom/android/settings/ap;

    iput-object p2, p0, Lcom/android/settings/fi;->ckb:Landroid/widget/ImageView;

    iput p3, p0, Lcom/android/settings/fi;->ckc:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/fi;->cjZ:Z

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/fi;->ckb:Landroid/widget/ImageView;

    sub-float v2, v3, v0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleX(F)V

    iget-object v1, p0, Lcom/android/settings/fi;->ckb:Landroid/widget/ImageView;

    sub-float v0, v3, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleY(F)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/android/settings/fi;->cjZ:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/android/settings/fi;->ckc:I

    iget-object v2, p0, Lcom/android/settings/fi;->ckb:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/settings/fi;->cka:Lcom/android/settings/ap;

    invoke-static {v3}, Lcom/android/settings/ap;->bqq(Lcom/android/settings/ap;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lmiui/app/ToggleManager;->updateImageView(ILandroid/widget/ImageView;I)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/fi;->cjZ:Z

    :cond_1
    iget-object v1, p0, Lcom/android/settings/fi;->ckb:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleX(F)V

    iget-object v1, p0, Lcom/android/settings/fi;->ckb:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleY(F)V

    goto :goto_0
.end method
