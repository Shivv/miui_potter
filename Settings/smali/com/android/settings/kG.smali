.class final Lcom/android/settings/kG;
.super Landroid/telephony/PhoneStateListener;
.source "WifiCallingSettings.java"


# instance fields
.field final synthetic crn:Lcom/android/settings/WifiCallingSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/WifiCallingSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/kG;->crn:Lcom/android/settings/WifiCallingSettings;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/kG;->crn:Lcom/android/settings/WifiCallingSettings;

    invoke-virtual {v0}, Lcom/android/settings/WifiCallingSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/kG;->crn:Lcom/android/settings/WifiCallingSettings;

    invoke-static {v1}, Lcom/android/settings/WifiCallingSettings;->bUH(Lcom/android/settings/WifiCallingSettings;)Lcom/android/ims/ImsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/ims/ImsManager;->isNonTtyOrTtyOnVolteEnabledForSlot()Z

    move-result v1

    invoke-virtual {v0}, Lcom/android/settings/bL;->bMF()Lcom/android/settings/widget/SwitchBar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/settings/widget/SwitchBar;->getSwitch()Lcom/android/settings/widget/ToggleSwitch;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/settings/widget/ToggleSwitch;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_4

    move v4, v1

    :goto_0
    if-nez p1, :cond_5

    :goto_1
    invoke-virtual {v5, v1}, Lcom/android/settings/widget/SwitchBar;->setEnabled(Z)V

    const-string/jumbo v1, "carrier_config"

    invoke-virtual {v0, v1}, Lcom/android/settings/bL;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string/jumbo v1, "editable_wfc_mode_bool"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string/jumbo v5, "editable_wfc_roaming_mode_bool"

    invoke-virtual {v0, v5}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_2
    iget-object v5, p0, Lcom/android/settings/kG;->crn:Lcom/android/settings/WifiCallingSettings;

    invoke-virtual {v5}, Lcom/android/settings/WifiCallingSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const-string/jumbo v6, "wifi_calling_mode"

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    if-eqz v5, :cond_1

    if-eqz v4, :cond_7

    if-eqz v1, :cond_7

    if-nez p1, :cond_6

    move v1, v3

    :goto_3
    invoke-virtual {v5, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_1
    iget-object v1, p0, Lcom/android/settings/kG;->crn:Lcom/android/settings/WifiCallingSettings;

    invoke-virtual {v1}, Lcom/android/settings/WifiCallingSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v5, "wifi_calling_roaming_mode"

    invoke-virtual {v1, v5}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_3

    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    move v2, v3

    :cond_2
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_3
    return-void

    :cond_4
    move v4, v2

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_3

    :cond_8
    move v0, v2

    move v1, v3

    goto :goto_2
.end method
