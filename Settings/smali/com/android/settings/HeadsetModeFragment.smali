.class public Lcom/android/settings/HeadsetModeFragment;
.super Lcom/android/settings/SettingsBasePreferenceFragment;
.source "HeadsetModeFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private cbj:Ljava/lang/String;

.field private cbk:Lmiui/preference/RadioButtonPreference;

.field private cbl:Lmiui/preference/RadioButtonPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsBasePreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/HeadsetModeFragment;->cbj:Ljava/lang/String;

    return-void
.end method

.method private bTr()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/settings/cd;->bQo()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/HeadsetModeFragment;->cbj:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iput-object v0, p0, Lcom/android/settings/HeadsetModeFragment;->cbj:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/HeadsetModeFragment;->cbl:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lmiui/preference/RadioButtonPreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/android/settings/HeadsetModeFragment;->cbk:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v1, v2}, Lmiui/preference/RadioButtonPreference;->setChecked(Z)V

    const-string/jumbo v1, "music"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/HeadsetModeFragment;->cbk:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0, v3}, Lmiui/preference/RadioButtonPreference;->setChecked(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/HeadsetModeFragment;->cbl:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0, v3}, Lmiui/preference/RadioButtonPreference;->setChecked(Z)V

    goto :goto_0
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/HeadsetModeFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/SettingsBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15008f

    invoke-virtual {p0, v0}, Lcom/android/settings/HeadsetModeFragment;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "mode_volume"

    invoke-virtual {p0, v0}, Lcom/android/settings/HeadsetModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/HeadsetModeFragment;->cbl:Lmiui/preference/RadioButtonPreference;

    const-string/jumbo v0, "mode_music"

    invoke-virtual {p0, v0}, Lcom/android/settings/HeadsetModeFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/HeadsetModeFragment;->cbk:Lmiui/preference/RadioButtonPreference;

    iget-object v0, p0, Lcom/android/settings/HeadsetModeFragment;->cbl:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0, p0}, Lmiui/preference/RadioButtonPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/HeadsetModeFragment;->cbk:Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0, p0}, Lmiui/preference/RadioButtonPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/HeadsetModeFragment;->cbj:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/HeadsetModeFragment;->cbl:Lmiui/preference/RadioButtonPreference;

    if-ne v1, p1, :cond_2

    const-string/jumbo v0, "volume"

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/settings/HeadsetModeFragment;->cbj:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lcom/android/settings/cd;->bQq(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/settings/HeadsetModeFragment;->bTr()V

    :cond_1
    const/4 v0, 0x0

    return v0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/HeadsetModeFragment;->cbk:Lmiui/preference/RadioButtonPreference;

    if-ne v1, p1, :cond_0

    const-string/jumbo v0, "music"

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsBasePreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/HeadsetModeFragment;->bTr()V

    return-void
.end method
