.class public Lcom/android/settings/datausage/DataUsageMeteredSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "DataUsageMeteredSettings.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private iK:Landroid/preference/PreferenceCategory;

.field private iL:Lcom/android/settingslib/D;

.field private iM:Landroid/net/NetworkPolicyManager;

.field private iN:Landroid/preference/PreferenceCategory;

.field private iO:Landroid/preference/Preference;

.field private iP:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/datausage/DataUsageMeteredSettings$1;

    invoke-direct {v0}, Lcom/android/settings/datausage/DataUsageMeteredSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private iF(Landroid/net/wifi/WifiConfiguration;)Landroid/preference/Preference;
    .locals 4

    invoke-virtual {p1}, Landroid/net/wifi/WifiConfiguration;->isPasspoint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->providerFriendlyName:Ljava/lang/String;

    :goto_0
    invoke-static {v0}, Landroid/net/NetworkTemplate;->buildTemplateWifi(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v1

    new-instance v2, Lcom/android/settings/datausage/DataUsageMeteredSettings$MeteredPreference;

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageMeteredSettings;->bWz()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3, v1}, Lcom/android/settings/datausage/DataUsageMeteredSettings$MeteredPreference;-><init>(Lcom/android/settings/datausage/DataUsageMeteredSettings;Landroid/content/Context;Landroid/net/NetworkTemplate;)V

    invoke-static {v0}, Landroid/net/wifi/WifiInfo;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/settings/datausage/DataUsageMeteredSettings$MeteredPreference;->setTitle(Ljava/lang/CharSequence;)V

    return-object v2

    :cond_0
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    goto :goto_0
.end method

.method private iG(Landroid/content/Context;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageMeteredSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iK:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iN:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    invoke-static {p1}, Lcom/android/settings/datausage/DataUsageSummary;->jh(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iP:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iP:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iN:Landroid/preference/PreferenceCategory;

    invoke-direct {p0, v0}, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iF(Landroid/net/wifi/WifiConfiguration;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iN:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iO:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    :cond_2
    return-void
.end method

.method static synthetic iH(Lcom/android/settings/datausage/DataUsageMeteredSettings;)Lcom/android/settingslib/D;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iL:Lcom/android/settingslib/D;

    return-object v0
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x44

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageMeteredSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/net/NetworkPolicyManager;->from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iM:Landroid/net/NetworkPolicyManager;

    const-string/jumbo v0, "wifi"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iP:Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/android/settingslib/D;

    iget-object v2, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iM:Landroid/net/NetworkPolicyManager;

    invoke-direct {v0, v2}, Lcom/android/settingslib/D;-><init>(Landroid/net/NetworkPolicyManager;)V

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iL:Lcom/android/settingslib/D;

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iL:Lcom/android/settingslib/D;

    invoke-virtual {v0}, Lcom/android/settingslib/D;->csb()V

    const v0, 0x7f150037

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageMeteredSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "mobile"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageMeteredSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iK:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageMeteredSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iN:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "wifi_disabled"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageMeteredSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iO:Landroid/preference/Preference;

    invoke-direct {p0, v1}, Lcom/android/settings/datausage/DataUsageMeteredSettings;->iG(Landroid/content/Context;)V

    return-void
.end method
