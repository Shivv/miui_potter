.class Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;
.super Landroid/os/AsyncTask;
.source "AppDataUsagePreference.java"


# instance fields
.field private final ks:Lcom/android/settingslib/AppItem;

.field private final kt:Lcom/android/settingslib/g/f;

.field private final ku:Lcom/android/settings/datausage/AppDataUsagePreference;


# direct methods
.method private constructor <init>(Lcom/android/settingslib/g/f;Lcom/android/settingslib/AppItem;Lcom/android/settings/datausage/AppDataUsagePreference;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/g/f;

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->kt:Lcom/android/settingslib/g/f;

    invoke-static {p2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/AppItem;

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->ks:Lcom/android/settingslib/AppItem;

    invoke-static {p3}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datausage/AppDataUsagePreference;

    iput-object v0, p0, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->ku:Lcom/android/settings/datausage/AppDataUsagePreference;

    return-void
.end method

.method private static jN(Lcom/android/settingslib/g/a;Landroid/preference/Preference;)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/g/a;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settingslib/g/a;->cJa:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static jO(Lcom/android/settingslib/g/f;Lcom/android/settingslib/AppItem;Lcom/android/settings/datausage/AppDataUsagePreference;)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p1, Lcom/android/settingslib/AppItem;->key:I

    invoke-virtual {p0, v0, v2}, Lcom/android/settingslib/g/f;->coN(IZ)Lcom/android/settingslib/g/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0, p2}, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->jN(Lcom/android/settingslib/g/a;Landroid/preference/Preference;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;-><init>(Lcom/android/settingslib/g/f;Lcom/android/settingslib/AppItem;Lcom/android/settings/datausage/AppDataUsagePreference;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/android/settingslib/g/a;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->kt:Lcom/android/settingslib/g/f;

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->ks:Lcom/android/settingslib/AppItem;

    iget v1, v1, Lcom/android/settingslib/AppItem;->key:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/g/f;->coN(IZ)Lcom/android/settingslib/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->doInBackground([Ljava/lang/Void;)Lcom/android/settingslib/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected jP(Lcom/android/settingslib/g/a;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->ku:Lcom/android/settings/datausage/AppDataUsagePreference;

    invoke-static {p1, v0}, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->jN(Lcom/android/settingslib/g/a;Landroid/preference/Preference;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/settingslib/g/a;

    invoke-virtual {p0, p1}, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->jP(Lcom/android/settingslib/g/a;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->ku:Lcom/android/settings/datausage/AppDataUsagePreference;

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->jN(Lcom/android/settingslib/g/a;Landroid/preference/Preference;)V

    return-void
.end method
