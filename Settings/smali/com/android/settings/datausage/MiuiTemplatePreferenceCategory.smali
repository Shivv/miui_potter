.class public Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;
.super Landroid/preference/PreferenceCategory;
.source "MiuiTemplatePreferenceCategory.java"

# interfaces
.implements Lcom/android/settings/datausage/TemplatePreference;


# instance fields
.field private js:I

.field private jt:Landroid/net/NetworkTemplate;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public addPreference(Landroid/preference/Preference;)Z
    .locals 2

    instance-of v0, p1, Lcom/android/settings/datausage/TemplatePreference;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "TemplatePreferenceCategories can only hold TemplatePreferences"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public hR(Landroid/net/NetworkTemplate;ILcom/android/settings/datausage/TemplatePreference$NetworkServices;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->jt:Landroid/net/NetworkTemplate;

    iput p2, p0, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->js:I

    return-void
.end method

.method public iW(Lcom/android/settings/datausage/TemplatePreference$NetworkServices;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->jt:Landroid/net/NetworkTemplate;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "null mTemplate for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datausage/TemplatePreference;

    iget-object v2, p0, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->jt:Landroid/net/NetworkTemplate;

    iget v3, p0, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->js:I

    invoke-interface {v0, v2, v3, p1}, Lcom/android/settings/datausage/TemplatePreference;->hR(Landroid/net/NetworkTemplate;ILcom/android/settings/datausage/TemplatePreference$NetworkServices;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method
