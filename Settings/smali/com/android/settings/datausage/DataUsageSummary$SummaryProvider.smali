.class Lcom/android/settings/datausage/DataUsageSummary$SummaryProvider;
.super Ljava/lang/Object;
.source "DataUsageSummary.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;


# instance fields
.field private final jQ:Landroid/app/Activity;

.field private final jR:Lcom/android/settingslib/g/b;

.field private final jS:Lcom/android/settings/dashboard/C;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/android/settings/dashboard/C;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/datausage/DataUsageSummary$SummaryProvider;->jQ:Landroid/app/Activity;

    iput-object p2, p0, Lcom/android/settings/datausage/DataUsageSummary$SummaryProvider;->jS:Lcom/android/settings/dashboard/C;

    new-instance v0, Lcom/android/settingslib/g/b;

    invoke-direct {v0, p1}, Lcom/android/settingslib/g/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary$SummaryProvider;->jR:Lcom/android/settingslib/g/b;

    return-void
.end method


# virtual methods
.method public jt(Z)V
    .locals 6

    const-wide/16 v4, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary$SummaryProvider;->jR:Lcom/android/settingslib/g/b;

    invoke-virtual {v0}, Lcom/android/settingslib/g/b;->coD()Lcom/android/settingslib/g/d;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary$SummaryProvider;->jQ:Landroid/app/Activity;

    invoke-static {v0, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageSummary$SummaryProvider;->jS:Lcom/android/settings/dashboard/C;

    iget-object v2, p0, Lcom/android/settings/datausage/DataUsageSummary$SummaryProvider;->jQ:Landroid/app/Activity;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const v0, 0x7f12051a

    invoke-virtual {v2, v0, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    iget-wide v2, v0, Lcom/android/settingslib/g/d;->cJk:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageSummary$SummaryProvider;->jQ:Landroid/app/Activity;

    iget-wide v2, v0, Lcom/android/settingslib/g/d;->cJl:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-wide v2, v0, Lcom/android/settingslib/g/d;->cJl:J

    iget-wide v0, v0, Lcom/android/settingslib/g/d;->cJk:J

    invoke-static {v2, v3, v0, v1}, Lcom/android/settings/aq;->cqF(JJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
