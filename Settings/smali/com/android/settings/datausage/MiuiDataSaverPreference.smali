.class public Lcom/android/settings/datausage/MiuiDataSaverPreference;
.super Landroid/preference/Preference;
.source "MiuiDataSaverPreference.java"

# interfaces
.implements Lcom/android/settings/datausage/DataSaverBackend$Listener;


# instance fields
.field private final iT:Lcom/android/settings/datausage/DataSaverBackend;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/settings/datausage/DataSaverBackend;

    invoke-direct {v0, p1}, Lcom/android/settings/datausage/DataSaverBackend;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/datausage/MiuiDataSaverPreference;->iT:Lcom/android/settings/datausage/DataSaverBackend;

    return-void
.end method


# virtual methods
.method public iA(IZ)V
    .locals 0

    return-void
.end method

.method public iy(IZ)V
    .locals 0

    return-void
.end method

.method public iz(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const v0, 0x7f1204d2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/MiuiDataSaverPreference;->setSummary(I)V

    return-void

    :cond_0
    const v0, 0x7f1204d1

    goto :goto_0
.end method

.method public onAttachedToActivity()V
    .locals 1

    invoke-super {p0}, Landroid/preference/Preference;->onAttachedToActivity()V

    iget-object v0, p0, Lcom/android/settings/datausage/MiuiDataSaverPreference;->iT:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v0, p0}, Lcom/android/settings/datausage/DataSaverBackend;->kn(Lcom/android/settings/datausage/DataSaverBackend$Listener;)V

    return-void
.end method
