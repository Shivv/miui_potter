.class public Lcom/android/settings/datausage/AppDataUsagePreference;
.super Landroid/preference/Preference;
.source "AppDataUsagePreference.java"


# instance fields
.field private final kq:Lcom/android/settingslib/AppItem;

.field private final kr:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/AppItem;ILcom/android/settingslib/g/f;)V
    .locals 4

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/datausage/AppDataUsagePreference;->kq:Lcom/android/settingslib/AppItem;

    iput p3, p0, Lcom/android/settings/datausage/AppDataUsagePreference;->kr:I

    const v0, 0x7f0d0090

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsagePreference;->setLayoutResource(I)V

    const v0, 0x7f0d026b

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsagePreference;->setWidgetLayoutResource(I)V

    iget-boolean v0, p2, Lcom/android/settingslib/AppItem;->cQK:Z

    if-eqz v0, :cond_0

    iget-wide v0, p2, Lcom/android/settingslib/AppItem;->cQM:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const v0, 0x7f1204e0

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsagePreference;->setSummary(I)V

    :goto_0
    invoke-static {p4, p2, p0}, Lcom/android/settings/datausage/AppDataUsagePreference$UidDetailTask;->jO(Lcom/android/settingslib/g/f;Lcom/android/settingslib/AppItem;Lcom/android/settings/datausage/AppDataUsagePreference;)V

    return-void

    :cond_0
    iget-wide v0, p2, Lcom/android/settingslib/AppItem;->cQM:J

    invoke-static {p1, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/AppDataUsagePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getItem()Lcom/android/settingslib/AppItem;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/AppDataUsagePreference;->kq:Lcom/android/settingslib/AppItem;

    return-object v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x102000d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsagePreference;->kq:Lcom/android/settingslib/AppItem;

    iget-boolean v1, v1, Lcom/android/settingslib/AppItem;->cQK:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/datausage/AppDataUsagePreference;->kq:Lcom/android/settingslib/AppItem;

    iget-wide v2, v1, Lcom/android/settingslib/AppItem;->cQM:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    iget v1, p0, Lcom/android/settings/datausage/AppDataUsagePreference;->kr:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method
