.class public Lcom/android/settings/datausage/DataUsageSummary;
.super Lcom/android/settings/datausage/DataUsageBase;
.source "DataUsageSummary.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;
.implements Lcom/android/settings/datausage/DataUsageEditController;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;


# instance fields
.field private jG:Lcom/android/settings/datausage/DataUsageInfoController;

.field private jH:Lcom/android/settingslib/g/b;

.field private jI:I

.field private jJ:Landroid/net/NetworkTemplate;

.field private jK:Landroid/preference/Preference;

.field private jL:Lcom/android/settings/datausage/NetworkRestrictionsPreference;

.field private jM:Lcom/android/settingslib/D;

.field private jN:Landroid/net/NetworkPolicyManager;

.field private jO:Lcom/android/settings/MiuiSummaryPreference;

.field private jP:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/datausage/DataUsageSummary$1;

    invoke-direct {v0}, Lcom/android/settings/datausage/DataUsageSummary$1;-><init>()V

    sput-object v0, Lcom/android/settings/datausage/DataUsageSummary;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    new-instance v0, Lcom/android/settings/datausage/DataUsageSummary$2;

    invoke-direct {v0}, Lcom/android/settings/datausage/DataUsageSummary$2;-><init>()V

    sput-object v0, Lcom/android/settings/datausage/DataUsageSummary;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/datausage/DataUsageBase;-><init>()V

    return-void
.end method

.method public static jh(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v0

    return v0
.end method

.method private ji()V
    .locals 4

    const v0, 0x7f150035

    invoke-direct {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->jr(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;

    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateEthernet()Landroid/net/NetworkTemplate;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->hR(Landroid/net/NetworkTemplate;ILcom/android/settings/datausage/TemplatePreference$NetworkServices;)V

    return-void
.end method

.method private jj(I)V
    .locals 3

    const v0, 0x7f150034

    invoke-direct {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->jr(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;

    invoke-direct {p0, p1}, Lcom/android/settings/datausage/DataUsageSummary;->jo(I)Landroid/net/NetworkTemplate;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    invoke-virtual {v0, v1, p1, v2}, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->hR(Landroid/net/NetworkTemplate;ILcom/android/settings/datausage/TemplatePreference$NetworkServices;)V

    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    invoke-virtual {v0, v1}, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->iW(Lcom/android/settings/datausage/TemplatePreference$NetworkServices;)V

    return-void
.end method

.method private jk()V
    .locals 4

    const v0, 0x7f150038

    invoke-direct {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->jr(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;

    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateWifiWildcard()Landroid/net/NetworkTemplate;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->hR(Landroid/net/NetworkTemplate;ILcom/android/settings/datausage/TemplatePreference$NetworkServices;)V

    return-void
.end method

.method private static jl(Landroid/content/Context;Ljava/lang/String;J)Ljava/lang/CharSequence;
    .locals 10

    const/high16 v3, 0x3fc80000    # 1.5625f

    const v9, 0x3f23d70a    # 0.64f

    const/16 v8, 0x12

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p2, p3, v7}, Landroid/text/format/Formatter;->formatBytes(Landroid/content/res/Resources;JI)Landroid/text/format/Formatter$BytesResult;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableString;

    iget-object v2, v0, Landroid/text/format/Formatter$BytesResult;->value:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v2, v3}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, v6, v3, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-instance v2, Landroid/text/SpannableString;

    const v3, 0x104021b

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "%1$s"

    const-string/jumbo v5, "^1"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "%2$s"

    const-string/jumbo v5, "^2"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    aput-object v1, v3, v6

    iget-object v0, v0, Landroid/text/format/Formatter$BytesResult;->units:Ljava/lang/String;

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v2, v9}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, v6, v3, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-array v2, v7, [Ljava/lang/CharSequence;

    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-static {v1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static jm(Landroid/content/Context;)I
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v1

    if-nez v1, :cond_0

    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/telephony/SubscriptionManager;->getDefaultDataSubscriptionInfo()Landroid/telephony/SubscriptionInfo;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Landroid/telephony/SubscriptionManager;->getAllSubscriptionInfoList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    return v2

    :cond_1
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    :cond_2
    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v0

    return v0
.end method

.method public static jn(Landroid/content/Context;I)Landroid/net/NetworkTemplate;
    .locals 2

    invoke-static {p0}, Lcom/android/settings/datausage/DataUsageSummary;->jq(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/NetworkTemplate;->buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getMergedSubscriberIds()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/net/NetworkTemplate;->normalize(Landroid/net/NetworkTemplate;[Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/android/settings/datausage/DataUsageSummary;->jh(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateWifiWildcard()Landroid/net/NetworkTemplate;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateEthernet()Landroid/net/NetworkTemplate;

    move-result-object v0

    return-object v0
.end method

.method private jo(I)Landroid/net/NetworkTemplate;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jl:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/NetworkTemplate;->buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v1, v1, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jl:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getMergedSubscriberIds()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/NetworkTemplate;->normalize(Landroid/net/NetworkTemplate;[Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v0

    return-object v0
.end method

.method public static jq(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v0

    return v0
.end method

.method private jr(I)Landroid/preference/Preference;
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->bWz()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/preference/PreferenceManager;->inflateFromResource(Landroid/content/Context;ILandroid/preference/PreferenceScreen;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOrder(I)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    return-object v1
.end method

.method private js()V
    .locals 14

    const/4 v13, 0x0

    const/4 v1, 0x1

    const/4 v12, 0x0

    const-wide/16 v10, 0x0

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jH:Lcom/android/settingslib/g/b;

    iget-object v2, p0, Lcom/android/settings/datausage/DataUsageSummary;->jJ:Landroid/net/NetworkTemplate;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/g/b;->coG(Landroid/net/NetworkTemplate;)Lcom/android/settingslib/g/d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jG:Lcom/android/settings/datausage/DataUsageInfoController;

    iget-object v4, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v4, v4, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jj:Lcom/android/settingslib/D;

    iget-object v5, p0, Lcom/android/settings/datausage/DataUsageSummary;->jJ:Landroid/net/NetworkTemplate;

    invoke-virtual {v4, v5}, Lcom/android/settingslib/D;->crU(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/android/settings/datausage/DataUsageInfoController;->id(Lcom/android/settingslib/g/d;Landroid/net/NetworkPolicy;)V

    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    iget v4, p0, Lcom/android/settings/datausage/DataUsageSummary;->jI:I

    invoke-virtual {p0, v4}, Lcom/android/settings/datausage/DataUsageSummary;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-wide v6, v0, Lcom/android/settingslib/g/d;->cJl:J

    invoke-static {v2, v4, v6, v7}, Lcom/android/settings/datausage/DataUsageSummary;->jl(Landroid/content/Context;Ljava/lang/String;J)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/settings/MiuiSummaryPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jG:Lcom/android/settings/datausage/DataUsageInfoController;

    invoke-virtual {v3, v0}, Lcom/android/settings/datausage/DataUsageInfoController;->ic(Lcom/android/settingslib/g/d;)J

    move-result-wide v4

    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    iget-object v6, v0, Lcom/android/settingslib/g/d;->cJp:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/android/settings/MiuiSummaryPreference;->setSummary(Ljava/lang/CharSequence;)V

    cmp-long v3, v4, v10

    if-gtz v3, :cond_3

    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    invoke-virtual {v3, v13}, Lcom/android/settings/MiuiSummaryPreference;->bfg(Z)V

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jK:Landroid/preference/Preference;

    if-eqz v3, :cond_5

    iget-wide v4, v0, Lcom/android/settingslib/g/d;->cJo:J

    cmp-long v3, v4, v10

    if-gtz v3, :cond_1

    iget-wide v4, v0, Lcom/android/settingslib/g/d;->cJk:J

    cmp-long v3, v4, v10

    if-lez v3, :cond_5

    :cond_1
    iget-wide v4, v0, Lcom/android/settingslib/g/d;->cJo:J

    invoke-static {v2, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    iget-wide v4, v0, Lcom/android/settingslib/g/d;->cJk:J

    invoke-static {v2, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/android/settings/datausage/DataUsageSummary;->jK:Landroid/preference/Preference;

    iget-wide v6, v0, Lcom/android/settingslib/g/d;->cJk:J

    cmp-long v0, v6, v10

    if-gtz v0, :cond_4

    const v0, 0x7f1203f1

    :goto_1
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v13

    aput-object v2, v5, v1

    invoke-virtual {p0, v0, v5}, Lcom/android/settings/datausage/DataUsageSummary;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jL:Lcom/android/settings/datausage/NetworkRestrictionsPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->updateNetworkRestrictionSummary(Lcom/android/settings/datausage/NetworkRestrictionsPreference;)V

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    :goto_3
    invoke-virtual {v2}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-ge v1, v0, :cond_6

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;

    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    invoke-virtual {v0, v3}, Lcom/android/settings/datausage/MiuiTemplatePreferenceCategory;->iW(Lcom/android/settings/datausage/TemplatePreference$NetworkServices;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    invoke-virtual {v3, v1}, Lcom/android/settings/MiuiSummaryPreference;->bfg(Z)V

    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    invoke-static {v2, v10, v11}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/android/settings/MiuiSummaryPreference;->bfh(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    iget-wide v6, v0, Lcom/android/settingslib/g/d;->cJl:J

    long-to-float v6, v6

    long-to-float v7, v4

    div-float/2addr v6, v7

    const/4 v7, 0x0

    iget-wide v8, v0, Lcom/android/settingslib/g/d;->cJl:J

    sub-long v8, v4, v8

    long-to-float v8, v8

    long-to-float v4, v4

    div-float v4, v8, v4

    invoke-virtual {v3, v6, v7, v4}, Lcom/android/settings/MiuiSummaryPreference;->setRatios(FFF)V

    goto :goto_0

    :cond_4
    const v0, 0x7f1203f0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jK:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jK:Landroid/preference/Preference;

    invoke-virtual {v0, v12}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_6
    return-void
.end method


# virtual methods
.method protected aq()I
    .locals 1

    const v0, 0x7f120819

    return v0
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 2

    const/4 v1, 0x0

    const-string/jumbo v0, "status_header"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-ne p1, v0, :cond_0

    invoke-static {p0, v1}, Lcom/android/settings/datausage/BillingCycleSettings$BytesEditorFragment;->kK(Lcom/android/settings/datausage/DataUsageEditController;Z)V

    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/datausage/DataUsageBase;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x25

    return v0
.end method

.method public iJ()Lcom/android/settingslib/D;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jj:Lcom/android/settingslib/D;

    return-object v0
.end method

.method public iK()Landroid/net/NetworkTemplate;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jJ:Landroid/net/NetworkTemplate;

    return-object v0
.end method

.method public iL()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/datausage/DataUsageSummary;->js()V

    return-void
.end method

.method isMetered(Landroid/net/wifi/WifiConfiguration;)Z
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    if-nez v0, :cond_0

    return v2

    :cond_0
    invoke-virtual {p1}, Landroid/net/wifi/WifiConfiguration;->isPasspoint()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->providerFriendlyName:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageSummary;->jM:Lcom/android/settingslib/D;

    invoke-static {v0}, Landroid/net/NetworkTemplate;->buildTemplateWifi(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/settingslib/D;->crQ(Landroid/net/NetworkTemplate;)Landroid/net/NetworkPolicy;

    move-result-object v0

    if-nez v0, :cond_2

    return v2

    :cond_1
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-wide v2, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    return v0

    :cond_3
    iget-boolean v0, v0, Landroid/net/NetworkPolicy;->metered:Z

    return v0
.end method

.method public jp(Landroid/content/Context;)Z
    .locals 10

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-static {p1}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v9

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jn:Landroid/net/INetworkStatsService;

    invoke-interface {v0}, Landroid/net/INetworkStatsService;->openSession()Landroid/net/INetworkStatsSession;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateEthernet()Landroid/net/NetworkTemplate;

    move-result-object v1

    const-wide/high16 v2, -0x8000000000000000L

    const-wide v4, 0x7fffffffffffffffL

    invoke-interface/range {v0 .. v5}, Landroid/net/INetworkStatsSession;->getSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkStats;->getTotalBytes()J

    move-result-wide v2

    invoke-static {v0}, Landroid/net/TrafficStats;->closeQuietly(Landroid/net/INetworkStatsSession;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v0, v2

    :goto_0
    if-eqz v9, :cond_1

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    move-wide v0, v6

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    move v0, v8

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/datausage/DataUsageBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/net/NetworkPolicyManager;->from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jN:Landroid/net/NetworkPolicyManager;

    const-string/jumbo v0, "wifi"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jP:Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/android/settingslib/D;

    iget-object v1, p0, Lcom/android/settings/datausage/DataUsageSummary;->jN:Landroid/net/NetworkPolicyManager;

    invoke-direct {v0, v1}, Lcom/android/settingslib/D;-><init>(Landroid/net/NetworkPolicyManager;)V

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jM:Lcom/android/settingslib/D;

    invoke-static {v3}, Lcom/android/settings/datausage/DataUsageSummary;->jq(Landroid/content/Context;)Z

    move-result v0

    new-instance v1, Lcom/android/settingslib/g/b;

    invoke-direct {v1, v3}, Lcom/android/settingslib/g/b;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/datausage/DataUsageSummary;->jH:Lcom/android/settingslib/g/b;

    new-instance v1, Lcom/android/settings/datausage/DataUsageInfoController;

    invoke-direct {v1}, Lcom/android/settings/datausage/DataUsageInfoController;-><init>()V

    iput-object v1, p0, Lcom/android/settings/datausage/DataUsageSummary;->jG:Lcom/android/settings/datausage/DataUsageInfoController;

    const v1, 0x7f150033

    invoke-virtual {p0, v1}, Lcom/android/settings/datausage/DataUsageSummary;->addPreferencesFromResource(I)V

    invoke-static {v3}, Lcom/android/settings/datausage/DataUsageSummary;->jm(Landroid/content/Context;)I

    move-result v4

    const/4 v1, -0x1

    if-ne v4, v1, :cond_a

    move v1, v2

    :goto_0
    invoke-static {v3, v4}, Lcom/android/settings/datausage/DataUsageSummary;->jn(Landroid/content/Context;I)Landroid/net/NetworkTemplate;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jJ:Landroid/net/NetworkTemplate;

    const-string/jumbo v0, "status_header"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSummaryPreference;

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->iR()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "restrict_background"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->bWK(Ljava/lang/String;)V

    :cond_1
    if-eqz v1, :cond_7

    const-string/jumbo v0, "limit_summary"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jK:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jo:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-direct {p0, v4}, Lcom/android/settings/datausage/DataUsageSummary;->jj(I)V

    :cond_3
    :goto_1
    if-eqz v5, :cond_4

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->jj(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    invoke-virtual {v0, v6}, Lcom/android/settings/MiuiSummaryPreference;->setSelectable(Z)V

    :goto_2
    invoke-static {v3}, Lcom/android/settings/datausage/DataUsageSummary;->jh(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/android/settings/datausage/DataUsageSummary;->jk()V

    :cond_5
    invoke-virtual {p0, v3}, Lcom/android/settings/datausage/DataUsageSummary;->jp(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-direct {p0}, Lcom/android/settings/datausage/DataUsageSummary;->ji()V

    :cond_6
    if-eqz v1, :cond_8

    const v0, 0x7f1203ef

    :goto_3
    iput v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jI:I

    invoke-virtual {p0, v6}, Lcom/android/settings/datausage/DataUsageSummary;->setHasOptionsMenu(Z)V

    return-void

    :cond_7
    const-string/jumbo v0, "limit_summary"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->bWK(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jO:Lcom/android/settings/MiuiSummaryPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/MiuiSummaryPreference;->setSelectable(Z)V

    goto :goto_2

    :cond_8
    if-eqz v0, :cond_9

    const v0, 0x7f1214fb

    goto :goto_3

    :cond_9
    const v0, 0x7f1206e8

    goto :goto_3

    :cond_a
    move v1, v0

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0e0001

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/datausage/DataUsageBase;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    return v1

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.qualcomm.qti.networksetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.qualcomm.qti.networksetting"

    const-string/jumbo v3, "com.qualcomm.qti.networksetting.MobileNetworkSettings"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v1

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.android.phone"

    const-string/jumbo v3, "com.android.phone.settings.MobileNetworkSettings"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a011b
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/datausage/DataUsageBase;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/datausage/DataUsageSummary;->js()V

    return-void
.end method

.method updateNetworkRestrictionSummary(Lcom/android/settings/datausage/NetworkRestrictionsPreference;)V
    .locals 5

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jM:Lcom/android/settingslib/D;

    invoke-virtual {v0}, Lcom/android/settingslib/D;->csb()V

    iget-object v0, p0, Lcom/android/settings/datausage/DataUsageSummary;->jP:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/DataUsageSummary;->isMetered(Landroid/net/wifi/WifiConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/datausage/DataUsageSummary;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    const v2, 0x7f10002b

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/datausage/NetworkRestrictionsPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method
