.class public Lcom/android/settings/datausage/AppPrefLoader;
.super Lcom/android/settings/utils/k;
.source "AppPrefLoader.java"


# instance fields
.field private jp:Landroid/util/ArraySet;

.field private jq:Landroid/content/Context;

.field private mPackageManager:Landroid/content/pm/PackageManager;


# virtual methods
.method protected iQ(Landroid/util/ArraySet;)V
    .locals 0

    return-void
.end method

.method public loadInBackground()Landroid/util/ArraySet;
    .locals 6

    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/datausage/AppPrefLoader;->jp:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    :try_start_0
    iget-object v4, p0, Lcom/android/settings/datausage/AppPrefLoader;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v0, p0, Lcom/android/settings/datausage/AppPrefLoader;->jp:Landroid/util/ArraySet;

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    new-instance v4, Landroid/support/v7/preference/Preference;

    iget-object v5, p0, Lcom/android/settings/datausage/AppPrefLoader;->jq:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Lcom/android/settings/datausage/AppPrefLoader;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v5}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v7/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/android/settings/datausage/AppPrefLoader;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v5}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/support/v7/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/support/v7/preference/Preference;->boV(Z)V

    invoke-virtual {v2, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v2

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/datausage/AppPrefLoader;->loadInBackground()Landroid/util/ArraySet;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onDiscardResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/util/ArraySet;

    invoke-virtual {p0, p1}, Lcom/android/settings/datausage/AppPrefLoader;->iQ(Landroid/util/ArraySet;)V

    return-void
.end method
