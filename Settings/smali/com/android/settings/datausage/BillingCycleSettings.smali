.class public Lcom/android/settings/datausage/BillingCycleSettings;
.super Lcom/android/settings/datausage/DataUsageBase;
.source "BillingCycleSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/datausage/DataUsageEditController;


# static fields
.field static final KEY_SET_DATA_LIMIT:Ljava/lang/String; = "set_data_limit"


# instance fields
.field private la:Landroid/preference/Preference;

.field private lb:Landroid/preference/Preference;

.field private lc:Lcom/android/settingslib/g/b;

.field private ld:Landroid/preference/Preference;

.field private le:Landroid/preference/SwitchPreference;

.field private lf:Landroid/preference/SwitchPreference;

.field private lg:Landroid/net/NetworkTemplate;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/datausage/DataUsageBase;-><init>()V

    return-void
.end method

.method private kH(J)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jj:Lcom/android/settingslib/D;

    iget-object v1, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lg:Landroid/net/NetworkTemplate;

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/settingslib/D;->crT(Landroid/net/NetworkTemplate;J)V

    invoke-direct {p0}, Lcom/android/settings/datausage/BillingCycleSettings;->kI()V

    return-void
.end method

.method private kI()V
    .locals 10

    const-wide/16 v8, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jj:Lcom/android/settingslib/D;

    iget-object v1, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lg:Landroid/net/NetworkTemplate;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/D;->crZ(Landroid/net/NetworkTemplate;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/datausage/BillingCycleSettings;->la:Landroid/preference/Preference;

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    const v0, 0x7f12026d

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/datausage/BillingCycleSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jj:Lcom/android/settingslib/D;

    iget-object v1, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lg:Landroid/net/NetworkTemplate;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/D;->crS(Landroid/net/NetworkTemplate;)J

    move-result-wide v0

    cmp-long v2, v0, v8

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/datausage/BillingCycleSettings;->ld:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/datausage/BillingCycleSettings;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->ld:Landroid/preference/Preference;

    invoke-virtual {v0, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lf:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v5}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jj:Lcom/android/settingslib/D;

    iget-object v1, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lg:Landroid/net/NetworkTemplate;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/D;->crS(Landroid/net/NetworkTemplate;)J

    move-result-wide v0

    cmp-long v2, v0, v8

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lb:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/datausage/BillingCycleSettings;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lb:Landroid/preference/Preference;

    invoke-virtual {v0, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->le:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v5}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    :goto_2
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->la:Landroid/preference/Preference;

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->ld:Landroid/preference/Preference;

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->ld:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lf:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v4}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lb:Landroid/preference/Preference;

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lb:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->le:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v4}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_2
.end method

.method static synthetic kJ(Lcom/android/settings/datausage/BillingCycleSettings;)Landroid/net/NetworkTemplate;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lg:Landroid/net/NetworkTemplate;

    return-object v0
.end method


# virtual methods
.method public at(Landroid/preference/Preference;)Z
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->la:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    invoke-static {p0}, Lcom/android/settings/datausage/BillingCycleSettings$CycleEditorFragment;->kN(Lcom/android/settings/datausage/BillingCycleSettings;)V

    return v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->ld:Landroid/preference/Preference;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/settings/datausage/BillingCycleSettings$BytesEditorFragment;->kK(Lcom/android/settings/datausage/DataUsageEditController;Z)V

    return v1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lb:Landroid/preference/Preference;

    if-ne p1, v0, :cond_2

    invoke-static {p0, v1}, Lcom/android/settings/datausage/BillingCycleSettings$BytesEditorFragment;->kK(Lcom/android/settings/datausage/DataUsageEditController;Z)V

    return v1

    :cond_2
    invoke-super {p0, p1}, Lcom/android/settings/datausage/DataUsageBase;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x156

    return v0
.end method

.method public iJ()Lcom/android/settingslib/D;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jj:Lcom/android/settingslib/D;

    return-object v0
.end method

.method public iK()Landroid/net/NetworkTemplate;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lg:Landroid/net/NetworkTemplate;

    return-object v0
.end method

.method public iL()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/datausage/BillingCycleSettings;->kI()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/datausage/DataUsageBase;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/settingslib/g/b;

    invoke-virtual {p0}, Lcom/android/settings/datausage/BillingCycleSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settingslib/g/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lc:Lcom/android/settingslib/g/b;

    invoke-virtual {p0}, Lcom/android/settings/datausage/BillingCycleSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "network_template"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkTemplate;

    iput-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lg:Landroid/net/NetworkTemplate;

    const v0, 0x7f150029

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/BillingCycleSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "billing_cycle"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/BillingCycleSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->la:Landroid/preference/Preference;

    const-string/jumbo v0, "set_data_warning"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/BillingCycleSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lf:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lf:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "data_warning"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/BillingCycleSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->ld:Landroid/preference/Preference;

    const-string/jumbo v0, "set_data_limit"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/BillingCycleSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->le:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->le:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "data_limit"

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/BillingCycleSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lb:Landroid/preference/Preference;

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    const-wide/16 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->le:Landroid/preference/SwitchPreference;

    if-ne v0, p1, :cond_1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v4, v5}, Lcom/android/settings/datausage/BillingCycleSettings;->setPolicyLimitBytes(J)V

    return v2

    :cond_0
    invoke-static {p0}, Lcom/android/settings/datausage/BillingCycleSettings$ConfirmLimitFragment;->kO(Lcom/android/settings/datausage/BillingCycleSettings;)V

    return v1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lf:Landroid/preference/SwitchPreference;

    if-ne v0, p1, :cond_3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lc:Lcom/android/settingslib/g/b;

    invoke-virtual {v0}, Lcom/android/settingslib/g/b;->coy()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/settings/datausage/BillingCycleSettings;->kH(J)V

    :goto_0
    return v2

    :cond_2
    invoke-direct {p0, v4, v5}, Lcom/android/settings/datausage/BillingCycleSettings;->kH(J)V

    goto :goto_0

    :cond_3
    return v1
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/datausage/DataUsageBase;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/datausage/BillingCycleSettings;->kI()V

    return-void
.end method

.method setPolicyLimitBytes(J)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/datausage/BillingCycleSettings;->jr:Lcom/android/settings/datausage/TemplatePreference$NetworkServices;

    iget-object v0, v0, Lcom/android/settings/datausage/TemplatePreference$NetworkServices;->jj:Lcom/android/settingslib/D;

    iget-object v1, p0, Lcom/android/settings/datausage/BillingCycleSettings;->lg:Landroid/net/NetworkTemplate;

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/settingslib/D;->crP(Landroid/net/NetworkTemplate;J)V

    invoke-direct {p0}, Lcom/android/settings/datausage/BillingCycleSettings;->kI()V

    return-void
.end method
