.class public Lcom/android/settings/datausage/ChartDataUsagePreference;
.super Landroid/preference/Preference;
.source "ChartDataUsagePreference.java"


# instance fields
.field private io:J

.field private final ip:I

.field private iq:Landroid/net/NetworkStatsHistory;

.field private ir:Landroid/net/NetworkPolicy;

.field private is:I

.field private it:I

.field private iu:J

.field private final iv:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->setSelectable(Z)V

    const v0, 0x1010543

    invoke-static {p1, v0}, Lcom/android/settings/aq;->cqJ(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->ip:I

    const v0, 0x1010038

    invoke-static {p1, v0}, Lcom/android/settings/aq;->cqJ(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iv:I

    const v0, 0x7f0d008f

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->setLayoutResource(I)V

    return-void
.end method

.method private ie(Lcom/android/settingslib/graph/UsageView;Landroid/net/NetworkPolicy;I)V
    .locals 12

    const-wide/32 v10, 0x80000

    const-wide/16 v8, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/CharSequence;

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget-wide v4, p2, Landroid/net/NetworkPolicy;->limitBytes:J

    cmp-long v0, v4, v8

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->ip:I

    iget-wide v4, p2, Landroid/net/NetworkPolicy;->limitBytes:J

    iget v3, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->ip:I

    const v6, 0x7f12051c

    invoke-direct {p0, v4, v5, v6, v3}, Lcom/android/settings/datausage/ChartDataUsagePreference;->ig(JII)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    :goto_0
    iget-wide v4, p2, Landroid/net/NetworkPolicy;->warningBytes:J

    cmp-long v3, v4, v8

    if-eqz v3, :cond_1

    iget-wide v4, p2, Landroid/net/NetworkPolicy;->warningBytes:J

    div-long/2addr v4, v10

    long-to-int v1, v4

    invoke-virtual {p1, v1}, Lcom/android/settingslib/graph/UsageView;->setDividerLoc(I)V

    iget-wide v4, p2, Landroid/net/NetworkPolicy;->warningBytes:J

    div-long/2addr v4, v10

    long-to-float v1, v4

    int-to-float v3, p3

    div-float/2addr v1, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, v1

    invoke-virtual {p1, v3, v1}, Lcom/android/settingslib/graph/UsageView;->setSideLabelWeights(FF)V

    iget v1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iv:I

    iget-wide v4, p2, Landroid/net/NetworkPolicy;->warningBytes:J

    iget v3, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iv:I

    const v6, 0x7f12051d

    invoke-direct {p0, v4, v5, v6, v3}, Lcom/android/settings/datausage/ChartDataUsagePreference;->ig(JII)Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    :cond_1
    invoke-virtual {p1, v2}, Lcom/android/settingslib/graph/UsageView;->setSideLabels([Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1, v0}, Lcom/android/settingslib/graph/UsageView;->setDividerColors(II)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private if(Lcom/android/settingslib/graph/UsageView;)V
    .locals 14

    new-instance v4, Landroid/util/SparseIntArray;

    invoke-direct {v4}, Landroid/util/SparseIntArray;-><init>()V

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iq:Landroid/net/NetworkStatsHistory;

    iget-wide v6, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iu:J

    invoke-virtual {v0, v6, v7}, Landroid/net/NetworkStatsHistory;->getIndexAfter(J)I

    move-result v0

    iget-object v5, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iq:Landroid/net/NetworkStatsHistory;

    iget-wide v6, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->io:J

    invoke-virtual {v5, v6, v7}, Landroid/net/NetworkStatsHistory;->getIndexAfter(J)I

    move-result v5

    if-gez v0, :cond_0

    return-void

    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/util/SparseIntArray;->put(II)V

    :goto_0
    if-gt v0, v5, :cond_1

    iget-object v6, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iq:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v6, v0, v1}, Landroid/net/NetworkStatsHistory;->getValues(ILandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    move-result-object v1

    iget-wide v6, v1, Landroid/net/NetworkStatsHistory$Entry;->bucketStart:J

    iget-wide v8, v1, Landroid/net/NetworkStatsHistory$Entry;->bucketDuration:J

    add-long/2addr v8, v6

    iget-wide v10, v1, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    iget-wide v12, v1, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    add-long/2addr v10, v12

    add-long/2addr v2, v10

    iget-wide v10, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iu:J

    sub-long/2addr v6, v10

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    invoke-direct {p0, v6, v7}, Lcom/android/settings/datausage/ChartDataUsagePreference;->il(J)I

    move-result v6

    const-wide/32 v10, 0x80000

    div-long v10, v2, v10

    long-to-int v7, v10

    invoke-virtual {v4, v6, v7}, Landroid/util/SparseIntArray;->put(II)V

    iget-wide v6, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iu:J

    sub-long v6, v8, v6

    invoke-direct {p0, v6, v7}, Lcom/android/settings/datausage/ChartDataUsagePreference;->il(J)I

    move-result v6

    const-wide/32 v8, 0x80000

    div-long v8, v2, v8

    long-to-int v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    invoke-virtual {p1, v4}, Lcom/android/settingslib/graph/UsageView;->cmq(Landroid/util/SparseIntArray;)V

    :cond_2
    return-void
.end method

.method private ig(JII)Ljava/lang/CharSequence;
    .locals 7

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1, p2, v5}, Landroid/text/format/Formatter;->formatBytes(Landroid/content/res/Resources;JI)Landroid/text/format/Formatter$BytesResult;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    iget-object v3, v0, Landroid/text/format/Formatter$BytesResult;->value:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object v0, v0, Landroid/text/format/Formatter$BytesResult;->units:Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, p4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1, v0, v2, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method private il(J)I
    .locals 3

    const-wide/32 v0, 0xea60

    div-long v0, p1, v0

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public getInspectEnd()J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->io:J

    return-wide v0
.end method

.method public getInspectStart()J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iu:J

    return-wide v0
.end method

.method public ih()I
    .locals 12

    const-wide/16 v0, 0x0

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iq:Landroid/net/NetworkStatsHistory;

    iget-wide v4, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iu:J

    invoke-virtual {v2, v4, v5}, Landroid/net/NetworkStatsHistory;->getIndexBefore(J)I

    move-result v2

    iget-object v4, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iq:Landroid/net/NetworkStatsHistory;

    iget-wide v6, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->io:J

    invoke-virtual {v4, v6, v7}, Landroid/net/NetworkStatsHistory;->getIndexAfter(J)I

    move-result v6

    move-wide v4, v0

    :goto_0
    if-gt v2, v6, :cond_0

    iget-object v7, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iq:Landroid/net/NetworkStatsHistory;

    invoke-virtual {v7, v2, v3}, Landroid/net/NetworkStatsHistory;->getValues(ILandroid/net/NetworkStatsHistory$Entry;)Landroid/net/NetworkStatsHistory$Entry;

    move-result-object v3

    iget-wide v8, v3, Landroid/net/NetworkStatsHistory$Entry;->rxBytes:J

    iget-wide v10, v3, Landroid/net/NetworkStatsHistory$Entry;->txBytes:J

    add-long/2addr v8, v10

    add-long/2addr v4, v8

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->ir:Landroid/net/NetworkPolicy;

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->ir:Landroid/net/NetworkPolicy;

    iget-wide v0, v0, Landroid/net/NetworkPolicy;->limitBytes:J

    iget-object v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->ir:Landroid/net/NetworkPolicy;

    iget-wide v2, v2, Landroid/net/NetworkPolicy;->warningBytes:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :cond_1
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const-wide/32 v2, 0x80000

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public ii(II)V
    .locals 0

    iput p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->it:I

    iput p2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->is:I

    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->notifyChanged()V

    return-void
.end method

.method public ij(Landroid/net/NetworkPolicy;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->ir:Landroid/net/NetworkPolicy;

    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->notifyChanged()V

    return-void
.end method

.method public ik(Landroid/net/NetworkStatsHistory;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iq:Landroid/net/NetworkStatsHistory;

    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->notifyChanged()V

    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 9

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a011a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/graph/UsageView;

    iget-object v1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iq:Landroid/net/NetworkStatsHistory;

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->ih()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/settingslib/graph/UsageView;->cmp()V

    iget-wide v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->io:J

    iget-wide v4, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iu:J

    sub-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/android/settings/datausage/ChartDataUsagePreference;->il(J)I

    move-result v2

    invoke-virtual {v0, v2, v1, v8, v8}, Lcom/android/settingslib/graph/UsageView;->cmr(IIZZ)V

    invoke-direct {p0, v0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->if(Lcom/android/settingslib/graph/UsageView;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iu:J

    iget-wide v6, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iu:J

    invoke-static {v3, v4, v5, v6, v7}, Lcom/android/settings/aq;->bqy(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->io:J

    iget-wide v6, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->io:J

    invoke-static {v3, v4, v5, v6, v7}, Lcom/android/settings/aq;->bqy(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Lcom/android/settingslib/graph/UsageView;->setBottomLabels([Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->ir:Landroid/net/NetworkPolicy;

    invoke-direct {p0, v0, v2, v1}, Lcom/android/settings/datausage/ChartDataUsagePreference;->ie(Lcom/android/settingslib/graph/UsageView;Landroid/net/NetworkPolicy;I)V

    return-void
.end method

.method public setVisibleRange(JJ)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->iu:J

    iput-wide p3, p0, Lcom/android/settings/datausage/ChartDataUsagePreference;->io:J

    invoke-virtual {p0}, Lcom/android/settings/datausage/ChartDataUsagePreference;->notifyChanged()V

    return-void
.end method
