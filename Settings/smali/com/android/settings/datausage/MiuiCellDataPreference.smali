.class public Lcom/android/settings/datausage/MiuiCellDataPreference;
.super Lcom/android/settings/datausage/MiuiCustomDialogPreference;
.source "MiuiCellDataPreference.java"

# interfaces
.implements Lcom/android/settings/datausage/TemplatePreference;
.implements Landroid/preference/PreferenceManager$OnActivityStopListener;


# instance fields
.field private kA:Landroid/telephony/SubscriptionManager;

.field private kB:Landroid/telephony/TelephonyManager;

.field private kv:Ljava/lang/String;

.field public kw:Z

.field private kx:Z

.field private final ky:Lcom/android/settings/datausage/MiuiCellDataPreference$DataStateListener;

.field public kz:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const v0, 0x101036d

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/datausage/MiuiCustomDialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    const-string/jumbo v0, "persist.radio.carrier_mode"

    const-string/jumbo v1, "default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kv:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kv:Ljava/lang/String;

    const-string/jumbo v1, "ct_class_a"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kx:Z

    new-instance v0, Lcom/android/settings/datausage/MiuiCellDataPreference$1;

    invoke-direct {v0, p0}, Lcom/android/settings/datausage/MiuiCellDataPreference$1;-><init>(Lcom/android/settings/datausage/MiuiCellDataPreference;)V

    iput-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->ky:Lcom/android/settings/datausage/MiuiCellDataPreference$DataStateListener;

    return-void
.end method

.method private jQ(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kB:Landroid/telephony/TelephonyManager;

    iget v1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    invoke-virtual {v0, v1, p1}, Landroid/telephony/TelephonyManager;->setDataEnabled(IZ)V

    invoke-direct {p0, p1}, Lcom/android/settings/datausage/MiuiCellDataPreference;->setChecked(Z)V

    return-void
.end method

.method private jR()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kB:Landroid/telephony/TelephonyManager;

    iget v1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getDataEnabled(I)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->setChecked(Z)V

    return-void
.end method

.method static synthetic jS(Lcom/android/settings/datausage/MiuiCellDataPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->jR()V

    return-void
.end method

.method private setChecked(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kw:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kw:Z

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->notifyChanged()V

    return-void
.end method


# virtual methods
.method public hR(Landroid/net/NetworkTemplate;ILcom/android/settings/datausage/TemplatePreference$NetworkServices;)V
    .locals 2

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "CellDataPreference needs a SubscriptionInfo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kA:Landroid/telephony/SubscriptionManager;

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kB:Landroid/telephony/TelephonyManager;

    iget v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    if-ne v0, v1, :cond_1

    iput p2, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->setKey(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->jR()V

    return-void
.end method

.method public onActivityStop()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->ky:Lcom/android/settings/datausage/MiuiCellDataPreference$DataStateListener;

    iget v1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/settings/datausage/MiuiCellDataPreference$DataStateListener;->jT(ZILandroid/content/Context;)V

    return-void
.end method

.method public onAttachedToActivity()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/datausage/MiuiCustomDialogPreference;->onAttachedToActivity()V

    iget-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->ky:Lcom/android/settings/datausage/MiuiCellDataPreference$DataStateListener;

    iget v1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/settings/datausage/MiuiCellDataPreference$DataStateListener;->jT(ZILandroid/content/Context;)V

    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/datausage/MiuiCustomDialogPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x1020040

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    check-cast v0, Landroid/widget/Checkable;

    iget-boolean v1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kw:Z

    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->jQ(Z)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    check-cast p1, Lcom/android/settings/datausage/MiuiCellDataPreference$CellDataState;

    invoke-virtual {p1}, Lcom/android/settings/datausage/MiuiCellDataPreference$CellDataState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/android/settings/datausage/MiuiCustomDialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kB:Landroid/telephony/TelephonyManager;

    iget-boolean v0, p1, Lcom/android/settings/datausage/MiuiCellDataPreference$CellDataState;->kC:Z

    iput-boolean v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kw:Z

    iget v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/android/settings/datausage/MiuiCellDataPreference$CellDataState;->kD:I

    iput v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->setKey(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->notifyChanged()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/android/settings/datausage/MiuiCellDataPreference$CellDataState;

    invoke-super {p0}, Lcom/android/settings/datausage/MiuiCustomDialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/datausage/MiuiCellDataPreference$CellDataState;-><init>(Landroid/os/Parcelable;)V

    iget-boolean v1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kw:Z

    iput-boolean v1, v0, Lcom/android/settings/datausage/MiuiCellDataPreference$CellDataState;->kC:Z

    iget v1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kz:I

    iput v1, v0, Lcom/android/settings/datausage/MiuiCellDataPreference$CellDataState;->kD:I

    return-object v0
.end method

.method public performClick(Landroid/preference/PreferenceScreen;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kw:Z

    xor-int/lit8 v1, v1, 0x1

    const/16 v2, 0xb2

    invoke-static {v0, v2, v1}, Lcom/android/internal/logging/MetricsLogger;->action(Landroid/content/Context;IZ)V

    iget-boolean v0, p0, Lcom/android/settings/datausage/MiuiCellDataPreference;->kw:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/android/settings/datausage/MiuiCustomDialogPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/datausage/MiuiCellDataPreference;->jQ(Z)V

    goto :goto_0
.end method
