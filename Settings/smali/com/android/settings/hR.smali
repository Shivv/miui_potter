.class final Lcom/android/settings/hR;
.super Ljava/lang/Object;
.source "SetUpChooseLockPattern.java"

# interfaces
.implements Lcom/android/settings/h;


# instance fields
.field final synthetic cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private cal(Ljava/util/List;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNa:Landroid/security/ChooseLockSettingsHelper;

    invoke-virtual {v0}, Landroid/security/ChooseLockSettingsHelper;->utils()Landroid/security/MiuiLockPatternUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEJ(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)I

    move-result v1

    const/16 v2, -0x2710

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-virtual {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lmiui/securityspace/CrossUserUtils;->hasAirSpace(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v1, v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    invoke-virtual {v1, v3}, Lcom/android/settings/LockPatternView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEI(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Landroid/os/AsyncTask;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEI(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Landroid/os/AsyncTask;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_1
    iget-object v1, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v2, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v3, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-virtual {v3}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEv(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/android/settings/hS;

    invoke-direct {v3, p0, p1}, Lcom/android/settings/hS;-><init>(Lcom/android/settings/hR;Ljava/util/List;)V

    invoke-static {v0, p1, v2, v3}, Lcom/android/settings/L;->blR(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;Ljava/util/List;Lcom/android/settings/N;)Landroid/os/AsyncTask;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEM(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/settings/hR;->cam(Ljava/util/List;)V

    goto :goto_0
.end method

.method private cam(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNJ:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    return-void
.end method

.method private can()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNk:Landroid/widget/TextView;

    const v1, 0x7f1209d2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEH(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEG(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method static synthetic cao(Lcom/android/settings/hR;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/hR;->cam(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public onPatternCellAdded(Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public onPatternCleared()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    iget-object v1, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEE(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/LockPatternView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onPatternDetected(Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNM:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNI:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-ne v0, v1, :cond_4

    :cond_0
    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "null chosen pattern in stage \'need to confirm"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNI:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNH:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-ne v0, v1, :cond_6

    :cond_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_8

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNH:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-virtual {v0, v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bED(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNO:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNP:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNN:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v0

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-virtual {v0}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->isResumed()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_2

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unexpected stage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v2}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEK(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " when "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "entering the pattern."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    invoke-direct {p0, p1}, Lcom/android/settings/hR;->cal(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public onPatternStart()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v0, v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNm:Lcom/android/settings/LockPatternView;

    iget-object v1, p0, Lcom/android/settings/hR;->cnG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEE(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/LockPatternView;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-direct {p0}, Lcom/android/settings/hR;->can()V

    return-void
.end method
