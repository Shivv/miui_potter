.class final enum Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;
.super Ljava/lang/Enum;
.source "PrivacyPasswordChooseAccessControl.java"


# static fields
.field public static final enum ahU:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

.field public static final enum ahV:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

.field public static final enum ahW:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

.field public static final enum ahX:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

.field public static final enum ahY:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

.field public static final enum ahZ:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

.field private static final synthetic aia:[Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;


# instance fields
.field final enabled:Z

.field text:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    const-string/jumbo v1, "Continue"

    const v2, 0x7f1209c9

    invoke-direct {v0, v1, v4, v2, v5}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahW:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    new-instance v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    const-string/jumbo v1, "ContinueDisabled"

    const v2, 0x7f1209c9

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahX:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    new-instance v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    const-string/jumbo v1, "Confirm"

    const v2, 0x7f120d5a

    invoke-direct {v0, v1, v6, v2, v5}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahU:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    new-instance v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    const-string/jumbo v1, "ConfirmDisabled"

    const v2, 0x7f120d5a

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahV:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    new-instance v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    const-string/jumbo v1, "Ok"

    const v2, 0x104000a

    invoke-direct {v0, v1, v8, v2, v5}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahZ:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    new-instance v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    const-string/jumbo v1, "Gone"

    const/4 v2, 0x5

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahY:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    sget-object v1, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahW:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahX:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahU:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahV:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahZ:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->ahY:Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->aia:[Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->text:I

    iput-boolean p4, p0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->enabled:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;
    .locals 1

    const-class v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    return-object v0
.end method

.method public static values()[Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;
    .locals 1

    sget-object v0, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;->aia:[Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl$RightButtonMode;

    return-object v0
.end method
