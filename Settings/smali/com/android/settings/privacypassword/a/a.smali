.class public Lcom/android/settings/privacypassword/a/a;
.super Ljava/lang/Object;
.source "AnalyticHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static Wb(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEventAnonymous(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static Wc(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEventAnonymous(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static Wd(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "action"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "com.android.settings.privacypassword"

    const-string/jumbo v2, "app1_unlock_binding_popup"

    invoke-static {v1, v2, v0}, Lcom/android/settings/privacypassword/a/a;->Wc(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static We(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "click_time"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "com.android.settings.privacypassword"

    const-string/jumbo v2, "private_forget"

    invoke-static {v1, v2, v0}, Lcom/android/settings/privacypassword/a/a;->Wc(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static Wf(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "com.android.settings.privacypassword"

    const-string/jumbo v2, "set1_forget_page_binding_result"

    invoke-static {v1, v2, v0}, Lcom/android/settings/privacypassword/a/a;->Wc(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static Wg()V
    .locals 2

    const-string/jumbo v0, "com.android.settings.privacypassword"

    const-string/jumbo v1, "private_forget_finish"

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/a/a;->Wb(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static Wh(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "account_status"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "com.android.settings.privacypassword"

    const-string/jumbo v2, "private_mistake_reach_max"

    invoke-static {v1, v2, v0}, Lcom/android/settings/privacypassword/a/a;->Wc(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static Wi(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "user_status"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "com.android.settings.privacypassword"

    const-string/jumbo v2, "set1_forget_page_account_status"

    invoke-static {v1, v2, v0}, Lcom/android/settings/privacypassword/a/a;->Wc(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static Wj(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "account_status"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "com.android.settings.privacypassword"

    const-string/jumbo v2, "set1_page_account_status"

    invoke-static {v1, v2, v0}, Lcom/android/settings/privacypassword/a/a;->Wc(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method
