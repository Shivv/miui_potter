.class final Lcom/android/settings/privacypassword/AddAccountActivity$1;
.super Ljava/lang/Object;
.source "AddAccountActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic aiy:Lcom/android/settings/privacypassword/AddAccountActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/privacypassword/AddAccountActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string/jumbo v2, "booleanResult"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/settings/privacypassword/AddAccountActivity;->Ya(Lcom/android/settings/privacypassword/AddAccountActivity;Z)Z

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/settings/privacypassword/AddAccountActivity;->XZ(Lcom/android/settings/privacypassword/AddAccountActivity;Z)Z

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-static {v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XX(Lcom/android/settings/privacypassword/AddAccountActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/android/settings/privacypassword/ModifyAndInstructionPrivacyPassword;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-static {v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XY(Lcom/android/settings/privacypassword/AddAccountActivity;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    move-result-object v0

    invoke-static {v1}, Lcom/android/settings/privacypassword/XiaomiAccountUtils;->XN(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->XE(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f120271

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-static {v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->Yb(Lcom/android/settings/privacypassword/AddAccountActivity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "not_logged_binding"

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/a/a;->Wf(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/AddAccountActivity;->Ya(Lcom/android/settings/privacypassword/AddAccountActivity;Z)Z

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/AddAccountActivity;->XZ(Lcom/android/settings/privacypassword/AddAccountActivity;Z)Z

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$1;->aiy:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-static {v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XY(Lcom/android/settings/privacypassword/AddAccountActivity;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->XE(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AddAccountActivity"

    const-string/jumbo v2, "fail loginXiaomiAccount"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
