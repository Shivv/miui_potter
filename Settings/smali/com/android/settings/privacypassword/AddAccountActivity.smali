.class public Lcom/android/settings/privacypassword/AddAccountActivity;
.super Lmiui/app/Activity;
.source "AddAccountActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private agB:Landroid/accounts/AccountManagerCallback;

.field private agC:Landroid/widget/ImageView;

.field private agD:Landroid/widget/TextView;

.field private agE:Landroid/widget/TextView;

.field private agF:I

.field private agG:Z

.field private agH:Z

.field private agI:Z

.field private agJ:Z

.field private agK:Landroid/widget/Button;

.field private agL:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

.field private agM:Z

.field private agN:Landroid/widget/Button;

.field private mAccount:Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    iput-boolean v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agM:Z

    iput-boolean v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agI:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agG:Z

    iput-boolean v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agJ:Z

    new-instance v0, Lcom/android/settings/privacypassword/AddAccountActivity$1;

    invoke-direct {v0, p0}, Lcom/android/settings/privacypassword/AddAccountActivity$1;-><init>(Lcom/android/settings/privacypassword/AddAccountActivity;)V

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agB:Landroid/accounts/AccountManagerCallback;

    return-void
.end method

.method private XP()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agH:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "logged_in_back"

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/a/a;->Wf(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agJ:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agG:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "not_logged_cancel_login_back"

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/a/a;->Wf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "not_logged_back"

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/a/a;->Wf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private XQ()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agH:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "logged_in_skip"

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/a/a;->Wf(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agJ:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agG:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "not_logged_cancel_login_skip"

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/a/a;->Wf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "not_logged_skip"

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/a/a;->Wf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private XR()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agF:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "binding_result"

    return-object v0

    :cond_0
    const-string/jumbo v0, "app_binding_result"

    return-object v0
.end method

.method private XS()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "is_start_modify"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agI:Z

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "enter_forgetpage_way"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agF:I

    invoke-static {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agL:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    const v0, 0x7f0a0375

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agC:Landroid/widget/ImageView;

    const v0, 0x7f0a0371

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agD:Landroid/widget/TextView;

    const v0, 0x7f0a01a6

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agK:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agK:Landroid/widget/Button;

    const v3, 0x7f120d55

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    const v0, 0x7f0a01a7

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agN:Landroid/widget/Button;

    const v0, 0x7f0a0373

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agE:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/android/settings/privacypassword/XiaomiAccountUtils;->XN(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agH:Z

    const-string/jumbo v0, "account"

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    iget-boolean v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agH:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    const-string/jumbo v2, "acc_user_name"

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agD:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agN:Landroid/widget/Button;

    const v1, 0x7f120d45

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    :goto_2
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agK:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agN:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agE:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XU()V

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agH:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "logged_in"

    :goto_3
    invoke-static {v0}, Lcom/android/settings/privacypassword/a/a;->Wi(Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agD:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agD:Landroid/widget/TextView;

    const v1, 0x7f120d56

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agN:Landroid/widget/Button;

    const v1, 0x7f120d53

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    :cond_3
    const-string/jumbo v0, "not_logged"

    goto :goto_3
.end method

.method private XT(Landroid/app/Activity;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agJ:Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agB:Landroid/accounts/AccountManagerCallback;

    invoke-static {p1, v0, v1}, Lcom/android/settings/privacypassword/XiaomiAccountUtils;->XM(Landroid/app/Activity;Landroid/os/Bundle;Landroid/accounts/AccountManagerCallback;)V

    return-void
.end method

.method private XU()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.xiaomi.account.action.BIND_XIAOMI_ACCOUNT_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.xiaomi.account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;-><init>(Lcom/android/settings/privacypassword/AddAccountActivity;Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/settings/privacypassword/AddAccountActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method static synthetic XV(Lcom/android/settings/privacypassword/AddAccountActivity;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic XW(Lcom/android/settings/privacypassword/AddAccountActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agC:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic XX(Lcom/android/settings/privacypassword/AddAccountActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agI:Z

    return v0
.end method

.method static synthetic XY(Lcom/android/settings/privacypassword/AddAccountActivity;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agL:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    return-object v0
.end method

.method static synthetic XZ(Lcom/android/settings/privacypassword/AddAccountActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agG:Z

    return p1
.end method

.method static synthetic Ya(Lcom/android/settings/privacypassword/AddAccountActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agM:Z

    return p1
.end method

.method static synthetic Yb(Lcom/android/settings/privacypassword/AddAccountActivity;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XR()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agM:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->setResult(I)V

    invoke-super {p0}, Lmiui/app/Activity;->finish()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Lmiui/app/Activity;->onBackPressed()V

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XP()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Landroid/security/ChooseLockSettingsHelper;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1}, Landroid/security/ChooseLockSettingsHelper;-><init>(Landroid/app/Activity;I)V

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agK:Landroid/widget/Button;

    if-ne p1, v1, :cond_2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/security/ChooseLockSettingsHelper;->setPrivacyPasswordEnabledAsUser(ZI)V

    iput-boolean v2, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agM:Z

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agI:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/settings/privacypassword/ModifyAndInstructionPrivacyPassword;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XQ()V

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agN:Landroid/widget/Button;

    if-ne p1, v1, :cond_5

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/security/ChooseLockSettingsHelper;->setPrivacyPasswordEnabledAsUser(ZI)V

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agH:Z

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agM:Z

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agL:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->XE(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agI:Z

    if-eqz v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/settings/privacypassword/ModifyAndInstructionPrivacyPassword;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->startActivity(Landroid/content/Intent;)V

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f120271

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XR()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "logged_in_binding"

    invoke-static {v0, v1}, Lcom/android/settings/privacypassword/a/a;->Wf(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->finish()V

    goto :goto_0

    :cond_4
    invoke-direct {p0, p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XT(Landroid/app/Activity;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agE:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agM:Z

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->Ye(Landroid/app/Activity;Z)V

    const v0, 0x7f0d0024

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->setContentView(I)V

    invoke-static {}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->Yd()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->setRequestedOrientation(I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XS()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lmiui/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agL:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->XA()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/settings/privacypassword/XiaomiAccountUtils;->XK(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->agL:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->XA()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/android/settings/privacypassword/XiaomiAccountUtils;->XL(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->finish()V

    :cond_0
    return-void
.end method
