.class Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;
.super Ljava/lang/Object;
.source "AddAccountActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private agO:Lcom/xiaomi/accountsdk/account/IXiaomiAccountService;

.field final synthetic agP:Lcom/android/settings/privacypassword/AddAccountActivity;


# direct methods
.method private constructor <init>(Lcom/android/settings/privacypassword/AddAccountActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agP:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/privacypassword/AddAccountActivity;Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;-><init>(Lcom/android/settings/privacypassword/AddAccountActivity;)V

    return-void
.end method


# virtual methods
.method public Yc(Landroid/graphics/Bitmap;IIIIZ)Landroid/graphics/Bitmap;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v7, v7, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    invoke-virtual {v2, p5}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v5, p4

    int-to-float v6, p4

    invoke-virtual {v1, v4, v5, v6, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v4, v7, v7, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, p1, v4, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    if-eqz p6, :cond_0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    const/16 v3, 0x4c

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    div-int/lit8 v3, p2, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, p3, 0x2

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return-object v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 9

    const/4 v8, 0x0

    invoke-static {p2}, Lcom/xiaomi/accountsdk/account/IXiaomiAccountService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/accountsdk/account/IXiaomiAccountService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agO:Lcom/xiaomi/accountsdk/account/IXiaomiAccountService;

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agP:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-static {v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XW(Lcom/android/settings/privacypassword/AddAccountActivity;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agP:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-virtual {v1}, Lcom/android/settings/privacypassword/AddAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agP:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-static {v0}, Lcom/android/settings/privacypassword/AddAccountActivity;->XV(Lcom/android/settings/privacypassword/AddAccountActivity;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agO:Lcom/xiaomi/accountsdk/account/IXiaomiAccountService;

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agP:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-static {v1}, Lcom/android/settings/privacypassword/AddAccountActivity;->XV(Lcom/android/settings/privacypassword/AddAccountActivity;)Landroid/accounts/Account;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/xiaomi/accountsdk/account/IXiaomiAccountService;->getAvatarFd(Landroid/accounts/Account;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    if-eqz v7, :cond_0

    :try_start_1
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    const/4 v5, -0x1

    const/4 v6, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->Yc(Landroid/graphics/Bitmap;IIIIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agP:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-static {v1}, Lcom/android/settings/privacypassword/AddAccountActivity;->XW(Lcom/android/settings/privacypassword/AddAccountActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :goto_0
    if-eqz v7, :cond_1

    :try_start_2
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agP:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-virtual {v0, p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->unbindService(Landroid/content/ServiceConnection;)V

    :goto_2
    iput-object v8, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agO:Lcom/xiaomi/accountsdk/account/IXiaomiAccountService;

    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "AddAccountActivity"

    const-string/jumbo v2, "close file error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v1, v8

    :goto_3
    :try_start_3
    const-string/jumbo v2, "AddAccountActivity"

    const-string/jumbo v3, "Fail getAvatarFd"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :cond_2
    :goto_4
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agP:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-virtual {v0, p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_2

    :catch_2
    move-exception v0

    const-string/jumbo v1, "AddAccountActivity"

    const-string/jumbo v2, "close file error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :catchall_0
    move-exception v0

    move-object v7, v8

    :goto_5
    if-eqz v7, :cond_3

    :try_start_5
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :cond_3
    :goto_6
    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agP:Lcom/android/settings/privacypassword/AddAccountActivity;

    invoke-virtual {v1, p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->unbindService(Landroid/content/ServiceConnection;)V

    iput-object v8, p0, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;->agO:Lcom/xiaomi/accountsdk/account/IXiaomiAccountService;

    throw v0

    :catch_3
    move-exception v1

    const-string/jumbo v2, "AddAccountActivity"

    const-string/jumbo v3, "close file error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v7, v1

    goto :goto_5

    :catch_4
    move-exception v0

    move-object v1, v7

    goto :goto_3

    :cond_4
    move-object v7, v8

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    return-void
.end method
