.class public Lcom/android/settings/BootloaderApplyActivity;
.super Lmiui/app/Activity;
.source "BootloaderApplyActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private cdI:Landroid/widget/Button;

.field private cdJ:Ljava/lang/CharSequence;

.field private cdK:I

.field private cdL:I

.field private cdM:Landroid/os/Handler;

.field private cdN:Landroid/widget/Button;

.field private cdO:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdL:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdK:I

    new-instance v0, Lcom/android/settings/kO;

    invoke-direct {v0, p0}, Lcom/android/settings/kO;-><init>(Lcom/android/settings/BootloaderApplyActivity;)V

    iput-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdM:Landroid/os/Handler;

    return-void
.end method

.method private bVA(ILjava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    const v0, 0x7f12036b

    invoke-virtual {p0, v0}, Lcom/android/settings/BootloaderApplyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    const v0, 0x7f12036c

    invoke-virtual {p0, v0}, Lcom/android/settings/BootloaderApplyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_2
    const v0, 0x7f12036d

    invoke-virtual {p0, v0}, Lcom/android/settings/BootloaderApplyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_3
    const v0, 0x7f12036e

    invoke-virtual {p0, v0}, Lcom/android/settings/BootloaderApplyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_4
    const v0, 0x7f12036f

    invoke-virtual {p0, v0}, Lcom/android/settings/BootloaderApplyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private bVB()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/settings/BootloaderApplyActivity;->setEnabled(Z)V

    return-void
.end method

.method static synthetic bVC(Lcom/android/settings/BootloaderApplyActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdI:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic bVD(Lcom/android/settings/BootloaderApplyActivity;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdK:I

    return v0
.end method

.method static synthetic bVE(Lcom/android/settings/BootloaderApplyActivity;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdL:I

    return v0
.end method

.method static synthetic bVF(Lcom/android/settings/BootloaderApplyActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdM:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic bVG(Lcom/android/settings/BootloaderApplyActivity;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/BootloaderApplyActivity;->cdK:I

    return p1
.end method

.method public static bVy()Z
    .locals 4

    const-string/jumbo v0, "support_bootloader_lock"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sget-boolean v1, Landroid/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "BootloaderApplyActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " support "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lmiui/yellowpage/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private bVz()V
    .locals 1

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/settings/BootloaderApplyActivity;->setEnabled(Z)V

    return-void
.end method

.method public static isEnabled()Z
    .locals 2

    const-string/jumbo v0, "persist.fastboot.enable"

    const-string/jumbo v1, "-1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static setEnabled(Z)V
    .locals 2

    const-string/jumbo v1, "persist.fastboot.enable"

    if-eqz p0, :cond_0

    const-string/jumbo v0, "1"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "0"

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/BootloaderApplyActivity;->setResult(ILandroid/content/Intent;)V

    invoke-super {p0}, Lmiui/app/Activity;->finish()V

    invoke-virtual {p0, v2, v2}, Lcom/android/settings/BootloaderApplyActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x5

    const/4 v3, 0x0

    const/16 v4, 0x64

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdM:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/android/settings/BootloaderApplyActivity;->bVB()V

    invoke-virtual {p0}, Lcom/android/settings/BootloaderApplyActivity;->finish()V

    goto :goto_0

    :sswitch_1
    iget v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdL:I

    if-ne v0, v5, :cond_0

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdM:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/android/settings/BootloaderApplyActivity;->bVz()V

    invoke-virtual {p0}, Lcom/android/settings/BootloaderApplyActivity;->finish()V

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdL:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdL:I

    iput v5, p0, Lcom/android/settings/BootloaderApplyActivity;->cdK:I

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdO:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/BootloaderApplyActivity;->cdL:I

    iget-object v2, p0, Lcom/android/settings/BootloaderApplyActivity;->cdJ:Ljava/lang/CharSequence;

    invoke-direct {p0, v1, v2}, Lcom/android/settings/BootloaderApplyActivity;->bVA(ILjava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdL:I

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdI:Landroid/widget/Button;

    new-array v1, v6, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/settings/BootloaderApplyActivity;->cdK:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f1203a0

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/BootloaderApplyActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdI:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdM:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdM:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdI:Landroid/widget/Button;

    new-array v1, v6, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/settings/BootloaderApplyActivity;->cdK:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f1203a4

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/BootloaderApplyActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a0008 -> :sswitch_1
        0x7f0a0384 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d0050

    invoke-virtual {p0, v0}, Lcom/android/settings/BootloaderApplyActivity;->setContentView(I)V

    const v0, 0x7f0a0510

    invoke-virtual {p0, v0}, Lcom/android/settings/BootloaderApplyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdO:Landroid/widget/TextView;

    const v0, 0x7f0a0384

    invoke-virtual {p0, v0}, Lcom/android/settings/BootloaderApplyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdN:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdN:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0008

    invoke-virtual {p0, v0}, Lcom/android/settings/BootloaderApplyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdI:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdI:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdO:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/BootloaderApplyActivity;->cdL:I

    iget-object v2, p0, Lcom/android/settings/BootloaderApplyActivity;->cdJ:Ljava/lang/CharSequence;

    invoke-direct {p0, v1, v2}, Lcom/android/settings/BootloaderApplyActivity;->bVA(ILjava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdI:Landroid/widget/Button;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/settings/BootloaderApplyActivity;->cdK:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f1203a4

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/BootloaderApplyActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdI:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdM:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/BootloaderApplyActivity;->cdM:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-super {p0}, Lmiui/app/Activity;->onDestroy()V

    return-void
.end method
