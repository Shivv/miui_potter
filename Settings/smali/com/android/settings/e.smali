.class final Lcom/android/settings/e;
.super Landroid/database/ContentObserver;
.source "KeyguardAdvancedSettings.java"


# instance fields
.field private final brS:Landroid/net/Uri;

.field private final brT:Landroid/net/Uri;

.field final synthetic brU:Lcom/android/settings/KeyguardAdvancedSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/KeyguardAdvancedSettings;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/e;->brU:Lcom/android/settings/KeyguardAdvancedSettings;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "lock_screen_allow_private_notifications"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/e;->brS:Landroid/net/Uri;

    const-string/jumbo v0, "lock_screen_show_notifications"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/e;->brT:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public bfc(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/e;->brU:Lcom/android/settings/KeyguardAdvancedSettings;

    invoke-virtual {v0}, Lcom/android/settings/KeyguardAdvancedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/settings/e;->brS:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/settings/e;->brT:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/e;->brS:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/e;->brT:Landroid/net/Uri;

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/e;->brU:Lcom/android/settings/KeyguardAdvancedSettings;

    invoke-static {v0}, Lcom/android/settings/KeyguardAdvancedSettings;->bfb(Lcom/android/settings/KeyguardAdvancedSettings;)V

    :cond_1
    return-void
.end method
