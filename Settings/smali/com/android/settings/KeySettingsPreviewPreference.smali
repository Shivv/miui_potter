.class public Lcom/android/settings/KeySettingsPreviewPreference;
.super Landroid/preference/Preference;
.source "KeySettingsPreviewPreference.java"


# instance fields
.field private final bLj:Ljava/util/ArrayList;

.field private bLk:Ljava/lang/String;

.field private bLl:Landroid/widget/ImageView;

.field private bLm:Landroid/widget/RelativeLayout;

.field private bLn:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLj:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    const v0, 0x7f15006e

    invoke-virtual {p0, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->setLayoutResource(I)V

    return-void
.end method

.method private bBH(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBI(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    return-void
.end method

.method private bBI(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V
    .locals 5

    invoke-static {p2}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bBU(Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v2, Lcom/android/settings/bv;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bBT(Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)I

    move-result v3

    const/16 v4, 0xa

    invoke-direct {v2, v1, v0, v3, v4}, Lcom/android/settings/bv;-><init>(Landroid/content/Context;Landroid/widget/ImageView;II)V

    if-eqz p3, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    new-instance v0, Lcom/android/settings/hv;

    invoke-direct {v0, p0}, Lcom/android/settings/hv;-><init>(Lcom/android/settings/KeySettingsPreviewPreference;)V

    invoke-virtual {v2, v0}, Lcom/android/settings/bv;->bKa(Lcom/android/settings/bw;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLj:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private bBJ()V
    .locals 4

    const v3, 0x7f0802e1

    const v2, 0x7f0802db

    const-string/jumbo v0, "launch_camera"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "screen_shot"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802dd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "launch_voice_assistant"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "launch_google_search"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "go_to_sleep"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "turn_on_torch"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802e0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v0, "close_app"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v0, "split_screen"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802df

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v0, "mi_pay"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_8
    const-string/jumbo v0, "show_menu"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802de

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_9
    const-string/jumbo v0, "launch_recents"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802da

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method private bBL(Landroid/view/View;)V
    .locals 7

    const v6, 0x7f070120

    const v5, 0x7f07011c

    const v4, 0x7f070121

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBM()V

    const-string/jumbo v0, "double_click_power_key"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v3}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLp:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBH(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    :goto_0
    const-string/jumbo v0, "animationanimation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLj:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLj:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bv;

    invoke-virtual {v0}, Lcom/android/settings/bv;->start()V

    goto :goto_1

    :cond_0
    const-string/jumbo v0, "long_press_menu_key"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLq:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0, v4}, Lcom/android/settings/KeySettingsPreviewPreference;->bBI(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "long_press_menu_key_when_lock"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLq:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0, v4}, Lcom/android/settings/KeySettingsPreviewPreference;->bBI(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "long_press_home_key"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLq:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0, v6}, Lcom/android/settings/KeySettingsPreviewPreference;->bBI(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "long_press_back_key"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLq:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0, v5}, Lcom/android/settings/KeySettingsPreviewPreference;->bBI(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_4
    const-string/jumbo v0, "key_combination_power_back"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v3}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLr:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBH(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLo:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0, v5}, Lcom/android/settings/KeySettingsPreviewPreference;->bBI(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v0, "key_combination_power_home"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, v3}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLr:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBH(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLo:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0, v6}, Lcom/android/settings/KeySettingsPreviewPreference;->bBI(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v0, "key_combination_power_menu"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v3}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLr:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBH(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLo:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0, v4}, Lcom/android/settings/KeySettingsPreviewPreference;->bBI(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v0, "three_gesture_down"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->bLs:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBH(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    goto/16 :goto_0

    :cond_8
    const-string/jumbo v0, "key_none"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_9
    const-string/jumbo v0, "launch_recents"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->bBO(Z)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_a
    return-void

    :cond_b
    return-void
.end method

.method private bBM()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLj:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLj:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bv;

    invoke-virtual {v0}, Lcom/android/settings/bv;->stop()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLj:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBN()V

    return-void
.end method

.method private bBN()V
    .locals 3

    const-string/jumbo v0, "close_app"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "show_menu"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "split_screen"

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802de

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802db

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private bBO(Z)V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLm:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f0802d8

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    const v0, 0x7f0802d7

    goto :goto_0
.end method

.method static synthetic bBR(Lcom/android/settings/KeySettingsPreviewPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBJ()V

    return-void
.end method

.method static synthetic bBS(Lcom/android/settings/KeySettingsPreviewPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBN()V

    return-void
.end method


# virtual methods
.method public bBK()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->bBM()V

    :cond_0
    return-void
.end method

.method public bBP(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLk:Ljava/lang/String;

    return-void
.end method

.method public bBQ(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLn:Ljava/lang/String;

    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a0237

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLm:Landroid/widget/RelativeLayout;

    const v0, 0x7f0a0236

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->bLl:Landroid/widget/ImageView;

    invoke-direct {p0, p1}, Lcom/android/settings/KeySettingsPreviewPreference;->bBL(Landroid/view/View;)V

    return-void
.end method
