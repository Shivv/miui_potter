.class public Lcom/android/settings/MiuiSeekBarPreference;
.super Lcom/android/settingslib/MiuiRestrictedPreference;
.source "MiuiSeekBarPreference.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field private bIK:Z

.field private bIL:I

.field private bIM:I

.field private bIN:Landroid/widget/SeekBar;

.field private bIO:Lcom/android/settings/ba;

.field private bIP:Z

.field private mProgress:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/MiuiSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x11100b2

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/MiuiSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/MiuiSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    const v3, 0x7f0d00fa

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settingslib/MiuiRestrictedPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIL:I

    sget-object v0, Lcom/android/internal/R$styleable;->ProgressBar:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIM:I

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiSeekBarPreference;->bzk(I)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    sget-object v0, Lcom/android/internal/R$styleable;->SeekBarPreference:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v3}, Lcom/android/settings/MiuiSeekBarPreference;->setLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public bzj()I
    .locals 1

    iget v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    return v0
.end method

.method public bzk(I)V
    .locals 1

    iget v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIM:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIM:I

    invoke-virtual {p0}, Lcom/android/settings/MiuiSeekBarPreference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public bzl(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIK:Z

    return-void
.end method

.method protected bzm(IZ)V
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIM:I

    if-le p1, v1, :cond_2

    iget v1, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIM:I

    :goto_0
    if-gez v1, :cond_1

    :goto_1
    iget v1, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSeekBarPreference;->persistInt(I)Z

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSeekBarPreference;->notifyChanged()V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, p1

    goto :goto_0
.end method

.method public bzn(Lcom/android/settings/ba;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIO:Lcom/android/settings/ba;

    return-void
.end method

.method bzo(Landroid/widget/SeekBar;)V
    .locals 2

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget v1, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiSeekBarPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSeekBarPreference;->bzm(IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settingslib/MiuiRestrictedPreference;->onBindView(Landroid/view/View;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v0, 0x7f0a03d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIN:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIN:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIN:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIM:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIN:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIN:Landroid/widget/SeekBar;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSeekBarPreference;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIN:Landroid/widget/SeekBar;

    instance-of v0, v0, Lcom/android/settings/widget/DefaultIndicatorSeekBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIN:Landroid/widget/SeekBar;

    check-cast v0, Lcom/android/settings/widget/DefaultIndicatorSeekBar;

    iget v1, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIL:I

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/DefaultIndicatorSeekBar;->setDefaultProgress(I)V

    :cond_0
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const v0, 0x7f0a03d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-virtual {v0, p2, p3}, Landroid/widget/SeekBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    if-eqz p3, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIK:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIP:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiSeekBarPreference;->bzo(Landroid/widget/SeekBar;)V

    :cond_1
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/android/settings/MiuiSeekBarPreference$SavedState;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/android/settingslib/MiuiRestrictedPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    :cond_0
    check-cast p1, Lcom/android/settings/MiuiSeekBarPreference$SavedState;

    invoke-virtual {p1}, Lcom/android/settings/MiuiSeekBarPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/android/settings/MiuiSeekBarPreference$SavedState;->progress:I

    iput v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    iget v0, p1, Lcom/android/settings/MiuiSeekBarPreference$SavedState;->bIQ:I

    iput v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIM:I

    invoke-virtual {p0}, Lcom/android/settings/MiuiSeekBarPreference;->notifyChanged()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Lcom/android/settingslib/MiuiRestrictedPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSeekBarPreference;->isPersistent()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v1, Lcom/android/settings/MiuiSeekBarPreference$SavedState;

    invoke-direct {v1, v0}, Lcom/android/settings/MiuiSeekBarPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    iput v0, v1, Lcom/android/settings/MiuiSeekBarPreference$SavedState;->progress:I

    iget v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIM:I

    iput v0, v1, Lcom/android/settings/MiuiSeekBarPreference$SavedState;->bIQ:I

    return-object v1
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSeekBarPreference;->getPersistedInt(I)I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSeekBarPreference;->setProgress(I)V

    return-void

    :cond_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIP:Z

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIP:Z

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget v1, p0, Lcom/android/settings/MiuiSeekBarPreference;->mProgress:I

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiSeekBarPreference;->bzo(Landroid/widget/SeekBar;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIO:Lcom/android/settings/ba;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiSeekBarPreference;->bIO:Lcom/android/settings/ba;

    invoke-interface {v0}, Lcom/android/settings/ba;->OE()V

    :cond_1
    return-void
.end method

.method public setProgress(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/MiuiSeekBarPreference;->bzm(IZ)V

    return-void
.end method
