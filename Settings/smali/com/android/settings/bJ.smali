.class Lcom/android/settings/bJ;
.super Lmiui/app/ProgressDialog;
.source "EncryptionSettings.java"


# instance fields
.field private bTq:Z

.field final synthetic bTr:Lcom/android/settings/EncryptionSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/EncryptionSettings;I)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/settings/bJ;->bTr:Lcom/android/settings/EncryptionSettings;

    invoke-virtual {p1}, Lcom/android/settings/EncryptionSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lmiui/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-boolean v1, p0, Lcom/android/settings/bJ;->bTq:Z

    invoke-virtual {p0, v1}, Lcom/android/settings/bJ;->setProgressStyle(I)V

    invoke-virtual {p0, v1}, Lcom/android/settings/bJ;->setCancelable(Z)V

    invoke-virtual {p0, v1}, Lcom/android/settings/bJ;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {p1, p2}, Lcom/android/settings/EncryptionSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/bJ;->setMessage(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/android/settings/jn;

    invoke-direct {v0, p0}, Lcom/android/settings/jn;-><init>(Lcom/android/settings/bJ;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/bJ;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/EncryptionSettings;ILcom/android/settings/bJ;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bJ;-><init>(Lcom/android/settings/EncryptionSettings;I)V

    return-void
.end method

.method static synthetic bMA(Lcom/android/settings/bJ;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bJ;->bTq:Z

    return p1
.end method

.method private bMz()V
    .locals 4

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/settings/jo;

    invoke-direct {v1, p0}, Lcom/android/settings/jo;-><init>(Lcom/android/settings/bJ;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public bMy()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/bJ;->bTq:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bJ;->dismiss()V

    :cond_0
    return-void
.end method

.method public show()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/settings/bJ;->bTq:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bJ;->bTq:Z

    invoke-super {p0}, Lmiui/app/ProgressDialog;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    invoke-direct {p0}, Lcom/android/settings/bJ;->bMz()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
