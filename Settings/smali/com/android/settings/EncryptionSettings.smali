.class public Lcom/android/settings/EncryptionSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "EncryptionSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final bTg:I


# instance fields
.field private bTh:Lcom/android/settings/bI;

.field private bTi:Z

.field private bTj:Landroid/preference/CheckBoxPreference;

.field private bTk:Landroid/content/IntentFilter;

.field private bTl:Lcom/android/settings/bJ;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    sput v0, Lcom/android/settings/EncryptionSettings;->bTg:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/jp;

    invoke-direct {v0, p0}, Lcom/android/settings/jp;-><init>(Lcom/android/settings/EncryptionSettings;)V

    iput-object v0, p0, Lcom/android/settings/EncryptionSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private bMp()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternUtils;->clearEncryptionPassword()V

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v0, v3}, Lcom/android/internal/widget/LockPatternUtils;->setCredentialRequiredToDecrypt(Z)V

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v0, Lcom/android/settings/bJ;

    const v1, 0x7f120fd5

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settings/bJ;-><init>(Lcom/android/settings/EncryptionSettings;ILcom/android/settings/bJ;)V

    iput-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTl:Lcom/android/settings/bJ;

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTl:Lcom/android/settings/bJ;

    invoke-virtual {v0}, Lcom/android/settings/bJ;->show()V

    invoke-direct {p0, v3}, Lcom/android/settings/EncryptionSettings;->bMt(Z)V

    return-void
.end method

.method private bMq()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "is_security_encryption_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private bMr(I)V
    .locals 8

    const/4 v2, 0x0

    const v5, 0x7f1209a7

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/bn;->bFG(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/android/settings/ConfirmLockPattern$ConfirmLockPatternFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v6, ""

    move-object v3, p0

    move v4, p1

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bra(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;IILjava/lang/CharSequence;I)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/android/settings/ConfirmLockPassword$ConfirmLockPasswordFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v6, ""

    move-object v3, p0

    move v4, p1

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bra(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;IILjava/lang/CharSequence;I)V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/android/settings/EncryptionSettings;->bMp()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x30000 -> :sswitch_1
        0x40000 -> :sswitch_1
        0x50000 -> :sswitch_1
        0x60000 -> :sswitch_1
    .end sparse-switch
.end method

.method private bMs()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v0, Lcom/android/settings/bJ;

    const v1, 0x7f120fd6

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settings/bJ;-><init>(Lcom/android/settings/EncryptionSettings;ILcom/android/settings/bJ;)V

    iput-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTl:Lcom/android/settings/bJ;

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTl:Lcom/android/settings/bJ;

    invoke-virtual {v0}, Lcom/android/settings/bJ;->show()V

    invoke-direct {p0, v3}, Lcom/android/settings/EncryptionSettings;->bMt(Z)V

    return-void
.end method

.method private bMt(Z)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "is_security_encryption_enabled"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic bMu(Lcom/android/settings/EncryptionSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTj:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic bMv(Lcom/android/settings/EncryptionSettings;)Lcom/android/settings/bJ;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTl:Lcom/android/settings/bJ;

    return-object v0
.end method

.method static synthetic bMw(Lcom/android/settings/EncryptionSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/EncryptionSettings;->bTi:Z

    return p1
.end method

.method static synthetic bMx(Lcom/android/settings/EncryptionSettings;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/EncryptionSettings;->bMr(I)V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/EncryptionSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    if-ne p2, v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/EncryptionSettings;->bMs()V

    goto :goto_0

    :sswitch_1
    if-ne p2, v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/EncryptionSettings;->bMp()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x65 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.mihomemanager"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->isPackageAvailable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f120fd7

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->finish()V

    :cond_0
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/EncryptionSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    const v0, 0x7f150055

    invoke-virtual {p0, v0}, Lcom/android/settings/EncryptionSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "security_encryption_enable"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTj:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTj:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0}, Lcom/android/settings/EncryptionSettings;->bMq()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTk:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTk:Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.miui.EncryptionPassword"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTl:Lcom/android/settings/bJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTl:Lcom/android/settings/bJ;

    invoke-virtual {v0}, Lcom/android/settings/bJ;->bMy()V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/EncryptionSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    const-string/jumbo v1, "security_encryption_enable"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTj:Landroid/preference/CheckBoxPreference;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v0, Lcom/android/settings/bI;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/bI;-><init>(Lcom/android/settings/EncryptionSettings;Lcom/android/settings/bI;)V

    iput-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTh:Lcom/android/settings/bI;

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTh:Lcom/android/settings/bI;

    invoke-virtual {v0}, Lcom/android/settings/bI;->show()V

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public onResume()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "saved_bundle"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const-string/jumbo v2, "show_dialog"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/EncryptionSettings;->bTi:Z

    iget-boolean v0, p0, Lcom/android/settings/EncryptionSettings;->bTi:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/bI;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/bI;-><init>(Lcom/android/settings/EncryptionSettings;Lcom/android/settings/bI;)V

    iput-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTh:Lcom/android/settings/bI;

    iget-object v0, p0, Lcom/android/settings/EncryptionSettings;->bTh:Lcom/android/settings/bI;

    invoke-virtual {v0}, Lcom/android/settings/bI;->show()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/EncryptionSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/EncryptionSettings;->bTk:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "show_dialog"

    iget-boolean v1, p0, Lcom/android/settings/EncryptionSettings;->bTi:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/android/settings/EncryptionSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "saved_bundle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method
