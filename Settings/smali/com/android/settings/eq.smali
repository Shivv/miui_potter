.class final Lcom/android/settings/eq;
.super Landroid/os/AsyncTask;
.source "LockPatternChecker.java"


# instance fields
.field final synthetic ciA:Ljava/util/List;

.field final synthetic ciB:J

.field final synthetic ciC:I

.field final synthetic ciD:Lcom/android/settings/O;

.field private ciy:I

.field final synthetic ciz:Lcom/android/internal/widget/LockPatternUtils;


# direct methods
.method constructor <init>(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;JILcom/android/settings/O;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/eq;->ciz:Lcom/android/internal/widget/LockPatternUtils;

    iput-object p2, p0, Lcom/android/settings/eq;->ciA:Ljava/util/List;

    iput-wide p3, p0, Lcom/android/settings/eq;->ciB:J

    iput p5, p0, Lcom/android/settings/eq;->ciC:I

    iput-object p6, p0, Lcom/android/settings/eq;->ciD:Lcom/android/settings/O;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bZP([B)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/eq;->ciD:Lcom/android/settings/O;

    iget v1, p0, Lcom/android/settings/eq;->ciy:I

    invoke-interface {v0, p1, v1}, Lcom/android/settings/O;->blZ([BI)V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/eq;->doInBackground([Ljava/lang/Void;)[B

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[B
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/eq;->ciz:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/eq;->ciA:Ljava/util/List;

    iget-wide v2, p0, Lcom/android/settings/eq;->ciB:J

    iget v4, p0, Lcom/android/settings/eq;->ciC:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/settings/bn;->bFo(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;JI)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/eq;->ciy:I

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/android/settings/eq;->bZP([B)V

    return-void
.end method
