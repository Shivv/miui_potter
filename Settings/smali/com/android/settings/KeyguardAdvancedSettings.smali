.class public Lcom/android/settings/KeyguardAdvancedSettings;
.super Lcom/android/settings/KeyguardSettingsPreferenceFragment;
.source "KeyguardAdvancedSettings.java"


# instance fields
.field private brJ:Landroid/preference/Preference;

.field private brK:Z

.field private brL:Lcom/android/settings/KeyguardRestrictedListPreference;

.field private brM:I

.field private brN:Landroid/preference/CheckBoxPreference;

.field private final brO:Lcom/android/settings/e;

.field private brP:Landroid/preference/Preference;

.field private brQ:Landroid/preference/CheckBoxPreference;

.field private brR:Z

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/e;

    invoke-direct {v0, p0}, Lcom/android/settings/e;-><init>(Lcom/android/settings/KeyguardAdvancedSettings;)V

    iput-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brO:Lcom/android/settings/e;

    return-void
.end method

.method private beT(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/KeyguardAdvancedSettings;->beU(Z)V

    return-void
.end method

.method private beU(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "enable_screen_on_proximity_sensor"

    invoke-static {v0, v1, p1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method private beV(I)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "lock_screen_allow_private_notifications"

    invoke-static {v1, v2, v0, p1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private beW(I)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "lock_screen_show_notifications"

    invoke-static {v1, v2, v0, p1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private beX()V
    .locals 7

    const v4, 0x7f120978

    const v6, 0x7f120976

    const v3, 0x7f120974

    const-string/jumbo v0, "lock_screen_notifications"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/KeyguardRestrictedListPreference;

    iput-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->hasCommonPassword(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brK:Z

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRu()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v3}, Lcom/android/settings/KeyguardAdvancedSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v4}, Lcom/android/settings/KeyguardAdvancedSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    const/16 v5, 0xc

    invoke-static {v4, v2, v3, v5}, Lcom/android/settings/aN;->bwN(Lcom/android/settings/KeyguardRestrictedListPreference;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    iget-boolean v2, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brK:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0, v6}, Lcom/android/settings/KeyguardAdvancedSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    const/4 v5, 0x4

    invoke-static {v4, v2, v3, v5}, Lcom/android/settings/aN;->bwN(Lcom/android/settings/KeyguardRestrictedListPreference;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    :cond_1
    iget-object v2, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->beY()V

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    new-instance v1, Lcom/android/settings/dt;

    invoke-direct {v1, p0}, Lcom/android/settings/dt;-><init>(Lcom/android/settings/KeyguardAdvancedSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/KeyguardRestrictedListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/KeyguardRestrictedListPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private beY()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->beW(I)Z

    move-result v1

    iget-boolean v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brK:Z

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->beV(I)Z

    move-result v0

    :goto_0
    if-nez v1, :cond_2

    const v0, 0x7f120974

    :goto_1
    iput v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brM:I

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brL:Lcom/android/settings/KeyguardRestrictedListPreference;

    iget v1, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brM:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/KeyguardRestrictedListPreference;->setValue(Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    const v0, 0x7f120978

    goto :goto_1

    :cond_3
    const v0, 0x7f120976

    goto :goto_1
.end method

.method static synthetic beZ(Lcom/android/settings/KeyguardAdvancedSettings;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brM:I

    return v0
.end method

.method static synthetic bfa(Lcom/android/settings/KeyguardAdvancedSettings;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brM:I

    return p1
.end method

.method static synthetic bfb(Lcom/android/settings/KeyguardAdvancedSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->beY()V

    return-void
.end method

.method public static isEllipticProximity(Landroid/content/Context;)Z
    .locals 4

    const/4 v1, 0x0

    const-string/jumbo v0, "Elliptic Proximity"

    const-string/jumbo v0, "Elliptic Labs"

    const-string/jumbo v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v2, "Elliptic Proximity"

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "Elliptic Labs"

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/KeyguardAdvancedSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    const v0, 0x7f150070

    invoke-virtual {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "screen_on_proximity_sensor"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "smartcover_sensitive_small_win_sensor"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brP:Landroid/preference/Preference;

    const-string/jumbo v0, "smartcover_lock_or_unlock_screen"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brQ:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "bluetooth_unlock"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brJ:Landroid/preference/Preference;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brJ:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->beX()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brO:Lcom/android/settings/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/e;->bfc(Z)V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 7

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v6, -0x1

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "lock_screen_signature"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, ":miui:starting_window_label"

    const-string/jumbo v1, ""

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "com.android.settings.OwnerInfoSettings"

    move-object v0, p0

    move-object v1, p0

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/KeyguardAdvancedSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    const-string/jumbo v1, "bluetooth_unlock"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v2, "com.android.settings.MiuiSecurityBluetoothSettingsFragment"

    const v5, 0x7f120363

    move-object v0, p0

    move-object v1, p0

    move v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/KeyguardAdvancedSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "smartcover_lock_or_unlock_screen"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v0, p2

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->setSmartCoverMode(Z)V

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "smartcover_sensitive_small_win_sensor"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v2, "com.android.settings.MiuiSmartCoverSettingsFragment"

    const v5, 0x7f1210e2

    move-object v0, p0

    move-object v1, p0

    move v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/KeyguardAdvancedSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->beU(Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 8

    const/4 v7, -0x1

    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->beX()V

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brO:Lcom/android/settings/e;

    invoke-virtual {v0, v1}, Lcom/android/settings/e;->bfc(Z)V

    const-string/jumbo v0, "lock_screen_signature"

    iget-object v3, p0, Lcom/android/settings/KeyguardAdvancedSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {v0, v3, p0}, Lcom/android/settings/aN;->bwO(Ljava/lang/String;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/settings/KeyguardAdvancedSettings;)V

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->hasCommonPassword(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brK:Z

    const-string/jumbo v0, "support_hall_sensor"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brR:Z

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brR:Z

    if-eqz v0, :cond_6

    const-string/jumbo v0, "support_multiple_small_win_cover"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_0
    new-instance v0, Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brJ:Landroid/preference/Preference;

    iget-boolean v5, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brK:Z

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    invoke-virtual {v0}, Landroid/security/MiuiLockPatternUtils;->getBluetoothUnlockEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brJ:Landroid/preference/Preference;

    const v4, 0x7f120365

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(I)V

    :goto_1
    iget-boolean v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brK:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v4, "android.hardware.sensor.proximity"

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/KeyguardAdvancedSettings;->isEllipticProximity(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_2
    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, v2}, Lcom/android/settings/KeyguardAdvancedSettings;->beU(Z)V

    :cond_3
    :goto_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brP:Landroid/preference/Preference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v0, "persist.sys.smartcover_mode"

    invoke-static {v0, v7}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    iget-object v4, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->setSmartCoverMode(Z)V

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brQ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brP:Landroid/preference/Preference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brJ:Landroid/preference/Preference;

    const v4, 0x7f120364

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "show_lock_before_unlock"

    sget-boolean v5, Landroid/provider/MiuiSettings$System;->SHOW_LOCK_BEFORE_UNLOCK_DEFAULT:Z

    invoke-static {v0, v4, v5}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    iget-object v4, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    if-nez v0, :cond_1

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/KeyguardAdvancedSettings;->beT(Z)V

    goto :goto_2

    :cond_9
    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "enable_screen_on_proximity_sensor"

    invoke-static {v0, v3, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v7, :cond_b

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "enable_screen_on_proximity_sensor"

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x110a0013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/KeyguardAdvancedSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "enable_screen_on_proximity_sensor"

    invoke-static {v0, v2, v1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/android/settings/KeyguardAdvancedSettings;->brN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    :cond_b
    if-nez v0, :cond_a

    move v1, v2

    goto :goto_5
.end method
