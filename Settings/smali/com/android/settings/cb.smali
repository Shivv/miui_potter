.class Lcom/android/settings/cb;
.super Ljava/lang/Object;
.source "EqualizerView.java"


# instance fields
.field private bWi:Lcom/android/settings/cc;

.field private bWj:Lcom/android/settings/cc;

.field private bWk:Lcom/android/settings/cc;

.field private bWl:Lcom/android/settings/cc;

.field private bWm:Lcom/android/settings/cc;

.field private bWn:Lcom/android/settings/cc;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected bQd(Lcom/android/settings/cc;)Lcom/android/settings/cc;
    .locals 4

    invoke-virtual {p1, p1}, Lcom/android/settings/cc;->bQj(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cb;->bWl:Lcom/android/settings/cc;

    iget-object v2, p0, Lcom/android/settings/cb;->bWm:Lcom/android/settings/cc;

    invoke-virtual {v2, p1}, Lcom/android/settings/cc;->bQh(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/cc;->bQf(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/cb;->bWn:Lcom/android/settings/cc;

    invoke-virtual {v2, v0}, Lcom/android/settings/cc;->bQh(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/cc;->bQf(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/cb;->bWi:Lcom/android/settings/cc;

    iget-object v3, p0, Lcom/android/settings/cb;->bWj:Lcom/android/settings/cc;

    invoke-virtual {v3, p1}, Lcom/android/settings/cc;->bQh(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settings/cc;->bQf(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/cb;->bWk:Lcom/android/settings/cc;

    invoke-virtual {v3, v0}, Lcom/android/settings/cc;->bQh(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/settings/cc;->bQf(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/settings/cc;->bQh(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v0

    return-object v0
.end method

.method protected bQe(FFFF)V
    .locals 18

    move/from16 v0, p1

    float-to-double v2, v0

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    mul-double/2addr v2, v4

    move/from16 v0, p2

    float-to-double v4, v0

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const/high16 v6, 0x42200000    # 40.0f

    div-float v6, p3, v6

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    div-double/2addr v8, v4

    add-double/2addr v8, v4

    const/high16 v10, 0x3f800000    # 1.0f

    div-float v10, v10, p4

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v10, v11

    float-to-double v10, v10

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    new-instance v8, Lcom/android/settings/cc;

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    add-double/2addr v10, v4

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v12, v4, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    mul-double/2addr v12, v14

    mul-double/2addr v12, v6

    add-double/2addr v10, v12

    mul-double/2addr v10, v4

    double-to-float v9, v10

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/android/settings/cc;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/android/settings/cb;->bWl:Lcom/android/settings/cc;

    new-instance v8, Lcom/android/settings/cc;

    const-wide/high16 v10, -0x4000000000000000L    # -2.0

    mul-double/2addr v10, v4

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v12, v4, v12

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    add-double/2addr v14, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    mul-double/2addr v10, v12

    double-to-float v9, v10

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/android/settings/cc;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/android/settings/cb;->bWm:Lcom/android/settings/cc;

    new-instance v8, Lcom/android/settings/cc;

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    add-double/2addr v10, v4

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v12, v4, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    mul-double/2addr v12, v14

    mul-double/2addr v12, v6

    sub-double/2addr v10, v12

    mul-double/2addr v10, v4

    double-to-float v9, v10

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/android/settings/cc;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/android/settings/cb;->bWn:Lcom/android/settings/cc;

    new-instance v8, Lcom/android/settings/cc;

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    add-double/2addr v10, v4

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v12, v4, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    mul-double/2addr v12, v14

    mul-double/2addr v12, v6

    add-double/2addr v10, v12

    double-to-float v9, v10

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/android/settings/cc;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/android/settings/cb;->bWi:Lcom/android/settings/cc;

    new-instance v8, Lcom/android/settings/cc;

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    sub-double v10, v4, v10

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    add-double/2addr v12, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    mul-double/2addr v10, v12

    double-to-float v9, v10

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/android/settings/cc;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/android/settings/cb;->bWj:Lcom/android/settings/cc;

    new-instance v8, Lcom/android/settings/cc;

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    add-double/2addr v10, v4

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v12, v4, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v2, v12

    sub-double v2, v10, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v10

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-float v2, v2

    const/4 v3, 0x0

    invoke-direct {v8, v2, v3}, Lcom/android/settings/cc;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/android/settings/cb;->bWk:Lcom/android/settings/cc;

    return-void
.end method
