.class public Lcom/android/settings/ringtone/MultiSimRingtoneSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MultiSimRingtoneSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

.field private boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

.field private bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

.field private bos:Landroid/preference/PreferenceCategory;

.field private bot:I

.field private bou:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

.field private bov:I

.field private bow:I

.field private box:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->box:Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bos:Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    iput v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bov:I

    iput v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bow:I

    new-instance v0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings$1;-><init>(Lcom/android/settings/ringtone/MultiSimRingtoneSettings;)V

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bou:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    return-void
.end method

.method private bbU()V
    .locals 3

    const-string/jumbo v0, "ringtone_slot_setting"

    invoke-virtual {p0, v0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->box:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->box:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->box:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    invoke-static {v1, v2}, Lmiui/util/SimRingtoneUtils;->isDefaultSoundUniform(Landroid/content/Context;I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "ringtone_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bos:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "ringtone_0"

    invoke-virtual {p0, v0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo v0, "ringtone_1"

    invoke-virtual {p0, v0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo v0, "ringtone_2"

    invoke-virtual {p0, v0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method private bbV()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->box:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bos:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bos:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bos:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bov:I

    invoke-virtual {v0, v1}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bbY(I)V

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bow:I

    invoke-virtual {v0, v1}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bbY(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bos:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bos:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bos:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    invoke-virtual {v0, v1}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bbY(I)V

    goto :goto_0
.end method

.method static synthetic bbW(Lcom/android/settings/ringtone/MultiSimRingtoneSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bbV()V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const-string/jumbo v0, "android.intent.extra.ringtone.PICKED_URI"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->onSaveRingtone(Landroid/net/Uri;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->onSaveRingtone(Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->onSaveRingtone(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->finish()V

    return-void

    :cond_0
    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    invoke-static {v0, v2}, Lmiui/util/SimRingtoneUtils;->getExtraRingtoneTypeBySlot(II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bov:I

    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    invoke-static {v0, v3}, Lmiui/util/SimRingtoneUtils;->getExtraRingtoneTypeBySlot(II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bow:I

    const v0, 0x7f15008e

    invoke-virtual {p0, v0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bbU()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bou:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Lmiui/telephony/SubscriptionManager;->removeOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->box:Landroid/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->box:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->box:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bot:I

    iget-object v2, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->box:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-static {v0, v1, v2}, Lmiui/util/SimRingtoneUtils;->setDefaultSoundUniform(Landroid/content/Context;IZ)V

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bbV()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bop:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bbX()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->boq:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bbX()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bor:Lcom/android/settings/ringtone/MultiSimRingtonePreference;

    invoke-virtual {v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bbX()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bou:Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v0, v1}, Lmiui/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtoneSettings;->bbV()V

    return-void
.end method
