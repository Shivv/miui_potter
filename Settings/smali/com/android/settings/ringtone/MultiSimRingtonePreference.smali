.class public Lcom/android/settings/ringtone/MultiSimRingtonePreference;
.super Lmiui/preference/ValuePreference;
.source "MultiSimRingtonePreference.java"


# instance fields
.field private boA:I

.field private boB:Landroid/os/Handler;

.field private boC:Ljava/lang/Runnable;

.field private final boy:I

.field private final boz:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lmiui/preference/ValuePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v2, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boy:I

    iput v3, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boz:I

    new-instance v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/ringtone/MultiSimRingtonePreference$1;-><init>(Lcom/android/settings/ringtone/MultiSimRingtonePreference;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boB:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/ringtone/MultiSimRingtonePreference$2;

    invoke-direct {v0, p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference$2;-><init>(Lcom/android/settings/ringtone/MultiSimRingtonePreference;)V

    iput-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boC:Ljava/lang/Runnable;

    iput v2, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    invoke-virtual {p0, v3}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->setShowRightArrow(Z)V

    return-void
.end method

.method private bbZ()I
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bcd()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bce()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    sget v0, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    return v0
.end method

.method private bca()Landroid/graphics/drawable/Drawable;
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bcd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0803f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bce()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0803f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private bcb()Landroid/util/Pair;
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f120e42

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    :cond_0
    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1210e8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1210eb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bbZ()I

    move-result v2

    invoke-virtual {v1, v2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method private bcc()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    invoke-static {v0, v1}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private bcd()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    const/16 v2, 0x40

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    const/16 v2, 0x400

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    const/16 v2, 0x100

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bce()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    const/16 v2, 0x80

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    const/16 v2, 0x800

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    const/16 v2, 0x200

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic bcf(Lcom/android/settings/ringtone/MultiSimRingtonePreference;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boB:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic bcg(Lcom/android/settings/ringtone/MultiSimRingtonePreference;)Landroid/util/Pair;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bcb()Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bch(Lcom/android/settings/ringtone/MultiSimRingtonePreference;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bcc()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method bbX()Landroid/content/Intent;
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.RINGTONE_PICKER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    sparse-switch v1, :sswitch_data_0

    :goto_0
    const-string/jumbo v1, "android.intent.extra.ringtone.EXISTING_URI"

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    invoke-static {v2, v3}, Landroid/media/ExtraRingtoneManager;->getDefaultSoundSettingUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.ringtone.SHOW_SILENT"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v1, "com.android.thememanager"

    const-string/jumbo v2, "com.android.thememanager.activity.ThemeTabActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0

    :sswitch_0
    const-string/jumbo v1, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.ringtone.SHOW_DEFAULT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "android.intent.extra.ringtone.TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.ringtone.SHOW_DEFAULT"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.ringtone.DEFAULT_URI"

    sget-object v2, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_1
        0x40 -> :sswitch_0
        0x80 -> :sswitch_0
        0x100 -> :sswitch_1
        0x200 -> :sswitch_1
        0x400 -> :sswitch_1
        0x800 -> :sswitch_1
    .end sparse-switch
.end method

.method public bbY(I)V
    .locals 2

    iget v0, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    invoke-direct {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->bca()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boC:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/preference/ValuePreference;->onBindView(Landroid/view/View;)V

    sget v0, Lmiui/R$id;->value_right:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    return-void
.end method

.method onSaveRingtone(Landroid/net/Uri;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/ringtone/MultiSimRingtonePreference;->boA:I

    invoke-static {v0, v1, p1}, Landroid/media/ExtraRingtoneManager;->saveDefaultSound(Landroid/content/Context;ILandroid/net/Uri;)V

    return-void
.end method
