.class Lcom/android/settings/u;
.super Landroid/os/AsyncTask;
.source "MiuiUserCredentialsSettings.java"


# instance fields
.field final synthetic bvy:Lcom/android/settings/MiuiUserCredentialsSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/MiuiUserCredentialsSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/u;->bvy:Lcom/android/settings/MiuiUserCredentialsSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/MiuiUserCredentialsSettings;Lcom/android/settings/u;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/u;-><init>(Lcom/android/settings/MiuiUserCredentialsSettings;)V

    return-void
.end method

.method private biA(Landroid/security/KeyStore;I)Ljava/util/SortedMap;
    .locals 12

    const/4 v2, 0x0

    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    invoke-static {}, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->values()[Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    move-result-object v5

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_5

    aget-object v7, v5, v3

    iget-object v0, v7, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->prefix:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Landroid/security/KeyStore;->list(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v8

    array-length v9, v8

    move v1, v2

    :goto_1
    if-ge v1, v9, :cond_4

    aget-object v10, v8, v1

    invoke-static {p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v11, 0x3e8

    if-ne v0, v11, :cond_2

    const-string/jumbo v0, "profile_key_name_encrypt_"

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "profile_key_name_decrypt_"

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "synthetic_password_"

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    invoke-interface {v4, v10}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    if-nez v0, :cond_3

    new-instance v0, Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    invoke-direct {v0, v10, p2}, Lcom/android/settings/MiuiUserCredentialsSettings$Credential;-><init>(Ljava/lang/String;I)V

    invoke-interface {v4, v10, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v0, v0, Lcom/android/settings/MiuiUserCredentialsSettings$Credential;->bvz:Ljava/util/EnumSet;

    invoke-virtual {v0, v7}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_5
    return-object v4
.end method


# virtual methods
.method protected biB(Ljava/util/List;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/u;->bvy:Lcom/android/settings/MiuiUserCredentialsSettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiUserCredentialsSettings;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/u;->bvy:Lcom/android/settings/MiuiUserCredentialsSettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiUserCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f121397

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/android/settings/u;->bvy:Lcom/android/settings/MiuiUserCredentialsSettings;

    invoke-virtual {v1, v0}, Lcom/android/settings/MiuiUserCredentialsSettings;->bWE(Landroid/view/View;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/u;->bvy:Lcom/android/settings/MiuiUserCredentialsSettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiUserCredentialsSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lcom/android/settings/v;

    iget-object v0, p0, Lcom/android/settings/u;->bvy:Lcom/android/settings/MiuiUserCredentialsSettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiUserCredentialsSettings;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    :goto_1
    const v4, 0x7f0d025e

    invoke-direct {v2, v3, v4, v0}, Lcom/android/settings/v;-><init>(Landroid/content/Context;I[Lcom/android/settings/MiuiUserCredentialsSettings$Credential;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/u;->bvy:Lcom/android/settings/MiuiUserCredentialsSettings;

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiUserCredentialsSettings;->bWE(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    new-array v0, v4, [Lcom/android/settings/MiuiUserCredentialsSettings$Credential;

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/u;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 4

    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    const/16 v2, 0x3e8

    invoke-static {v1, v2}, Landroid/os/UserHandle;->getUid(II)I

    move-result v2

    const/16 v3, 0x3f2

    invoke-static {v1, v3}, Landroid/os/UserHandle;->getUid(II)I

    move-result v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v0, v2}, Lcom/android/settings/u;->biA(Landroid/security/KeyStore;I)Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0, v0, v1}, Lcom/android/settings/u;->biA(Landroid/security/KeyStore;I)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/settings/u;->biB(Ljava/util/List;)V

    return-void
.end method
