.class public Lcom/android/settings/BugreportPreference;
.super Lcom/android/settings/CustomDialogPreference;
.source "BugreportPreference.java"


# instance fields
.field private caR:Landroid/widget/TextView;

.field private caS:Landroid/widget/CheckedTextView;

.field private caT:Landroid/widget/TextView;

.field private caU:Landroid/widget/CheckedTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/CustomDialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private bTk(I)V
    .locals 4

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/app/IActivityManager;->requestBugReport(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "BugreportPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "error taking bugreport (bugreportType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic bTl(Lcom/android/settings/BugreportPreference;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BugreportPreference;->caR:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bTm(Lcom/android/settings/BugreportPreference;)Landroid/widget/CheckedTextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BugreportPreference;->caS:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic bTn(Lcom/android/settings/BugreportPreference;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BugreportPreference;->caT:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic bTo(Lcom/android/settings/BugreportPreference;)Landroid/widget/CheckedTextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BugreportPreference;->caU:Landroid/widget/CheckedTextView;

    return-object v0
.end method


# virtual methods
.method protected iX(Landroid/app/AlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/settings/CustomDialogPreference;->iX(Landroid/app/AlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/BugreportPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0054

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a00b3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/android/settings/BugreportPreference;->caU:Landroid/widget/CheckedTextView;

    const v0, 0x7f0a00b2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BugreportPreference;->caT:Landroid/widget/TextView;

    const v0, 0x7f0a00b1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/android/settings/BugreportPreference;->caS:Landroid/widget/CheckedTextView;

    const v0, 0x7f0a00b0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/BugreportPreference;->caR:Landroid/widget/TextView;

    new-instance v0, Lcom/android/settings/kw;

    invoke-direct {v0, p0}, Lcom/android/settings/kw;-><init>(Lcom/android/settings/BugreportPreference;)V

    iget-object v2, p0, Lcom/android/settings/BugreportPreference;->caU:Landroid/widget/CheckedTextView;

    invoke-virtual {v2, v0}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/settings/BugreportPreference;->caS:Landroid/widget/CheckedTextView;

    invoke-virtual {v2, v0}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/settings/BugreportPreference;->caT:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/settings/BugreportPreference;->caR:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x1040572

    invoke-virtual {p1, v0, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method protected onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/BugreportPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/BugreportPreference;->caS:Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "BugreportPreference"

    const-string/jumbo v2, "Taking full bugreport right away"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v1

    new-array v2, v4, [Landroid/util/Pair;

    const/16 v3, 0x127

    invoke-virtual {v1, v0, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    invoke-direct {p0, v4}, Lcom/android/settings/BugreportPreference;->bTk(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "BugreportPreference"

    const-string/jumbo v2, "Taking interactive bugreport right away"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v1

    new-array v2, v4, [Landroid/util/Pair;

    const/16 v3, 0x126

    invoke-virtual {v1, v0, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/BugreportPreference;->bTk(I)V

    goto :goto_0
.end method
