.class public Lcom/android/settings/ap;
.super Ljava/lang/Object;
.source "ToggleAnimHelper.java"


# instance fields
.field private bCj:Ljava/util/HashMap;

.field private bCk:Ljava/util/HashMap;

.field private bCl:I

.field private bCm:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/ap;->bCk:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/ap;->bCj:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/ap;->bCm:Ljava/util/ArrayList;

    invoke-static {p1}, Lmiui/app/ToggleManager;->createInstance(Landroid/content/Context;)Lmiui/app/ToggleManager;

    invoke-static {p1}, Lmiui/app/ToggleManager;->getUserSelectedToggleOrder(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ap;->bCm:Ljava/util/ArrayList;

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/ap;->bCm:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/ap;->bCm:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lcom/android/settings/ap;->bCj:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060114

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    iput v2, p0, Lcom/android/settings/ap;->bCl:I

    return-void

    :cond_1
    move v2, v0

    goto :goto_1
.end method

.method private bqo(ILandroid/widget/ImageView;Z)Z
    .locals 5

    const/4 v4, 0x0

    if-nez p2, :cond_0

    return v4

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ap;->bCk:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    if-nez v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/settings/ap;->bCk:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    return v4

    :cond_2
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    new-instance v1, Lcom/android/settings/fi;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/settings/fi;-><init>(Lcom/android/settings/ap;Landroid/widget/ImageView;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v1, Lcom/android/settings/fj;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/android/settings/fj;-><init>(Lcom/android/settings/ap;IZLandroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    const/4 v0, 0x1

    return v0

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic bqp(Lcom/android/settings/ap;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ap;->bCj:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic bqq(Lcom/android/settings/ap;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/ap;->bCl:I

    return v0
.end method

.method static synthetic bqr(Lcom/android/settings/ap;ILandroid/widget/ImageView;Z)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/ap;->bqo(ILandroid/widget/ImageView;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public bqn(ILandroid/widget/ImageView;)V
    .locals 3

    invoke-static {p1}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v1

    iget-object v0, p0, Lcom/android/settings/ap;->bCj:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v1, v0, :cond_0

    invoke-static {p1}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/ap;->bqo(ILandroid/widget/ImageView;Z)Z

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/settings/ap;->bCl:I

    invoke-static {p1, p2, v0}, Lmiui/app/ToggleManager;->updateImageView(ILandroid/widget/ImageView;I)V

    goto :goto_0
.end method
