.class public Lcom/android/settings/MiuiSettings;
.super Lmiui/preference/PreferenceActivity;
.source "MiuiSettings.java"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;


# static fields
.field private static final CATEGORY_MAP:Ljava/util/Map;

.field private static final TAG:Ljava/lang/String;

.field private static final btY:Ljava/util/HashMap;


# instance fields
.field private btO:[I

.field private btP:Lcom/android/settingslib/c/a;

.field private btQ:I

.field private btR:I

.field private btS:Lmiui/widget/NavigationLayout;

.field private btT:Z

.field private btU:Ljava/lang/String;

.field private btV:Lcom/android/settings/SettingsFragment;

.field private btW:Z

.field private btX:Lcom/android/settings/vpn2/VpnManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/MiuiSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/MiuiSettings;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/android/settings/MiuiSettings$1;

    invoke-direct {v0}, Lcom/android/settings/MiuiSettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/MiuiSettings;->btY:Ljava/util/HashMap;

    new-instance v0, Lcom/android/settings/MiuiSettings$2;

    invoke-direct {v0}, Lcom/android/settings/MiuiSettings$2;-><init>()V

    sput-object v0, Lcom/android/settings/MiuiSettings;->CATEGORY_MAP:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, -0x1

    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    const v0, 0x7f0a02b2

    const v1, 0x7f0a02f2

    const v2, 0x7f0a052f

    const v3, 0x7f0a01a5

    filled-new-array {v0, v1, v2, v3}, [I

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btO:[I

    const-string/jumbo v0, "com.android.settings.device.MiuiDeviceInfoSettings"

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btU:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettings;->btT:Z

    iput v4, p0, Lcom/android/settings/MiuiSettings;->btR:I

    iput v4, p0, Lcom/android/settings/MiuiSettings;->btQ:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettings;->btW:Z

    return-void
.end method

.method private bgA()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private bgB(Landroid/preference/PreferenceActivity$Header;)Z
    .locals 4

    const/4 v0, 0x0

    iget-wide v2, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    long-to-int v1, v2

    const v2, 0x7f0a052f

    if-ne v1, v2, :cond_1

    const-string/jumbo v0, "no_config_tethering"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    invoke-static {p0, v0}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    const/4 v0, 0x1

    return v0

    :cond_1
    const v2, 0x7f0a02b2

    if-ne v1, v2, :cond_0

    const-string/jumbo v0, "no_config_mobile_networks"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public static bgC(Landroid/content/pm/PackageManager;)Z
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "IN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/settings/MiuiSettings;->bgp()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static bgm()Z
    .locals 2

    const/4 v0, 0x0

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_0

    const-string/jumbo v1, "is_hongmi"

    invoke-static {v1, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    return v0
.end method

.method private bgn(Ljava/util/List;)V
    .locals 8

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-wide v4, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    long-to-int v3, v4

    const v4, 0x7f0a02f5

    if-ne v4, v3, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    move v1, v0

    :cond_1
    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserProfiles()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v3

    const/16 v4, 0x3e7

    if-eq v3, v4, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "com.android.settings.action.EXTRA_SETTINGS"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5, v0}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-boolean v4, v0, Landroid/content/pm/ResolveInfo;->system:Z

    if-eqz v4, :cond_3

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v4, :cond_3

    const-string/jumbo v5, "com.android.settings.category"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-nez v5, :cond_3

    const-string/jumbo v5, "com.android.settings.category"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/android/settings/MiuiSettings;->CATEGORY_MAP:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_3

    new-instance v5, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v5}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    iget-object v7, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, v5, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    sget-object v0, Lcom/android/settings/MiuiSettings;->CATEGORY_MAP:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v5, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    invoke-static {p0, p1, v5}, Lcom/android/settings/aq;->bqI(Landroid/content/Context;Ljava/util/List;Landroid/preference/PreferenceActivity$Header;)Z

    invoke-interface {p1, v1, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    :cond_4
    return-void
.end method

.method private bgo(Ljava/util/List;)V
    .locals 6

    const/4 v1, -0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettings;->btW:Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/MiuiSettings;->bgC(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-wide v4, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    long-to-int v3, v4

    const v4, 0x7f0a000e

    if-ne v4, v3, :cond_1

    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    if-ne v1, v0, :cond_2

    return-void

    :cond_2
    new-instance v1, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v1}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    invoke-static {}, Lcom/android/settings/MiuiSettings;->bgp()Landroid/content/Intent;

    move-result-object v2

    iput-object v2, v1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120a8f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    const v2, 0x7f080209

    iput v2, v1, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettings;->btW:Z

    const-string/jumbo v0, "in_mipay_settings"

    const-string/jumbo v1, "mipay_settings_enabled"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static bgp()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const-string/jumbo v2, "inmipay://inwalletapp?id=mipay.home&miref=miui_setting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string/jumbo v1, "com.mipay.wallet.in"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private bgq(Landroid/os/Bundle;)V
    .locals 3

    const v2, 0x1020002

    const v0, 0x7f0d01bc

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->setContentView(I)V

    if-nez p1, :cond_0

    new-instance v0, Lcom/android/settings/SettingsFragment;

    invoke-direct {v0}, Lcom/android/settings/SettingsFragment;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/SettingsFragment;

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    goto :goto_0
.end method

.method private bgr(Landroid/os/Bundle;)V
    .locals 3

    const v0, 0x7f0d01bf

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->setContentView(I)V

    const v0, 0x7f0a02ba

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/NavigationLayout;

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btS:Lmiui/widget/NavigationLayout;

    iget-object v0, p0, Lcom/android/settings/MiuiSettings;->btS:Lmiui/widget/NavigationLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/widget/NavigationLayout;->openNavigationDrawer(Z)V

    if-nez p1, :cond_0

    new-instance v0, Lcom/android/settings/SettingsFragment;

    invoke-direct {v0}, Lcom/android/settings/SettingsFragment;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lmiui/R$id;->navigation:I

    iget-object v2, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget v1, Lmiui/R$id;->navigation:I

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/SettingsFragment;

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    goto :goto_0
.end method

.method public static bgt(Landroid/content/Context;)I
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Secure;->getSecondSpaceEntranceStatus(Landroid/content/ContentResolver;I)I

    move-result v0

    return v0
.end method

.method private bgv()V
    .locals 4

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.android.settings.FRAGMENT_CLASS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "com.android.settings.FRAGMENT_CLASS"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btU:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/MiuiSettings;->bgA()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->invalidateHeaders()V

    :cond_1
    return-void
.end method

.method public static bgw(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lmiui/securityspace/ConfigUtils;->isSupportSecuritySpace()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/settingslib/H;->csn()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {p0, v2}, Lmiui/securityspace/CrossUserUtils;->isAirSpace(Landroid/content/Context;I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p0}, Lcom/android/settings/MiuiSettings;->bgt(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static bgx(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return v2

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->SIMPLIFIED_CHINESE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    return v2

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method private bgz(Landroid/preference/PreferenceActivity$Header;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v5

    if-lez v5, :cond_2

    invoke-virtual {v4, v2}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v0

    iget-object v3, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    invoke-interface {v0}, Landroid/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_0

    invoke-virtual {v4}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    if-ne v0, v1, :cond_1

    :goto_2
    return v1

    :cond_1
    move v1, v2

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method protected and(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    const-string/jumbo v0, "select_header"

    iget-object v1, p0, Lcom/android/settings/MiuiSettings;->btU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btU:Ljava/lang/String;

    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettings;->bgr(Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettings;->bgq(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public ang(Ljava/util/List;)V
    .locals 17

    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/MiuiSettings;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    invoke-static/range {p0 .. p0}, Lcom/android/settingslib/H;->csk(Landroid/content/Context;)Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/MiuiSettings;->btX:Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {v3}, Lcom/android/settings/vpn2/VpnManager;->OM()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/MiuiSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {}, Lcom/android/settingslib/H;->csn()Z

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/MiuiSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v11

    invoke-static {}, Lmiui/securityspace/ConfigUtils;->isSupportSecuritySpace()Z

    move-result v12

    invoke-static {}, Lcom/android/settings/device/h;->aDV()Z

    move-result v13

    move v4, v2

    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_d

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceActivity$Header;

    iget-wide v14, v2, Landroid/preference/PreferenceActivity$Header;->id:J

    long-to-int v14, v14

    sparse-switch v14, :sswitch_data_0

    :cond_0
    :goto_1
    if-eqz v11, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/settings/MiuiSettings;->btO:[I

    invoke-static {v3, v14}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/android/settings/MiuiSettings;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "updateHeaderList remove header,  myUserId = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v3, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_c

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v2, :cond_c

    add-int/lit8 v4, v4, 0x1

    move v2, v4

    :goto_2
    move v4, v2

    goto :goto_0

    :sswitch_0
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_1
    const/4 v3, 0x0

    if-eqz v3, :cond_2

    const-string/jumbo v3, "ro.miui.singlesim"

    const/4 v15, 0x0

    invoke-static {v3, v15}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v15, 0x1

    if-ne v3, v15, :cond_2

    const v3, 0x7f1210b1

    iput v3, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    iget v3, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :sswitch_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2}, Lcom/android/settings/aq;->bqI(Landroid/content/Context;Ljava/util/List;Landroid/preference/PreferenceActivity$Header;)Z

    goto :goto_1

    :sswitch_3
    const-string/jumbo v3, "android.hardware.wifi"

    invoke-virtual {v10, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/16 :goto_1

    :sswitch_4
    const-string/jumbo v3, "android.hardware.bluetooth"

    invoke-virtual {v10, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/16 :goto_1

    :sswitch_5
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_6
    invoke-static/range {p0 .. p0}, Lmiui/payment/PaymentManager;->get(Landroid/content/Context;)Lmiui/payment/PaymentManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/payment/PaymentManager;->isMibiServiceDisabled()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_7
    invoke-static {}, Lcom/android/settings/MiuiSettings;->bgm()Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_8
    if-eqz v6, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_9
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v3, :cond_3

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v15, 0x15

    if-ge v3, v15, :cond_3

    :goto_3
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_3
    sget-boolean v3, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-eqz v3, :cond_0

    goto :goto_3

    :sswitch_a
    if-eqz v9, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_b
    if-nez v9, :cond_4

    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v3, :cond_0

    :cond_4
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_c
    const-string/jumbo v3, "support_edge_handgrip"

    const/4 v15, 0x0

    invoke-static {v3, v15}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_d
    const-string/jumbo v3, "vibrator"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/settings/MiuiSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Vibrator;

    invoke-virtual {v3}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v3

    if-eqz v3, :cond_5

    const v3, 0x7f1210fb

    :goto_4
    iput v3, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    iget v3, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    goto/16 :goto_1

    :cond_5
    const v3, 0x7f1210f6

    goto :goto_4

    :sswitch_e
    new-instance v3, Lcom/android/settings/bM;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/android/settings/bM;->bNd()Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f120991

    iput v3, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    iget v3, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    goto/16 :goto_1

    :sswitch_f
    invoke-static/range {p0 .. p0}, Lcom/android/settings/MiuiSettings;->bgw(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_10
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v3, :cond_6

    const/4 v3, 0x0

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    :cond_6
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_11
    if-nez v11, :cond_7

    if-nez v9, :cond_7

    xor-int/lit8 v3, v12, 0x1

    if-eqz v3, :cond_0

    :cond_7
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_12
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_8
    iget-object v3, v2, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-eqz v3, :cond_0

    iget-object v3, v2, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    const-string/jumbo v15, "enter_way"

    const-string/jumbo v16, "00003"

    move-object/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    :sswitch_13
    const/4 v3, 0x1

    if-ge v7, v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_14
    sget-object v3, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v15, "clover"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_15
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v3, :cond_0

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v15, "wifi"

    invoke-virtual {v3, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->isPortableHotspotSupported()Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/android/settings/MiuiSettings;->TAG:Ljava/lang/String;

    const-string/jumbo v15, "remove wifi_tether_settings for not supported"

    invoke-static {v3, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_16
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_17
    if-eqz v13, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_18
    if-nez v13, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_19
    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_1a
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_1b
    const/4 v3, 0x0

    if-eqz v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_1c
    const-string/jumbo v3, "window"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Landroid/view/IWindowManager;->hasNavigationBar()Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v3

    goto/16 :goto_1

    :sswitch_1d
    const-string/jumbo v3, "support_main_xiaoai"

    const/4 v15, 0x0

    invoke-static {v3, v15}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/android/settings/dc;->bYw(Landroid/content/Context;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    :cond_9
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_1e
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v3, :cond_a

    const-string/jumbo v3, "support_dual_sim_card"

    const/4 v15, 0x0

    invoke-static {v3, v15}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    :cond_a
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_1f
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :sswitch_20
    iget-object v3, v2, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-eqz v3, :cond_b

    iget-object v3, v2, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/android/settings/MiuiSettings;->bgx(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    :cond_b
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_c
    move v2, v4

    goto/16 :goto_2

    :cond_d
    sget-boolean v2, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v2, :cond_e

    invoke-direct/range {p0 .. p1}, Lcom/android/settings/MiuiSettings;->bgn(Ljava/util/List;)V

    :cond_e
    invoke-direct/range {p0 .. p1}, Lcom/android/settings/MiuiSettings;->bgo(Ljava/util/List;)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a0006 -> :sswitch_17
        0x7f0a0057 -> :sswitch_0
        0x7f0a0070 -> :sswitch_12
        0x7f0a0094 -> :sswitch_4
        0x7f0a011d -> :sswitch_14
        0x7f0a014b -> :sswitch_9
        0x7f0a015f -> :sswitch_c
        0x7f0a01c2 -> :sswitch_1a
        0x7f0a0209 -> :sswitch_1c
        0x7f0a027a -> :sswitch_e
        0x7f0a0287 -> :sswitch_2
        0x7f0a029b -> :sswitch_6
        0x7f0a029e -> :sswitch_1b
        0x7f0a02a0 -> :sswitch_20
        0x7f0a02b2 -> :sswitch_1
        0x7f0a02b6 -> :sswitch_18
        0x7f0a02bc -> :sswitch_1e
        0x7f0a02e9 -> :sswitch_7
        0x7f0a02f2 -> :sswitch_2
        0x7f0a0313 -> :sswitch_16
        0x7f0a0314 -> :sswitch_1f
        0x7f0a03ca -> :sswitch_f
        0x7f0a03d2 -> :sswitch_19
        0x7f0a041b -> :sswitch_d
        0x7f0a042c -> :sswitch_8
        0x7f0a0477 -> :sswitch_10
        0x7f0a0492 -> :sswitch_b
        0x7f0a04e9 -> :sswitch_5
        0x7f0a04f7 -> :sswitch_1d
        0x7f0a04ff -> :sswitch_13
        0x7f0a0501 -> :sswitch_a
        0x7f0a0520 -> :sswitch_3
        0x7f0a052f -> :sswitch_15
        0x7f0a053d -> :sswitch_11
    .end sparse-switch
.end method

.method public bgl()V
    .locals 2

    const v0, 0x7f0a02ba

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/NavigationLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/widget/NavigationLayout;->setNavigationEanbled(Z)V

    return-void
.end method

.method public bgs()Lcom/android/settingslib/c/a;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSettings;->btP:Lcom/android/settingslib/c/a;

    return-object v0
.end method

.method public bgu()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSettings;->btU:Ljava/lang/String;

    return-object v0
.end method

.method public bgy()V
    .locals 2

    const v0, 0x7f0a02ba

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/NavigationLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/widget/NavigationLayout;->setNavigationEanbled(Z)V

    return-void
.end method

.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettings;->btT:Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->invalidateHeaders()V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->finish()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget v0, Lmiui/R$id;->content:I

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v2, v0, Lcom/android/settings/bc;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/android/settings/bc;

    invoke-interface {v0}, Lcom/android/settings/bc;->onBackPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->finish()V

    :goto_0
    return-void

    :cond_2
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MiuiSettings;->btT:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->isFinishing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0}, Lcom/android/settings/SettingsFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0}, Lcom/android/settings/SettingsFragment;->bxj()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettings;->btT:Z

    :cond_0
    return-void
.end method

.method public onBuildStartFragmentIntent(Ljava/lang/String;Landroid/os/Bundle;II)Landroid/content/Intent;
    .locals 6

    invoke-super {p0, p1, p2, p3, p4}, Lmiui/preference/PreferenceActivity;->onBuildStartFragmentIntent(Ljava/lang/String;Landroid/os/Bundle;II)Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lcom/android/settings/wifi/WifiSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-class v1, Lcom/android/settings/wifi/p2p/WifiP2pSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-class v1, Lcom/android/settings/wfd/WifiDisplaySettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-class v1, Lcom/android/settings/bluetooth/BluetoothSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-class v1, Lcom/android/settings/DreamSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string/jumbo v1, "settings:ui_options"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_1
    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v1, :cond_2

    const-class v1, Lcom/android/settings/applications/ApplicationsContainer;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "com.miui.securitycenter"

    const-string/jumbo v2, "com.miui.appmanager.AppManagerMainActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    const-string/jumbo v4, ""

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/android/settings/aq;->bqH(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/android/settingslib/c/a;

    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settingslib/c/a;-><init>(Landroid/content/Context;Landroid/os/UserHandle;Lcom/android/settingslib/c/b;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btP:Lcom/android/settingslib/c/a;

    iget-object v0, p0, Lcom/android/settings/MiuiSettings;->btP:Lcom/android/settingslib/c/a;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/c/a;->cgc(Landroid/content/Context;)V

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v2, v1}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiSettings;->and(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiSettings;->bgv()V

    new-instance v0, Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/vpn2/VpnManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btX:Lcom/android/settings/vpn2/VpnManager;

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    return-void
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .locals 10

    const/high16 v6, 0x10000000

    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    sget-object v0, Lcom/android/settings/MiuiSettings;->btY:Ljava/util/HashMap;

    iget-wide v4, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    long-to-int v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "entry_level_primary"

    invoke-static {v1, v0}, Lcom/android/settings/b/a;->aNn(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v4, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    long-to-int v1, v4

    invoke-static {v0, v1}, Lcom/android/settings/notify/a;->aZm(Landroid/content/Context;I)V

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btU:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettings;->bgB(Landroid/preference/PreferenceActivity$Header;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a029b

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    invoke-static {p0}, Lmiui/payment/PaymentManager;->get(Landroid/content/Context;)Lmiui/payment/PaymentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/payment/PaymentManager;->gotoMiliCenter(Landroid/app/Activity;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a0299

    cmp-long v0, v0, v4

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/notify/a;->aZn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/android/settings/notify/a;->aZo(Landroid/content/Context;Z)V

    :cond_4
    invoke-static {}, Lcom/android/settings/notify/a;->aZp()V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v3, :cond_5

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi"

    new-instance v6, Lcom/android/settings/dx;

    invoke-direct {v6, p0}, Lcom/android/settings/dx;-><init>(Lcom/android/settings/MiuiSettings;)V

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0

    :cond_6
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a0501

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a0492

    cmp-long v0, v0, v4

    if-nez v0, :cond_8

    :cond_7
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    invoke-static {p0}, Lcom/android/settings/dc;->bYs(Landroid/app/Activity;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/android/settings/MiuiSettings;->overridePendingTransition(II)V

    goto :goto_0

    :cond_8
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a01a5

    cmp-long v0, v0, v4

    if-nez v0, :cond_9

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "theme://zhuti.xiaomi.com/list?S.REQUEST_RESOURCE_CODE=fonts&miback=true&miref="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string/jumbo v1, ":miui:starting_window_label"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/android/settings/dc;->bYt(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_9
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a011d

    cmp-long v0, v0, v4

    if-nez v0, :cond_b

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "cappu"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string/jumbo v4, "com.android.settings.datausage.DataUsageSummary"

    iget-object v5, p1, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    iget v8, p1, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    iget v9, p1, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    move-object v3, p0

    move-object v6, v2

    invoke-virtual/range {v3 .. v9}, Lcom/android/settings/MiuiSettings;->startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_b
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a0070

    cmp-long v0, v0, v4

    if-eqz v0, :cond_c

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a0313

    cmp-long v0, v0, v4

    if-nez v0, :cond_e

    :cond_c
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->isInMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_d
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_e
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a053d

    cmp-long v0, v0, v4

    if-eqz v0, :cond_c

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a03ca

    cmp-long v0, v0, v4

    if-eqz v0, :cond_c

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a025a

    cmp-long v0, v0, v4

    if-nez v0, :cond_f

    invoke-static {}, Lcom/android/settings/dc;->bYx()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_f
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a02a0

    cmp-long v0, v0, v4

    if-nez v0, :cond_10

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_10

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    const-string/jumbo v1, "http://userguide.miui.com/home"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_10
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a00af

    cmp-long v0, v0, v4

    if-nez v0, :cond_12

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    const-string/jumbo v1, "appTitle"

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120395

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->isInMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_11
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_12
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a04f7

    cmp-long v0, v0, v4

    if-nez v0, :cond_13

    invoke-static {}, Lcom/android/settings/dc;->bYy()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_13
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    if-eqz v0, :cond_15

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_14

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettings;->bgz(Landroid/preference/PreferenceActivity$Header;)Z

    move-result v0

    if-eqz v0, :cond_14

    return-void

    :cond_14
    iget-object v4, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    iget-object v5, p1, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    iget v8, p1, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    iget v9, p1, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    move-object v3, p0

    move-object v6, v2

    invoke-virtual/range {v3 .. v9}, Lcom/android/settings/MiuiSettings;->startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V

    goto/16 :goto_0

    :cond_15
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_2

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0a02b2

    cmp-long v0, v0, v2

    if-nez v0, :cond_16

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    const-string/jumbo v1, ":miui:starting_window_label"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_16
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget v1, Lmiui/R$id;->content:I

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/settings/BaseFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/settings/BaseFragment;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/BaseFragment;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lmiui/preference/PreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget v1, Lmiui/R$id;->content:I

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/settings/BaseFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/settings/BaseFragment;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/BaseFragment;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lmiui/preference/PreferenceActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiSettings;->setIntent(Landroid/content/Intent;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/MiuiSettings;->btT:Z

    invoke-direct {p0}, Lcom/android/settings/MiuiSettings;->bgv()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->finish()V

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "select_header"

    iget-object v1, p0, Lcom/android/settings/MiuiSettings;->btU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSettings;->btU:Ljava/lang/String;

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onResume()V
    .locals 5

    const/4 v0, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x1

    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    iget-object v2, p0, Lcom/android/settings/MiuiSettings;->btX:Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {v2}, Lcom/android/settings/vpn2/VpnManager;->OM()I

    move-result v2

    iget v3, p0, Lcom/android/settings/MiuiSettings;->btR:I

    if-ne v3, v4, :cond_3

    iput v2, p0, Lcom/android/settings/MiuiSettings;->btR:I

    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p0}, Lcom/android/settings/MiuiSettings;->bgt(Landroid/content/Context;)I

    move-result v2

    iget v3, p0, Lcom/android/settings/MiuiSettings;->btQ:I

    if-ne v3, v4, :cond_4

    iput v2, p0, Lcom/android/settings/MiuiSettings;->btQ:I

    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/android/settings/MiuiSettings;->btW:Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/MiuiSettings;->bgC(Landroid/content/pm/PackageManager;)Z

    move-result v3

    if-eq v2, v3, :cond_5

    :goto_2
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiSettings;->btV:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0}, Lcom/android/settings/SettingsFragment;->bxj()V

    :cond_2
    return-void

    :cond_3
    iget v3, p0, Lcom/android/settings/MiuiSettings;->btR:I

    if-eq v2, v3, :cond_0

    iput v2, p0, Lcom/android/settings/MiuiSettings;->btR:I

    move v0, v1

    goto :goto_0

    :cond_4
    iget v3, p0, Lcom/android/settings/MiuiSettings;->btQ:I

    if-eq v2, v3, :cond_1

    iput v2, p0, Lcom/android/settings/MiuiSettings;->btQ:I

    move v0, v1

    goto :goto_1

    :cond_5
    move v1, v0

    goto :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "select_header"

    iget-object v1, p0, Lcom/android/settings/MiuiSettings;->btU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    move v4, p6

    move v5, p3

    invoke-virtual/range {v0 .. v6}, Lcom/android/settings/MiuiSettings;->startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V

    return-void
.end method

.method public startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V
    .locals 3

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    invoke-super/range {p0 .. p6}, Lmiui/preference/PreferenceActivity;->startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_3

    if-lez p5, :cond_2

    if-nez p2, :cond_1

    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    :cond_1
    const-string/jumbo v0, ":android:show_fragment_title"

    invoke-virtual {p2, v0, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    invoke-static {p0, p1, p2}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v0

    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {v0, p3, p4}, Landroid/app/Fragment;->setTargetFragment(Landroid/app/Fragment;I)V

    :cond_4
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    sget v2, Lmiui/R$id;->content:I

    invoke-virtual {v1, v2, v0, p1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method
