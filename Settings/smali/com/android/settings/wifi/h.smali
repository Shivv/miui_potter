.class Lcom/android/settings/wifi/h;
.super Landroid/net/wifi/WifiManager$WpsCallback;
.source "WpsFragment.java"


# instance fields
.field private alB:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/android/settings/wifi/WpsFragment;)V
    .locals 1

    invoke-direct {p0}, Landroid/net/wifi/WifiManager$WpsCallback;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/wifi/h;->alB:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public onFailed(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/h;->alB:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WpsFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/wifi/WpsFragment;->isAdded()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/android/settings/wifi/WpsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v2, 0x7f121647

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    sget-object v2, Lcom/android/settings/wifi/WpsFragment$State;->aly:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-static {v0, v2, v1}, Lcom/android/settings/wifi/WpsFragment;->abI(Lcom/android/settings/wifi/WpsFragment;Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V

    return-void

    :pswitch_1
    const v2, 0x7f121648

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    const v2, 0x7f12164a

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    const v2, 0x7f121649

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_4
    const v2, 0x7f12164b

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onStarted(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/h;->alB:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WpsFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/wifi/WpsFragment;->isAdded()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-static {v0, p1}, Lcom/android/settings/wifi/WpsFragment;->abD(Lcom/android/settings/wifi/WpsFragment;Ljava/lang/String;)Ljava/lang/String;

    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alA:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-static {v0, v1, v2}, Lcom/android/settings/wifi/WpsFragment;->abI(Lcom/android/settings/wifi/WpsFragment;Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    invoke-static {v0, v2}, Lcom/android/settings/wifi/WpsFragment;->abD(Lcom/android/settings/wifi/WpsFragment;Ljava/lang/String;)Ljava/lang/String;

    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alA:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-static {v0, v1, v2}, Lcom/android/settings/wifi/WpsFragment;->abI(Lcom/android/settings/wifi/WpsFragment;Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSucceeded()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/wifi/h;->alB:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WpsFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/wifi/WpsFragment;->isAdded()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alx:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-virtual {v0}, Lcom/android/settings/wifi/WpsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f121644

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/wifi/WpsFragment;->abI(Lcom/android/settings/wifi/WpsFragment;Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V

    return-void
.end method
