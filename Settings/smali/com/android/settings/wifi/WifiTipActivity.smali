.class public Lcom/android/settings/wifi/WifiTipActivity;
.super Lmiui/app/Activity;
.source "WifiTipActivity.java"


# instance fields
.field private asS:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method public static aiQ(Landroid/content/Context;Z)V
    .locals 2

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "donot_remind_wifi_cmcc_connected_dialog"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static aiR(Landroid/content/Context;)Z
    .locals 3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "donot_remind_wifi_cmcc_connected_dialog"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aiS(I)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, ""

    packed-switch p0, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "donot_remind_wifi_off_airplane_on_dialog"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "donot_remind_wifi_cmcc_connected_dialog"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private aiT()Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, ""

    iget v1, p0, Lcom/android/settings/wifi/WifiTipActivity;->asS:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f12156f

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f1214f0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private aiU()Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, ""

    iget v1, p0, Lcom/android/settings/wifi/WifiTipActivity;->asS:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f121572

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f1214f3

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private aiV()Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, ""

    iget v1, p0, Lcom/android/settings/wifi/WifiTipActivity;->asS:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f121570

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f1214f1

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private aiW()Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, ""

    iget v1, p0, Lcom/android/settings/wifi/WifiTipActivity;->asS:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f121573

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f1214f4

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private aiX()V
    .locals 3

    new-instance v0, Lmiui/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiTipActivity;->aiW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiTipActivity;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiTipActivity;->aiU()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setCheckBox(ZLjava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiTipActivity;->aiT()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/wifi/bD;

    invoke-direct {v2, p0}, Lcom/android/settings/wifi/bD;-><init>(Lcom/android/settings/wifi/WifiTipActivity;)V

    invoke-virtual {v0, v1, v2}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiTipActivity;->aiV()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/wifi/bE;

    invoke-direct {v2, p0}, Lcom/android/settings/wifi/bE;-><init>(Lcom/android/settings/wifi/WifiTipActivity;)V

    invoke-virtual {v0, v1, v2}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->show()V

    return-void
.end method

.method static synthetic aiY(Lcom/android/settings/wifi/WifiTipActivity;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/wifi/WifiTipActivity;->asS:I

    return v0
.end method

.method private getMessage()Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, ""

    iget v1, p0, Lcom/android/settings/wifi/WifiTipActivity;->asS:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f121571

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f1214f2

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiTipActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiTipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_dialog_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/wifi/WifiTipActivity;->asS:I

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiTipActivity;->aiX()V

    return-void
.end method
