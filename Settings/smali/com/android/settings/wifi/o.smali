.class public Lcom/android/settings/wifi/o;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "WifiDatabaseHelper.java"


# static fields
.field private static ame:Lcom/android/settings/wifi/o;

.field private static amf:Lcom/android/settings/wifi/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/settings/wifi/o;->amf:Lcom/android/settings/wifi/o;

    sput-object v0, Lcom/android/settings/wifi/o;->ame:Lcom/android/settings/wifi/o;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string/jumbo v0, "wifi_settings.db"

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method private acK(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string/jumbo v0, "ALTER TABLE wifi ADD COLUMN share_state INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/o;->acL(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method private acL(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string/jumbo v0, "CREATE TABLE wifi_share(_id INTEGER PRIMARY KEY AUTOINCREMENT,uuid TEXT ,marker BIGINT DEFAULT 0,longitude TEXT,latitude TEXT,deleted INTEGER DEFAULT 0,sync_state INTEGER DEFAULT 0,share_state INTEGER DEFAULT 0,share_count INTEGER DEFAULT 0,share_connect_state INTEGER DEFAULT 0,share_feedback INTEGER DEFAULT 0,share_upate_time INTEGER DEFAULT 0,account TEXT,ssid TEXT,bssid TEXT,psk TEXT,wepkey0 TEXT,wepkey1 TEXT,wepkey2 TEXT,wepkey3 TEXT,wep_tx_keyidx INTEGER,priority INTEGER,scan_ssid INTEGER,adhoc INTEGER,keyMgmt TEXT,proto TEXT,authAlg TEXT,pairwise TEXT,groupCipher TEXT,eap TEXT,phase2 TEXT,identity TEXT,anonymousIdentity TEXT,password TEXT,clientCert TEXT,privateKey TEXT,caCert TEXT,clientCertFile TEXT,privateKeyFile TEXT,caCertFile TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private acM(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string/jumbo v0, "CREATE TABLE wifi(_id INTEGER PRIMARY KEY AUTOINCREMENT,uuid TEXT ,marker INTEGER DEFAULT 0,longitude TEXT,latitude TEXT,deleted INTEGER DEFAULT 0,sync_state INTEGER DEFAULT 0,share_state INTEGER DEFAULT 0,account TEXT,ssid TEXT,bssid TEXT,psk TEXT,wepkey0 TEXT,wepkey1 TEXT,wepkey2 TEXT,wepkey3 TEXT,wep_tx_keyidx INTEGER,priority INTEGER,scan_ssid INTEGER,adhoc INTEGER,keyMgmt TEXT,proto TEXT,authAlg TEXT,pairwise TEXT,groupCipher TEXT,eap TEXT,phase2 TEXT,identity TEXT,anonymousIdentity TEXT,password TEXT,clientCert TEXT,privateKey TEXT,caCert TEXT,clientCertFile TEXT,privateKeyFile TEXT,caCertFile TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string/jumbo v0, "CREATE TABLE wifi_sync(_id INTEGER PRIMARY KEY AUTOINCREMENT,account_name TEXT NOT NULL,marker INTEGER DEFAULT 0,sync_extra_info TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/o;->acL(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method static declared-synchronized acN(Landroid/content/Context;)Lcom/android/settings/wifi/o;
    .locals 3

    const-class v1, Lcom/android/settings/wifi/o;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/settings/wifi/o;->amf:Lcom/android/settings/wifi/o;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/wifi/o;

    invoke-static {p0}, Lcom/android/settings/wifi/p;->acO(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/settings/wifi/o;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/wifi/o;->amf:Lcom/android/settings/wifi/o;

    :cond_0
    sget-object v0, Lcom/android/settings/wifi/o;->amf:Lcom/android/settings/wifi/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/o;->acM(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    const/4 v1, 0x6

    const/4 v2, 0x3

    if-ge p2, v2, :cond_5

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/o;->acK(Landroid/database/sqlite/SQLiteDatabase;)V

    move v0, v2

    :goto_0
    if-ne v0, v2, :cond_0

    const-string/jumbo v0, "update wifi set deleted = 1, sync_state=0 where keyMgmt = \"NONE\" "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    move v0, v1

    :cond_0
    if-ne v0, v1, :cond_2

    :try_start_0
    const-string/jumbo v1, "select * from wifi_share"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    const-string/jumbo v1, "ALTER TABLE wifi_sync ADD COLUMN sync_extra_info TEXT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    :cond_3
    if-eq v0, p3, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Upgrade wifi database to version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "fails"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v1

    :try_start_1
    invoke-direct {p0, p1}, Lcom/android/settings/wifi/o;->acK(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    throw v0

    :cond_4
    return-void

    :cond_5
    move v0, p2

    goto :goto_0
.end method
