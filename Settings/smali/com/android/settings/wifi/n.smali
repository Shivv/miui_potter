.class public Lcom/android/settings/wifi/n;
.super Ljava/lang/Object;
.source "WifiEnabler.java"

# interfaces
.implements Lcom/android/settings/widget/j;


# instance fields
.field private alX:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final alY:Lcom/android/settings/wifi/y;

.field private final alZ:Landroid/content/IntentFilter;

.field private ama:Z

.field private amb:Z

.field private final amc:Lcom/android/settings/widget/i;

.field private final amd:Landroid/net/wifi/WifiManager;

.field private mContext:Landroid/content/Context;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/widget/i;Lcom/android/settings/core/instrumentation/e;)V
    .locals 2

    new-instance v1, Lcom/android/settings/wifi/y;

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-direct {v1, v0}, Lcom/android/settings/wifi/y;-><init>(Landroid/net/ConnectivityManager;)V

    invoke-direct {p0, p1, p2, p3, v1}, Lcom/android/settings/wifi/n;-><init>(Landroid/content/Context;Lcom/android/settings/widget/i;Lcom/android/settings/core/instrumentation/e;Lcom/android/settings/wifi/y;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settings/widget/i;Lcom/android/settings/core/instrumentation/e;Lcom/android/settings/wifi/y;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/android/settings/wifi/n;->ama:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/settings/wifi/n;->alX:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Lcom/android/settings/wifi/as;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/as;-><init>(Lcom/android/settings/wifi/n;)V

    iput-object v0, p0, Lcom/android/settings/wifi/n;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/i;->aAj(Lcom/android/settings/widget/j;)V

    iput-object p3, p0, Lcom/android/settings/wifi/n;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/n;->amd:Landroid/net/wifi/WifiManager;

    iput-object p4, p0, Lcom/android/settings/wifi/n;->alY:Lcom/android/settings/wifi/y;

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/wifi/n;->alZ:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/n;->alZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/n;->alZ:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/n;->acE()V

    return-void
.end method

.method private acA(Z)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/wifi/n;->amd:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v2

    if-eqz p1, :cond_2

    const/16 v3, 0xc

    if-eq v2, v3, :cond_0

    const/16 v3, 0xd

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private acD(Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/n;->amb:Z

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/i;->setChecked(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/wifi/n;->amb:Z

    return-void
.end method

.method static synthetic acG(Lcom/android/settings/wifi/n;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/n;->alX:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic acH(Lcom/android/settings/wifi/n;)Landroid/net/wifi/WifiManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amd:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic acI(Lcom/android/settings/wifi/n;Landroid/net/NetworkInfo$DetailedState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/n;->acy(Landroid/net/NetworkInfo$DetailedState;)V

    return-void
.end method

.method static synthetic acJ(Lcom/android/settings/wifi/n;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/n;->acz(I)V

    return-void
.end method

.method private acy(Landroid/net/NetworkInfo$DetailedState;)V
    .locals 0

    return-void
.end method

.method private acz(I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/i;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, v3}, Lcom/android/settings/wifi/n;->acD(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/n;->acA(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "no_config_tethering"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v3}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    :cond_0
    :goto_1
    return-void

    :pswitch_1
    invoke-direct {p0, v2}, Lcom/android/settings/wifi/n;->acD(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v3}, Lcom/android/settings/wifi/n;->acD(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "no_config_tethering"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/i;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public acB(Z)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/settings/wifi/n;->amb:Z

    if-eqz v0, :cond_0

    return v3

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-static {v0, v1}, Lcom/android/settingslib/G;->csg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    const v1, 0x7f121549

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/i;->setChecked(Z)V

    return v2

    :cond_1
    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/dc;->bsf(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/n;->acA(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/n;->alY:Lcom/android/settings/wifi/y;

    invoke-virtual {v0, v2}, Lcom/android/settings/wifi/y;->aeG(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/n;->amd:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v3}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    const v1, 0x7f121537

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_3
    return v3
.end method

.method public acC(Landroid/content/Context;)V
    .locals 3

    iput-object p1, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/n;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/n;->alZ:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/android/settings/wifi/n;->ama:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAk()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/n;->ama:Z

    :cond_0
    return-void
.end method

.method public acE()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amd:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/n;->acz(I)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/n;->ama:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAk()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/n;->ama:Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAl()V

    return-void
.end method

.method public acF()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/wifi/n;->ama:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAm()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/wifi/n;->ama:Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAn()V

    return-void
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/n;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/n;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/n;->ama:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/n;->amc:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAm()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/wifi/n;->ama:Z

    :cond_0
    return-void
.end method
