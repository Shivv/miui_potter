.class final Lcom/android/settings/wifi/aS;
.super Landroid/content/BroadcastReceiver;
.source "WifiStatusController.java"


# instance fields
.field final synthetic aun:Lcom/android/settings/wifi/M;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/M;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/aS;->aun:Lcom/android/settings/wifi/M;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/aS;->aun:Lcom/android/settings/wifi/M;

    const-string/jumbo v1, "wifi_state"

    const/4 v2, 0x4

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/wifi/M;->agZ(Lcom/android/settings/wifi/M;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/aS;->aun:Lcom/android/settings/wifi/M;

    invoke-virtual {v0}, Lcom/android/settings/wifi/M;->wv()V

    goto :goto_0
.end method
