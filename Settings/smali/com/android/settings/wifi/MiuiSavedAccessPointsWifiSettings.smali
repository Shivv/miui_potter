.class public Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiSavedAccessPointsWifiSettings.java"


# instance fields
.field private amt:Ljava/util/List;

.field private amu:Lmiui/widget/EditableListViewWrapper;

.field private amv:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private adc(Lcom/android/settingslib/wifi/i;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amv:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    iget v1, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    new-instance v2, Lcom/android/settings/wifi/az;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/wifi/az;-><init>(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;Lcom/android/settingslib/wifi/i;)V

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->forget(ILandroid/net/wifi/WifiManager$ActionListener;)V

    return-void
.end method

.method private add()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/j;

    invoke-virtual {v0}, Lcom/android/settings/wifi/j;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/wifi/j;->abS()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->adc(Lcom/android/settingslib/wifi/i;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private ade()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amu:Lmiui/widget/EditableListViewWrapper;

    if-nez v0, :cond_0

    new-instance v0, Lmiui/widget/EditableListViewWrapper;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/widget/EditableListViewWrapper;-><init>(Landroid/widget/AbsListView;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amu:Lmiui/widget/EditableListViewWrapper;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amu:Lmiui/widget/EditableListViewWrapper;

    new-instance v1, Lcom/android/settings/wifi/r;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/r;-><init>(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;)V

    invoke-virtual {v0, v1}, Lmiui/widget/EditableListViewWrapper;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amu:Lmiui/widget/EditableListViewWrapper;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/widget/EditableListViewWrapper;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    return-void
.end method

.method private adf(Ljava/util/Collection;)Ljava/util/ArrayList;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/wifi/i;

    new-instance v3, Lcom/android/settings/wifi/j;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/android/settings/wifi/j;-><init>(Lcom/android/settingslib/wifi/i;Landroid/content/Context;)V

    iget-object v0, v0, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/android/settings/wifi/j;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/android/settings/wifi/ax;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/ax;-><init>(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_1
    return-object v1
.end method

.method private adg()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amu:Lmiui/widget/EditableListViewWrapper;

    invoke-virtual {v0}, Lmiui/widget/EditableListViewWrapper;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->adh(Z)V

    :cond_0
    return-void
.end method

.method private adh(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/j;

    invoke-virtual {v0, p1}, Lcom/android/settings/wifi/j;->abU(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private adi(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/j;

    invoke-virtual {v0, p1}, Lcom/android/settings/wifi/j;->abT(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private adj()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-static {v0, v2, v1, v2}, Lcom/android/settingslib/wifi/a;->cgE(Landroid/content/Context;ZZZ)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "MiuiSavedAccessPointsWifiSettings"

    const-string/jumbo v1, "Saved networks activity loaded, but there are no saved networks!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v3, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->adf(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/j;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic adk(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amt:Ljava/util/List;

    return-object v0
.end method

.method static synthetic adl(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;Lcom/android/settingslib/wifi/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->adc(Lcom/android/settingslib/wifi/i;)V

    return-void
.end method

.method static synthetic adm(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->add()V

    return-void
.end method

.method static synthetic adn(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->adg()V

    return-void
.end method

.method static synthetic ado(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->adh(Z)V

    return-void
.end method

.method static synthetic adp(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->adi(Z)V

    return-void
.end method

.method static synthetic adq(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->adj()V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->adj()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->ade()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f1500b3

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->amv:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    const/4 v5, 0x1

    instance-of v0, p2, Lcom/android/settings/wifi/j;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/android/settings/wifi/j;

    invoke-virtual {p2}, Lcom/android/settings/wifi/j;->abS()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lmiui/R$style;->Theme_Light_Dialog_Alert:I

    invoke-direct {v1, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f12056b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, v0, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f1201f3

    invoke-virtual {p0, v3, v2}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/settings/wifi/ay;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/wifi/ay;-><init>(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;Lcom/android/settingslib/wifi/i;)V

    const v0, 0x7f121559

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1215c6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return v5

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    return-void
.end method
