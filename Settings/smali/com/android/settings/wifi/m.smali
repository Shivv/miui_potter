.class public Lcom/android/settings/wifi/m;
.super Ljava/lang/Object;
.source "WifiApEnabler.java"


# instance fields
.field alQ:Landroid/net/ConnectivityManager;

.field private final alR:Lcom/android/settings/datausage/DataSaverBackend;

.field private final alS:Landroid/content/IntentFilter;

.field private final alT:Ljava/lang/CharSequence;

.field private final alU:Landroid/preference/CheckBoxPreference;

.field private alV:Landroid/net/wifi/WifiManager;

.field private alW:[Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/datausage/DataSaverBackend;Landroid/preference/CheckBoxPreference;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/wifi/ar;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/ar;-><init>(Lcom/android/settings/wifi/m;)V

    iput-object v0, p0, Lcom/android/settings/wifi/m;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settings/wifi/m;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/wifi/m;->alR:Lcom/android/settings/datausage/DataSaverBackend;

    iput-object p3, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p3}, Landroid/preference/CheckBoxPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/m;->alT:Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/m;->alV:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/android/settings/wifi/m;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/settings/wifi/m;->alQ:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alQ:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherableWifiRegexs()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/m;->alW:[Ljava/lang/String;

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/wifi/m;->alS:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alS:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alS:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-void
.end method

.method private acq()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/m;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "airplane_mode_on"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    const-string/jumbo v0, "WifiApEnabler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set enableWifiApSwitch to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/m;->alR:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v2}, Lcom/android/settings/datausage/DataSaverBackend;->kq()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " in enableWifiSwitch()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/m;->alR:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v1}, Lcom/android/settings/datausage/DataSaverBackend;->kq()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/wifi/m;->alT:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_1
.end method

.method private acr(II)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    if-ne p2, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121594

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/wifi/m;->acq()V

    :goto_1
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121620

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/m;->alR:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v1}, Lcom/android/settings/datausage/DataSaverBackend;->kq()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121621

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/m;->alT:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/m;->acq()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121537

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private acu([Ljava/lang/Object;[Ljava/lang/Object;[Ljava/lang/Object;)V
    .locals 11

    const/4 v2, 0x1

    const/4 v4, 0x0

    array-length v7, p2

    move v5, v4

    move v6, v4

    :goto_0
    if-ge v5, v7, :cond_2

    aget-object v0, p2, v5

    check-cast v0, Ljava/lang/String;

    iget-object v8, p0, Lcom/android/settings/wifi/m;->alW:[Ljava/lang/String;

    array-length v9, v8

    move v3, v4

    move v1, v6

    :goto_1
    if-ge v3, v9, :cond_1

    aget-object v6, v8, v3

    invoke-virtual {v0, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v1, v2

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v6, v1

    goto :goto_0

    :cond_2
    array-length v7, p3

    move v5, v4

    move v1, v4

    :goto_2
    if-ge v5, v7, :cond_5

    aget-object v0, p3, v5

    check-cast v0, Ljava/lang/String;

    iget-object v8, p0, Lcom/android/settings/wifi/m;->alW:[Ljava/lang/String;

    array-length v9, v8

    move v3, v4

    :goto_3
    if-ge v3, v9, :cond_4

    aget-object v10, v8, v3

    invoke-virtual {v0, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    move v1, v2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_5
    if-eqz v6, :cond_6

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alV:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/m;->act(Landroid/net/wifi/WifiConfiguration;)V

    :goto_4
    return-void

    :cond_6
    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121537

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121547

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_4
.end method

.method static synthetic acv(Lcom/android/settings/wifi/m;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/m;->acq()V

    return-void
.end method

.method static synthetic acw(Lcom/android/settings/wifi/m;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/m;->acr(II)V

    return-void
.end method

.method static synthetic acx(Lcom/android/settings/wifi/m;[Ljava/lang/Object;[Ljava/lang/Object;[Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/wifi/m;->acu([Ljava/lang/Object;[Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public acs()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/m;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/m;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/m;->alS:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/android/settings/wifi/m;->acq()V

    return-void
.end method

.method public act(Landroid/net/wifi/WifiConfiguration;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/wifi/m;->mContext:Landroid/content/Context;

    const v1, 0x1108004a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/m;->alU:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/wifi/m;->mContext:Landroid/content/Context;

    const v3, 0x7f12161c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    if-nez p1, :cond_0

    :goto_0
    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/m;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/m;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
