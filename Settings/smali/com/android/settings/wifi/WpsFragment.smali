.class public Lcom/android/settings/wifi/WpsFragment;
.super Lcom/android/settings/BaseFragment;
.source "WpsFragment.java"


# instance fields
.field private alg:Landroid/widget/Button;

.field private alh:Landroid/content/IntentFilter;

.field private ali:Landroid/os/Handler;

.field private alj:Z

.field private alk:Ljava/lang/String;

.field private all:Lmiui/widget/ProgressBar;

.field private alm:Landroid/widget/Button;

.field aln:Lcom/android/settings/wifi/WpsFragment$State;

.field private alo:Landroid/widget/TextView;

.field private alp:Lmiui/widget/ProgressBar;

.field private alq:Ljava/util/Timer;

.field private alr:Landroid/net/wifi/WifiManager;

.field private als:Landroid/widget/ImageView;

.field private alt:Landroid/net/wifi/WifiManager$WpsCallback;

.field private alu:I

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->ali:Landroid/os/Handler;

    sget-object v0, Lcom/android/settings/wifi/WpsFragment$State;->alz:Lcom/android/settings/wifi/WpsFragment$State;

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->aln:Lcom/android/settings/wifi/WpsFragment$State;

    return-void
.end method

.method static synthetic abA(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic abB(Lcom/android/settings/wifi/WpsFragment;)Lmiui/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alp:Lmiui/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic abC(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->als:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic abD(Lcom/android/settings/wifi/WpsFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/WpsFragment;->alk:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic abE(Lcom/android/settings/wifi/WpsFragment;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/WpsFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic abF(Lcom/android/settings/wifi/WpsFragment;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/WpsFragment;->abq(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic abG(Lcom/android/settings/wifi/WpsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/WpsFragment;->start()V

    return-void
.end method

.method static synthetic abH(Lcom/android/settings/wifi/WpsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/WpsFragment;->stop()V

    return-void
.end method

.method static synthetic abI(Lcom/android/settings/wifi/WpsFragment;Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/WpsFragment;->abr(Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic abJ(Lcom/android/settings/wifi/WpsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/WpsFragment;->abs()V

    return-void
.end method

.method private abq(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->aln:Lcom/android/settings/wifi/WpsFragment$State;

    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alx:Lcom/android/settings/wifi/WpsFragment$State;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/wifi/WpsFragment;->abt()V

    :cond_0
    return-void
.end method

.method private abr(Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/wifi/WpsFragment;->aln:Lcom/android/settings/wifi/WpsFragment$State;

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->ali:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/wifi/ao;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/wifi/ao;-><init>(Lcom/android/settings/wifi/WpsFragment;Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private abs()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alk:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alo:Landroid/widget/TextView;

    const v1, 0x7f12164e    # 1.941831E38f

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/WpsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/settings/wifi/WpsFragment;->alk:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->als:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alo:Landroid/widget/TextView;

    const v1, 0x7f12164d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->als:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private abt()V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/android/settingslib/wifi/i;->cij(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const v1, 0x7f121645

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/WpsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-boolean v4, p0, Lcom/android/settings/wifi/WpsFragment;->alj:Z

    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alw:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-direct {p0, v1, v0}, Lcom/android/settings/wifi/WpsFragment;->abr(Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method static synthetic abu(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alg:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic abv(Lcom/android/settings/wifi/WpsFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->ali:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic abw(Lcom/android/settings/wifi/WpsFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alk:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic abx(Lcom/android/settings/wifi/WpsFragment;)Lmiui/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->all:Lmiui/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic aby(Lcom/android/settings/wifi/WpsFragment;)Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic abz(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alm:Landroid/widget/Button;

    return-object v0
.end method

.method private start()V
    .locals 6

    const-wide/16 v2, 0x3e8

    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alq:Ljava/util/Timer;

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alq:Ljava/util/Timer;

    new-instance v1, Lcom/android/settings/wifi/al;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/al;-><init>(Lcom/android/settings/wifi/WpsFragment;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    new-instance v0, Lcom/android/settings/wifi/an;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/an;-><init>(Lcom/android/settings/wifi/WpsFragment;)V

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Lcom/android/settings/wifi/WpsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WpsFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/WpsFragment;->alh:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/android/settings/wifi/WpsFragment;->alj:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/wifi/WpsFragment;->abt()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/net/wifi/WpsInfo;

    invoke-direct {v0}, Landroid/net/wifi/WpsInfo;-><init>()V

    iget v1, p0, Lcom/android/settings/wifi/WpsFragment;->alu:I

    iput v1, v0, Landroid/net/wifi/WpsInfo;->setup:I

    iget-object v1, p0, Lcom/android/settings/wifi/WpsFragment;->alr:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/android/settings/wifi/WpsFragment;->alt:Landroid/net/wifi/WifiManager$WpsCallback;

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->startWps(Landroid/net/wifi/WpsInfo;Landroid/net/wifi/WifiManager$WpsCallback;)V

    goto :goto_0
.end method

.method private stop()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->aln:Lcom/android/settings/wifi/WpsFragment$State;

    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alx:Lcom/android/settings/wifi/WpsFragment$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager;->cancelWps(Landroid/net/wifi/WifiManager$WpsCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alq:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alq:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v2, p0, Lcom/android/settings/wifi/WpsFragment;->alq:Ljava/util/Timer;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/wifi/WpsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/WpsFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/android/settings/wifi/WpsFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    :cond_2
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/WpsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WpsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/WpsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    iput v2, p0, Lcom/android/settings/wifi/WpsFragment;->alu:I

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/WpsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alr:Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/android/settings/wifi/h;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/h;-><init>(Lcom/android/settings/wifi/WpsFragment;)V

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alt:Landroid/net/wifi/WifiManager$WpsCallback;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alh:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alh:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    const-string/jumbo v0, "wps_setup_finish"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/WpsFragment;->alj:Z

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/wifi/WpsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "wps_setup"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/wifi/WpsFragment;->alu:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/WpsFragment;->stop()V

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "wps_setup_finish"

    iget-boolean v1, p0, Lcom/android/settings/wifi/WpsFragment;->alj:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f0a053b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alo:Landroid/widget/TextView;

    const v0, 0x7f0a0208

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->als:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/android/settings/wifi/WpsFragment;->abs()V

    const v0, 0x7f0a053a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alp:Lmiui/widget/ProgressBar;

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alp:Lmiui/widget/ProgressBar;

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, Lmiui/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alp:Lmiui/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/widget/ProgressBar;->setProgress(I)V

    const v0, 0x7f0a0539

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->all:Lmiui/widget/ProgressBar;

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->all:Lmiui/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lmiui/widget/ProgressBar;->setVisibility(I)V

    const v0, 0x7f0a00d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alg:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alg:Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/wifi/aj;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/aj;-><init>(Lcom/android/settings/wifi/WpsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0390

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alm:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/wifi/WpsFragment;->alm:Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/wifi/ak;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/ak;-><init>(Lcom/android/settings/wifi/WpsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/WpsFragment;->start()V

    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d0286

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
