.class public Lcom/android/settings/wifi/MiuiTetherDeviceSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiTetherDeviceSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private anj:Z

.field private ank:Z

.field private anl:Ljava/util/Set;

.field private anm:Landroid/preference/Preference;

.field private ann:Landroid/content/SharedPreferences;

.field private ano:Landroid/preference/PreferenceCategory;

.field private anp:Landroid/net/ConnectivityManager;

.field private anq:Ljava/util/List;

.field private anr:Lcom/android/settings/wifi/v;

.field private ans:Z

.field private ant:Landroid/content/IntentFilter;

.field private anu:Landroid/preference/ListPreference;

.field private anv:Landroid/net/wifi/WifiManager;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/wifi/aE;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/aE;-><init>(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private aef()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ank:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "connected_devices"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ano:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ano:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    :goto_0
    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ank:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anq:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/D;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ano:Landroid/preference/PreferenceCategory;

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aeh(Lcom/android/settings/wifi/D;)Lmiui/preference/ValuePreference;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ann:Landroid/content/SharedPreferences;

    invoke-virtual {v0}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aek(Lcom/android/settings/wifi/D;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anq:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/D;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aeh(Lcom/android/settings/wifi/D;)Lmiui/preference/ValuePreference;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ano:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ano:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_5
    return-void
.end method

.method private aeg()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ann:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anl:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aej(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private aeh(Lcom/android/settings/wifi/D;)Lmiui/preference/ValuePreference;
    .locals 3

    new-instance v0, Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/preference/ValuePreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/android/settings/wifi/D;->afO()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    return-object v0

    :cond_0
    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private aei()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ank:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dc;->brZ(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anl:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "tetherBlockListPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ann:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aeg()V

    :cond_0
    return-void
.end method

.method private aej(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ann:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private aek(Lcom/android/settings/wifi/D;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ann:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settings/wifi/D;->afO()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method static synthetic ael(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anl:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic aem(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)Landroid/net/ConnectivityManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anp:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic aen(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anq:Ljava/util/List;

    return-object v0
.end method

.method static synthetic aeo(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)Landroid/net/wifi/WifiManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anv:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic aep(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anq:Ljava/util/List;

    return-object p1
.end method

.method static synthetic aeq(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ans:Z

    return p1
.end method

.method static synthetic aer(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aef()V

    return-void
.end method

.method static synthetic aes(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Lcom/android/settings/wifi/D;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aek(Lcom/android/settings/wifi/D;)V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150088

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "connected_devices"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ano:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "block_list"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anm:Landroid/preference/Preference;

    const-string/jumbo v0, "max_number"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/16 v3, 0x7d7

    invoke-virtual {v1, v2, v3}, Lcom/android/settings/dc;->bsa(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anp:Landroid/net/ConnectivityManager;

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anv:Landroid/net/wifi/WifiManager;

    const-string/jumbo v0, "qcom"

    const-string/jumbo v1, "vendor"

    invoke-static {v1}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ank:Z

    const-string/jumbo v0, "mediatek"

    const-string/jumbo v1, "vendor"

    invoke-static {v1}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anj:Z

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aei()V

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dc;->bsc(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anq:Ljava/util/List;

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aef()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ant:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ant:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ant:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ant:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ant:Landroid/content/IntentFilter;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/dc;->bse()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_1

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/dc;->bsj(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anv:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anp:Landroid/net/ConnectivityManager;

    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->stopTethering(I)V

    new-instance v1, Lcom/android/settings/wifi/u;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/wifi/u;-><init>(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Landroid/content/Context;)V

    new-array v0, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/android/settings/wifi/u;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    return v3
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ank:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anm:Landroid/preference/Preference;

    if-ne p2, v0, :cond_1

    const-class v0, Lcom/android/settings/wifi/MiuiTetherBlockList;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const v5, 0x7f12027f

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anu:Landroid/preference/ListPreference;

    if-eq p2, v0, :cond_0

    invoke-virtual {p2}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/android/settings/wifi/D;

    const-string/jumbo v1, ""

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/wifi/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    new-instance v1, Lcom/android/settings/wifi/v;

    invoke-direct {v1, p0, v0, v4}, Lcom/android/settings/wifi/v;-><init>(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Lcom/android/settings/wifi/D;Lcom/android/settings/wifi/v;)V

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anr:Lcom/android/settings/wifi/v;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anr:Lcom/android/settings/wifi/v;

    invoke-virtual {v0}, Lcom/android/settings/wifi/v;->show()V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/settings/wifi/D;

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/wifi/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aei()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "saved_bundle"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const-string/jumbo v2, "show_dialog"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ans:Z

    iget-boolean v2, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ans:Z

    if-eqz v2, :cond_0

    new-instance v2, Lcom/android/settings/wifi/v;

    new-instance v3, Lcom/android/settings/wifi/D;

    const-string/jumbo v4, "save_device_name"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "save_device_mac"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/android/settings/wifi/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, p0, v3, v1}, Lcom/android/settings/wifi/v;-><init>(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Lcom/android/settings/wifi/D;Lcom/android/settings/wifi/v;)V

    iput-object v2, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anr:Lcom/android/settings/wifi/v;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anr:Lcom/android/settings/wifi/v;

    invoke-virtual {v0}, Lcom/android/settings/wifi/v;->show()V

    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anr:Lcom/android/settings/wifi/v;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "show_dialog"

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ans:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "save_device_name"

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anr:Lcom/android/settings/wifi/v;

    invoke-virtual {v1}, Lcom/android/settings/wifi/v;->aet()Lcom/android/settings/wifi/D;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/wifi/D;->afO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "save_device_mac"

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->anr:Lcom/android/settings/wifi/v;

    invoke-virtual {v1}, Lcom/android/settings/wifi/v;->aet()Lcom/android/settings/wifi/D;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "saved_bundle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStart()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->ant:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0d0242

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStop()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
