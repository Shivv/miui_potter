.class final Lcom/android/settings/wifi/details/c;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "WifiDetailPreferenceController.java"


# instance fields
.field final synthetic akq:Lcom/android/settings/wifi/details/a;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/details/a;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method

.method private aao(Landroid/net/NetworkCapabilities;I)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v1}, Lcom/android/settings/wifi/details/a;->aah(Lcom/android/settings/wifi/details/a;)Landroid/net/NetworkCapabilities;

    move-result-object v1

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v1}, Lcom/android/settings/wifi/details/a;->aah(Lcom/android/settings/wifi/details/a;)Landroid/net/NetworkCapabilities;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v1

    invoke-virtual {p1, p2}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v2

    if-eq v1, v2, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCapabilitiesChanged(Landroid/net/Network;Landroid/net/NetworkCapabilities;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->aag(Lcom/android/settings/wifi/details/a;)Landroid/net/Network;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->aah(Lcom/android/settings/wifi/details/a;)Landroid/net/NetworkCapabilities;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/NetworkCapabilities;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    const/16 v0, 0x10

    invoke-direct {p0, p2, v0}, Lcom/android/settings/wifi/details/c;->aao(Landroid/net/NetworkCapabilities;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x11

    invoke-direct {p0, p2, v0}, Lcom/android/settings/wifi/details/c;->aao(Landroid/net/NetworkCapabilities;I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->aal(Lcom/android/settings/wifi/details/a;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0, p2}, Lcom/android/settings/wifi/details/a;->aaj(Lcom/android/settings/wifi/details/a;Landroid/net/NetworkCapabilities;)Landroid/net/NetworkCapabilities;

    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->aan(Lcom/android/settings/wifi/details/a;)V

    :cond_2
    return-void
.end method

.method public onLinkPropertiesChanged(Landroid/net/Network;Landroid/net/LinkProperties;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->aag(Lcom/android/settings/wifi/details/a;)Landroid/net/Network;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->aaf(Lcom/android/settings/wifi/details/a;)Landroid/net/LinkProperties;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/LinkProperties;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0, p2}, Lcom/android/settings/wifi/details/a;->aai(Lcom/android/settings/wifi/details/a;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;

    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->aan(Lcom/android/settings/wifi/details/a;)V

    :cond_0
    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->aag(Lcom/android/settings/wifi/details/a;)Landroid/net/Network;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/details/c;->akq:Lcom/android/settings/wifi/details/a;

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->aak(Lcom/android/settings/wifi/details/a;)V

    :cond_0
    return-void
.end method
