.class public Lcom/android/settings/wifi/details/a;
.super Lcom/android/settings/core/c;
.source "WifiDetailPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/b;


# static fields
.field static final KEY_BUTTONS_PREF:Ljava/lang/String; = "buttons"

.field static final KEY_CONNECTION_DETAIL_PREF:Ljava/lang/String; = "connection_detail"

.field static final KEY_DNS_PREF:Ljava/lang/String; = "dns"

.field static final KEY_FREQUENCY_PREF:Ljava/lang/String; = "frequency"

.field static final KEY_GATEWAY_PREF:Ljava/lang/String; = "gateway"

.field static final KEY_IPV6_ADDRESSES_PREF:Ljava/lang/String; = "ipv6_addresses"

.field static final KEY_IPV6_CATEGORY:Ljava/lang/String; = "ipv6_category"

.field static final KEY_IP_ADDRESS_PREF:Ljava/lang/String; = "ip_address"

.field static final KEY_LINK_SPEED:Ljava/lang/String; = "link_speed"

.field static final KEY_MAC_ADDRESS_PREF:Ljava/lang/String; = "mac_address"

.field static final KEY_SECURITY_PREF:Ljava/lang/String; = "security"

.field static final KEY_SIGNAL_STRENGTH_PREF:Ljava/lang/String; = "signal_strength"

.field static final KEY_SUBNET_MASK_PREF:Ljava/lang/String; = "subnet_mask"

.field private static final ajH:Z


# instance fields
.field private ajI:Lcom/android/settingslib/wifi/i;

.field private ajJ:Lcom/android/settings/applications/LayoutPreference;

.field private ajK:Landroid/support/v7/preference/Preference;

.field private final ajL:Landroid/net/ConnectivityManager;

.field private final ajM:Lcom/android/settings/vpn2/ConnectivityManagerWrapper;

.field private ajN:Lcom/android/settings/wifi/WifiDetailPreference;

.field private final ajO:Landroid/content/IntentFilter;

.field private ajP:Landroid/widget/Button;

.field private final ajQ:Landroid/app/Fragment;

.field private ajR:Lcom/android/settings/wifi/WifiDetailPreference;

.field private ajS:Lcom/android/settings/wifi/WifiDetailPreference;

.field private final ajT:Landroid/os/Handler;

.field private ajU:Lcom/android/settings/wifi/WifiDetailPreference;

.field private ajV:Landroid/support/v7/preference/Preference;

.field private ajW:Landroid/support/v7/preference/PreferenceCategory;

.field private ajX:Landroid/net/LinkProperties;

.field private ajY:Lcom/android/settings/wifi/WifiDetailPreference;

.field private ajZ:Lcom/android/settings/wifi/WifiDetailPreference;

.field private aka:Landroid/net/Network;

.field private final akb:Landroid/net/ConnectivityManager$NetworkCallback;

.field private akc:Landroid/net/NetworkCapabilities;

.field private akd:Landroid/net/NetworkInfo;

.field private final ake:Landroid/net/NetworkRequest;

.field private akf:Landroid/content/Context;

.field private akg:I

.field private akh:Lcom/android/settings/wifi/WifiDetailPreference;

.field private aki:Landroid/widget/Button;

.field private akj:[Ljava/lang/String;

.field private akk:Lcom/android/settings/wifi/WifiDetailPreference;

.field private akl:Lcom/android/settings/wifi/WifiDetailPreference;

.field private final akm:Landroid/net/wifi/WifiConfiguration;

.field private akn:Landroid/net/wifi/WifiInfo;

.field private final ako:Landroid/net/wifi/WifiManager;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "WifiDetailsPrefCtrl"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/wifi/details/a;->ajH:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/settingslib/wifi/i;Lcom/android/settings/vpn2/ConnectivityManagerWrapper;Landroid/content/Context;Landroid/app/Fragment;Landroid/os/Handler;Lcom/android/settings/core/lifecycle/a;Landroid/net/wifi/WifiManager;Lcom/android/settings/core/instrumentation/e;)V
    .locals 2

    invoke-direct {p0, p3}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/wifi/details/b;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/details/b;-><init>(Lcom/android/settings/wifi/details/a;)V

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->clearCapabilities()Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ake:Landroid/net/NetworkRequest;

    new-instance v0, Lcom/android/settings/wifi/details/c;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/details/c;-><init>(Lcom/android/settings/wifi/details/a;)V

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akb:Landroid/net/ConnectivityManager$NetworkCallback;

    iput-object p1, p0, Lcom/android/settings/wifi/details/a;->ajI:Lcom/android/settingslib/wifi/i;

    invoke-interface {p2}, Lcom/android/settings/vpn2/ConnectivityManagerWrapper;->Pl()Landroid/net/ConnectivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajL:Landroid/net/ConnectivityManager;

    iput-object p2, p0, Lcom/android/settings/wifi/details/a;->ajM:Lcom/android/settings/vpn2/ConnectivityManagerWrapper;

    iput-object p4, p0, Lcom/android/settings/wifi/details/a;->ajQ:Landroid/app/Fragment;

    iput-object p5, p0, Lcom/android/settings/wifi/details/a;->ajT:Landroid/os/Handler;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akj:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akm:Landroid/net/wifi/WifiConfiguration;

    iput-object p7, p0, Lcom/android/settings/wifi/details/a;->ako:Landroid/net/wifi/WifiManager;

    iput-object p8, p0, Lcom/android/settings/wifi/details/a;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajO:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajO:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajO:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p6, p0}, Lcom/android/settings/core/lifecycle/a;->ajt(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method

.method private ZR()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->isEphemeral()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akm:Landroid/net/wifi/WifiConfiguration;

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ZS()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akc:Landroid/net/NetworkCapabilities;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akc:Landroid/net/NetworkCapabilities;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ZT()V
    .locals 2

    sget-boolean v0, Lcom/android/settings/wifi/details/a;->ajH:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "WifiDetailsPrefCtrl"

    const-string/jumbo v1, "Exiting the WifiNetworkDetailsPage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajQ:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private ZU()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->isEphemeral()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ako:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->disableEphemeralNetwork(Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->ajQ:Landroid/app/Fragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/util/Pair;

    const/16 v3, 0x89

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajQ:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akm:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akm:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->isPasspoint()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ako:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akm:Landroid/net/wifi/WifiConfiguration;

    iget-object v1, v1, Landroid/net/wifi/WifiConfiguration;->FQDN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->removePasspointConfiguration(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ako:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akm:Landroid/net/wifi/WifiConfiguration;

    iget v1, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->forget(ILandroid/net/wifi/WifiManager$ActionListener;)V

    goto :goto_0
.end method

.method private static ZV(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x4

    :try_start_0
    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    invoke-static {v0, p0}, Landroid/net/NetworkUtils;->getNetworkPart(Ljava/net/InetAddress;I)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return-object v0

    :array_0
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data
.end method

.method private ZW()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajI:Lcom/android/settingslib/wifi/i;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akm:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    iget-object v3, p0, Lcom/android/settings/wifi/details/a;->akd:Landroid/net/NetworkInfo;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settingslib/wifi/i;->cin(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiInfo;Landroid/net/NetworkInfo;)Z

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajK:Landroid/support/v7/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->ajI:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chz()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private ZX()V
    .locals 4

    iget v0, p0, Lcom/android/settings/wifi/details/a;->akg:I

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/net/NetworkBadging;->getWifiIcon(IILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->ajK:Landroid/support/v7/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/support/v7/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/details/a;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f060130

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akk:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/wifi/WifiDetailPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajI:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chn()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akk:Lcom/android/settings/wifi/WifiDetailPreference;

    iget-object v2, p0, Lcom/android/settings/wifi/details/a;->akj:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Lcom/android/settings/wifi/WifiDetailPreference;->adu(Ljava/lang/String;)V

    return-void
.end method

.method private ZY()V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajL:Landroid/net/ConnectivityManager;

    iget-object v4, p0, Lcom/android/settings/wifi/details/a;->aka:Landroid/net/Network;

    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(Landroid/net/Network;)Landroid/net/NetworkInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akd:Landroid/net/NetworkInfo;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ako:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->aka:Landroid/net/Network;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akd:Landroid/net/NetworkInfo;

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZT()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/android/settings/wifi/details/a;->ajP:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZR()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZW()V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v0

    iput v0, p0, Lcom/android/settings/wifi/details/a;->akg:I

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZX()V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajZ:Lcom/android/settings/wifi/WifiDetailPreference;

    iget-object v4, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/android/settings/wifi/WifiDetailPreference;->adu(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v0

    iget-object v4, p0, Lcom/android/settings/wifi/details/a;->ajY:Lcom/android/settings/wifi/WifiDetailPreference;

    if-ltz v0, :cond_3

    move v0, v2

    :goto_1
    invoke-virtual {v4, v0}, Lcom/android/settings/wifi/WifiDetailPreference;->setVisible(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajY:Lcom/android/settings/wifi/WifiDetailPreference;

    iget-object v4, p0, Lcom/android/settings/wifi/details/a;->mContext:Landroid/content/Context;

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    const v1, 0x7f120912

    invoke-virtual {v4, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/WifiDetailPreference;->adu(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v0

    const/16 v1, 0x960

    if-lt v0, v1, :cond_4

    const/16 v1, 0x9c4

    if-ge v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1214d9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->ajR:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/wifi/WifiDetailPreference;->adu(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZZ()V

    return-void

    :cond_2
    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    const/16 v1, 0x1324

    if-lt v0, v1, :cond_5

    const/16 v1, 0x170c

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1214da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    const-string/jumbo v1, "WifiDetailsPrefCtrl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unexpected frequency "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    goto :goto_2
.end method

.method private ZZ()V
    .locals 9

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/settings/wifi/details/a;->aki:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZS()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/settings/wifi/details/a;->ajJ:Lcom/android/settings/applications/LayoutPreference;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajP:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->aki:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Lcom/android/settings/applications/LayoutPreference;->setVisible(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->aka:Landroid/net/Network;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajX:Landroid/net/LinkProperties;

    if-nez v0, :cond_4

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajU:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/WifiDetailPreference;->setVisible(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akl:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/WifiDetailPreference;->setVisible(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajS:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/WifiDetailPreference;->setVisible(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajN:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/WifiDetailPreference;->setVisible(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajW:Landroid/support/v7/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceCategory;->setVisible(Z)V

    return-void

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    new-instance v6, Ljava/util/StringJoiner;

    const-string/jumbo v0, "\n"

    invoke-direct {v6, v0}, Ljava/util/StringJoiner;-><init>(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajX:Landroid/net/LinkProperties;

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v3, v4

    move-object v5, v4

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/LinkAddress;

    invoke-virtual {v0}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v8

    instance-of v8, v8, Ljava/net/Inet4Address;

    if-eqz v8, :cond_5

    invoke-virtual {v0}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Landroid/net/LinkAddress;->getPrefixLength()I

    move-result v0

    invoke-static {v0}, Lcom/android/settings/wifi/details/a;->ZV(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    move-object v3, v5

    :goto_3
    move-object v5, v3

    move-object v3, v0

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v8

    instance-of v8, v8, Ljava/net/Inet6Address;

    if-eqz v8, :cond_a

    invoke-virtual {v0}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/StringJoiner;->add(Ljava/lang/CharSequence;)Ljava/util/StringJoiner;

    move-object v0, v3

    move-object v3, v5

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajX:Landroid/net/LinkProperties;

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/RouteInfo;

    invoke-virtual {v0}, Landroid/net/RouteInfo;->isIPv4Default()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-virtual {v0}, Landroid/net/RouteInfo;->hasGateway()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-virtual {v0}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v4

    :cond_8
    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajX:Landroid/net/LinkProperties;

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getDnsServers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    const-class v7, Ljava/net/Inet4Address;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v8, Lcom/android/settings/wifi/details/-$Lambda$HNKNav2vO_bYqtrii9xLx4CTlEw$2;

    invoke-direct {v8, v7}, Lcom/android/settings/wifi/details/-$Lambda$HNKNav2vO_bYqtrii9xLx4CTlEw$2;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v8}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    sget-object v7, Lcom/android/settings/wifi/details/-$Lambda$HNKNav2vO_bYqtrii9xLx4CTlEw;->$INST$0:Lcom/android/settings/wifi/details/-$Lambda$HNKNav2vO_bYqtrii9xLx4CTlEw;

    invoke-interface {v0, v7}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v0

    const-string/jumbo v7, ","

    invoke-static {v7}, Ljava/util/stream/Collectors;->joining(Ljava/lang/CharSequence;)Ljava/util/stream/Collector;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v7, p0, Lcom/android/settings/wifi/details/a;->ajU:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-direct {p0, v7, v5}, Lcom/android/settings/wifi/details/a;->aaa(Lcom/android/settings/wifi/WifiDetailPreference;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/settings/wifi/details/a;->akl:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-direct {p0, v5, v3}, Lcom/android/settings/wifi/details/a;->aaa(Lcom/android/settings/wifi/WifiDetailPreference;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/wifi/details/a;->ajS:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-direct {p0, v3, v4}, Lcom/android/settings/wifi/details/a;->aaa(Lcom/android/settings/wifi/WifiDetailPreference;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/wifi/details/a;->ajN:Lcom/android/settings/wifi/WifiDetailPreference;

    invoke-direct {p0, v3, v0}, Lcom/android/settings/wifi/details/a;->aaa(Lcom/android/settings/wifi/WifiDetailPreference;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/util/StringJoiner;->length()I

    move-result v0

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajV:Landroid/support/v7/preference/Preference;

    invoke-virtual {v6}, Ljava/util/StringJoiner;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajW:Landroid/support/v7/preference/PreferenceCategory;

    invoke-virtual {v0, v2}, Landroid/support/v7/preference/PreferenceCategory;->setVisible(Z)V

    :goto_4
    return-void

    :cond_9
    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajW:Landroid/support/v7/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceCategory;->setVisible(Z)V

    goto :goto_4

    :cond_a
    move-object v0, v3

    move-object v3, v5

    goto/16 :goto_3
.end method

.method private aaa(Lcom/android/settings/wifi/WifiDetailPreference;Ljava/lang/String;)V
    .locals 1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/android/settings/wifi/WifiDetailPreference;->adu(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/settings/wifi/WifiDetailPreference;->setVisible(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/settings/wifi/WifiDetailPreference;->setVisible(Z)V

    goto :goto_0
.end method

.method static synthetic aad(Ljava/lang/Class;Ljava/net/InetAddress;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic aae(Ljava/net/InetAddress;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic aaf(Lcom/android/settings/wifi/details/a;)Landroid/net/LinkProperties;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajX:Landroid/net/LinkProperties;

    return-object v0
.end method

.method static synthetic aag(Lcom/android/settings/wifi/details/a;)Landroid/net/Network;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->aka:Landroid/net/Network;

    return-object v0
.end method

.method static synthetic aah(Lcom/android/settings/wifi/details/a;)Landroid/net/NetworkCapabilities;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akc:Landroid/net/NetworkCapabilities;

    return-object v0
.end method

.method static synthetic aai(Lcom/android/settings/wifi/details/a;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/details/a;->ajX:Landroid/net/LinkProperties;

    return-object p1
.end method

.method static synthetic aaj(Lcom/android/settings/wifi/details/a;Landroid/net/NetworkCapabilities;)Landroid/net/NetworkCapabilities;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/details/a;->akc:Landroid/net/NetworkCapabilities;

    return-object p1
.end method

.method static synthetic aak(Lcom/android/settings/wifi/details/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZT()V

    return-void
.end method

.method static synthetic aal(Lcom/android/settings/wifi/details/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZW()V

    return-void
.end method

.method static synthetic aam(Lcom/android/settings/wifi/details/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZY()V

    return-void
.end method

.method static synthetic aan(Lcom/android/settings/wifi/details/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZZ()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/core/c;->a(Landroid/support/v7/preference/PreferenceScreen;)V

    invoke-virtual {p1}, Landroid/support/v7/preference/PreferenceScreen;->dky()Landroid/support/v7/preference/k;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akf:Landroid/content/Context;

    const-string/jumbo v0, "connection_detail"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajK:Landroid/support/v7/preference/Preference;

    const-string/jumbo v0, "buttons"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/LayoutPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajJ:Lcom/android/settings/applications/LayoutPreference;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajJ:Lcom/android/settings/applications/LayoutPreference;

    const v1, 0x7f0a0405

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/LayoutPreference;->oh(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->aki:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->aki:Landroid/widget/Button;

    const v1, 0x7f121204

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->aki:Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/wifi/details/-$Lambda$HNKNav2vO_bYqtrii9xLx4CTlEw$1;

    invoke-direct {v1, v2, p0}, Lcom/android/settings/wifi/details/-$Lambda$HNKNav2vO_bYqtrii9xLx4CTlEw$1;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string/jumbo v0, "signal_strength"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WifiDetailPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akk:Lcom/android/settings/wifi/WifiDetailPreference;

    const-string/jumbo v0, "link_speed"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WifiDetailPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajY:Lcom/android/settings/wifi/WifiDetailPreference;

    const-string/jumbo v0, "frequency"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WifiDetailPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajR:Lcom/android/settings/wifi/WifiDetailPreference;

    const-string/jumbo v0, "security"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WifiDetailPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akh:Lcom/android/settings/wifi/WifiDetailPreference;

    const-string/jumbo v0, "mac_address"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WifiDetailPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajZ:Lcom/android/settings/wifi/WifiDetailPreference;

    const-string/jumbo v0, "ip_address"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WifiDetailPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajU:Lcom/android/settings/wifi/WifiDetailPreference;

    const-string/jumbo v0, "gateway"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WifiDetailPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajS:Lcom/android/settings/wifi/WifiDetailPreference;

    const-string/jumbo v0, "subnet_mask"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WifiDetailPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akl:Lcom/android/settings/wifi/WifiDetailPreference;

    const-string/jumbo v0, "dns"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/WifiDetailPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajN:Lcom/android/settings/wifi/WifiDetailPreference;

    const-string/jumbo v0, "ipv6_category"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajW:Landroid/support/v7/preference/PreferenceCategory;

    const-string/jumbo v0, "ipv6_addresses"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajV:Landroid/support/v7/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->akh:Lcom/android/settings/wifi/WifiDetailPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->ajI:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1, v2}, Lcom/android/settingslib/wifi/i;->chV(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/WifiDetailPreference;->adu(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajJ:Lcom/android/settings/applications/LayoutPreference;

    const v1, 0x7f0a01ac

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/LayoutPreference;->oh(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajP:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajP:Landroid/widget/Button;

    const v1, 0x7f120785

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajP:Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/wifi/details/-$Lambda$HNKNav2vO_bYqtrii9xLx4CTlEw$1;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p0}, Lcom/android/settings/wifi/details/-$Lambda$HNKNav2vO_bYqtrii9xLx4CTlEw$1;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method synthetic aab(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajM:Lcom/android/settings/vpn2/ConnectivityManagerWrapper;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->aka:Landroid/net/Network;

    invoke-interface {v0, v1}, Lcom/android/settings/vpn2/ConnectivityManagerWrapper;->Pn(Landroid/net/Network;)V

    return-void
.end method

.method synthetic aac(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZU()V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->aka:Landroid/net/Network;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajX:Landroid/net/LinkProperties;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akc:Landroid/net/NetworkCapabilities;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akd:Landroid/net/NetworkInfo;

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akn:Landroid/net/wifi/WifiInfo;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajL:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->akb:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ako:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->aka:Landroid/net/Network;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajL:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->aka:Landroid/net/Network;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->ajX:Landroid/net/LinkProperties;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajL:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->aka:Landroid/net/Network;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/details/a;->akc:Landroid/net/NetworkCapabilities;

    invoke-direct {p0}, Lcom/android/settings/wifi/details/a;->ZY()V

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/details/a;->ajO:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/wifi/details/a;->ajM:Lcom/android/settings/vpn2/ConnectivityManagerWrapper;

    iget-object v1, p0, Lcom/android/settings/wifi/details/a;->ake:Landroid/net/NetworkRequest;

    iget-object v2, p0, Lcom/android/settings/wifi/details/a;->akb:Landroid/net/ConnectivityManager$NetworkCallback;

    iget-object v3, p0, Lcom/android/settings/wifi/details/a;->ajT:Landroid/os/Handler;

    invoke-interface {v0, v1, v2, v3}, Lcom/android/settings/vpn2/ConnectivityManagerWrapper;->Pm(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;Landroid/os/Handler;)V

    return-void
.end method
