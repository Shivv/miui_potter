.class Lcom/android/settings/wifi/aa;
.super Landroid/os/Handler;
.source "MiuiWifiService.java"


# instance fields
.field final synthetic asR:Lcom/android/settings/wifi/MiuiWifiService;


# direct methods
.method public constructor <init>(Lcom/android/settings/wifi/MiuiWifiService;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/aa;->asR:Lcom/android/settings/wifi/MiuiWifiService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/android/settings/wifi/aa;->asR:Lcom/android/settings/wifi/MiuiWifiService;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lcom/android/settings/wifi/MiuiWifiService;->aiP(Lcom/android/settings/wifi/MiuiWifiService;Ljava/util/List;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/settings/wifi/aa;->asR:Lcom/android/settings/wifi/MiuiWifiService;

    invoke-static {v2}, Lcom/android/settings/wifi/k;->getInstance(Landroid/content/Context;)Lcom/android/settings/wifi/k;

    move-result-object v2

    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/settings/wifi/k;->abX(Ljava/lang/String;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/wifi/aa;->asR:Lcom/android/settings/wifi/MiuiWifiService;

    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v1}, Lcom/android/settings/wifi/k;->abV(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_1
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/aa;->asR:Lcom/android/settings/wifi/MiuiWifiService;

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/android/settings/wifi/k;->abZ(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/aa;->asR:Lcom/android/settings/wifi/MiuiWifiService;

    invoke-static {v0}, Lcom/android/settings/wifi/MiuiWifiService;->aiN(Lcom/android/settings/wifi/MiuiWifiService;)Lcom/android/settings/wifi/a;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v1, v0}, Lcom/android/settings/wifi/a;->aat(Landroid/net/wifi/WifiConfiguration;)V

    goto :goto_0

    :pswitch_3
    iget-object v3, p0, Lcom/android/settings/wifi/aa;->asR:Lcom/android/settings/wifi/MiuiWifiService;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v4, p1, Landroid/os/Message;->arg1:I

    if-ne v4, v1, :cond_3

    :goto_1
    invoke-static {v3, v0, v1}, Lcom/android/settings/wifi/MiuiWifiService;->aiO(Lcom/android/settings/wifi/MiuiWifiService;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/wifi/aa;->asR:Lcom/android/settings/wifi/MiuiWifiService;

    invoke-virtual {v0}, Lcom/android/settings/wifi/MiuiWifiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lmiui/provider/Wifi$SyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lmiui/provider/Wifi;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "DELETED_SYNCED_DATA"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "DELETED_SYNCED_DATA"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
