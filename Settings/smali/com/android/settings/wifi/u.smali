.class Lcom/android/settings/wifi/u;
.super Landroid/os/AsyncTask;
.source "MiuiTetherDeviceSettings.java"


# instance fields
.field private anw:Landroid/content/Context;

.field final synthetic anx:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/u;->anx:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/android/settings/wifi/u;->anw:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/wifi/u;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/wifi/u;->anx:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    invoke-static {v2}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aeo(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)Landroid/net/wifi/WifiManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v2

    const/16 v3, 0xb

    if-eq v2, v3, :cond_0

    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/u;->anx:Lcom/android/settings/wifi/MiuiTetherDeviceSettings;

    invoke-static {v0}, Lcom/android/settings/wifi/MiuiTetherDeviceSettings;->aem(Lcom/android/settings/wifi/MiuiTetherDeviceSettings;)Landroid/net/ConnectivityManager;

    move-result-object v0

    new-instance v2, Lcom/android/settings/wifi/aD;

    invoke-direct {v2, p0}, Lcom/android/settings/wifi/aD;-><init>(Lcom/android/settings/wifi/u;)V

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/net/ConnectivityManager;->startTethering(IZLandroid/net/ConnectivityManager$OnStartTetheringCallback;)V

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method
