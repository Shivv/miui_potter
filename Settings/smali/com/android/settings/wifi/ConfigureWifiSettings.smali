.class public Lcom/android/settings/wifi/ConfigureWifiSettings;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "ConfigureWifiSettings.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private apq:Lcom/android/settings/wifi/W;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/wifi/aL;

    invoke-direct {v0}, Lcom/android/settings/wifi/aL;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/ConfigureWifiSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "ConfigureWifiSettings"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f15010f

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x152

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 5

    new-instance v1, Lcom/android/settings/network/a;

    const-class v0, Landroid/net/NetworkScoreManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkScoreManager;

    invoke-direct {v1, v0}, Lcom/android/settings/network/a;-><init>(Landroid/net/NetworkScoreManager;)V

    new-instance v0, Lcom/android/settings/wifi/W;

    invoke-virtual {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v2

    invoke-direct {v0, p1, p0, v1, v2}, Lcom/android/settings/wifi/W;-><init>(Landroid/content/Context;Landroid/app/Fragment;Lcom/android/settings/network/a;Lcom/android/settings/core/lifecycle/c;)V

    iput-object v0, p0, Lcom/android/settings/wifi/ConfigureWifiSettings;->apq:Lcom/android/settings/wifi/W;

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/android/settings/wifi/d;

    invoke-virtual {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Lcom/android/settings/wifi/d;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/android/settings/network/c;

    invoke-direct {v3, p1, v1}, Lcom/android/settings/network/c;-><init>(Landroid/content/Context;Lcom/android/settings/network/a;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/wifi/b;

    invoke-virtual {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v3

    invoke-direct {v1, p1, v3}, Lcom/android/settings/wifi/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/wifi/ConfigureWifiSettings;->apq:Lcom/android/settings/wifi/W;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/wifi/x;

    invoke-direct {v1, p1, v0}, Lcom/android/settings/wifi/x;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/wifi/ad;

    invoke-virtual {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v3

    invoke-direct {v1, p1, v3, v0}, Lcom/android/settings/wifi/ad;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Landroid/net/wifi/WifiManager;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/wifi/V;

    invoke-direct {v1, p1}, Lcom/android/settings/wifi/V;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/wifi/p2p/b;

    invoke-virtual {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v3

    invoke-direct {v1, p1, v3, v0}, Lcom/android/settings/wifi/p2p/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Landroid/net/wifi/WifiManager;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/network/n;

    invoke-direct {v1, p1}, Lcom/android/settings/network/n;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/wifi/q;

    invoke-virtual {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-direct {v1, p1, v3, v0, v4}, Lcom/android/settings/wifi/q;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Landroid/net/wifi/WifiManager;Landroid/app/FragmentManager;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v2
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/ConfigureWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/ConfigureWifiSettings;->apq:Lcom/android/settings/wifi/W;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/ConfigureWifiSettings;->apq:Lcom/android/settings/wifi/W;

    invoke-virtual {v0, p1, p2}, Lcom/android/settings/wifi/W;->aig(II)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onActivityResult(IILandroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onAttach(Landroid/content/Context;)V

    return-void
.end method
