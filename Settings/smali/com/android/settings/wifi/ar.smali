.class final Lcom/android/settings/wifi/ar;
.super Landroid/content/BroadcastReceiver;
.source "WifiApEnabler.java"


# instance fields
.field final synthetic atE:Lcom/android/settings/wifi/m;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/m;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/ar;->atE:Lcom/android/settings/wifi/m;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/16 v3, 0xe

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v0, "wifi_state"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_1

    const-string/jumbo v1, "wifi_ap_error_code"

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/wifi/ar;->atE:Lcom/android/settings/wifi/m;

    invoke-static {v2, v0, v1}, Lcom/android/settings/wifi/m;->acw(Lcom/android/settings/wifi/m;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/wifi/ar;->atE:Lcom/android/settings/wifi/m;

    invoke-static {v1, v0, v2}, Lcom/android/settings/wifi/m;->acw(Lcom/android/settings/wifi/m;II)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v0, "availableArray"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const-string/jumbo v1, "tetherArray"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string/jumbo v2, "erroredArray"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/wifi/ar;->atE:Lcom/android/settings/wifi/m;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v3, v0, v1, v2}, Lcom/android/settings/wifi/m;->acx(Lcom/android/settings/wifi/m;[Ljava/lang/Object;[Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/ar;->atE:Lcom/android/settings/wifi/m;

    invoke-static {v0}, Lcom/android/settings/wifi/m;->acv(Lcom/android/settings/wifi/m;)V

    goto :goto_0
.end method
