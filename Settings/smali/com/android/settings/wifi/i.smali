.class public Lcom/android/settings/wifi/i;
.super Ljava/lang/Object;
.source "MiuiWifiEnabler.java"


# instance fields
.field private alC:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private alD:Lcom/android/settings/MiuiSettingsPreferenceFragment;

.field private final alE:Landroid/content/IntentFilter;

.field private alF:Landroid/preference/CheckBoxPreference;

.field private alG:Z

.field private final alH:Landroid/net/wifi/WifiManager;

.field private final mContext:Landroid/content/Context;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Lcom/android/settings/MiuiSettingsPreferenceFragment;Landroid/preference/CheckBoxPreference;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/settings/wifi/i;->alC:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Lcom/android/settings/wifi/ap;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/ap;-><init>(Lcom/android/settings/wifi/i;)V

    iput-object v0, p0, Lcom/android/settings/wifi/i;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settings/wifi/i;->alD:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/i;->alH:Landroid/net/wifi/WifiManager;

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/wifi/i;->alE:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alE:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alE:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/android/settings/wifi/i;->abO(Landroid/preference/CheckBoxPreference;)V

    return-void
.end method

.method private abL(Landroid/net/NetworkInfo$DetailedState;)V
    .locals 0

    return-void
.end method

.method private abM(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, v1}, Lcom/android/settings/wifi/i;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/i;->alD:Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->invalidateOptionsMenu()V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v2}, Lcom/android/settings/wifi/i;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v1}, Lcom/android/settings/wifi/i;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic abP(Lcom/android/settings/wifi/i;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alC:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic abQ(Lcom/android/settings/wifi/i;Landroid/net/NetworkInfo$DetailedState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/i;->abL(Landroid/net/NetworkInfo$DetailedState;)V

    return-void
.end method

.method static synthetic abR(Lcom/android/settings/wifi/i;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/i;->abM(I)V

    return-void
.end method

.method private setChecked(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/i;->alG:Z

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/wifi/i;->alG:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public abK(Z)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/android/settings/wifi/i;->alG:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-static {v0, v1}, Lcom/android/settingslib/G;->csg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    const v1, 0x7f121549

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/i;->alH:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v0

    if-eqz p1, :cond_3

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/android/settings/dc;->bsf(Landroid/content/Context;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->stopTethering(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/i;->alH:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    const v1, 0x7f121537

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_4
    :goto_0
    return-void

    :cond_5
    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/cF;->bTK(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0
.end method

.method public abN()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/i;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/i;->alE:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public abO(Landroid/preference/CheckBoxPreference;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/wifi/i;->alH:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v3

    const/4 v2, 0x3

    if-ne v3, v2, :cond_2

    move v2, v1

    :goto_0
    if-ne v3, v1, :cond_0

    move v0, v1

    :cond_0
    iget-object v3, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v3, p0, Lcom/android/settings/wifi/i;->alF:Landroid/preference/CheckBoxPreference;

    if-nez v2, :cond_1

    move v1, v0

    :cond_1
    invoke-virtual {v3, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    return-void

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/i;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/i;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
