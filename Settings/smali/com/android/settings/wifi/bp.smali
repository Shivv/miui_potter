.class final Lcom/android/settings/wifi/bp;
.super Ljava/lang/Object;
.source "MiuiAccessPointPreference.java"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ajo(Lcom/android/settings/wifi/AccessPointPreference;Lcom/android/settings/wifi/AccessPointPreference;)I
    .locals 3

    instance-of v0, p1, Lcom/android/settings/wifi/AccessPointPreference;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    instance-of v0, p2, Lcom/android/settings/wifi/AccessPointPreference;

    if-nez v0, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/android/settings/wifi/AccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    iget-object v1, v0, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/android/settings/wifi/AccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    iget-object v2, v0, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    :cond_2
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/wifi/AccessPointPreference;

    check-cast p2, Lcom/android/settings/wifi/AccessPointPreference;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/wifi/bp;->ajo(Lcom/android/settings/wifi/AccessPointPreference;Lcom/android/settings/wifi/AccessPointPreference;)I

    move-result v0

    return v0
.end method
