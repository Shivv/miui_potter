.class Lcom/android/settings/wifi/P;
.super Ljava/lang/Object;
.source "WifiSettings.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;
.implements Lcom/android/settings/widget/x;


# instance fields
.field private final arn:Lcom/android/settings/dashboard/C;

.field private final mContext:Landroid/content/Context;

.field mSummaryHelper:Lcom/android/settings/wifi/U;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/wifi/P;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/wifi/P;->arn:Lcom/android/settings/dashboard/C;

    new-instance v0, Lcom/android/settings/wifi/U;

    iget-object v1, p0, Lcom/android/settings/wifi/P;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/android/settings/wifi/U;-><init>(Landroid/content/Context;Lcom/android/settings/widget/x;)V

    iput-object v0, p0, Lcom/android/settings/wifi/P;->mSummaryHelper:Lcom/android/settings/wifi/U;

    return-void
.end method


# virtual methods
.method public ahr(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/P;->arn:Lcom/android/settings/dashboard/C;

    invoke-virtual {v0, p0, p1}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public jt(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/P;->mSummaryHelper:Lcom/android/settings/wifi/U;

    invoke-virtual {v0, p1}, Lcom/android/settings/wifi/U;->aib(Z)V

    return-void
.end method
