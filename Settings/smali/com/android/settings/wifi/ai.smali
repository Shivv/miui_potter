.class final Lcom/android/settings/wifi/ai;
.super Ljava/lang/Object;
.source "MiuiWifiSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field final synthetic atp:Lcom/android/settings/wifi/MiuiWifiSettings;

.field final synthetic atq:Landroid/view/WindowManager$LayoutParams;

.field final synthetic atr:F

.field final synthetic ats:Landroid/view/Window;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/MiuiWifiSettings;Landroid/view/WindowManager$LayoutParams;FLandroid/view/Window;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/ai;->atp:Lcom/android/settings/wifi/MiuiWifiSettings;

    iput-object p2, p0, Lcom/android/settings/wifi/ai;->atq:Landroid/view/WindowManager$LayoutParams;

    iput p3, p0, Lcom/android/settings/wifi/ai;->atr:F

    iput-object p4, p0, Lcom/android/settings/wifi/ai;->ats:Landroid/view/Window;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/ai;->atp:Lcom/android/settings/wifi/MiuiWifiSettings;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->abn(Lcom/android/settings/wifi/MiuiWifiSettings;Z)Z

    iget-object v0, p0, Lcom/android/settings/wifi/ai;->atp:Lcom/android/settings/wifi/MiuiWifiSettings;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->abl(Lcom/android/settings/wifi/MiuiWifiSettings;Lcom/android/settingslib/wifi/i;)Lcom/android/settingslib/wifi/i;

    iget-object v0, p0, Lcom/android/settings/wifi/ai;->atq:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/android/settings/wifi/ai;->atr:F

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    iget-object v0, p0, Lcom/android/settings/wifi/ai;->ats:Landroid/view/Window;

    iget-object v1, p0, Lcom/android/settings/wifi/ai;->atq:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v0, p0, Lcom/android/settings/wifi/ai;->ats:Landroid/view/Window;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    return-void
.end method
