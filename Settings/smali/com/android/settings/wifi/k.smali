.class public Lcom/android/settings/wifi/k;
.super Ljava/lang/Object;
.source "AutoConnectUtils.java"


# static fields
.field private static alO:Lcom/android/settings/wifi/k;


# instance fields
.field private alM:Ljava/lang/String;

.field private alN:Ljava/util/HashSet;

.field private mLock:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/k;->mLock:Ljava/lang/Object;

    const-string/jumbo v0, "open_wifi_auto_connect_ssid_list"

    iput-object v0, p0, Lcom/android/settings/wifi/k;->alM:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/k;->abY(Landroid/content/Context;)V

    return-void
.end method

.method private abW(Landroid/content/Context;)Ljava/util/Set;
    .locals 4

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/android/settings/wifi/k;->alM:Ljava/lang/String;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private abY(Landroid/content/Context;)V
    .locals 4

    new-instance v0, Lcom/android/settings/wifi/aq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/android/settings/wifi/aq;-><init>(Lcom/android/settings/wifi/k;Landroid/os/Handler;Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "disable_wifi_auto_connect_ssid"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-static {p1}, Landroid/provider/MiuiSettings$System;->getDisableWifiAutoConnectSsid(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/k;->alN:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic aca(Lcom/android/settings/wifi/k;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/k;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic acb(Lcom/android/settings/wifi/k;Ljava/util/HashSet;)Ljava/util/HashSet;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/k;->alN:Ljava/util/HashSet;

    return-object p1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/settings/wifi/k;
    .locals 2

    sget-object v0, Lcom/android/settings/wifi/k;->alO:Lcom/android/settings/wifi/k;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/wifi/k;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/wifi/k;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/wifi/k;->alO:Lcom/android/settings/wifi/k;

    :cond_0
    sget-object v0, Lcom/android/settings/wifi/k;->alO:Lcom/android/settings/wifi/k;

    return-object v0
.end method


# virtual methods
.method public abV(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/settings/wifi/k;->mLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p3, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/wifi/k;->alN:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/k;->alN:Ljava/util/HashSet;

    invoke-static {p1, v0}, Landroid/provider/MiuiSettings$System;->setDisableWifiAutoConnectSsid(Landroid/content/Context;Ljava/util/HashSet;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/settings/wifi/k;->alN:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public abX(Ljava/lang/String;)Z
    .locals 2

    iget-object v1, p0, Lcom/android/settings/wifi/k;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/wifi/k;->alN:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public abZ(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/k;->abW(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/k;->alM:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return-void
.end method
