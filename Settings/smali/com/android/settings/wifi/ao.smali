.class final Lcom/android/settings/wifi/ao;
.super Ljava/lang/Object;
.source "WpsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final synthetic atB:[I


# instance fields
.field final synthetic atA:Ljava/lang/String;

.field final synthetic aty:Lcom/android/settings/wifi/WpsFragment;

.field final synthetic atz:Lcom/android/settings/wifi/WpsFragment$State;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/WpsFragment;Lcom/android/settings/wifi/WpsFragment$State;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    iput-object p2, p0, Lcom/android/settings/wifi/ao;->atz:Lcom/android/settings/wifi/WpsFragment$State;

    iput-object p3, p0, Lcom/android/settings/wifi/ao;->atA:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static synthetic aji()[I
    .locals 3

    sget-object v0, Lcom/android/settings/wifi/ao;->atB:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/wifi/ao;->atB:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/settings/wifi/WpsFragment$State;->values()[Lcom/android/settings/wifi/WpsFragment$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alw:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsFragment$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alx:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsFragment$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->aly:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsFragment$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alz:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsFragment$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/settings/wifi/WpsFragment$State;->alA:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsFragment$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    sput-object v0, Lcom/android/settings/wifi/ao;->atB:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/16 v4, 0x8

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-virtual {v0}, Lcom/android/settings/wifi/WpsFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/settings/wifi/ao;->aji()[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/ao;->atz:Lcom/android/settings/wifi/WpsFragment$State;

    invoke-virtual {v1}, Lcom/android/settings/wifi/WpsFragment$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abw(Lcom/android/settings/wifi/WpsFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abA(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    const v2, 0x7f12164e    # 1.941831E38f

    invoke-virtual {v1, v2}, Lcom/android/settings/wifi/WpsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v3}, Lcom/android/settings/wifi/WpsFragment;->abw(Lcom/android/settings/wifi/WpsFragment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abC(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abA(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f12164d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abC(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abB(Lcom/android/settings/wifi/WpsFragment;)Lmiui/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmiui/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abC(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abx(Lcom/android/settings/wifi/WpsFragment;)Lmiui/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v5}, Lmiui/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abA(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/ao;->atA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abz(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abB(Lcom/android/settings/wifi/WpsFragment;)Lmiui/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmiui/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abx(Lcom/android/settings/wifi/WpsFragment;)Lmiui/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmiui/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abC(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abu(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f1205f7

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abA(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/ao;->atA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abz(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abB(Lcom/android/settings/wifi/WpsFragment;)Lmiui/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmiui/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abx(Lcom/android/settings/wifi/WpsFragment;)Lmiui/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Lmiui/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abC(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abu(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f1203c7

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->abA(Lcom/android/settings/wifi/WpsFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/ao;->atA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0}, Lcom/android/settings/wifi/WpsFragment;->aby(Lcom/android/settings/wifi/WpsFragment;)Landroid/content/BroadcastReceiver;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-virtual {v0}, Lcom/android/settings/wifi/WpsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v1}, Lcom/android/settings/wifi/WpsFragment;->aby(Lcom/android/settings/wifi/WpsFragment;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/wifi/ao;->aty:Lcom/android/settings/wifi/WpsFragment;

    invoke-static {v0, v2}, Lcom/android/settings/wifi/WpsFragment;->abE(Lcom/android/settings/wifi/WpsFragment;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
