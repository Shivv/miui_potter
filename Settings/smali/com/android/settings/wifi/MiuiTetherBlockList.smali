.class public Lcom/android/settings/wifi/MiuiTetherBlockList;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiTetherBlockList.java"


# instance fields
.field private ast:Ljava/util/Set;

.field private asu:Landroid/content/SharedPreferences;

.field private asv:Lcom/android/settings/wifi/Y;

.field private asw:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private ain()V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->ast:Ljava/util/Set;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asu:Landroid/content/SharedPreferences;

    invoke-interface {v2, v0, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const-string/jumbo v3, "MiuiTetherBlockList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "something wrong, no device name, mac = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v3, Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lmiui/preference/ValuePreference;-><init>(Landroid/content/Context;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Lmiui/preference/ValuePreference;->setTitle(Ljava/lang/CharSequence;)V

    :goto_1
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v3, v2}, Lmiui/preference/ValuePreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v0}, Lmiui/preference/ValuePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private aio()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asu:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->ast:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->aip(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private aip(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asu:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method static synthetic aiq(Lcom/android/settings/wifi/MiuiTetherBlockList;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->ast:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic air(Lcom/android/settings/wifi/MiuiTetherBlockList;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asw:Z

    return p1
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/wifi/MiuiTetherBlockList;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150087

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->addPreferencesFromResource(I)V

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dc;->brZ(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->ast:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "tetherBlockListPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asu:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->aio()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->ain()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/wifi/D;

    const-string/jumbo v1, ""

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/wifi/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    new-instance v1, Lcom/android/settings/wifi/Y;

    invoke-direct {v1, p0, v0, v3}, Lcom/android/settings/wifi/Y;-><init>(Lcom/android/settings/wifi/MiuiTetherBlockList;Lcom/android/settings/wifi/D;Lcom/android/settings/wifi/Y;)V

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asv:Lcom/android/settings/wifi/Y;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asv:Lcom/android/settings/wifi/Y;

    invoke-virtual {v0}, Lcom/android/settings/wifi/Y;->show()V

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_0
    new-instance v0, Lcom/android/settings/wifi/D;

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/wifi/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "saved_bundle"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const-string/jumbo v2, "show_dialog"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asw:Z

    iget-boolean v2, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asw:Z

    if-eqz v2, :cond_0

    new-instance v2, Lcom/android/settings/wifi/Y;

    new-instance v3, Lcom/android/settings/wifi/D;

    const-string/jumbo v4, "save_device_name"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "save_device_mac"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/android/settings/wifi/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, p0, v3, v1}, Lcom/android/settings/wifi/Y;-><init>(Lcom/android/settings/wifi/MiuiTetherBlockList;Lcom/android/settings/wifi/D;Lcom/android/settings/wifi/Y;)V

    iput-object v2, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asv:Lcom/android/settings/wifi/Y;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asv:Lcom/android/settings/wifi/Y;

    invoke-virtual {v0}, Lcom/android/settings/wifi/Y;->show()V

    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asv:Lcom/android/settings/wifi/Y;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "show_dialog"

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asw:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "save_device_name"

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asv:Lcom/android/settings/wifi/Y;

    invoke-virtual {v1}, Lcom/android/settings/wifi/Y;->ais()Lcom/android/settings/wifi/D;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/wifi/D;->afO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "save_device_mac"

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiTetherBlockList;->asv:Lcom/android/settings/wifi/Y;

    invoke-virtual {v1}, Lcom/android/settings/wifi/Y;->ais()Lcom/android/settings/wifi/D;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/wifi/D;->afP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "saved_bundle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStart()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0d0242

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0483

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f12027b

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiTetherBlockList;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method
