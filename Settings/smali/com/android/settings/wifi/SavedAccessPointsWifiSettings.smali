.class public Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "SavedAccessPointsWifiSettings.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;
.implements Lcom/android/settings/wifi/t;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field private static final aqz:Ljava/util/Comparator;


# instance fields
.field private aqA:Landroid/os/Bundle;

.field private aqB:Lcom/android/settings/wifi/s;

.field private aqC:Lcom/android/settingslib/wifi/i;

.field private final aqD:Landroid/net/wifi/WifiManager$ActionListener;

.field private aqE:Lcom/android/settingslib/wifi/i;

.field private aqF:Lcom/android/settings/wifi/w;

.field private aqG:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/wifi/aT;

    invoke-direct {v0}, Lcom/android/settings/wifi/aT;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqz:Ljava/util/Comparator;

    new-instance v0, Lcom/android/settings/wifi/aV;

    invoke-direct {v0}, Lcom/android/settings/wifi/aV;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/wifi/aU;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/aU;-><init>(Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;)V

    iput-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqD:Landroid/net/wifi/WifiManager$ActionListener;

    return-void
.end method

.method private ahb()V
    .locals 10

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->bWz()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqG:Landroid/net/wifi/WifiManager;

    invoke-static {v2, v0}, Lcom/android/settingslib/wifi/m;->cis(Landroid/content/Context;Landroid/net/wifi/WifiManager;)Ljava/util/List;

    move-result-object v8

    sget-object v0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqz:Ljava/util/Comparator;

    invoke-static {v8, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {v7}, Landroid/preference/PreferenceScreen;->removeAll()V

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_0

    new-instance v0, Lcom/android/settings/wifi/ac;

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/wifi/i;

    iget-object v3, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqF:Lcom/android/settings/wifi/w;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/ac;-><init>(Lcom/android/settingslib/wifi/i;Landroid/content/Context;Lcom/android/settings/wifi/w;ZLandroid/app/Fragment;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/ac;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-ge v0, v4, :cond_1

    const-string/jumbo v0, "SavedAccessPointsWifiSettings"

    const-string/jumbo v1, "Saved networks activity loaded, but there are no saved networks!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private ahc(Lcom/android/settings/wifi/ac;Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqB:Lcom/android/settings/wifi/s;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->bWO(I)V

    iput-object v2, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqB:Lcom/android/settings/wifi/s;

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/wifi/ac;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqC:Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->alW(I)V

    return-void
.end method

.method static synthetic ahd(Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->ahb()V

    return-void
.end method


# virtual methods
.method public Lx(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    return v0

    :pswitch_0
    const/16 v0, 0x25a

    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public adG(Lcom/android/settings/wifi/s;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqE:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqE:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chZ()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqG:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqE:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chj()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->removePasspointConfiguration(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->ahb()V

    :goto_1
    iput-object v3, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqE:Lcom/android/settingslib/wifi/i;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SavedAccessPointsWifiSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Failed to remove Passpoint configuration for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqE:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v2}, Lcom/android/settingslib/wifi/i;->chx()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqG:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqE:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    iget v1, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    iget-object v2, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqD:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->forget(ILandroid/net/wifi/WifiManager$ActionListener;)V

    goto :goto_1
.end method

.method public adH(Lcom/android/settings/wifi/s;)V
    .locals 0

    return-void
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 1

    instance-of v0, p1, Lcom/android/settings/wifi/ac;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/settings/wifi/ac;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->ahc(Lcom/android/settings/wifi/ac;Z)V

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x6a

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqG:Landroid/net/wifi/WifiManager;

    if-eqz p1, :cond_0

    const-string/jumbo v0, "wifi_ap_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "wifi_ap_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqA:Landroid/os/Bundle;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150110

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->addPreferencesFromResource(I)V

    new-instance v0, Lcom/android/settings/wifi/w;

    invoke-virtual {p0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/wifi/w;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqF:Lcom/android/settings/wifi/w;

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqC:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqA:Landroid/os/Bundle;

    invoke-direct {v0, v1, v2}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqC:Lcom/android/settingslib/wifi/i;

    iput-object v3, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqA:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqC:Lcom/android/settingslib/wifi/i;

    iput-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqE:Lcom/android/settingslib/wifi/i;

    new-instance v0, Lcom/android/settings/wifi/s;

    invoke-virtual {p0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqC:Lcom/android/settingslib/wifi/i;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/s;-><init>(Landroid/content/Context;Lcom/android/settings/wifi/t;Lcom/android/settingslib/wifi/i;IZ)V

    iput-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqB:Lcom/android/settings/wifi/s;

    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqB:Lcom/android/settings/wifi/s;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->ahb()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqB:Lcom/android/settings/wifi/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqB:Lcom/android/settings/wifi/s;

    invoke-virtual {v0}, Lcom/android/settings/wifi/s;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqC:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqA:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqC:Lcom/android/settingslib/wifi/i;

    iget-object v1, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqA:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/wifi/i;->cie(Landroid/os/Bundle;)V

    const-string/jumbo v0, "wifi_ap_state"

    iget-object v1, p0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;->aqA:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method
