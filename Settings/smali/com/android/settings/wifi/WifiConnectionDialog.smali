.class public Lcom/android/settings/wifi/WifiConnectionDialog;
.super Lmiui/app/Activity;
.source "WifiConnectionDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private aqb:Landroid/net/wifi/WifiConfiguration;

.field private aqc:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method static agL(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "wifi_dialog_remind_type"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static agM(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "wifi_connect_type"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static agN(Landroid/content/Context;)Z
    .locals 6

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-string/jumbo v1, "donot_remind_switch_to_wifi_dialog"

    const-wide/32 v4, 0x7fffffff

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sub-long v0, v2, v0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private agO()V
    .locals 3

    new-instance v0, Lmiui/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->agP()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    invoke-static {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->agL(Landroid/content/Context;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    const v2, 0x7f1214fd

    invoke-virtual {p0, v2}, Lcom/android/settings/wifi/WifiConnectionDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/app/AlertDialog$Builder;->setCheckBox(ZLjava/lang/CharSequence;)Lmiui/app/AlertDialog$Builder;

    const v1, 0x1040009

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/WifiConnectionDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    const v1, 0x1040013

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/WifiConnectionDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->show()Lmiui/app/AlertDialog;

    return-void
.end method

.method private agP()Ljava/lang/CharSequence;
    .locals 2

    iget v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqc:I

    const/4 v1, 0x2

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    if-nez v0, :cond_0

    const v0, 0x7f121616

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f1214d6

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private agQ(Z)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "wifi_dialog_remind_type"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private agR(J)V
    .locals 3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "donot_remind_switch_to_wifi_dialog"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private agS()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiConnectionDialog;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private getMessage()Ljava/lang/CharSequence;
    .locals 4

    const/4 v3, 0x0

    const v0, 0x7f121602

    const/4 v2, 0x1

    iget v1, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqc:I

    if-ne v2, v1, :cond_1

    invoke-static {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->agM(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    iget-object v1, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f120adb

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqc:I

    const/4 v2, 0x2

    if-ne v2, v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const v0, 0x7f121615

    goto :goto_1

    :cond_3
    return-object v3
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_6

    iget v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqc:I

    if-ne v2, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->agM(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->agS()V

    :cond_0
    :goto_0
    move-object v0, p1

    check-cast v0, Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->isChecked()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_1
    invoke-direct {p0, v0}, Lcom/android/settings/wifi/WifiConnectionDialog;->agQ(Z)V

    :cond_1
    :goto_2
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->finish()V

    return-void

    :cond_2
    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    iget v3, v3, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v0, v3, v1}, Landroid/net/wifi/WifiManager;->connect(ILandroid/net/wifi/WifiManager$ActionListener;)V

    const-wide/32 v0, 0x7fffffff

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/WifiConnectionDialog;->agR(J)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqc:I

    if-ne v3, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->agS()V

    goto :goto_0

    :cond_4
    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    const/4 v0, -0x2

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqc:I

    if-ne v2, v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_7

    invoke-static {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->agM(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/WifiConnectionDialog;->agR(J)V

    goto :goto_2

    :cond_7
    iget v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqc:I

    if-ne v3, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_1

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v0, "miui.intent.action.SELECT_WIFI_AP"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "extra_best_ap"

    move-object v0, v1

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/android/settings/wifi/WifiConnectionDialog;->startActivity(Landroid/content/Intent;)V

    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->requestFeature(I)Z

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "miui.intent.action.SWITCH_TO_WIFI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput v2, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqc:I

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_best_ap"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iput-object v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    if-nez v0, :cond_1

    const-string/jumbo v0, "SwitchToWifiDialog"

    const-string/jumbo v1, "config is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->finish()V

    return-void

    :cond_0
    const-string/jumbo v1, "miui.intent.action.SELECT_WIFI_AP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqc:I

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_best_ap"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iput-object v0, p0, Lcom/android/settings/wifi/WifiConnectionDialog;->aqb:Landroid/net/wifi/WifiConfiguration;

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->agO()V

    return-void

    :cond_2
    const-string/jumbo v1, "SwitchToWifiDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiConnectionDialog;->finish()V

    return-void
.end method
