.class public Lcom/android/settings/wifi/MiuiAccessPointPreference;
.super Lcom/android/settings/wifi/AccessPointPreference;
.source "MiuiAccessPointPreference.java"


# static fields
.field private static final arO:[B

.field private static arX:Ljava/util/Comparator;


# instance fields
.field private arP:Landroid/view/View$OnClickListener;

.field private arQ:I

.field private arR:I

.field private arS:Z

.field private arT:Z

.field private arU:Z

.field private arV:Z

.field private arW:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arO:[B

    new-instance v0, Lcom/android/settings/wifi/bp;

    invoke-direct {v0}, Lcom/android/settings/wifi/bp;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arX:Ljava/util/Comparator;

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x17t
        -0xet
        0x6t
        0x1t
        0x1t
        0x3t
        0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/AccessPointPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arS:Z

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arW:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/settingslib/wifi/i;Landroid/content/Context;Lcom/android/settings/wifi/w;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/wifi/AccessPointPreference;-><init>(Lcom/android/settingslib/wifi/i;Landroid/content/Context;Lcom/android/settings/wifi/w;Z)V

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arS:Z

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arW:Z

    invoke-direct {p0, p2}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->init(Landroid/content/Context;)V

    return-void
.end method

.method public static ahL()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arX:Ljava/util/Comparator;

    return-object v0
.end method

.method private ahM(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "device_provisioned"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private ahN(Landroid/net/wifi/ScanResult;)Z
    .locals 5

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-object v2, p1, Landroid/net/wifi/ScanResult;->informationElements:[Landroid/net/wifi/ScanResult$InformationElement;

    if-eqz v2, :cond_1

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-object v3, v2, v0

    iget v3, v3, Landroid/net/wifi/ScanResult$InformationElement;->id:I

    const/16 v4, 0xdd

    if-ne v3, v4, :cond_0

    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/net/wifi/ScanResult$InformationElement;->bytes:[B

    sget-object v4, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arO:[B

    array-length v4, v4

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    sget-object v4, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arO:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private init(Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x1

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->ahM(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arV:Z

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arV:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0d0175

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->setLayoutResource(I)V

    :goto_0
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lmiui/R$attr;->textColorChecked:I

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arQ:I

    sget v2, Lmiui/R$attr;->preferencePrimaryTextColor:I

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arR:I

    return-void

    :cond_0
    const v0, 0x7f0d001b

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->setLayoutResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public ahI(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arT:Z

    return-void
.end method

.method public ahJ(Lcom/android/settingslib/wifi/i;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aew()V

    return-void
.end method

.method public ahK(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arP:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public ahO()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    iget-object v0, v0, Lcom/android/settingslib/wifi/i;->cCI:Landroid/net/wifi/ScanResult;

    invoke-static {v0}, Lcom/android/settings/wifi/g;->aaP(Landroid/net/wifi/ScanResult;)Z

    move-result v0

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 11

    const/16 v5, 0x8

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/wifi/AccessPointPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a0133

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    iget-object v6, v6, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    aput-object v6, v2, v4

    const v6, 0x7f120b3d

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v1, Lmiui/R$drawable;->btn_inline_detail_light:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arS:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arS:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arP:Landroid/view/View$OnClickListener;

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arW:Z

    if-eqz v1, :cond_2

    move v1, v4

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f0a0404

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aex()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arQ:I

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setTextColor(I)V

    :goto_2
    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arU:Z

    if-eqz v1, :cond_4

    const v1, 0x7f080145

    move v2, v1

    :goto_3
    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f0702ca

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setCompoundDrawablePadding(I)V

    invoke-virtual {v0, v4, v4, v2, v4}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    const v1, 0x7f0a051d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-nez v2, :cond_0

    const/4 v2, 0x5

    invoke-virtual {v1, v4, v2, v4, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v2

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCQ:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v6, v4

    move v7, v4

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/ScanResult;

    invoke-virtual {v2}, Landroid/net/wifi/ScanResult;->is24GHz()Z

    move-result v10

    if-eqz v10, :cond_5

    move v2, v8

    move v6, v7

    :goto_5
    move v7, v6

    move v6, v2

    goto :goto_4

    :cond_1
    move-object v1, v3

    goto/16 :goto_0

    :cond_2
    move v1, v5

    goto/16 :goto_1

    :cond_3
    iget v1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arR:I

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setTextColor(I)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->ahO()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v1

    iget-object v1, v1, Lcom/android/settingslib/wifi/i;->cCI:Landroid/net/wifi/ScanResult;

    invoke-static {v1}, Lcom/android/settings/wifi/g;->aaO(Landroid/net/wifi/ScanResult;)I

    move-result v1

    move v2, v1

    goto :goto_3

    :cond_5
    invoke-virtual {v2}, Landroid/net/wifi/ScanResult;->is5GHz()Z

    move-result v2

    if-eqz v2, :cond_13

    move v2, v6

    move v6, v8

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v9, v2, Landroid/util/DisplayMetrics;->density:F

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v10, 0x7f0800a1

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v6, :cond_b

    if-eqz v7, :cond_b

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0800a0

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, v2

    :goto_6
    if-eqz v7, :cond_8

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    const/high16 v2, 0x40a00000    # 5.0f

    mul-float/2addr v2, v9

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v2, v5

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v2

    if-ne v2, v8, :cond_c

    :goto_7
    if-eqz v8, :cond_d

    move v2, v1

    :goto_8
    if-eqz v8, :cond_7

    move v1, v4

    :cond_7
    invoke-virtual {v0, v2, v4, v1, v4}, Landroid/widget/CheckedTextView;->setPadding(IIII)V

    :cond_8
    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arV:Z

    if-eqz v0, :cond_f

    new-instance v1, Landroid/widget/RadioButton;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aex()Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v4

    :goto_9
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aex()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setFocusable(Z)V

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setClickable(Z)V

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v5, -0x1

    invoke-direct {v0, v2, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    const v0, 0x7f0a005b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_a
    const v0, 0x7f0a0403

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chn()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageLevel(I)V

    if-lez v1, :cond_9

    sget-object v2, Lcom/android/settings/wifi/MiuiAccessPointPreference;->anE:[I

    array-length v2, v2

    if-gt v1, v2, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v4, Lcom/android/settings/wifi/MiuiAccessPointPreference;->anE:[I

    add-int/lit8 v1, v1, -0x1

    aget v1, v4, v1

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_9
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v1

    iget-object v1, v1, Lcom/android/settingslib/wifi/i;->cCI:Landroid/net/wifi/ScanResult;

    invoke-direct {p0, v1}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->ahN(Landroid/net/wifi/ScanResult;)Z

    move-result v1

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->anF:Lcom/android/settingslib/wifi/i;

    iget v1, v1, Lcom/android/settingslib/wifi/i;->cCS:I

    if-eqz v1, :cond_10

    const v1, 0x7f0802a5

    :goto_b
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_c
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aex()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chY()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v0, v1, :cond_a

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cil()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "WPA"

    if-ne v0, v1, :cond_a

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->arV:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_a

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAccessPointPreference;->mContext:Landroid/content/Context;

    const v2, 0x7f1214e8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    return-void

    :cond_b
    if-nez v7, :cond_12

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object v1, v2

    goto/16 :goto_6

    :cond_c
    move v8, v4

    goto/16 :goto_7

    :cond_d
    move v2, v4

    goto/16 :goto_8

    :cond_e
    const/4 v0, 0x4

    goto/16 :goto_9

    :cond_f
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lmiui/R$dimen;->preference_custom_widget_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p1, v1, v2, v0, v4}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_a

    :cond_10
    const v1, 0x7f0802be

    goto :goto_b

    :cond_11
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_c

    :cond_12
    move-object v1, v2

    goto/16 :goto_6

    :cond_13
    move v2, v6

    move v6, v7

    goto/16 :goto_5

    :cond_14
    move v2, v4

    goto/16 :goto_3
.end method

.method public setChecked(Z)V
    .locals 0

    return-void
.end method
