.class public Lcom/android/settings/wifi/MiuiWifiSettings;
.super Lcom/android/settings/wifi/WifiSettings;
.source "MiuiWifiSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/bc;


# static fields
.field public static final alf:Ljava/util/HashSet;


# instance fields
.field private akG:Landroid/widget/TextView;

.field private akH:Landroid/widget/ImageButton;

.field private akI:Landroid/widget/TextView;

.field private akJ:Landroid/preference/CheckBoxPreference;

.field private akK:Landroid/preference/ListPreference;

.field private akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

.field private akM:Lcom/android/settingslib/wifi/i;

.field private akN:Landroid/preference/CheckBoxPreference;

.field private akO:Ljava/lang/String;

.field private akP:Landroid/content/IntentFilter;

.field private akQ:Z

.field private akR:Z

.field private akS:Z

.field private akT:Ljava/lang/String;

.field private akU:Landroid/net/NetworkInfo$State;

.field private akV:Landroid/widget/ImageButton;

.field private akW:Landroid/widget/TextView;

.field private akX:Landroid/preference/CheckBoxPreference;

.field private akY:Lcom/android/settings/aD;

.field private akZ:Landroid/widget/TextView;

.field private ala:Landroid/preference/ListPreference;

.field private alb:Z

.field private alc:Landroid/preference/CheckBoxPreference;

.field private ald:Landroid/net/wifi/WifiManager;

.field private ale:Lmiui/preference/ValuePreference;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/MiuiWifiSettings;->alf:Ljava/util/HashSet;

    sget-object v0, Lcom/android/settings/wifi/MiuiWifiSettings;->alf:Ljava/util/HashSet;

    const-string/jumbo v1, "CMCC"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/wifi/MiuiWifiSettings;->alf:Ljava/util/HashSet;

    const-string/jumbo v1, "CMCC-AUTO"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/wifi/MiuiWifiSettings;->alf:Ljava/util/HashSet;

    const-string/jumbo v1, "CMCC-EDU"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/wifi/MiuiWifiSettings;->alf:Ljava/util/HashSet;

    const-string/jumbo v1, "CMCC-WEB"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiSettings;-><init>()V

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akQ:Z

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akR:Z

    return-void
.end method

.method private aaR()V
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akJ:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akJ:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "cmcc_network_notification"

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akJ:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12156c

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akJ:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12156b

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    const-string/jumbo v0, "wifi_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akJ:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->abh()V

    return-void
.end method

.method private aaU()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqI:Z

    if-eqz v0, :cond_2

    :cond_0
    const-string/jumbo v0, "wifi_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void

    :cond_2
    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akX:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    const-string/jumbo v0, "wifi_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akX:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akX:Landroid/preference/CheckBoxPreference;

    :cond_3
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "wifi_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const-string/jumbo v3, "saved_wifi"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_4
    const-string/jumbo v0, "connect_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akK:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akK:Landroid/preference/ListPreference;

    if-eqz v0, :cond_6

    const-string/jumbo v0, "ro.boot.hwversion"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string/jumbo v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string/jumbo v1, "support_choose_connect_mode"

    invoke-static {v1, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v0, :cond_9

    array-length v1, v0

    if-lez v1, :cond_9

    aget-object v0, v0, v4

    const-string/jumbo v1, "4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_5
    const-string/jumbo v0, "wifi_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akK:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_6
    :goto_1
    const-string/jumbo v0, "traffic_balance"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ala:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ala:Landroid/preference/ListPreference;

    if-eqz v0, :cond_7

    const-string/jumbo v0, "sys.net.support.netprio"

    invoke-static {v0, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_a

    const-string/jumbo v0, "wifi_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ala:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_7
    :goto_2
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->aaR()V

    return-void

    :cond_8
    move-object v0, v1

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akK:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_1

    :cond_a
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ala:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_2
.end method

.method private aaV()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aaW()V
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x64

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/wifi/MiuiWifiSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :cond_0
    return-void
.end method

.method private abc(Ljava/util/Collection;)Ljava/util/ArrayList;
    .locals 9

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/wifi/i;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v1, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1, p0}, Lcom/android/settingslib/wifi/i;->chw(Lcom/android/settingslib/wifi/j;)V

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chh()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cio()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cio()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->ahJ(Lcom/android/settingslib/wifi/i;)V

    :goto_1
    iget-object v2, v1, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/android/settings/wifi/T;

    invoke-direct {v2, v1, p0}, Lcom/android/settings/wifi/T;-><init>(Lcom/android/settingslib/wifi/i;Lcom/android/settings/MiuiSettingsPreferenceFragment;)V

    invoke-virtual {v0, v2}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->ahK(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v1, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/android/settings/wifi/ac;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getThemedContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqP:Lcom/android/settings/wifi/w;

    const/4 v4, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/ac;-><init>(Lcom/android/settingslib/wifi/i;Landroid/content/Context;Lcom/android/settings/wifi/w;ZLandroid/app/Fragment;)V

    invoke-virtual {v1, v0}, Lcom/android/settingslib/wifi/i;->chK(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->ahL()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v6
.end method

.method private abd(Landroid/content/Context;)V
    .locals 3

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "wifi_country_code"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->setCountryCode(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method private abe(Ljava/lang/String;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    if-eqz p1, :cond_0

    iget-object v3, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/net/wifi/ScanResult;)V

    invoke-virtual {v3}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v3, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->ahe(Lcom/android/settingslib/wifi/i;I)V

    :cond_1
    move-object p1, v1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private abf(Lcom/android/settingslib/wifi/i;)V
    .locals 8

    const/4 v7, 0x0

    const-string/jumbo v0, "wifi_settings"

    const-string/jumbo v1, "wifi_share_password"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akM:Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/settings/wifi/F;->agF(Landroid/content/Context;Lcom/android/settingslib/wifi/i;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akR:Z

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v4, v3, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    const v0, 0x3f4ccccd    # 0.8f

    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-virtual {v2, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const/16 v0, 0x80

    invoke-virtual {v2, v0}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v5, 0x7f0d027d

    invoke-virtual {v0, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v0, 0x7f0a0376

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v6, Lmiui/R$style;->Theme_Light_Dialog_Alert:I

    invoke-direct {v0, v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f1215f4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1215f2

    invoke-virtual {v0, v1, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lcom/android/settings/wifi/ai;

    invoke-direct {v1, p0, v3, v4, v2}, Lcom/android/settings/wifi/ai;-><init>(Lcom/android/settings/wifi/MiuiWifiSettings;Landroid/view/WindowManager$LayoutParams;FLandroid/view/Window;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private abh()V
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akJ:Landroid/preference/CheckBoxPreference;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/wifi/WifiTipActivity;->aiR(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akJ:Landroid/preference/CheckBoxPreference;

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akJ:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    return-void
.end method

.method static synthetic abj(Lcom/android/settings/wifi/MiuiWifiSettings;)Landroid/net/NetworkInfo$State;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akU:Landroid/net/NetworkInfo$State;

    return-object v0
.end method

.method static synthetic abk(Lcom/android/settings/wifi/MiuiWifiSettings;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->alb:Z

    return v0
.end method

.method static synthetic abl(Lcom/android/settings/wifi/MiuiWifiSettings;Lcom/android/settingslib/wifi/i;)Lcom/android/settingslib/wifi/i;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akM:Lcom/android/settingslib/wifi/i;

    return-object p1
.end method

.method static synthetic abm(Lcom/android/settings/wifi/MiuiWifiSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akQ:Z

    return p1
.end method

.method static synthetic abn(Lcom/android/settings/wifi/MiuiWifiSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akR:Z

    return p1
.end method

.method static synthetic abo(Lcom/android/settings/wifi/MiuiWifiSettings;Landroid/net/NetworkInfo$State;)Landroid/net/NetworkInfo$State;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akU:Landroid/net/NetworkInfo$State;

    return-object p1
.end method

.method static synthetic abp(Lcom/android/settings/wifi/MiuiWifiSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->alb:Z

    return p1
.end method


# virtual methods
.method public QH(ILandroid/os/Bundle;)V
    .locals 4

    const/4 v3, -0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    const-string/jumbo v0, "config"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqN:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->save(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqM:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->connect(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    goto :goto_0

    :sswitch_1
    const-string/jumbo v0, "network_id"

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v0, "is_delete"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    const-string/jumbo v0, "config"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    if-eqz v2, :cond_1

    if-eq v1, v3, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqO:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->forget(ILandroid/net/wifi/WifiManager$ActionListener;)V

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqN:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->save(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch
.end method

.method protected aaS(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->alc:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    return-void
.end method

.method protected aaT(Z)V
    .locals 5

    const v4, 0x7f080382

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/wifi/WifiSettings;->aaT(Z)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akZ:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akG:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akV:Landroid/widget/ImageButton;

    const v1, 0x7f080376

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akV:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akZ:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akG:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akV:Landroid/widget/ImageButton;

    const v1, 0x7f080377

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akV:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public aaX(Lcom/android/settingslib/wifi/i;)V
    .locals 1

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->cio()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->cio()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v0, p1}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->ahJ(Lcom/android/settingslib/wifi/i;)V

    :cond_0
    return-void
.end method

.method public aaY()V
    .locals 12

    const/4 v5, 0x4

    const/4 v11, 0x3

    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bXB()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v2

    const-string/jumbo v0, "nearby_wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "connected_wifi"

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    packed-switch v2, :pswitch_data_0

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akX:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akX:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->abh()V

    return-void

    :pswitch_0
    if-nez v0, :cond_10

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    move-object v2, v0

    :goto_1
    invoke-virtual {v2}, Landroid/preference/PreferenceGroup;->removeAll()V

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    move-object v1, v3

    :cond_4
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/a;->cgM()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->abc(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v5, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v4

    move-object v6, v1

    move v1, v4

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v9

    if-eqz v9, :cond_9

    iget-boolean v9, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-nez v9, :cond_8

    if-nez v6, :cond_5

    new-instance v6, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v6, v9}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const-string/jumbo v9, "connected_wifi"

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    const v9, 0x7f120476

    invoke-virtual {v6, v9}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    invoke-virtual {v6, v11}, Landroid/preference/PreferenceCategory;->setOrder(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {v6}, Landroid/preference/PreferenceCategory;->removeAll()V

    :cond_5
    invoke-virtual {v7}, Lcom/android/settingslib/wifi/i;->chv()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v0, v10}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->setOrder(I)V

    invoke-virtual {v6, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v0, v4}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->ahI(Z)V

    move v0, v1

    move v1, v5

    move-object v5, v6

    :goto_3
    move-object v6, v5

    move v5, v1

    move v1, v0

    goto :goto_2

    :cond_6
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const-string/jumbo v2, "nearby_wifi"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->setKey(Ljava/lang/String;)V

    const v2, 0x7f120b34

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->setTitle(I)V

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceGroup;->setOrder(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    move-object v2, v0

    goto/16 :goto_1

    :cond_7
    add-int/lit8 v7, v5, 0x1

    invoke-virtual {v0, v5}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->setOrder(I)V

    invoke-virtual {v6, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    move v0, v1

    move-object v5, v6

    move v1, v7

    goto :goto_3

    :cond_8
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->setOrder(I)V

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    move v0, v7

    move v1, v5

    move-object v5, v6

    goto :goto_3

    :cond_9
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->setOrder(I)V

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    move v0, v7

    move v1, v5

    move-object v5, v6

    goto :goto_3

    :cond_a
    new-instance v0, Lcom/android/settings/wifi/O;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getThemedContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/android/settings/wifi/O;-><init>(Landroid/content/Context;)V

    const-string/jumbo v4, "manually_add_network"

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    const v4, 0x7f1214c2

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOrder(I)V

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->alc:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_1
    const v2, 0x7f12160f

    invoke-virtual {p0, v2}, Lcom/android/settings/wifi/MiuiWifiSettings;->aaS(I)V

    :pswitch_2
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v2, v10}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aev(Z)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v2}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aew()V

    iput-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    :cond_b
    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_c
    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v2, v10}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aev(Z)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v2}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aew()V

    iput-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    :cond_d
    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_e
    if-eqz v1, :cond_f

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_f
    const v0, 0x7f121531

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->aaS(I)V

    goto/16 :goto_0

    :cond_10
    move-object v2, v0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public aaZ()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/wifi/WifiSettings;->aaZ()V

    return-void
.end method

.method public aba(Lcom/android/settingslib/wifi/i;)V
    .locals 1

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->cio()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/i;->cio()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aey()V

    :cond_0
    return-void
.end method

.method public abb(I)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/wifi/WifiSettings;->abb(I)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const v0, 0x7f121531

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->aaS(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method abg(Lcom/android/settings/wifi/A;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/wifi/WifiSettings;->abg(Lcom/android/settings/wifi/A;)V

    return-void
.end method

.method protected abi(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akY:Lcom/android/settings/aD;

    invoke-virtual {v0}, Lcom/android/settings/aD;->bmR()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akY:Lcom/android/settings/aD;

    invoke-virtual {v0}, Lcom/android/settings/aD;->bmS()V

    goto :goto_0
.end method

.method protected aq()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    new-instance v0, Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getThemedContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-direct {v0, v3, v4}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    const-string/jumbo v3, ""

    iput-object v3, v0, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    new-instance v3, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getThemedContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v0, v4, v5, v2}, Lcom/android/settings/wifi/MiuiAccessPointPreference;-><init>(Lcom/android/settingslib/wifi/i;Landroid/content/Context;Lcom/android/settings/wifi/w;Z)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-super {p0, p1}, Lcom/android/settings/wifi/WifiSettings;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "wifi_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    const-string/jumbo v4, "network_check"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v3, "wifi_enable"

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->alc:Landroid/preference/CheckBoxPreference;

    new-instance v0, Lcom/android/settings/wifi/i;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->alc:Landroid/preference/CheckBoxPreference;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/wifi/i;-><init>(Lcom/android/settings/MiuiSettingsPreferenceFragment;Landroid/preference/CheckBoxPreference;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqK:Lcom/android/settings/wifi/i;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v3, "enable_data_and_wifi_roam"

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akN:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "wifi_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v5, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akN:Landroid/preference/CheckBoxPreference;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akN:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "data_and_wifi_roam"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ale:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ale:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v5, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ale:Lmiui/preference/ValuePreference;

    :cond_4
    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->alc:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getPaddingRight()I

    move-result v4

    invoke-virtual {v0, v1, v3, v4, v2}, Landroid/widget/ListView;->setPadding(IIII)V

    :cond_5
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akX:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akX:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_6
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "miui.intent.extra.OPEN_WIFI_SSID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    new-instance v1, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v1}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    invoke-static {v0}, Lcom/android/settingslib/wifi/i;->chG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iget-object v0, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v1, v5}, Landroid/net/wifi/WifiManager;->connect(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    :cond_7
    return-void

    :cond_8
    move v0, v2

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->QH(ILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akQ:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/wifi/WifiSettings;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "is_dialog_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akR:Z

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akR:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "accesspoint_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/android/settingslib/wifi/i;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/settingslib/wifi/i;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-direct {p0, v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->abf(Lcom/android/settingslib/wifi/i;)V

    :cond_0
    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_2

    const v0, 0x7f130205

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->setThemeRes(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "extra_disable_back"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akQ:Z

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->abd(Landroid/content/Context;)V

    :goto_0
    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqI:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    :cond_1
    new-instance v0, Lcom/android/settings/aD;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/aD;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akY:Lcom/android/settings/aD;

    const v0, 0x7f150115

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->aaU()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akP:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akP:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akP:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Lcom/android/settings/wifi/af;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/af;-><init>(Lcom/android/settings/wifi/MiuiWifiSettings;)V

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akT:Ljava/lang/String;

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_3

    const v0, 0x7f13020d

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->setThemeRes(I)V

    goto :goto_0

    :cond_3
    const v0, 0x7f13020c

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->setThemeRes(I)V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bXB()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqI:Z

    if-eqz v0, :cond_2

    :cond_1
    return-void

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/android/settings/wifi/WifiSettings;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akY:Lcom/android/settings/aD;

    invoke-virtual {v1}, Lcom/android/settings/aD;->buX()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_3
    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/wifi/WifiSettings;->onDestroy()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    const/4 v3, -0x1

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/settings/dc;->bYu(Landroid/app/Activity;Landroid/view/View;)V

    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0d023f

    :goto_0
    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v1, :cond_3

    :goto_1
    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    const v2, 0x7f0d0029

    invoke-virtual {p1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_1

    new-instance v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/preference/PreferenceFrameLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-object v1

    :cond_2
    const v0, 0x7f0d0177

    goto :goto_0

    :cond_3
    const v0, 0x7f0d027a

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Lcom/android/settings/wifi/WifiSettings;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :sswitch_0
    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akQ:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->finish()V

    return v1

    :sswitch_1
    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->abi(Z)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/wifi/WifiSettings;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "traffic_balance"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f030101

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ala:Landroid/preference/ListPreference;

    aget-object v3, v3, v0

    invoke-virtual {v4, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "user_network_priority_enabled"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    return v2

    :cond_1
    const-string/jumbo v3, "connect_mode"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_1
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f03005d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akK:Landroid/preference/ListPreference;

    aget-object v3, v3, v0

    invoke-virtual {v4, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "wireless_compatible_mode"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-static {}, Landroid/net/wifi/MiuiWifiManager;->getInstance()Landroid/net/wifi/MiuiWifiManager;

    move-result-object v3

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/net/wifi/MiuiWifiManager;->setCompatibleMode(Z)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    return v2

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8

    const/4 v7, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "manually_add_network"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->aaW()V

    return v1

    :cond_0
    const-string/jumbo v3, "wifi_enable"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqK:Lcom/android/settings/wifi/i;

    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/settings/wifi/i;->abK(Z)V

    return v1

    :cond_1
    const-string/jumbo v3, "cmcc_network_notification"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    move-object v0, p2

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v3, v0}, Lcom/android/settings/wifi/WifiTipActivity;->aiQ(Landroid/content/Context;Z)V

    :cond_2
    :goto_0
    instance-of v0, p2, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    if-eqz v0, :cond_6

    move-object v0, p2

    check-cast v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aex()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/settingslib/wifi/i;->chY()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v3

    sget-object v4, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v3, v4, :cond_6

    invoke-virtual {v0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/settingslib/wifi/i;->cil()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->abf(Lcom/android/settingslib/wifi/i;)V

    return v1

    :cond_3
    const-string/jumbo v3, "enable_data_and_wifi_roam"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "data_and_wifi_roam"

    move-object v0, p2

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v3, "connectivity"

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->reportBadNetwork(Landroid/net/Network;)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    const-string/jumbo v3, "network_check"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "com.miui.securitycenter"

    const-string/jumbo v4, "com.miui.networkassistant.ui.activity.NetworkDiagnosticsActivity"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    instance-of v0, p2, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    if-eqz v0, :cond_d

    iput-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->alb:Z

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_7

    const-string/jumbo v0, "provision_wifi"

    const-string/jumbo v3, "provision_wifi_connect_count"

    invoke-static {v0, v3}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move-object v0, p2

    check-cast v0, Lcom/android/settings/wifi/MiuiAccessPointPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aev(Z)V

    invoke-virtual {v0}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aeu()Lcom/android/settingslib/wifi/i;

    move-result-object v3

    iget-object v4, v3, Lcom/android/settingslib/wifi/i;->cCN:Landroid/net/wifi/WifiConfiguration;

    iget v5, v3, Lcom/android/settingslib/wifi/i;->cCO:I

    if-le v5, v7, :cond_c

    iget v5, v3, Lcom/android/settingslib/wifi/i;->cCS:I

    if-eqz v5, :cond_8

    iget v5, v3, Lcom/android/settingslib/wifi/i;->cCS:I

    const/16 v6, 0xa

    if-ne v5, v6, :cond_a

    :cond_8
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v2

    iget v5, v3, Lcom/android/settingslib/wifi/i;->cCO:I

    if-eq v2, v5, :cond_e

    :cond_9
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    iget v3, v3, Lcom/android/settingslib/wifi/i;->cCO:I

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqM:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v2, v3, v4}, Landroid/net/wifi/WifiManager;->connect(ILandroid/net/wifi/WifiManager$ActionListener;)V

    :goto_2
    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akL:Lcom/android/settings/wifi/MiuiAccessPointPreference;

    return v1

    :cond_a
    iget v5, v3, Lcom/android/settingslib/wifi/i;->cCS:I

    const/16 v6, 0xb

    if-eq v5, v6, :cond_8

    iget v5, v3, Lcom/android/settingslib/wifi/i;->cCS:I

    const/4 v6, 0x3

    if-eq v5, v6, :cond_8

    if-eqz v4, :cond_b

    iget-object v5, v4, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-nez v5, :cond_8

    :cond_b
    iget-object v5, v4, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aget-object v2, v5, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_8

    :cond_c
    iput v7, v3, Lcom/android/settingslib/wifi/i;->cCO:I

    :cond_d
    invoke-super {p0, p1, p2}, Lcom/android/settings/wifi/WifiSettings;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_e
    iget v2, v4, Landroid/net/wifi/WifiConfiguration;->status:I

    if-eq v2, v1, :cond_9

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/MiuiAccessPointPreference;->aev(Z)V

    goto :goto_2
.end method

.method public onResume()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0}, Lcom/android/settings/wifi/WifiSettings;->onResume()V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akN:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "data_and_wifi_roam"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akK:Landroid/preference/ListPreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "wireless_compatible_mode"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akK:Landroid/preference/ListPreference;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akK:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akK:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ala:Landroid/preference/ListPreference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "cloud_network_priority_enabled"

    invoke-static {v0, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "on"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "user_network_priority_enabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ala:Landroid/preference/ListPreference;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ala:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ala:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/wifi/C;->afN(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akS:Z

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->abh()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akP:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqL:Lcom/android/settingslib/wifi/a;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->ald:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getVerboseLoggingLevel()I

    move-result v0

    sput v0, Lcom/android/settingslib/wifi/a;->cCd:I

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "provision_wifi_page"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blD(Landroid/app/Activity;Ljava/lang/String;)V

    const-string/jumbo v0, "provision_wifi"

    const-string/jumbo v1, "provision_wifi_page_count"

    invoke-static {v0, v1}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "ssid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akO:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akO:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akO:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->abe(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v1, "fast"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v1, 0x2

    goto/16 :goto_1

    :cond_7
    move v1, v2

    goto/16 :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/wifi/WifiSettings;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "is_dialog_shown"

    iget-boolean v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akR:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akR:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akM:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akM:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/wifi/i;->cie(Landroid/os/Bundle;)V

    const-string/jumbo v1, "accesspoint_info"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0x8

    invoke-super {p0, p1, p2}, Lcom/android/settings/wifi/WifiSettings;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqH:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0a0363

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    const v0, 0x7f0a0361

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akZ:Landroid/widget/TextView;

    const v0, 0x7f0a035d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akG:Landroid/widget/TextView;

    const v0, 0x7f0a0360

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akI:Landroid/widget/TextView;

    const v0, 0x7f0a035e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akH:Landroid/widget/ImageButton;

    const v0, 0x7f0a035f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akV:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->aqJ:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akZ:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akG:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akI:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akH:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akV:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_1
    sget-boolean v0, Lmiui/os/Build;->IS_MIPAD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->aaV()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->aaT(Z)V

    new-instance v0, Lcom/android/settings/wifi/ag;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/ag;-><init>(Lcom/android/settings/wifi/MiuiWifiSettings;)V

    new-instance v1, Lcom/android/settings/wifi/ah;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/ah;-><init>(Lcom/android/settings/wifi/MiuiWifiSettings;)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akZ:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akG:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akI:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akV:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akH:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akG:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akW:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akI:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akH:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings;->akV:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method
