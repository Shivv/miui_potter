.class public Lcom/android/settings/wifi/p2p/b;
.super Lcom/android/settings/core/e;
.source "WifiP2pPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/b;


# instance fields
.field private final aiS:Landroid/content/IntentFilter;

.field private aiT:Landroid/preference/Preference;

.field private final aiU:Landroid/net/wifi/WifiManager;

.field final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Landroid/net/wifi/WifiManager;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/wifi/p2p/d;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/p2p/d;-><init>(Lcom/android/settings/wifi/p2p/b;)V

    iput-object v0, p0, Lcom/android/settings/wifi/p2p/b;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/wifi/p2p/b;->aiS:Landroid/content/IntentFilter;

    iput-object p3, p0, Lcom/android/settings/wifi/p2p/b;->aiU:Landroid/net/wifi/WifiManager;

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method

.method private Zq()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/b;->aiT:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/b;->aiT:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/wifi/p2p/b;->aiU:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method static synthetic Zr(Lcom/android/settings/wifi/p2p/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/p2p/b;->Zq()V

    return-void
.end method


# virtual methods
.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v0, "wifi_direct"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/p2p/b;->aiT:Landroid/preference/Preference;

    invoke-direct {p0}, Lcom/android/settings/wifi/p2p/b;->Zq()V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "wifi_direct"

    return-object v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/b;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/p2p/b;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/p2p/b;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/p2p/b;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/p2p/b;->aiS:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
