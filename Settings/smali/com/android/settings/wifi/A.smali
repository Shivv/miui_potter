.class public Lcom/android/settings/wifi/A;
.super Ljava/lang/Object;
.source "WifiConfigController.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final aof:[I


# instance fields
.field private aoA:[Ljava/lang/String;

.field private aoB:I

.field private aoC:Ljava/lang/String;

.field private aoD:Landroid/widget/TextView;

.field protected aoE:Landroid/widget/TextView;

.field private aoF:Landroid/widget/ArrayAdapter;

.field private final aoG:Landroid/widget/ArrayAdapter;

.field private final aoH:Landroid/widget/ArrayAdapter;

.field private aoI:Landroid/widget/Spinner;

.field protected aoJ:Landroid/widget/TextView;

.field protected aoK:Landroid/widget/TextView;

.field protected aoL:Landroid/widget/TextView;

.field protected aoM:Landroid/widget/TextView;

.field protected aoN:Landroid/net/IpConfiguration$ProxySettings;

.field protected aoO:Landroid/widget/Spinner;

.field private aoP:Landroid/widget/Spinner;

.field private aoQ:Landroid/widget/ImageView;

.field aoR:Lcom/android/settings/wifi/J;

.field protected aoS:Landroid/widget/Spinner;

.field private aoT:Ljava/util/ArrayList;

.field protected aoU:Landroid/widget/TextView;

.field protected aoV:Landroid/net/StaticIpConfiguration;

.field private aoW:Landroid/telephony/SubscriptionManager;

.field private aoX:Landroid/telephony/TelephonyManager;

.field private final aoY:Landroid/os/Handler;

.field private aoZ:Ljava/lang/String;

.field private final aog:Lcom/android/settingslib/wifi/i;

.field protected aoh:I

.field protected final aoi:Lcom/android/settings/wifi/G;

.field private aoj:Landroid/widget/TextView;

.field private aok:Landroid/widget/TextView;

.field private aol:Ljava/lang/String;

.field private aom:Ljava/lang/String;

.field private aon:Landroid/widget/TextView;

.field private aoo:Landroid/widget/Spinner;

.field private aop:Landroid/widget/TextView;

.field private aoq:Landroid/widget/TextView;

.field private aor:Landroid/widget/Spinner;

.field private aos:Landroid/widget/Spinner;

.field private aot:Landroid/widget/TextView;

.field private aou:Z

.field protected aov:Landroid/net/ProxyInfo;

.field private aow:Landroid/widget/TextView;

.field protected aox:Landroid/net/IpConfiguration$IpAssignment;

.field protected aoy:Landroid/widget/Spinner;

.field private aoz:Z

.field private apa:Ljava/lang/String;

.field private final apb:Landroid/view/View;

.field private apc:Landroid/widget/Spinner;

.field private apd:Landroid/widget/Spinner;

.field private ape:I

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/wifi/A;->aof:[I

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/android/settingslib/wifi/i;)V
    .locals 5

    const v4, 0x1090008

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/android/settings/wifi/A;->aou:Z

    sget-object v0, Landroid/net/IpConfiguration$IpAssignment;->UNASSIGNED:Landroid/net/IpConfiguration$IpAssignment;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aox:Landroid/net/IpConfiguration$IpAssignment;

    sget-object v0, Landroid/net/IpConfiguration$ProxySettings;->UNASSIGNED:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v1, p0, Lcom/android/settings/wifi/A;->aov:Landroid/net/ProxyInfo;

    iput-object v1, p0, Lcom/android/settings/wifi/A;->aoV:Landroid/net/StaticIpConfiguration;

    iput-object v1, p0, Lcom/android/settings/wifi/A;->aoW:Landroid/telephony/SubscriptionManager;

    iput-object p1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    iput-object p2, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    iput-object v1, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoY:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoX:Landroid/telephony/TelephonyManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoT:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121626

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoZ:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121564

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoC:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121628

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->apa:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121526

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aol:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121527

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aom:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a03cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    const-string/jumbo v0, "support_wapi"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0298

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a021a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a036c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a03ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0317

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a00c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a04dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a01f5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoq:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a005a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aon:Landroid/widget/TextView;

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f030132

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoH:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoH:Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f030134

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoG:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoG:Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/wifi/G;Landroid/view/View;Lcom/android/settingslib/wifi/i;I)V
    .locals 11

    const v8, 0x1090008

    const/16 v5, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/android/settings/wifi/A;->aou:Z

    sget-object v0, Landroid/net/IpConfiguration$IpAssignment;->UNASSIGNED:Landroid/net/IpConfiguration$IpAssignment;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aox:Landroid/net/IpConfiguration$IpAssignment;

    sget-object v0, Landroid/net/IpConfiguration$ProxySettings;->UNASSIGNED:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v6, p0, Lcom/android/settings/wifi/A;->aov:Landroid/net/ProxyInfo;

    iput-object v6, p0, Lcom/android/settings/wifi/A;->aoV:Landroid/net/StaticIpConfiguration;

    iput-object v6, p0, Lcom/android/settings/wifi/A;->aoW:Landroid/telephony/SubscriptionManager;

    iput-object p1, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    iput-object p2, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    iput-object p3, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-nez p3, :cond_4

    move v0, v2

    :goto_0
    iput v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    iput p4, p0, Lcom/android/settings/wifi/A;->aoB:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoY:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoX:Landroid/telephony/TelephonyManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoT:Ljava/util/ArrayList;

    const v0, 0x7f030139

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoA:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1120053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v4, 0x7f030132

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoH:Landroid/widget/ArrayAdapter;

    :goto_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoH:Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v4, 0x7f030134

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoG:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoG:Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121626

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoZ:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121564

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoC:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121628

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->apa:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121526

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aol:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v1, 0x7f121527

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aom:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a021a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a036c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v0, 0x7f0a0249

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoQ:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoQ:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    const v1, 0x7f1214c2

    invoke-interface {v0, v1}, Lcom/android/settings/wifi/G;->setTitle(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0425

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoU:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoU:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a03cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    const-string/jumbo v0, "support_wapi"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0298

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    sget v1, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a04c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afB()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afD()V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a051b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a051c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    const v1, 0x7f121595

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/wifi/G;->adC(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a051c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->isSplitSystemUser()Z

    move-result v0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    const v1, 0x7f1214e4

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/wifi/G;->adA(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->ady()Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->aeS()V

    :cond_3
    return-void

    :cond_4
    invoke-virtual {p3}, Lcom/android/settingslib/wifi/i;->chP()I

    move-result v0

    goto/16 :goto_0

    :cond_5
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v4, 0x7f030132

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoH:Landroid/widget/ArrayAdapter;

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chZ()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chW()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/wifi/G;->setTitle(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a020a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v1

    if-eqz v1, :cond_20

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->getIpAssignment()Landroid/net/IpConfiguration$IpAssignment;

    move-result-object v1

    sget-object v8, Landroid/net/IpConfiguration$IpAssignment;->STATIC:Landroid/net/IpConfiguration$IpAssignment;

    if-ne v1, v8, :cond_d

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v1, v3}, Landroid/widget/Spinner;->setSelection(I)V

    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->getStaticIpConfiguration()Landroid/net/StaticIpConfiguration;

    move-result-object v1

    if-eqz v1, :cond_c

    iget-object v8, v1, Landroid/net/StaticIpConfiguration;->ipAddress:Landroid/net/LinkAddress;

    if-eqz v8, :cond_c

    iget-object v1, v1, Landroid/net/StaticIpConfiguration;->ipAddress:Landroid/net/LinkAddress;

    invoke-virtual {v1}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    const v8, 0x7f12154c

    invoke-direct {p0, v0, v8, v1}, Lcom/android/settings/wifi/A;->aeV(Landroid/view/ViewGroup;ILjava/lang/String;)V

    move v1, v3

    :goto_4
    iget-boolean v8, v4, Landroid/net/wifi/WifiConfiguration;->shared:Z

    if-nez v8, :cond_7

    move v1, v3

    :cond_7
    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->getProxySettings()Landroid/net/IpConfiguration$ProxySettings;

    move-result-object v8

    sget-object v9, Landroid/net/IpConfiguration$ProxySettings;->STATIC:Landroid/net/IpConfiguration$ProxySettings;

    if-ne v8, v9, :cond_e

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v1, v3}, Landroid/widget/Spinner;->setSelection(I)V

    move v1, v3

    :goto_5
    if-eqz v4, :cond_10

    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->isPasspoint()Z

    move-result v8

    if-eqz v8, :cond_1f

    iget-object v8, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v9, 0x7f120c77

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v3, [Ljava/lang/Object;

    iget-object v4, v4, Landroid/net/wifi/WifiConfiguration;->providerFriendlyName:Ljava/lang/String;

    aput-object v4, v9, v2

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const v8, 0x7f120c78

    invoke-direct {p0, v0, v8, v4}, Lcom/android/settings/wifi/A;->aeV(Landroid/view/ViewGroup;ILjava/lang/String;)V

    move v4, v1

    :goto_6
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chM()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chZ()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_9

    :cond_8
    iget v1, p0, Lcom/android/settings/wifi/A;->aoB:I

    if-eqz v1, :cond_a

    :cond_9
    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afE()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afB()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afD()V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v8, 0x7f0a051c

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iget-object v8, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v9, 0x7f0a051b

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v8, 0x7f0a051a

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    if-eqz v4, :cond_11

    move v1, v2

    :goto_7
    invoke-virtual {v8, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_a
    iget v1, p0, Lcom/android/settings/wifi/A;->aoB:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_12

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    const v1, 0x7f121595

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/wifi/G;->adC(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_b
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chx()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/wifi/G;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_c
    move v1, v3

    goto/16 :goto_4

    :cond_d
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    move v1, v2

    goto/16 :goto_4

    :cond_e
    invoke-virtual {v4}, Landroid/net/wifi/WifiConfiguration;->getProxySettings()Landroid/net/IpConfiguration$ProxySettings;

    move-result-object v8

    sget-object v9, Landroid/net/IpConfiguration$ProxySettings;->PAC:Landroid/net/IpConfiguration$ProxySettings;

    if-ne v8, v9, :cond_f

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    const/4 v8, 0x2

    invoke-virtual {v1, v8}, Landroid/widget/Spinner;->setSelection(I)V

    move v1, v3

    goto/16 :goto_5

    :cond_f
    iget-object v8, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v8, v2}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_5

    :cond_10
    move v4, v1

    goto/16 :goto_6

    :cond_11
    move v1, v5

    goto :goto_7

    :cond_12
    iget v1, p0, Lcom/android/settings/wifi/A;->aoB:I

    if-ne v1, v3, :cond_13

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    const v1, 0x7f1214ef

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/wifi/G;->adC(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_13
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chY()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->getSignalString()Ljava/lang/String;

    move-result-object v8

    if-eqz v4, :cond_14

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v4, v1, :cond_16

    :cond_14
    if-eqz v8, :cond_16

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    const v1, 0x7f1214ef

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/wifi/G;->adC(Ljava/lang/CharSequence;)V

    :goto_8
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chM()Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chZ()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_15
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    const v1, 0x7f12153c

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settings/wifi/G;->adB(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_16
    if-eqz v4, :cond_17

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cib()Z

    move-result v9

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration;->isPasspoint()Z

    move-result v10

    if-eqz v10, :cond_1e

    iget-object v1, v1, Landroid/net/wifi/WifiConfiguration;->providerFriendlyName:Ljava/lang/String;

    :goto_9
    iget-object v10, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v10}, Lcom/android/settings/wifi/G;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v4, v9, v1}, Lcom/android/settingslib/wifi/i;->chC(Landroid/content/Context;Landroid/net/NetworkInfo$DetailedState;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f12160d

    invoke-direct {p0, v0, v4, v1}, Lcom/android/settings/wifi/A;->aeV(Landroid/view/ViewGroup;ILjava/lang/String;)V

    :cond_17
    if-eqz v8, :cond_18

    const v1, 0x7f121601

    invoke-direct {p0, v0, v1, v8}, Lcom/android/settings/wifi/A;->aeV(Landroid/view/ViewGroup;ILjava/lang/String;)V

    :cond_18
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chk()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v4

    const/4 v8, -0x1

    if-eq v4, v8, :cond_19

    const v4, 0x7f120912

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f121603

    invoke-direct {p0, v0, v4, v3}, Lcom/android/settings/wifi/A;->aeV(Landroid/view/ViewGroup;ILjava/lang/String;)V

    :cond_19
    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1a

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v1

    const/16 v3, 0x960

    if-lt v1, v3, :cond_1c

    const/16 v3, 0x9c4

    if-ge v1, v3, :cond_1c

    const v1, 0x7f1214d9

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_a
    if-eqz v6, :cond_1a

    const v1, 0x7f12153d

    invoke-direct {p0, v0, v1, v6}, Lcom/android/settings/wifi/A;->aeV(Landroid/view/ViewGroup;ILjava/lang/String;)V

    :cond_1a
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1, v2}, Lcom/android/settingslib/wifi/i;->chV(Z)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f1215a3

    invoke-direct {p0, v0, v3, v1}, Lcom/android/settings/wifi/A;->aeV(Landroid/view/ViewGroup;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0219

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8

    :cond_1b
    move-object v1, v6

    goto/16 :goto_9

    :cond_1c
    const/16 v3, 0x1324

    if-lt v1, v3, :cond_1d

    const/16 v3, 0x170c

    if-ge v1, v3, :cond_1d

    const v1, 0x7f1214da

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_a

    :cond_1d
    const-string/jumbo v3, "WifiConfigController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Unexpected frequency "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    :cond_1e
    move-object v1, v6

    goto/16 :goto_9

    :cond_1f
    move v4, v1

    goto/16 :goto_6

    :cond_20
    move v4, v2

    goto/16 :goto_6
.end method

.method private aeV(Landroid/view/ViewGroup;ILjava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0272

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x1020016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x1020010

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private static aeW(Ljava/lang/String;)I
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_5

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x30

    if-lt v1, v2, :cond_1

    const/16 v2, 0x39

    if-le v1, v2, :cond_4

    :cond_1
    const/16 v2, 0x41

    if-lt v1, v2, :cond_2

    const/16 v2, 0x46

    if-le v1, v2, :cond_4

    :cond_2
    const/16 v2, 0x61

    if-lt v1, v2, :cond_3

    const/16 v2, 0x66

    if-le v1, v2, :cond_4

    :cond_3
    const/4 v0, -0x2

    return v0

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_5
    return v3
.end method

.method private aeY(Ljava/lang/String;I)Z
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12149b

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return v3

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x40

    if-le v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12149e

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return v3

    :cond_2
    if-ne p2, v2, :cond_4

    invoke-static {p1}, Lcom/android/settings/wifi/A;->aeW(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x2

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12149c

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return v3

    :cond_3
    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f12149d

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return v3

    :cond_4
    return v2
.end method

.method private afA(I)V
    .locals 9

    const v4, 0x7f0a0248

    const v8, 0x7f0a0240

    const v3, 0x7f0a023f

    const/16 v7, 0x8

    const/4 v6, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/A;->afG(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0244

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0242

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0241

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afs()V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0401

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afw()V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afp()V

    :cond_3
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afu()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afn()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afp()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afm()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afy()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afw()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a024b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afu()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afm()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afr()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afw()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoF:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoH:Landroid/widget/ArrayAdapter;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoH:Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoF:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoF:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afC()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afy()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afw()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoF:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoG:Landroid/widget/ArrayAdapter;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoG:Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoF:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoF:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_5
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afy()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afw()V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    sget v4, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoT:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/android/settings/wifi/A;->aoT:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v2, v3, v4, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    sget v0, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a024a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/settings/wifi/A;->ape:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_6
    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afu()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afm()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afn()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afp()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afy()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->aft()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afq()V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afr()V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoR:Lcom/android/settings/wifi/J;

    invoke-virtual {v0}, Lcom/android/settings/wifi/J;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afx()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private afC()V
    .locals 4

    const v3, 0x7f0a0242

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoq:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->aft()V

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a023f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private afG(I)I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "CMCC"

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    iget-object v1, v1, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x4

    :cond_0
    return p1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private afd(Ljava/lang/String;)Ljava/net/Inet4Address;
    .locals 1

    :try_start_0
    invoke-static {p1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    check-cast v0, Ljava/net/Inet4Address;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return-object v0
.end method

.method private aff()V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoW:Landroid/telephony/SubscriptionManager;

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoX:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoW:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v2, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoForSimSlotIndex(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v3, p0, Lcom/android/settings/wifi/A;->aoT:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    add-int/lit8 v4, v0, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const v4, 0x7f1210a0

    invoke-virtual {v2, v4, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    return-void
.end method

.method private afi()Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    const/16 v1, 0x40

    if-le v0, v1, :cond_1

    :cond_0
    return v3

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/settings/wifi/A;->aof:[I

    iget-object v2, p0, Lcom/android/settings/wifi/A;->apd:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    aget v1, v1, v2

    if-ne v1, v4, :cond_3

    const-string/jumbo v1, "WifiConfigController"

    const-string/jumbo v2, "isWapiPskValid hex mode"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_2

    const-string/jumbo v1, "[0-9A-Fa-f]*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    :cond_2
    return v3

    :cond_3
    return v4
.end method

.method private afj(Ljava/lang/String;I)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    if-gtz p2, :cond_1

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x5

    if-eq p2, v0, :cond_2

    const/16 v0, 0xd

    if-ne p2, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    return v0

    :cond_3
    const/16 v0, 0x10

    if-eq p2, v0, :cond_2

    const/16 v0, 0xa

    if-eq p2, v0, :cond_4

    const/16 v0, 0x1a

    if-ne p2, v0, :cond_6

    :cond_4
    :goto_0
    const-string/jumbo v0, "[0-9A-Fa-f]*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_5
    return v1

    :cond_6
    const/16 v0, 0x20

    if-ne p2, v0, :cond_5

    goto :goto_0
.end method

.method private afk(Landroid/widget/Spinner;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 5

    invoke-virtual {p1}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoZ:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_0

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoC:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p5, :cond_1

    iget-object v2, p0, Lcom/android/settings/wifi/A;->apa:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v2

    const/16 v3, 0x3f2

    invoke-virtual {v2, p2, v3}, Landroid/security/KeyStore;->list(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Landroid/widget/ArrayAdapter;

    sget v3, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v2, v1, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    sget v0, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method private afl(Landroid/widget/Spinner;)V
    .locals 9

    const/16 v8, 0x3f2

    const/4 v7, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->getContext()Landroid/content/Context;

    move-result-object v3

    const v0, 0x7f121626

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f121493

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v0, "mediatek"

    const-string/jumbo v2, "vendor"

    invoke-static {v2}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v0

    const-string/jumbo v2, "WAPI_CACERT_"

    invoke-virtual {v0, v2, v8}, Landroid/security/KeyStore;->list(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_0

    array-length v0, v2

    if-gtz v0, :cond_3

    :cond_0
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v7, :cond_4

    iput-boolean v7, p0, Lcom/android/settings/wifi/A;->aou:Z

    :goto_1
    new-instance v2, Landroid/widget/ArrayAdapter;

    new-array v0, v1, [Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const v1, 0x1090008

    invoke-direct {v2, v3, v1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v0, 0x1090009

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void

    :cond_2
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v0

    const-string/jumbo v2, "WAPI_USER_"

    invoke-virtual {v0, v2, v8}, Landroid/security/KeyStore;->list(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    :goto_2
    array-length v4, v2

    if-ge v0, v4, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iput-boolean v1, p0, Lcom/android/settings/wifi/A;->aou:Z

    goto :goto_1
.end method

.method private afm()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a023f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aon:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private afn()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0240

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoZ:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/A;->afv(Landroid/widget/Spinner;Ljava/lang/String;)V

    return-void
.end method

.method private afo(Lcom/android/settingslib/wifi/i;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, -0x1

    if-eqz p1, :cond_2

    const-string/jumbo v0, "SG"

    invoke-static {v0}, Lmiui/os/Build;->checkRegion(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    const-string/jumbo v2, "Singtel"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gt v0, v4, :cond_0

    iget-object v0, p1, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    const-string/jumbo v2, "Wireless@SGx"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-le v0, v4, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    const-string/jumbo v2, "SIM"

    iget-object v3, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_1
    iget-object v0, p1, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    const-string/jumbo v2, "Singtel"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-le v0, v4, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private afp()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0241

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aop:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private afq()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0242

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method private afr()V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0246

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0401

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private afs()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0246

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0401

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private aft()V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a030b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0401

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private afu()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0248

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method private afv(Landroid/widget/Spinner;Ljava/lang/String;)V
    .locals 3

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method private afw()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a024a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private afx()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a024a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private afy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a024b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoZ:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/A;->afv(Landroid/widget/Spinner;Ljava/lang/String;)V

    return-void
.end method

.method private afz(II)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method aeR()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->ady()Landroid/widget/Button;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method aeS()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->ady()Landroid/widget/Button;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afh()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method aeT()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->adx()Landroid/widget/Button;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method public aeU()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0306

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a03ff

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x90

    :goto_0
    or-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setInputType(I)V

    return-void

    :cond_0
    const/16 v1, 0x80

    goto :goto_0
.end method

.method public aeX()Z
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f121625

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    const/16 v2, 0xb

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/wifi/A;->aou:Z

    if-nez v0, :cond_4

    const/4 v0, 0x0

    return v0

    :cond_0
    iget v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    const/16 v2, 0xa

    if-ne v0, v2, :cond_3

    sget-object v0, Lcom/android/settings/wifi/A;->aof:[I

    iget-object v2, p0, Lcom/android/settings/wifi/A;->apd:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    aget v2, v0, v2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v4

    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_2
    invoke-direct {p0, v3, v2}, Lcom/android/settings/wifi/A;->aeY(Ljava/lang/String;I)Z

    move-result v0

    return v0

    :cond_3
    return v4

    :cond_4
    return v4
.end method

.method public aeZ()Lcom/android/settingslib/wifi/i;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    return-object v0
.end method

.method protected afB()V
    .locals 5

    const v4, 0x7f0a042a

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0219

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aow:Landroid/widget/TextView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a021c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aow:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aow:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a01c3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aot:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aot:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a02bd

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoD:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoD:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0146

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoj:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoj:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0147

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aok:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aok:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_0
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration;->getStaticIpConfiguration()Landroid/net/StaticIpConfiguration;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v1, v0, Landroid/net/StaticIpConfiguration;->ipAddress:Landroid/net/LinkAddress;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aow:Landroid/widget/TextView;

    iget-object v2, v0, Landroid/net/StaticIpConfiguration;->ipAddress:Landroid/net/LinkAddress;

    invoke-virtual {v2}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoD:Landroid/widget/TextView;

    iget-object v2, v0, Landroid/net/StaticIpConfiguration;->ipAddress:Landroid/net/LinkAddress;

    invoke-virtual {v2}, Landroid/net/LinkAddress;->getNetworkPrefixLength()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, v0, Landroid/net/StaticIpConfiguration;->gateway:Ljava/net/InetAddress;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aot:Landroid/widget/TextView;

    iget-object v2, v0, Landroid/net/StaticIpConfiguration;->gateway:Ljava/net/InetAddress;

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, v0, Landroid/net/StaticIpConfiguration;->dnsServers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoj:Landroid/widget/TextView;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aok:Landroid/widget/TextView;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    move-object v1, v0

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chv()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getDhcpInfo()Landroid/net/DhcpInfo;

    move-result-object v0

    if-eqz v0, :cond_4

    iget v1, v0, Landroid/net/DhcpInfo;->ipAddress:I

    invoke-static {v1}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    iget v0, v0, Landroid/net/DhcpInfo;->gateway:I

    invoke-static {v0}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aow:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aot:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_8
    move-object v1, v0

    goto/16 :goto_0
.end method

.method protected afD()V
    .locals 7

    const v6, 0x7f0a036a

    const v5, 0x7f0a0367

    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a036d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    const v0, 0x7f0a036f

    invoke-direct {p0, v0, v4}, Lcom/android/settings/wifi/A;->afz(II)V

    invoke-direct {p0, v5, v4}, Lcom/android/settings/wifi/A;->afz(II)V

    invoke-direct {p0, v6, v3}, Lcom/android/settings/wifi/A;->afz(II)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoK:Landroid/widget/TextView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0368

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoK:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoK:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a036b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoM:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoM:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0366

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoJ:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoJ:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoK:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/net/ProxyInfo;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoM:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/net/ProxyInfo;->getPort()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoJ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/net/ProxyInfo;->getExclusionListAsString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_5

    const v0, 0x7f0a036f

    invoke-direct {p0, v0, v3}, Lcom/android/settings/wifi/A;->afz(II)V

    invoke-direct {p0, v5, v3}, Lcom/android/settings/wifi/A;->afz(II)V

    invoke-direct {p0, v6, v4}, Lcom/android/settings/wifi/A;->afz(II)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoL:Landroid/widget/TextView;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a0369

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoL:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoL:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_4
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoL:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/net/ProxyInfo;->getPacFileUrl()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_5
    const v0, 0x7f0a036f

    invoke-direct {p0, v0, v3}, Lcom/android/settings/wifi/A;->afz(II)V

    invoke-direct {p0, v5, v3}, Lcom/android/settings/wifi/A;->afz(II)V

    invoke-direct {p0, v6, v3}, Lcom/android/settings/wifi/A;->afz(II)V

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method protected afE()V
    .locals 15

    const v14, 0x7f0a030b

    const/4 v5, 0x1

    const/16 v13, 0x8

    const/4 v4, 0x0

    iget v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a03cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a03cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0306

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a03ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    const v1, 0x7f121625

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(I)V

    :cond_1
    iget v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0509

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0401

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a050a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->apd:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apd:Landroid/widget/Spinner;

    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->wapiPskType:I

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apd:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_3
    :goto_0
    iget v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a015d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0306

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    return-void

    :cond_5
    iget v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0401

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0505

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0508

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->apc:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apc:Landroid/widget/Spinner;

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/A;->afl(Landroid/widget/Spinner;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    const-string/jumbo v1, "WifiConfigController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Read WAPI_CERT sel mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/net/wifi/WifiConfiguration;->wapiCertSelMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, v0, Landroid/net/wifi/WifiConfiguration;->wapiCertSelMode:I

    if-nez v1, :cond_6

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apc:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v1, "WifiConfigController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Read WAPI_CERT sel cert name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->wapiCertSel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apc:Landroid/widget/Spinner;

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->wapiCertSel:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/android/settings/wifi/A;->afv(Landroid/widget/Spinner;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0509

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0505

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0401

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a015d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0245

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_d

    const-string/jumbo v0, "CMCC"

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    iget-object v1, v1, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v12, v0

    :goto_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    if-nez v0, :cond_1b

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->aff()V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0297

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1120053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    sget v3, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    sget v0, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_a
    const-string/jumbo v0, "support_eap_sim"

    invoke-static {v0, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    sget v3, Lmiui/R$layout;->simple_spinner_dropdown_item:I

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    if-eqz v12, :cond_e

    const v0, 0x7f03012c

    :goto_2
    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_b
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0317

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a00c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a014d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aop:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aop:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a04dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0408

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a01f5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoq:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a005a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aon:Landroid/widget/TextView;

    new-instance v0, Lcom/android/settings/wifi/J;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/wifi/J;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoR:Lcom/android/settings/wifi/J;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v2, :cond_c

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/settingslib/wifi/i;->cij(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    iget-object v2, v2, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/settingslib/wifi/i;->cij(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    :cond_d
    move v12, v4

    goto/16 :goto_1

    :cond_e
    const v0, 0x7f03012b

    goto/16 :goto_2

    :cond_f
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoR:Lcom/android/settings/wifi/J;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoR:Lcom/android/settings/wifi/J;

    invoke-virtual {v0}, Lcom/android/settings/wifi/J;->getCount()I

    move-result v0

    if-gt v0, v5, :cond_14

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_10
    :goto_4
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    const-string/jumbo v2, "CACERT_"

    iget-object v3, p0, Lcom/android/settings/wifi/A;->aom:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/wifi/A;->afk(Landroid/widget/Spinner;Ljava/lang/String;Ljava/lang/String;ZZ)V

    iget-object v7, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    const-string/jumbo v8, "USRPKEY_"

    iget-object v9, p0, Lcom/android/settings/wifi/A;->aol:Ljava/lang/String;

    move-object v6, p0

    move v10, v4

    move v11, v4

    invoke-direct/range {v6 .. v11}, Lcom/android/settings/wifi/A;->afk(Landroid/widget/Spinner;Ljava/lang/String;Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getEapMethod()I

    move-result v1

    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getPhase2Method()I

    move-result v3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    if-lt v1, v0, :cond_1e

    add-int/lit8 v0, v0, -0x1

    :goto_5
    iget-object v6, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v6, v0}, Landroid/widget/Spinner;->setSelection(I)V

    invoke-direct {p0, v1}, Lcom/android/settings/wifi/A;->afA(I)V

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    :goto_6
    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apa:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/A;->afv(Landroid/widget/Spinner;Ljava/lang/String;)V

    :goto_7
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aop:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getDomainSuffixMatch()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getClientCertificateAlias()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_19

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aol:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/A;->afv(Landroid/widget/Spinner;Ljava/lang/String;)V

    :goto_8
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoq:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getIdentity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aon:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getAnonymousIdentity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_9
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/A;->afo(Lcom/android/settingslib/wifi/i;)V

    :goto_a
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    iget-object v0, v0, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    iget-object v0, v0, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    const-string/jumbo v1, "MIOffice"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_1c

    :goto_b
    if-eqz v5, :cond_11

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_11
    if-nez v5, :cond_12

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v0, :cond_13

    const-string/jumbo v0, "CMCC-AUTO"

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    iget-object v1, v1, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    if-eqz v12, :cond_13

    :cond_12
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0244

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v12, :cond_1d

    :goto_c
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0248

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0240

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a024b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a023f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    :cond_13
    return-void

    :cond_14
    move v0, v4

    :goto_d
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoR:Lcom/android/settings/wifi/J;

    invoke-virtual {v1}, Lcom/android/settings/wifi/J;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_10

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoR:Lcom/android/settings/wifi/J;

    invoke-virtual {v1, v0}, Lcom/android/settings/wifi/J;->getItemId(I)J

    move-result-wide v2

    int-to-long v6, v4

    cmp-long v1, v2, v6

    if-nez v1, :cond_15

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_4

    :cond_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :pswitch_1
    packed-switch v3, :pswitch_data_1

    :pswitch_2
    const-string/jumbo v0, "WifiConfigController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Invalid phase 2 method "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_6

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_6

    :pswitch_5
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_6

    :pswitch_6
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_6

    :pswitch_7
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_6

    :pswitch_8
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_6

    :pswitch_9
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/settings/wifi/A;->ape:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_6

    :cond_16
    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaCertificateAliases()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aom:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/A;->afv(Landroid/widget/Spinner;Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_17
    array-length v1, v0

    if-ne v1, v5, :cond_18

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    aget-object v0, v0, v4

    invoke-direct {p0, v1, v0}, Lcom/android/settings/wifi/A;->afv(Landroid/widget/Spinner;Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_18
    iget-object v7, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    const-string/jumbo v8, "CACERT_"

    iget-object v9, p0, Lcom/android/settings/wifi/A;->aom:Ljava/lang/String;

    move-object v6, p0

    move v10, v5

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/android/settings/wifi/A;->afk(Landroid/widget/Spinner;Ljava/lang/String;Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoC:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/A;->afv(Landroid/widget/Spinner;Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_19
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    invoke-direct {p0, v1, v0}, Lcom/android/settings/wifi/A;->afv(Landroid/widget/Spinner;Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_1a
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0317

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/A;->afA(I)V

    goto/16 :goto_9

    :cond_1b
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/A;->afA(I)V

    goto/16 :goto_a

    :cond_1c
    move v5, v4

    goto/16 :goto_b

    :cond_1d
    move v4, v13

    goto/16 :goto_c

    :cond_1e
    move v0, v1

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method afF()V
    .locals 6

    const v5, 0x7f0a02dd

    const v4, 0x7f0a02dc

    const/4 v3, 0x0

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0240

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/wifi/A;->apa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aop:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v1, 0x7f0a0241

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aop:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method protected afH(Landroid/net/StaticIpConfiguration;)I
    .locals 8

    const v7, 0x7f121550

    const v6, 0x7f12154f

    const v3, 0x7f121551

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aow:Landroid/widget/TextView;

    if-nez v0, :cond_0

    return v5

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aow:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v3

    :cond_1
    invoke-direct {p0, v0}, Lcom/android/settings/wifi/A;->afd(Ljava/lang/String;)Ljava/net/Inet4Address;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v0, Ljava/net/Inet4Address;->ANY:Ljava/net/InetAddress;

    invoke-virtual {v1, v0}, Ljava/net/Inet4Address;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    return v3

    :cond_3
    const/4 v0, -0x1

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoD:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_4

    const/16 v2, 0x20

    if-le v0, v2, :cond_5

    :cond_4
    const v0, 0x7f121552

    return v0

    :cond_5
    new-instance v2, Landroid/net/LinkAddress;

    invoke-direct {v2, v1, v0}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object v2, p1, Landroid/net/StaticIpConfiguration;->ipAddress:Landroid/net/LinkAddress;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/wifi/A;->aot:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    :try_start_1
    invoke-static {v1, v0}, Landroid/net/NetworkUtils;->getNetworkPart(Ljava/net/InetAddress;I)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aot:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoj:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoj:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v2, 0x7f121523

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aok:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_c

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aok:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/A;->afd(Ljava/lang/String;)Ljava/net/Inet4Address;

    move-result-object v0

    if-nez v0, :cond_b

    return v6

    :catch_0
    move-exception v0

    return v3

    :catch_1
    move-exception v2

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoD:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const v4, 0x7f121566

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_6
    invoke-direct {p0, v2}, Lcom/android/settings/wifi/A;->afd(Ljava/lang/String;)Ljava/net/Inet4Address;

    move-result-object v0

    if-nez v0, :cond_7

    return v7

    :cond_7
    invoke-virtual {v0}, Ljava/net/InetAddress;->isMulticastAddress()Z

    move-result v1

    if-eqz v1, :cond_8

    return v7

    :cond_8
    iput-object v0, p1, Landroid/net/StaticIpConfiguration;->gateway:Ljava/net/InetAddress;

    goto :goto_1

    :cond_9
    invoke-direct {p0, v0}, Lcom/android/settings/wifi/A;->afd(Ljava/lang/String;)Ljava/net/Inet4Address;

    move-result-object v0

    if-nez v0, :cond_a

    return v6

    :cond_a
    iget-object v1, p1, Landroid/net/StaticIpConfiguration;->dnsServers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_b
    iget-object v1, p1, Landroid/net/StaticIpConfiguration;->dnsServers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    return v5

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method afa()Landroid/net/wifi/WifiConfiguration;
    .locals 1

    iget v0, p0, Lcom/android/settings/wifi/A;->aoB:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/A;->afb(Z)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    return-object v0
.end method

.method protected afb(Z)Landroid/net/wifi/WifiConfiguration;
    .locals 10

    const/4 v9, 0x3

    const/16 v4, 0x22

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x0

    new-instance v2, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v2}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/wifi/i;->chG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iput-boolean v8, v2, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    :goto_0
    if-eqz p1, :cond_2

    iget v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cih()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/wifi/i;->chG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    iput v0, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    goto :goto_0

    :pswitch_1
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v7}, Ljava/util/BitSet;->set(I)V

    :cond_2
    :goto_1
    new-instance v0, Landroid/net/IpConfiguration;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aox:Landroid/net/IpConfiguration$IpAssignment;

    iget-object v3, p0, Lcom/android/settings/wifi/A;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    iget-object v4, p0, Lcom/android/settings/wifi/A;->aoV:Landroid/net/StaticIpConfiguration;

    iget-object v5, p0, Lcom/android/settings/wifi/A;->aov:Landroid/net/ProxyInfo;

    invoke-direct {v0, v1, v3, v4, v5}, Landroid/net/IpConfiguration;-><init>(Landroid/net/IpConfiguration$IpAssignment;Landroid/net/IpConfiguration$ProxySettings;Landroid/net/StaticIpConfiguration;Landroid/net/ProxyInfo;)V

    invoke-virtual {v2, v0}, Landroid/net/wifi/WifiConfiguration;->setIpConfiguration(Landroid/net/IpConfiguration;)V

    return-object v2

    :pswitch_2
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v7}, Ljava/util/BitSet;->set(I)V

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v0, v7}, Ljava/util/BitSet;->set(I)V

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v0, v8}, Ljava/util/BitSet;->set(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0xa

    if-eq v0, v3, :cond_3

    const/16 v3, 0x1a

    if-ne v0, v3, :cond_4

    :cond_3
    const-string/jumbo v0, "[0-9A-Fa-f]*"

    invoke-virtual {v1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aput-object v1, v0, v7

    goto :goto_1

    :cond_4
    const/16 v3, 0x20

    if-eq v0, v3, :cond_3

    :cond_5
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    goto :goto_1

    :pswitch_3
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v8}, Ljava/util/BitSet;->set(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "[0-9A-Fa-f]{64}"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    iput-object v0, v2, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->set(I)V

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v0, v9}, Ljava/util/BitSet;->set(I)V

    new-instance v0, Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-direct {v0}, Landroid/net/wifi/WifiEnterpriseConfig;-><init>()V

    iput-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/A;->afG(I)I

    move-result v3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoI:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v4, v3}, Landroid/net/wifi/WifiEnterpriseConfig;->setEapMethod(I)V

    packed-switch v3, :pswitch_data_1

    :pswitch_5
    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v4, v0}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    :goto_2
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v4, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaCertificateAliases([Ljava/lang/String;)V

    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v4, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaPath(Ljava/lang/String;)V

    iget-object v1, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v4, p0, Lcom/android/settings/wifi/A;->aop:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setDomainSuffixMatch(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    :cond_7
    :goto_3
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaCertificateAliases()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "WifiConfigController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "ca_cert ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v4}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaCertificateAliases()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ") and ca_path ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v4}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ") should not both be non-null"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aos:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aol:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_9
    const-string/jumbo v0, ""

    :cond_a
    iget-object v1, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v1, v0}, Landroid/net/wifi/WifiEnterpriseConfig;->setClientCertificateAlias(Ljava/lang/String;)V

    const/4 v0, 0x4

    if-eq v3, v0, :cond_b

    const/4 v0, 0x5

    if-ne v3, v0, :cond_10

    :cond_b
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setIdentity(Ljava/lang/String;)V

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setAnonymousIdentity(Ljava/lang/String;)V

    :goto_4
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setPassword(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_6
    packed-switch v0, :pswitch_data_2

    const-string/jumbo v4, "WifiConfigController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Unknown phase2 method"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :pswitch_7
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0, v7}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_2

    :pswitch_8
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v0, v9}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_2

    :pswitch_9
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_2

    :pswitch_a
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_2

    :pswitch_b
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_2

    :pswitch_c
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const/4 v4, 0x7

    invoke-virtual {v0, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto/16 :goto_2

    :pswitch_d
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/wifi/A;->ape:I

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v2, v0}, Lcom/android/settings/dc;->bsh(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;I)V

    goto/16 :goto_2

    :cond_c
    iget-object v1, p0, Lcom/android/settings/wifi/A;->apa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const-string/jumbo v1, "/system/etc/security/cacerts"

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaPath(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_d
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v0

    if-nez v0, :cond_e

    const-string/jumbo v0, "WifiConfigController"

    const-string/jumbo v1, "Multiple certs can only be set when editing saved network"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chL()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    iget-object v1, v1, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v1}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaCertificateAliases()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaCertificateAliases([Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_f
    iget-object v1, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    new-array v4, v8, [Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-virtual {v1, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaCertificateAliases([Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_10
    const/4 v0, 0x6

    if-eq v3, v0, :cond_b

    if-ne v3, v9, :cond_11

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoq:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setIdentity(Ljava/lang/String;)V

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setAnonymousIdentity(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_11
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoq:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setIdentity(Ljava/lang/String;)V

    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aon:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setAnonymousIdentity(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_12
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setPassword(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_e
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    sget-object v1, Lcom/android/settings/wifi/A;->aof:[I

    iget-object v3, p0, Lcom/android/settings/wifi/A;->apd:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    aget v1, v1, v3

    iget-object v3, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/android/settings/dc;->bsl(Landroid/net/wifi/WifiConfiguration;ILjava/lang/String;)V

    goto/16 :goto_1

    :pswitch_f
    iget-object v0, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->set(I)V

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apc:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-nez v0, :cond_13

    move-object v0, v1

    :goto_5
    invoke-virtual {v3, v2, v0}, Lcom/android/settings/dc;->bsk(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_13
    iget-object v0, p0, Lcom/android/settings/wifi/A;->apc:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_d
        :pswitch_d
        :pswitch_d
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public afc()I
    .locals 1

    iget v0, p0, Lcom/android/settings/wifi/A;->aoh:I

    return v0
.end method

.method public afe()I
    .locals 1

    iget v0, p0, Lcom/android/settings/wifi/A;->aoB:I

    return v0
.end method

.method protected afg()Z
    .locals 7

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v5, :cond_0

    sget-object v0, Landroid/net/IpConfiguration$IpAssignment;->STATIC:Landroid/net/IpConfiguration$IpAssignment;

    :goto_0
    iput-object v0, p0, Lcom/android/settings/wifi/A;->aox:Landroid/net/IpConfiguration$IpAssignment;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aox:Landroid/net/IpConfiguration$IpAssignment;

    sget-object v2, Landroid/net/IpConfiguration$IpAssignment;->STATIC:Landroid/net/IpConfiguration$IpAssignment;

    if-ne v0, v2, :cond_1

    new-instance v0, Landroid/net/StaticIpConfiguration;

    invoke-direct {v0}, Landroid/net/StaticIpConfiguration;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoV:Landroid/net/StaticIpConfiguration;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoV:Landroid/net/StaticIpConfiguration;

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/A;->afH(Landroid/net/StaticIpConfiguration;)I

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_0
    sget-object v0, Landroid/net/IpConfiguration$IpAssignment;->DHCP:Landroid/net/IpConfiguration$IpAssignment;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    sget-object v2, Landroid/net/IpConfiguration$ProxySettings;->NONE:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v2, p0, Lcom/android/settings/wifi/A;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v3, p0, Lcom/android/settings/wifi/A;->aov:Landroid/net/ProxyInfo;

    if-ne v0, v5, :cond_4

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoK:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    sget-object v0, Landroid/net/IpConfiguration$ProxySettings;->STATIC:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoK:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoM:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoJ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :try_start_1
    invoke-static {v3, v2, v4}, Lcom/android/settings/ProxySelector;->blm(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    move v6, v2

    move v2, v0

    move v0, v6

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Landroid/net/ProxyInfo;

    invoke-direct {v0, v3, v2, v4}, Landroid/net/ProxyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aov:Landroid/net/ProxyInfo;

    :cond_2
    :goto_2
    return v5

    :catch_0
    move-exception v0

    move v0, v1

    :goto_3
    const v2, 0x7f120d99

    move v6, v2

    move v2, v0

    move v0, v6

    goto :goto_1

    :cond_3
    return v1

    :cond_4
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoL:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    sget-object v0, Landroid/net/IpConfiguration$ProxySettings;->PAC:Landroid/net/IpConfiguration$ProxySettings;

    iput-object v0, p0, Lcom/android/settings/wifi/A;->aoN:Landroid/net/IpConfiguration$ProxySettings;

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoL:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    return v1

    :cond_5
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_6

    return v1

    :cond_6
    new-instance v1, Landroid/net/ProxyInfo;

    invoke-direct {v1, v0}, Landroid/net/ProxyInfo;-><init>(Landroid/net/Uri;)V

    iput-object v1, p0, Lcom/android/settings/wifi/A;->aov:Landroid/net/ProxyInfo;

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_3
.end method

.method afh()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/android/settings/wifi/A;->aoh:I

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->length()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/android/settings/wifi/A;->afj(Ljava/lang/String;I)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_1

    :cond_0
    iget v2, p0, Lcom/android/settings/wifi/A;->aoh:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->length()I

    move-result v2

    const/16 v3, 0x8

    if-lt v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->length()I

    move-result v2

    const/16 v3, 0x3f

    if-le v2, v3, :cond_4

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->length()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v2}, Lcom/android/settingslib/wifi/i;->cii()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    :cond_2
    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoU:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoU:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->length()I

    move-result v2

    if-nez v2, :cond_6

    :cond_3
    :goto_1
    return v1

    :cond_4
    iget v2, p0, Lcom/android/settings/wifi/A;->aoh:I

    const/16 v3, 0xa

    if-ne v2, v3, :cond_5

    invoke-direct {p0}, Lcom/android/settings/wifi/A;->afi()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afg()Z

    move-result v1

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoY:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/wifi/aJ;

    invoke-direct {v1, p0}, Lcom/android/settings/wifi/aJ;-><init>(Lcom/android/settings/wifi/A;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method getSignalString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chh()Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/wifi/A;->aog:Lcom/android/settingslib/wifi/i;

    invoke-virtual {v1}, Lcom/android/settingslib/wifi/i;->chn()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoA:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoA:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :cond_1
    const/4 v2, -0x1

    if-le v1, v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoA:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoA:[Ljava/lang/String;

    aget-object v0, v0, v1

    :cond_2
    return-object v0
.end method

.method isSplitSystemUser()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserManager;->isSplitSystemUser()Z

    move-result v0

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    const v2, 0x7f0a03ff

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    if-eqz p2, :cond_1

    const/16 v0, 0x90

    :goto_0
    or-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setInputType(I)V

    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/16 v0, 0x80

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    const v2, 0x7f0a051c

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v2, 0x7f0a051b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p2, :cond_3

    const v0, 0x7f1214c9

    :goto_2
    iget-object v3, p0, Lcom/android/settings/wifi/A;->apb:Landroid/view/View;

    const v4, 0x7f0a051a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/A;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    const/16 v1, 0x8

    const v0, 0x7f1214c8

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0249

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    iget-boolean v0, p0, Lcom/android/settings/wifi/A;->aoz:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/A;->aoz:Z

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoQ:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/android/settings/wifi/A;->aoz:Z

    if-eqz v0, :cond_1

    const v0, 0x7f08045d

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/android/settings/wifi/A;->aoz:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x90

    :goto_1
    or-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setInputType(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f08045a

    goto :goto_0

    :cond_2
    const/16 v0, 0x80

    goto :goto_1
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afh()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->adv()V

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoP:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_1

    iput p3, p0, Lcom/android/settings/wifi/A;->aoh:I

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afE()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afF()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->aeS()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aor:Landroid/widget/Spinner;

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoo:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afE()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoO:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afD()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afg()Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoy:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afB()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afg()Z

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoR:Lcom/android/settings/wifi/J;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoR:Lcom/android/settings/wifi/J;

    iget-object v1, p0, Lcom/android/settings/wifi/A;->aoS:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/wifi/J;->agX(I)V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoE:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    const/16 v0, 0x42

    if-ne p2, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/wifi/A;->afh()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/A;->aoi:Lcom/android/settings/wifi/G;

    invoke-interface {v0}, Lcom/android/settings/wifi/G;->adv()V

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
