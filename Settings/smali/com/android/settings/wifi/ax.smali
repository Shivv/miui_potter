.class final Lcom/android/settings/wifi/ax;
.super Ljava/lang/Object;
.source "MiuiSavedAccessPointsWifiSettings.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic atL:Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/ax;->atL:Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ajk(Lcom/android/settings/wifi/j;Lcom/android/settings/wifi/j;)I
    .locals 3

    instance-of v0, p1, Lcom/android/settings/wifi/j;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    instance-of v0, p2, Lcom/android/settings/wifi/j;

    if-nez v0, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/android/settings/wifi/j;->abS()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    iget-object v1, v0, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/android/settings/wifi/j;->abS()Lcom/android/settingslib/wifi/i;

    move-result-object v0

    iget-object v2, v0, Lcom/android/settingslib/wifi/i;->cCL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    :cond_2
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/wifi/j;

    check-cast p2, Lcom/android/settings/wifi/j;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/wifi/ax;->ajk(Lcom/android/settings/wifi/j;Lcom/android/settings/wifi/j;)I

    move-result v0

    return v0
.end method
