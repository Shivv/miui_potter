.class public Lcom/android/settings/fingerprint/a;
.super Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;
.source "FingerprintUiHelper.java"


# instance fields
.field private bag:Lcom/android/settings/fingerprint/b;

.field private bah:Landroid/os/CancellationSignal;

.field private bai:Landroid/widget/TextView;

.field private baj:Landroid/hardware/fingerprint/FingerprintManager;

.field private bak:Landroid/widget/ImageView;

.field private bal:Ljava/lang/Runnable;

.field private mUserId:I


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Landroid/widget/TextView;Lcom/android/settings/fingerprint/b;I)V
    .locals 1

    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;-><init>()V

    new-instance v0, Lcom/android/settings/fingerprint/w;

    invoke-direct {v0, p0}, Lcom/android/settings/fingerprint/w;-><init>(Lcom/android/settings/fingerprint/a;)V

    iput-object v0, p0, Lcom/android/settings/fingerprint/a;->bal:Ljava/lang/Runnable;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqF(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fingerprint/a;->baj:Landroid/hardware/fingerprint/FingerprintManager;

    iput-object p1, p0, Lcom/android/settings/fingerprint/a;->bak:Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/android/settings/fingerprint/a;->bai:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/android/settings/fingerprint/a;->bag:Lcom/android/settings/fingerprint/b;

    iput p4, p0, Lcom/android/settings/fingerprint/a;->mUserId:I

    return-void
.end method

.method private aOA(Ljava/lang/CharSequence;)V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/fingerprint/a;->aOx()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bak:Landroid/widget/ImageView;

    const v1, 0x7f0801d1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bai:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bai:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/fingerprint/a;->bal:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bai:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/fingerprint/a;->bal:Ljava/lang/Runnable;

    const-wide/16 v2, 0x514

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic aOB(Lcom/android/settings/fingerprint/a;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bai:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic aOC(Lcom/android/settings/fingerprint/a;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bak:Landroid/widget/ImageView;

    return-object v0
.end method

.method private aOz(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/settings/fingerprint/a;->bak:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bag:Lcom/android/settings/fingerprint/b;

    invoke-interface {v0, p1}, Lcom/android/settings/fingerprint/b;->aOE(Z)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public aOw()V
    .locals 7

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->baj:Landroid/hardware/fingerprint/FingerprintManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->baj:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->isHardwareDetected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->baj:Landroid/hardware/fingerprint/FingerprintManager;

    iget v2, p0, Lcom/android/settings/fingerprint/a;->mUserId:I

    invoke-virtual {v0, v2}, Landroid/hardware/fingerprint/FingerprintManager;->getEnrolledFingerprints(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fingerprint/a;->bah:Landroid/os/CancellationSignal;

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->baj:Landroid/hardware/fingerprint/FingerprintManager;

    iget v2, p0, Lcom/android/settings/fingerprint/a;->mUserId:I

    invoke-virtual {v0, v2}, Landroid/hardware/fingerprint/FingerprintManager;->setActiveUser(I)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->baj:Landroid/hardware/fingerprint/FingerprintManager;

    iget-object v2, p0, Lcom/android/settings/fingerprint/a;->bah:Landroid/os/CancellationSignal;

    iget v6, p0, Lcom/android/settings/fingerprint/a;->mUserId:I

    move-object v4, p0

    move-object v5, v1

    invoke-virtual/range {v0 .. v6}, Landroid/hardware/fingerprint/FingerprintManager;->authenticate(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;Landroid/os/CancellationSignal;ILandroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;Landroid/os/Handler;I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/fingerprint/a;->aOz(Z)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bak:Landroid/widget/ImageView;

    const v1, 0x7f0801cf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-void
.end method

.method public aOx()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bah:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bah:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aOy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bah:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bah:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    iput-object v1, p0, Lcom/android/settings/fingerprint/a;->bah:Landroid/os/CancellationSignal;

    :cond_0
    return-void
.end method

.method public onAuthenticationError(ILjava/lang/CharSequence;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/android/settings/fingerprint/a;->aOA(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/fingerprint/a;->aOz(Z)V

    return-void
.end method

.method public onAuthenticationFailed()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bak:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f120733

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/fingerprint/a;->aOA(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onAuthenticationHelp(ILjava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p2}, Lcom/android/settings/fingerprint/a;->aOA(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onAuthenticationSucceeded(Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bak:Landroid/widget/ImageView;

    const v1, 0x7f0801d4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/settings/fingerprint/a;->bag:Lcom/android/settings/fingerprint/b;

    invoke-interface {v0}, Lcom/android/settings/fingerprint/b;->aOD()V

    return-void
.end method
