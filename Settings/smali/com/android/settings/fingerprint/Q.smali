.class final Lcom/android/settings/fingerprint/Q;
.super Landroid/hardware/fingerprint/FingerprintManager$RemovalCallback;
.source "FingerprintRemoveSidecar.java"


# instance fields
.field final synthetic bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;


# direct methods
.method constructor <init>(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$RemovalCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onRemovalError(Landroid/hardware/fingerprint/Fingerprint;ILjava/lang/CharSequence;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->aPW(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)Lcom/android/settings/fingerprint/k;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->aPW(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)Lcom/android/settings/fingerprint/k;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/android/settings/fingerprint/k;->onRemovalError(Landroid/hardware/fingerprint/Fingerprint;ILjava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-static {v0, v3}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->aPX(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;Landroid/hardware/fingerprint/Fingerprint;)Landroid/hardware/fingerprint/Fingerprint;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->aPV(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)Ljava/util/Queue;

    move-result-object v0

    new-instance v1, Lcom/android/settings/fingerprint/j;

    iget-object v2, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/android/settings/fingerprint/j;-><init>(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;Landroid/hardware/fingerprint/Fingerprint;ILjava/lang/CharSequence;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onRemovalSucceeded(Landroid/hardware/fingerprint/Fingerprint;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->aPW(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)Lcom/android/settings/fingerprint/k;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->aPW(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)Lcom/android/settings/fingerprint/k;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/settings/fingerprint/k;->aPY(Landroid/hardware/fingerprint/Fingerprint;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-static {v0, v1}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->aPX(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;Landroid/hardware/fingerprint/Fingerprint;)Landroid/hardware/fingerprint/Fingerprint;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fingerprint/Q;->bck:Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;

    invoke-static {v0}, Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;->aPV(Lcom/android/settings/fingerprint/FingerprintRemoveSidecar;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
