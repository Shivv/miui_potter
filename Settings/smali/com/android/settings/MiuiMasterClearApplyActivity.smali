.class public Lcom/android/settings/MiuiMasterClearApplyActivity;
.super Lmiui/app/Activity;
.source "MiuiMasterClearApplyActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static bSK:[I


# instance fields
.field private bSL:Landroid/widget/Button;

.field private bSM:I

.field private bSN:I

.field private bSO:Landroid/os/Handler;

.field private bSP:Landroid/widget/Button;

.field private bSQ:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [I

    sput-object v0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSK:[I

    sget-object v0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSK:[I

    const v1, 0x7f120a24

    const/4 v2, 0x1

    aput v1, v0, v2

    sget-object v0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSK:[I

    const v1, 0x7f120a25

    const/4 v2, 0x2

    aput v1, v0, v2

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    new-instance v0, Lcom/android/settings/jk;

    invoke-direct {v0, p0}, Lcom/android/settings/jk;-><init>(Lcom/android/settings/MiuiMasterClearApplyActivity;)V

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSO:Landroid/os/Handler;

    return-void
.end method

.method private bLX(I)Ljava/lang/CharSequence;
    .locals 1

    sget-object v0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSK:[I

    aget v0, v0, p1

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private bLY()V
    .locals 1

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSM:I

    return-void
.end method

.method static synthetic bLZ(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSL:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic bMa(Lcom/android/settings/MiuiMasterClearApplyActivity;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSM:I

    return v0
.end method

.method static synthetic bMb(Lcom/android/settings/MiuiMasterClearApplyActivity;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSN:I

    return v0
.end method

.method static synthetic bMc(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSO:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic bMd(Lcom/android/settings/MiuiMasterClearApplyActivity;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSM:I

    return p1
.end method


# virtual methods
.method public finish()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lmiui/app/Activity;->finish()V

    invoke-virtual {p0, v0, v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/16 v4, 0x64

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSO:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0, v3}, Lcom/android/settings/MiuiMasterClearApplyActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->finish()V

    goto :goto_0

    :sswitch_1
    iget v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSN:I

    if-ne v0, v5, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSO:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->finish()V

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSN:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSN:I

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->bLY()V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSQ:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSN:I

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->bLX(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSN:I

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSL:Landroid/widget/Button;

    new-array v1, v2, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSM:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f1203a6

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSL:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSO:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSO:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSL:Landroid/widget/Button;

    new-array v1, v2, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSM:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f1203a4

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a00c6 -> :sswitch_0
        0x7f0a02e4 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/high16 v2, 0x8000000

    const/4 v4, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "format_internal_storage"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSN:I

    const v0, 0x7f0d00ee

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->bLY()V

    const v0, 0x7f0a0510

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSQ:Landroid/widget/TextView;

    const v0, 0x7f0a00c6

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSP:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSP:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a02e4

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSL:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSL:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSQ:Landroid/widget/TextView;

    iget v3, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSN:I

    invoke-direct {p0, v3}, Lcom/android/settings/MiuiMasterClearApplyActivity;->bLX(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSL:Landroid/widget/Button;

    if-eqz v2, :cond_1

    const v0, 0x7f1203a4

    :goto_1
    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSM:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSL:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSO:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const v0, 0x7f1203a6

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lmiui/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->bSO:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method
