.class final Lcom/android/settings/fM;
.super Ljava/lang/Object;
.source "PrivacySettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic ckS:Lcom/android/settings/PrivacySettings;


# direct methods
.method constructor <init>(Lcom/android/settings/PrivacySettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fM;->ckS:Lcom/android/settings/PrivacySettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/settings/fM;->ckS:Lcom/android/settings/PrivacySettings;

    invoke-static {v2}, Lcom/android/settings/PrivacySettings;->buc(Lcom/android/settings/PrivacySettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    if-ne p1, v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/fM;->ckS:Lcom/android/settings/PrivacySettings;

    invoke-static {v2}, Lcom/android/settings/PrivacySettings;->bud(Lcom/android/settings/PrivacySettings;)Landroid/app/backup/IBackupManager;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/app/backup/IBackupManager;->setAutoRestore(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/android/settings/fM;->ckS:Lcom/android/settings/PrivacySettings;

    invoke-static {v2}, Lcom/android/settings/PrivacySettings;->buc(Lcom/android/settings/PrivacySettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0
.end method
