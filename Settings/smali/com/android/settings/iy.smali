.class final Lcom/android/settings/iy;
.super Ljava/lang/Object;
.source "RadioInfo.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic cow:Lcom/android/settings/RadioInfo;

.field final synthetic cox:I

.field final synthetic coy:Z


# direct methods
.method constructor <init>(Lcom/android/settings/RadioInfo;IZ)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/iy;->cow:Lcom/android/settings/RadioInfo;

    iput p2, p0, Lcom/android/settings/iy;->cox:I

    iput-boolean p3, p0, Lcom/android/settings/iy;->coy:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/iy;->cow:Lcom/android/settings/RadioInfo;

    invoke-static {v0}, Lcom/android/settings/RadioInfo;->bGQ(Lcom/android/settings/RadioInfo;)Lcom/android/ims/ImsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->getConfigInterface()Lcom/android/ims/ImsConfig;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/iy;->cox:I

    iget-boolean v0, p0, Lcom/android/settings/iy;->coy:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/android/ims/ImsConfig;->setProvisionedValue(II)I
    :try_end_0
    .catch Lcom/android/ims/ImsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "RadioInfo"

    const-string/jumbo v2, "setImsConfigProvisioned() exception:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
