.class public Lcom/android/settings/MiuiSoundSettings;
.super Lcom/android/settings/SoundSettings;
.source "MiuiSoundSettings.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# static fields
.field private static final brl:[Ljava/lang/String;


# instance fields
.field private brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private brB:Landroid/preference/CheckBoxPreference;

.field private brC:Landroid/preference/CheckBoxPreference;

.field private final brD:Landroid/database/ContentObserver;

.field private brE:Z

.field private brF:Ljava/util/ArrayList;

.field private brm:Landroid/media/AudioManager;

.field private brn:Landroid/preference/CheckBoxPreference;

.field private bro:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private final brp:Landroid/os/Handler;

.field private brq:Landroid/os/Handler;

.field private brr:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private brs:Landroid/content/pm/PackageManager;

.field private final brt:Landroid/content/BroadcastReceiver;

.field private bru:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private brv:Landroid/preference/CheckBoxPreference;

.field private final brw:Lcom/android/settings/c;

.field private brx:Landroid/preference/CheckBoxPreference;

.field private bry:Lcom/android/settings/dndmode/LabelPreference;

.field private brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private mContext:Landroid/content/Context;

.field protected mUserId:I

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "ring_volume"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "media_volume"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "alarm_volume"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/MiuiSoundSettings;->brl:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/SoundSettings;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brF:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiSoundSettings;->brE:Z

    new-instance v0, Lcom/android/settings/c;

    invoke-direct {v0, p0}, Lcom/android/settings/c;-><init>(Lcom/android/settings/MiuiSoundSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brw:Lcom/android/settings/c;

    new-instance v0, Lcom/android/settings/dp;

    invoke-direct {v0, p0}, Lcom/android/settings/dp;-><init>(Lcom/android/settings/MiuiSoundSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brt:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brp:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/dq;

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brp:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dq;-><init>(Lcom/android/settings/MiuiSoundSettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brD:Landroid/database/ContentObserver;

    return-void
.end method

.method public static beC(Landroid/content/Context;)I
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "haptic_feedback_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "haptic_feedback_level"

    sget v2, Landroid/provider/MiuiSettings$System;->HAPTIC_FEEDBACK_LEVEL_DEFAULT:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private beD(II)I
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brm:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    if-gez p2, :cond_0

    return v1

    :cond_0
    if-le p2, v0, :cond_1

    return v0

    :cond_1
    return p2
.end method

.method private beF()V
    .locals 2

    const-string/jumbo v0, "MiuiSoundSettings"

    const-string/jumbo v1, "init all ringtone type"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bru:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bru:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bro:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bro:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brr:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brr:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_4
    return-void
.end method

.method private beG(Ljava/lang/String;II)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v0, p2}, Lcom/android/settings/sound/VolumeSeekBarPreference;->setStream(I)V

    invoke-virtual {v0, p3}, Lcom/android/settings/sound/VolumeSeekBarPreference;->setIcon(I)V

    new-instance v1, Lcom/android/settings/sound/b;

    invoke-direct {v1, v0}, Lcom/android/settings/sound/b;-><init>(Lcom/android/settings/sound/VolumeSeekBarPreference;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/sound/VolumeSeekBarPreference;->GC(Lcom/android/settings/sound/b;)V

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brF:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private beH()Z
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v2, "telecom"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->isInCall()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brm:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private beI()V
    .locals 7

    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bry:Lcom/android/settings/dndmode/LabelPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/MiuiSettings$SilenceMode;->getZenMode(Landroid/content/Context;)I

    move-result v1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brq:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bry:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f121086

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f121088

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-ne v1, v6, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f12108a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/app/ExtraNotificationManager;->getZenModeConfig(Landroid/content/Context;)Landroid/service/notification/ZenModeConfig;

    move-result-object v1

    iget-object v2, v1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    if-eqz v2, :cond_2

    iget-object v1, v1, Landroid/service/notification/ZenModeConfig;->manualRule:Landroid/service/notification/ZenModeConfig$ZenRule;

    iget-object v1, v1, Landroid/service/notification/ZenModeConfig$ZenRule;->conditionId:Landroid/net/Uri;

    invoke-static {v1}, Landroid/service/notification/ZenModeConfig;->tryParseCountdownConditionId(Landroid/net/Uri;)J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2, v3}, Lcom/android/settings/MiuiSoundSettings;->beN(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brq:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/settings/MiuiSoundSettings;->brq:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_2
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->bry:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/dndmode/LabelPreference;->mp(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private beL(Z)V
    .locals 3

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "persist.sys.backtouch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private beM(I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "haptic_feedback_enabled"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "haptic_feedback_level"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v0, Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v0, v3, v2}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(IZ)Z

    :goto_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "haptic_feedback_enabled"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public static beN(J)Ljava/lang/String;
    .locals 8

    const-wide/32 v6, 0x36ee80

    const/16 v4, 0xa

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    div-long v2, p0, v6

    long-to-int v1, v2

    rem-long v2, p0, v6

    long-to-int v2, v2

    const v3, 0xea60

    div-int/2addr v2, v3

    if-ge v1, v4, :cond_0

    const-string/jumbo v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ge v2, v4, :cond_1

    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic beO(Lcom/android/settings/MiuiSoundSettings;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brq:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic beP(Lcom/android/settings/MiuiSoundSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->beI()V

    return-void
.end method


# virtual methods
.method beB()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lmiui/util/AudioManagerHelper;->isVibrateEnabled(Landroid/content/Context;I)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lmiui/util/AudioManagerHelper;->isVibrateEnabled(Landroid/content/Context;I)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_1
    return-void
.end method

.method protected beE(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->bro:Lcom/android/settings/MiuiDefaultRingtonePreference;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method beJ()V
    .locals 3

    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->beB()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brx:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brx:Landroid/preference/CheckBoxPreference;

    invoke-static {v0}, Lmiui/util/AudioManagerHelper;->isSilentEnabled(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_1
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lmiui/util/AudioManagerHelper;->isVibrateEnabled(Landroid/content/Context;I)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_2
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lmiui/util/AudioManagerHelper;->isVibrateEnabled(Landroid/content/Context;I)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_3
    return-void
.end method

.method protected beK()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {v0}, Lcom/android/settings/MiuiDefaultRingtonePreference;->byK()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSoundSettings;->bRK(Landroid/net/Uri;I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {v0}, Lcom/android/settings/MiuiDefaultRingtonePreference;->byK()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSoundSettings;->bRK(Landroid/net/Uri;I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bro:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bro:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {v0}, Lcom/android/settings/MiuiDefaultRingtonePreference;->byK()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSoundSettings;->bRK(Landroid/net/Uri;I)V

    :cond_3
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/SoundSettings;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/MiuiSoundSettings;->mUserId:I

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brs:Landroid/content/pm/PackageManager;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mUserManager:Landroid/os/UserManager;

    const-string/jumbo v0, "ring_volume"

    const/4 v1, 0x2

    const v2, 0x7f0803b9

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/MiuiSoundSettings;->beG(Ljava/lang/String;II)V

    const-string/jumbo v0, "alarm_volume"

    const/4 v1, 0x4

    const v2, 0x7f080082

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/MiuiSoundSettings;->beG(Ljava/lang/String;II)V

    const-string/jumbo v0, "media_volume"

    const/4 v1, 0x3

    const v2, 0x7f08030d

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/MiuiSoundSettings;->beG(Ljava/lang/String;II)V

    new-instance v0, Lcom/android/settings/dr;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/dr;-><init>(Lcom/android/settings/MiuiSoundSettings;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brq:Landroid/os/Handler;

    const-string/jumbo v0, "key_silent_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bry:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bry:Lcom/android/settings/dndmode/LabelPreference;

    new-instance v1, Lcom/android/settings/ds;

    invoke-direct {v1, p0}, Lcom/android/settings/ds;-><init>(Lcom/android/settings/MiuiSoundSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/MiuiSoundSettings;->beC(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    const-string/jumbo v0, "sms_received_sound"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const-string/jumbo v0, "sms_delivered_sound"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const-string/jumbo v0, "calendar_sound"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bro:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const-string/jumbo v0, "notification_sound"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brr:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brA:Lcom/android/settings/MiuiDefaultRingtonePreference;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brz:Lcom/android/settings/MiuiDefaultRingtonePreference;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXN:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->bXN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->bXN:Landroid/preference/CheckBoxPreference;

    :cond_3
    const-string/jumbo v0, "audio"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brm:Landroid/media/AudioManager;

    const-string/jumbo v0, "silent_mode"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brx:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "miui_vibrate_in_silent_key"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "miui_vibrate_in_normal_key"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brx:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brx:Landroid/preference/CheckBoxPreference;

    :goto_0
    const-string/jumbo v0, "backtouch"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brn:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "centaur"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brn:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brn:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "1"

    const-string/jumbo v2, "persist.sys.backtouch"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_4
    :goto_1
    const-string/jumbo v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_5
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_6
    const-string/jumbo v1, "miui_vibrate_category"

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_7
    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    :cond_8
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    :cond_9
    const-string/jumbo v0, "ringtone"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bru:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_b

    :cond_a
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bru:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->bru:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->bru:Lcom/android/settings/MiuiDefaultRingtonePreference;

    :cond_b
    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->beF()V

    const-string/jumbo v0, "screenshot_sound"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brv:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brv:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "has_screenshot_sound"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, v4}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brw:Lcom/android/settings/c;

    invoke-virtual {v0}, Lcom/android/settings/c;->beQ()V

    return-void

    :cond_c
    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->bry:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->bry:Lcom/android/settings/dndmode/LabelPreference;

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brn:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/SoundSettings;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brF:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->GD()Lcom/android/settings/sound/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/sound/b;->stop()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brw:Lcom/android/settings/c;

    invoke-virtual {v0}, Lcom/android/settings/c;->beR()V

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7

    const/4 v2, 0x3

    const/4 v0, 0x2

    const/16 v6, 0x19

    const/4 v1, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->beH()Z

    move-result v3

    if-eqz v3, :cond_0

    return v5

    :cond_0
    iget-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brm:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->getUiSoundsStreamType()I

    move-result v3

    invoke-static {v0, v5}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v0

    :cond_1
    :goto_0
    if-eq p2, v6, :cond_2

    const/16 v0, 0x18

    if-ne p2, v0, :cond_4

    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brm:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getLastAudibleStreamVolume(I)I

    move-result v3

    if-ne p2, v6, :cond_6

    iput-boolean v1, p0, Lcom/android/settings/MiuiSoundSettings;->brE:Z

    const/4 v0, -0x1

    :goto_1
    iget-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->brm:Landroid/media/AudioManager;

    add-int/2addr v0, v3

    invoke-direct {p0, v2, v0}, Lcom/android/settings/MiuiSoundSettings;->beD(II)I

    move-result v0

    const v3, 0x100400

    invoke-virtual {v4, v2, v0, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return v1

    :cond_3
    invoke-static {v2, v5}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x4

    invoke-static {v0, v5}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x4

    move v2, v0

    goto :goto_0

    :cond_4
    if-ne p2, v6, :cond_5

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_5

    iput-boolean v5, p0, Lcom/android/settings/MiuiSoundSettings;->brE:Z

    :cond_5
    return v5

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v2, v3

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brD:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brt:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brq:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-super {p0}, Lcom/android/settings/SoundSettings;->onPause()V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brF:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->GD()Lcom/android/settings/sound/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/sound/b;->pause()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_0

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->beM(I)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brx:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_2

    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v3}, Lmiui/util/AudioManagerHelper;->toggleSilent(Landroid/content/Context;I)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/SoundSettings;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->bXM:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setSoundEffectsEnabled(Z)V

    :cond_1
    return v1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brB:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-static {v0, v1, v3}, Lmiui/util/AudioManagerHelper;->setVibrateSetting(Landroid/content/Context;ZZ)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->brC:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lmiui/util/AudioManagerHelper;->setVibrateSetting(Landroid/content/Context;ZZ)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brn:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brn:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->beL(Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brv:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "has_screenshot_sound"

    iget-object v2, p0, Lcom/android/settings/MiuiSoundSettings;->brv:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 7

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/SoundSettings;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.media.RINGER_MODE_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brt:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "vibrate_in_silent"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brD:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string/jumbo v2, "vibrate_in_normal"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->brD:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->brF:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->GD()Lcom/android/settings/sound/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/sound/b;->Gg()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->beJ()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v2, "no_adjust_volume"

    iget v3, p0, Lcom/android/settings/MiuiSoundSettings;->mUserId:I

    invoke-static {v0, v2, v3}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v3, "no_adjust_volume"

    iget v4, p0, Lcom/android/settings/MiuiSoundSettings;->mUserId:I

    invoke-static {v0, v3, v4}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v3

    sget-object v4, Lcom/android/settings/MiuiSoundSettings;->brl:[Ljava/lang/String;

    array-length v5, v4

    :goto_1
    if-ge v1, v5, :cond_3

    aget-object v0, v4, v1

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    xor-int/lit8 v6, v3, 0x1

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_1
    instance-of v6, v0, Lcom/android/settingslib/MiuiRestrictedPreference;

    if-eqz v6, :cond_2

    xor-int/lit8 v6, v3, 0x1

    if-eqz v6, :cond_2

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/MiuiRestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "cell_broadcast_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedPreference;

    if-eqz v0, :cond_4

    const-string/jumbo v1, "no_config_cell_broadcasts"

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqc(Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->beI()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/SoundSettings;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    return-void
.end method
