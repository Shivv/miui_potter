.class public Lcom/android/settings/EncryptionAndCredential;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "EncryptionAndCredential.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field private static final bAo:I


# instance fields
.field private bAp:Z

.field private bAq:Landroid/security/KeyStore;

.field private bAr:Lcom/android/settingslib/MiuiRestrictedPreference;

.field private bAs:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    sput v0, Lcom/android/settings/EncryptionAndCredential;->bAo:I

    new-instance v0, Lcom/android/settings/ac;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/settings/ac;-><init>(Lcom/android/settings/ac;)V

    sput-object v0, Lcom/android/settings/EncryptionAndCredential;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private bod()Landroid/preference/PreferenceScreen;
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/EncryptionAndCredential;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    const v0, 0x7f150054

    invoke-virtual {p0, v0}, Lcom/android/settings/EncryptionAndCredential;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/EncryptionAndCredential;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/EncryptionAndCredential;->bAs:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/EncryptionAndCredential;->bAp:Z

    iget-boolean v0, p0, Lcom/android/settings/EncryptionAndCredential;->bAp:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/internal/widget/LockPatternUtils;->isDeviceEncryptionEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f1500c2

    invoke-virtual {p0, v0}, Lcom/android/settings/EncryptionAndCredential;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "security_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/EncryptionAndCredential;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-static {}, Landroid/os/storage/StorageManager;->isFileEncryptedNativeOrEmulated()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "security_encryption_title"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_0
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/EncryptionAndCredential;->bAq:Landroid/security/KeyStore;

    invoke-virtual {p0}, Lcom/android/settings/EncryptionAndCredential;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "no_config_credentials"

    sget v3, Lcom/android/settings/EncryptionAndCredential;->bAo:I

    invoke-static {v0, v1, v3}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "user_credentials"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedPreference;

    const-string/jumbo v1, "no_config_credentials"

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqc(Ljava/lang/String;)V

    const-string/jumbo v0, "credential_storage_type"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedPreference;

    const-string/jumbo v1, "no_config_credentials"

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqc(Ljava/lang/String;)V

    const-string/jumbo v1, "credentials_install"

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/MiuiRestrictedPreference;

    const-string/jumbo v3, "no_config_credentials"

    invoke-virtual {v1, v3}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqc(Ljava/lang/String;)V

    const-string/jumbo v1, "credentials_reset"

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/MiuiRestrictedPreference;

    iput-object v1, p0, Lcom/android/settings/EncryptionAndCredential;->bAr:Lcom/android/settingslib/MiuiRestrictedPreference;

    iget-object v1, p0, Lcom/android/settings/EncryptionAndCredential;->bAr:Lcom/android/settingslib/MiuiRestrictedPreference;

    const-string/jumbo v3, "no_config_credentials"

    invoke-virtual {v1, v3}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqc(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/EncryptionAndCredential;->bAq:Landroid/security/KeyStore;

    invoke-virtual {v1}, Landroid/security/KeyStore;->isHardwareBacked()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f120489

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setSummary(I)V

    :goto_2
    return-object v2

    :cond_2
    const-string/jumbo v1, "crypt_keeper_encrypt_title"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_3
    const v0, 0x7f1500d6

    invoke-virtual {p0, v0}, Lcom/android/settings/EncryptionAndCredential;->addPreferencesFromResource(I)V

    goto :goto_0

    :cond_4
    const v1, 0x7f12048a

    goto :goto_1

    :cond_5
    const-string/jumbo v0, "credentials_management"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    const-string/jumbo v1, "credentials_reset"

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v1, "credentials_install"

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v1, "credential_storage_type"

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v1, "user_credentials"

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_2
.end method


# virtual methods
.method protected aq()I
    .locals 1

    const v0, 0x7f120822

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x34e

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    invoke-direct {p0}, Lcom/android/settings/EncryptionAndCredential;->bod()Landroid/preference/PreferenceScreen;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/EncryptionAndCredential;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/EncryptionAndCredential;->bAs:Landroid/os/UserManager;

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/EncryptionAndCredential;->bod()Landroid/preference/PreferenceScreen;

    iget-object v0, p0, Lcom/android/settings/EncryptionAndCredential;->bAr:Lcom/android/settingslib/MiuiRestrictedPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/EncryptionAndCredential;->bAr:Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqf()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/EncryptionAndCredential;->bAr:Lcom/android/settingslib/MiuiRestrictedPreference;

    iget-object v1, p0, Lcom/android/settings/EncryptionAndCredential;->bAq:Landroid/security/KeyStore;

    invoke-virtual {v1}, Landroid/security/KeyStore;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setEnabled(Z)V

    :cond_0
    return-void
.end method
