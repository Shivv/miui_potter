.class Lcom/android/settings/aC;
.super Ljava/lang/Object;
.source "SecuritySettings.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;


# instance fields
.field private final bEP:Lcom/android/settings/dashboard/C;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/aC;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/aC;->bEP:Lcom/android/settings/dashboard/C;

    return-void
.end method


# virtual methods
.method public jt(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/aC;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->bqF(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->isHardwareDetected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/aC;->bEP:Lcom/android/settings/dashboard/C;

    iget-object v1, p0, Lcom/android/settings/aC;->mContext:Landroid/content/Context;

    const v2, 0x7f120fc4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/aC;->bEP:Lcom/android/settings/dashboard/C;

    iget-object v1, p0, Lcom/android/settings/aC;->mContext:Landroid/content/Context;

    const v2, 0x7f120fc5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
