.class public abstract Lcom/android/settings/ab;
.super Ljava/lang/Object;
.source "MiuiAnimationController.java"


# instance fields
.field private bzq:Z

.field private bzr:Landroid/graphics/drawable/Animatable;

.field private bzs:Landroid/graphics/drawable/Drawable;

.field private bzt:Ljava/lang/Runnable;

.field private bzu:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/eD;

    invoke-direct {v0, p0}, Lcom/android/settings/eD;-><init>(Lcom/android/settings/ab;)V

    iput-object v0, p0, Lcom/android/settings/ab;->bzt:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/ab;->bzu:Landroid/os/Handler;

    invoke-direct {p0, p1, p2}, Lcom/android/settings/ab;->bmT(Landroid/content/Context;I)V

    return-void
.end method

.method private bmT(Landroid/content/Context;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    iput-object v0, p0, Lcom/android/settings/ab;->bzs:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/settings/ab;->bzs:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/android/settings/ab;->amk(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Animatable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ab;->bzr:Landroid/graphics/drawable/Animatable;

    iget-object v0, p0, Lcom/android/settings/ab;->bzs:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    iget-object v0, p0, Lcom/android/settings/ab;->bzr:Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    return-void
.end method

.method private bmU()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/ab;->bzu:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/ab;->bzt:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/settings/ab;->bzu:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/ab;->bzt:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private bmV()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/ab;->bzs:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "MiuiAnimationDrawable"

    const-string/jumbo v1, "playAnimationImmediately: callback is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ab;->bzr:Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/ab;->bzq:Z

    goto :goto_0
.end method

.method static synthetic bmW(Lcom/android/settings/ab;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ab;->bmV()V

    return-void
.end method


# virtual methods
.method protected abstract amk(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Animatable;
.end method

.method protected bmP()Landroid/graphics/drawable/Animatable;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ab;->bzr:Landroid/graphics/drawable/Animatable;

    return-object v0
.end method

.method public bmQ()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ab;->bzs:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public bmR()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/ab;->bzs:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    iget-object v0, p0, Lcom/android/settings/ab;->bzs:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/ab;->bmV()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/ab;->bmU()V

    goto :goto_0
.end method

.method public bmS()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/ab;->bzs:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    iget-object v0, p0, Lcom/android/settings/ab;->bzr:Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    iput-boolean v1, p0, Lcom/android/settings/ab;->bzq:Z

    iget-object v0, p0, Lcom/android/settings/ab;->bzu:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/ab;->bzt:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
