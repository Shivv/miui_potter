.class public Lcom/android/settings/search2/AppSearchResult;
.super Lcom/android/settings/search2/SearchResult;
.source "AppSearchResult.java"


# instance fields
.field public final info:Landroid/content/pm/ApplicationInfo;


# direct methods
.method public constructor <init>(Lcom/android/settings/search2/AppSearchResult$Builder;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/search2/SearchResult;-><init>(Lcom/android/settings/search2/SearchResult$Builder;)V

    iget-object v0, p1, Lcom/android/settings/search2/AppSearchResult$Builder;->mInfo:Landroid/content/pm/ApplicationInfo;

    iput-object v0, p0, Lcom/android/settings/search2/AppSearchResult;->info:Landroid/content/pm/ApplicationInfo;

    return-void
.end method


# virtual methods
.method public getAppUserHandle()Landroid/os/UserHandle;
    .locals 2

    new-instance v0, Landroid/os/UserHandle;

    iget-object v1, p0, Lcom/android/settings/search2/AppSearchResult;->info:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    return-object v0
.end method
