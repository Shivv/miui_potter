.class public Lcom/android/settings/search2/SavedQueryPayload;
.super Lcom/android/settings/search2/ResultPayload;
.source "SavedQueryPayload.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/search2/SavedQueryPayload$1;

    invoke-direct {v0}, Lcom/android/settings/search2/SavedQueryPayload$1;-><init>()V

    sput-object v0, Lcom/android/settings/search2/SavedQueryPayload;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/search2/ResultPayload;-><init>(Landroid/content/Intent;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search2/SavedQueryPayload;->query:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/search2/ResultPayload;-><init>(Landroid/content/Intent;)V

    iput-object p1, p0, Lcom/android/settings/search2/SavedQueryPayload;->query:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search2/SavedQueryPayload;->query:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
