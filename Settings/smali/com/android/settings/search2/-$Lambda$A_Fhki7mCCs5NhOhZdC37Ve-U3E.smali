.class final synthetic Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final synthetic $id:B

.field private final synthetic -$f0:Ljava/lang/Object;

.field private final synthetic -$f1:Ljava/lang/Object;


# direct methods
.method private final synthetic $m$0(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/settings/search2/SearchFragment;

    iget-object v1, p0, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->-$f1:Ljava/lang/Object;

    check-cast v1, Lcom/android/settings/search2/SearchResult;

    invoke-static {v0, v1, p1}, Lcom/android/settings/search2/SavedQueryViewHolder;->lambda$-com_android_settings_search2_SavedQueryViewHolder_1211(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;Landroid/view/View;)V

    return-void
.end method

.method private final synthetic $m$1(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->-$f0:Ljava/lang/Object;

    check-cast v0, Lcom/android/settings/search2/SearchFragment;

    iget-object v1, p0, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->-$f1:Ljava/lang/Object;

    check-cast v1, Lcom/android/settings/search2/SearchResult;

    invoke-static {v0, v1, p1}, Lcom/android/settings/search2/SavedQueryViewHolder;->lambda$-com_android_settings_search2_SavedQueryViewHolder_1301(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;Landroid/view/View;)V

    return-void
.end method

.method public synthetic constructor <init>(BLjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte p1, p0, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->$id:B

    iput-object p2, p0, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->-$f0:Ljava/lang/Object;

    iput-object p3, p0, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->-$f1:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-byte v0, p0, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->$id:B

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->$m$0(Landroid/view/View;)V

    return-void

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/settings/search2/-$Lambda$A_Fhki7mCCs5NhOhZdC37Ve-U3E;->$m$1(Landroid/view/View;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
