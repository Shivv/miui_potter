.class public abstract Lcom/android/settings/search2/SearchViewHolder;
.super Landroid/support/v7/widget/p;
.source "SearchViewHolder.java"


# instance fields
.field private final DYNAMIC_PLACEHOLDER:Ljava/lang/String;

.field public final breadcrumbView:Landroid/widget/TextView;

.field public final iconView:Landroid/widget/ImageView;

.field private final mIconDrawableFactory:Landroid/util/IconDrawableFactory;

.field protected final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private final mPlaceholderSummary:Ljava/lang/String;

.field public final summaryView:Landroid/widget/TextView;

.field public final titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v7/widget/p;-><init>(Landroid/view/View;)V

    const-string/jumbo v0, "%s"

    iput-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->DYNAMIC_PLACEHOLDER:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->titleView:Landroid/widget/TextView;

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->summaryView:Landroid/widget/TextView;

    const v0, 0x1020006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->iconView:Landroid/widget/ImageView;

    const v0, 0x7f0a00a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->breadcrumbView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1211e8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->mPlaceholderSummary:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/util/IconDrawableFactory;->newInstance(Landroid/content/Context;)Landroid/util/IconDrawableFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->mIconDrawableFactory:Landroid/util/IconDrawableFactory;

    return-void
.end method

.method private bindBreadcrumbView(Lcom/android/settings/search2/SearchResult;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v6, 0x0

    iget-object v0, p1, Lcom/android/settings/search2/SearchResult;->breadcrumbs:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/settings/search2/SearchResult;->breadcrumbs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->breadcrumbView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->breadcrumbView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, p1, Lcom/android/settings/search2/SearchResult;->breadcrumbs:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p1, Lcom/android/settings/search2/SearchResult;->breadcrumbs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    move-object v2, v0

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    iget-object v2, p1, Lcom/android/settings/search2/SearchResult;->breadcrumbs:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v5, v1

    const v2, 0x7f120f1b

    invoke-virtual {v3, v2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->breadcrumbView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->breadcrumbView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public onBind(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->titleView:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settings/search2/SearchResult;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/android/settings/search2/SearchResult;->summary:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/android/settings/search2/SearchResult;->summary:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/search2/SearchViewHolder;->mPlaceholderSummary:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/android/settings/search2/SearchResult;->summary:Ljava/lang/CharSequence;

    const-string/jumbo v1, "%s"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->summaryView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    instance-of v0, p2, Lcom/android/settings/search2/AppSearchResult;

    if-eqz v0, :cond_2

    move-object v0, p2

    check-cast v0, Lcom/android/settings/search2/AppSearchResult;

    invoke-virtual {p1}, Lcom/android/settings/search2/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/settings/search2/AppSearchResult;->getAppUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/search2/SearchViewHolder;->mIconDrawableFactory:Landroid/util/IconDrawableFactory;

    iget-object v4, v0, Lcom/android/settings/search2/AppSearchResult;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/util/IconDrawableFactory;->getBadgedIcon(Landroid/content/pm/ApplicationInfo;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/search2/SearchViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p0, Lcom/android/settings/search2/SearchViewHolder;->titleView:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/android/settings/search2/AppSearchResult;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getUserBadgedLabel(Ljava/lang/CharSequence;Landroid/os/UserHandle;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-direct {p0, p2}, Lcom/android/settings/search2/SearchViewHolder;->bindBreadcrumbView(Lcom/android/settings/search2/SearchResult;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->summaryView:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/android/settings/search2/SearchResult;->summary:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->summaryView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/search2/SearchViewHolder;->iconView:Landroid/widget/ImageView;

    iget-object v1, p2, Lcom/android/settings/search2/SearchResult;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
