.class public Lcom/android/settings/search2/SearchResultsAdapter;
.super Landroid/support/v7/widget/b;
.source "SearchResultsAdapter.java"


# instance fields
.field private final mFragment:Lcom/android/settings/search2/SearchFragment;

.field private mResultsMap:Ljava/util/Map;

.field private mSearchResults:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/android/settings/search2/SearchFragment;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/b;-><init>()V

    iput-object p1, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mFragment:Lcom/android/settings/search2/SearchFragment;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mResultsMap:Ljava/util/Map;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/search2/SearchResultsAdapter;->setHasStableIds(Z)V

    return-void
.end method


# virtual methods
.method public addSearchResults(Ljava/util/List;Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mResultsMap:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public clearResults()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mResultsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {p0}, Lcom/android/settings/search2/SearchResultsAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public displaySavedQuery(Ljava/util/List;)I
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/search2/SearchResultsAdapter;->clearResults()V

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/android/settings/search2/SearchResultsAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public displaySearchResults()I
    .locals 11

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mResultsMap:Ljava/util/Map;

    const-class v1, Lcom/android/settings/search2/DatabaseResultLoader;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mResultsMap:Ljava/util/Map;

    const-class v2, Lcom/android/settings/search2/InstalledAppResultLoader;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    move v9, v2

    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v3, v2

    :goto_1
    new-instance v10, Ljava/util/ArrayList;

    add-int v2, v9, v3

    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(I)V

    move v8, v4

    move v5, v4

    move v2, v4

    :goto_2
    const/16 v6, 0xa

    if-gt v8, v6, :cond_4

    move v7, v2

    :goto_3
    if-ge v7, v9, :cond_2

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/search2/SearchResult;

    iget v2, v2, Lcom/android/settings/search2/SearchResult;->rank:I

    if-ne v2, v8, :cond_2

    add-int/lit8 v6, v7, 0x1

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/search2/SearchResult;

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v7, v6

    goto :goto_3

    :cond_0
    move v9, v4

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    :cond_2
    :goto_4
    if-ge v5, v3, :cond_3

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/search2/SearchResult;

    iget v2, v2, Lcom/android/settings/search2/SearchResult;->rank:I

    if-ne v2, v8, :cond_3

    add-int/lit8 v6, v5, 0x1

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/search2/SearchResult;

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v5, v6

    goto :goto_4

    :cond_3
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v2, v7

    goto :goto_2

    :cond_4
    :goto_5
    if-ge v2, v9, :cond_6

    add-int/lit8 v6, v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/search2/SearchResult;

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v6

    goto :goto_5

    :goto_6
    if-ge v0, v3, :cond_5

    add-int/lit8 v2, v0, 0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/search2/SearchResult;

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v2

    goto :goto_6

    :cond_5
    new-instance v0, Lcom/android/settings/search2/SearchResultDiffCallback;

    iget-object v1, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-direct {v0, v1, v10}, Lcom/android/settings/search2/SearchResultDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v0, v4}, Landroid/support/v7/c/a;->dIU(Landroid/support/v7/c/b;Z)Landroid/support/v7/c/e;

    move-result-object v0

    iput-object v10, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-virtual {v0, p0}, Landroid/support/v7/c/e;->dJc(Landroid/support/v7/widget/b;)V

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_6
    move v0, v5

    goto :goto_6
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/search2/SearchResult;

    iget-wide v0, v0, Lcom/android/settings/search2/SearchResult;->stableId:J

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/search2/SearchResult;

    iget v0, v0, Lcom/android/settings/search2/SearchResult;->viewType:I

    return v0
.end method

.method public getSearchResults()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/p;I)V
    .locals 0

    check-cast p1, Lcom/android/settings/search2/SearchViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/search2/SearchResultsAdapter;->onBindViewHolder(Lcom/android/settings/search2/SearchViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/android/settings/search2/SearchViewHolder;I)V
    .locals 2

    iget-object v1, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mFragment:Lcom/android/settings/search2/SearchFragment;

    iget-object v0, p0, Lcom/android/settings/search2/SearchResultsAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/search2/SearchResult;

    invoke-virtual {p1, v1, v0}, Lcom/android/settings/search2/SearchViewHolder;->onBind(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/search2/SearchResultsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/search2/SearchViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/search2/SearchViewHolder;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    return-object v0

    :pswitch_1
    const v0, 0x7f0d01ab

    invoke-virtual {v1, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/settings/search2/IntentSearchViewHolder;

    invoke-direct {v1, v0}, Lcom/android/settings/search2/IntentSearchViewHolder;-><init>(Landroid/view/View;)V

    return-object v1

    :pswitch_2
    const v2, 0x7f0d01aa

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/android/settings/search2/InlineSwitchViewHolder;

    invoke-direct {v2, v1, v0}, Lcom/android/settings/search2/InlineSwitchViewHolder;-><init>(Landroid/view/View;Landroid/content/Context;)V

    return-object v2

    :pswitch_3
    const v0, 0x7f0d01af

    invoke-virtual {v1, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/settings/search2/SavedQueryViewHolder;

    invoke-direct {v1, v0}, Lcom/android/settings/search2/SavedQueryViewHolder;-><init>(Landroid/view/View;)V

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
