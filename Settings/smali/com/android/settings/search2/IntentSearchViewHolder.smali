.class public Lcom/android/settings/search2/IntentSearchViewHolder;
.super Lcom/android/settings/search2/SearchViewHolder;
.source "IntentSearchViewHolder.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "IntentSearchViewHolder"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/search2/SearchViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method synthetic lambda$-com_android_settings_search2_IntentSearchViewHolder_2878(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;Landroid/view/View;)V
    .locals 4

    const/4 v2, 0x0

    new-array v0, v2, [Landroid/util/Pair;

    invoke-virtual {p1, p0, p2, v0}, Lcom/android/settings/search2/SearchFragment;->onSearchResultClicked(Lcom/android/settings/search2/SearchViewHolder;Lcom/android/settings/search2/SearchResult;[Landroid/util/Pair;)V

    iget-object v0, p2, Lcom/android/settings/search2/SearchResult;->payload:Lcom/android/settings/search2/ResultPayload;

    invoke-virtual {v0}, Lcom/android/settings/search2/ResultPayload;->getIntent()Landroid/content/Intent;

    move-result-object v0

    instance-of v1, p2, Lcom/android/settings/search2/AppSearchResult;

    if-eqz v1, :cond_0

    check-cast p2, Lcom/android/settings/search2/AppSearchResult;

    invoke-virtual {p2}, Lcom/android/settings/search2/AppSearchResult;->getAppUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settings/search2/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/app/Activity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/search2/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {p1, v0}, Lcom/android/settings/search2/SearchFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "IntentSearchViewHolder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cannot launch search result, title: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/android/settings/search2/SearchResult;->title:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onBind(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/settings/search2/SearchViewHolder;->onBind(Lcom/android/settings/search2/SearchFragment;Lcom/android/settings/search2/SearchResult;)V

    iget-object v0, p0, Lcom/android/settings/search2/IntentSearchViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/android/settings/search2/-$Lambda$N4jzUF57BncNb8XTYrMZd_K4_Pw;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/search2/-$Lambda$N4jzUF57BncNb8XTYrMZd_K4_Pw;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
