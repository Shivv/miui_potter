.class Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;
.super Ljava/lang/Object;
.source "DatabaseIndexingManager.java"


# instance fields
.field public dataToDisable:Ljava/util/List;

.field public dataToUpdate:Ljava/util/List;

.field public nonIndexableKeys:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->dataToUpdate:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->dataToDisable:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->nonIndexableKeys:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->dataToUpdate:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->dataToUpdate:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->dataToDisable:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->dataToDisable:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->nonIndexableKeys:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->nonIndexableKeys:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->dataToUpdate:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->dataToDisable:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;->nonIndexableKeys:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public copy()Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;
    .locals 1

    new-instance v0, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;

    invoke-direct {v0, p0}, Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;-><init>(Lcom/android/settings/search2/DatabaseIndexingManager$UpdateData;)V

    return-object v0
.end method
