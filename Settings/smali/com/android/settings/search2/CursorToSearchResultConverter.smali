.class Lcom/android/settings/search2/CursorToSearchResultConverter;
.super Ljava/lang/Object;
.source "CursorToSearchResultConverter.java"


# static fields
.field private static final prioritySettings:Ljava/util/Set;

.field private static final whiteList:[Ljava/lang/String;


# instance fields
.field private final LONG_TITLE_LENGTH:I

.field private final TAG:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mKeys:Ljava/util/Set;

.field private final mQueryText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "main_toggle_wifi"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "main_toggle_bluetooth"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "toggle_airplane"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string/jumbo v1, "tether_settings"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string/jumbo v1, "battery_saver"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string/jumbo v1, "toggle_nfc"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "restrict_background"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string/jumbo v1, "data_usage_enable"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string/jumbo v1, "button_roaming_key"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/search2/CursorToSearchResultConverter;->whiteList:[Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/android/settings/search2/CursorToSearchResultConverter;->whiteList:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/settings/search2/CursorToSearchResultConverter;->prioritySettings:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "CursorConverter"

    iput-object v0, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->TAG:Ljava/lang/String;

    const/16 v0, 0x14

    iput v0, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->LONG_TITLE_LENGTH:I

    iput-object p1, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->mKeys:Ljava/util/Set;

    iput-object p2, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->mQueryText:Ljava/lang/String;

    return-void
.end method

.method private buildSingleSearchResultFromCursor(Lcom/android/settings/dashboard/l;Ljava/util/Map;Landroid/database/Cursor;I)Lcom/android/settings/search2/SearchResult;
    .locals 10

    const/4 v0, 0x0

    const/4 v9, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->mKeys:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v9

    :cond_0
    iget-object v1, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->mKeys:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x8

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x7

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x4

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v0, 0xa

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x6

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v0, 0xb

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0xc

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v1, v0}, Lcom/android/settings/search2/CursorToSearchResultConverter;->getUnmarshalledPayload([BI)Lcom/android/settings/search2/ResultPayload;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, p3}, Lcom/android/settings/search2/CursorToSearchResultConverter;->getBreadcrumbs(Lcom/android/settings/dashboard/l;Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v6, v1, p4, v3}, Lcom/android/settings/search2/CursorToSearchResultConverter;->getRank(Ljava/lang/String;Ljava/util/List;ILjava/lang/String;)I

    move-result v2

    new-instance v3, Lcom/android/settings/search2/SearchResult$Builder;

    invoke-direct {v3}, Lcom/android/settings/search2/SearchResult$Builder;-><init>()V

    invoke-virtual {v3, v6}, Lcom/android/settings/search2/SearchResult$Builder;->addTitle(Ljava/lang/CharSequence;)Lcom/android/settings/search2/SearchResult$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/android/settings/search2/SearchResult$Builder;->addSummary(Ljava/lang/CharSequence;)Lcom/android/settings/search2/SearchResult$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/android/settings/search2/SearchResult$Builder;->addBreadcrumbs(Ljava/util/List;)Lcom/android/settings/search2/SearchResult$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/settings/search2/SearchResult$Builder;->addRank(I)Lcom/android/settings/search2/SearchResult$Builder;

    move-result-object v1

    invoke-direct {p0, p2, v5, v4, v8}, Lcom/android/settings/search2/CursorToSearchResultConverter;->getIconForPackage(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/search2/SearchResult$Builder;->addIcon(Landroid/graphics/drawable/Drawable;)Lcom/android/settings/search2/SearchResult$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/search2/SearchResult$Builder;->addPayload(Lcom/android/settings/search2/ResultPayload;)Lcom/android/settings/search2/SearchResult$Builder;

    invoke-virtual {v3}, Lcom/android/settings/search2/SearchResult$Builder;->build()Lcom/android/settings/search2/SearchResult;

    move-result-object v0

    return-object v0

    :cond_1
    if-nez v0, :cond_2

    move-object v0, p0

    move-object v1, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/search2/CursorToSearchResultConverter;->getIntentPayload(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/search2/IntentPayload;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "CursorConverter"

    const-string/jumbo v1, "Error creating payload - bad marshalling data or mismatched types"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-object v9
.end method

.method private getBreadcrumbs(Lcom/android/settings/dashboard/l;Landroid/database/Cursor;)Ljava/util/List;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez p1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v0, v2, v1}, Lcom/android/settings/dashboard/l;->Dj(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private getIconForPackage(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-nez v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_2
    :try_start_1
    invoke-virtual {v0, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v0, "CursorConverter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cannot create Context for package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->mContext:Landroid/content/Context;

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method private getIntentPayload(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/search2/IntentPayload;
    .locals 8

    const/4 v4, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, ":settings:fragment_args_key"

    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/search2/CursorToSearchResultConverter;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/16 v7, 0x22

    move-object v1, p4

    move v6, v4

    invoke-static/range {v0 .. v7}, Lcom/android/settings/aq;->bqs(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ILjava/lang/CharSequence;ZI)Landroid/content/Intent;

    move-result-object v1

    new-instance v0, Lcom/android/settings/search2/IntentPayload;

    invoke-direct {v0, v1}, Lcom/android/settings/search2/IntentPayload;-><init>(Landroid/content/Intent;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, p5, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :cond_1
    const-string/jumbo v0, ":settings:fragment_args_key"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Lcom/android/settings/search2/IntentPayload;

    invoke-direct {v0, v1}, Lcom/android/settings/search2/IntentPayload;-><init>(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private getRank(Ljava/lang/String;Ljava/util/List;ILjava/lang/String;)I
    .locals 2

    sget-object v0, Lcom/android/settings/search2/CursorToSearchResultConverter;->prioritySettings:Ljava/util/Set;

    invoke-interface {v0, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/search2/DatabaseResultLoader;->BASE_RANKS:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    if-ge p3, v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x14

    if-le v0, v1, :cond_1

    add-int/lit8 v0, p3, 0x2

    return v0

    :cond_1
    return p3
.end method

.method private getUnmarshalledPayload([BI)Lcom/android/settings/search2/ResultPayload;
    .locals 4

    packed-switch p2, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    :try_start_0
    sget-object v0, Lcom/android/settings/search2/InlineSwitchPayload;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, Lcom/android/settings/search2/ResultPayloadUtils;->unmarshall([BLandroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/search2/ResultPayload;
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "CursorConverter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error creating parcelable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public convertCursor(Lcom/android/settings/dashboard/l;Landroid/database/Cursor;I)Ljava/util/List;
    .locals 3

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/android/settings/search2/CursorToSearchResultConverter;->buildSingleSearchResultFromCursor(Lcom/android/settings/dashboard/l;Ljava/util/Map;Landroid/database/Cursor;I)Lcom/android/settings/search2/SearchResult;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v1
.end method
