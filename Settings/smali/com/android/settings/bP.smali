.class public abstract Lcom/android/settings/bP;
.super Ljava/lang/Object;
.source "BaseEnabler.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field protected bUb:Landroid/app/Activity;

.field protected bUc:Z

.field protected bUd:Landroid/database/ContentObserver;

.field protected bUe:Lmiui/widget/SlidingButton;

.field protected bUf:Landroid/widget/TextView;


# virtual methods
.method public bNu(Landroid/widget/TextView;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bP;->bUf:Landroid/widget/TextView;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/settings/bP;->bUf:Landroid/widget/TextView;

    return-void
.end method

.method public bNv(Lmiui/widget/SlidingButton;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/bP;->bUe:Lmiui/widget/SlidingButton;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lmiui/widget/SlidingButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bP;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Lcom/android/settings/bP;->bNv(Lmiui/widget/SlidingButton;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bP;->bUe:Lmiui/widget/SlidingButton;

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iput-object p1, p0, Lcom/android/settings/bP;->bUe:Lmiui/widget/SlidingButton;

    invoke-virtual {p0}, Lcom/android/settings/bP;->bNy()V

    iget-object v0, p0, Lcom/android/settings/bP;->bUe:Lmiui/widget/SlidingButton;

    invoke-virtual {v0, p0}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public bNw()V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/settings/bP;->bNy()V

    invoke-virtual {p0}, Lcom/android/settings/bP;->bNx()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bP;->bUb:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/bP;->bNx()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bP;->bUd:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bP;->bUe:Lmiui/widget/SlidingButton;

    invoke-virtual {v0, p0}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iput-boolean v3, p0, Lcom/android/settings/bP;->bUc:Z

    return-void
.end method

.method protected abstract bNx()Landroid/net/Uri;
.end method

.method protected abstract bNy()V
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    return-void
.end method

.method public pause()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/bP;->bNx()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bP;->bUb:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bP;->bUd:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bP;->bUe:Lmiui/widget/SlidingButton;

    invoke-virtual {v0, v2}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bP;->bUc:Z

    return-void
.end method
