.class final Lcom/android/settings/search/SearchUpdater$UpdateHandler;
.super Landroid/os/Handler;
.source "SearchUpdater.java"


# instance fields
.field final synthetic this$0:Lcom/android/settings/search/SearchUpdater;


# direct methods
.method constructor <init>(Lcom/android/settings/search/SearchUpdater;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->this$0:Lcom/android/settings/search/SearchUpdater;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->this$0:Lcom/android/settings/search/SearchUpdater;

    invoke-static {v0}, Lcom/android/settings/search/SearchUpdater;->-get0(Lcom/android/settings/search/SearchUpdater;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/app/AppGlobals;->getInitialApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.providers.appindex"

    iget-object v2, p0, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->this$0:Lcom/android/settings/search/SearchUpdater;

    invoke-static {v2}, Lcom/android/settings/search/SearchUpdater;->-get0(Lcom/android/settings/search/SearchUpdater;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    iget-object v0, p0, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->this$0:Lcom/android/settings/search/SearchUpdater;

    invoke-static {v0}, Lcom/android/settings/search/SearchUpdater;->-get0(Lcom/android/settings/search/SearchUpdater;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->this$0:Lcom/android/settings/search/SearchUpdater;

    invoke-static {v0}, Lcom/android/settings/search/SearchUpdater;->-get1(Lcom/android/settings/search/SearchUpdater;)Lcom/android/settings/search/SearchUpdater$TaskManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/search/SearchUpdater$TaskManager;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "SearchUpdater"

    const-string/jumbo v2, "error occures when applyBatch"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->this$0:Lcom/android/settings/search/SearchUpdater;

    invoke-static {v0}, Lcom/android/settings/search/SearchUpdater;->-get1(Lcom/android/settings/search/SearchUpdater;)Lcom/android/settings/search/SearchUpdater$TaskManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/search/SearchUpdater$TaskManager;->get()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->this$0:Lcom/android/settings/search/SearchUpdater;

    invoke-static {v1}, Lcom/android/settings/search/SearchUpdater;->-get1(Lcom/android/settings/search/SearchUpdater;)Lcom/android/settings/search/SearchUpdater$TaskManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/settings/search/SearchUpdater$TaskManager;->remove(I)V

    iget-object v1, p0, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->this$0:Lcom/android/settings/search/SearchUpdater;

    invoke-static {v1, v0}, Lcom/android/settings/search/SearchUpdater;->-wrap0(Lcom/android/settings/search/SearchUpdater;I)V

    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    invoke-virtual {p0, v0}, Lcom/android/settings/search/SearchUpdater$UpdateHandler;->sendEmptyMessage(I)Z

    return-void
.end method
