.class Lcom/android/settings/search/provider/SettingsTreeHelper;
.super Ljava/lang/Object;
.source "SettingsTreeHelper.java"


# static fields
.field private static final ASSET_FILENAME:Ljava/lang/String; = "assets/index.json"

.field private static final BUILD_TREE:Ljava/lang/String; = "tree"

.field private static final BUILD_UTC:Ljava/lang/String; = "ro.build.date.utc"

.field private static final FILENAME:Ljava/lang/String; = "index.json"

.field private static final TAG:Ljava/lang/String; = "SettingsTreeHelper"

.field private static sInstance:Lcom/android/settings/search/provider/SettingsTreeHelper;


# instance fields
.field private mFile:Ljava/io/File;

.field private mTree:Lcom/miui/internal/search/SettingsTree;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    :cond_0
    new-instance v2, Ljava/io/File;

    const-string/jumbo v3, "index.json"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mFile:Ljava/io/File;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setContextClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v1, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v1}, Lcom/miui/internal/search/SearchUtils;->readJSONObject(Ljava/io/InputStream;)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v2, "ro.build.date.utc"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v3, "ro.build.date.utc"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v2, v3, :cond_1

    const-string/jumbo v2, "tree"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/miui/internal/search/SettingsTree;->newInstance(Landroid/content/Context;Lorg/json/JSONObject;Z)Lcom/miui/internal/search/SettingsTree;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    if-nez v1, :cond_2

    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string/jumbo v2, "index.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/internal/search/SearchUtils;->readJSONObject(Ljava/io/InputStream;)Lorg/json/JSONObject;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p1, v1, v2}, Lcom/miui/internal/search/SettingsTree;->newInstance(Landroid/content/Context;Lorg/json/JSONObject;Z)Lcom/miui/internal/search/SettingsTree;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    invoke-direct {p0, v4}, Lcom/android/settings/search/provider/SettingsTreeHelper;->commit(Z)V

    return-void

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    if-eqz v0, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/RuntimeException;->addSuppressed(Ljava/lang/Throwable;)V

    :cond_3
    throw v2

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private commit(Z)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    invoke-virtual {v0}, Lcom/miui/internal/search/SettingsTree;->needCommit()Z

    move-result v0

    if-nez v0, :cond_0

    xor-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mFile:Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    invoke-virtual {v2, v1}, Lcom/miui/internal/search/SettingsTree;->dispatchCommit(Ljava/lang/StringBuilder;)V

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string/jumbo v3, "tree"

    new-instance v4, Lorg/json/JSONObject;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string/jumbo v2, "ro.build.date.utc"

    const-string/jumbo v3, "ro.build.date.utc"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    if-eqz v0, :cond_1

    :try_start_2
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string/jumbo v0, "SettingsTreeHelper"

    const-string/jumbo v1, "close file error!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_1
    :try_start_4
    const-string/jumbo v1, "SettingsTreeHelper"

    const-string/jumbo v2, "commit error!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v0, :cond_1

    :try_start_5
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_6
    const-string/jumbo v0, "SettingsTreeHelper"

    const-string/jumbo v1, "close file error!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catchall_1
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    :try_start_7
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_2
    :goto_3
    :try_start_8
    throw v0

    :catch_3
    move-exception v1

    const-string/jumbo v1, "SettingsTreeHelper"

    const-string/jumbo v2, "close file error!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/settings/search/provider/SettingsTreeHelper;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/settings/search/provider/SettingsTreeHelper;->getInstance(Landroid/content/Context;Z)Lcom/android/settings/search/provider/SettingsTreeHelper;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Z)Lcom/android/settings/search/provider/SettingsTreeHelper;
    .locals 6

    const/4 v2, 0x0

    const-class v1, Lcom/android/settings/search/provider/SettingsTreeHelper;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lmiui/os/Build;->IS_CTA_BUILD:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/search/provider/SettingsTreeHelper;

    invoke-direct {v0}, Lcom/android/settings/search/provider/SettingsTreeHelper;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/android/settings/search/provider/SettingsTreeHelper;->sInstance:Lcom/android/settings/search/provider/SettingsTreeHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_3

    if-nez p1, :cond_1

    monitor-exit v1

    return-object v2

    :cond_1
    if-nez p0, :cond_2

    :try_start_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "instance not prepared yet"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v0, Lcom/android/settings/search/provider/SettingsTreeHelper;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/android/settings/search/provider/SettingsTreeHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/search/provider/SettingsTreeHelper;->sInstance:Lcom/android/settings/search/provider/SettingsTreeHelper;

    long-to-double v2, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-double v4, v4

    const-string/jumbo v0, "-"

    invoke-static {v2, v3, v4, v5, v0}, Lcom/miui/internal/search/SearchUtils;->logCost(DDLjava/lang/Object;)V

    :cond_3
    sget-object v0, Lcom/android/settings/search/provider/SettingsTreeHelper;->sInstance:Lcom/android/settings/search/provider/SettingsTreeHelper;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public delete(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    if-nez v0, :cond_0

    return v2

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/miui/internal/search/SettingsTree;->dispatchDelete(Ljava/lang/String;[Ljava/lang/String;Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    invoke-direct {p0, v2}, Lcom/android/settings/search/provider/SettingsTreeHelper;->commit(Z)V

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public insert(Landroid/content/ContentValues;)Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    invoke-virtual {v0, p1}, Lcom/miui/internal/search/SettingsTree;->dispatchInsert(Landroid/content/ContentValues;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/settings/search/provider/SettingsTreeHelper;->commit(Z)V

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    if-nez v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    invoke-virtual {v0, p1, p2}, Lcom/miui/internal/search/SettingsTree;->dispatchOnReceive(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/search/provider/SettingsTreeHelper;->commit(Z)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public query([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/search/provider/SettingsTreeHelper;->commit(Z)V

    new-instance v1, Lcom/miui/internal/search/RankedResult;

    invoke-direct {v1, p1}, Lcom/miui/internal/search/RankedResult;-><init>([Ljava/lang/String;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/internal/search/SettingsTree;->dispatchQuery(Landroid/database/MatrixCursor;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public update(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    if-nez v0, :cond_0

    return v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/search/provider/SettingsTreeHelper;->mTree:Lcom/miui/internal/search/SettingsTree;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/internal/search/SettingsTree;->dispatchUpdate(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    invoke-direct {p0, v1}, Lcom/android/settings/search/provider/SettingsTreeHelper;->commit(Z)V

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
