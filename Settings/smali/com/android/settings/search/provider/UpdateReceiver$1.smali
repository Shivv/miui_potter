.class final Lcom/android/settings/search/provider/UpdateReceiver$1;
.super Ljava/lang/Object;
.source "UpdateReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/android/settings/search/provider/UpdateReceiver;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/settings/search/provider/UpdateReceiver;Landroid/content/Intent;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/search/provider/UpdateReceiver$1;->this$0:Lcom/android/settings/search/provider/UpdateReceiver;

    iput-object p2, p0, Lcom/android/settings/search/provider/UpdateReceiver$1;->val$intent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/android/settings/search/provider/UpdateReceiver$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/search/provider/UpdateReceiver$1;->val$intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/miui/internal/search/SearchUtils;->clearPackageExistedCache()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/search/provider/UpdateReceiver$1;->val$context:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/search/provider/SettingsTreeHelper;->getInstance(Landroid/content/Context;Z)Lcom/android/settings/search/provider/SettingsTreeHelper;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/search/provider/UpdateReceiver$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/search/provider/UpdateReceiver$1;->val$intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/search/provider/SettingsTreeHelper;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_2
    return-void
.end method
