.class Lcom/android/settings/search/KeySettingsUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;
.source "KeySettingsUpdateHelper.java"


# static fields
.field private static final FORCE_TOUCH_RESOURCE:Ljava/lang/String; = "force_touch"

.field private static final LONG_PRESS_VOLUME_DOWN_RESOURCE:Ljava/lang/String; = "long_press_volume_down"

.field private static final SCREEN_BUTTON_HIDE_RESOURCE:Ljava/lang/String; = "status_bar_screen_button_key_force_immersive"

.field private static final SCREEN_MAX_ASPECT_RATIO_RESOURCE:Ljava/lang/String; = "screen_max_aspect_ratio_title"

.field private static final SWITCH_SCREEN_BUTTON_ORDER_RESOURCE:Ljava/lang/String; = "switch_screen_button_order"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method private static removeLongPressVolumeDown(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f030096

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    const-string/jumbo v2, "support_camera_quick_snap"

    invoke-static {v2, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "Street-snap-picture"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "Street-snap-movie"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "key_trans_card_in_ese"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v0, "public_transportation_shortcuts"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1

    invoke-static {}, Landroid/provider/MiuiSettings$ForceTouch;->isSupport()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "force_touch"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/KeySettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Landroid/view/IWindowManager;->hasNavigationBar()Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "status_bar_screen_button_key_force_immersive"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/KeySettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "switch_screen_button_order"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/KeySettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "screen_max_aspect_ratio_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/KeySettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/android/settings/search/KeySettingsUpdateHelper;->removeLongPressVolumeDown(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "long_press_volume_down"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/KeySettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
