.class Lcom/android/settings/search/BackupSettingsUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;
.source "BackupSettingsUpdateHelper.java"


# static fields
.field private static final AUTO_RESTORE_RESOURCE:Ljava/lang/String; = "auto_restore_title"

.field private static final BACKUP_DATA_RESOURCE:Ljava/lang/String; = "backup_data_title"

.field private static final CLOUD_BACKUP_RESOURCE:Ljava/lang/String; = "cloud_backup_settings_section_title"

.field private static final CLOUD_RESTORE_RESOURCE:Ljava/lang/String; = "cloud_restore_section_title"

.field private static final CONFIGURE_ACCOUNT_RESOURCE:Ljava/lang/String; = "backup_configure_account_title"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-static {p0}, Lmiui/accounts/ExtraAccountManager;->getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "cloud_restore_section_title"

    invoke-static {p0, p1, v1, v2}, Lcom/android/settings/search/BackupSettingsUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    const-string/jumbo v1, "cloud_backup_settings_section_title"

    invoke-static {p0, p1, v1, v2}, Lcom/android/settings/search/BackupSettingsUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v3, "com.google.settings"

    invoke-virtual {v1, v3, v0}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_0
    if-eqz v1, :cond_3

    const-string/jumbo v0, "backup_data_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/BackupSettingsUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "backup_configure_account_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/BackupSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "auto_restore_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/BackupSettingsUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "backup"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Landroid/app/backup/IBackupManager;->isBackupEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :try_start_1
    invoke-interface {v3}, Landroid/app/backup/IBackupManager;->getCurrentTransport()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/app/backup/IBackupManager;->getConfigurationIntent(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    if-eqz v3, :cond_4

    move v0, v1

    :cond_4
    :goto_2
    if-nez v1, :cond_5

    const-string/jumbo v1, "auto_restore_title"

    invoke-static {p0, p1, v1, v2}, Lcom/android/settings/search/BackupSettingsUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    :cond_5
    if-nez v0, :cond_1

    const-string/jumbo v0, "backup_configure_account_title"

    invoke-static {p0, p1, v0, v2}, Lcom/android/settings/search/BackupSettingsUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    goto :goto_1

    :catch_0
    move-exception v1

    move v1, v0

    goto :goto_2

    :catch_1
    move-exception v3

    goto :goto_2
.end method
