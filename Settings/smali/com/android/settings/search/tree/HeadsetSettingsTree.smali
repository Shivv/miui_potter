.class public Lcom/android/settings/search/tree/HeadsetSettingsTree;
.super Lcom/miui/internal/search/SettingsTree;
.source "HeadsetSettingsTree.java"


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/miui/internal/search/SettingsTree;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/internal/search/SettingsTree;-><init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/miui/internal/search/SettingsTree;Z)V

    return-void
.end method

.method private getHiFiState(Landroid/content/Context;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/miui/a/a/a/h;->ctk()Z

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const-string/jumbo v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const-string/jumbo v2, "hifi_mode"

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 3

    const-string/jumbo v0, "resource"

    invoke-virtual {p0, v0}, Lcom/android/settings/search/tree/HeadsetSettingsTree;->getColumnValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/cd;->bQm(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/settings/cd;->bQn()Z

    move-result v0

    :goto_0
    const-string/jumbo v2, "music_headset_calibrate"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/settings/HeadsetCalibrateActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-super {p0}, Lcom/miui/internal/search/SettingsTree;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getStatus()I
    .locals 4

    const/4 v0, 0x3

    const/4 v1, 0x1

    const-string/jumbo v2, "resource"

    invoke-virtual {p0, v2}, Lcom/android/settings/search/tree/HeadsetSettingsTree;->getColumnValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "music_mi_effect_title"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/settings/cd;->bQm(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/android/settings/search/tree/HeadsetSettingsTree;->getHiFiState(Landroid/content/Context;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const-string/jumbo v3, "music_equalizer"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v2, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/settings/cd;->bQm(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/android/settings/search/tree/HeadsetSettingsTree;->getHiFiState(Landroid/content/Context;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/miui/a/a/a/h;->cth()Lcom/miui/a/a/a/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/a/a/a/h;->initialize()V

    :try_start_0
    iget-object v3, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/miui/a/a/a/h;->isEnabled(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/miui/a/a/a/h;->csx()V

    return v0

    :cond_2
    invoke-virtual {v2}, Lcom/miui/a/a/a/h;->csx()V

    return v1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/miui/a/a/a/h;->csx()V

    throw v0

    :cond_3
    return v1

    :cond_4
    const-string/jumbo v3, "music_hifi_title"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v2, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/settings/cd;->bQm(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_1
    return v0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    const-string/jumbo v3, "music_headset_calibrate"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/settings/cd;->bQm(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-static {}, Lcom/android/settings/cd;->bQn()Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    move v1, v0

    :cond_8
    return v1

    :cond_9
    invoke-super {p0}, Lcom/miui/internal/search/SettingsTree;->getStatus()I

    move-result v0

    return v0
.end method

.method public initialize()Z
    .locals 3

    const/4 v2, 0x1

    const-string/jumbo v0, "resource"

    invoke-virtual {p0, v0}, Lcom/android/settings/search/tree/HeadsetSettingsTree;->getColumnValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "music_hifi_title"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/miui/a/a/a/h;->ctk()Z

    move-result v0

    if-nez v0, :cond_0

    return v2

    :cond_0
    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "scorpio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "lithium"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string/jumbo v0, "resource"

    const-string/jumbo v1, "music_hd_title"

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/search/tree/HeadsetSettingsTree;->setColumnValue(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-super {p0}, Lcom/miui/internal/search/SettingsTree;->initialize()Z

    move-result v0

    return v0

    :cond_3
    const-string/jumbo v1, "music_mi_effect_title"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string/jumbo v1, "music_equalizer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/android/settings/search/tree/HeadsetSettingsTree;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_2

    return v2

    :cond_5
    const-string/jumbo v1, "music_title_dolby_control"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "aries"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    return v2
.end method
