.class public Lcom/android/settings/search/tree/SecondSpaceSettingsTree;
.super Lcom/miui/internal/search/SettingsTree;
.source "SecondSpaceSettingsTree.java"


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/miui/internal/search/SettingsTree;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/internal/search/SettingsTree;-><init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/miui/internal/search/SettingsTree;Z)V

    return-void
.end method


# virtual methods
.method protected getStatus()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search/tree/SecondSpaceSettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/MiuiSettings;->bgw(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-super {p0}, Lcom/miui/internal/search/SettingsTree;->getStatus()I

    move-result v0

    return v0
.end method
