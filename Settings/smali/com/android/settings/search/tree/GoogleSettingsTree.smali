.class public Lcom/android/settings/search/tree/GoogleSettingsTree;
.super Lcom/miui/internal/search/SettingsTree;
.source "GoogleSettingsTree.java"


# static fields
.field static final ACTIVITY_NAME:Ljava/lang/String; = "activityName"


# instance fields
.field private final mActivityName:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/miui/internal/search/SettingsTree;Z)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/internal/search/SettingsTree;-><init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/miui/internal/search/SettingsTree;Z)V

    const-string/jumbo v0, "activityName"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search/tree/GoogleSettingsTree;->mActivityName:Ljava/lang/String;

    const-string/jumbo v0, "title"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search/tree/GoogleSettingsTree;->mTitle:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/android/settings/search/tree/GoogleSettingsTree;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/search/tree/GoogleSettingsTree;->mActivityName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getTitle(Z)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search/tree/GoogleSettingsTree;->mTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/search/tree/GoogleSettingsTree;->mTitle:Ljava/lang/String;

    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/miui/internal/search/SettingsTree;->getTitle(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
