.class public Lcom/android/settings/search/SimSettingsUpdateHelper;
.super Ljava/lang/Object;
.source "SimSettingsUpdateHelper.java"


# static fields
.field private static final ALWAYS_ENABLE_MMS_RESOURCE:Ljava/lang/String; = "always_enable_mms"

.field private static final DATA_ENABLED_RESOURCE:Ljava/lang/String; = "data_enabled"

.field private static final DATA_USAGE_RESOURCE:Ljava/lang/String; = "preference_data_usage_title"

.field private static final VOLTE_SWITCH_RESOURCE:Ljava/lang/String; = "volte_switch_title"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 3

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/dc;->bYM(Landroid/content/Context;)Z

    move-result v0

    const-string/jumbo v1, "data_enabled"

    xor-int/lit8 v2, v0, 0x1

    invoke-static {p0, p1, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    const-string/jumbo v1, "volte_switch_title"

    xor-int/lit8 v2, v0, 0x1

    invoke-static {p0, p1, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    const-string/jumbo v1, "preference_data_usage_title"

    xor-int/lit8 v2, v0, 0x1

    invoke-static {p0, p1, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    const-string/jumbo v1, "always_enable_mms"

    xor-int/lit8 v0, v0, 0x1

    invoke-static {p0, p1, v1, v0}, Lcom/android/settings/search/BaseSearchUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    return-void
.end method
