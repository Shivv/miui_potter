.class Lcom/android/settings/search/WirelessUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;
.source "WirelessUpdateHelper.java"


# static fields
.field private static final ACTION_MI_PLAY:Ljava/lang/String; = "miui.intent.action.MIPLAY"

.field private static final BLUETOOTH_TETHER_RESOURCE:Ljava/lang/String; = "bluetooth_tether_checkbox_text"

.field private static final CELL_BROADCAST_RESOURCE:Ljava/lang/String; = "cell_broadcast_settings"

.field private static final DETECTION_RESOURCE:Ljava/lang/String; = "wifi_poor_network_detection"

.field private static final DIALOG_REMIND_RESOURCE:Ljava/lang/String; = "wifi_dialog_remind_type_title"

.field private static final ENABLE_WFD_RESOURCE:Ljava/lang/String; = "enable_wifi_display"

.field private static final FREQUENCY_BAND_RESOURCE:Ljava/lang/String; = "wifi_setting_frequency_band_title"

.field private static final GSM_TO_WIFI_CONNECT_TYPE_RESOURCE:Ljava/lang/String; = "gsm_to_wifi_connect_type_title"

.field private static final NFC_PAYMENT_RESOURCE:Ljava/lang/String; = "nfc_payment_settings_title"

.field private static final NFC_SE_ROUTE_RESOURCE:Ljava/lang/String; = "nfc_se_route_title"

.field private static final NFC_SE_WALLET_RESOURCE:Ljava/lang/String; = "nfc_se_wallet_title"

.field private static final PRIORITY_SETTINGS_RESOURCE:Ljava/lang/String; = "wifi_priority_settings_title"

.field private static final PRIORITY_TYPE_RESOURCE:Ljava/lang/String; = "wifi_priority_type_title"

.field private static final SELECT_SSID_RESOURCE:Ljava/lang/String; = "select_ssid_type_title"

.field private static final USB_TETHER_RESOURCE:Ljava/lang/String; = "usb_tethering_button_text"

.field private static final VOLTE_SWITCH_RESOURCE:Ljava/lang/String; = "volte_switch_title"

.field private static final WAPI_CERT_INSTALL_RESOURCE:Ljava/lang/String; = "wifi_wapi_cert_install"

.field private static final WAPI_CERT_UNINSTALL_RESOURCE:Ljava/lang/String; = "wifi_wapi_cert_uninstall"

.field private static final WFD_SETTINGS_RESOURCE:Ljava/lang/String; = "wfd_settings_title"

.field private static final WIFI_AUTOMATICALLY_CONNECT_RESOURCE:Ljava/lang/String; = "wifi_automatically_connect_title"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x5

    const/4 v6, 0x0

    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManagerEx;->isVolteEnabledByPlatform()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "volte_switch_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "wifi_automatically_connect_title"

    invoke-static {p0, p1, v2}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1
    const-string/jumbo v2, "support_wapi"

    invoke-static {v2, v6}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v2, "mediatek"

    const-string/jumbo v3, "vendor"

    invoke-static {v3}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    const-string/jumbo v2, "wifi_wapi_cert_install"

    invoke-static {p0, p1, v2}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v2, "wifi_wapi_cert_uninstall"

    invoke-static {p0, p1, v2}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isDualBandSupported()Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "wifi_setting_frequency_band_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_3
    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-nez v0, :cond_4

    const-string/jumbo v0, "wifi_priority_type_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "wifi_priority_settings_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "gsm_to_wifi_connect_type_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "select_ssid_type_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "wifi_dialog_remind_type_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_4
    invoke-static {p0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "wifi_poor_network_detection"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_5
    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-nez v0, :cond_7

    const-string/jumbo v0, "pisces"

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    const-string/jumbo v0, "nfc_se_route_title"

    invoke-static {p0, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v3, "name"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f120b71

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1, v0, v3, v4}, Lcom/android/settings/search/WirelessUpdateHelper;->updateItemData(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string/jumbo v0, "nfc_se_route_title"

    const-string/jumbo v2, "nfc_se_wallet_title"

    invoke-static {p0, p1, v0, v2}, Lcom/android/settings/search/WirelessUpdateHelper;->updatePath(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    :try_start_0
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/cardemulation/CardEmulation;->getInstance(Landroid/nfc/NfcAdapter;)Landroid/nfc/cardemulation/CardEmulation;

    move-result-object v0

    const-string/jumbo v2, "payment"

    invoke-virtual {v0, v2}, Landroid/nfc/cardemulation/CardEmulation;->getServices(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_8

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    const-string/jumbo v0, "nfc_payment_settings_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_9
    invoke-static {p0, p1}, Lcom/android/settings/search/WirelessUpdateHelper;->updateCellBroadcast(Landroid/content/Context;Ljava/util/ArrayList;)V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.MIPLAY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "wfd_settings_title"

    invoke-static {p0, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "intent_action"

    aput-object v3, v2, v6

    const-string/jumbo v3, "intent_data"

    aput-object v3, v2, v8

    const-string/jumbo v3, "dest_package"

    aput-object v3, v2, v9

    const-string/jumbo v3, "dest_class"

    const/4 v4, 0x3

    aput-object v3, v2, v4

    const-string/jumbo v3, "fragment"

    const/4 v4, 0x4

    aput-object v3, v2, v4

    new-array v3, v7, [Ljava/lang/String;

    const-string/jumbo v4, "miui.intent.action.MIPLAY"

    aput-object v4, v3, v6

    const-string/jumbo v4, ""

    aput-object v4, v3, v8

    const-string/jumbo v4, ""

    aput-object v4, v3, v9

    const-string/jumbo v4, ""

    const/4 v5, 0x3

    aput-object v4, v3, v5

    const-string/jumbo v4, ""

    const/4 v5, 0x4

    aput-object v4, v3, v5

    invoke-static {p1, v0, v7, v2, v3}, Lcom/android/settings/search/WirelessUpdateHelper;->updateSearchItem(Ljava/util/ArrayList;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v0, v1

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v0, "enable_wifi_display"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_b
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherableUsbRegexs()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_c

    const-string/jumbo v1, "usb_tethering_button_text"

    invoke-static {p0, p1, v1}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_c
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_d

    const-string/jumbo v0, "bluetooth_tether_checkbox_text"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_d
    return-void
.end method

.method private static updateCellBroadcast(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x110a0007

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    :try_start_0
    invoke-static {p0}, Lcom/android/settings/e/a;->aZJ(Landroid/content/Context;)V

    if-eqz v1, :cond_1

    const-string/jumbo v4, "com.android.cellbroadcastreceiver"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    move v1, v2

    :cond_0
    invoke-static {v3}, Lcom/android/settings/e/a;->aZH(Landroid/content/pm/PackageManager;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    const-string/jumbo v1, "no_config_cell_broadcasts"

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string/jumbo v0, "cell_broadcast_settings"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/WirelessUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_3
    return-void

    :catch_0
    move-exception v1

    move v1, v2

    goto :goto_0
.end method
