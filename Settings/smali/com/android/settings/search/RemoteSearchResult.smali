.class public Lcom/android/settings/search/RemoteSearchResult;
.super Ljava/lang/Object;
.source "RemoteSearchResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final path:Ljava/lang/String;

.field private final score:I

.field private final searchOrigin:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/search/RemoteSearchResult$1;

    invoke-direct {v0}, Lcom/android/settings/search/RemoteSearchResult$1;-><init>()V

    sput-object v0, Lcom/android/settings/search/RemoteSearchResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->title:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->path:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/settings/search/RemoteSearchResult;->score:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->searchOrigin:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->title:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->path:Ljava/lang/String;

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/android/settings/search/RemoteSearchResult;->score:I

    iput-object p1, p0, Lcom/android/settings/search/RemoteSearchResult;->searchOrigin:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/search/RemoteSearchResult;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/search/RemoteSearchResult;->path:Ljava/lang/String;

    iput p3, p0, Lcom/android/settings/search/RemoteSearchResult;->score:I

    iput-object p4, p0, Lcom/android/settings/search/RemoteSearchResult;->searchOrigin:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getScore()I
    .locals 1

    iget v0, p0, Lcom/android/settings/search/RemoteSearchResult;->score:I

    return v0
.end method

.method public getSearchOrigin()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->searchOrigin:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->title:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->path:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/settings/search/RemoteSearchResult;->score:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/settings/search/RemoteSearchResult;->searchOrigin:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
