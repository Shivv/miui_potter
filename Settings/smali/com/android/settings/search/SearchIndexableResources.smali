.class public final Lcom/android/settings/search/SearchIndexableResources;
.super Ljava/lang/Object;
.source "SearchIndexableResources.java"


# static fields
.field public static final NO_DATA_RES_ID:I

.field static final sResMap:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const v7, 0x7f080243

    const v6, 0x7f08025c

    const v5, 0x7f080255

    const v4, 0x7f080253

    const/4 v3, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/settings/search/SearchIndexableResources;->sResMap:Ljava/util/HashMap;

    const-class v0, Lcom/android/settings/wifi/WifiSettings;

    const v1, 0x7f080267

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/network/NetworkDashboardFragment;

    const v1, 0x7f080267

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/wifi/ConfigureWifiSettings;

    const v1, 0x7f080267

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/wifi/SavedAccessPointsWifiSettings;

    const v1, 0x7f080267

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/bluetooth/BluetoothSettings;

    const v1, 0x7f08024a

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/sim/SimSettings;

    const v1, 0x7f08026d

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/datausage/DataUsageSummary;

    const v1, 0x7f08024d

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/datausage/DataUsageMeteredSettings;

    const v1, 0x7f08024d

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/display/ScreenZoomSettings;

    const v1, 0x7f080251

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/DisplaySettings;

    const v1, 0x7f080251

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/wallpaper/WallpaperTypeSettings;

    const v1, 0x7f080251

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/notification/ConfigureNotificationSettings;

    const v1, 0x7f15002f

    invoke-static {v0, v1, v6}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/applications/AppAndNotificationDashboardFragment;

    const v1, 0x7f080246

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/notification/SoundSettings;

    const v1, 0x7f080262

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/notification/ZenModeSettings;

    const v1, 0x7f15011b

    invoke-static {v0, v1, v6}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/notification/ZenModePrioritySettings;

    const v1, 0x7f150119

    invoke-static {v0, v1, v6}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/deviceinfo/StorageSettings;

    const v1, 0x7f080263

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/fuelgauge/PowerUsageSummary;

    const v1, 0x7f1500a6

    const v2, 0x7f080249

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;

    const v1, 0x7f080249

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/fuelgauge/BatterySaverSettings;

    const v1, 0x7f150027

    const v2, 0x7f080249

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/applications/AdvancedAppSettings;

    const v1, 0x7f080246

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/applications/assist/ManageAssist;

    const v1, 0x7f080246

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/applications/SpecialAccessSettings;

    const v1, 0x7f1500e5

    const v2, 0x7f080246

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/users/UserSettings;

    const v1, 0x7f080258

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/gestures/AssistGestureSettings;

    invoke-static {v0, v3, v4}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/gestures/PickupGestureSettings;

    invoke-static {v0, v3, v4}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/gestures/DoubleTapScreenSettings;

    invoke-static {v0, v3, v4}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/gestures/DoubleTapPowerSettings;

    invoke-static {v0, v3, v4}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/gestures/DoubleTwistGestureSettings;

    invoke-static {v0, v3, v4}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/gestures/SwipeToNotificationSettings;

    invoke-static {v0, v3, v4}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/language/LanguageAndInputSettings;

    invoke-static {v0, v3, v5}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/location/LocationSettings;

    const v1, 0x7f15007a

    const v2, 0x7f080256

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/location/ScanningSettings;

    const v1, 0x7f150079

    const v2, 0x7f080256

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/SecuritySettings;

    const v1, 0x7f080260

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/EncryptionAndCredential;

    const v1, 0x7f150054

    const v2, 0x7f080260

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/ScreenPinningSettings;

    const v1, 0x7f080260

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/accounts/UserAndAccountDashboardFragment;

    const v1, 0x7f080245

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/inputmethod/VirtualKeyboardFragment;

    invoke-static {v0, v3, v5}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;

    invoke-static {v0, v3, v5}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/inputmethod/PhysicalKeyboardFragment;

    invoke-static {v0, v3, v5}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/backup/BackupSettingsActivity;

    const v1, 0x7f080247

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/backup/BackupSettingsFragment;

    const v1, 0x7f080247

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/DateTimeSettings;

    const v1, 0x7f08024e

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/accessibility/AccessibilitySettings;

    const v1, 0x7f080244

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/print/PrintSettingsFragment;

    const v1, 0x7f08025e

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/development/DevelopmentSettings;

    const v1, 0x7f08024f

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/DeviceInfoSettings;

    invoke-static {v0, v3, v7}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/LegalSettings;

    invoke-static {v0, v3, v7}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/notification/ZenModeVisualInterruptionSettings;

    const v1, 0x7f15011c

    invoke-static {v0, v1, v6}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/system/SystemDashboardFragment;

    invoke-static {v0, v3, v7}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/system/ResetDashboardFragment;

    const v1, 0x7f080235

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;

    const v1, 0x7f080263

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/connecteddevice/ConnectedDeviceDashboardFragment;

    const v1, 0x7f0801bf

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/enterprise/EnterprisePrivacySettings;

    invoke-static {v0, v3, v7}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/nfc/PaymentSettings;

    const v1, 0x7f080259

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/tts/TtsEnginePreferenceFragment;

    invoke-static {v0, v3, v5}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/security/LockscreenDashboardFragment;

    const v1, 0x7f1500ba

    const v2, 0x7f080260

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/accessibility/MagnificationPreferenceFragment;

    const v1, 0x7f080244

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/accessibility/AccessibilityShortcutPreferenceFragment;

    const v1, 0x7f080244

    invoke-static {v0, v3, v1}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    const-class v0, Lcom/android/settings/notification/ChannelImportanceSettings;

    invoke-static {v0, v3, v6}, Lcom/android/settings/search/SearchIndexableResources;->addIndex(Ljava/lang/Class;II)V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static addIndex(Ljava/lang/Class;II)V
    .locals 4

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/settings/search/SearchIndexableResources;->sResMap:Ljava/util/HashMap;

    new-instance v2, Landroid/provider/SearchIndexableResource;

    const/4 v3, 0x0

    invoke-direct {v2, v3, p1, v0, p2}, Landroid/provider/SearchIndexableResource;-><init>(IILjava/lang/String;I)V

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static getResourceByName(Ljava/lang/String;)Landroid/provider/SearchIndexableResource;
    .locals 1

    sget-object v0, Lcom/android/settings/search/SearchIndexableResources;->sResMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/SearchIndexableResource;

    return-object v0
.end method

.method public static size()I
    .locals 1

    sget-object v0, Lcom/android/settings/search/SearchIndexableResources;->sResMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public static values()Ljava/util/Collection;
    .locals 1

    sget-object v0, Lcom/android/settings/search/SearchIndexableResources;->sResMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
