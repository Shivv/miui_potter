.class public interface abstract Lcom/android/settings/search/Indexable$SearchIndexProvider;
.super Ljava/lang/Object;
.source "Indexable.java"


# virtual methods
.method public abstract getNonIndexableKeys(Landroid/content/Context;)Ljava/util/List;
.end method

.method public abstract getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
.end method

.method public abstract getRawDataToIndex(Landroid/content/Context;Z)Ljava/util/List;
.end method

.method public abstract getXmlResourcesToIndex(Landroid/content/Context;Z)Ljava/util/List;
.end method
