.class Lcom/android/settings/search/SecurityUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;
.source "SecurityUpdateHelper.java"


# static fields
.field private static final ADD_FACE_RECOGINITION_RESOURCE:Ljava/lang/String; = "add_facerecoginition_text"

.field private static final ADD_FINGERPRINT_RESOURCE:Ljava/lang/String; = "add_fingerprint_text"

.field private static final BLUETOOTH_UNLOCK_RESOURCE:Ljava/lang/String; = "bluetooth_unlock_title"

.field private static final CREDENTIALS_RESET_RESOURCE:Ljava/lang/String; = "credentials_reset"

.field private static final LOCK_RESOURCE:Ljava/lang/String; = "lock_settings"

.field private static final LOCK_RESOURCE_WITH_FINGERPRINT_RESOURCE:Ljava/lang/String; = "lock_settings_with_fingerprint"

.field private static final NEW_ENCRYPTION_RESOURCE:Ljava/lang/String; = "security_encryption_title"

.field private static final OLD_ENCRYPTION_RESOURCE:Ljava/lang/String; = "crypt_keeper_encrypt_title"

.field private static final PALM_ENABLED_RESOURCE:Ljava/lang/String; = "palm_enabled"

.field private static final SENSOR_PROXIMITY_RESOURCE:Ljava/lang/String; = "screen_on_proximity_sensor_title"

.field private static final SMARTCOVER_RESOURCE:Ljava/lang/String; = "smartcover_lock_or_unlock_screen_tittle"

.field private static final SUSPEND_GESTURE_RESOURCE:Ljava/lang/String; = "suspend_gesture_enabled"

.field private static final TRUSTED_CREDENTIALS_RESOURCE:Ljava/lang/String; = "trusted_credentials"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method private static isEllipticProximity(Landroid/content/Context;)Z
    .locals 4

    const/4 v1, 0x0

    const-string/jumbo v0, "Elliptic Proximity"

    const-string/jumbo v0, "Elliptic Labs"

    const-string/jumbo v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v2, "Elliptic Proximity"

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "Elliptic Labs"

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/settings/bM;

    invoke-direct {v0, p0}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/bM;->bNd()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "lock_settings"

    invoke-static {p0, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v4, "name"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f120991

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, p1, v0, v4, v5}, Lcom/android/settings/search/SecurityUpdateHelper;->updateItemData(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "lock_settings"

    const-string/jumbo v4, "lock_settings_with_fingerprint"

    invoke-static {p0, p1, v0, v4}, Lcom/android/settings/search/SecurityUpdateHelper;->updatePath(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "add_fingerprint_text"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lcom/android/internal/widget/LockPatternUtils;->isDeviceEncryptionEnabled()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "file"

    const-string/jumbo v3, "ro.crypto.type"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string/jumbo v0, "crypt_keeper_encrypt_title"

    invoke-static {p0, p1, v0, v1}, Lcom/android/settings/search/SecurityUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    const-string/jumbo v0, "security_encryption_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :goto_1
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v3, "centaur"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "palm_enabled"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v0, "suspend_gesture_enabled"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_2
    const-string/jumbo v0, "support_hall_sensor"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string/jumbo v0, "smartcover_lock_or_unlock_screen_tittle"

    invoke-static {p0, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    const-string/jumbo v3, "support_multiple_small_win_cover"

    invoke-static {v3, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_2
    invoke-static {p0, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->getAdditionalSettingsValue(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x1

    or-long/2addr v4, v6

    invoke-static {p0, p1, v0, v4, v5}, Lcom/android/settings/search/SecurityUpdateHelper;->updateItemAdditionalData(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;J)V

    :goto_3
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v3, "android.hardware.sensor.proximity"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/android/settings/search/SecurityUpdateHelper;->isEllipticProximity(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const-string/jumbo v0, "screen_on_proximity_sensor_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_4
    const-string/jumbo v0, "bluetooth_unlock_title"

    invoke-static {p0, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->getAdditionalSettingsValue(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x2

    or-long/2addr v4, v6

    invoke-static {p0, p1, v0, v4, v5}, Lcom/android/settings/search/SecurityUpdateHelper;->updateItemAdditionalData(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;J)V

    const-string/jumbo v0, "no_config_credentials"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-static {p0, v0, v3}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_4
    const-string/jumbo v1, "trusted_credentials"

    invoke-static {p0, p1, v1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    const-string/jumbo v1, "credentials_reset"

    invoke-static {p0, p1, v1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->disableByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Z)V

    invoke-static {p0}, Lcom/android/settings/aH;->bvb(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const-string/jumbo v0, "add_facerecoginition_text"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_6
    return-void

    :cond_7
    const-string/jumbo v0, "crypt_keeper_encrypt_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v0, "security_encryption_title"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_9
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_2

    :cond_a
    const-string/jumbo v0, "smartcover_lock_or_unlock_screen_tittle"

    invoke-static {p0, p1, v0}, Lcom/android/settings/search/SecurityUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_3

    :cond_b
    move v0, v2

    goto :goto_4
.end method
