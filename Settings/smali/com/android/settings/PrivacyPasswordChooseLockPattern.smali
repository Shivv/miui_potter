.class public Lcom/android/settings/PrivacyPasswordChooseLockPattern;
.super Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;
.source "PrivacyPasswordChooseLockPattern.java"


# instance fields
.field private cfp:Lcom/android/settings/privacypassword/PrivacyPasswordManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;-><init>()V

    return-void
.end method


# virtual methods
.method protected bXO(Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_0

    new-instance v0, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;

    invoke-direct {v0}, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;-><init>()V

    invoke-static {}, Lcom/android/settings/privacypassword/BussinessPackageInfoCache;->Wk()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/privacypassword/BussinessPackageInfo;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->ahL:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v0, v0, Lcom/android/settings/privacypassword/BussinessPackageInfo;->agT:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->bXO(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->cfp:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    iget-object v0, p0, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->cfp:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->XG()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->cfp:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->XI(Z)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordChooseAccessControl;->onResume()V

    invoke-static {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->cfp:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    iget-object v0, p0, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->cfp:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->Xs()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/PrivacyPasswordChooseLockPattern;->finish()V

    :cond_0
    return-void
.end method
