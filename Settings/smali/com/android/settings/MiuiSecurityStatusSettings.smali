.class public Lcom/android/settings/MiuiSecurityStatusSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiSecurityStatusSettings.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private bVv()Landroid/preference/Preference;
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityStatusSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/settings/overlay/a;->aIn(Landroid/content/Context;)Lcom/android/settings/dashboard/n;

    move-result-object v3

    const-string/jumbo v0, "com.android.settings.category.ia.security"

    invoke-interface {v3, v0}, Lcom/android/settings/dashboard/n;->De(Ljava/lang/String;)Lcom/android/settingslib/drawer/DashboardCategory;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, v0, Lcom/android/settingslib/drawer/DashboardCategory;->czy:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    return-object v1

    :cond_2
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/drawer/Tile;

    invoke-interface {v3, v0}, Lcom/android/settings/dashboard/n;->Db(Lcom/android/settingslib/drawer/Tile;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string/jumbo v6, "security_status_package_verifier"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v1, v0}, Lcom/android/settings/MiuiSecurityStatusSettings;->bVu(Landroid/app/Activity;Landroid/preference/Preference;Lcom/android/settingslib/drawer/Tile;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic bVw(Landroid/app/Activity;Landroid/content/Intent;Landroid/preference/Preference;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public bVu(Landroid/app/Activity;Landroid/preference/Preference;Lcom/android/settingslib/drawer/Tile;)V
    .locals 2

    iget-object v0, p3, Lcom/android/settingslib/drawer/Tile;->title:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p3, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p3, Lcom/android/settingslib/drawer/Tile;->summary:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p3, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    new-instance v1, Lcom/android/settings/-$Lambda$GLxCXr_XRGPUcwIzD-Y970Fa8EQ;

    invoke-direct {v1, p1, v0}, Lcom/android/settings/-$Lambda$GLxCXr_XRGPUcwIzD-Y970Fa8EQ;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p2, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :cond_1
    return-void
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/MiuiSecurityStatusSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f1500da

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSecurityStatusSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "find_my_device"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSecurityStatusSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityStatusSettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lmiui/cloud/finddevice/FindDeviceStatusManagerProvider;->isLastStatusOpen(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f120716

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiSecurityStatusSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    sget-object v1, Landroid/os/Build$VERSION;->SECURITY_PATCH:Ljava/lang/String;

    const-string/jumbo v0, "security_update"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSecurityStatusSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0}, Lcom/android/settings/MiuiSecurityStatusSettings;->bVv()Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityStatusSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void

    :cond_1
    const v1, 0x7f120715

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiSecurityStatusSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityStatusSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method
