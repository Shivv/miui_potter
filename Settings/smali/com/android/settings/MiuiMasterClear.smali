.class public Lcom/android/settings/MiuiMasterClear;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiMasterClear.java"


# instance fields
.field private bAA:Lcom/android/settings/ad;

.field private bAB:Z

.field private bAC:Lcom/android/settingslib/n;

.field private bAD:Landroid/preference/CheckBoxPreference;

.field private bAE:Landroid/preference/CheckBoxPreference;

.field private bAF:Landroid/app/Dialog;

.field private bAG:Z

.field private bAH:Z

.field private bAI:Ljava/lang/String;

.field private bAJ:Z

.field private bAK:Lcom/android/settings/af;

.field private bAz:Landroid/accounts/AccountManagerFuture;

.field protected mUserId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic boA(Lcom/android/settings/MiuiMasterClear;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->bof()V

    return-void
.end method

.method static synthetic boB(Lcom/android/settings/MiuiMasterClear;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->bog()V

    return-void
.end method

.method static synthetic boC(Lcom/android/settings/MiuiMasterClear;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boi()V

    return-void
.end method

.method static synthetic boD(Lcom/android/settings/MiuiMasterClear;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boj()V

    return-void
.end method

.method static synthetic boE(Lcom/android/settings/MiuiMasterClear;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->bor()V

    return-void
.end method

.method private bof()V
    .locals 4

    const v0, 0x7f1206f8

    const v1, 0x7f1206f7

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiMasterClear;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1203fe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private bog()V
    .locals 5

    const/4 v4, 0x0

    invoke-static {}, Lmiui/net/ConnectivityHelper;->getInstance()Lmiui/net/ConnectivityHelper;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/net/ConnectivityHelper;->isNetworkConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const v1, 0x7f1206fb

    const v0, 0x7f1206fa

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClear;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f12107f    # 1.9415294E38f

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_0
    const v1, 0x7f121081

    const v0, 0x7f121080

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClear;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private boh()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x103006d

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x11030010

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    iget-object v2, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7e5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    const v1, 0x110c002b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimatedRotateDrawable;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1107001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesCount(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1107001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesDuration(I)V

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->start()V

    new-instance v0, Lcom/android/settings/ag;

    invoke-direct {v0, p0}, Lcom/android/settings/ag;-><init>(Lcom/android/settings/MiuiMasterClear;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/ag;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private boi()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAD:Landroid/preference/CheckBoxPreference;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lmiui/os/MiuiInit;->doFactoryReset(Z)V

    const-string/jumbo v0, "MasterClearRec"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "doFactoryReset hex password:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiMasterClear;->bAI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->bou()V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boj()V

    goto :goto_1
.end method

.method private boj()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "support_erase_external_storage"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.FACTORY_RESET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "format_sdcard"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->bom()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "support_erase_external_storage"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.FACTORY_RESET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "format_sdcard"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAI:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "password"

    iget-object v2, p0, Lcom/android/settings/MiuiMasterClear;->bAI:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iput-object v4, p0, Lcom/android/settings/MiuiMasterClear;->bAI:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.FACTORY_RESET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "android"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private bok(Z)V
    .locals 2

    const/4 v1, 0x0

    const-string/jumbo v0, "statusbar"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClear;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    if-nez p1, :cond_0

    const/high16 v1, 0x1610000

    :cond_0
    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    return-void
.end method

.method private bol()Z
    .locals 2

    const-string/jumbo v0, "ro.crypto.state"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "encrypted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private bom()Z
    .locals 2

    const-string/jumbo v0, "ro.product.device"

    const-string/jumbo v1, "UNKNOWN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "leo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->bol()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private bon(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiMasterClear;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, p2}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method private boo()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAA:Lcom/android/settings/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAA:Lcom/android/settings/ad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/ad;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/settings/ad;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/ad;-><init>(Lcom/android/settings/MiuiMasterClear;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAA:Lcom/android/settings/ad;

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAA:Lcom/android/settings/ad;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/ad;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private bop(I)Z
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Lcom/android/settings/cx;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    const v2, 0x7f120a38

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Lcom/android/settings/cx;->bSQ(ILjava/lang/CharSequence;Z)Z

    move-result v0

    return v0
.end method

.method private boq()Z
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/MiuiMasterClear;->bAG:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAC:Lcom/android/settingslib/n;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAC:Lcom/android/settingslib/n;

    invoke-static {v0, v1}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private bor()V
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v0, "com.android.settings"

    const-string/jumbo v2, "com.android.settings.MiuiMasterClearApplyActivity"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "format_internal_storage"

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 v0, 0x39

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/MiuiMasterClear;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private bos()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAK:Lcom/android/settings/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAK:Lcom/android/settings/af;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/af;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/settings/af;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/af;-><init>(Lcom/android/settings/MiuiMasterClear;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAK:Lcom/android/settings/af;

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAK:Lcom/android/settings/af;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/af;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private bot(Z)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_buttons_state"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bou()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/android/settings/eR;

    invoke-direct {v1, p0}, Lcom/android/settings/eR;-><init>(Lcom/android/settings/MiuiMasterClear;)V

    invoke-static {v0, v1}, Lcom/android/settings/MasterClearConfirm;->bvX(Landroid/app/Activity;Lcom/android/settings/aL;)V

    return-void
.end method

.method static synthetic bov(Lcom/android/settings/MiuiMasterClear;)Landroid/accounts/AccountManagerFuture;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAz:Landroid/accounts/AccountManagerFuture;

    return-object v0
.end method

.method static synthetic bow(Lcom/android/settings/MiuiMasterClear;)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic box(Lcom/android/settings/MiuiMasterClear;Landroid/accounts/AccountManagerFuture;)Landroid/accounts/AccountManagerFuture;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiMasterClear;->bAz:Landroid/accounts/AccountManagerFuture;

    return-object p1
.end method

.method static synthetic boy(Lcom/android/settings/MiuiMasterClear;Lcom/android/settings/ad;)Lcom/android/settings/ad;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiMasterClear;->bAA:Lcom/android/settings/ad;

    return-object p1
.end method

.method static synthetic boz(Lcom/android/settings/MiuiMasterClear;Lcom/android/settings/af;)Lcom/android/settings/af;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiMasterClear;->bAK:Lcom/android/settings/af;

    return-object p1
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/MiuiMasterClear;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x3a

    if-ne p1, v0, :cond_0

    if-ne p2, v3, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/android/settings/MiuiMasterClear;->bAB:Z

    :cond_0
    const/16 v0, 0x38

    if-ne p1, v0, :cond_3

    if-ne p2, v3, :cond_1

    iput-boolean v1, p0, Lcom/android/settings/MiuiMasterClear;->bAJ:Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    const/16 v0, 0x39

    if-ne p1, v0, :cond_7

    if-ne p2, v3, :cond_5

    invoke-static {}, Lcom/android/settings/aq;->bqE()Z

    move-result v0

    if-eqz v0, :cond_4

    return-void

    :cond_4
    invoke-direct {p0, v1}, Lcom/android/settings/MiuiMasterClear;->bot(Z)V

    invoke-direct {p0, v2}, Lcom/android/settings/MiuiMasterClear;->bok(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAD:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boh()V

    :cond_5
    :goto_1
    return-void

    :cond_6
    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boi()V

    goto :goto_1

    :cond_7
    const/16 v0, 0x37

    if-eq p1, v0, :cond_8

    return-void

    :cond_8
    if-ne p2, v3, :cond_a

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->bom()Z

    move-result v0

    if-eqz v0, :cond_9

    if-eqz p3, :cond_9

    const-string/jumbo v0, "password"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-static {}, Lcom/android/settings/cn;->bRs()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-static {v0, v1}, Lcom/android/settings/cn;->bRt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAI:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_9
    :goto_2
    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boo()V

    :cond_a
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiuiMasterClear"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    invoke-virtual {p0, v5}, Lcom/android/settings/MiuiMasterClear;->setHasOptionsMenu(Z)V

    const v0, 0x7f150081

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClear;->addPreferencesFromResource(I)V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/MiuiMasterClear;->mUserId:I

    const-string/jumbo v0, "erase_application"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClear;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAD:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "erase_external_storage"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClear;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "erase_optional"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiMasterClear;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "support_erase_application"

    invoke-static {v1, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiMasterClear;->bAD:Landroid/preference/CheckBoxPreference;

    :cond_0
    const-string/jumbo v1, "ro.boot.sdcard.type"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "support_erase_external_storage"

    invoke-static {v2, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_2

    :cond_1
    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->bol()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_2
    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    :goto_0
    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAD:Landroid/preference/CheckBoxPreference;

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "clear_all"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/MiuiMasterClear;->bAH:Z

    iget-boolean v0, p0, Lcom/android/settings/MiuiMasterClear;->bAH:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAD:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boh()V

    :cond_5
    :goto_1
    return-void

    :cond_6
    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    const v2, 0x7f1206d7

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    iget-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAE:Landroid/preference/CheckBoxPreference;

    const v2, 0x7f1206d8

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    const-string/jumbo v1, "erase_data"

    const-string/jumbo v2, "erase_external_no_emulate_sd"

    invoke-direct {p0, v1, v2}, Lcom/android/settings/MiuiMasterClear;->bon(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const-string/jumbo v1, "erase_data"

    const-string/jumbo v2, "erase_external_no_emulate_sd"

    invoke-direct {p0, v1, v2}, Lcom/android/settings/MiuiMasterClear;->bon(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boi()V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    const v1, 0x7f120a27

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f080062

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "enable_demo_mode"

    invoke-static {v1, v2, v3}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAF:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAA:Lcom/android/settings/ad;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAA:Lcom/android/settings/ad;

    invoke-virtual {v0, v2}, Lcom/android/settings/ad;->cancel(Z)Z

    iput-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAA:Lcom/android/settings/ad;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAK:Lcom/android/settings/af;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAK:Lcom/android/settings/af;

    invoke-virtual {v0, v2}, Lcom/android/settings/af;->cancel(Z)Z

    iput-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAK:Lcom/android/settings/af;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAz:Landroid/accounts/AccountManagerFuture;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAz:Landroid/accounts/AccountManagerFuture;

    invoke-interface {v0, v2}, Landroid/accounts/AccountManagerFuture;->cancel(Z)Z

    iput-object v1, p0, Lcom/android/settings/MiuiMasterClear;->bAz:Landroid/accounts/AccountManagerFuture;

    :cond_3
    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiMasterClear;->bAJ:Z

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boq()Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/16 v0, 0x37

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiMasterClear;->bop(I)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boo()V

    return v1

    :cond_3
    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiMasterClear;->bot(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiMasterClear;->bok(Z)V

    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "no_factory_reset"

    iget v2, p0, Lcom/android/settings/MiuiMasterClear;->mUserId:I

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClear;->bAC:Lcom/android/settingslib/n;

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClear;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "no_factory_reset"

    iget v2, p0, Lcom/android/settings/MiuiMasterClear;->mUserId:I

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/MiuiMasterClear;->bAG:Z

    iget-boolean v0, p0, Lcom/android/settings/MiuiMasterClear;->bAJ:Z

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/android/settings/MiuiMasterClear;->bAJ:Z

    const/16 v0, 0x37

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiMasterClear;->bop(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->boo()V

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/MiuiMasterClear;->bAB:Z

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lcom/android/settings/MiuiMasterClear;->bAB:Z

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClear;->bos()V

    :cond_1
    return-void
.end method
