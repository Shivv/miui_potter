.class public Lcom/android/settings/KeySettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "KeySettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private ccQ:Lmiui/preference/ValuePreference;

.field private ccR:Landroid/preference/CheckBoxPreference;

.field private ccS:Landroid/util/ArrayMap;

.field private ccT:Landroid/preference/PreferenceCategory;

.field private ccU:Landroid/preference/ListPreference;

.field private ccV:Landroid/preference/ListPreference;

.field private ccW:Landroid/preference/CheckBoxPreference;

.field private ccX:Landroid/content/res/Resources;

.field private ccY:Lmiui/preference/ValuePreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    return-void
.end method

.method private bUK(Z)V
    .locals 9

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_key_press_app_switch"

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v4

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, v3

    :goto_0
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    const v5, 0x7f120896

    invoke-virtual {v0, v5}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccX:Landroid/content/res/Resources;

    const v1, 0x7f030089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v0, "long_press_menu_key_when_lock"

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "double_click_power_key"

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "three_gesture_down"

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    array-length v6, v1

    move v0, v3

    :goto_2
    if-ge v0, v6, :cond_2

    aget-object v7, v1, v0

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v4, :cond_4

    const-string/jumbo v6, "launch_recents"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    :cond_4
    if-nez v4, :cond_6

    const-string/jumbo v6, "show_menu"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    :cond_5
    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v6, "none"

    const/4 v7, -0x2

    invoke-static {v1, v0, v6, v7}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_3

    :cond_6
    iget-object v6, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    invoke-virtual {v6, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/preference/ValuePreference;

    if-eqz v1, :cond_3

    iget-object v6, p0, Lcom/android/settings/KeySettings;->ccX:Landroid/content/res/Resources;

    const-string/jumbo v7, "string"

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v0, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccR:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "fingerprint_nav_center_action"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccR:Landroid/preference/CheckBoxPreference;

    if-ne v0, v2, :cond_b

    move v0, v2

    :goto_4
    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_8
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccW:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "single_key_use_enable"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccW:Landroid/preference/CheckBoxPreference;

    if-ne v0, v2, :cond_c

    :goto_5
    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_9
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    if-eqz v0, :cond_a

    const-string/jumbo v0, "persist.sys.handswap"

    const-string/jumbo v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_a
    return-void

    :cond_b
    move v0, v3

    goto :goto_4

    :cond_c
    move v2, v3

    goto :goto_5
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/KeySettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/KeySettings;->ccX:Landroid/content/res/Resources;

    const v0, 0x7f15006d

    invoke-virtual {p0, v0}, Lcom/android/settings/KeySettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_key_press_app_switch"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Key;->isTSMClientInstalled(Landroid/content/Context;)Z

    move-result v4

    const-string/jumbo v0, "function_shortcut"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "launch_camera"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v2, "launch_camera"

    invoke-virtual {v1, v2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "screen_shot"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v2, "screen_shot"

    invoke-virtual {v1, v2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v0, :cond_d

    const-string/jumbo v0, "launch_google_search"

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iget-object v2, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    sget-boolean v1, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v1, :cond_e

    const-string/jumbo v1, "launch_voice_assistant"

    :goto_1
    invoke-virtual {v2, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    sget-boolean v2, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v2, :cond_f

    const-string/jumbo v2, "launch_google_search"

    :goto_2
    invoke-virtual {v5, v2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "go_to_sleep"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v2, "go_to_sleep"

    invoke-virtual {v1, v2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "turn_on_torch"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v2, "turn_on_torch"

    invoke-virtual {v1, v2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "close_app"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v2, "close_app"

    invoke-virtual {v1, v2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "split_screen"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v2, "split_screen"

    invoke-virtual {v1, v2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "mi_pay"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_8

    if-eqz v4, :cond_10

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v2, "mi_pay"

    invoke-virtual {v1, v2, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "show_menu"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/KeySettings;->ccY:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccY:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v1, "show_menu"

    iget-object v2, p0, Lcom/android/settings/KeySettings;->ccY:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "launch_recents"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/KeySettings;->ccQ:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccQ:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v1, "launch_recents"

    iget-object v2, p0, Lcom/android/settings/KeySettings;->ccQ:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_a
    if-eqz v3, :cond_11

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccQ:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_b
    :goto_4
    const-string/jumbo v0, "menu_press"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "key_position_cat"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "support_screen_key_swap"

    invoke-static {v1, v7}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_12

    const-string/jumbo v1, "screen_key_position"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_5
    const-string/jumbo v0, "convenience_key"

    invoke-virtual {p0, v0}, Lcom/android/settings/KeySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    const-string/jumbo v1, "pref_fingerprint_nav_center_to_home"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/KeySettings;->ccR:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "pref_single_key_use"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/KeySettings;->ccW:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "support_tap_fingerprint_sensor_to_home"

    invoke-static {v1, v7}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lcom/android/settings/KeySettings;->ccR:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-object v6, p0, Lcom/android/settings/KeySettings;->ccW:Landroid/preference/CheckBoxPreference;

    :cond_c
    return-void

    :cond_d
    const-string/jumbo v0, "launch_voice_assistant"

    goto/16 :goto_0

    :cond_e
    const-string/jumbo v1, "launch_google_search"

    goto/16 :goto_1

    :cond_f
    const-string/jumbo v2, "launch_voice_assistant"

    goto/16 :goto_2

    :cond_10
    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_3

    :cond_11
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccY:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_4

    :cond_12
    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_5
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_1

    check-cast p2, Ljava/lang/String;

    const-string/jumbo v0, "persist.sys.handswap"

    invoke-static {v0, p2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccV:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    if-ne p1, v0, :cond_0

    check-cast p2, Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccU:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_key_press_app_switch"

    invoke-static {v1, v2, v0}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccY:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccQ:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v1, "launch_recents"

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    :goto_1
    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccX:Landroid/content/res/Resources;

    invoke-virtual {v0}, Lmiui/preference/ValuePreference;->getValueRes()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "none"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const v2, 0x7f120896

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setValue(I)V

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "none"

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccQ:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccT:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/KeySettings;->ccY:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/KeySettings;->ccS:Landroid/util/ArrayMap;

    const-string/jumbo v1, "show_menu"

    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    goto :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/KeySettings;->ccR:Landroid/preference/CheckBoxPreference;

    if-ne p2, v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/KeySettings;->ccR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "fingerprint_nav_center_action"

    if-eqz v2, :cond_1

    :goto_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/settings/KeySettings;->ccW:Landroid/preference/CheckBoxPreference;

    if-ne p2, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/KeySettings;->ccW:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/KeySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "single_key_use_enable"

    if-eqz v2, :cond_3

    :goto_2
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/KeySettings;->bUK(Z)V

    return-void
.end method
