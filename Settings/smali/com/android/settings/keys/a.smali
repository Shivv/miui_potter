.class final Lcom/android/settings/keys/a;
.super Landroid/os/AsyncTask;
.source "AutoDisableScreenButtonsCloudConfigService.java"


# instance fields
.field final synthetic bgb:Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;


# direct methods
.method constructor <init>(Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/keys/a;->bgb:Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/keys/a;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/keys/a;->bgb:Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;

    iget-object v1, p0, Lcom/android/settings/keys/a;->bgb:Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;

    invoke-virtual {v1}, Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;->aUg(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;->aUf(Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/keys/a;->bgb:Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;

    invoke-virtual {v0}, Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "auto_disable_screen_button_cloud_setting"

    iget-object v2, p0, Lcom/android/settings/keys/a;->bgb:Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;

    invoke-static {v2}, Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;->aUd(Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/keys/a;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/keys/a;->bgb:Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;

    iget-object v0, p0, Lcom/android/settings/keys/a;->bgb:Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;

    invoke-static {v0}, Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;->aUe(Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;)Landroid/app/job/JobParameters;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/keys/a;->bgb:Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;

    invoke-static {v0}, Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;->aUd(Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/android/settings/keys/AutoDisableScreenButtonsCloudConfigService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
