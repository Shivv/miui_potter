.class final Lcom/android/settings/inputmethod/SpellCheckersSettings$1;
.super Ljava/lang/Object;
.source "SpellCheckersSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic axF:Lcom/android/settings/inputmethod/SpellCheckersSettings;

.field final synthetic axG:Landroid/view/textservice/SpellCheckerInfo;


# direct methods
.method constructor <init>(Lcom/android/settings/inputmethod/SpellCheckersSettings;Landroid/view/textservice/SpellCheckerInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings$1;->axF:Lcom/android/settings/inputmethod/SpellCheckersSettings;

    iput-object p2, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings$1;->axG:Landroid/view/textservice/SpellCheckerInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    const/4 v0, 0x0

    if-nez p2, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings$1;->axF:Lcom/android/settings/inputmethod/SpellCheckersSettings;

    invoke-static {v1}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->als(Lcom/android/settings/inputmethod/SpellCheckersSettings;)Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "selected_spell_checker_subtype"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v0, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings$1;->axF:Lcom/android/settings/inputmethod/SpellCheckersSettings;

    invoke-static {v0}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alv(Lcom/android/settings/inputmethod/SpellCheckersSettings;)V

    return-void

    :cond_0
    invoke-static {p2}, Lcom/android/settings/inputmethod/SpellCheckersSettings;->alt(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/inputmethod/SpellCheckersSettings$1;->axG:Landroid/view/textservice/SpellCheckerInfo;

    invoke-virtual {v1, v0}, Landroid/view/textservice/SpellCheckerInfo;->getSubtypeAt(I)Landroid/view/textservice/SpellCheckerSubtype;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/textservice/SpellCheckerSubtype;->hashCode()I

    move-result v0

    goto :goto_0
.end method
