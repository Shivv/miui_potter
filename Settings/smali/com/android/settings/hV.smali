.class final Lcom/android/settings/hV;
.super Landroid/os/AsyncTask;
.source "SetUpChooseLockPattern.java"


# instance fields
.field final synthetic cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

.field final synthetic cnM:Lcom/android/internal/widget/LockPatternUtils;

.field final synthetic cnN:Z

.field final synthetic cnO:Z


# direct methods
.method constructor <init>(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;Lcom/android/internal/widget/LockPatternUtils;ZZ)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hV;->cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iput-object p2, p0, Lcom/android/settings/hV;->cnM:Lcom/android/internal/widget/LockPatternUtils;

    iput-boolean p3, p0, Lcom/android/settings/hV;->cnN:Z

    iput-boolean p4, p0, Lcom/android/settings/hV;->cnO:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected cap([B)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/hV;->cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v0, p1}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEN(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;[B)V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/hV;->doInBackground([Ljava/lang/Void;)[B

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[B
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/hV;->cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-virtual {v2}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/hV;->cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-virtual {v2}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/hV;->cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v3}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEF(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)Lcom/android/settings/bM;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/bn;->bFm(Lcom/android/settings/bM;)J

    move-result-wide v4

    if-eqz v2, :cond_0

    const-string/jumbo v3, "has_challenge"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/hV;->cnM:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v3, p0, Lcom/android/settings/hV;->cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v3, v3, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    iget-object v6, p0, Lcom/android/settings/hV;->cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v6}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEL(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)I

    move-result v6

    iget-boolean v7, p0, Lcom/android/settings/hV;->cnN:Z

    invoke-static {v2, v3, v6, v7}, Lcom/android/settings/bn;->bFn(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;IZ)V

    iget-boolean v2, p0, Lcom/android/settings/hV;->cnO:Z

    if-nez v2, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/hV;->cnM:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v2, p0, Lcom/android/settings/hV;->cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    iget-object v2, v2, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bNc:Ljava/util/List;

    iget-object v3, p0, Lcom/android/settings/hV;->cnL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;

    invoke-static {v3}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;->bEL(Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment;)I

    move-result v3

    invoke-static {v0, v2, v4, v5, v3}, Lcom/android/settings/bn;->bFo(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;JI)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SetUpChooseLockPattern"

    const-string/jumbo v2, "critical: no token returned for known good pattern"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/android/settings/hV;->cap([B)V

    return-void
.end method
