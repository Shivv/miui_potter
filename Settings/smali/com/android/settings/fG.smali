.class final Lcom/android/settings/fG;
.super Ljava/lang/Object;
.source "ResetNetwork.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic ckL:Lcom/android/settings/ResetNetwork;

.field final synthetic ckM:I


# direct methods
.method constructor <init>(Lcom/android/settings/ResetNetwork;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fG;->ckL:Lcom/android/settings/ResetNetwork;

    iput p2, p0, Lcom/android/settings/fG;->ckM:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/fG;->ckL:Lcom/android/settings/ResetNetwork;

    invoke-static {v0}, Lcom/android/settings/ResetNetwork;->btE(Lcom/android/settings/ResetNetwork;)I

    move-result v0

    iget v1, p0, Lcom/android/settings/fG;->ckM:I

    if-ne v0, v1, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fG;->ckL:Lcom/android/settings/ResetNetwork;

    iget v1, p0, Lcom/android/settings/fG;->ckM:I

    invoke-static {v0, v1}, Lcom/android/settings/ResetNetwork;->btG(Lcom/android/settings/ResetNetwork;I)I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/fG;->ckL:Lcom/android/settings/ResetNetwork;

    invoke-static {v0}, Lcom/android/settings/ResetNetwork;->btF(Lcom/android/settings/ResetNetwork;)Landroid/preference/PreferenceCategory;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget v0, p0, Lcom/android/settings/fG;->ckM:I

    if-eq v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/fG;->ckL:Lcom/android/settings/ResetNetwork;

    invoke-static {v0}, Lcom/android/settings/ResetNetwork;->btF(Lcom/android/settings/ResetNetwork;)Landroid/preference/PreferenceCategory;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/RadioButtonPreference;

    invoke-virtual {v0, v2}, Lmiui/preference/RadioButtonPreference;->setChecked(Z)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return v3

    :cond_3
    iget-object v0, p0, Lcom/android/settings/fG;->ckL:Lcom/android/settings/ResetNetwork;

    invoke-static {v0}, Lcom/android/settings/ResetNetwork;->btE(Lcom/android/settings/ResetNetwork;)I

    move-result v0

    iget v1, p0, Lcom/android/settings/fG;->ckM:I

    if-eq v0, v1, :cond_4

    move v2, v3

    :cond_4
    return v2
.end method
