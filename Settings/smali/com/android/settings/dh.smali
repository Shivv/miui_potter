.class final Lcom/android/settings/dh;
.super Ljava/lang/Object;
.source "EditPinPreference.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic cgZ:Lcom/android/settings/EditPinPreference;

.field final synthetic cha:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/android/settings/EditPinPreference;Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dh;->cgZ:Lcom/android/settings/EditPinPreference;

    iput-object p2, p0, Lcom/android/settings/dh;->cha:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p1, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    iget-object v2, p0, Lcom/android/settings/dh;->cha:Landroid/widget/Button;

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
