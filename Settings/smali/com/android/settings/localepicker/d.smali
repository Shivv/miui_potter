.class Lcom/android/settings/localepicker/d;
.super Landroid/support/v7/widget/b;
.source "LocaleDragAndDropAdapter.java"


# instance fields
.field private NA:Ljava/text/NumberFormat;

.field private NB:Landroid/support/v7/widget/RecyclerView;

.field private NC:Z

.field private Nv:Z

.field private final Nw:Ljava/util/List;

.field private final Nx:Landroid/support/v7/widget/a/b;

.field private Ny:Landroid/os/LocaleList;

.field private Nz:Landroid/os/LocaleList;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/b;-><init>()V

    iput-object v1, p0, Lcom/android/settings/localepicker/d;->NB:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v4, p0, Lcom/android/settings/localepicker/d;->NC:Z

    iput-boolean v2, p0, Lcom/android/settings/localepicker/d;->Nv:Z

    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/localepicker/d;->NA:Ljava/text/NumberFormat;

    iput-object v1, p0, Lcom/android/settings/localepicker/d;->Nz:Landroid/os/LocaleList;

    iput-object v1, p0, Lcom/android/settings/localepicker/d;->Ny:Landroid/os/LocaleList;

    iput-object p2, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    iput-object p1, p0, Lcom/android/settings/localepicker/d;->mContext:Landroid/content/Context;

    const/high16 v0, 0x41000000    # 8.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    new-instance v1, Landroid/support/v7/widget/a/b;

    new-instance v2, Lcom/android/settings/localepicker/l;

    const/4 v3, 0x3

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/android/settings/localepicker/l;-><init>(Lcom/android/settings/localepicker/d;IIF)V

    invoke-direct {v1, v2}, Landroid/support/v7/widget/a/b;-><init>(Landroid/support/v7/widget/a/c;)V

    iput-object v1, p0, Lcom/android/settings/localepicker/d;->Nx:Landroid/support/v7/widget/a/b;

    return-void
.end method

.method private HC(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/localepicker/d;->Nv:Z

    return-void
.end method

.method static synthetic HE(Lcom/android/settings/localepicker/d;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic HF(Lcom/android/settings/localepicker/d;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/localepicker/d;->Nv:Z

    return v0
.end method

.method static synthetic HG(Lcom/android/settings/localepicker/d;)Landroid/support/v7/widget/a/b;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nx:Landroid/support/v7/widget/a/b;

    return-object v0
.end method

.method static synthetic HH(Lcom/android/settings/localepicker/d;)Landroid/os/LocaleList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Ny:Landroid/os/LocaleList;

    return-object v0
.end method

.method static synthetic HI(Lcom/android/settings/localepicker/d;)Landroid/os/LocaleList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nz:Landroid/os/LocaleList;

    return-object v0
.end method

.method static synthetic HJ(Lcom/android/settings/localepicker/d;Landroid/os/LocaleList;)Landroid/os/LocaleList;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/localepicker/d;->Ny:Landroid/os/LocaleList;

    return-object p1
.end method

.method static synthetic HK(Lcom/android/settings/localepicker/d;Landroid/os/LocaleList;)Landroid/os/LocaleList;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/localepicker/d;->Nz:Landroid/os/LocaleList;

    return-object p1
.end method

.method static synthetic HL(Lcom/android/settings/localepicker/d;Ljava/text/NumberFormat;)Ljava/text/NumberFormat;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/localepicker/d;->NA:Ljava/text/NumberFormat;

    return-object p1
.end method


# virtual methods
.method HA(Lcom/android/internal/app/LocaleStore$LocaleInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/localepicker/d;->notifyItemInserted(I)V

    invoke-virtual {p0}, Lcom/android/settings/localepicker/d;->Hs()V

    return-void
.end method

.method public HB(Lcom/android/settings/localepicker/e;I)V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/LocaleStore$LocaleInfo;

    invoke-virtual {p1}, Lcom/android/settings/localepicker/e;->HM()Lcom/android/settings/localepicker/LocaleDragCell;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->getFullNameNative()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->getFullNameInUiLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lcom/android/settings/localepicker/LocaleDragCell;->HP(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->isTranslated()Z

    move-result v1

    invoke-virtual {v3, v1}, Lcom/android/settings/localepicker/LocaleDragCell;->HQ(Z)V

    iget-object v1, p0, Lcom/android/settings/localepicker/d;->NA:Ljava/text/NumberFormat;

    add-int/lit8 v4, p2, 0x1

    int-to-long v4, v4

    invoke-virtual {v1, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/android/settings/localepicker/LocaleDragCell;->HR(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/android/settings/localepicker/d;->NC:Z

    invoke-virtual {v3, v1}, Lcom/android/settings/localepicker/LocaleDragCell;->HS(Z)V

    iget-boolean v1, p0, Lcom/android/settings/localepicker/d;->NC:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Lcom/android/settings/localepicker/LocaleDragCell;->HT(Z)V

    iget-boolean v1, p0, Lcom/android/settings/localepicker/d;->NC:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/settings/localepicker/d;->Nv:Z

    :goto_0
    invoke-virtual {v3, v1}, Lcom/android/settings/localepicker/LocaleDragCell;->HU(Z)V

    iget-boolean v1, p0, Lcom/android/settings/localepicker/d;->NC:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->getChecked()Z

    move-result v2

    :cond_0
    invoke-virtual {v3, v2}, Lcom/android/settings/localepicker/LocaleDragCell;->setChecked(Z)V

    invoke-virtual {v3, v0}, Lcom/android/settings/localepicker/LocaleDragCell;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v3}, Lcom/android/settings/localepicker/LocaleDragCell;->HN()Landroid/widget/CheckBox;

    move-result-object v0

    new-instance v1, Lcom/android/settings/localepicker/m;

    invoke-direct {v1, p0, v3}, Lcom/android/settings/localepicker/m;-><init>(Lcom/android/settings/localepicker/d;Lcom/android/settings/localepicker/LocaleDragCell;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public HD(Landroid/os/LocaleList;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nz:Landroid/os/LocaleList;

    invoke-virtual {p1, v0}, Landroid/os/LocaleList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Landroid/os/LocaleList;->setDefault(Landroid/os/LocaleList;)V

    iput-object p1, p0, Lcom/android/settings/localepicker/d;->Nz:Landroid/os/LocaleList;

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->NB:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()Landroid/support/v7/widget/u;

    move-result-object v0

    new-instance v1, Lcom/android/settings/localepicker/n;

    invoke-direct {v1, p0}, Lcom/android/settings/localepicker/n;-><init>(Lcom/android/settings/localepicker/d;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/u;->duZ(Landroid/support/v7/widget/V;)Z

    return-void
.end method

.method public Hs()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v3, v2, [Ljava/util/Locale;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/LocaleStore$LocaleInfo;

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->getLocale()Ljava/util/Locale;

    move-result-object v0

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/os/LocaleList;

    invoke-direct {v0, v3}, Landroid/os/LocaleList;-><init>([Ljava/util/Locale;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/localepicker/d;->HD(Landroid/os/LocaleList;)V

    return-void
.end method

.method Ht()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/localepicker/d;->NC:Z

    return v0
.end method

.method Hu(II)V
    .locals 6

    const/4 v5, 0x0

    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/LocaleStore$LocaleInfo;

    iget-object v1, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/settings/localepicker/d;->notifyItemChanged(I)V

    invoke-virtual {p0, p2}, Lcom/android/settings/localepicker/d;->notifyItemChanged(I)V

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/localepicker/d;->notifyItemMoved(II)V

    return-void

    :cond_0
    const-string/jumbo v0, "LocaleDragAndDropAdapter"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v2, "Negative position in onItemMove %d -> %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public Hv(Landroid/os/Bundle;)V
    .locals 4

    if-eqz p1, :cond_3

    iget-boolean v0, p0, Lcom/android/settings/localepicker/d;->NC:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "selectedLocales"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/LocaleStore$LocaleInfo;

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->setChecked(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/localepicker/d;->notifyItemRangeChanged(II)V

    :cond_3
    return-void
.end method

.method Hw(Z)V
    .locals 4

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/android/settings/localepicker/d;->NC:Z

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/LocaleStore$LocaleInfo;

    invoke-virtual {v0, v2}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->setChecked(Z)V

    invoke-virtual {p0, v1}, Lcom/android/settings/localepicker/d;->notifyItemChanged(I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method Hx()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/LocaleStore$LocaleInfo;

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->getChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method Hy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/LocaleStore$LocaleInfo;

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->getChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/localepicker/d;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/android/settings/localepicker/d;->Hs()V

    return-void
.end method

.method public Hz(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/localepicker/d;->NB:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nx:Landroid/support/v7/widget/a/b;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a/b;->dnm(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public getItemCount()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    iget-boolean v2, p0, Lcom/android/settings/localepicker/d;->NC:Z

    if-eqz v2, :cond_2

    :cond_0
    invoke-direct {p0, v1}, Lcom/android/settings/localepicker/d;->HC(Z)V

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/settings/localepicker/d;->HC(Z)V

    goto :goto_1
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/p;I)V
    .locals 0

    check-cast p1, Lcom/android/settings/localepicker/e;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/localepicker/d;->HB(Lcom/android/settings/localepicker/e;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/localepicker/d;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/localepicker/e;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/localepicker/e;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d00de

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/localepicker/LocaleDragCell;

    new-instance v1, Lcom/android/settings/localepicker/e;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/localepicker/e;-><init>(Lcom/android/settings/localepicker/d;Lcom/android/settings/localepicker/LocaleDragCell;)V

    return-object v1
.end method

.method removeItem(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    return-void

    :cond_0
    if-ltz p1, :cond_1

    if-lt p1, v0, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/settings/localepicker/d;->notifyDataSetChanged()V

    return-void
.end method

.method public saveState(Landroid/os/Bundle;)V
    .locals 4

    if-eqz p1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/settings/localepicker/d;->Nw:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/LocaleStore$LocaleInfo;

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->getChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/android/internal/app/LocaleStore$LocaleInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "selectedLocales"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_2
    return-void
.end method
