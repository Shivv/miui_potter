.class final Lcom/android/settings/localepicker/l;
.super Landroid/support/v7/widget/a/d;
.source "LocaleDragAndDropAdapter.java"


# instance fields
.field private NQ:I

.field final synthetic NR:Lcom/android/settings/localepicker/d;

.field final synthetic NS:F


# direct methods
.method constructor <init>(Lcom/android/settings/localepicker/d;IIF)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/localepicker/l;->NR:Lcom/android/settings/localepicker/d;

    iput p4, p0, Lcom/android/settings/localepicker/l;->NS:F

    invoke-direct {p0, p2, p3}, Landroid/support/v7/widget/a/d;-><init>(II)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/localepicker/l;->NQ:I

    return-void
.end method


# virtual methods
.method public Bs(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;)Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/localepicker/l;->NR:Lcom/android/settings/localepicker/d;

    invoke-virtual {p2}, Landroid/support/v7/widget/p;->getAdapterPosition()I

    move-result v1

    invoke-virtual {p3}, Landroid/support/v7/widget/p;->getAdapterPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/localepicker/d;->Hu(II)V

    const/4 v0, 0x1

    return v0
.end method

.method public Bt(Landroid/support/v7/widget/p;I)V
    .locals 0

    return-void
.end method

.method public HV(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;FFIZ)V
    .locals 4

    const/4 v3, -0x1

    invoke-super/range {p0 .. p7}, Landroid/support/v7/widget/a/d;->HV(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;FFIZ)V

    iget v0, p0, Lcom/android/settings/localepicker/l;->NQ:I

    if-eq v0, v3, :cond_0

    iget-object v1, p3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    iget v0, p0, Lcom/android/settings/localepicker/l;->NQ:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/android/settings/localepicker/l;->NS:F

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setElevation(F)V

    iput v3, p0, Lcom/android/settings/localepicker/l;->NQ:I

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public HW(Landroid/support/v7/widget/p;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/a/d;->HW(Landroid/support/v7/widget/p;I)V

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/localepicker/l;->NQ:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    iput v1, p0, Lcom/android/settings/localepicker/l;->NQ:I

    goto :goto_0
.end method
