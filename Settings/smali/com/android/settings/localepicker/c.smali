.class public Lcom/android/settings/localepicker/c;
.super Landroid/support/v7/widget/al;
.source "LocaleLinearLayoutManager.java"


# instance fields
.field private final Nk:Landroid/support/v4/view/a/c;

.field private final Nl:Landroid/support/v4/view/a/c;

.field private final Nm:Landroid/support/v4/view/a/c;

.field private final Nn:Landroid/support/v4/view/a/c;

.field private final No:Landroid/support/v4/view/a/c;

.field private final Np:Lcom/android/settings/localepicker/d;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/localepicker/d;)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/widget/al;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/localepicker/c;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/localepicker/c;->Np:Lcom/android/settings/localepicker/d;

    new-instance v0, Landroid/support/v4/view/a/c;

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->mContext:Landroid/content/Context;

    const v2, 0x7f12008e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0029

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/android/settings/localepicker/c;->Nn:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->mContext:Landroid/content/Context;

    const v2, 0x7f12008c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0027

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/android/settings/localepicker/c;->Nl:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->mContext:Landroid/content/Context;

    const v2, 0x7f12008d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0028

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/android/settings/localepicker/c;->Nm:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->mContext:Landroid/content/Context;

    const v2, 0x7f12008b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0026

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/android/settings/localepicker/c;->Nk:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->mContext:Landroid/content/Context;

    const v2, 0x7f12008f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a002a

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/android/settings/localepicker/c;->No:Landroid/support/v4/view/a/c;

    return-void
.end method


# virtual methods
.method public Hh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/View;Landroid/support/v4/view/a/a;)V
    .locals 4

    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/al;->Hh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/View;Landroid/support/v4/view/a/a;)V

    invoke-virtual {p0}, Lcom/android/settings/localepicker/c;->getItemCount()I

    move-result v0

    invoke-virtual {p0, p3}, Lcom/android/settings/localepicker/c;->drJ(Landroid/view/View;)I

    move-result v1

    check-cast p3, Lcom/android/settings/localepicker/LocaleDragCell;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/android/settings/localepicker/LocaleDragCell;->HN()Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Landroid/support/v4/view/a/a;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/localepicker/c;->Np:Lcom/android/settings/localepicker/d;

    invoke-virtual {v2}, Lcom/android/settings/localepicker/d;->Ht()Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    if-lez v1, :cond_1

    iget-object v2, p0, Lcom/android/settings/localepicker/c;->Nn:Landroid/support/v4/view/a/c;

    invoke-virtual {p4, v2}, Landroid/support/v4/view/a/a;->dMM(Landroid/support/v4/view/a/c;)V

    iget-object v2, p0, Lcom/android/settings/localepicker/c;->Nm:Landroid/support/v4/view/a/c;

    invoke-virtual {p4, v2}, Landroid/support/v4/view/a/a;->dMM(Landroid/support/v4/view/a/c;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    if-ge v1, v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->Nl:Landroid/support/v4/view/a/c;

    invoke-virtual {p4, v1}, Landroid/support/v4/view/a/a;->dMM(Landroid/support/v4/view/a/c;)V

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->Nk:Landroid/support/v4/view/a/c;

    invoke-virtual {p4, v1}, Landroid/support/v4/view/a/a;->dMM(Landroid/support/v4/view/a/c;)V

    :cond_2
    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/localepicker/c;->No:Landroid/support/v4/view/a/c;

    invoke-virtual {p4, v0}, Landroid/support/v4/view/a/a;->dMM(Landroid/support/v4/view/a/c;)V

    :cond_3
    return-void
.end method

.method public Hi(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/settings/localepicker/c;->getItemCount()I

    move-result v2

    invoke-virtual {p0, p3}, Lcom/android/settings/localepicker/c;->drJ(Landroid/view/View;)I

    move-result v3

    packed-switch p4, :pswitch_data_0

    invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/al;->Hi(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0

    :pswitch_0
    if-lez v3, :cond_1

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->Np:Lcom/android/settings/localepicker/d;

    add-int/lit8 v2, v3, -0x1

    invoke-virtual {v1, v3, v2}, Lcom/android/settings/localepicker/d;->Hu(II)V

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->Np:Lcom/android/settings/localepicker/d;

    invoke-virtual {v1}, Lcom/android/settings/localepicker/d;->Hs()V

    :cond_0
    return v0

    :pswitch_1
    add-int/lit8 v4, v3, 0x1

    if-ge v4, v2, :cond_1

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->Np:Lcom/android/settings/localepicker/d;

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v1, v3, v2}, Lcom/android/settings/localepicker/d;->Hu(II)V

    goto :goto_0

    :pswitch_2
    if-eqz v3, :cond_1

    iget-object v2, p0, Lcom/android/settings/localepicker/c;->Np:Lcom/android/settings/localepicker/d;

    invoke-virtual {v2, v3, v1}, Lcom/android/settings/localepicker/d;->Hu(II)V

    goto :goto_0

    :pswitch_3
    add-int/lit8 v4, v2, -0x1

    if-eq v3, v4, :cond_1

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->Np:Lcom/android/settings/localepicker/d;

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v3, v2}, Lcom/android/settings/localepicker/d;->Hu(II)V

    goto :goto_0

    :pswitch_4
    if-le v2, v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/localepicker/c;->Np:Lcom/android/settings/localepicker/d;

    invoke-virtual {v1, v3}, Lcom/android/settings/localepicker/d;->removeItem(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0026
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
