.class public Lcom/android/settings/cH;
.super Lmiui/external/a;
.source "SettingsApplication.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final cdg:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/cH;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/cH;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiui/external/a;-><init>()V

    const-string/jumbo v0, "SECURITY_TOPIC"

    iput-object v0, p0, Lcom/android/settings/cH;->cdg:Ljava/lang/String;

    return-void
.end method

.method private bUP(Landroid/content/Context;)V
    .locals 3

    const-string/jumbo v0, "key_delete_v5_shortcuts"

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "key_delete_v5_shortcuts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/cJ;->getInstance(Landroid/content/Context;)Lcom/android/settings/cJ;

    move-result-object v1

    sget-object v2, Lcom/android/settings/ShortcutHelper$Shortcut;->cdn:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1, v2}, Lcom/android/settings/cJ;->bUV(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v2, Lcom/android/settings/ShortcutHelper$Shortcut;->cdp:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1, v2}, Lcom/android/settings/cJ;->bUV(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v2, Lcom/android/settings/ShortcutHelper$Shortcut;->cdq:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1, v2}, Lcom/android/settings/cJ;->bUV(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v2, Lcom/android/settings/ShortcutHelper$Shortcut;->cdo:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1, v2}, Lcom/android/settings/cJ;->bUV(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v2, Lcom/android/settings/ShortcutHelper$Shortcut;->cdm:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1, v2}, Lcom/android/settings/cJ;->bUV(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v2, Lcom/android/settings/ShortcutHelper$Shortcut;->cdl:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v1, v2}, Lcom/android/settings/cJ;->bUV(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "key_delete_v5_shortcuts"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return-void
.end method

.method static synthetic bUR(Lcom/android/settings/cH;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/cH;->bUP(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bUQ(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    const-string/jumbo v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v4

    :cond_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v3, v1, :cond_1

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    return-object v0

    :cond_2
    return-object v4
.end method

.method public onCreate()V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0}, Lmiui/external/a;->onCreate()V

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/settings/cloud/h;->aLk()V

    invoke-virtual {p0}, Lcom/android/settings/cH;->eqs()Lmiui/external/b;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/external/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "2882303761517161661"

    const-string/jumbo v2, "5881716163661"

    sget-object v3, Lmiui/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->initialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/xiaomi/mistatistic/sdk/CustomSettings;->setUseSystemUploadingService(Z)V

    const-wide/16 v0, 0x0

    invoke-static {v4, v0, v1}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->setUploadPolicy(IJ)V

    invoke-static {v4}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->enableExceptionCatcher(Z)V

    new-instance v0, Lcom/android/settings/cI;

    invoke-virtual {p0}, Lcom/android/settings/cH;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/cI;-><init>(Lcom/android/settings/cH;Landroid/content/Context;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
