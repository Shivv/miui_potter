.class public Lcom/android/settings/MiuiDropDownPreference;
.super Landroid/preference/ListPreference;
.source "MiuiDropDownPreference.java"


# instance fields
.field private final bIR:Landroid/widget/ArrayAdapter;

.field private final bIS:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private bIT:Landroid/widget/Spinner;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/MiuiDropDownPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x7f040095

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/MiuiDropDownPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/MiuiDropDownPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Lcom/android/settings/gO;

    invoke-direct {v0, p0}, Lcom/android/settings/gO;-><init>(Lcom/android/settings/MiuiDropDownPreference;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDropDownPreference;->bIS:Landroid/widget/AdapterView$OnItemSelectedListener;

    iput-object p1, p0, Lcom/android/settings/MiuiDropDownPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->bzp()Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiDropDownPreference;->bIR:Landroid/widget/ArrayAdapter;

    invoke-direct {p0}, Lcom/android/settings/MiuiDropDownPreference;->bzr()V

    return-void
.end method

.method private bzr()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/MiuiDropDownPreference;->bIR:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/android/settings/MiuiDropDownPreference;->bIR:Landroid/widget/ArrayAdapter;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic bzs(Lcom/android/settings/MiuiDropDownPreference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiDropDownPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected bzp()Landroid/widget/ArrayAdapter;
    .locals 3

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/MiuiDropDownPreference;->mContext:Landroid/content/Context;

    const v2, 0x1090009

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public bzq(Ljava/lang/String;)I
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz p1, :cond_1

    if-eqz v1, :cond_1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method protected notifyChanged()V
    .locals 1

    invoke-super {p0}, Landroid/preference/ListPreference;->notifyChanged()V

    iget-object v0, p0, Lcom/android/settings/MiuiDropDownPreference;->bIR:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    new-instance v0, Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/MiuiDropDownPreference;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDropDownPreference;->bIT:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/MiuiDropDownPreference;->bIT:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/MiuiDropDownPreference;->bIR:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/android/settings/MiuiDropDownPreference;->bIT:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/MiuiDropDownPreference;->bIS:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiDropDownPreference;->bIT:Landroid/widget/Spinner;

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiDropDownPreference;->bzq(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onBindView(Landroid/view/View;)V

    return-void
.end method

.method protected onClick()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiDropDownPreference;->bIT:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->performClick()Z

    return-void
.end method

.method public setEntries([Ljava/lang/CharSequence;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiDropDownPreference;->bzr()V

    return-void
.end method

.method public setValueIndex(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiDropDownPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDropDownPreference;->setValue(Ljava/lang/String;)V

    return-void
.end method
