.class public Lcom/android/settings/BandMode;
.super Landroid/app/Activity;
.source "BandMode.java"


# static fields
.field private static final bFD:[Ljava/lang/String;


# instance fields
.field private bFE:Landroid/widget/ListView;

.field private bFF:Landroid/widget/ArrayAdapter;

.field private bFG:Landroid/widget/AdapterView$OnItemClickListener;

.field private bFH:Landroid/os/Handler;

.field private bFI:Lcom/android/internal/telephony/Phone;

.field private bFJ:Landroid/content/DialogInterface;

.field private bFK:Lcom/android/settings/aI;


# direct methods
.method static synthetic -get0()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/BandMode;->bFD:[Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Automatic"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "Europe"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "United States"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string/jumbo v1, "Japan"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string/jumbo v1, "Australia"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string/jumbo v1, "Australia 2"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "Cellular 800"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string/jumbo v1, "PCS"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 3 (JTACS)"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 4 (Korea-PCS)"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 5"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 6 (IMT2000)"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 7 (700Mhz-Upper)"

    const/16 v2, 0xc

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 8 (1800Mhz-Upper)"

    const/16 v2, 0xd

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 9 (900Mhz)"

    const/16 v2, 0xe

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 10 (800Mhz-Secondary)"

    const/16 v2, 0xf

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 11 (Europe PAMR 400Mhz)"

    const/16 v2, 0x10

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 15 (US-AWS)"

    const/16 v2, 0x11

    aput-object v1, v0, v2

    const-string/jumbo v1, "Class 16 (US-2500Mhz)"

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/BandMode;->bFD:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/android/settings/BandMode;->bFK:Lcom/android/settings/aI;

    iput-object v0, p0, Lcom/android/settings/BandMode;->bFI:Lcom/android/internal/telephony/Phone;

    new-instance v0, Lcom/android/settings/gf;

    invoke-direct {v0, p0}, Lcom/android/settings/gf;-><init>(Lcom/android/settings/BandMode;)V

    iput-object v0, p0, Lcom/android/settings/BandMode;->bFG:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Lcom/android/settings/gg;

    invoke-direct {v0, p0}, Lcom/android/settings/gg;-><init>(Lcom/android/settings/BandMode;)V

    iput-object v0, p0, Lcom/android/settings/BandMode;->bFH:Landroid/os/Handler;

    return-void
.end method

.method private bvB(Landroid/os/AsyncResult;)V
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/BandMode;->bFJ:Landroid/content/DialogInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/BandMode;->bFJ:Landroid/content/DialogInterface;

    invoke-interface {v0}, Landroid/content/DialogInterface;->dismiss()V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/BandMode;->bvC()V

    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v0, :cond_5

    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v0, [I

    array-length v3, v0

    if-nez v3, :cond_1

    const-string/jumbo v0, "phone"

    const-string/jumbo v1, "No Supported Band Modes"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    aget v4, v0, v1

    if-lez v4, :cond_5

    iget-object v3, p0, Lcom/android/settings/BandMode;->bFF:Landroid/widget/ArrayAdapter;

    new-instance v5, Lcom/android/settings/aI;

    invoke-direct {v5, v1}, Lcom/android/settings/aI;-><init>(I)V

    invoke-virtual {v3, v5}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    move v3, v2

    :goto_0
    if-gt v3, v4, :cond_3

    aget v5, v0, v3

    if-nez v5, :cond_2

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    new-instance v5, Lcom/android/settings/aI;

    aget v6, v0, v3

    invoke-direct {v5, v6}, Lcom/android/settings/aI;-><init>(I)V

    iget-object v6, p0, Lcom/android/settings/BandMode;->bFF:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6, v5}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v0, v2

    :goto_2
    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    const/16 v1, 0x13

    if-ge v0, v1, :cond_4

    new-instance v1, Lcom/android/settings/aI;

    invoke-direct {v1, v0}, Lcom/android/settings/aI;-><init>(I)V

    iget-object v2, p0, Lcom/android/settings/BandMode;->bFF:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/android/settings/BandMode;->bFE:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    return-void

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method private bvC()V
    .locals 3

    const/4 v2, 0x0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/BandMode;->bFF:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/BandMode;->bFF:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/settings/BandMode;->bFF:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private bvD(Ljava/lang/Throwable;)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f1201ef

    invoke-virtual {p0, v1}, Lcom/android/settings/BandMode;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/BandMode;->bFK:Lcom/android/settings/aI;

    invoke-virtual {v1}, Lcom/android/settings/aI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f1201ed

    invoke-virtual {p0, v1}, Lcom/android/settings/BandMode;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/BandMode;->bFJ:Landroid/content/DialogInterface;

    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f1201f0

    invoke-virtual {p0, v1}, Lcom/android/settings/BandMode;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private bvE()V
    .locals 2

    const v0, 0x7f1201ee

    invoke-virtual {p0, v0}, Lcom/android/settings/BandMode;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/BandMode;->bFJ:Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/android/settings/BandMode;->bFH:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/BandMode;->bFI:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1, v0}, Lcom/android/internal/telephony/Phone;->queryAvailableBandMode(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic bvF(Lcom/android/settings/BandMode;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BandMode;->bFH:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic bvG(Lcom/android/settings/BandMode;)Lcom/android/internal/telephony/Phone;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BandMode;->bFI:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic bvH(Lcom/android/settings/BandMode;)Lcom/android/settings/aI;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/BandMode;->bFK:Lcom/android/settings/aI;

    return-object v0
.end method

.method static synthetic bvI(Lcom/android/settings/BandMode;Lcom/android/settings/aI;)Lcom/android/settings/aI;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/BandMode;->bFK:Lcom/android/settings/aI;

    return-object p1
.end method

.method static synthetic bvJ(Lcom/android/settings/BandMode;Landroid/os/AsyncResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/BandMode;->bvB(Landroid/os/AsyncResult;)V

    return-void
.end method

.method static synthetic bvK(Lcom/android/settings/BandMode;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/BandMode;->bvD(Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/android/settings/BandMode;->requestWindowFeature(I)Z

    const v0, 0x7f0d0045

    invoke-virtual {p0, v0}, Lcom/android/settings/BandMode;->setContentView(I)V

    const v0, 0x7f1201f1

    invoke-virtual {p0, v0}, Lcom/android/settings/BandMode;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/BandMode;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settings/BandMode;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/BandMode;->bFI:Lcom/android/internal/telephony/Phone;

    const v0, 0x7f0a0084

    invoke-virtual {p0, v0}, Lcom/android/settings/BandMode;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/BandMode;->bFE:Landroid/widget/ListView;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090003

    invoke-direct {v0, p0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/settings/BandMode;->bFF:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/BandMode;->bFE:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/BandMode;->bFF:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/settings/BandMode;->bFE:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/BandMode;->bFG:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-direct {p0}, Lcom/android/settings/BandMode;->bvE()V

    return-void
.end method
