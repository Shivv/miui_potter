.class final Lcom/android/settings/hd;
.super Ljava/lang/Object;
.source "MutedVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# instance fields
.field final synthetic cmF:Lcom/android/settings/MutedVideoView;


# direct methods
.method constructor <init>(Lcom/android/settings/MutedVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hd;->cmF:Lcom/android/settings/MutedVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/hd;->cmF:Lcom/android/settings/MutedVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/MutedVideoView;->bAS(Lcom/android/settings/MutedVideoView;I)I

    iget-object v0, p0, Lcom/android/settings/hd;->cmF:Lcom/android/settings/MutedVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/MutedVideoView;->bAR(Lcom/android/settings/MutedVideoView;I)I

    iget-object v0, p0, Lcom/android/settings/hd;->cmF:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAH(Lcom/android/settings/MutedVideoView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/hd;->cmF:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAG(Lcom/android/settings/MutedVideoView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/hd;->cmF:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Lcom/android/settings/MutedVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/hd;->cmF:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAH(Lcom/android/settings/MutedVideoView;)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/hd;->cmF:Lcom/android/settings/MutedVideoView;

    invoke-static {v2}, Lcom/android/settings/MutedVideoView;->bAG(Lcom/android/settings/MutedVideoView;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    iget-object v0, p0, Lcom/android/settings/hd;->cmF:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Lcom/android/settings/MutedVideoView;->requestLayout()V

    :cond_0
    return-void
.end method
