.class final Lcom/android/settings/lr;
.super Landroid/os/AsyncTask;
.source "SetUpChooseLockPassword.java"


# instance fields
.field final synthetic csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

.field final synthetic cso:Ljava/lang/String;

.field final synthetic csp:Z

.field final synthetic csq:Z


# direct methods
.method constructor <init>(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;Ljava/lang/String;ZZ)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/lr;->csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    iput-object p2, p0, Lcom/android/settings/lr;->cso:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/android/settings/lr;->csp:Z

    iput-boolean p4, p0, Lcom/android/settings/lr;->csq:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected caA([B)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/lr;->csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v0, p1}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZw(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;[B)V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/lr;->doInBackground([Ljava/lang/Void;)[B

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)[B
    .locals 9

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/lr;->csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-virtual {v2}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/lr;->csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v3}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZk(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Lcom/android/settings/bM;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/bn;->bFE(Lcom/android/settings/bM;)J

    move-result-wide v4

    if-eqz v2, :cond_0

    const-string/jumbo v3, "has_challenge"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/lr;->csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v2}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZn(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/lr;->cso:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/settings/lr;->csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v6}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZp(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)I

    move-result v6

    iget-object v7, p0, Lcom/android/settings/lr;->csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v7}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZs(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)I

    move-result v7

    iget-boolean v8, p0, Lcom/android/settings/lr;->csp:Z

    invoke-static {v2, v3, v6, v7, v8}, Lcom/android/settings/bn;->bFF(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;IIZ)V

    iget-boolean v2, p0, Lcom/android/settings/lr;->csq:Z

    if-nez v2, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/lr;->csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v0}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZn(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/lr;->cso:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/lr;->csn:Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;

    invoke-static {v3}, Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;->bZs(Lcom/android/settings/SetUpChooseLockPassword$SetUpChooseLockPasswordFragment;)I

    move-result v3

    invoke-static {v0, v2, v4, v5, v3}, Lcom/android/settings/bn;->bFr(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;JI)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SetUpChooseLockPassword"

    const-string/jumbo v2, "critical: no token returned for known good password"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/android/settings/lr;->caA([B)V

    return-void
.end method
