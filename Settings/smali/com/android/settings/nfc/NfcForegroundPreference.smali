.class public Lcom/android/settings/nfc/NfcForegroundPreference;
.super Landroid/preference/ListPreference;
.source "NfcForegroundPreference.java"

# interfaces
.implements Lcom/android/settings/nfc/PaymentBackend$Callback;


# instance fields
.field private final aXg:Lcom/android/settings/nfc/PaymentBackend;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/nfc/PaymentBackend;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/nfc/NfcForegroundPreference;->aXg:Lcom/android/settings/nfc/PaymentBackend;

    iget-object v0, p0, Lcom/android/settings/nfc/NfcForegroundPreference;->aXg:Lcom/android/settings/nfc/PaymentBackend;

    invoke-virtual {v0, p0}, Lcom/android/settings/nfc/PaymentBackend;->aLE(Lcom/android/settings/nfc/PaymentBackend$Callback;)V

    invoke-virtual {p0}, Lcom/android/settings/nfc/NfcForegroundPreference;->aLJ()V

    new-instance v0, Lcom/android/settings/nfc/NfcForegroundPreference$1;

    invoke-direct {v0, p0}, Lcom/android/settings/nfc/NfcForegroundPreference$1;-><init>(Lcom/android/settings/nfc/NfcForegroundPreference;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/nfc/NfcForegroundPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method


# virtual methods
.method public aLI()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/nfc/NfcForegroundPreference;->aLJ()V

    return-void
.end method

.method aLJ()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/nfc/NfcForegroundPreference;->aXg:Lcom/android/settings/nfc/PaymentBackend;

    invoke-virtual {v0}, Lcom/android/settings/nfc/PaymentBackend;->aLz()Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;

    iget-object v0, p0, Lcom/android/settings/nfc/NfcForegroundPreference;->aXg:Lcom/android/settings/nfc/PaymentBackend;

    invoke-virtual {v0}, Lcom/android/settings/nfc/PaymentBackend;->aLC()Z

    move-result v0

    invoke-virtual {p0, v4}, Lcom/android/settings/nfc/NfcForegroundPreference;->setPersistent(Z)V

    invoke-virtual {p0}, Lcom/android/settings/nfc/NfcForegroundPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f120b6d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/nfc/NfcForegroundPreference;->setTitle(Ljava/lang/CharSequence;)V

    new-array v1, v6, [Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/settings/nfc/NfcForegroundPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f120b65

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0}, Lcom/android/settings/nfc/NfcForegroundPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f120b64

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v1}, Lcom/android/settings/nfc/NfcForegroundPreference;->setEntries([Ljava/lang/CharSequence;)V

    new-array v1, v6, [Ljava/lang/CharSequence;

    const-string/jumbo v2, "1"

    aput-object v2, v1, v4

    const-string/jumbo v2, "0"

    aput-object v2, v1, v5

    invoke-virtual {p0, v1}, Lcom/android/settings/nfc/NfcForegroundPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_0

    const-string/jumbo v0, "1"

    invoke-virtual {p0, v0}, Lcom/android/settings/nfc/NfcForegroundPreference;->setValue(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "0"

    invoke-virtual {p0, v0}, Lcom/android/settings/nfc/NfcForegroundPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected persistString(Ljava/lang/String;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/settings/nfc/NfcForegroundPreference;->aXg:Lcom/android/settings/nfc/PaymentBackend;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v2, v0}, Lcom/android/settings/nfc/PaymentBackend;->aLG(Z)V

    return v1
.end method
