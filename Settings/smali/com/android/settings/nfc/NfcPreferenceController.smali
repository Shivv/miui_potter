.class public Lcom/android/settings/nfc/NfcPreferenceController;
.super Lcom/android/settings/core/c;
.source "NfcPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field private aXE:I

.field private aXF:Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;

.field private aXG:Lcom/android/settingslib/RestrictedPreference;

.field private aXH:Landroid/nfc/NfcAdapter;

.field private aXI:Lcom/android/settings/nfc/NfcEnabler;

.field private aXJ:Landroid/support/v14/preference/SwitchPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXH:Landroid/nfc/NfcAdapter;

    return-void
.end method

.method private aLZ()V
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/nfc/NfcPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "airplane_mode_on"

    iget v3, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXE:I

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iget v2, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXE:I

    if-ne v1, v2, :cond_0

    return-void

    :cond_0
    iput v1, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXE:I

    iget v1, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXE:I

    if-eq v1, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXH:Landroid/nfc/NfcAdapter;

    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->enable()Z

    :goto_1
    iget-object v1, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXJ:Landroid/support/v14/preference/SwitchPreference;

    invoke-virtual {v1, v0}, Landroid/support/v14/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXG:Lcom/android/settingslib/RestrictedPreference;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/RestrictedPreference;->setEnabled(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXH:Landroid/nfc/NfcAdapter;

    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->disable()Z

    goto :goto_1
.end method

.method static synthetic aMa(Lcom/android/settings/nfc/NfcPreferenceController;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic aMb(Lcom/android/settings/nfc/NfcPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/nfc/NfcPreferenceController;->aLZ()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/nfc/NfcPreferenceController;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "toggle_nfc"

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/nfc/NfcPreferenceController;->cdI(Landroid/support/v7/preference/PreferenceScreen;Ljava/lang/String;)V

    const-string/jumbo v0, "android_beam_settings"

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/nfc/NfcPreferenceController;->cdI(Landroid/support/v7/preference/PreferenceScreen;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXI:Lcom/android/settings/nfc/NfcEnabler;

    return-void

    :cond_0
    const-string/jumbo v0, "toggle_nfc"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/support/v14/preference/SwitchPreference;

    iput-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXJ:Landroid/support/v14/preference/SwitchPreference;

    const-string/jumbo v0, "android_beam_settings"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/RestrictedPreference;

    iput-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXG:Lcom/android/settingslib/RestrictedPreference;

    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "airplane_mode_toggleable_radios"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v1, "nfc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_1
    new-instance v0, Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;

    invoke-direct {v0, p0, v2}, Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;-><init>(Lcom/android/settings/nfc/NfcPreferenceController;Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;)V

    iput-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXF:Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;

    invoke-direct {p0}, Lcom/android/settings/nfc/NfcPreferenceController;->aLZ()V

    :cond_2
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXH:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXF:Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXF:Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;

    invoke-virtual {v0}, Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;->aMd()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXI:Lcom/android/settings/nfc/NfcEnabler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXI:Lcom/android/settings/nfc/NfcEnabler;

    invoke-virtual {v0}, Lcom/android/settings/nfc/NfcEnabler;->pause()V

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXF:Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXF:Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;

    invoke-virtual {v0}, Lcom/android/settings/nfc/NfcPreferenceController$AirplaneModeObserver;->aMc()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXI:Lcom/android/settings/nfc/NfcEnabler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/nfc/NfcPreferenceController;->aXI:Lcom/android/settings/nfc/NfcEnabler;

    invoke-virtual {v0}, Lcom/android/settings/nfc/NfcEnabler;->aMj()V

    :cond_1
    return-void
.end method
