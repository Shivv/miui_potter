.class public Lcom/android/settings/nfc/MiuiAndroidBeam;
.super Lcom/android/settings/BaseFragment;
.source "MiuiAndroidBeam.java"


# instance fields
.field private aXO:Lmiui/widget/SlidingButton;

.field private aXP:Landroid/nfc/NfcAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    return-void
.end method

.method private aMg(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->aXP:Landroid/nfc/NfcAdapter;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x7f0a0058

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SlidingButton;

    iput-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->aXO:Lmiui/widget/SlidingButton;

    iget-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->aXO:Lmiui/widget/SlidingButton;

    iget-object v1, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->aXP:Landroid/nfc/NfcAdapter;

    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->isNdefPushEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->aXO:Lmiui/widget/SlidingButton;

    new-instance v1, Lcom/android/settings/nfc/MiuiAndroidBeam$1;

    invoke-direct {v1, p0}, Lcom/android/settings/nfc/MiuiAndroidBeam$1;-><init>(Lcom/android/settings/nfc/MiuiAndroidBeam;)V

    invoke-virtual {v0, v1}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method static synthetic aMh(Lcom/android/settings/nfc/MiuiAndroidBeam;)Lmiui/widget/SlidingButton;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->aXO:Lmiui/widget/SlidingButton;

    return-object v0
.end method

.method static synthetic aMi(Lcom/android/settings/nfc/MiuiAndroidBeam;)Landroid/nfc/NfcAdapter;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->aXP:Landroid/nfc/NfcAdapter;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/nfc/MiuiAndroidBeam;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->aXP:Landroid/nfc/NfcAdapter;

    iget-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->aXP:Landroid/nfc/NfcAdapter;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/nfc/MiuiAndroidBeam;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d002a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/nfc/MiuiAndroidBeam;->aMg(Landroid/view/View;)V

    return-object v0
.end method
