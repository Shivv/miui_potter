.class public Lcom/android/settings/nfc/PaymentBackend;
.super Ljava/lang/Object;
.source "PaymentBackend.java"


# instance fields
.field private final aWS:Landroid/nfc/NfcAdapter;

.field private aWT:Ljava/util/ArrayList;

.field private aWU:Ljava/util/ArrayList;

.field private final aWV:Landroid/nfc/cardemulation/CardEmulation;

.field private aWW:Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;

.field private final aWX:Landroid/os/Handler;

.field private final aWY:Lcom/android/internal/content/PackageMonitor;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/nfc/PaymentBackend$SettingsPackageMonitor;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/nfc/PaymentBackend$SettingsPackageMonitor;-><init>(Lcom/android/settings/nfc/PaymentBackend;Lcom/android/settings/nfc/PaymentBackend$SettingsPackageMonitor;)V

    iput-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWY:Lcom/android/internal/content/PackageMonitor;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWU:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/settings/nfc/PaymentBackend$1;

    invoke-direct {v0, p0}, Lcom/android/settings/nfc/PaymentBackend$1;-><init>(Lcom/android/settings/nfc/PaymentBackend;)V

    iput-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWX:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/settings/nfc/PaymentBackend;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWS:Landroid/nfc/NfcAdapter;

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWS:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWS:Landroid/nfc/NfcAdapter;

    invoke-static {v0}, Landroid/nfc/cardemulation/CardEmulation;->getInstance(Landroid/nfc/NfcAdapter;)Landroid/nfc/cardemulation/CardEmulation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWV:Landroid/nfc/cardemulation/CardEmulation;

    invoke-virtual {p0}, Lcom/android/settings/nfc/PaymentBackend;->aLy()V

    :goto_0
    return-void

    :cond_0
    iput-object v1, p0, Lcom/android/settings/nfc/PaymentBackend;->aWV:Landroid/nfc/cardemulation/CardEmulation;

    goto :goto_0
.end method

.method static synthetic aLH(Lcom/android/settings/nfc/PaymentBackend;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWX:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public aLA()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWT:Ljava/util/ArrayList;

    return-object v0
.end method

.method aLB()Landroid/content/ComponentName;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "nfc_payment_default_component"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v2
.end method

.method aLC()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/nfc/PaymentBackend;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "nfc_payment_foreground"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    move-exception v1

    return v0
.end method

.method aLD()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWU:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/nfc/PaymentBackend$Callback;

    invoke-interface {v0}, Lcom/android/settings/nfc/PaymentBackend$Callback;->aLI()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public aLE(Lcom/android/settings/nfc/PaymentBackend$Callback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWU:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public aLF(Landroid/content/ComponentName;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/nfc/PaymentBackend;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "nfc_payment_default_component"

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-virtual {p0}, Lcom/android/settings/nfc/PaymentBackend;->aLy()V

    return-void
.end method

.method aLG(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "nfc_payment_foreground"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aLy()V
    .locals 11

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWV:Landroid/nfc/cardemulation/CardEmulation;

    const-string/jumbo v1, "payment"

    invoke-virtual {v0, v1}, Landroid/nfc/cardemulation/CardEmulation;->getServices(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/nfc/PaymentBackend;->aLD()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/nfc/PaymentBackend;->aLB()Landroid/content/ComponentName;

    move-result-object v6

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v1, v2

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/nfc/cardemulation/ApduServiceInfo;

    new-instance v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;

    invoke-direct {v3}, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;-><init>()V

    invoke-virtual {v0, v4}, Landroid/nfc/cardemulation/ApduServiceInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    iput-object v8, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aWZ:Ljava/lang/CharSequence;

    iget-object v8, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aWZ:Ljava/lang/CharSequence;

    if-nez v8, :cond_1

    invoke-virtual {v0, v4}, Landroid/nfc/cardemulation/ApduServiceInfo;->loadAppLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    iput-object v8, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aWZ:Ljava/lang/CharSequence;

    :cond_1
    invoke-virtual {v0}, Landroid/nfc/cardemulation/ApduServiceInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v8

    iput-boolean v8, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aXd:Z

    iget-boolean v8, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aXd:Z

    if-eqz v8, :cond_2

    move-object v1, v3

    :cond_2
    invoke-virtual {v0}, Landroid/nfc/cardemulation/ApduServiceInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    iput-object v8, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aXb:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/nfc/cardemulation/ApduServiceInfo;->getSettingsActivityName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    new-instance v9, Landroid/content/ComponentName;

    iget-object v10, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aXb:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v9, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aXe:Landroid/content/ComponentName;

    :goto_1
    invoke-virtual {v0}, Landroid/nfc/cardemulation/ApduServiceInfo;->getDescription()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aXc:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/nfc/cardemulation/ApduServiceInfo;->loadBanner(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aXa:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iput-object v2, v3, Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;->aXe:Landroid/content/ComponentName;

    goto :goto_1

    :cond_4
    iput-object v5, p0, Lcom/android/settings/nfc/PaymentBackend;->aWT:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/android/settings/nfc/PaymentBackend;->aWW:Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;

    invoke-virtual {p0}, Lcom/android/settings/nfc/PaymentBackend;->aLD()V

    return-void
.end method

.method public aLz()Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWW:Lcom/android/settings/nfc/PaymentBackend$PaymentAppInfo;

    return-object v0
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWY:Lcom/android/internal/content/PackageMonitor;

    invoke-virtual {v0}, Lcom/android/internal/content/PackageMonitor;->unregister()V

    return-void
.end method

.method public onResume()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/nfc/PaymentBackend;->aWY:Lcom/android/internal/content/PackageMonitor;

    iget-object v1, p0, Lcom/android/settings/nfc/PaymentBackend;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/nfc/PaymentBackend;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Z)V

    return-void
.end method
