.class public Lcom/android/settings/MiuiSecurityTrustedCredentials;
.super Lcom/android/settings/BaseFragment;
.source "MiuiSecurityTrustedCredentials.java"

# interfaces
.implements Lmiui/app/ActionBar$FragmentViewPagerChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    return-void
.end method

.method private bOY(Lmiui/app/ActionBar;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;I)V
    .locals 7

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "tab_tag"

    invoke-static {p2}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPg(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lmiui/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {p2}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPf(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    invoke-static {p2}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPg(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;)Ljava/lang/String;

    move-result-object v1

    const-class v4, Lcom/android/settings/MiuiSecurityTrustedCredentials$TrustedCredentialFragment;

    const/4 v6, 0x1

    move-object v0, p1

    move v3, p3

    invoke-virtual/range {v0 .. v6}, Lmiui/app/ActionBar;->addFragmentTab(Ljava/lang/String;Landroid/app/ActionBar$Tab;ILjava/lang/Class;Landroid/os/Bundle;Z)I

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0

    return-void
.end method

.method public onPageScrolled(IFZZ)V
    .locals 0

    return-void
.end method

.method public onPageSelected(I)V
    .locals 0

    return-void
.end method

.method public vB(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v3

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/app/ActionBar;->setFragmentViewPagerMode(Landroid/content/Context;Landroid/app/FragmentManager;)V

    invoke-virtual {v0, p0}, Lmiui/app/ActionBar;->addOnFragmentViewPagerChangeListener(Lmiui/app/ActionBar$FragmentViewPagerChangeListener;)V

    sget-object v1, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVl:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    invoke-direct {p0, v0, v1, v4}, Lcom/android/settings/MiuiSecurityTrustedCredentials;->bOY(Lmiui/app/ActionBar;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;I)V

    sget-object v1, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVm:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/MiuiSecurityTrustedCredentials;->bOY(Lmiui/app/ActionBar;Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;I)V

    invoke-virtual {v0, v4}, Lmiui/app/ActionBar;->setSelectedNavigationItem(I)V

    return-object v3
.end method
