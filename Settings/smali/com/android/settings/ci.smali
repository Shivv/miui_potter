.class public Lcom/android/settings/ci;
.super Lcom/android/internal/widget/PagerAdapter;
.source "PreviewPagerAdapter.java"


# static fields
.field private static final bWP:Landroid/view/animation/Interpolator;

.field private static final bWQ:Landroid/view/animation/Interpolator;


# instance fields
.field private bWR:I

.field private bWS:Ljava/lang/Runnable;

.field private bWT:Z

.field private bWU:[Landroid/widget/FrameLayout;

.field private bWV:[[Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/android/settings/ci;->bWP:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Lcom/android/settings/ci;->bWQ:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z[I[Landroid/content/res/Configuration;)V
    .locals 7

    const/4 v6, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/internal/widget/PagerAdapter;-><init>()V

    iput-boolean p2, p0, Lcom/android/settings/ci;->bWT:Z

    array-length v0, p3

    new-array v0, v0, [Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v2, 0x2

    new-array v2, v2, [I

    array-length v3, p3

    aput v3, v2, v1

    array-length v3, p4

    const/4 v4, 0x1

    aput v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/settings/ci;->bWV:[[Z

    move v0, v1

    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_2

    iget-boolean v2, p0, Lcom/android/settings/ci;->bWT:Z

    if-eqz v2, :cond_0

    array-length v2, p3

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    :goto_1
    iget-object v3, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    new-instance v4, Landroid/widget/FrameLayout;

    invoke-direct {v4, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    aput-object v4, v3, v2

    iget-object v3, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    aget-object v3, v3, v2

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v3, v1

    :goto_2
    array-length v4, p4

    if-ge v3, v4, :cond_1

    aget-object v4, p4, v3

    invoke-virtual {p1, v4}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getThemeResId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->setTheme(I)V

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    new-instance v5, Landroid/view/ViewStub;

    invoke-direct {v5, v4}, Landroid/view/ViewStub;-><init>(Landroid/content/Context;)V

    aget v4, p3, v0

    invoke-virtual {v5, v4}, Landroid/view/ViewStub;->setLayoutResource(I)V

    new-instance v4, Lcom/android/settings/jZ;

    invoke-direct {v4, p0, v0, v3}, Lcom/android/settings/jZ;-><init>(Lcom/android/settings/ci;II)V

    invoke-virtual {v5, v4}, Landroid/view/ViewStub;->setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V

    iget-object v4, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    aget-object v4, v4, v2

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_0
    move v2, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private bQK()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/ci;->bWS:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/ci;->bQH()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/ci;->bWS:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iput-object v1, p0, Lcom/android/settings/ci;->bWS:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method private bQL(Landroid/view/View;IZ)V
    .locals 6

    const-wide/16 v4, 0x190

    const/4 v2, 0x0

    if-nez p2, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    if-nez p3, :cond_1

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    sget-object v1, Lcom/android/settings/ci;->bWP:Landroid/view/animation/Interpolator;

    :goto_2
    if-nez p2, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/android/settings/ci;->bWP:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/settings/cj;

    invoke-direct {v1, p0, v2}, Lcom/android/settings/cj;-><init>(Lcom/android/settings/ci;Lcom/android/settings/cj;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/settings/ka;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/ka;-><init>(Lcom/android/settings/ci;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/android/settings/ci;->bWQ:Landroid/view/animation/Interpolator;

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/android/settings/ci;->bWQ:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/settings/cj;

    invoke-direct {v1, p0, v2}, Lcom/android/settings/cj;-><init>(Lcom/android/settings/ci;Lcom/android/settings/cj;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/settings/kb;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/kb;-><init>(Lcom/android/settings/ci;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method

.method static synthetic bQM(Lcom/android/settings/ci;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/ci;->bWR:I

    return v0
.end method

.method static synthetic bQN(Lcom/android/settings/ci;)[[Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ci;->bWV:[[Z

    return-object v0
.end method

.method static synthetic bQO(Lcom/android/settings/ci;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/ci;->bWR:I

    return p1
.end method

.method static synthetic bQP(Lcom/android/settings/ci;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ci;->bQK()V

    return-void
.end method


# virtual methods
.method bQH()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/ci;->bWR:I

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method bQI(Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/ci;->bWS:Ljava/lang/Runnable;

    return-void
.end method

.method bQJ(IIIZ)V
    .locals 8

    const/4 v7, 0x4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    if-ltz p2, :cond_0

    invoke-virtual {v5, p2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v6, p0, Lcom/android/settings/ci;->bWV:[[Z

    aget-object v6, v6, p3

    aget-boolean v6, v6, p2

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    aget-object v6, v6, p3

    if-ne v5, v6, :cond_2

    invoke-direct {p0, v0, v7, p4}, Lcom/android/settings/ci;->bQL(Landroid/view/View;IZ)V

    :cond_0
    :goto_1
    invoke-virtual {v5, p1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v6, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    aget-object v6, v6, p3

    if-ne v5, v6, :cond_3

    iget-object v5, p0, Lcom/android/settings/ci;->bWV:[[Z

    aget-object v5, v5, p3

    aget-boolean v5, v5, p1

    if-nez v5, :cond_1

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    invoke-direct {p0, v0, v2, p4}, Lcom/android/settings/ci;->bQL(Landroid/view/View;IZ)V

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0, v7, v2}, Lcom/android/settings/ci;->bQL(Landroid/view/View;IZ)V

    goto :goto_1

    :cond_3
    invoke-direct {p0, v0, v2, v2}, Lcom/android/settings/ci;->bQL(Landroid/view/View;IZ)V

    goto :goto_2

    :cond_4
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    array-length v0, v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    aget-object v0, v0, p2

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/ci;->bWU:[Landroid/widget/FrameLayout;

    aget-object v0, v0, p2

    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
