.class Lcom/android/settings/w;
.super Ljava/lang/Object;
.source "DiracEqualizer.java"


# instance fields
.field public final bvS:Ljava/lang/String;

.field public final bvT:I

.field public final bvU:[F

.field public final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[FI)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x7

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/settings/w;->bvU:[F

    iput-object p1, p0, Lcom/android/settings/w;->bvS:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/w;->mTitle:Ljava/lang/String;

    iput p4, p0, Lcom/android/settings/w;->bvT:I

    invoke-virtual {p0, p3}, Lcom/android/settings/w;->bjm([F)V

    return-void
.end method


# virtual methods
.method public bjm([F)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    array-length v0, p1

    iget-object v1, p0, Lcom/android/settings/w;->bvU:[F

    array-length v1, v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/w;->bvU:[F

    iget-object v1, p0, Lcom/android/settings/w;->bvU:[F

    array-length v1, v1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy([FI[FII)V

    :cond_0
    return-void
.end method
