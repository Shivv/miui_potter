.class public Lcom/android/settings/V;
.super Ljava/lang/Object;
.source "MiuiOptionUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bmy(I)I
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    const/4 v3, -0x1

    if-eq p0, v3, :cond_2

    if-eq p0, v2, :cond_2

    if-lez p0, :cond_1

    :goto_1
    invoke-static {v0}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    return p0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    return v2
.end method
