.class public Lcom/android/settings/bG;
.super Ljava/lang/Object;
.source "MusicDsClient.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bMg()Lcom/android/settings/bG;
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    const-string/jumbo v0, "com.android.settings.dolby.DsClientWrapper"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :try_start_1
    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bG;

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "MusicDsClient"

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v1, "MusicDsClient"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/settings/bG;

    invoke-direct {v0}, Lcom/android/settings/bG;-><init>()V

    return-object v0
.end method


# virtual methods
.method public bMh(Lcom/android/settings/bH;)V
    .locals 0

    return-void
.end method

.method public bMi(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bMj(Landroid/content/Context;)V
    .locals 0

    return-void
.end method

.method public bMk()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bMl()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public bMm()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bMn()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
