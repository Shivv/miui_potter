.class Lcom/android/settings/be;
.super Landroid/widget/ArrayAdapter;
.source "MiuiLocaleSettings.java"


# instance fields
.field final synthetic bKD:Lcom/android/settings/MiuiLocaleSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/MiuiLocaleSettings;Landroid/content/Context;IILjava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/be;->bKD:Lcom/android/settings/MiuiLocaleSettings;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a0272

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {p0, p1}, Lcom/android/settings/be;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/bf;

    iget-object v1, v1, Lcom/android/settings/bf;->bKE:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/be;->bKD:Lcom/android/settings/MiuiLocaleSettings;

    invoke-static {v3}, Lcom/android/settings/MiuiLocaleSettings;->bBg(Lcom/android/settings/MiuiLocaleSettings;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    return-object v2
.end method
