.class final Lcom/android/settings/dI;
.super Ljava/lang/Object;
.source "DiracEqualizer.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic chD:Lcom/android/settings/DiracEqualizer;


# direct methods
.method constructor <init>(Lcom/android/settings/DiracEqualizer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dI;->chD:Lcom/android/settings/DiracEqualizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    const-string/jumbo v0, "DiracEqualizer_Setting"

    const-string/jumbo v1, "listDialog onClick"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/dI;->chD:Lcom/android/settings/DiracEqualizer;

    invoke-static {v0}, Lcom/android/settings/DiracEqualizer;->bja(Lcom/android/settings/DiracEqualizer;)Lcom/android/settings/x;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/settings/x;->bjq(I)Lcom/android/settings/w;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/dI;->chD:Lcom/android/settings/DiracEqualizer;

    invoke-static {v1}, Lcom/android/settings/DiracEqualizer;->bjb(Lcom/android/settings/DiracEqualizer;)Lcom/android/settings/w;

    move-result-object v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/dI;->chD:Lcom/android/settings/DiracEqualizer;

    invoke-virtual {v1, v0}, Lcom/android/settings/DiracEqualizer;->biD(Lcom/android/settings/w;)V

    iget-object v1, p0, Lcom/android/settings/dI;->chD:Lcom/android/settings/DiracEqualizer;

    invoke-virtual {v1, v0}, Lcom/android/settings/DiracEqualizer;->biV(Lcom/android/settings/w;)V

    iget-object v0, p0, Lcom/android/settings/dI;->chD:Lcom/android/settings/DiracEqualizer;

    invoke-static {v0}, Lcom/android/settings/DiracEqualizer;->bjj(Lcom/android/settings/DiracEqualizer;)V

    iget-object v0, p0, Lcom/android/settings/dI;->chD:Lcom/android/settings/DiracEqualizer;

    invoke-static {v0}, Lcom/android/settings/DiracEqualizer;->bjk(Lcom/android/settings/DiracEqualizer;)V

    iget-object v0, p0, Lcom/android/settings/dI;->chD:Lcom/android/settings/DiracEqualizer;

    invoke-static {v0}, Lcom/android/settings/DiracEqualizer;->bjl(Lcom/android/settings/DiracEqualizer;)V

    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_0
    const-string/jumbo v0, "DiracEqualizer_Setting"

    const-string/jumbo v1, "listDialog invalid config"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
