.class public final Lcom/android/settings/gestures/GesturePreference;
.super Landroid/support/v14/preference/SwitchPreference;
.source "GesturePreference.java"


# instance fields
.field private aYS:Z

.field private aYT:Landroid/media/MediaPlayer;

.field private aYU:I

.field private aYV:Z

.field private aYW:Landroid/net/Uri;

.field private aYX:Z

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/support/v14/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/android/settings/gestures/GesturePreference;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/android/settings/cw;->bYK:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v3, "android.resource"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYW:Landroid/net/Uri;

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/gestures/GesturePreference;->aYW:Landroid/net/Uri;

    invoke-static {v0, v2}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    if-lez v0, :cond_0

    const v0, 0x7f0d00ca

    invoke-virtual {p0, v0}, Lcom/android/settings/gestures/GesturePreference;->setLayoutResource(I)V

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYU:I

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/android/settings/gestures/n;

    invoke-direct {v2, p0}, Lcom/android/settings/gestures/n;-><init>(Lcom/android/settings/gestures/GesturePreference;)V

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/android/settings/gestures/o;

    invoke-direct {v2, p0}, Lcom/android/settings/gestures/o;-><init>(Lcom/android/settings/gestures/GesturePreference;)V

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYS:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string/jumbo v0, "GesturePreference"

    const-string/jumbo v2, "Animation resource not found. Will not show animation."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method static synthetic aMQ(Lcom/android/settings/gestures/GesturePreference;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic aMR(Lcom/android/settings/gestures/GesturePreference;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYV:Z

    return v0
.end method

.method static synthetic aMS(Lcom/android/settings/gestures/GesturePreference;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYX:Z

    return v0
.end method

.method static synthetic aMT(Lcom/android/settings/gestures/GesturePreference;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/gestures/GesturePreference;->aYV:Z

    return p1
.end method

.method static synthetic aMU(Lcom/android/settings/gestures/GesturePreference;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/gestures/GesturePreference;->aYX:Z

    return p1
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v14/preference/SwitchPreference;->al(Landroid/support/v7/preference/l;)V

    iget-boolean v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYS:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x7f0a01c7

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    const v1, 0x7f0a01c5

    invoke-virtual {p1, v1}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v2, p0, Lcom/android/settings/gestures/GesturePreference;->aYU:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    const v2, 0x7f0a01c6

    invoke-virtual {p1, v2}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    new-instance v3, Lcom/android/settings/gestures/p;

    invoke-direct {v3, p0, v2}, Lcom/android/settings/gestures/p;-><init>(Lcom/android/settings/gestures/GesturePreference;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v3}, Landroid/view/TextureView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Lcom/android/settings/gestures/q;

    invoke-direct {v3, p0, v1, v2}, Lcom/android/settings/gestures/q;-><init>(Lcom/android/settings/gestures/GesturePreference;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v3}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    return-void
.end method

.method public iN()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/android/settings/gestures/GesturePreference;->aYT:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    :cond_0
    invoke-super {p0}, Landroid/support/v14/preference/SwitchPreference;->iN()V

    return-void
.end method
