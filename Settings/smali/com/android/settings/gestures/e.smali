.class Lcom/android/settings/gestures/e;
.super Lcom/android/settings/applications/assist/f;
.source "AssistGesturePreferenceController.java"


# instance fields
.field private final aZe:Landroid/net/Uri;

.field final synthetic aZf:Lcom/android/settings/gestures/d;


# direct methods
.method constructor <init>(Lcom/android/settings/gestures/d;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/gestures/e;->aZf:Lcom/android/settings/gestures/d;

    invoke-direct {p0}, Lcom/android/settings/applications/assist/f;-><init>()V

    const-string/jumbo v0, "assist_gesture_enabled"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/gestures/e;->aZe:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method protected mM()Ljava/util/List;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/settings/gestures/e;->aZe:Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public mN()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/gestures/e;->aZf:Lcom/android/settings/gestures/d;

    invoke-static {v0}, Lcom/android/settings/gestures/d;->aNa(Lcom/android/settings/gestures/d;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/gestures/e;->aZf:Lcom/android/settings/gestures/d;

    invoke-virtual {v1}, Lcom/android/settings/gestures/d;->p()Z

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/gestures/e;->aZf:Lcom/android/settings/gestures/d;

    invoke-static {v0}, Lcom/android/settings/gestures/d;->aNc(Lcom/android/settings/gestures/d;)V

    iget-object v0, p0, Lcom/android/settings/gestures/e;->aZf:Lcom/android/settings/gestures/d;

    iget-object v1, p0, Lcom/android/settings/gestures/e;->aZf:Lcom/android/settings/gestures/d;

    invoke-virtual {v1}, Lcom/android/settings/gestures/d;->p()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/gestures/d;->aNb(Lcom/android/settings/gestures/d;Z)Z

    :cond_0
    return-void
.end method
