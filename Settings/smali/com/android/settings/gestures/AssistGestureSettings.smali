.class public Lcom/android/settings/gestures/AssistGestureSettings;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "AssistGestureSettings.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/gestures/s;

    invoke-direct {v0}, Lcom/android/settings/gestures/s;-><init>()V

    sput-object v0, Lcom/android/settings/gestures/AssistGestureSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method

.method private static aMX(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/android/settings/gestures/d;

    const-string/jumbo v2, "gesture_assist"

    invoke-direct {v1, p0, p1, v2}, Lcom/android/settings/gestures/d;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/gestures/g;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/gestures/g;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method static synthetic aMY(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;
    .locals 1

    invoke-static {p0, p1}, Lcom/android/settings/gestures/AssistGestureSettings;->aMX(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "AssistGesture"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f150021

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/gestures/AssistGestureSettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/settings/gestures/AssistGestureSettings;->aMX(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
