.class public Lcom/android/settings/MiuiLocaleSettings;
.super Lcom/android/settings/BaseListFragment;
.source "MiuiLocaleSettings.java"


# instance fields
.field private bKA:Landroid/os/Handler;

.field private bKB:Ljava/lang/String;

.field private bKC:Z

.field private bKy:Landroid/widget/ArrayAdapter;

.field private bKz:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseListFragment;-><init>()V

    new-instance v0, Lcom/android/settings/ho;

    invoke-direct {v0, p0}, Lcom/android/settings/ho;-><init>(Lcom/android/settings/MiuiLocaleSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKA:Landroid/os/Handler;

    return-void
.end method

.method private bBc([Ljava/lang/String;)Landroid/widget/ArrayAdapter;
    .locals 6

    const/4 v1, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    new-instance v2, Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    aget-object v4, p1, v0

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/android/settings/bf;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/android/settings/bf;-><init>(Lcom/android/settings/MiuiLocaleSettings;Lcom/android/settings/bf;)V

    aget-object v4, p1, v0

    iput-object v4, v3, Lcom/android/settings/bf;->bKE:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/android/settings/bf;->bKF:Ljava/lang/String;

    iget-object v2, v3, Lcom/android/settings/bf;->bKE:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/MiuiLocaleSettings;->bKB:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, v3, Lcom/android/settings/bf;->bKF:Ljava/lang/String;

    aput-object v4, v2, v1

    const v4, 0x7f120483

    invoke-virtual {p0, v4, v2}, Lcom/android/settings/MiuiLocaleSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/android/settings/bf;->bKF:Ljava/lang/String;

    invoke-virtual {v5, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/android/settings/be;

    invoke-virtual {p0}, Lcom/android/settings/MiuiLocaleSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0d00db

    const v4, 0x7f0a0272

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/be;-><init>(Lcom/android/settings/MiuiLocaleSettings;Landroid/content/Context;IILjava/util/List;)V

    return-object v0
.end method

.method private bBd([Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget-object v1, p1, v0

    invoke-virtual {p2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    array-length v0, p1

    return v0
.end method

.method private bBe()V
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/android/settings/MiuiLocaleSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    const v1, 0x7f12086c

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiLocaleSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKA:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method static synthetic bBf(Lcom/android/settings/MiuiLocaleSettings;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic bBg(Lcom/android/settings/MiuiLocaleSettings;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKB:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bBh(Lcom/android/settings/MiuiLocaleSettings;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic bBi(Lcom/android/settings/MiuiLocaleSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiLocaleSettings;->bKC:Z

    return p1
.end method

.method static synthetic bBj(Lcom/android/settings/MiuiLocaleSettings;[Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/MiuiLocaleSettings;->bBd([Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKB:Ljava/lang/String;

    invoke-static {}, Lmiui/os/MiuiInit;->getCustVariants()[Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string/jumbo v1, "on_setup"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/MiuiLocaleSettings;->bKC:Z

    iget-boolean v1, p0, Lcom/android/settings/MiuiLocaleSettings;->bKC:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/MiuiLocaleSettings;->bBe()V

    :cond_0
    if-eqz v0, :cond_1

    array-length v1, v0

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiLocaleSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0300ae

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/android/settings/hp;

    invoke-direct {v2, p0, v1}, Lcom/android/settings/hp;-><init>(Lcom/android/settings/MiuiLocaleSettings;[Ljava/lang/String;)V

    invoke-static {v0, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiLocaleSettings;->bBc([Ljava/lang/String;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKy:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKy:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiLocaleSettings;->setListAdapter(Landroid/widget/ListAdapter;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKA:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    invoke-super/range {p0 .. p5}, Lcom/android/settings/BaseListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKy:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bf;

    iget-object v0, v0, Lcom/android/settings/bf;->bKE:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/MiuiLocaleSettings;->bKB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKB:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/MiuiLocaleSettings;->bKy:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/os/MiuiInit;->initCustEnvironment(Ljava/lang/String;Lmiui/os/IMiuiInitObserver;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string/jumbo v2, "time-zone"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKC:Z

    invoke-direct {p0}, Lcom/android/settings/MiuiLocaleSettings;->bBe()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiLocaleSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    new-instance v0, Lcom/android/settings/hq;

    invoke-direct {v0, p0}, Lcom/android/settings/hq;-><init>(Lcom/android/settings/MiuiLocaleSettings;)V

    invoke-virtual {v0}, Lcom/android/settings/hq;->start()V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "MiuiLocaleSettings"

    const-string/jumbo v1, "Fail to call MiuiInit.initCustEnvironment, please retry."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/MiuiLocaleSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "MiuiLocaleSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No need to set since same local chosen,selectedLocale="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/BaseListFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKA:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    iput-object v2, p0, Lcom/android/settings/MiuiLocaleSettings;->bKz:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "on_setup"

    iget-boolean v1, p0, Lcom/android/settings/MiuiLocaleSettings;->bKC:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
