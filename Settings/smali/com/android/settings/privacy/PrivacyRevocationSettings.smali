.class public Lcom/android/settings/privacy/PrivacyRevocationSettings;
.super Lmiui/app/Activity;
.source "PrivacyRevocationSettings.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final LC:[Ljava/lang/String;

.field private static LE:Landroid/os/CountDownTimer;


# instance fields
.field private LD:Lcom/android/settings/privacy/f;

.field private LF:Landroid/widget/ListView;

.field private LG:Landroid/app/AlertDialog;


# direct methods
.method static synthetic -get0()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LC:[Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "com.miui.videoplayer"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "pl.zdunex25.updater"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.securitycenter"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.msa.global"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.daemon"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.bugreport"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.providers.downloads.ui"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.discover"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.xiaomi.simactivate.service"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.klo.bugreport"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.miui.powerkeeper"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string/jumbo v1, "com.android.settings"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LC:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method private Fs(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string/jumbo v1, "privacy_revoke_tips"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "PrivacyRevocationSettings"

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    return-object v3
.end method

.method private Fu(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "privacy_status_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private Fv(Landroid/content/Context;Ljava/lang/String;Lcom/android/settings/privacy/b;)Landroid/app/AlertDialog;
    .locals 8

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f120d2b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f120d2a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    :goto_0
    new-instance v1, Lcom/android/settings/privacy/m;

    invoke-direct {v1, p0, p1, p3}, Lcom/android/settings/privacy/m;-><init>(Lcom/android/settings/privacy/PrivacyRevocationSettings;Landroid/content/Context;Lcom/android/settings/privacy/b;)V

    const v2, 0x7f120d29

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/privacy/n;

    invoke-direct {v1, p0}, Lcom/android/settings/privacy/n;-><init>(Lcom/android/settings/privacy/PrivacyRevocationSettings;)V

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    const/4 v0, -0x1

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/widget/Button;->setClickable(Z)V

    new-instance v0, Lcom/android/settings/privacy/o;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/privacy/o;-><init>(Lcom/android/settings/privacy/PrivacyRevocationSettings;JJLandroid/widget/Button;)V

    invoke-virtual {v0}, Lcom/android/settings/privacy/o;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    sput-object v0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LE:Landroid/os/CountDownTimer;

    return-object v7

    :cond_0
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method static synthetic Fw(Lcom/android/settings/privacy/PrivacyRevocationSettings;)Lcom/android/settings/privacy/f;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    return-object v0
.end method

.method static synthetic Fx(Lcom/android/settings/privacy/PrivacyRevocationSettings;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->Fu(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public Ft(Lcom/android/settings/privacy/b;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    invoke-virtual {v0}, Lcom/android/settings/privacy/f;->notifyDataSetChanged()V

    :cond_0
    iget-boolean v0, p1, Lcom/android/settings/privacy/b;->LA:Z

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/android/settings/cloud/a/f;->aKn(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f120d27

    invoke-static {p0, v0}, Lcom/android/settings/cloud/a/f;->aKo(Landroid/content/Context;I)V

    return-void

    :cond_1
    iget-object v0, p1, Lcom/android/settings/privacy/b;->packageName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->Fs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p0, v0, p1}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->Fv(Landroid/content/Context;Ljava/lang/String;Lcom/android/settings/privacy/b;)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LG:Landroid/app/AlertDialog;

    :goto_0
    return-void

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.PRIVACY_AUTHORIZATION_DIALOG"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "key"

    iget-object v2, p1, Lcom/android/settings/privacy/b;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0xdc

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lmiui/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0xdc

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p3, :cond_1

    const-string/jumbo v0, "key"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "PrivacyRevocationSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "packageName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    invoke-virtual {v1, v0}, Lcom/android/settings/privacy/f;->FJ(Ljava/lang/String;)Lcom/android/settings/privacy/b;

    move-result-object v0

    if-eqz v0, :cond_1

    packed-switch p2, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    invoke-virtual {v0}, Lcom/android/settings/privacy/f;->notifyDataSetChanged()V

    :cond_1
    return-void

    :pswitch_0
    const-string/jumbo v1, "PrivacyRevocationSettings"

    const-string/jumbo v2, "RESULT_OK: "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/settings/privacy/b;->LA:Z

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "PrivacyRevocationSettings"

    const-string/jumbo v2, "RESULT_CANCELED: "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/settings/privacy/b;->LA:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0d016f

    invoke-virtual {p0, v0}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->setContentView(I)V

    const v0, 0x7f0d0171

    invoke-static {p0, v0, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0d0170

    invoke-static {p0, v0, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lcom/android/settings/privacy/f;

    invoke-direct {v0, p0}, Lcom/android/settings/privacy/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    const v0, 0x7f0a026f

    invoke-virtual {p0, v0}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LF:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LF:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v4, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LF:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LF:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LF:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 1

    new-instance v0, Lcom/android/settings/privacy/l;

    invoke-direct {v0, p0, p0}, Lcom/android/settings/privacy/l;-><init>(Lcom/android/settings/privacy/PrivacyRevocationSettings;Landroid/content/Context;)V

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    invoke-virtual {v0}, Lcom/android/settings/privacy/f;->FI()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-lt p3, v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gt p3, v1, :cond_1

    add-int/lit8 v1, p3, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/privacy/b;

    const-string/jumbo v1, "PrivacyRevocationSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "privacyItem = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/settings/privacy/b;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->Ft(Lcom/android/settings/privacy/b;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->onLoadFinished(Landroid/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Ljava/util/List;)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    invoke-virtual {v0, p2}, Lcom/android/settings/privacy/f;->FH(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LD:Lcom/android/settings/privacy/f;

    invoke-virtual {v0}, Lcom/android/settings/privacy/f;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lmiui/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LG:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LG:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LG:Landroid/app/AlertDialog;

    :cond_0
    sget-object v0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LE:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LE:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    sput-object v1, Lcom/android/settings/privacy/PrivacyRevocationSettings;->LE:Landroid/os/CountDownTimer;

    :cond_1
    return-void
.end method
