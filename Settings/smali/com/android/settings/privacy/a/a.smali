.class public Lcom/android/settings/privacy/a/a;
.super Ljava/lang/Object;
.source "NonceFactory.java"


# static fields
.field private static Lw:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/Random;

    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    sput-object v0, Lcom/android/settings/privacy/a/a;->Lw:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Fo()Ljava/lang/String;
    .locals 8

    new-instance v0, Lcom/android/settings/privacy/a/b;

    sget-object v1, Lcom/android/settings/privacy/a/a;->Lw:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    div-long/2addr v4, v6

    long-to-int v1, v4

    invoke-direct {v0, v2, v3, v1}, Lcom/android/settings/privacy/a/b;-><init>(JI)V

    invoke-virtual {v0}, Lcom/android/settings/privacy/a/b;->Fp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
