.class Lcom/android/settings/privacy/c;
.super Landroid/os/AsyncTask;
.source "PrivacyRevocationSettings.java"


# instance fields
.field private LH:Landroid/content/Context;

.field private LI:Lmiui/app/ProgressDialog;

.field private LJ:Lcom/android/settings/privacy/b;

.field final synthetic LK:Lcom/android/settings/privacy/PrivacyRevocationSettings;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/settings/privacy/PrivacyRevocationSettings;Landroid/content/Context;Lcom/android/settings/privacy/b;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/privacy/c;->LK:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacy/c;->LH:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/privacy/c;->LJ:Lcom/android/settings/privacy/b;

    iput-object p4, p0, Lcom/android/settings/privacy/c;->url:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected Fy(Ljava/lang/Boolean;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/privacy/c;->LI:Lmiui/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LI:Lmiui/app/ProgressDialog;

    invoke-virtual {v0}, Lmiui/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LI:Lmiui/app/ProgressDialog;

    invoke-virtual {v0}, Lmiui/app/ProgressDialog;->dismiss()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LH:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/privacy/c;->LJ:Lcom/android/settings/privacy/b;

    iget-object v1, v1, Lcom/android/settings/privacy/b;->packageName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$Privacy;->setEnabled(Landroid/content/Context;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LJ:Lcom/android/settings/privacy/b;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/settings/privacy/b;->LA:Z

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LH:Landroid/content/Context;

    const v1, 0x7f120d2d

    invoke-static {v0, v1}, Lcom/android/settings/cloud/a/f;->aKo(Landroid/content/Context;I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/privacy/c;->LK:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-static {v0}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->Fw(Lcom/android/settings/privacy/PrivacyRevocationSettings;)Lcom/android/settings/privacy/f;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LK:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-static {v0}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->Fw(Lcom/android/settings/privacy/PrivacyRevocationSettings;)Lcom/android/settings/privacy/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/privacy/f;->notifyDataSetChanged()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/privacy/c;->LH:Landroid/content/Context;

    const v1, 0x7f120d2c

    invoke-static {v0, v1}, Lcom/android/settings/cloud/a/f;->aKo(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LH:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/privacy/c;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/privacy/c;->LJ:Lcom/android/settings/privacy/b;

    iget-object v2, v2, Lcom/android/settings/privacy/b;->packageName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/privacy/k;->FT(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/privacy/c;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/settings/privacy/c;->Fy(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LK:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    const-string/jumbo v1, ""

    iget-object v2, p0, Lcom/android/settings/privacy/c;->LK:Lcom/android/settings/privacy/PrivacyRevocationSettings;

    invoke-virtual {v2}, Lcom/android/settings/privacy/PrivacyRevocationSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f120d30

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lmiui/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lmiui/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacy/c;->LI:Lmiui/app/ProgressDialog;

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LI:Lmiui/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/android/settings/privacy/c;->LI:Lmiui/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiui/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    return-void
.end method
