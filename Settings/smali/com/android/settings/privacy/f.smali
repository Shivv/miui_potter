.class public Lcom/android/settings/privacy/f;
.super Landroid/widget/BaseAdapter;
.source "PrivacyRevocationAdapter.java"


# instance fields
.field private LN:Ljava/util/List;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    iput-object p1, p0, Lcom/android/settings/privacy/f;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic FK(Lcom/android/settings/privacy/f;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/privacy/f;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public FH(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    return-void
.end method

.method public FI()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    return-object v0
.end method

.method public FJ(Ljava/lang/String;)Lcom/android/settings/privacy/b;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/privacy/b;

    iget-object v3, v0, Lcom/android/settings/privacy/b;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_0
    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    if-nez p2, :cond_1

    new-instance v1, Lcom/android/settings/privacy/g;

    invoke-direct {v1, p0}, Lcom/android/settings/privacy/g;-><init>(Lcom/android/settings/privacy/f;)V

    iget-object v0, p0, Lcom/android/settings/privacy/f;->mContext:Landroid/content/Context;

    const v2, 0x7f0d0172

    invoke-static {v0, v2, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f0a049a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/settings/privacy/g;->LQ:Landroid/widget/TextView;

    const v0, 0x7f0a01f0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/settings/privacy/g;->LO:Landroid/widget/ImageView;

    const v0, 0x7f0a0410

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/SlidingButton;

    iput-object v0, v1, Lcom/android/settings/privacy/g;->LP:Lmiui/widget/SlidingButton;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacy/f;->LN:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/privacy/b;

    iget-object v2, v1, Lcom/android/settings/privacy/g;->LQ:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/android/settings/privacy/b;->LB:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/android/settings/privacy/b;->Lz:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/android/settings/privacy/g;->LO:Landroid/widget/ImageView;

    iget-object v3, v0, Lcom/android/settings/privacy/b;->Lz:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    iget-object v2, v1, Lcom/android/settings/privacy/g;->LP:Lmiui/widget/SlidingButton;

    invoke-virtual {v2, v4}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v2, v1, Lcom/android/settings/privacy/g;->LP:Lmiui/widget/SlidingButton;

    iget-boolean v3, v0, Lcom/android/settings/privacy/b;->LA:Z

    invoke-virtual {v2, v3}, Lmiui/widget/SlidingButton;->setChecked(Z)V

    iget-object v1, v1, Lcom/android/settings/privacy/g;->LP:Lmiui/widget/SlidingButton;

    new-instance v2, Lcom/android/settings/privacy/p;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/privacy/p;-><init>(Lcom/android/settings/privacy/f;Lcom/android/settings/privacy/b;)V

    invoke-virtual {v1, v2}, Lmiui/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_0
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/privacy/g;

    move-object v1, v0

    goto :goto_0

    :cond_2
    iget-object v2, v1, Lcom/android/settings/privacy/g;->LO:Landroid/widget/ImageView;

    const v3, 0x7f0800c7

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
