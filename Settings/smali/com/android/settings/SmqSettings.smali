.class public Lcom/android/settings/SmqSettings;
.super Ljava/lang/Object;
.source "SmqSettings.java"


# instance fields
.field private bXl:Landroid/content/SharedPreferences;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/SmqSettings;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/settings/bm;

    iget-object v1, p0, Lcom/android/settings/SmqSettings;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/bm;-><init>(Landroid/content/Context;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/bm;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/android/settings/SmqSettings;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "smqpreferences"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SmqSettings;->bXl:Landroid/content/SharedPreferences;

    return-void
.end method


# virtual methods
.method public bRg()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/SmqSettings;->bXl:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "app_status"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
