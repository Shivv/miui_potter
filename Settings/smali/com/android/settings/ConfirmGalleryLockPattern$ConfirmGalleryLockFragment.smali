.class public Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;
.super Lcom/android/settings/ConfirmLockPattern$ConfirmLockPatternFragment;
.source "ConfirmGalleryLockPattern.java"


# static fields
.field private static bBb:J


# instance fields
.field private bAZ:Landroid/security/ChooseLockSettingsHelper;

.field private bBa:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ConfirmLockPattern$ConfirmLockPatternFragment;-><init>()V

    return-void
.end method

.method private boR()V
    .locals 3

    iget v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bBa:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bAZ:Landroid/security/ChooseLockSettingsHelper;

    invoke-static {}, Lmiui/os/UserHandle;->myUserId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/security/ChooseLockSettingsHelper;->setPrivateGalleryEnabledAsUser(ZI)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected boI(Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->boR()V

    invoke-super {p0, p1}, Lcom/android/settings/ConfirmLockPattern$ConfirmLockPatternFragment;->boI(Ljava/util/List;)V

    return-void
.end method

.method protected boJ(Ljava/util/List;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bEY:Landroid/security/MiuiLockPatternUtils;

    invoke-static {}, Lmiui/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Landroid/security/MiuiLockPatternUtils;->checkMiuiLockPatternAsUser(Ljava/util/List;I)Z

    move-result v0

    return v0
.end method

.method protected boK()I
    .locals 1

    const v0, 0x7f120d6a

    return v0
.end method

.method protected boL()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "private_gallery_lock_enabled"

    return-object v0
.end method

.method protected boM()Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "private_gallery_lock_pattern_visible_pattern"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected boN()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/settings/ChooseGalleryLockPattern;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method protected boO(J)J
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-wide v2, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bBb:J

    cmp-long v2, v2, v0

    if-ltz v2, :cond_0

    sget-wide v2, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bBb:J

    const-wide/16 v4, 0x7530

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    :cond_0
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bBb:J

    :cond_1
    sget-wide v0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bBb:J

    return-wide v0
.end method

.method protected boP()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected boQ(J)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/settings/ConfirmLockPattern$ConfirmLockPatternFragment;->boQ(J)V

    invoke-virtual {p0}, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bEZ:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bEZ:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method protected boS()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bEY:Landroid/security/MiuiLockPatternUtils;

    invoke-static {}, Lmiui/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/security/MiuiLockPatternUtils;->savedMiuiLockPatternExistsAsUser(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->boR()V

    invoke-virtual {p0}, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method protected boT(Landroid/content/Intent;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-super {p0, p1}, Lcom/android/settings/ConfirmLockPattern$ConfirmLockPatternFragment;->boT(Landroid/content/Intent;)V

    const-string/jumbo v0, "confirm_purpose"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bBa:I

    :cond_0
    return-void
.end method

.method protected boU(J)V
    .locals 1

    sput-wide p1, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bBb:J

    iget-object v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bEY:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v0}, Landroid/security/MiuiLockPatternUtils;->clearLockoutAttemptDeadline()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/ConfirmLockPattern$ConfirmLockPatternFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/security/ChooseLockSettingsHelper;

    invoke-virtual {p0}, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Landroid/security/ChooseLockSettingsHelper;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bAZ:Landroid/security/ChooseLockSettingsHelper;

    iget-object v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bAZ:Landroid/security/ChooseLockSettingsHelper;

    invoke-virtual {v0}, Landroid/security/ChooseLockSettingsHelper;->utils()Landroid/security/MiuiLockPatternUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ConfirmGalleryLockPattern$ConfirmGalleryLockFragment;->bEY:Landroid/security/MiuiLockPatternUtils;

    return-void
.end method
