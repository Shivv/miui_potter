.class final Lcom/android/settings/kP;
.super Ljava/lang/Object;
.source "HttpInvokeAppSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic cry:Lcom/android/settings/HttpInvokeAppSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/HttpInvokeAppSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/kP;->cry:Lcom/android/settings/HttpInvokeAppSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/kP;->cry:Lcom/android/settings/HttpInvokeAppSettings;

    invoke-virtual {v0}, Lcom/android/settings/HttpInvokeAppSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v1, v0}, Landroid/provider/MiuiSettings$Secure;->enableHttpInvokeApp(Landroid/content/ContentResolver;Z)V

    const-string/jumbo v1, "http_invoke_app"

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "turn_on"

    :goto_0
    invoke-static {v1, v0}, Lcom/android/settings/E;->blF(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string/jumbo v0, "turn_off"

    goto :goto_0
.end method
