.class public abstract Lcom/android/settings/core/b;
.super Lcom/android/settings/core/e;
.source "MiuiDynamicAvailabilityPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;


# instance fields
.field private avI:Lcom/android/settings/core/a;

.field private avJ:Landroid/preference/Preference;

.field private avK:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/core/b;->avI:Lcom/android/settings/core/a;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    return-void
.end method


# virtual methods
.method protected ake(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/b;->avI:Lcom/android/settings/core/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/core/b;->avI:Lcom/android/settings/core/a;

    invoke-virtual {p0}, Lcom/android/settings/core/b;->l()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/android/settings/core/a;->RK(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public akf(Lcom/android/settings/core/a;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/core/b;->avI:Lcom/android/settings/core/a;

    return-void
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/core/b;->avK:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/core/b;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/core/b;->avJ:Landroid/preference/Preference;

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->i(Landroid/preference/PreferenceScreen;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/core/b;->p()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/core/b;->avK:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/core/b;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/core/b;->cdK(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/core/b;->avJ:Landroid/preference/Preference;

    invoke-virtual {p0, v0}, Lcom/android/settings/core/b;->cz(Landroid/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/core/b;->avK:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/core/b;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/core/b;->avK:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/core/b;->avJ:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void
.end method
