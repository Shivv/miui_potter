.class public abstract Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;
.super Lcom/android/settings/core/lifecycle/MiuiObservablePreferenceFragment;
.source "MiuiInstrumentedPreferenceFragment.java"

# interfaces
.implements Lcom/android/settings/core/instrumentation/f;


# instance fields
.field protected final avL:I

.field private final avM:Lcom/android/settings/core/instrumentation/j;

.field protected mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/core/lifecycle/MiuiObservablePreferenceFragment;-><init>()V

    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->avL:I

    new-instance v0, Lcom/android/settings/core/instrumentation/j;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->getMetricsCategory()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/android/settings/core/instrumentation/j;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->avM:Lcom/android/settings/core/instrumentation/j;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->avM:Lcom/android/settings/core/instrumentation/j;

    invoke-virtual {v0, v1}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    new-instance v1, Lcom/android/settings/a/b;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/settings/a/b;-><init>(Landroid/app/Fragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/lifecycle/MiuiObservablePreferenceFragment;->onAttach(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    return-void
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->avM:Lcom/android/settings/core/instrumentation/j;

    invoke-virtual {p0}, Lcom/android/settings/core/MiuiInstrumentedPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/core/instrumentation/j;->akd(Landroid/app/Activity;)V

    invoke-super {p0}, Lcom/android/settings/core/lifecycle/MiuiObservablePreferenceFragment;->onResume()V

    return-void
.end method
