.class public Lcom/android/settings/core/f;
.super Ljava/lang/Object;
.source "TouchOverlayManager.java"


# instance fields
.field private final avP:Landroid/os/IBinder;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/android/settings/core/f;->avP:Landroid/os/IBinder;

    iput-object p1, p0, Lcom/android/settings/core/f;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public akj(Z)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/core/f;->mContext:Landroid/content/Context;

    const-class v1, Landroid/app/AppOpsManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    if-eqz v0, :cond_0

    xor-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Lcom/android/settings/core/f;->avP:Landroid/os/IBinder;

    const/16 v3, 0x18

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/AppOpsManager;->setUserRestriction(IZLandroid/os/IBinder;)V

    xor-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Lcom/android/settings/core/f;->avP:Landroid/os/IBinder;

    const/16 v3, 0x2d

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/AppOpsManager;->setUserRestriction(IZLandroid/os/IBinder;)V

    :cond_0
    return-void
.end method
