.class public Lcom/android/settings/core/lifecycle/c;
.super Ljava/lang/Object;
.source "MiuiLifecycle.java"


# instance fields
.field protected final avo:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;
    .locals 1

    invoke-static {}, Lcom/android/settings/utils/d;->ayq()V

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/l;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/l;

    invoke-interface {v0, p1}, Lcom/android/settings/core/lifecycle/a/l;->onAttach(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/m;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/m;

    invoke-interface {v0, p1}, Lcom/android/settings/core/lifecycle/a/m;->onCreate(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/g;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/g;

    invoke-interface {v0, p1, p2}, Lcom/android/settings/core/lifecycle/a/g;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/e;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/e;

    invoke-interface {v0}, Lcom/android/settings/core/lifecycle/a/e;->onDestroy()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/a;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/a;

    invoke-interface {v0, p1}, Lcom/android/settings/core/lifecycle/a/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/d;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/d;

    invoke-interface {v0}, Lcom/android/settings/core/lifecycle/a/d;->onPause()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/k;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/k;

    invoke-interface {v0, p1}, Lcom/android/settings/core/lifecycle/a/k;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/b;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/b;

    invoke-interface {v0}, Lcom/android/settings/core/lifecycle/a/b;->onResume()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/h;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/h;

    invoke-interface {v0, p1}, Lcom/android/settings/core/lifecycle/a/h;->onSaveInstanceState(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/c;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/c;

    invoke-interface {v0}, Lcom/android/settings/core/lifecycle/a/c;->onStart()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/j;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/j;

    invoke-interface {v0}, Lcom/android/settings/core/lifecycle/a/j;->onStop()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setPreferenceScreen(Landroid/preference/PreferenceScreen;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/c;->avo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/lifecycle/b;

    instance-of v2, v0, Lcom/android/settings/core/lifecycle/a/i;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/settings/core/lifecycle/a/i;

    invoke-interface {v0, p1}, Lcom/android/settings/core/lifecycle/a/i;->ajs(Landroid/preference/PreferenceScreen;)V

    goto :goto_0

    :cond_1
    return-void
.end method
