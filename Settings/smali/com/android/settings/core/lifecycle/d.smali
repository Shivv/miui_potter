.class public Lcom/android/settings/core/lifecycle/d;
.super Landroid/app/Activity;
.source "ObservableActivity.java"


# instance fields
.field private final mLifecycle:Lcom/android/settings/core/lifecycle/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/settings/core/lifecycle/a;

    invoke-direct {v0}, Lcom/android/settings/core/lifecycle/a;-><init>()V

    iput-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    return-void
.end method


# virtual methods
.method protected getLifecycle()Lcom/android/settings/core/lifecycle/a;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    invoke-virtual {v0, p0}, Lcom/android/settings/core/lifecycle/a;->onAttach(Landroid/content/Context;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/PersistableBundle;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    invoke-virtual {v0, p0}, Lcom/android/settings/core/lifecycle/a;->onAttach(Landroid/content/Context;)V

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;Landroid/os/PersistableBundle;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/settings/core/lifecycle/a;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/a;->onDestroy()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/lifecycle/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :cond_0
    return v0
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/a;->onPause()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    invoke-virtual {v0, p1}, Lcom/android/settings/core/lifecycle/a;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/a;->onResume()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/a;->onStart()V

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/core/lifecycle/d;->mLifecycle:Lcom/android/settings/core/lifecycle/a;

    invoke-virtual {v0}, Lcom/android/settings/core/lifecycle/a;->onStop()V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
