.class public Lcom/android/settings/core/instrumentation/i;
.super Ljava/lang/Object;
.source "SettingSuggestionsLogWriter.java"

# interfaces
.implements Lcom/android/settings/core/instrumentation/g;


# instance fields
.field private avC:Lcom/android/settings/dashboard/suggestions/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public varargs ajH(Landroid/content/Context;I[Landroid/util/Pair;)V
    .locals 0

    return-void
.end method

.method public ajI(Landroid/content/Context;II)V
    .locals 0

    return-void
.end method

.method public ajJ(Landroid/content/Context;IZ)V
    .locals 0

    return-void
.end method

.method public varargs ajK(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/i;->avC:Lcom/android/settings/dashboard/suggestions/e;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/dashboard/suggestions/e;

    invoke-direct {v0, p1}, Lcom/android/settings/dashboard/suggestions/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/core/instrumentation/i;->avC:Lcom/android/settings/dashboard/suggestions/e;

    :cond_0
    packed-switch p2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/core/instrumentation/i;->avC:Lcom/android/settings/dashboard/suggestions/e;

    const-string/jumbo v1, "shown"

    invoke-virtual {v0, p3, v1}, Lcom/android/settings/dashboard/suggestions/e;->BN(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/core/instrumentation/i;->avC:Lcom/android/settings/dashboard/suggestions/e;

    const-string/jumbo v1, "dismissed"

    invoke-virtual {v0, p3, v1}, Lcom/android/settings/dashboard/suggestions/e;->BN(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/core/instrumentation/i;->avC:Lcom/android/settings/dashboard/suggestions/e;

    const-string/jumbo v1, "clicked"

    invoke-virtual {v0, p3, v1}, Lcom/android/settings/dashboard/suggestions/e;->BN(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x180
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public ajL(Landroid/content/Context;II)V
    .locals 0

    return-void
.end method

.method public ajM(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public ajN(Landroid/content/Context;I)V
    .locals 0

    return-void
.end method

.method public ajO(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public ajP(Landroid/content/Context;II)V
    .locals 0

    return-void
.end method
