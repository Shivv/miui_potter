.class Lcom/android/settings/core/instrumentation/b;
.super Landroid/os/AsyncTask;
.source "SharedPreferencesLogger.java"


# instance fields
.field final synthetic avu:Lcom/android/settings/core/instrumentation/a;


# direct methods
.method private constructor <init>(Lcom/android/settings/core/instrumentation/a;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/core/instrumentation/b;->avu:Lcom/android/settings/core/instrumentation/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/core/instrumentation/a;Lcom/android/settings/core/instrumentation/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/instrumentation/b;-><init>(Lcom/android/settings/core/instrumentation/a;)V

    return-void
.end method


# virtual methods
.method protected varargs ajG([Ljava/lang/String;)Ljava/lang/Void;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x0

    aget-object v1, p1, v0

    aget-object v0, p1, v4

    iget-object v2, p0, Lcom/android/settings/core/instrumentation/b;->avu:Lcom/android/settings/core/instrumentation/a;

    invoke-static {v2}, Lcom/android/settings/core/instrumentation/a;->ajB(Lcom/android/settings/core/instrumentation/a;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    :try_start_0
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v3

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :cond_0
    :goto_0
    const/high16 v3, 0x400000

    :try_start_1
    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    iget-object v2, p0, Lcom/android/settings/core/instrumentation/b;->avu:Lcom/android/settings/core/instrumentation/a;

    invoke-static {v2, v1, v0}, Lcom/android/settings/core/instrumentation/a;->ajC(Lcom/android/settings/core/instrumentation/a;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    return-object v5

    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/android/settings/core/instrumentation/b;->avu:Lcom/android/settings/core/instrumentation/a;

    invoke-static {v2, v1, v0, v4}, Lcom/android/settings/core/instrumentation/a;->ajD(Lcom/android/settings/core/instrumentation/a;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/settings/core/instrumentation/b;->ajG([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
