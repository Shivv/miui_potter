.class public Lcom/android/settings/core/instrumentation/h;
.super Ljava/lang/Object;
.source "VisibilityLoggerMixin.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/l;


# instance fields
.field private avA:Lcom/android/settings/core/instrumentation/e;

.field private avB:I

.field private final avz:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/core/instrumentation/h;-><init>(ILcom/android/settings/core/instrumentation/e;)V

    return-void
.end method

.method public constructor <init>(ILcom/android/settings/core/instrumentation/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/core/instrumentation/h;->avB:I

    iput p1, p0, Lcom/android/settings/core/instrumentation/h;->avz:I

    iput-object p2, p0, Lcom/android/settings/core/instrumentation/h;->avA:Lcom/android/settings/core/instrumentation/e;

    return-void
.end method


# virtual methods
.method public akc(Landroid/app/Activity;)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/settings/core/instrumentation/h;->avB:I

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    const-string/jumbo v1, ":settings:source_metrics"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/core/instrumentation/h;->avB:I

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/core/instrumentation/h;->avA:Lcom/android/settings/core/instrumentation/e;

    return-void
.end method

.method public onPause()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/h;->avA:Lcom/android/settings/core/instrumentation/e;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/core/instrumentation/h;->avz:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/h;->avA:Lcom/android/settings/core/instrumentation/e;

    iget v1, p0, Lcom/android/settings/core/instrumentation/h;->avz:I

    invoke-virtual {v0, v2, v1}, Lcom/android/settings/core/instrumentation/e;->ajX(Landroid/content/Context;I)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/h;->avA:Lcom/android/settings/core/instrumentation/e;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/core/instrumentation/h;->avz:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/core/instrumentation/h;->avA:Lcom/android/settings/core/instrumentation/e;

    iget v1, p0, Lcom/android/settings/core/instrumentation/h;->avB:I

    iget v2, p0, Lcom/android/settings/core/instrumentation/h;->avz:I

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/settings/core/instrumentation/e;->ajW(Landroid/content/Context;II)V

    :cond_0
    return-void
.end method
