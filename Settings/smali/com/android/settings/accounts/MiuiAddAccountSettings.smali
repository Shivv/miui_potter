.class public Lcom/android/settings/accounts/MiuiAddAccountSettings;
.super Lcom/android/settings/MiuiSettings;
.source "MiuiAddAccountSettings.java"


# instance fields
.field private ayT:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettings;-><init>()V

    return-void
.end method

.method private anc()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->finish()V

    return-void
.end method

.method private ane()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "account_setup_wizard"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    return v0
.end method

.method private anf()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "com.android.settings"

    const-string/jumbo v2, "com.android.settings.accounts.AddAccountSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "account_setup_wizard"

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->ane()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic anh(Lcom/android/settings/accounts/MiuiAddAccountSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->anc()V

    return-void
.end method


# virtual methods
.method protected and(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    const v0, 0x7f0d0025

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->setContentView(I)V

    new-instance v0, Lcom/android/settings/i;

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->getHeaders()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->bgs()Lcom/android/settingslib/c/a;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/settings/i;-><init>(Landroid/app/Activity;Ljava/util/List;Lcom/android/settingslib/c/a;Z)V

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->ane()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a02ca

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lcom/android/settings/accounts/MiuiAddAccountSettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings$1;-><init>(Lcom/android/settings/accounts/MiuiAddAccountSettings;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public ang(Ljava/util/List;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettings;->ang(Ljava/util/List;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    long-to-int v0, v2

    const v2, 0x7f0a0010

    if-ne v0, v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->anf()V

    goto :goto_0

    :cond_0
    const v0, 0x7f0a039a

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->anc()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/accounts/MiuiAddAccountSettings;->ayT:Z

    if-eqz v0, :cond_0

    const v0, 0x7f15000f

    invoke-virtual {p0, v0, p1}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->loadHeadersFromResource(ILjava/util/List;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->ang(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->invalidateHeaders()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettings;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->anf()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/accounts/MiuiAddAccountSettings;->ayT:Z

    return-void
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .locals 2

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    long-to-int v0, v0

    const v1, 0x7f0a000b

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiAddAccountSettings;->anf()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettings;->onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V

    goto :goto_0
.end method
