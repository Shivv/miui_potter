.class Lcom/android/settings/accounts/MiuiManageAccountsSettings$LoadVIPNameTask;
.super Landroid/os/AsyncTask;
.source "MiuiManageAccountsSettings.java"


# instance fields
.field private axY:Landroid/content/Context;

.field final synthetic axZ:Lcom/android/settings/accounts/MiuiManageAccountsSettings;


# virtual methods
.method protected aml(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$LoadVIPNameTask;->axZ:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-static {v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->amh(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)Lmiui/preference/ValuePreference;

    move-result-object v0

    const v1, 0x7f12167f

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$LoadVIPNameTask;->axZ:Lcom/android/settings/accounts/MiuiManageAccountsSettings;

    invoke-static {v0}, Lcom/android/settings/accounts/MiuiManageAccountsSettings;->amh(Lcom/android/settings/accounts/MiuiManageAccountsSettings;)Lmiui/preference/ValuePreference;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$LoadVIPNameTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 9

    const/4 v8, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$LoadVIPNameTask;->axY:Landroid/content/Context;

    invoke-static {v0}, Lmiui/cloud/sync/MiCloudStatusInfo;->fromUserData(Landroid/content/Context;)Lmiui/cloud/sync/MiCloudStatusInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiui/cloud/sync/MiCloudStatusInfo;->getQuotaInfo()Lmiui/cloud/sync/MiCloudStatusInfo$QuotaInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lmiui/cloud/sync/MiCloudStatusInfo;->getQuotaInfo()Lmiui/cloud/sync/MiCloudStatusInfo$QuotaInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$LoadVIPNameTask;->axY:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    :try_start_0
    iget-object v4, p0, Lcom/android/settings/accounts/MiuiManageAccountsSettings$LoadVIPNameTask;->axY:Landroid/content/Context;

    invoke-static {v4}, Lcom/xiaomi/micloudsdk/request/Request;->init(Landroid/content/Context;)V

    invoke-virtual {v0}, Lmiui/cloud/sync/MiCloudStatusInfo;->getUserId()Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v4, v0}, Lcom/xiaomi/micloudsdk/CommonSdk;->getMiCloudMemberStatusInfo(Ljava/lang/String;Ljava/lang/String;)Lmiui/cloud/sync/VipInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v0, Lmiui/cloud/sync/VipInfo;->vipName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "%s/%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2}, Lmiui/cloud/sync/MiCloudStatusInfo$QuotaInfo;->getUsed()J

    move-result-wide v6

    invoke-static {v3, v8, v6, v7}, Lcom/android/settings/dc;->bYp(Ljava/util/Locale;IJ)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v2}, Lmiui/cloud/sync/MiCloudStatusInfo$QuotaInfo;->getTotal()J

    move-result-wide v6

    invoke-static {v3, v8, v6, v7}, Lcom/android/settings/dc;->bYp(Ljava/util/Locale;IJ)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1

    :cond_1
    :try_start_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v4, "MiuiManageAccountsSettings"

    const-string/jumbo v5, "Fail to get vip info."

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_1

    :cond_2
    const-string/jumbo v0, ""

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/settings/accounts/MiuiManageAccountsSettings$LoadVIPNameTask;->aml(Ljava/lang/String;)V

    return-void
.end method
