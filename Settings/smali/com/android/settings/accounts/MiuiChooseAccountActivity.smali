.class public Lcom/android/settings/accounts/MiuiChooseAccountActivity;
.super Lmiui/preference/PreferenceActivity;
.source "MiuiChooseAccountActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private aoQ()V
    .locals 3

    const v2, 0x1020002

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiChooseAccountActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/accounts/ChooseAccountFragment;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/accounts/ChooseAccountFragment;

    invoke-direct {v0}, Lcom/android/settings/accounts/ChooseAccountFragment;-><init>()V

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_0
    return-void
.end method

.method private aoR()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiChooseAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "account_setup_wizard"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    return v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiChooseAccountActivity;->aoR()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiChooseAccountActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiChooseAccountActivity;->aoR()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f130131

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/MiuiChooseAccountActivity;->setTheme(I)V

    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/accounts/MiuiChooseAccountActivity;->aoQ()V

    return-void
.end method

.method public onNavigateUp()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiChooseAccountActivity;->onBackPressed()V

    const/4 v0, 0x1

    return v0
.end method
