.class Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;
.super Lcom/android/settings/ab;
.source "MiuiAccountSettings.java"


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    const v0, 0x7f080067

    invoke-direct {p0, p1, v0}, Lcom/android/settings/ab;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/MiuiAccountSettings$SyncDrawable;->bmP()Landroid/graphics/drawable/Animatable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimatedRotateDrawable;

    const/16 v1, 0x38

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesCount(I)V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesDuration(I)V

    return-void
.end method


# virtual methods
.method protected amk(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Animatable;
    .locals 3

    check-cast p1, Landroid/graphics/drawable/StateListDrawable;

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x101009e

    const/4 v2, 0x0

    aput v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawableIndex([I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimatedRotateDrawable;

    return-object v0
.end method
