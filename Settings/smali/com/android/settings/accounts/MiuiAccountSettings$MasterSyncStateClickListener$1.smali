.class final Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;
.super Ljava/lang/Object;
.source "MiuiAccountSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic aBo:Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

.field final synthetic aBp:Lcom/android/settings/widget/TogglePreference;

.field final synthetic aBq:Z


# direct methods
.method constructor <init>(Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;Lcom/android/settings/widget/TogglePreference;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBo:Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

    iput-object p2, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBp:Lcom/android/settings/widget/TogglePreference;

    iput-boolean p3, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBq:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBp:Lcom/android/settings/widget/TogglePreference;

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBq:Z

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/TogglePreference;->setCheckedInternal(Z)V

    iget-boolean v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBq:Z

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBo:Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

    invoke-static {v1}, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aoF(Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;)Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v1

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->setMasterSyncAutomaticallyAsUser(ZI)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBo:Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aAn:Lcom/android/settings/accounts/MiuiAccountSettings;

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBq:Z

    iget-object v2, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBo:Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

    invoke-static {v2}, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aoF(Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;)Landroid/os/UserHandle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoB(Lcom/android/settings/accounts/MiuiAccountSettings;ZI)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBo:Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aAn:Lcom/android/settings/accounts/MiuiAccountSettings;

    invoke-virtual {v0}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoh()V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBo:Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener;->aAn:Lcom/android/settings/accounts/MiuiAccountSettings;

    iget-boolean v1, p0, Lcom/android/settings/accounts/MiuiAccountSettings$MasterSyncStateClickListener$1;->aBq:Z

    invoke-static {v0, v1}, Lcom/android/settings/accounts/MiuiAccountSettings;->aoz(Lcom/android/settings/accounts/MiuiAccountSettings;Z)V

    return-void
.end method
