.class Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;
.super Ljava/lang/Object;
.source "MiuiManageAccounts.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final azF:Ljava/lang/String;

.field private final azG:I

.field final synthetic azH:Lcom/android/settings/accounts/MiuiManageAccounts;


# direct methods
.method public constructor <init>(Lcom/android/settings/accounts/MiuiManageAccounts;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;->azH:Lcom/android/settings/accounts/MiuiManageAccounts;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;->azF:Ljava/lang/String;

    iput p3, p0, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;->azG:I

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;->azH:Lcom/android/settings/accounts/MiuiManageAccounts;

    invoke-virtual {v0}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;->azF:Ljava/lang/String;

    iget v3, p0, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;->azG:I

    const/4 v6, 0x0

    move-object v4, v2

    move-object v5, v2

    invoke-static/range {v0 .. v6}, Lcom/android/settings/dc;->bYv(Landroid/app/Activity;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    iget-object v0, p0, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;->azF:Ljava/lang/String;

    const-class v1, Lcom/android/settings/location/MiuiLocationSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.android.settings.accounts.LAUNCHING_LOCATION_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/accounts/MiuiManageAccounts$FragmentStarter;->azH:Lcom/android/settings/accounts/MiuiManageAccounts;

    invoke-virtual {v1}, Lcom/android/settings/accounts/MiuiManageAccounts;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "android.permission.WRITE_SECURE_SETTINGS"

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
