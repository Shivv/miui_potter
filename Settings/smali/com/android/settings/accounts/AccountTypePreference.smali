.class public Lcom/android/settings/accounts/AccountTypePreference;
.super Landroid/preference/Preference;
.source "AccountTypePreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final ayA:I

.field private final ayB:I

.field private final ayC:Ljava/lang/String;

.field private final ayy:Ljava/lang/String;

.field private final ayz:Landroid/os/Bundle;

.field private final mSummary:Ljava/lang/CharSequence;

.field private final mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/accounts/Account;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/lang/String;Landroid/os/Bundle;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iget-object v0, p3, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/accounts/AccountTypePreference;->mTitle:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayC:Ljava/lang/String;

    iput p5, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayB:I

    iput-object p6, p0, Lcom/android/settings/accounts/AccountTypePreference;->mSummary:Ljava/lang/CharSequence;

    iput-object p7, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayy:Ljava/lang/String;

    iput-object p8, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayz:Landroid/os/Bundle;

    iput p2, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayA:I

    const v0, 0x7f0d001e

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/AccountTypePreference;->setWidgetLayoutResource(I)V

    invoke-static {p3}, Lcom/android/settings/accounts/AccountTypePreference;->amQ(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/AccountTypePreference;->setKey(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountTypePreference;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/settings/accounts/AccountTypePreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p6}, Lcom/android/settings/accounts/AccountTypePreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p9}, Lcom/android/settings/accounts/AccountTypePreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, p0}, Lcom/android/settings/accounts/AccountTypePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method public static amQ(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/accounts/Account;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/AccountTypePreference;->mSummary:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/AccountTypePreference;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 10

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayy:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountTypePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iget-object v1, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayz:Landroid/os/Bundle;

    const-string/jumbo v2, "android.intent.extra.USER"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/UserHandle;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountTypePreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v5

    invoke-static {v2, v0, v5}, Lcom/android/settings/aq;->bqu(Landroid/content/Context;Landroid/os/UserManager;I)Z

    move-result v0

    if-eqz v0, :cond_0

    return v9

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountTypePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/aq;->bqN(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    return v9

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountTypePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayy:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayz:Landroid/os/Bundle;

    iget-object v5, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayC:Ljava/lang/String;

    iget v6, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayB:I

    iget v8, p0, Lcom/android/settings/accounts/AccountTypePreference;->ayA:I

    move-object v7, v3

    invoke-static/range {v0 .. v8}, Lcom/android/settings/aq;->bqO(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;ILjava/lang/String;ILjava/lang/CharSequence;I)V

    return v9

    :cond_2
    return v4
.end method
