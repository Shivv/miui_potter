.class public Lcom/android/settings/ConfirmSmsLockPattern;
.super Lcom/android/settings/ConfirmLockPattern;
.source "ConfirmSmsLockPattern.java"


# instance fields
.field private cbt:Lcom/android/settings/ConfirmSmsLockPattern$ConfirmSmsLockFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ConfirmLockPattern;-><init>()V

    return-void
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-super {p0}, Lcom/android/settings/ConfirmLockPattern;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string/jumbo v1, ":settings:show_fragment"

    const-class v2, Lcom/android/settings/ConfirmSmsLockPattern$ConfirmSmsLockFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 0

    check-cast p1, Lcom/android/settings/ConfirmSmsLockPattern$ConfirmSmsLockFragment;

    iput-object p1, p0, Lcom/android/settings/ConfirmSmsLockPattern;->cbt:Lcom/android/settings/ConfirmSmsLockPattern$ConfirmSmsLockFragment;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/ConfirmLockPattern;->onNewIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/settings/ConfirmSmsLockPattern;->cbt:Lcom/android/settings/ConfirmSmsLockPattern$ConfirmSmsLockFragment;

    invoke-virtual {v0, p1}, Lcom/android/settings/ConfirmSmsLockPattern$ConfirmSmsLockFragment;->boT(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/settings/ConfirmSmsLockPattern;->cbt:Lcom/android/settings/ConfirmSmsLockPattern$ConfirmSmsLockFragment;

    sget-object v1, Lcom/android/settings/ConfirmLockPattern$Stage;->bEW:Lcom/android/settings/ConfirmLockPattern$Stage;

    invoke-virtual {v0, v1}, Lcom/android/settings/ConfirmSmsLockPattern$ConfirmSmsLockFragment;->bvg(Lcom/android/settings/ConfirmLockPattern$Stage;)V

    return-void
.end method
