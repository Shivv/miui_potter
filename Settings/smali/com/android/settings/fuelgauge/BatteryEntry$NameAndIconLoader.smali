.class Lcom/android/settings/fuelgauge/BatteryEntry$NameAndIconLoader;
.super Ljava/lang/Thread;
.source "BatteryEntry.java"


# instance fields
.field private Of:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string/jumbo v0, "BatteryUsage Icon Loader"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/fuelgauge/BatteryEntry$NameAndIconLoader;->Of:Z

    return-void
.end method


# virtual methods
.method public If()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/fuelgauge/BatteryEntry$NameAndIconLoader;->Of:Z

    return-void
.end method

.method public run()V
    .locals 3

    :goto_0
    sget-object v1, Lcom/android/settings/fuelgauge/BatteryEntry;->NZ:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/settings/fuelgauge/BatteryEntry;->NZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/fuelgauge/BatteryEntry$NameAndIconLoader;->Of:Z

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/android/settings/fuelgauge/BatteryEntry;->Ob:Landroid/os/Handler;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/settings/fuelgauge/BatteryEntry;->Ob:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    sget-object v0, Lcom/android/settings/fuelgauge/BatteryEntry;->NZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :cond_2
    :try_start_1
    sget-object v0, Lcom/android/settings/fuelgauge/BatteryEntry;->NZ:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/fuelgauge/BatteryEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    invoke-virtual {v0}, Lcom/android/settings/fuelgauge/BatteryEntry;->Ib()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
