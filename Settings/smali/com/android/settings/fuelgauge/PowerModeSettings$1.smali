.class final Lcom/android/settings/fuelgauge/PowerModeSettings$1;
.super Ljava/lang/Object;
.source "PowerModeSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic Tc:Lcom/android/settings/fuelgauge/PowerModeSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/fuelgauge/PowerModeSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fuelgauge/PowerModeSettings$1;->Tc:Lcom/android/settings/fuelgauge/PowerModeSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerModeSettings$1;->Tc:Lcom/android/settings/fuelgauge/PowerModeSettings;

    invoke-static {v0}, Lcom/android/settings/fuelgauge/PowerModeSettings;->Ku(Lcom/android/settings/fuelgauge/PowerModeSettings;)[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-ge p2, v0, :cond_0

    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerModeSettings$1;->Tc:Lcom/android/settings/fuelgauge/PowerModeSettings;

    invoke-static {v0}, Lcom/android/settings/fuelgauge/PowerModeSettings;->Ku(Lcom/android/settings/fuelgauge/PowerModeSettings;)[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, p2

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "persist.sys.aries.power_profile"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/fuelgauge/PowerModeSettings$1;->Tc:Lcom/android/settings/fuelgauge/PowerModeSettings;

    invoke-virtual {v1}, Lcom/android/settings/fuelgauge/PowerModeSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "power_mode"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.POWER_MODE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/fuelgauge/PowerModeSettings$1;->Tc:Lcom/android/settings/fuelgauge/PowerModeSettings;

    invoke-virtual {v1, v0}, Lcom/android/settings/fuelgauge/PowerModeSettings;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerModeSettings$1;->Tc:Lcom/android/settings/fuelgauge/PowerModeSettings;

    invoke-virtual {v0}, Lcom/android/settings/fuelgauge/PowerModeSettings;->finish()V

    return-void
.end method
