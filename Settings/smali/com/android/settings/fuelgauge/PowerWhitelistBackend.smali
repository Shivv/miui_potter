.class public Lcom/android/settings/fuelgauge/PowerWhitelistBackend;
.super Ljava/lang/Object;
.source "PowerWhitelistBackend.java"


# static fields
.field private static Sr:Lcom/android/settings/fuelgauge/PowerWhitelistBackend;


# instance fields
.field private final So:Landroid/os/IDeviceIdleController;

.field private final Sp:Landroid/util/ArraySet;

.field private final Sq:Landroid/util/ArraySet;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sq:Landroid/util/ArraySet;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sp:Landroid/util/ArraySet;

    const-string/jumbo v0, "deviceidle"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/IDeviceIdleController$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IDeviceIdleController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->So:Landroid/os/IDeviceIdleController;

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->refreshList()V

    return-void
.end method

.method public static getInstance()Lcom/android/settings/fuelgauge/PowerWhitelistBackend;
    .locals 1

    sget-object v0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sr:Lcom/android/settings/fuelgauge/PowerWhitelistBackend;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;

    invoke-direct {v0}, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;-><init>()V

    sput-object v0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sr:Lcom/android/settings/fuelgauge/PowerWhitelistBackend;

    :cond_0
    sget-object v0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sr:Lcom/android/settings/fuelgauge/PowerWhitelistBackend;

    return-object v0
.end method


# virtual methods
.method public Kh(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sq:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public Ki(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sp:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public Kj(Ljava/lang/String;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->So:Landroid/os/IDeviceIdleController;

    invoke-interface {v0, p1}, Landroid/os/IDeviceIdleController;->addPowerSaveWhitelistApp(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sq:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "PowerWhitelistBackend"

    const-string/jumbo v2, "Unable to reach IDeviceIdleController"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public Kk(Ljava/lang/String;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->So:Landroid/os/IDeviceIdleController;

    invoke-interface {v0, p1}, Landroid/os/IDeviceIdleController;->removePowerSaveWhitelistApp(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sq:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "PowerWhitelistBackend"

    const-string/jumbo v2, "Unable to reach IDeviceIdleController"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method refreshList()V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sp:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->clear()V

    iget-object v1, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sq:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->clear()V

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->So:Landroid/os/IDeviceIdleController;

    invoke-interface {v1}, Landroid/os/IDeviceIdleController;->getFullPowerWhitelist()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    iget-object v5, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sq:Landroid/util/ArraySet;

    invoke-virtual {v5, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->So:Landroid/os/IDeviceIdleController;

    invoke-interface {v1}, Landroid/os/IDeviceIdleController;->getSystemPowerWhitelist()[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Sp:Landroid/util/ArraySet;

    invoke-virtual {v4, v3}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "PowerWhitelistBackend"

    const-string/jumbo v2, "Unable to reach IDeviceIdleController"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    return-void
.end method
