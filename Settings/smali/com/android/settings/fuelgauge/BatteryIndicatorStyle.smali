.class public Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;
.super Landroid/app/Activity;
.source "BatteryIndicatorStyle.java"


# instance fields
.field private Se:[Ljava/lang/CharSequence;

.field private Sf:[Ljava/lang/CharSequence;

.field private Sg:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private JR()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget v1, Lmiui/R$style;->Theme_Light_Dialog_Alert:I

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f120217

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Sg:Landroid/content/ContentResolver;

    const-string/jumbo v2, "battery_indicator_style"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->JS(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Se:[Ljava/lang/CharSequence;

    new-instance v3, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$1;

    invoke-direct {v3, p0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$1;-><init>(Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;)V

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$2;

    invoke-direct {v1, p0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$2;-><init>(Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;)V

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$3;

    invoke-direct {v1, p0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle$3;-><init>(Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic JT(Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;)[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Sf:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic JU(Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;)Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Sg:Landroid/content/ContentResolver;

    return-object v0
.end method


# virtual methods
.method public JS(Ljava/lang/String;)I
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Sf:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Sf:[Ljava/lang/CharSequence;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Sf:[Ljava/lang/CharSequence;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f03001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Se:[Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f03001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Sf:[Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->Sg:Landroid/content/ContentResolver;

    invoke-direct {p0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->JR()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/fuelgauge/BatteryIndicatorStyle;->setVisible(Z)V

    return-void
.end method
