.class Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;
.super Ljava/lang/Object;
.source "PowerUsageAdvanced.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public RF:D

.field public RG:I

.field public RH:D

.field public RI:J

.field public RJ:Ljava/util/List;

.field public RK:I

.field public summary:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;-><init>(ID)V

    return-void
.end method

.method public constructor <init>(ID)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RH:D

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RI:J

    invoke-direct {p0, p1}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->Js(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RG:I

    iput-wide p2, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RH:D

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RJ:Ljava/util/List;

    return-void
.end method

.method private Js(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f120cad

    return v0

    :pswitch_0
    const v0, 0x7f120cd7

    return v0

    :pswitch_1
    const v0, 0x7f120cb2

    return v0

    :pswitch_2
    const v0, 0x7f120ccd

    return v0

    :pswitch_3
    const v0, 0x7f120cb0

    return v0

    :pswitch_4
    const v0, 0x7f120cd6

    return v0

    :pswitch_5
    const v0, 0x7f120cbf

    return v0

    :pswitch_6
    const v0, 0x7f120cce

    return v0

    :pswitch_7
    const v0, 0x7f120cc5

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public Jr(Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;)I
    .locals 4

    iget-wide v0, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RH:D

    iget-wide v2, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RH:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    iget v1, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;

    invoke-virtual {p0, p1}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->Jr(Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;)I

    move-result v0

    return v0
.end method
