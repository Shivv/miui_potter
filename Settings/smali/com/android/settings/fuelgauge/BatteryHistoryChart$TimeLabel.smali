.class Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;
.super Ljava/lang/Object;
.source "BatteryHistoryChart.java"


# instance fields
.field final Rd:Ljava/lang/String;

.field final Re:I

.field final Rf:I


# direct methods
.method constructor <init>(Landroid/text/TextPaint;ILjava/util/Calendar;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rf:I

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    if-eqz p4, :cond_0

    const-string/jumbo v0, "km"

    :goto_0
    invoke-static {v1, v0}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rd:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Rd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/settings/fuelgauge/BatteryHistoryChart$TimeLabel;->Re:I

    return-void

    :cond_0
    const-string/jumbo v0, "ha"

    goto :goto_0
.end method
