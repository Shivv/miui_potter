.class public Lcom/android/settings/fuelgauge/PowerUsageAdvanced;
.super Lcom/android/settings/fuelgauge/PowerUsageBase;
.source "PowerUsageAdvanced.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private RB:Ljava/util/Map;

.field RC:Landroid/os/Handler;

.field private RD:Lcom/android/settings/fuelgauge/BatteryHistoryPreference;

.field private RE:Landroid/preference/PreferenceGroup;

.field private mBatteryUtils:Lcom/android/settings/fuelgauge/BatteryUtils;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mPowerUsageFeatureProvider:Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

.field final mUsageTypes:[I

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$2;

    invoke-direct {v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$2;-><init>()V

    sput-object v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/fuelgauge/PowerUsageBase;-><init>()V

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mUsageTypes:[I

    new-instance v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;

    invoke-direct {v0, p0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;-><init>(Lcom/android/settings/fuelgauge/PowerUsageAdvanced;)V

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->RC:Landroid/os/Handler;

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x0
        0x7
        0x8
    .end array-data
.end method

.method static synthetic Jq(Lcom/android/settings/fuelgauge/PowerUsageAdvanced;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->RB:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "AdvancedBatteryUsage"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f1500a3

    return v0
.end method

.method calculateHiddenPower(Ljava/util/List;)D
    .locals 4

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;

    iget v2, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    const/4 v3, 0x7

    if-ne v2, v3, :cond_0

    iget-wide v0, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RH:D

    return-wide v0

    :cond_1
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method extractUsageType(Lcom/android/internal/os/BatterySipper;)I
    .locals 2

    iget-object v0, p1, Lcom/android/internal/os/BatterySipper;->drainType:Lcom/android/internal/os/BatterySipper$DrainType;

    invoke-virtual {p1}, Lcom/android/internal/os/BatterySipper;->getUid()I

    sget-object v1, Lcom/android/internal/os/BatterySipper$DrainType;->WIFI:Lcom/android/internal/os/BatterySipper$DrainType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    sget-object v1, Lcom/android/internal/os/BatterySipper$DrainType;->BLUETOOTH:Lcom/android/internal/os/BatterySipper$DrainType;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x4

    return v0

    :cond_1
    sget-object v1, Lcom/android/internal/os/BatterySipper$DrainType;->IDLE:Lcom/android/internal/os/BatterySipper$DrainType;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x6

    return v0

    :cond_2
    sget-object v1, Lcom/android/internal/os/BatterySipper$DrainType;->USER:Lcom/android/internal/os/BatterySipper$DrainType;

    if-ne v0, v1, :cond_3

    const/4 v0, 0x5

    return v0

    :cond_3
    sget-object v1, Lcom/android/internal/os/BatterySipper$DrainType;->CELL:Lcom/android/internal/os/BatterySipper$DrainType;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x2

    return v0

    :cond_4
    sget-object v1, Lcom/android/internal/os/BatterySipper$DrainType;->UNACCOUNTED:Lcom/android/internal/os/BatterySipper$DrainType;

    if-ne v0, v1, :cond_5

    const/4 v0, 0x7

    return v0

    :cond_5
    sget-object v1, Lcom/android/internal/os/BatterySipper$DrainType;->OVERCOUNTED:Lcom/android/internal/os/BatterySipper$DrainType;

    if-ne v0, v1, :cond_6

    const/16 v0, 0x8

    return v0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mPowerUsageFeatureProvider:Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    invoke-interface {v0, p1}, Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;->Kz(Lcom/android/internal/os/BatterySipper;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mPowerUsageFeatureProvider:Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    invoke-interface {v0, p1}, Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;->KA(Lcom/android/internal/os/BatterySipper;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_7
    const/4 v0, 0x3

    return v0

    :cond_8
    const/4 v0, 0x0

    return v0
.end method

.method findBatterySipperWithMaxBatteryUsage(Ljava/util/List;)Lcom/android/internal/os/BatterySipper;
    .locals 8

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/os/BatterySipper;

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    move-object v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/os/BatterySipper;

    iget-wide v4, v0, Lcom/android/internal/os/BatterySipper;->totalPowerMah:D

    iget-wide v6, v1, Lcom/android/internal/os/BatterySipper;->totalPowerMah:D

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x33

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/fuelgauge/PowerUsageBase;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "battery_graph"

    invoke-virtual {p0, v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/fuelgauge/BatteryHistoryPreference;

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->RD:Lcom/android/settings/fuelgauge/BatteryHistoryPreference;

    const-string/jumbo v0, "battery_usage_list"

    invoke-virtual {p0, v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->RE:Landroid/preference/PreferenceGroup;

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/overlay/a;->aIs(Landroid/content/Context;)Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mPowerUsageFeatureProvider:Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string/jumbo v0, "user"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mUserManager:Landroid/os/UserManager;

    invoke-static {v1}, Lcom/android/settings/fuelgauge/BatteryUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/fuelgauge/BatteryUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mBatteryUtils:Lcom/android/settings/fuelgauge/BatteryUtils;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/fuelgauge/PowerUsageBase;->onDestroy()V

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/settings/fuelgauge/BatteryEntry;->HY()V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-static {}, Lcom/android/settings/fuelgauge/BatteryEntry;->Id()V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->RC:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-super {p0}, Lcom/android/settings/fuelgauge/PowerUsageBase;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/fuelgauge/PowerUsageBase;->onResume()V

    return-void
.end method

.method parsePowerUsageData(Lcom/android/internal/os/BatteryStatsHelper;)Ljava/util/List;
    .locals 12

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/android/internal/os/BatteryStatsHelper;->getUsageList()Ljava/util/List;

    move-result-object v1

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iget-object v3, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mUsageTypes:[I

    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {v6, v7}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;-><init>(I)V

    invoke-interface {v9, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/os/BatterySipper;

    iget-object v1, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0}, Lcom/android/internal/os/BatterySipper;->getUid()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/os/BatterySipper;->mPackages:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->extractUsageType(Lcom/android/internal/os/BatterySipper;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;

    iget-wide v4, v1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RH:D

    iget-wide v6, v0, Lcom/android/internal/os/BatterySipper;->totalPowerMah:D

    add-double/2addr v4, v6

    iput-wide v4, v1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RH:D

    iget-object v4, v0, Lcom/android/internal/os/BatterySipper;->drainType:Lcom/android/internal/os/BatterySipper$DrainType;

    sget-object v5, Lcom/android/internal/os/BatterySipper$DrainType;->APP:Lcom/android/internal/os/BatterySipper$DrainType;

    if-ne v4, v5, :cond_2

    iget-wide v4, v0, Lcom/android/internal/os/BatterySipper;->usageTimeMs:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mBatteryUtils:Lcom/android/settings/fuelgauge/BatteryUtils;

    iget-object v5, v0, Lcom/android/internal/os/BatterySipper;->uidObj:Landroid/os/BatteryStats$Uid;

    invoke-virtual {v4, v2, v5, v2}, Lcom/android/settings/fuelgauge/BatteryUtils;->Jt(ILandroid/os/BatteryStats$Uid;I)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/internal/os/BatterySipper;->usageTimeMs:J

    :cond_2
    iget-wide v4, v1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RI:J

    iget-wide v6, v0, Lcom/android/internal/os/BatterySipper;->usageTimeMs:J

    add-long/2addr v4, v6

    iput-wide v4, v1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RI:J

    invoke-virtual {p0, v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->shouldShowBatterySipper(Lcom/android/internal/os/BatterySipper;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v1, v1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RJ:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v10, Ljava/util/ArrayList;

    invoke-interface {v9}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1}, Lcom/android/internal/os/BatteryStatsHelper;->getStats()Landroid/os/BatteryStats;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/BatteryStats;->getDischargeAmount(I)I

    move-result v8

    invoke-virtual {p1}, Lcom/android/internal/os/BatteryStatsHelper;->getTotalPower()D

    move-result-wide v4

    invoke-virtual {p0, v10}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->calculateHiddenPower(Ljava/util/List;)D

    move-result-wide v6

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;

    iget-object v1, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mBatteryUtils:Lcom/android/settings/fuelgauge/BatteryUtils;

    iget-wide v2, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RH:D

    invoke-virtual/range {v1 .. v8}, Lcom/android/settings/fuelgauge/BatteryUtils;->Jx(DDDI)D

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RF:D

    invoke-virtual {p0, v0, v4, v5, v8}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->updateUsageDataSummary(Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;DI)V

    goto :goto_2

    :cond_4
    invoke-static {v10}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iput-object v9, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->RB:Ljava/util/Map;

    return-object v10
.end method

.method refreshPowerUsageDataList(Lcom/android/internal/os/BatteryStatsHelper;Landroid/preference/PreferenceGroup;)V
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->parsePowerUsageData(Lcom/android/internal/os/BatteryStatsHelper;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {p2}, Landroid/preference/PreferenceGroup;->removeAll()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;

    invoke-virtual {p0, v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->shouldHideCategory(Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v5, Lcom/android/settings/fuelgauge/PowerGaugePreference;

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->bWz()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/settings/fuelgauge/PowerGaugePreference;-><init>(Landroid/content/Context;)V

    iget v6, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/settings/fuelgauge/PowerGaugePreference;->setKey(Ljava/lang/String;)V

    iget v6, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RG:I

    invoke-virtual {v5, v6}, Lcom/android/settings/fuelgauge/PowerGaugePreference;->setTitle(I)V

    iget-object v6, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->summary:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Lcom/android/settings/fuelgauge/PowerGaugePreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-wide v6, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RF:D

    invoke-virtual {v5, v6, v7}, Lcom/android/settings/fuelgauge/PowerGaugePreference;->JZ(D)V

    invoke-virtual {v5, v2}, Lcom/android/settings/fuelgauge/PowerGaugePreference;->setSelectable(Z)V

    invoke-virtual {p2, v5}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected refreshUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->RD:Lcom/android/settings/fuelgauge/BatteryHistoryPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->Kn(Lcom/android/settings/fuelgauge/BatteryHistoryPreference;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->SE:Lcom/android/internal/os/BatteryStatsHelper;

    iget-object v1, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->RE:Landroid/preference/PreferenceGroup;

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->refreshPowerUsageDataList(Lcom/android/internal/os/BatteryStatsHelper;Landroid/preference/PreferenceGroup;)V

    invoke-static {}, Lcom/android/settings/fuelgauge/BatteryEntry;->Ic()V

    return-void
.end method

.method setBatteryUtils(Lcom/android/settings/fuelgauge/BatteryUtils;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mBatteryUtils:Lcom/android/settings/fuelgauge/BatteryUtils;

    return-void
.end method

.method setPackageManager(Landroid/content/pm/PackageManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mPackageManager:Landroid/content/pm/PackageManager;

    return-void
.end method

.method setPowerUsageFeatureProvider(Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mPowerUsageFeatureProvider:Lcom/android/settings/fuelgauge/PowerUsageFeatureProvider;

    return-void
.end method

.method setUserManager(Landroid/os/UserManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mUserManager:Landroid/os/UserManager;

    return-void
.end method

.method shouldHideCategory(Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    const/4 v3, 0x7

    if-eq v2, v3, :cond_0

    iget v2, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v2}, Landroid/os/UserManager;->getUserCount()I

    move-result v2

    if-eq v2, v0, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method shouldHideSummary(Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;)Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eq v1, v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method shouldShowBatterySipper(Lcom/android/internal/os/BatterySipper;)Z
    .locals 2

    iget-object v0, p1, Lcom/android/internal/os/BatterySipper;->drainType:Lcom/android/internal/os/BatterySipper$DrainType;

    sget-object v1, Lcom/android/internal/os/BatterySipper$DrainType;->SCREEN:Lcom/android/internal/os/BatterySipper$DrainType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateUsageDataSummary(Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;DI)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0, p1}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->shouldHideSummary(Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v6, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RI:J

    long-to-double v2, v2

    invoke-static {v0, v2, v3, v7}, Lcom/android/settings/aq;->bqR(Landroid/content/Context;DZ)Ljava/lang/CharSequence;

    move-result-object v0

    iget v1, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RK:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    :goto_0
    iput-object v0, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->summary:Ljava/lang/CharSequence;

    :goto_1
    return-void

    :cond_1
    const v1, 0x7f12026a

    invoke-virtual {p0, v1}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/CharSequence;

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->RJ:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->findBatterySipperWithMaxBatteryUsage(Ljava/util/List;)Lcom/android/internal/os/BatterySipper;

    move-result-object v0

    new-instance v1, Lcom/android/settings/fuelgauge/BatteryEntry;

    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->RC:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->mUserManager:Landroid/os/UserManager;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/android/settings/fuelgauge/BatteryEntry;-><init>(Landroid/content/Context;Landroid/os/Handler;Landroid/os/UserManager;Lcom/android/internal/os/BatterySipper;)V

    iget-wide v2, v0, Lcom/android/internal/os/BatterySipper;->totalPowerMah:D

    div-double/2addr v2, p2

    int-to-double v4, p4

    mul-double/2addr v2, v4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v6}, Lcom/android/settings/aq;->cqE(DZ)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    iget-object v1, v1, Lcom/android/settings/fuelgauge/BatteryEntry;->name:Ljava/lang/String;

    aput-object v1, v0, v6

    const v1, 0x7f120269

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->summary:Ljava/lang/CharSequence;

    goto :goto_1
.end method
