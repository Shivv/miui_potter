.class public Lcom/android/settings/fuelgauge/BatterySaverController;
.super Lcom/android/settings/core/c;
.source "BatterySaverController.java"

# interfaces
.implements Landroid/support/v7/preference/f;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/c;
.implements Lcom/android/settings/core/lifecycle/a/j;


# instance fields
.field private Og:Lcom/android/settings/widget/MasterSwitchPreference;

.field private final Oh:Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;

.field private final Oi:Landroid/database/ContentObserver;

.field private final Oj:Landroid/os/PowerManager;


# direct methods
.method static synthetic Ig(Lcom/android/settings/fuelgauge/BatterySaverController;)Lcom/android/settings/widget/MasterSwitchPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Og:Lcom/android/settings/widget/MasterSwitchPreference;

    return-object v0
.end method

.method static synthetic Ih(Lcom/android/settings/fuelgauge/BatterySaverController;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic Ii(Lcom/android/settings/fuelgauge/BatterySaverController;)Landroid/os/PowerManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Oj:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic Ij(Lcom/android/settings/fuelgauge/BatterySaverController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/fuelgauge/BatterySaverController;->updateSummary()V

    return-void
.end method

.method private updateSummary()V
    .locals 8

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Oj:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f12024b

    :goto_0
    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "low_power_trigger_level"

    invoke-static {v1, v2, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-lez v2, :cond_1

    const v1, 0x7f120249

    :goto_1
    iget-object v3, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->mContext:Landroid/content/Context;

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->mContext:Landroid/content/Context;

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Lcom/android/settings/aq;->cqH(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    invoke-virtual {v5, v1, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Og:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/MasterSwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const v0, 0x7f12024a

    goto :goto_0

    :cond_1
    const v1, 0x7f120248

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/c;->a(Landroid/support/v7/preference/PreferenceScreen;)V

    const-string/jumbo v0, "battery_saver_summary"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/MasterSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Og:Lcom/android/settings/widget/MasterSwitchPreference;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "battery_saver_summary"

    return-object v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public eH(Landroid/support/v7/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Oj:Landroid/os/PowerManager;

    invoke-virtual {v1}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Oj:Landroid/os/PowerManager;

    invoke-virtual {v1, v0}, Landroid/os/PowerManager;->setPowerSaveMode(Z)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/fuelgauge/BatterySaverController;->refreshConditionManager()V

    invoke-direct {p0}, Lcom/android/settings/fuelgauge/BatterySaverController;->updateSummary()V

    const/4 v0, 0x1

    return v0
.end method

.method public onStart()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "low_power_trigger_level"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Oi:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Oh:Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;

    invoke-virtual {v0, v3}, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ik(Z)V

    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Oi:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->Oh:Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/fuelgauge/BatterySaverController$BatteryStateChangeReceiver;->Ik(Z)V

    return-void
.end method

.method refreshConditionManager()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatterySaverController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/dashboard/conditional/h;->AQ(Landroid/content/Context;)Lcom/android/settings/dashboard/conditional/h;

    move-result-object v0

    const-class v1, Lcom/android/settings/dashboard/conditional/k;

    invoke-virtual {v0, v1}, Lcom/android/settings/dashboard/conditional/h;->AR(Ljava/lang/Class;)Lcom/android/settings/dashboard/conditional/e;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dashboard/conditional/k;

    invoke-virtual {v0}, Lcom/android/settings/dashboard/conditional/k;->AE()V

    return-void
.end method
