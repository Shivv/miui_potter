.class final Lcom/android/settings/fuelgauge/BatteryUtils$1;
.super Ljava/lang/Object;
.source "BatteryUtils.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic SU:Lcom/android/settings/fuelgauge/BatteryUtils;


# direct methods
.method constructor <init>(Lcom/android/settings/fuelgauge/BatteryUtils;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fuelgauge/BatteryUtils$1;->SU:Lcom/android/settings/fuelgauge/BatteryUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public KF(Lcom/android/internal/os/BatterySipper;Lcom/android/internal/os/BatterySipper;)I
    .locals 4

    iget-wide v0, p2, Lcom/android/internal/os/BatterySipper;->totalPowerMah:D

    iget-wide v2, p1, Lcom/android/internal/os/BatterySipper;->totalPowerMah:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/internal/os/BatterySipper;

    check-cast p2, Lcom/android/internal/os/BatterySipper;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/fuelgauge/BatteryUtils$1;->KF(Lcom/android/internal/os/BatterySipper;Lcom/android/internal/os/BatterySipper;)I

    move-result v0

    return v0
.end method
