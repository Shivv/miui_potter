.class final Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;
.super Landroid/os/Handler;
.source "PowerUsageAdvanced.java"


# instance fields
.field final synthetic ST:Lcom/android/settings/fuelgauge/PowerUsageAdvanced;


# direct methods
.method constructor <init>(Lcom/android/settings/fuelgauge/PowerUsageAdvanced;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;->ST:Lcom/android/settings/fuelgauge/PowerUsageAdvanced;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;->ST:Lcom/android/settings/fuelgauge/PowerUsageAdvanced;

    iget-object v0, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->SE:Lcom/android/internal/os/BatteryStatsHelper;

    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsHelper;->getStats()Landroid/os/BatteryStats;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/BatteryStats;->getDischargeAmount(I)I

    move-result v1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;->ST:Lcom/android/settings/fuelgauge/PowerUsageAdvanced;

    iget-object v0, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->SE:Lcom/android/internal/os/BatteryStatsHelper;

    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsHelper;->getTotalPower()D

    move-result-wide v2

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/settings/fuelgauge/BatteryEntry;

    iget-object v4, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;->ST:Lcom/android/settings/fuelgauge/PowerUsageAdvanced;

    iget-object v0, v0, Lcom/android/settings/fuelgauge/BatteryEntry;->Od:Lcom/android/internal/os/BatterySipper;

    invoke-virtual {v4, v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->extractUsageType(Lcom/android/internal/os/BatterySipper;)I

    move-result v4

    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;->ST:Lcom/android/settings/fuelgauge/PowerUsageAdvanced;

    invoke-static {v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->Jq(Lcom/android/settings/fuelgauge/PowerUsageAdvanced;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;

    iget-object v5, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;->ST:Lcom/android/settings/fuelgauge/PowerUsageAdvanced;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;->ST:Lcom/android/settings/fuelgauge/PowerUsageAdvanced;

    invoke-virtual {v5, v0, v2, v3, v1}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->updateUsageDataSummary(Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;DI)V

    iget-object v0, v0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$PowerUsageData;->summary:Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/fuelgauge/PowerUsageAdvanced$1;->ST:Lcom/android/settings/fuelgauge/PowerUsageAdvanced;

    invoke-virtual {v0}, Lcom/android/settings/fuelgauge/PowerUsageAdvanced;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->reportFullyDrawn()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
