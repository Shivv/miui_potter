.class final Lcom/android/settings/iP;
.super Ljava/lang/Object;
.source "PreviewSeekBarPreferenceFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic coS:Lcom/android/settings/PreviewSeekBarPreferenceFragment;

.field final synthetic coT:Lcom/android/settings/widget/LabeledSeekBar;


# direct methods
.method constructor <init>(Lcom/android/settings/PreviewSeekBarPreferenceFragment;Lcom/android/settings/widget/LabeledSeekBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/iP;->coS:Lcom/android/settings/PreviewSeekBarPreferenceFragment;

    iput-object p2, p0, Lcom/android/settings/iP;->coT:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/iP;->coT:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/LabeledSeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/iP;->coT:Lcom/android/settings/widget/LabeledSeekBar;

    invoke-virtual {v1}, Lcom/android/settings/widget/LabeledSeekBar;->getMax()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/iP;->coT:Lcom/android/settings/widget/LabeledSeekBar;

    add-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/widget/LabeledSeekBar;->setProgress(IZ)V

    :cond_0
    return-void
.end method
