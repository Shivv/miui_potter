.class final Lcom/android/settings/jU;
.super Landroid/content/BroadcastReceiver;
.source "CryptKeeperSettings.java"


# instance fields
.field final synthetic cqq:Lcom/android/settings/CryptKeeperSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/CryptKeeperSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/jU;->cqq:Lcom/android/settings/CryptKeeperSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/16 v3, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "level"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v4, "plugged"

    invoke-virtual {p2, v4, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string/jumbo v4, "invalid_charger"

    invoke-virtual {p2, v4, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const/16 v4, 0x50

    if-lt v2, v4, :cond_1

    move v4, v0

    :goto_0
    and-int/lit8 v2, v5, 0x7

    if-eqz v2, :cond_3

    if-nez v6, :cond_2

    :goto_1
    iget-object v2, p0, Lcom/android/settings/jU;->cqq:Lcom/android/settings/CryptKeeperSettings;

    invoke-static {v2}, Lcom/android/settings/CryptKeeperSettings;->bPV(Lcom/android/settings/CryptKeeperSettings;)Landroid/widget/Button;

    move-result-object v5

    if-eqz v4, :cond_4

    move v2, v0

    :goto_2
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/jU;->cqq:Lcom/android/settings/CryptKeeperSettings;

    invoke-static {v2}, Lcom/android/settings/CryptKeeperSettings;->bPW(Lcom/android/settings/CryptKeeperSettings;)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_5

    move v0, v3

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/jU;->cqq:Lcom/android/settings/CryptKeeperSettings;

    invoke-static {v0}, Lcom/android/settings/CryptKeeperSettings;->bPU(Lcom/android/settings/CryptKeeperSettings;)Landroid/view/View;

    move-result-object v0

    if-eqz v4, :cond_6

    :goto_4
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v4, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    move v3, v1

    goto :goto_4
.end method
