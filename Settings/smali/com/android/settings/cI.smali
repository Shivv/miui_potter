.class Lcom/android/settings/cI;
.super Landroid/os/AsyncTask;
.source "SettingsApplication.java"


# instance fields
.field final synthetic cdh:Lcom/android/settings/cH;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/settings/cH;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cI;->cdh:Lcom/android/settings/cH;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/android/settings/cI;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected bUS(Ljava/lang/Boolean;)V
    .locals 0

    return-void
.end method

.method public varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/cI;->cdh:Lcom/android/settings/cH;

    iget-object v1, p0, Lcom/android/settings/cI;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/settings/cH;->bUR(Lcom/android/settings/cH;Landroid/content/Context;)V

    const-string/jumbo v0, "com.android.settings:remote"

    iget-object v1, p0, Lcom/android/settings/cI;->cdh:Lcom/android/settings/cH;

    iget-object v2, p0, Lcom/android/settings/cI;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/android/settings/cH;->bUQ(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0xabe0

    invoke-static {v0}, Lcom/android/settings/ar;->brO(I)V

    const v0, 0xabe8

    invoke-static {v0}, Lcom/android/settings/ar;->brO(I)V

    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_1

    const v0, 0xabe5

    invoke-static {v0}, Lcom/android/settings/ar;->brO(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/cI;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/deviceinfo/SDCardStateUploader;->aum(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/android/settings/deviceinfo/SDCardStateUploader;

    iget-object v1, p0, Lcom/android/settings/cI;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/deviceinfo/SDCardStateUploader;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/deviceinfo/SDCardStateUploader;->auo()V

    :cond_2
    invoke-static {}, Lcom/android/settings/device/h;->aDY()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0xabe4

    invoke-static {v0}, Lcom/android/settings/ar;->brO(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/cI;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/ar;->brQ(Landroid/content/Context;)Z

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/cI;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/settings/cI;->bUS(Ljava/lang/Boolean;)V

    return-void
.end method
