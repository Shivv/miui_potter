.class public Lcom/android/settings/cloud/h;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static aWA:Ljava/lang/String;

.field public static aWB:Ljava/lang/String;

.field public static aWC:Ljava/lang/String;

.field public static aWz:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static aLk()V
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, "/data/system/cloudsettings_staging"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/settings/cloud/h;->aLn()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/settings/cloud/h;->aLm()V

    goto :goto_0
.end method

.method private static aLl()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/settings/cloud/h;->aWC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "common/whiteList/allList"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/cloud/h;->aWz:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/settings/cloud/h;->aWB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "data/cloud"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/cloud/h;->aWA:Ljava/lang/String;

    return-void
.end method

.method public static aLm()V
    .locals 1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "https://api.sec.intl.miui.com/"

    sput-object v0, Lcom/android/settings/cloud/h;->aWC:Ljava/lang/String;

    const-string/jumbo v0, "https://data.sec.intl.miui.com/"

    sput-object v0, Lcom/android/settings/cloud/h;->aWB:Ljava/lang/String;

    :goto_0
    invoke-static {}, Lcom/android/settings/cloud/h;->aLl()V

    return-void

    :cond_0
    const-string/jumbo v0, "https://api.sec.miui.com/"

    sput-object v0, Lcom/android/settings/cloud/h;->aWC:Ljava/lang/String;

    const-string/jumbo v0, "https://data.sec.miui.com/"

    sput-object v0, Lcom/android/settings/cloud/h;->aWB:Ljava/lang/String;

    goto :goto_0
.end method

.method public static aLn()V
    .locals 1

    const-string/jumbo v0, "http://staging.api.sec.miui.com/"

    sput-object v0, Lcom/android/settings/cloud/h;->aWC:Ljava/lang/String;

    const-string/jumbo v0, "http://staging.api.sec.miui.com/"

    sput-object v0, Lcom/android/settings/cloud/h;->aWB:Ljava/lang/String;

    invoke-static {}, Lcom/android/settings/cloud/h;->aLl()V

    return-void
.end method
