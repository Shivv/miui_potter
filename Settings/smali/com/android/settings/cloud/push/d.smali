.class public Lcom/android/settings/cloud/push/d;
.super Ljava/lang/Object;
.source "CompatChecker.java"


# static fields
.field public static aVm:Lcom/android/settings/cloud/push/d;


# instance fields
.field private aVn:Landroid/app/ActivityManager;

.field private aVo:Lcom/android/settings/cloud/push/g;

.field private mContext:Landroid/content/Context;

.field private mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/cloud/push/d;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/cloud/push/d;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string/jumbo v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/settings/cloud/push/d;->aVn:Landroid/app/ActivityManager;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/cloud/push/d;->aVo:Lcom/android/settings/cloud/push/g;

    return-void
.end method

.method static synthetic aJq(Lcom/android/settings/cloud/push/d;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/d;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic aJr(Lcom/android/settings/cloud/push/d;)Lcom/android/settings/cloud/push/g;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/d;->aVo:Lcom/android/settings/cloud/push/g;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/settings/cloud/push/d;
    .locals 2

    sget-object v0, Lcom/android/settings/cloud/push/d;->aVm:Lcom/android/settings/cloud/push/d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/cloud/push/d;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/cloud/push/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/cloud/push/d;->aVm:Lcom/android/settings/cloud/push/d;

    :cond_0
    sget-object v0, Lcom/android/settings/cloud/push/d;->aVm:Lcom/android/settings/cloud/push/d;

    return-object v0
.end method


# virtual methods
.method public aJl(Ljava/util/List;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cloud/push/g;

    invoke-virtual {p0, v0}, Lcom/android/settings/cloud/push/d;->aJn(Lcom/android/settings/cloud/push/g;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public aJm(Ljava/util/List;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cloud/push/i;

    invoke-virtual {p0, v0}, Lcom/android/settings/cloud/push/d;->aJo(Lcom/android/settings/cloud/push/i;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public aJn(Lcom/android/settings/cloud/push/g;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/cloud/push/g;->aJC()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/cloud/push/d;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-nez v2, :cond_2

    return-void

    :catch_0
    move-exception v0

    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/android/settings/cloud/push/g;->aJD()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/android/settings/cloud/push/g;->aJE()Ljava/util/Set;

    move-result-object v0

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object p1, p0, Lcom/android/settings/cloud/push/d;->aVo:Lcom/android/settings/cloud/push/g;

    invoke-virtual {p0, v1}, Lcom/android/settings/cloud/push/d;->aJp(Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    invoke-virtual {p1}, Lcom/android/settings/cloud/push/g;->aJE()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-lt v4, v0, :cond_5

    iput-object p1, p0, Lcom/android/settings/cloud/push/d;->aVo:Lcom/android/settings/cloud/push/g;

    invoke-virtual {p0, v1}, Lcom/android/settings/cloud/push/d;->aJp(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public aJo(Lcom/android/settings/cloud/push/i;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/cloud/push/i;->aJN()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/cloud/push/d;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-nez v2, :cond_2

    return-void

    :catch_0
    move-exception v0

    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/android/settings/cloud/push/i;->aJO()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/android/settings/cloud/push/i;->aJP()Ljava/util/Set;

    move-result-object v0

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/cloud/push/d;->aVn:Landroid/app/ActivityManager;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    invoke-virtual {p1}, Lcom/android/settings/cloud/push/i;->aJP()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v4, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-lt v4, v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/cloud/push/d;->aVn:Landroid/app/ActivityManager;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public aJp(Ljava/lang/String;)V
    .locals 3

    new-instance v1, Lcom/android/settings/cloud/push/e;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/android/settings/cloud/push/e;-><init>(Lcom/android/settings/cloud/push/d;Lcom/android/settings/cloud/push/e;)V

    iget-object v0, p0, Lcom/android/settings/cloud/push/d;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    monitor-enter v1

    :goto_0
    :try_start_0
    iget-boolean v0, v1, Lcom/android/settings/cloud/push/e;->aVp:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {v1}, Lcom/android/settings/cloud/push/e;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    monitor-exit v1

    return-void
.end method
