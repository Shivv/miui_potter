.class public Lcom/android/settings/cloud/push/i;
.super Ljava/lang/Object;
.source "RunningCompatibility.java"


# instance fields
.field private aVE:Ljava/lang/String;

.field private aVF:Z

.field private aVG:Ljava/util/Set;

.field private mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public aJJ(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cloud/push/i;->mPackageName:Ljava/lang/String;

    return-void
.end method

.method public aJK(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cloud/push/i;->aVE:Ljava/lang/String;

    return-void
.end method

.method public aJL(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/cloud/push/i;->aVF:Z

    return-void
.end method

.method public aJM(Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/cloud/push/i;->aVG:Ljava/util/Set;

    return-void
.end method

.method public aJN()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/i;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public aJO()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/cloud/push/i;->aVF:Z

    return v0
.end method

.method public aJP()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/i;->aVG:Ljava/util/Set;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cloud/push/i;->aVE:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "RunningCompatibility : PackageName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cloud/push/i;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Message = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cloud/push/i;->aVE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Precise = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/settings/cloud/push/i;->aVF:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " Versions = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cloud/push/i;->aVG:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
