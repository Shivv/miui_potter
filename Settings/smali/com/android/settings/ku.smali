.class final Lcom/android/settings/ku;
.super Ljava/lang/Object;
.source "BasePreferenceFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic cra:Lcom/android/settings/BasePreferenceFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/BasePreferenceFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/ku;->cra:Lcom/android/settings/BasePreferenceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    sget-boolean v0, Lmiui/os/Build;->IS_MIPAD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/ku;->cra:Lcom/android/settings/BasePreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/BasePreferenceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ku;->cra:Lcom/android/settings/BasePreferenceFragment;

    invoke-virtual {v0}, Lcom/android/settings/BasePreferenceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v1, p3, v0

    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/ku;->cra:Lcom/android/settings/BasePreferenceFragment;

    iget-object v0, v0, Lcom/android/settings/BasePreferenceFragment;->caL:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/ku;->cra:Lcom/android/settings/BasePreferenceFragment;

    iget-object v0, v0, Lcom/android/settings/BasePreferenceFragment;->caL:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-object v2, p0, Lcom/android/settings/ku;->cra:Lcom/android/settings/BasePreferenceFragment;

    invoke-static {v2, v0, v1}, Lcom/android/settings/BasePreferenceFragment;->bTh(Lcom/android/settings/BasePreferenceFragment;Landroid/preference/PreferenceActivity$Header;I)V

    return-void
.end method
