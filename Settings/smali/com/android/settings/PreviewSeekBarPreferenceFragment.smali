.class public abstract Lcom/android/settings/PreviewSeekBarPreferenceFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "PreviewSeekBarPreferenceFragment.java"


# instance fields
.field private bQA:Landroid/view/View;

.field protected bQo:I

.field protected bQp:[I

.field protected bQq:[Ljava/lang/String;

.field protected bQr:I

.field protected bQs:I

.field private bQt:Landroid/widget/TextView;

.field private bQu:Landroid/view/View;

.field private bQv:Lcom/android/settings/widget/DotsPageIndicator;

.field private bQw:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

.field private bQx:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

.field private bQy:Lcom/android/internal/widget/ViewPager;

.field private bQz:Lcom/android/settings/ci;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/iM;

    invoke-direct {v0, p0}, Lcom/android/settings/iM;-><init>(Lcom/android/settings/PreviewSeekBarPreferenceFragment;)V

    iput-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQx:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    new-instance v0, Lcom/android/settings/iN;

    invoke-direct {v0, p0}, Lcom/android/settings/iN;-><init>(Lcom/android/settings/PreviewSeekBarPreferenceFragment;)V

    iput-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQw:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    return-void
.end method

.method private bIX(I)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQv:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-virtual {p0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bWz()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQp:[I

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const v3, 0x7f120d03

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/DotsPageIndicator;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private bIY(IZ)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQt:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQq:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQA:Landroid/view/View;

    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQu:Landroid/view/View;

    iget-object v3, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQq:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ge p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQy:Lcom/android/internal/widget/ViewPager;

    invoke-virtual {v0}, Lcom/android/internal/widget/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bIX(I)V

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQz:Lcom/android/settings/ci;

    iget v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQs:I

    iget-object v2, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQy:Lcom/android/internal/widget/ViewPager;

    invoke-virtual {v2}, Lcom/android/internal/widget/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2, p2}, Lcom/android/settings/ci;->bQJ(IIIZ)V

    iput p1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQs:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method static synthetic bIZ(Lcom/android/settings/PreviewSeekBarPreferenceFragment;)Lcom/android/internal/widget/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQy:Lcom/android/internal/widget/ViewPager;

    return-object v0
.end method

.method static synthetic bJa(Lcom/android/settings/PreviewSeekBarPreferenceFragment;)Lcom/android/settings/ci;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQz:Lcom/android/settings/ci;

    return-object v0
.end method

.method static synthetic bJb(Lcom/android/settings/PreviewSeekBarPreferenceFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bIX(I)V

    return-void
.end method

.method static synthetic bJc(Lcom/android/settings/PreviewSeekBarPreferenceFragment;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bIY(IZ)V

    return-void
.end method


# virtual methods
.method protected abstract Mt(Landroid/content/res/Configuration;I)Landroid/content/res/Configuration;
.end method

.method protected abstract commit()V
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v4

    const v0, 0x102003f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQo:I

    invoke-virtual {p1, v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v0, 0x7f0a0110

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQq:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    const v0, 0x7f0a03d3

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/LabeledSeekBar;

    iget-object v6, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQq:[Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/android/settings/widget/LabeledSeekBar;->setLabels([Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/LabeledSeekBar;->setMax(I)V

    iget v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQr:I

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/LabeledSeekBar;->setProgress(I)V

    new-instance v1, Lcom/android/settings/bp;

    const/4 v6, 0x0

    invoke-direct {v1, p0, v6}, Lcom/android/settings/bp;-><init>(Lcom/android/settings/PreviewSeekBarPreferenceFragment;Lcom/android/settings/bp;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/LabeledSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const v1, 0x7f0a0412

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQA:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQA:Landroid/view/View;

    new-instance v6, Lcom/android/settings/iO;

    invoke-direct {v6, p0, v0}, Lcom/android/settings/iO;-><init>(Lcom/android/settings/PreviewSeekBarPreferenceFragment;Lcom/android/settings/widget/LabeledSeekBar;)V

    invoke-virtual {v1, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a0257

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQu:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQu:Landroid/view/View;

    new-instance v6, Lcom/android/settings/iP;

    invoke-direct {v6, p0, v0}, Lcom/android/settings/iP;-><init>(Lcom/android/settings/PreviewSeekBarPreferenceFragment;Lcom/android/settings/widget/LabeledSeekBar;)V

    invoke-virtual {v1, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQq:[Ljava/lang/String;

    array-length v1, v1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, v3}, Lcom/android/settings/widget/LabeledSeekBar;->setEnabled(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    if-ne v0, v2, :cond_1

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQq:[Ljava/lang/String;

    array-length v0, v0

    new-array v8, v0, [Landroid/content/res/Configuration;

    move v0, v3

    :goto_1
    iget-object v9, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQq:[Ljava/lang/String;

    array-length v9, v9

    if-ge v0, v9, :cond_2

    invoke-virtual {p0, v7, v0}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->Mt(Landroid/content/res/Configuration;I)Landroid/content/res/Configuration;

    move-result-object v9

    aput-object v9, v8, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    const v0, 0x7f0a0334

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/ViewPager;

    iput-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQy:Lcom/android/internal/widget/ViewPager;

    new-instance v0, Lcom/android/settings/ci;

    iget-object v7, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQp:[I

    invoke-direct {v0, v6, v1, v7, v8}, Lcom/android/settings/ci;-><init>(Landroid/content/Context;Z[I[Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQz:Lcom/android/settings/ci;

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQy:Lcom/android/internal/widget/ViewPager;

    iget-object v6, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQz:Lcom/android/settings/ci;

    invoke-virtual {v0, v6}, Lcom/android/internal/widget/ViewPager;->setAdapter(Lcom/android/internal/widget/PagerAdapter;)V

    iget-object v6, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQy:Lcom/android/internal/widget/ViewPager;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQp:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_2
    invoke-virtual {v6, v0}, Lcom/android/internal/widget/ViewPager;->setCurrentItem(I)V

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQy:Lcom/android/internal/widget/ViewPager;

    iget-object v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQx:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ViewPager;->setOnPageChangeListener(Lcom/android/internal/widget/ViewPager$OnPageChangeListener;)V

    const v0, 0x7f0a0300

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/DotsPageIndicator;

    iput-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQv:Lcom/android/settings/widget/DotsPageIndicator;

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQp:[I

    array-length v0, v0

    if-le v0, v2, :cond_4

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQv:Lcom/android/settings/widget/DotsPageIndicator;

    iget-object v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQy:Lcom/android/internal/widget/ViewPager;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/DotsPageIndicator;->setViewPager(Lcom/android/internal/widget/ViewPager;)V

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQv:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-virtual {v0, v3}, Lcom/android/settings/widget/DotsPageIndicator;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQv:Lcom/android/settings/widget/DotsPageIndicator;

    iget-object v1, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQw:Lcom/android/internal/widget/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/DotsPageIndicator;->setOnPageChangeListener(Lcom/android/internal/widget/ViewPager$OnPageChangeListener;)V

    :goto_3
    iget v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQr:I

    invoke-direct {p0, v0, v3}, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bIY(IZ)V

    return-object v4

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/android/settings/PreviewSeekBarPreferenceFragment;->bQv:Lcom/android/settings/widget/DotsPageIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/DotsPageIndicator;->setVisibility(I)V

    goto :goto_3
.end method
