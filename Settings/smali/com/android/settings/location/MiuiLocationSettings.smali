.class public Lcom/android/settings/location/MiuiLocationSettings;
.super Lcom/android/settings/location/LocationSettingsBase;
.source "MiuiLocationSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;


# instance fields
.field private abK:Lcom/android/settings/location/SettingsInjector;

.field private abL:Landroid/preference/Preference;

.field private abM:Z

.field private abN:Landroid/preference/CheckBoxPreference;

.field private abO:Landroid/preference/CheckBoxPreference;

.field private abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

.field private abQ:Landroid/content/SharedPreferences$Editor;

.field private abR:Z

.field private abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

.field private abT:Landroid/location/LocationManager;

.field private abU:Landroid/preference/Preference;

.field private abV:Landroid/os/UserHandle;

.field private abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field private abX:Landroid/preference/Preference$OnPreferenceClickListener;

.field private abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

.field private abZ:Landroid/content/SharedPreferences;

.field private aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field private acb:Landroid/os/UserManager;

.field private acc:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/location/LocationSettingsBase;-><init>()V

    new-instance v0, Lcom/android/settings/location/MiuiLocationSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/location/MiuiLocationSettings$1;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abX:Landroid/preference/Preference$OnPreferenceClickListener;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->acc:Z

    return-void
.end method

.method private Sf(Landroid/content/Context;Landroid/preference/PreferenceScreen;Z)V
    .locals 4

    const-string/jumbo v0, "location_services"

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    new-instance v1, Lcom/android/settings/location/SettingsInjector;

    invoke-direct {v1, p1}, Lcom/android/settings/location/SettingsInjector;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abK:Lcom/android/settings/location/SettingsInjector;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/location/MiuiLocationSettings;->abK:Lcom/android/settings/location/SettingsInjector;

    if-eqz p3, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    :goto_0
    invoke-virtual {v3, v2, v1}, Lcom/android/settings/location/SettingsInjector;->RU(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/android/settings/location/MiuiLocationSettings$6;

    invoke-direct {v2, p0}, Lcom/android/settings/location/MiuiLocationSettings$6;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    iput-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v3, "android.location.InjectedSettingChanged"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/location/MiuiLocationSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v3, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-direct {p0, v1, v0}, Lcom/android/settings/location/MiuiLocationSettings;->Sg(Ljava/util/List;Landroid/preference/PreferenceGroup;)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, -0x2

    goto :goto_0

    :cond_1
    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method private Sg(Ljava/util/List;Landroid/preference/PreferenceGroup;)V
    .locals 2

    new-instance v0, Lcom/android/settings/location/MiuiLocationSettings$2;

    invoke-direct {v0, p0}, Lcom/android/settings/location/MiuiLocationSettings$2;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private Sh(Z)V
    .locals 5

    const v0, 0x7f12120f

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "no_share_location"

    iget-object v3, p0, Lcom/android/settings/location/MiuiLocationSettings;->abV:Landroid/os/UserHandle;

    invoke-virtual {v3}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/settings/location/MiuiLocationSettings;->Sj()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v4}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, p1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, v4}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    :goto_1
    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setSummary(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    xor-int/lit8 v3, v2, 0x1

    invoke-virtual {v1, v3}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    if-eqz v2, :cond_3

    :goto_2
    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abX:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v1, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_1

    :cond_3
    const v0, 0x7f121210

    goto :goto_2
.end method

.method private Si()Landroid/preference/PreferenceScreen;
    .locals 6

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    const v0, 0x7f150085

    invoke-virtual {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/settings/location/MiuiLocationSettings;->Sm(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v0, "location_toggle"

    invoke-virtual {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "location_mode"

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    new-instance v4, Lcom/android/settings/location/MiuiLocationSettings$3;

    invoke-direct {v4, p0}, Lcom/android/settings/location/MiuiLocationSettings$3;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v4, "android.hardware.location.gps"

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abR:Z

    iget-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abR:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v5, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    :goto_0
    const-string/jumbo v0, "support_agps"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abM:Z

    const-string/jumbo v0, "assisted_gps"

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abO:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "agps_roaming"

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abN:Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/android/settings/location/MiuiLocationSettings;->abN:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    iget-object v5, p0, Lcom/android/settings/location/MiuiLocationSettings;->abT:Landroid/location/LocationManager;

    invoke-virtual {v0, v5}, Lcom/android/settings/dc;->bYA(Landroid/location/LocationManager;)I

    move-result v0

    if-ne v0, v2, :cond_6

    move v0, v2

    :goto_2
    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "assisted_gps_params"

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abL:Landroid/preference/Preference;

    iget-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abM:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abL:Landroid/preference/Preference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abO:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/android/settings/location/MiuiLocationSettings;->abO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v5, "assisted_gps_enabled"

    invoke-static {v0, v5, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_9

    move v0, v2

    :goto_4
    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abV:Landroid/os/UserHandle;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->acb:Landroid/os/UserManager;

    const-string/jumbo v4, "no_share_location"

    iget-object v5, p0, Lcom/android/settings/location/MiuiLocationSettings;->abV:Landroid/os/UserHandle;

    invoke-virtual {v0, v4, v5}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v1, v2

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0, v3, v1}, Lcom/android/settings/location/MiuiLocationSettings;->Sf(Landroid/content/Context;Landroid/preference/PreferenceScreen;Z)V

    invoke-virtual {p0, v2}, Lcom/android/settings/location/MiuiLocationSettings;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->Sw()V

    return-object v3

    :cond_4
    const-string/jumbo v0, "high_accuracy"

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/location/MiuiRadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    const-string/jumbo v0, "battery_saving"

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/location/MiuiRadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    const-string/jumbo v0, "sensors_only"

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/location/MiuiRadioButtonPreference;

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/location/MiuiRadioButtonPreference;->SL(Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/location/MiuiRadioButtonPreference;->SL(Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/location/MiuiRadioButtonPreference;->SL(Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto/16 :goto_1

    :cond_6
    move v0, v1

    goto/16 :goto_2

    :cond_7
    const-string/jumbo v0, "support_agps_paras"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3

    :cond_8
    const-string/jumbo v0, "support_agps_roaming"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abL:Landroid/preference/Preference;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_3

    :cond_9
    move v0, v1

    goto :goto_4
.end method

.method private Sj()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abV:Landroid/os/UserHandle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->acb:Landroid/os/UserManager;

    const-string/jumbo v1, "no_share_location"

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abV:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/os/UserManager;->hasBaseUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v0

    return v0
.end method

.method private Sm(Landroid/preference/PreferenceScreen;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->acb:Landroid/os/UserManager;

    invoke-static {v0}, Lcom/android/settings/aq;->bqQ(Landroid/os/UserManager;)Landroid/os/UserHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abV:Landroid/os/UserHandle;

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abV:Landroid/os/UserHandle;

    if-nez v0, :cond_0

    const-string/jumbo v0, "managed_profile_location_switch"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "managed_profile_location_switch"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method private Sn(Lmiui/preference/RadioButtonPreference;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v2}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setChecked(Z)V

    goto :goto_0
.end method

.method static synthetic So(Lcom/android/settings/location/MiuiLocationSettings;)Lcom/android/settings/location/SettingsInjector;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abK:Lcom/android/settings/location/SettingsInjector;

    return-object v0
.end method

.method static synthetic Sp(Lcom/android/settings/location/MiuiLocationSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abN:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic Sq(Lcom/android/settings/location/MiuiLocationSettings;)Landroid/location/LocationManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abT:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic Sr(Lcom/android/settings/location/MiuiLocationSettings;)Landroid/os/UserHandle;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abV:Landroid/os/UserHandle;

    return-object v0
.end method

.method static synthetic Ss(Lcom/android/settings/location/MiuiLocationSettings;)Lcom/android/settingslib/MiuiRestrictedSwitchPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abW:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    return-object v0
.end method

.method static synthetic St(Lcom/android/settings/location/MiuiLocationSettings;)Landroid/os/UserManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->acb:Landroid/os/UserManager;

    return-object v0
.end method


# virtual methods
.method public Sk(IZ)V
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v2, "no_share_location"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v3, "no_share_location"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-static {v0, v3, v4}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v3

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v3, :cond_3

    if-eqz v2, :cond_3

    iget-object v3, p0, Lcom/android/settings/location/MiuiLocationSettings;->aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v3, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    :goto_1
    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isChecked()Z

    move-result v2

    if-eq v0, v2, :cond_1

    iget-boolean v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->acc:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v2, v5}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v2, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-boolean v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->acc:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v2, p0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_1
    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    if-nez v2, :cond_4

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    xor-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    goto :goto_1

    :cond_4
    packed-switch p1, :pswitch_data_0

    :goto_2
    if-eqz p1, :cond_5

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abQ:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v3, "last_mode"

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abQ:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_5
    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    if-eqz v0, :cond_7

    xor-int/lit8 v0, p2, 0x1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    if-eqz p1, :cond_6

    xor-int/lit8 v1, p2, 0x1

    :cond_6
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/location/MiuiRadioButtonPreference;->setEnabled(Z)V

    invoke-direct {p0, v1}, Lcom/android/settings/location/MiuiLocationSettings;->Sh(Z)V

    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    const v3, 0x7f12094e

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    invoke-direct {p0, v5}, Lcom/android/settings/location/MiuiLocationSettings;->Sn(Lmiui/preference/RadioButtonPreference;)V

    goto :goto_2

    :pswitch_1
    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    const v3, 0x7f120951

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-direct {p0, v2}, Lcom/android/settings/location/MiuiLocationSettings;->Sn(Lmiui/preference/RadioButtonPreference;)V

    goto :goto_2

    :pswitch_2
    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    const v3, 0x7f12094b

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-direct {p0, v2}, Lcom/android/settings/location/MiuiLocationSettings;->Sn(Lmiui/preference/RadioButtonPreference;)V

    goto :goto_2

    :pswitch_3
    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abU:Landroid/preference/Preference;

    const v3, 0x7f12094d

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    invoke-direct {p0, v2}, Lcom/android/settings/location/MiuiLocationSettings;->Sn(Lmiui/preference/RadioButtonPreference;)V

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public Sl(Lmiui/preference/RadioButtonPreference;)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abS:Lcom/android/settings/location/MiuiRadioButtonPreference;

    if-ne p1, v1, :cond_1

    const/4 v0, 0x3

    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->Sx(I)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abP:Lcom/android/settings/location/MiuiRadioButtonPreference;

    if-ne p1, v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abY:Lcom/android/settings/location/MiuiRadioButtonPreference;

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public aq()I
    .locals 1

    const v0, 0x7f12081d

    return v0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/location/MiuiLocationSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/location/LocationSettingsBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "location"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abT:Landroid/location/LocationManager;

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "location_last_mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abZ:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abZ:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abQ:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->acb:Landroid/os/UserManager;

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->acb:Landroid/os/UserManager;

    invoke-static {v0}, Lcom/android/settings/aq;->bqQ(Landroid/os/UserManager;)Landroid/os/UserHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abV:Landroid/os/UserHandle;

    return-void
.end method

.method public onPause()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-super {p0}, Lcom/android/settings/location/LocationSettingsBase;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->acc:Z

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->aca:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x0

    instance-of v0, p1, Landroid/preference/TwoStatePreference;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abZ:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "last_mode"

    iget-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abR:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    :goto_0
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->Sx(I)V

    :goto_1
    return v3

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v3}, Lcom/android/settings/location/MiuiLocationSettings;->Sx(I)V

    goto :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abO:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "assisted_gps_supl_host"

    invoke-static {v5, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "assisted_gps_supl_port"

    invoke-static {v5, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :try_start_0
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    new-instance v6, Ljava/io/File;

    const-string/jumbo v1, "/etc/gps.conf"

    invoke-direct {v6, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string/jumbo v2, "assisted_gps_supl_host"

    const-string/jumbo v6, "SUPL_HOST"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v2, v6}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const-string/jumbo v2, "assisted_gps_supl_port"

    const-string/jumbo v6, "SUPL_PORT"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v2, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    const-string/jumbo v1, "assisted_gps_enabled"

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    :goto_1
    invoke-static {v5, v1, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_2
    return v3

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_3
    :try_start_3
    const-string/jumbo v2, "LocationSettings"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Could not open GPS configuration file /etc/gps.conf, e="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    if-eqz v1, :cond_2

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :goto_5
    throw v0

    :catch_3
    move-exception v1

    goto :goto_5

    :cond_3
    move v0, v4

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abN:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->abN:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/android/settings/location/MiuiLocationSettings$4;

    invoke-direct {v1, p0}, Lcom/android/settings/location/MiuiLocationSettings$4;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/location/MiuiLocationSettings$5;

    invoke-direct {v1, p0}, Lcom/android/settings/location/MiuiLocationSettings$5;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1200cc

    invoke-virtual {p0, v1}, Lcom/android/settings/location/MiuiLocationSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1200cb

    invoke-virtual {p0, v1}, Lcom/android/settings/location/MiuiLocationSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->abT:Landroid/location/LocationManager;

    invoke-virtual {v0, v1, v4}, Lcom/android/settings/dc;->bYB(Landroid/location/LocationManager;I)V

    goto/16 :goto_2

    :cond_6
    invoke-super {p0, p1, p2}, Lcom/android/settings/location/LocationSettingsBase;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto/16 :goto_3
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/location/LocationSettingsBase;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->acc:Z

    invoke-direct {p0}, Lcom/android/settings/location/MiuiLocationSettings;->Si()Landroid/preference/PreferenceScreen;

    return-void
.end method
