.class public Lcom/android/settings/location/LocationSettings;
.super Lcom/android/settings/location/LocationSettingsBase;
.source "LocationSettings.java"

# interfaces
.implements Lcom/android/settings/widget/t;


# static fields
.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;


# instance fields
.field private acf:Lcom/android/settings/location/SettingsInjector;

.field private acg:Z

.field private ach:Landroid/preference/SwitchPreference;

.field private aci:Landroid/preference/PreferenceCategory;

.field private acj:Landroid/preference/Preference;

.field private ack:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field private acl:Landroid/os/UserHandle;

.field private acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

.field private acn:Landroid/preference/Preference$OnPreferenceClickListener;

.field private aco:Landroid/widget/Switch;

.field private acp:Lcom/android/settings/widget/SwitchBar;

.field private acq:Landroid/os/UserManager;

.field private acr:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/location/LocationSettings$2;

    invoke-direct {v0}, Lcom/android/settings/location/LocationSettings$2;-><init>()V

    sput-object v0, Lcom/android/settings/location/LocationSettings;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/location/LocationSettingsBase;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/location/LocationSettings;->acr:Z

    new-instance v0, Lcom/android/settings/location/LocationSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/location/LocationSettings$1;-><init>(Lcom/android/settings/location/LocationSettings;)V

    iput-object v0, p0, Lcom/android/settings/location/LocationSettings;->acn:Landroid/preference/Preference$OnPreferenceClickListener;

    return-void
.end method

.method private SA(Ljava/util/List;Landroid/preference/PreferenceGroup;)V
    .locals 2

    new-instance v0, Lcom/android/settings/location/LocationSettings$3;

    invoke-direct {v0, p0}, Lcom/android/settings/location/LocationSettings$3;-><init>(Lcom/android/settings/location/LocationSettings;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private SB()V
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->ach:Landroid/preference/SwitchPreference;

    invoke-virtual {v0}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "assisted_gps_supl_host"

    invoke-static {v3, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "assisted_gps_supl_port"

    invoke-static {v3, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :try_start_0
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    new-instance v4, Ljava/io/File;

    const-string/jumbo v1, "/etc/gps.conf"

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string/jumbo v2, "assisted_gps_supl_host"

    const-string/jumbo v4, "SUPL_HOST"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v4}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const-string/jumbo v2, "assisted_gps_supl_port"

    const-string/jumbo v4, "SUPL_PORT"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v2, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    const-string/jumbo v1, "assisted_gps_enabled"

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->ach:Landroid/preference/SwitchPreference;

    invoke-virtual {v0}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v3, v1, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_3
    const-string/jumbo v2, "LocationSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Could not open GPS configuration file /etc/gps.conf, e="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_2

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    goto :goto_4

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2
.end method

.method private SC(Z)V
    .locals 5

    const v0, 0x7f12120f

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "no_share_location"

    iget-object v3, p0, Lcom/android/settings/location/LocationSettings;->acl:Landroid/os/UserHandle;

    invoke-virtual {v3}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/settings/location/LocationSettings;->SE()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v4}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, p1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, v4}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    :goto_1
    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setSummary(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    xor-int/lit8 v3, v2, 0x1

    invoke-virtual {v1, v3}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    if-eqz v2, :cond_3

    :goto_2
    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v2, p0, Lcom/android/settings/location/LocationSettings;->acn:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v1, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_1

    :cond_3
    const v0, 0x7f121210

    goto :goto_2
.end method

.method private SD()Landroid/preference/PreferenceScreen;
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    const v1, 0x7f15007a

    invoke-virtual {p0, v1}, Lcom/android/settings/location/LocationSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const-string/jumbo v1, "location_toggle"

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v1, p0, Lcom/android/settings/location/LocationSettings;->ack:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v5, p0, Lcom/android/settings/location/LocationSettings;->ack:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v6, "location_mode"

    invoke-static {v1, v6, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v5, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->ack:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    new-instance v5, Lcom/android/settings/location/LocationSettings$4;

    invoke-direct {v5, p0}, Lcom/android/settings/location/LocationSettings$4;-><init>(Lcom/android/settings/location/LocationSettings;)V

    invoke-virtual {v1, v5}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-direct {p0, v4}, Lcom/android/settings/location/LocationSettings;->SF(Landroid/preference/PreferenceScreen;)V

    const-string/jumbo v1, "location_mode"

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/location/LocationSettings;->acj:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acj:Landroid/preference/Preference;

    new-instance v5, Lcom/android/settings/location/LocationSettings$5;

    invoke-direct {v5, p0, v0}, Lcom/android/settings/location/LocationSettings$5;-><init>(Lcom/android/settings/location/LocationSettings;Lcom/android/settings/bL;)V

    invoke-virtual {v1, v5}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    new-instance v1, Lcom/android/settingslib/k/a;

    invoke-direct {v1, v0}, Lcom/android/settingslib/k/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/android/settingslib/k/a;->cpE()Ljava/util/List;

    move-result-object v5

    new-instance v1, Lcom/android/settings/location/AppLocationPermissionPreferenceController;

    invoke-direct {v1, v0}, Lcom/android/settings/location/AppLocationPermissionPreferenceController;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Lcom/android/settings/location/AppLocationPermissionPreferenceController;->i(Landroid/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f050007

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/location/LocationSettings;->acg:Z

    const-string/jumbo v1, "assisted_gps"

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/SwitchPreference;

    iput-object v1, p0, Lcom/android/settings/location/LocationSettings;->ach:Landroid/preference/SwitchPreference;

    iget-boolean v1, p0, Lcom/android/settings/location/LocationSettings;->acg:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->ach:Landroid/preference/SwitchPreference;

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_1
    const-string/jumbo v1, "recent_location_requests"

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    iput-object v1, p0, Lcom/android/settings/location/LocationSettings;->aci:Landroid/preference/PreferenceCategory;

    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/k/b;

    new-instance v8, Lcom/android/settings/DimmableIconPreference;

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->bWz()Landroid/content/Context;

    move-result-object v9

    iget-object v10, v1, Lcom/android/settingslib/k/b;->cJT:Ljava/lang/CharSequence;

    invoke-direct {v8, v9, v10}, Lcom/android/settings/DimmableIconPreference;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-object v9, v1, Lcom/android/settingslib/k/b;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, v9}, Lcom/android/settings/DimmableIconPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object v9, v1, Lcom/android/settingslib/k/b;->cJW:Ljava/lang/CharSequence;

    invoke-virtual {v8, v9}, Lcom/android/settings/DimmableIconPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-boolean v9, v1, Lcom/android/settingslib/k/b;->cJV:Z

    if-eqz v9, :cond_5

    const v9, 0x7f120948

    invoke-virtual {v8, v9}, Lcom/android/settings/DimmableIconPreference;->setSummary(I)V

    :goto_3
    new-instance v9, Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;

    iget-object v10, v1, Lcom/android/settingslib/k/b;->packageName:Ljava/lang/String;

    iget-object v1, v1, Lcom/android/settingslib/k/b;->cJU:Landroid/os/UserHandle;

    invoke-direct {v9, p0, v10, v1}, Lcom/android/settings/location/LocationSettings$PackageEntryClickedListener;-><init>(Lcom/android/settings/location/LocationSettings;Ljava/lang/String;Landroid/os/UserHandle;)V

    invoke-virtual {v8, v9}, Lcom/android/settings/DimmableIconPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    move v1, v3

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->ach:Landroid/preference/SwitchPreference;

    if-eqz v1, :cond_1

    iget-object v6, p0, Lcom/android/settings/location/LocationSettings;->ach:Landroid/preference/SwitchPreference;

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v7, "assisted_gps_enabled"

    invoke-static {v1, v7, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v2, :cond_4

    move v1, v2

    :goto_4
    invoke-virtual {v6, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->bWB()Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->ach:Landroid/preference/SwitchPreference;

    new-instance v6, Lcom/android/settings/location/LocationSettings$6;

    invoke-direct {v6, p0}, Lcom/android/settings/location/LocationSettings$6;-><init>(Lcom/android/settings/location/LocationSettings;)V

    invoke-virtual {v1, v6}, Landroid/preference/SwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_4

    :cond_5
    const v9, 0x7f120949

    invoke-virtual {v8, v9}, Lcom/android/settings/DimmableIconPreference;->setSummary(I)V

    goto :goto_3

    :cond_6
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_8

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->aci:Landroid/preference/PreferenceCategory;

    invoke-direct {p0, v6, v1}, Lcom/android/settings/location/LocationSettings;->SA(Ljava/util/List;Landroid/preference/PreferenceGroup;)V

    :goto_5
    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acl:Landroid/os/UserHandle;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acq:Landroid/os/UserManager;

    const-string/jumbo v5, "no_share_location"

    iget-object v6, p0, Lcom/android/settings/location/LocationSettings;->acl:Landroid/os/UserHandle;

    invoke-virtual {v1, v5, v6}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v3, v2

    :cond_7
    invoke-direct {p0, v0, v4, v3}, Lcom/android/settings/location/LocationSettings;->Sz(Landroid/content/Context;Landroid/preference/PreferenceScreen;Z)V

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->Sw()V

    return-object v4

    :cond_8
    new-instance v1, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->bWz()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0d00e1

    invoke-virtual {v1, v5}, Landroid/preference/Preference;->setLayoutResource(I)V

    const v5, 0x7f120956

    invoke-virtual {v1, v5}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setSelectable(Z)V

    iget-object v5, p0, Lcom/android/settings/location/LocationSettings;->aci:Landroid/preference/PreferenceCategory;

    invoke-virtual {v5, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_5
.end method

.method private SE()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acl:Landroid/os/UserHandle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acq:Landroid/os/UserManager;

    const-string/jumbo v1, "no_share_location"

    iget-object v2, p0, Lcom/android/settings/location/LocationSettings;->acl:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/os/UserManager;->hasBaseUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v0

    return v0
.end method

.method private SF(Landroid/preference/PreferenceScreen;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acq:Landroid/os/UserManager;

    invoke-static {v0}, Lcom/android/settings/aq;->bqQ(Landroid/os/UserManager;)Landroid/os/UserHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/LocationSettings;->acl:Landroid/os/UserHandle;

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acl:Landroid/os/UserHandle;

    if-nez v0, :cond_0

    const-string/jumbo v0, "managed_profile_location_switch"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v1, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "managed_profile_location_switch"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method static synthetic SG(Lcom/android/settings/location/LocationSettings;)Lcom/android/settings/location/SettingsInjector;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acf:Lcom/android/settings/location/SettingsInjector;

    return-object v0
.end method

.method static synthetic SH(Lcom/android/settings/location/LocationSettings;)Landroid/os/UserHandle;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acl:Landroid/os/UserHandle;

    return-object v0
.end method

.method static synthetic SI(Lcom/android/settings/location/LocationSettings;)Lcom/android/settingslib/MiuiRestrictedSwitchPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acm:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    return-object v0
.end method

.method static synthetic SJ(Lcom/android/settings/location/LocationSettings;)Landroid/os/UserManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acq:Landroid/os/UserManager;

    return-object v0
.end method

.method static synthetic SK(Lcom/android/settings/location/LocationSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/location/LocationSettings;->SB()V

    return-void
.end method

.method private Sz(Landroid/content/Context;Landroid/preference/PreferenceScreen;Z)V
    .locals 4

    const-string/jumbo v0, "location_services"

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    new-instance v1, Lcom/android/settings/location/SettingsInjector;

    invoke-direct {v1, p1}, Lcom/android/settings/location/SettingsInjector;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/location/LocationSettings;->acf:Lcom/android/settings/location/SettingsInjector;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/location/LocationSettings;->acf:Lcom/android/settings/location/SettingsInjector;

    if-eqz p3, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    :goto_0
    invoke-virtual {v3, v2, v1}, Lcom/android/settings/location/SettingsInjector;->RU(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/android/settings/location/LocationSettings$7;

    invoke-direct {v2, p0}, Lcom/android/settings/location/LocationSettings$7;-><init>(Lcom/android/settings/location/LocationSettings;)V

    iput-object v2, p0, Lcom/android/settings/location/LocationSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v3, "android.location.InjectedSettingChanged"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/location/LocationSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v3, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-direct {p0, v1, v0}, Lcom/android/settings/location/LocationSettings;->SA(Ljava/util/List;Landroid/preference/PreferenceGroup;)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, -0x2

    goto :goto_0

    :cond_1
    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method


# virtual methods
.method public Sk(IZ)V
    .locals 6

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/android/settings/location/LocationPreferenceController;->RR(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/settings/location/LocationSettings;->acj:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(I)V

    :cond_0
    if-eqz p1, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string/jumbo v3, "no_share_location"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string/jumbo v4, "no_share_location"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v3

    if-nez v3, :cond_5

    if-eqz v2, :cond_5

    iget-object v3, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v3, v2}, Lcom/android/settings/widget/SwitchBar;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    iget-object v3, p0, Lcom/android/settings/location/LocationSettings;->ack:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {v3, v2}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    :goto_1
    iget-object v2, p0, Lcom/android/settings/location/LocationSettings;->acj:Landroid/preference/Preference;

    if-eqz v0, :cond_1

    xor-int/lit8 v1, p2, 0x1

    :cond_1
    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->aci:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->aco:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    if-eq v0, v1, :cond_3

    iget-boolean v1, p0, Lcom/android/settings/location/LocationSettings;->acr:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v1, p0}, Lcom/android/settings/widget/SwitchBar;->aBy(Lcom/android/settings/widget/t;)V

    :cond_2
    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->aco:Landroid/widget/Switch;

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    iget-boolean v1, p0, Lcom/android/settings/location/LocationSettings;->acr:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v1, p0}, Lcom/android/settings/widget/SwitchBar;->aBx(Lcom/android/settings/widget/t;)V

    :cond_3
    invoke-direct {p0, v0}, Lcom/android/settings/location/LocationSettings;->SC(Z)V

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acf:Lcom/android/settings/location/SettingsInjector;

    invoke-virtual {v0}, Lcom/android/settings/location/SettingsInjector;->RY()V

    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    xor-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Lcom/android/settings/widget/SwitchBar;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/location/LocationSettings;->ack:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    xor-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setEnabled(Z)V

    goto :goto_1
.end method

.method public aq()I
    .locals 1

    const v0, 0x7f12081d

    return v0
.end method

.method public gG(Landroid/widget/Switch;Z)V
    .locals 1

    if-eqz p2, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/location/LocationSettings;->Sx(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/location/LocationSettings;->Sx(I)V

    goto :goto_0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x3f

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/location/LocationSettingsBase;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Lcom/android/settings/bL;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    iput-object v1, p0, Lcom/android/settings/location/LocationSettings;->acq:Landroid/os/UserManager;

    invoke-virtual {p0, v2}, Lcom/android/settings/location/LocationSettings;->setHasOptionsMenu(Z)V

    invoke-virtual {v0}, Lcom/android/settings/bL;->bMF()Lcom/android/settings/widget/SwitchBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchBar;->getSwitch()Lcom/android/settings/widget/ToggleSwitch;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/LocationSettings;->aco:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchBar;->show()V

    invoke-virtual {p0, v2}, Lcom/android/settings/location/LocationSettings;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/location/LocationSettingsBase;->onDestroyView()V

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0}, Lcom/android/settings/widget/SwitchBar;->aBz()V

    return-void
.end method

.method public onPause()V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/android/settings/location/LocationSettings;->acr:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/SwitchBar;->aBy(Lcom/android/settings/widget/t;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/location/LocationSettings;->acr:Z

    :cond_1
    invoke-super {p0}, Lcom/android/settings/location/LocationSettingsBase;->onPause()V

    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "LocationSettings"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "LocationSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Swallowing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acq:Landroid/os/UserManager;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/location/LocationSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/location/LocationSettings;->acq:Landroid/os/UserManager;

    :cond_0
    invoke-super {p0}, Lcom/android/settings/location/LocationSettingsBase;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/location/LocationSettings;->SD()Landroid/preference/PreferenceScreen;

    iget-boolean v0, p0, Lcom/android/settings/location/LocationSettings;->acr:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings;->acp:Lcom/android/settings/widget/SwitchBar;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/SwitchBar;->aBx(Lcom/android/settings/widget/t;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/location/LocationSettings;->acr:Z

    :cond_1
    return-void
.end method
