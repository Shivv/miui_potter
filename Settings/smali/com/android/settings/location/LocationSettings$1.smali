.class final Lcom/android/settings/location/LocationSettings$1;
.super Ljava/lang/Object;
.source "LocationSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic acV:Lcom/android/settings/location/LocationSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/location/LocationSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/location/LocationSettings$1;->acV:Lcom/android/settings/location/LocationSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    iget-object v0, p0, Lcom/android/settings/location/LocationSettings$1;->acV:Lcom/android/settings/location/LocationSettings;

    invoke-static {v0}, Lcom/android/settings/location/LocationSettings;->SI(Lcom/android/settings/location/LocationSettings;)Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings$1;->acV:Lcom/android/settings/location/LocationSettings;

    invoke-static {v1}, Lcom/android/settings/location/LocationSettings;->SJ(Lcom/android/settings/location/LocationSettings;)Landroid/os/UserManager;

    move-result-object v1

    const-string/jumbo v2, "no_share_location"

    xor-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lcom/android/settings/location/LocationSettings$1;->acV:Lcom/android/settings/location/LocationSettings;

    invoke-static {v4}, Lcom/android/settings/location/LocationSettings;->SH(Lcom/android/settings/location/LocationSettings;)Landroid/os/UserHandle;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/UserManager;->setUserRestriction(Ljava/lang/String;ZLandroid/os/UserHandle;)V

    iget-object v1, p0, Lcom/android/settings/location/LocationSettings$1;->acV:Lcom/android/settings/location/LocationSettings;

    invoke-static {v1}, Lcom/android/settings/location/LocationSettings;->SI(Lcom/android/settings/location/LocationSettings;)Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    move-result-object v1

    if-eqz v0, :cond_0

    const v0, 0x7f121210

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;->setSummary(I)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const v0, 0x7f12120f

    goto :goto_0
.end method
