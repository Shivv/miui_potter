.class public Lcom/android/settings/location/MiuiRadioButtonPreference;
.super Lmiui/preference/RadioButtonPreference;
.source "MiuiRadioButtonPreference.java"


# instance fields
.field private acv:Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/location/MiuiRadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/location/MiuiRadioButtonPreference;->acv:Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/location/MiuiRadioButtonPreference;->acv:Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;

    return-void
.end method


# virtual methods
.method SL(Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/location/MiuiRadioButtonPreference;->acv:Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;

    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/preference/RadioButtonPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    :cond_0
    const v0, 0x1020001

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    :cond_1
    return-void
.end method

.method public onClick()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/MiuiRadioButtonPreference;->acv:Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/location/MiuiRadioButtonPreference;->acv:Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;

    invoke-interface {v0, p0}, Lcom/android/settings/location/MiuiRadioButtonPreference$OnClickListener;->Sl(Lmiui/preference/RadioButtonPreference;)V

    :cond_0
    return-void
.end method
