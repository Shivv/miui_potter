.class public Lcom/android/settings/MaxAspectRatioSettings;
.super Lcom/android/settings/BaseFragment;
.source "MaxAspectRatioSettings.java"


# instance fields
.field public bWq:Ljava/util/List;

.field public bWr:Ljava/util/List;

.field public bWs:Ljava/util/List;

.field public bWt:Lcom/android/settings/cf;

.field private bWu:Lcom/android/settings/cN;

.field private bWv:Landroid/view/LayoutInflater;

.field private bWw:Landroid/widget/ListView;

.field private bWx:Landroid/content/BroadcastReceiver;

.field private bWy:Lcom/android/settings/bQ;

.field public mContext:Landroid/content/Context;

.field private mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Lcom/android/settings/jY;

    invoke-direct {v0, p0}, Lcom/android/settings/jY;-><init>(Lcom/android/settings/MaxAspectRatioSettings;)V

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWx:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWq:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWr:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWs:Ljava/util/List;

    return-void
.end method

.method private bQu()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWy:Lcom/android/settings/bQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWy:Lcom/android/settings/bQ;

    invoke-virtual {v0}, Lcom/android/settings/bQ;->isCancelled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWy:Lcom/android/settings/bQ;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/bQ;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/settings/bQ;

    invoke-virtual {p0}, Lcom/android/settings/MaxAspectRatioSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/bQ;-><init>(Landroid/app/FragmentManager;)V

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWy:Lcom/android/settings/bQ;

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWy:Lcom/android/settings/bQ;

    invoke-virtual {v0, p0}, Lcom/android/settings/bQ;->bNz(Lcom/android/settings/MaxAspectRatioSettings;)V

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWy:Lcom/android/settings/bQ;

    const v1, 0x7f120a39

    invoke-virtual {v0, v1}, Lcom/android/settings/bQ;->setMessage(I)Lmiui/os/AsyncTaskWithProgress;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiui/os/AsyncTaskWithProgress;->setCancelable(Z)Lmiui/os/AsyncTaskWithProgress;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lmiui/os/AsyncTaskWithProgress;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic bQv(Lcom/android/settings/MaxAspectRatioSettings;)Lcom/android/settings/cN;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWu:Lcom/android/settings/cN;

    return-object v0
.end method

.method static synthetic bQw(Lcom/android/settings/MaxAspectRatioSettings;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWv:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic bQx(Lcom/android/settings/MaxAspectRatioSettings;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic bQy(Lcom/android/settings/MaxAspectRatioSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MaxAspectRatioSettings;->bQu()V

    return-void
.end method


# virtual methods
.method public bQs(Ljava/lang/String;)Lcom/android/settings/ce;
    .locals 1

    new-instance v0, Lcom/android/settings/ce;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/ce;-><init>(Lcom/android/settings/MaxAspectRatioSettings;Ljava/lang/String;)V

    return-object v0
.end method

.method public bQt(Landroid/content/pm/ApplicationInfo;ZI)Lcom/android/settings/ce;
    .locals 1

    new-instance v0, Lcom/android/settings/ce;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/settings/ce;-><init>(Lcom/android/settings/MaxAspectRatioSettings;Landroid/content/pm/ApplicationInfo;ZI)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/MaxAspectRatioSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/settings/cN;

    iget-object v1, p0, Lcom/android/settings/MaxAspectRatioSettings;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/cN;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWu:Lcom/android/settings/cN;

    invoke-virtual {p0}, Lcom/android/settings/MaxAspectRatioSettings;->bLS()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/MaxAspectRatioSettings;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWu:Lcom/android/settings/cN;

    invoke-virtual {v0}, Lcom/android/settings/cN;->stop()V

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    iput-object p1, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWv:Landroid/view/LayoutInflater;

    const v0, 0x7f0d00f1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    :cond_0
    return-object v1
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWy:Lcom/android/settings/bQ;

    invoke-virtual {v0}, Lcom/android/settings/bQ;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWy:Lcom/android/settings/bQ;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/bQ;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/MaxAspectRatioSettings;->bQu()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f0a0270

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWw:Landroid/widget/ListView;

    new-instance v0, Lcom/android/settings/cf;

    invoke-direct {v0, p0}, Lcom/android/settings/cf;-><init>(Lcom/android/settings/MaxAspectRatioSettings;)V

    iput-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWt:Lcom/android/settings/cf;

    iget-object v0, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWw:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/MaxAspectRatioSettings;->bWt:Lcom/android/settings/cf;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
