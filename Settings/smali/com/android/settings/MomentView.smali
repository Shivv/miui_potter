.class public Lcom/android/settings/MomentView;
.super Landroid/view/View;
.source "MomentView.java"


# instance fields
.field byD:Landroid/graphics/Bitmap;

.field byE:F

.field byF:F

.field byG:Landroid/graphics/Paint;

.field byH:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/MomentView;->bma()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/settings/MomentView;->bma()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/android/settings/MomentView;->bma()V

    return-void
.end method

.method private bma()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MomentView;->byG:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settings/MomentView;->byG:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/android/settings/MomentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080323

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MomentView;->byD:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/MomentView;->byD:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/android/settings/MomentView;->byE:F

    iget v1, p0, Lcom/android/settings/MomentView;->byF:F

    iget v2, p0, Lcom/android/settings/MomentView;->byH:F

    iget-object v3, p0, Lcom/android/settings/MomentView;->byG:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    div-int/lit8 v0, p1, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/android/settings/MomentView;->byE:F

    div-int/lit8 v0, p2, 0x2

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    iput v0, p0, Lcom/android/settings/MomentView;->byF:F

    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/android/settings/MomentView;->byH:F

    return-void
.end method

.method public setColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MomentView;->byG:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/android/settings/MomentView;->invalidate()V

    return-void
.end method
