.class public final Lcom/android/settings/bluetooth/DeviceProfilesSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "DeviceProfilesSettings.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/c;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final aES:Ljava/util/HashMap;

.field private aET:Lcom/android/settingslib/bluetooth/b;

.field private aEU:Landroid/preference/EditTextPreference;

.field private aEV:Landroid/app/AlertDialog;

.field private aEW:Lcom/android/settingslib/bluetooth/q;

.field private aEX:Landroid/preference/PreferenceGroup;

.field private aEY:Z

.field private aEZ:Lcom/android/settingslib/bluetooth/e;

.field private aFa:Lcom/android/settings/bluetooth/DeviceProfilesSettings$RenameEditTextPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aES:Ljava/util/HashMap;

    return-void
.end method

.method private asN()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciR()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    const-string/jumbo v2, "PBAP Server"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asP(Lcom/android/settingslib/bluetooth/f;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjf()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->cko()Lcom/android/settingslib/bluetooth/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/e;->ckc()Lcom/android/settingslib/bluetooth/F;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asP(Lcom/android/settingslib/bluetooth/f;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->cko()Lcom/android/settingslib/bluetooth/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/e;->cjW()Lcom/android/settingslib/bluetooth/H;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->cjw()I

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/H;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asP(Lcom/android/settingslib/bluetooth/f;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asW()V

    return-void
.end method

.method private asO(Landroid/content/Context;Lcom/android/settingslib/bluetooth/f;)V
    .locals 6

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v0, 0x7f1202a3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-interface {p2, v2}, Lcom/android/settingslib/bluetooth/f;->asm(Landroid/bluetooth/BluetoothDevice;)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f1202b9

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v0, v4, v2

    const v0, 0x7f1202b8

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;

    invoke-direct {v2, p0, v1, p2}, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;-><init>(Lcom/android/settings/bluetooth/DeviceProfilesSettings;Lcom/android/settingslib/bluetooth/b;Lcom/android/settingslib/bluetooth/f;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEV:Landroid/app/AlertDialog;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-static {p1, v1, v2, v3, v0}, Lcom/android/settings/bluetooth/Utils;->arr(Landroid/content/Context;Landroid/app/AlertDialog;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEV:Landroid/app/AlertDialog;

    return-void
.end method

.method private asP(Lcom/android/settingslib/bluetooth/f;)Landroid/preference/CheckBoxPreference;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/android/settingslib/bluetooth/f;->asm(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setPersistent(Z)V

    invoke-interface {p1}, Lcom/android/settingslib/bluetooth/f;->asn()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asR(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOrder(I)V

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->cja()Landroid/bluetooth/BluetoothClass;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/android/settingslib/bluetooth/f;->asl(Landroid/bluetooth/BluetoothClass;)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->ciA()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    invoke-direct {p0, v0, p1}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asU(Landroid/preference/CheckBoxPreference;Lcom/android/settingslib/bluetooth/f;)V

    return-object v0
.end method

.method private asQ(Landroid/preference/Preference;)Lcom/android/settingslib/bluetooth/f;
    .locals 3

    const/4 v2, 0x0

    instance-of v0, p1, Landroid/preference/CheckBoxPreference;

    if-nez v0, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object v2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEZ:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/e;->ckd(Ljava/lang/String;)Lcom/android/settingslib/bluetooth/f;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    return-object v2
.end method

.method private asR(I)I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->getOrder()I

    move-result v0

    mul-int/lit8 v1, p1, 0xa

    add-int/2addr v0, v1

    return v0
.end method

.method private asS(Lcom/android/settingslib/bluetooth/f;Landroid/preference/CheckBoxPreference;)V
    .locals 6

    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    const-string/jumbo v0, "PBAP Server"

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjf()I

    move-result v0

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iget-object v5, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v5, v0}, Lcom/android/settingslib/bluetooth/b;->ciO(I)V

    if-ne v0, v2, :cond_2

    :goto_1
    invoke-virtual {p2, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->cko()Lcom/android/settingslib/bluetooth/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/e;->ckc()Lcom/android/settingslib/bluetooth/F;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/settingslib/bluetooth/F;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v2

    if-ne v2, v1, :cond_3

    invoke-virtual {v0, v4}, Lcom/android/settingslib/bluetooth/F;->asj(Landroid/bluetooth/BluetoothDevice;)Z

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    :cond_3
    if-nez v2, :cond_0

    invoke-virtual {v0, v4}, Lcom/android/settingslib/bluetooth/F;->asi(Landroid/bluetooth/BluetoothDevice;)Z

    goto :goto_2

    :cond_4
    invoke-interface {p1, v4}, Lcom/android/settingslib/bluetooth/f;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asO(Landroid/content/Context;Lcom/android/settingslib/bluetooth/f;)V

    :goto_4
    return-void

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    instance-of v0, p1, Lcom/android/settingslib/bluetooth/H;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/bluetooth/b;->cjq(I)V

    :cond_7
    invoke-interface {p1, v4}, Lcom/android/settingslib/bluetooth/f;->asr(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_9

    instance-of v0, p1, Lcom/android/settingslib/bluetooth/y;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/b;->cjd(Lcom/android/settingslib/bluetooth/f;)V

    :goto_5
    invoke-direct {p0, p2, p1}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asU(Landroid/preference/CheckBoxPreference;Lcom/android/settingslib/bluetooth/f;)V

    goto :goto_4

    :cond_8
    invoke-interface {p1, v4, v3}, Lcom/android/settingslib/bluetooth/f;->ast(Landroid/bluetooth/BluetoothDevice;Z)V

    goto :goto_5

    :cond_9
    invoke-interface {p1, v4, v2}, Lcom/android/settingslib/bluetooth/f;->ast(Landroid/bluetooth/BluetoothDevice;Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/b;->cjd(Lcom/android/settingslib/bluetooth/f;)V

    goto :goto_5
.end method

.method private asT()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asV()V

    return-void
.end method

.method private asU(Landroid/preference/CheckBoxPreference;Lcom/android/settingslib/bluetooth/f;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v3}, Lcom/android/settingslib/bluetooth/b;->ciA()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {p1, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    instance-of v3, p2, Lcom/android/settingslib/bluetooth/H;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v3}, Lcom/android/settingslib/bluetooth/b;->cjw()I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :goto_1
    invoke-interface {p2, v2}, Lcom/android/settingslib/bluetooth/f;->aso(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    instance-of v3, p2, Lcom/android/settingslib/bluetooth/F;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v3}, Lcom/android/settingslib/bluetooth/b;->cjf()I

    move-result v3

    if-ne v3, v0, :cond_2

    :goto_2
    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    instance-of v3, p2, Lcom/android/settingslib/bluetooth/y;

    if-eqz v3, :cond_5

    invoke-interface {p2, v2}, Lcom/android/settingslib/bluetooth/f;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    :goto_3
    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    invoke-interface {p2, v2}, Lcom/android/settingslib/bluetooth/f;->asr(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1
.end method

.method private asV()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciR()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asP(Lcom/android/settingslib/bluetooth/f;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asU(Landroid/preference/CheckBoxPreference;Lcom/android/settingslib/bluetooth/f;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjr()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    const-string/jumbo v2, "PBAP Server"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string/jumbo v3, "DeviceProfilesSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Removing "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, " from profile list"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asW()V

    return-void
.end method

.method private asW()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    iget-boolean v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEY:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEY:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEY:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iput-boolean v2, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEY:Z

    goto :goto_0
.end method

.method private asX()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjl()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckm()Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/t;->ckK(Lcom/android/settingslib/bluetooth/b;)V

    return-void
.end method

.method static synthetic asY(Lcom/android/settings/bluetooth/DeviceProfilesSettings;)Landroid/preference/EditTextPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method static synthetic asZ(Lcom/android/settings/bluetooth/DeviceProfilesSettings;Landroid/preference/CheckBoxPreference;Lcom/android/settingslib/bluetooth/f;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asU(Landroid/preference/CheckBoxPreference;Lcom/android/settingslib/bluetooth/f;)V

    return-void
.end method


# virtual methods
.method public arT()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asT()V

    return-void
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "device"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    move-object v1, v0

    :goto_0
    const v0, 0x7f15002a

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->setOrderingAsAdded(Z)V

    const-string/jumbo v0, "profile_container"

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEX:Landroid/preference/PreferenceGroup;

    const-string/jumbo v0, "rename_device"

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    if-nez v1, :cond_1

    const-string/jumbo v0, "DeviceProfilesSettings"

    const-string/jumbo v1, "Activity started without a remote Bluetooth device"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->finish()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "device"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    move-object v1, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$RenameEditTextPreference;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/bluetooth/DeviceProfilesSettings$RenameEditTextPreference;-><init>(Lcom/android/settings/bluetooth/DeviceProfilesSettings;Lcom/android/settings/bluetooth/DeviceProfilesSettings$RenameEditTextPreference;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aFa:Lcom/android/settings/bluetooth/DeviceProfilesSettings$RenameEditTextPreference;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/bluetooth/Utils;->arq(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/q;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEW:Lcom/android/settingslib/bluetooth/q;

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckm()Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/q;->cko()Lcom/android/settingslib/bluetooth/e;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEZ:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    if-nez v0, :cond_2

    const-string/jumbo v0, "DeviceProfilesSettings"

    const-string/jumbo v1, "Device not found, cannot connect to it"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->finish()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    new-instance v1, Lcom/android/settings/bluetooth/DeviceProfilesSettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings$1;-><init>(Lcom/android/settings/bluetooth/DeviceProfilesSettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-direct {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asN()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEV:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEV:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEV:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/b;->ciz(Lcom/android/settingslib/bluetooth/c;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEW:Lcom/android/settingslib/bluetooth/q;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/q;->ckp(Landroid/content/Context;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/android/settingslib/bluetooth/b;->ciT(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    instance-of v0, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asQ(Landroid/preference/Preference;)Lcom/android/settingslib/bluetooth/f;

    move-result-object v0

    check-cast p1, Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asS(Lcom/android/settingslib/bluetooth/f;Landroid/preference/CheckBoxPreference;)V

    return v1

    :cond_1
    return v1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unpair"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asX()V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->finish()V

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/settingslib/bluetooth/q;->ckp(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/b;->ciM(Lcom/android/settingslib/bluetooth/c;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    const/16 v2, 0xa

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asT()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aFa:Lcom/android/settings/bluetooth/DeviceProfilesSettings$RenameEditTextPreference;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aEU:Landroid/preference/EditTextPreference;

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    instance-of v3, v0, Landroid/app/AlertDialog;

    if-eqz v3, :cond_1

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "device"

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->aET:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
