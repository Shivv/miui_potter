.class public Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;
.super Lcom/android/settings/MiuiRestrictedSettingsFragment;
.source "MiuiMiscBtListFragment.java"


# instance fields
.field private aDB:Lcom/android/settingslib/bluetooth/q;

.field private aDC:Lmiui/bluetooth/ble/MiBleDeviceManager;

.field private aDD:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDD:Z

    return-void
.end method

.method private arv()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDB:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckm()Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/t;->ckU()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->arw(Lcom/android/settingslib/bluetooth/b;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic ary(Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDD:Z

    return p1
.end method


# virtual methods
.method arw(Lcom/android/settingslib/bluetooth/b;)V
    .locals 2

    invoke-static {p1}, Lcom/android/settings/bluetooth/MiuiBTUtils;->asK(Lcom/android/settingslib/bluetooth/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/b;)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method protected arx()V
    .locals 6

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    instance-of v0, v1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arP()Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    iget-boolean v3, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDD:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDC:Lmiui/bluetooth/ble/MiBleDeviceManager;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDC:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/bluetooth/ble/MiBleDeviceManager;->getDeviceType(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/android/settings/bluetooth/MiuiBluetoothDevicePreference;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDC:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-direct {v3, v4, v0, v5}, Lcom/android/settings/bluetooth/MiuiBluetoothDevicePreference;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/b;Lmiui/bluetooth/ble/MiBleDeviceManager;)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/bluetooth/Utils;->arq(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/q;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDB:Lcom/android/settingslib/bluetooth/q;

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDB:Lcom/android/settingslib/bluetooth/q;

    if-nez v0, :cond_0

    const-string/jumbo v0, "MiuiMiscBtListFragment"

    const-string/jumbo v1, "Bluetooth is not supported on this device"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment$1;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment$1;-><init>(Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;)V

    invoke-static {v0, v1}, Lmiui/bluetooth/ble/MiBleDeviceManager;->createManager(Landroid/content/Context;Lmiui/bluetooth/ble/MiBleDeviceManager$MiBleDeviceManagerListener;)Lmiui/bluetooth/ble/MiBleDeviceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDC:Lmiui/bluetooth/ble/MiBleDeviceManager;

    const v0, 0x7f150045

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOrderingAsAdded(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDC:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {v0}, Lmiui/bluetooth/ble/MiBleDeviceManager;->close()V

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDB:Lcom/android/settingslib/bluetooth/q;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/q;->ckp(Landroid/content/Context;)V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    const/4 v3, 0x1

    instance-of v0, p2, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    if-eqz v0, :cond_1

    instance-of v0, p2, Lcom/android/settings/bluetooth/MiuiBluetoothDevicePreference;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/android/settings/bluetooth/MiuiBluetoothDevicePreference;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/MiuiBluetoothDevicePreference;->arP()Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDC:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/bluetooth/ble/MiBleDeviceManager;->getDeviceType(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/bluetooth/MiuiBTUtils;->asL(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V

    return v3

    :cond_0
    check-cast p2, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    invoke-virtual {p2}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arQ()V

    return v3

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->aDB:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/q;->ckp(Landroid/content/Context;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onStart()V

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->arv()V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->arx()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onStop()V

    return-void
.end method
