.class public Lcom/android/settings/bluetooth/BluetoothDevicePreference;
.super Landroid/preference/Preference;
.source "BluetoothDevicePreference.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/c;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static aEj:I


# instance fields
.field public final aDX:Ljava/lang/String;

.field protected aDY:Lcom/android/settingslib/bluetooth/b;

.field public final aDZ:Ljava/lang/String;

.field public final aEa:Ljava/lang/String;

.field public final aEb:Ljava/lang/String;

.field public final aEc:Ljava/lang/String;

.field public final aEd:Ljava/lang/String;

.field public final aEe:Ljava/lang/String;

.field private aEf:Ljava/lang/String;

.field private aEg:Landroid/app/AlertDialog;

.field private aEh:Landroid/view/View$OnClickListener;

.field aEi:Landroid/content/res/Resources;

.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, -0x80000000

    sput v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEj:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/b;)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEf:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEi:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEi:Landroid/content/res/Resources;

    const v1, 0x7f120340

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDZ:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEi:Landroid/content/res/Resources;

    const v1, 0x7f120344

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEd:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEi:Landroid/content/res/Resources;

    const v1, 0x7f120342

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEb:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEi:Landroid/content/res/Resources;

    const v1, 0x7f120345

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEe:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEi:Landroid/content/res/Resources;

    const v1, 0x7f120343

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEc:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEi:Landroid/content/res/Resources;

    const v1, 0x7f120341

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEa:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEi:Landroid/content/res/Resources;

    const v1, 0x7f12033f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDX:Ljava/lang/String;

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->mUserManager:Landroid/os/UserManager;

    sget v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEj:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010033

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEj:I

    :cond_0
    iput-object p2, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/b;->ciM(Lcom/android/settingslib/bluetooth/c;)V

    const v0, 0x7f0d0126

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setLayoutResource(I)V

    invoke-virtual {p2}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    const-string/jumbo v1, "no_config_bluetooth"

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0d0124

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setWidgetLayoutResource(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arT()V

    return-void
.end method

.method private arU()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v0, 0x7f1202a3

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const v0, 0x7f1202bc

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f1202c1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/settings/bluetooth/BluetoothDevicePreference$1;

    invoke-direct {v3, p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference$1;-><init>(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V

    iget-object v4, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEg:Landroid/app/AlertDialog;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-static {v1, v4, v3, v2, v0}, Lcom/android/settings/bluetooth/Utils;->arr(Landroid/content/Context;Landroid/app/AlertDialog;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEg:Landroid/app/AlertDialog;

    return-void
.end method

.method private arV(Lcom/android/settingslib/bluetooth/b;)I
    .locals 4

    const/4 v3, 0x1

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cja()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    if-nez v0, :cond_0

    return v3

    :cond_0
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->cja()Landroid/bluetooth/BluetoothClass;

    move-result-object v1

    if-nez v1, :cond_1

    return v2

    :cond_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    return v0

    :cond_2
    if-nez v0, :cond_3

    return v3

    :cond_3
    if-nez v1, :cond_4

    return v2

    :cond_4
    invoke-static {v0, v1}, Lcom/android/internal/util/CharSequences;->compareToIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    return v0
.end method

.method private arW(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lmiui/text/ChinesePinyinConverter;->getInstance()Lmiui/text/ChinesePinyinConverter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/text/ChinesePinyinConverter;->get(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/text/ChinesePinyinConverter$Token;

    iget-object v0, v0, Lmiui/text/ChinesePinyinConverter$Token;->target:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private arX()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciK()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f1202fa

    invoke-static {v0, v1, v2}, Lcom/android/settings/bluetooth/Utils;->ars(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method arP()Lcom/android/settingslib/bluetooth/b;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    return-object v0
.end method

.method arQ()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/b;->ciC()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v2, v3, [Landroid/util/Pair;

    const/16 v3, 0x364

    invoke-virtual {v1, v0, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arU()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v2, 0xc

    if-ne v0, v2, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v2, v3, [Landroid/util/Pair;

    const/16 v3, 0x363

    invoke-virtual {v1, v0, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/b;->ciQ(Z)V

    goto :goto_0

    :cond_2
    const/16 v2, 0xa

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v2, v3, [Landroid/util/Pair;

    const/16 v3, 0x362

    invoke-virtual {v1, v0, v3, v2}, Lcom/android/settings/core/instrumentation/e;->ajS(Landroid/content/Context;I[Landroid/util/Pair;)V

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arX()V

    goto :goto_0
.end method

.method arR(Lcom/android/settingslib/bluetooth/b;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    return-void
.end method

.method arS()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/b;->ciz(Lcom/android/settingslib/bluetooth/c;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/b;->ciM(Lcom/android/settingslib/bluetooth/c;)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arT()V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->notifyChanged()V

    return-void
.end method

.method public arT()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciJ()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setSummary(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aru()Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setIcon(I)V

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEf:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciA()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->notifyHierarchyChanged()V

    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public arY(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEh:Landroid/view/View$OnClickListener;

    return-void
.end method

.method protected aru()Landroid/util/Pair;
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cja()Landroid/bluetooth/BluetoothClass;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->cjp()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/f;

    invoke-interface {v0, v1}, Lcom/android/settingslib/bluetooth/f;->asl(Landroid/bluetooth/BluetoothClass;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v0, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    :sswitch_0
    new-instance v0, Landroid/util/Pair;

    const v1, 0x7f0801ab

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDZ:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :sswitch_1
    new-instance v0, Landroid/util/Pair;

    const v1, 0x7f0801a5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEe:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :sswitch_2
    new-instance v0, Landroid/util/Pair;

    invoke-static {v1}, Lcom/android/settingslib/bluetooth/A;->clr(Landroid/bluetooth/BluetoothClass;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEd:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :sswitch_3
    new-instance v0, Landroid/util/Pair;

    const v1, 0x7f0801a9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEc:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_1
    const-string/jumbo v0, "BluetoothDevicePreference"

    const-string/jumbo v2, "mBtClass is null"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_4

    invoke-virtual {v1, v4}, Landroid/bluetooth/BluetoothClass;->doesClassMatch(I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/util/Pair;

    const v1, 0x7f0801a8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEb:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_3
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothClass;->doesClassMatch(I)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Landroid/util/Pair;

    const v1, 0x7f0801a7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEa:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_4
    new-instance v0, Landroid/util/Pair;

    const v1, 0x7f0801a4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDX:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
        0x500 -> :sswitch_2
        0x600 -> :sswitch_3
    .end sparse-switch
.end method

.method public compareTo(Landroid/preference/Preference;)I
    .locals 2

    instance-of v0, p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/android/settings/bluetooth/MiuiMiscBluetoothPreference;

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/Preference;->compareTo(Landroid/preference/Preference;)I

    move-result v0

    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getOrder()I

    move-result v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getOrder()I

    move-result v1

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getOrder()I

    move-result v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getOrder()I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    :cond_2
    check-cast p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    iget-object v0, p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arV(Lcom/android/settingslib/bluetooth/b;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    check-cast p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    iget-object v1, p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->hashCode()I

    move-result v0

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 4

    const/16 v2, 0xc

    const-string/jumbo v0, "bt_checkbox"

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->findPreferenceInHierarchy(Ljava/lang/String;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "bt_checkbox"

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setDependency(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    if-ne v0, v2, :cond_1

    const v0, 0x7f0a0138

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    :cond_1
    const v0, 0x1020006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEf:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    if-ne v0, v2, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/view/View;->setPadding(IIII)V

    :cond_3
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEh:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEh:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method protected onPrepareForRemoval()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/preference/Preference;->onPrepareForRemoval()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/b;->ciz(Lcom/android/settingslib/bluetooth/c;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aEg:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method public setOrder(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->aDY:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    add-int/lit8 p1, p1, -0x64

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/Preference;->setOrder(I)V

    return-void
.end method
