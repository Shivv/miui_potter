.class public Lcom/android/settings/bluetooth/MiuiBluetoothSettings;
.super Lcom/android/settings/bluetooth/BluetoothSettings;
.source "MiuiBluetoothSettings.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private aCE:I

.field private aCF:Landroid/preference/Preference$OnPreferenceClickListener;

.field private aCG:Lmiui/preference/ValuePreference;

.field private aCH:Lcom/android/settings/bluetooth/GattProfile;

.field aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

.field aCJ:Z

.field private aCK:Ljava/util/List;

.field private aCL:Lcom/android/settings/aD;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BluetoothSettings;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCJ:Z

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCE:I

    new-instance v0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings$1;-><init>(Lcom/android/settings/bluetooth/MiuiBluetoothSettings;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCF:Landroid/preference/Preference$OnPreferenceClickListener;

    return-void
.end method

.method static synthetic aqA(Lcom/android/settings/bluetooth/MiuiBluetoothSettings;)Lcom/android/settings/bluetooth/GattProfile;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;

    return-object v0
.end method

.method private aqw()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/bluetooth/d;->cjC(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCG:Lmiui/preference/ValuePreference;

    invoke-virtual {v1, v0}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method private aqx()I
    .locals 4

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckm()Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/t;->ckU()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/b;

    invoke-static {v0}, Lcom/android/settings/bluetooth/MiuiBTUtils;->asK(Lcom/android/settingslib/bluetooth/b;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v2

    const/16 v3, 0xc

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method aqn()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/GattProfile;->asd()V

    invoke-super {p0}, Lcom/android/settings/bluetooth/BluetoothSettings;->aqn()V

    return-void
.end method

.method aqo()V
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/bluetooth/BluetoothSettings;->aqo()V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string/jumbo v0, "bluetooth_enable"

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEl:Landroid/preference/CheckBoxPreference;

    new-instance v0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;

    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEl:Landroid/preference/CheckBoxPreference;

    invoke-direct {v0, v2, v3}, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;-><init>(Landroid/content/Context;Landroid/preference/CheckBoxPreference;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEm:Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;

    const-string/jumbo v0, "bluetooth_device_name_edit"

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCG:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCG:Lmiui/preference/ValuePreference;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCG:Lmiui/preference/ValuePreference;

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCF:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v2}, Lmiui/preference/ValuePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCG:Lmiui/preference/ValuePreference;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->bXB()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lmiui/preference/ValuePreference;->setEnabled(Z)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method aqp(Lcom/android/settingslib/bluetooth/b;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCX:Landroid/preference/PreferenceGroup;

    instance-of v0, v0, Landroid/preference/PreferenceScreen;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCJ:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/bluetooth/ble/MiBleDeviceManager;->getDeviceType(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->bWH(Ljava/lang/String;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEn:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    new-instance v0, Lcom/android/settings/bluetooth/MiuiBluetoothDevicePreference;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-direct {v0, v2, p1, v3}, Lcom/android/settings/bluetooth/MiuiBluetoothDevicePreference;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/b;Lmiui/bluetooth/ble/MiBleDeviceManager;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/MiuiBluetoothDevicePreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqq(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCY:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSettings;->aqp(Lcom/android/settingslib/bluetooth/b;)V

    goto :goto_0
.end method

.method aqq(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V
    .locals 3

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arP()Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/bluetooth/ble/MiBleDeviceManager;->getDeviceType(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/settings/bluetooth/GattProfile;->asg(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0124

    invoke-virtual {p1, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setWidgetLayoutResource(I)V

    :cond_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSettings;->aqq(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V

    return-void
.end method

.method public aqr(I)V
    .locals 1

    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqz(Z)V

    :cond_0
    const/16 v0, 0xc

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/GattProfile;->asd()V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqy(I)V

    return-void
.end method

.method public aqs(Lcom/android/settingslib/bluetooth/b;)V
    .locals 4

    const/16 v3, 0xc

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCY:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    if-eq v0, v3, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/GattProfile;->asf(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCJ:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/bluetooth/ble/MiBleDeviceManager;->getDeviceType(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    if-eqz v0, :cond_5

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothClass;->doesClassMatch(I)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "HID over BLE device found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCZ:Lcom/android/settingslib/bluetooth/h;

    sget-object v1, Lcom/android/settingslib/bluetooth/g;->cEl:Lcom/android/settingslib/bluetooth/h;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/GattProfile;->asg(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqp(Lcom/android/settingslib/bluetooth/b;)V

    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settings/bluetooth/GattProfile;->asg(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Bonded BLE device found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unknown ble device found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_5
    sget-object v0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "BLE device without bt class found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->cjc()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_6
    invoke-static {p1}, Lcom/android/settings/bluetooth/MiuiBTUtils;->asK(Lcom/android/settingslib/bluetooth/b;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciP()I

    move-result v0

    if-eq v0, v3, :cond_8

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    check-cast v0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aqR(I)V

    :cond_7
    return-void

    :cond_8
    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSettings;->aqs(Lcom/android/settingslib/bluetooth/b;)V

    return-void
.end method

.method public aqt(Lcom/android/settingslib/bluetooth/b;I)V
    .locals 2

    iput p2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCE:I

    const/16 v0, 0xc

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqI(Landroid/preference/PreferenceGroup;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqJ(Lcom/android/settingslib/bluetooth/b;)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEn:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEn:Landroid/preference/PreferenceGroup;

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqI(Landroid/preference/PreferenceGroup;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqp(Lcom/android/settingslib/bluetooth/b;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0xa

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCY:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEn:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEn:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-gtz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEn:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqI(Landroid/preference/PreferenceGroup;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqs(Lcom/android/settingslib/bluetooth/b;)V

    goto :goto_0
.end method

.method aqu(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCJ:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arP()Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/bluetooth/ble/MiBleDeviceManager;->getDeviceType(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settings/bluetooth/MiuiBTUtils;->asL(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;)V

    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSettings;->aqu(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V

    return-void
.end method

.method public aqv(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSettings;->aqv(Z)V

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    check-cast v0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aqR(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method protected aqy(I)V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqw()V

    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSettings;->aqy(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    check-cast v0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqx()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aqR(I)V

    :cond_0
    return-void
.end method

.method protected aqz(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCL:Lcom/android/settings/aD;

    invoke-virtual {v0}, Lcom/android/settings/aD;->bmR()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCL:Lcom/android/settings/aD;

    invoke-virtual {v0}, Lcom/android/settings/aD;->bmS()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    new-instance v0, Lcom/android/settings/aD;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/aD;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCL:Lcom/android/settings/aD;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/MiuiBluetoothSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings$2;-><init>(Lcom/android/settings/bluetooth/MiuiBluetoothSettings;)V

    invoke-static {v0, v1}, Lmiui/bluetooth/ble/MiBleDeviceManager;->createManager(Landroid/content/Context;Lmiui/bluetooth/ble/MiBleDeviceManager$MiBleDeviceManagerListener;)Lmiui/bluetooth/ble/MiBleDeviceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSettings;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/settings/bluetooth/GattProfile;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCV:Lcom/android/settingslib/bluetooth/d;

    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v3}, Lcom/android/settingslib/bluetooth/q;->ckm()Lcom/android/settingslib/bluetooth/t;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v4}, Lcom/android/settingslib/bluetooth/q;->cko()Lcom/android/settingslib/bluetooth/e;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/bluetooth/GattProfile;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;Lmiui/bluetooth/ble/MiBleDeviceManager;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCV:Lcom/android/settingslib/bluetooth/d;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->bXB()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    const/16 v3, 0xc

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_0
    const v3, 0x7f12032c

    invoke-interface {p1, v2, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCL:Lcom/android/settings/aD;

    invoke-virtual {v4}, Lcom/android/settings/aD;->buX()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    if-nez v0, :cond_2

    invoke-virtual {p0, v2}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqz(Z)V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCI:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {v0}, Lmiui/bluetooth/ble/MiBleDeviceManager;->close()V

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/GattProfile;->ase()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCH:Lcom/android/settings/bluetooth/GattProfile;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-super {p0}, Lcom/android/settings/bluetooth/BluetoothSettings;->onDestroy()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSettings;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    iget v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCE:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "Cannot start scanning since device is in bonding state."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 7

    const/4 v2, 0x0

    const-string/jumbo v0, "bluetooth_enable"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEm:Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;

    move-object v0, p2

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aqi(Z)V

    :cond_0
    instance-of v0, p2, Lcom/android/settings/bluetooth/MiuiMiscBluetoothPreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v1, Lcom/android/settings/bluetooth/MiuiMiscBtListFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f1202b2

    const/4 v6, 0x0

    move-object v4, v2

    move-object v5, v2

    invoke-static/range {v0 .. v6}, Lcom/android/settings/dc;->bYv(Landroid/app/Activity;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/settings/bluetooth/BluetoothSettings;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/bluetooth/BluetoothSettings;->onStart()V

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCE:I

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aCK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    check-cast v0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aqx()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aqR(I)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/bluetooth/BluetoothSettings;->onStop()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothSettings;->aEk:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->removeAll()V

    :cond_0
    return-void
.end method
