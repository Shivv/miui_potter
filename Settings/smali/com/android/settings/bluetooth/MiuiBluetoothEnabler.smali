.class public final Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;
.super Ljava/lang/Object;
.source "MiuiBluetoothEnabler.java"


# instance fields
.field private final aCB:Landroid/content/IntentFilter;

.field private final aCC:Lcom/android/settingslib/bluetooth/d;

.field private aCD:Landroid/preference/CheckBoxPreference;

.field private final mContext:Landroid/content/Context;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/preference/CheckBoxPreference;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler$1;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler$1;-><init>(Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p2}, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aql(Landroid/preference/CheckBoxPreference;)V

    invoke-static {p1}, Lcom/android/settings/bluetooth/Utils;->arq(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/q;

    move-result-object v0

    if-nez v0, :cond_0

    iput-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCC:Lcom/android/settingslib/bluetooth/d;

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :goto_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCB:Landroid/content/IntentFilter;

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckl()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCC:Lcom/android/settingslib/bluetooth/d;

    goto :goto_0
.end method

.method private aqj(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic aqm(Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aqj(I)V

    return-void
.end method

.method private maybeEnforceRestrictions()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "no_bluetooth"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "no_config_bluetooth"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    :cond_1
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public aqi(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->maybeEnforceRestrictions()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCC:Lcom/android/settingslib/bluetooth/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCC:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/d;->cjy(Z)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_2
    return-void
.end method

.method public aqk()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCC:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCC:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aqj(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCB:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public aql(Landroid/preference/CheckBoxPreference;)V
    .locals 5

    const/16 v4, 0xa

    const/4 v0, 0x0

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCC:Lcom/android/settingslib/bluetooth/d;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCC:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v2

    move v3, v2

    :goto_0
    const/16 v2, 0xc

    if-ne v3, v2, :cond_3

    move v2, v1

    :goto_1
    if-ne v3, v4, :cond_0

    move v0, v1

    :cond_0
    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCD:Landroid/preference/CheckBoxPreference;

    if-nez v2, :cond_1

    move v1, v0

    :cond_1
    invoke-virtual {v3, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    return-void

    :cond_2
    move v3, v4

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->aCC:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothEnabler;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
