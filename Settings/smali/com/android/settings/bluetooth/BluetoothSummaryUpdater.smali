.class public final Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;
.super Lcom/android/settings/widget/w;
.source "BluetoothSummaryUpdater.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/a;


# instance fields
.field private final aDE:Lcom/android/settingslib/bluetooth/d;

.field private final aDF:Lcom/android/settingslib/bluetooth/q;

.field private aDG:I

.field private mEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/widget/x;Lcom/android/settingslib/bluetooth/q;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/w;-><init>(Landroid/content/Context;Lcom/android/settings/widget/x;)V

    iput-object p3, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDF:Lcom/android/settingslib/bluetooth/q;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDF:Lcom/android/settingslib/bluetooth/q;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDF:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckl()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDE:Lcom/android/settingslib/bluetooth/d;

    return-void
.end method

.method private arA()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDE:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDE:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjD()I

    move-result v0

    iget v2, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDG:I

    if-eq v0, v2, :cond_1

    iput v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDG:I

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->arz()Ljava/util/Collection;

    move-result-object v0

    if-nez v0, :cond_2

    iput v4, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDG:I

    return-void

    :cond_2
    iget v2, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDG:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciC()Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_0
    if-nez v0, :cond_4

    iput v4, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDG:I

    :cond_4
    return-void

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method private arz()Ljava/util/Collection;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDF:Lcom/android/settingslib/bluetooth/q;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDF:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckm()Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/t;->ckU()Ljava/util/Collection;

    move-result-object v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public aib(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDE:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDE:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->isEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mEnabled:Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDE:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjD()I

    move-result v0

    iput v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDG:I

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aBK()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDF:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckn()Lcom/android/settingslib/bluetooth/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/s;->ckt(Lcom/android/settingslib/bluetooth/a;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDF:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckn()Lcom/android/settingslib/bluetooth/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/s;->ckw(Lcom/android/settingslib/bluetooth/a;)V

    goto :goto_0
.end method

.method public aqJ(Lcom/android/settingslib/bluetooth/b;)V
    .locals 0

    return-void
.end method

.method public aqK(Lcom/android/settingslib/bluetooth/b;I)V
    .locals 0

    iput p2, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDG:I

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->arA()V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aBK()V

    return-void
.end method

.method public aqr(I)V
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0xc

    if-eq p1, v1, :cond_0

    const/16 v1, 0xb

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mEnabled:Z

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aBK()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aqs(Lcom/android/settingslib/bluetooth/b;)V
    .locals 0

    return-void
.end method

.method public aqt(Lcom/android/settingslib/bluetooth/b;I)V
    .locals 0

    return-void
.end method

.method public aqv(Z)V
    .locals 0

    return-void
.end method

.method getConnectedDeviceSummary()Ljava/lang/String;
    .locals 7

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x1

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDE:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/d;->cjB()Ljava/util/Set;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    return-object v0

    :cond_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    move-object v3, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v0, v1, 0x1

    if-le v0, v6, :cond_2

    :goto_1
    if-le v0, v6, :cond_3

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mContext:Landroid/content/Context;

    const v1, 0x7f120298

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_2
    move-object v1, v3

    :goto_3
    move-object v3, v1

    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mContext:Landroid/content/Context;

    new-array v1, v6, [Ljava/lang/Object;

    aput-object v3, v1, v2

    const v2, 0x7f12029d

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    move v0, v1

    move-object v1, v3

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public getSummary()Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mEnabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mContext:Landroid/content/Context;

    const v1, 0x7f1202ba

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aDG:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mContext:Landroid/content/Context;

    const v1, 0x7f1205e0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->getConnectedDeviceSummary()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mContext:Landroid/content/Context;

    const v1, 0x7f12029e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mContext:Landroid/content/Context;

    const v1, 0x7f1202c3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
