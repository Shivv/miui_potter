.class public Lcom/android/settings/bluetooth/GattProfile;
.super Landroid/bluetooth/BluetoothGattCallback;
.source "GattProfile.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/f;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private aEA:Ljava/util/HashMap;

.field private aEB:Landroid/os/Handler;

.field private final aEC:Lcom/android/settingslib/bluetooth/d;

.field private aED:Landroid/security/MiuiLockPatternUtils;

.field private aEE:Lmiui/bluetooth/ble/MiBleDeviceManager;

.field private aEF:Landroid/database/ContentObserver;

.field private final aEG:Lcom/android/settingslib/bluetooth/e;

.field private final aEH:Landroid/net/Uri;

.field private aEx:Landroid/bluetooth/BluetoothManager;

.field private aEy:Ljava/util/List;

.field private final aEz:Lcom/android/settingslib/bluetooth/t;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/bluetooth/GattProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/bluetooth/GattProfile;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/t;Lcom/android/settingslib/bluetooth/e;Lmiui/bluetooth/ble/MiBleDeviceManager;)V
    .locals 4

    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEy:Ljava/util/List;

    new-instance v0, Lcom/android/settings/bluetooth/GattProfile$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/bluetooth/GattProfile$1;-><init>(Lcom/android/settings/bluetooth/GattProfile;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEF:Landroid/database/ContentObserver;

    iput-object p1, p0, Lcom/android/settings/bluetooth/GattProfile;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/bluetooth/GattProfile;->aEC:Lcom/android/settingslib/bluetooth/d;

    iput-object p3, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    iput-object p4, p0, Lcom/android/settings/bluetooth/GattProfile;->aEG:Lcom/android/settingslib/bluetooth/e;

    const-string/jumbo v0, "bluetooth"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    iput-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEx:Landroid/bluetooth/BluetoothManager;

    iput-object p5, p0, Lcom/android/settings/bluetooth/GattProfile;->aEE:Lmiui/bluetooth/ble/MiBleDeviceManager;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEA:Ljava/util/HashMap;

    new-instance v0, Landroid/security/MiuiLockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/bluetooth/GattProfile;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aED:Landroid/security/MiuiLockPatternUtils;

    const-string/jumbo v0, "content://com.android.bluetooth.ble.settingsprovider/devices"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEH:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/GattProfile;->asd()V

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/GattProfile;->aEH:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/settings/bluetooth/GattProfile;->aEF:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEB:Landroid/os/Handler;

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/settings/bluetooth/GattProfile;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "registerContentObserver failed: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static ash(Landroid/bluetooth/BluetoothGatt;)Z
    .locals 4

    const/4 v3, 0x0

    if-nez p0, :cond_0

    return v3

    :cond_0
    :try_start_0
    const-class v1, Landroid/bluetooth/BluetoothGatt;

    const-string/jumbo v2, "refresh"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return v3
.end method

.method static synthetic asu(Lcom/android/settings/bluetooth/GattProfile;)Lcom/android/settingslib/bluetooth/t;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized asd()V
    .locals 8

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aED:Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v0}, Landroid/security/MiuiLockPatternUtils;->getBluetoothAddressToUnlock()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    invoke-virtual {v3, v1}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/android/settings/bluetooth/GattProfile;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "GattProfile get bluetooth unlock device: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    iget-object v4, p0, Lcom/android/settings/bluetooth/GattProfile;->aEC:Lcom/android/settingslib/bluetooth/d;

    iget-object v5, p0, Lcom/android/settings/bluetooth/GattProfile;->aEG:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {v3, v4, v5, v1}, Lcom/android/settingslib/bluetooth/t;->ckP(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/e;Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->cjn()V

    :cond_0
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEE:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {v0}, Lmiui/bluetooth/ble/MiBleDeviceManager;->getBoundDevices()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    iget-object v1, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    invoke-virtual {v1, v4}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/android/settings/bluetooth/GattProfile;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "GattProfile found new device: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    iget-object v5, p0, Lcom/android/settings/bluetooth/GattProfile;->aEC:Lcom/android/settingslib/bluetooth/d;

    iget-object v6, p0, Lcom/android/settings/bluetooth/GattProfile;->aEG:Lcom/android/settingslib/bluetooth/e;

    invoke-virtual {v1, v5, v6, v4}, Lcom/android/settingslib/bluetooth/t;->ckP(Lcom/android/settingslib/bluetooth/d;Lcom/android/settingslib/bluetooth/e;Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v1

    :cond_2
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    iget-object v5, p0, Lcom/android/settings/bluetooth/GattProfile;->aEE:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-virtual {v5, v0}, Lmiui/bluetooth/ble/MiBleDeviceManager;->getScanResult(Ljava/lang/String;)Lmiui/bluetooth/ble/ScanResult;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lmiui/bluetooth/ble/ScanResult;->getScanRecord()Lmiui/bluetooth/ble/ScanRecord;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_4

    :try_start_3
    invoke-virtual {v0}, Lmiui/bluetooth/ble/ScanRecord;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v1, v0}, Lcom/android/settingslib/bluetooth/b;->ciT(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    :goto_2
    :try_start_4
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->cjn()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEy:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    invoke-virtual {v3, v0}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v4, p0, Lcom/android/settings/bluetooth/GattProfile;->aEB:Landroid/os/Handler;

    new-instance v5, Lcom/android/settings/bluetooth/GattProfile$2;

    invoke-direct {v5, p0, v0, v3}, Lcom/android/settings/bluetooth/GattProfile$2;-><init>(Lcom/android/settings/bluetooth/GattProfile;Landroid/bluetooth/BluetoothDevice;Lcom/android/settingslib/bluetooth/b;)V

    const-wide/16 v6, 0x32

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3

    :cond_7
    iput-object v2, p0, Lcom/android/settings/bluetooth/GattProfile;->aEy:Ljava/util/List;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public ase()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/GattProfile;->aEF:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEF:Landroid/database/ContentObserver;

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEA:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->close()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEA:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public asf(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/GattProfile;->asg(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public asg(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEy:Ljava/util/List;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public asi(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEA:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v0, v4, p0}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/settings/bluetooth/GattProfile;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "connectGatt() return null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v4

    :cond_0
    iget-object v1, p0, Lcom/android/settings/bluetooth/GattProfile;->aEA:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-static {v0}, Lcom/android/settings/bluetooth/GattProfile;->ash(Landroid/bluetooth/BluetoothGatt;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, p0, v3}, Lcom/android/settingslib/bluetooth/b;->ciG(Lcom/android/settingslib/bluetooth/f;I)V

    :cond_2
    return v3

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/GattProfile;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    if-eq v1, v3, :cond_4

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    :cond_4
    return v3

    :cond_5
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->connect()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/android/settings/bluetooth/GattProfile;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "__clientConnect return false"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v4
.end method

.method public asj(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEA:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v0, v2, p0}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/settings/bluetooth/GattProfile;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "disconnectGatt() return null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/GattProfile;->ask(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    if-ne v1, v3, :cond_2

    :cond_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x3

    invoke-virtual {v0, p0, v1}, Lcom/android/settingslib/bluetooth/b;->ciG(Lcom/android/settingslib/bluetooth/f;I)V

    :cond_2
    return v3
.end method

.method public ask(Landroid/bluetooth/BluetoothDevice;)I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/GattProfile;->aEx:Landroid/bluetooth/BluetoothManager;

    const/4 v1, 0x7

    invoke-virtual {v0, p1, v1}, Landroid/bluetooth/BluetoothManager;->getConnectionState(Landroid/bluetooth/BluetoothDevice;I)I

    move-result v0

    return v0
.end method

.method public asl(Landroid/bluetooth/BluetoothClass;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public asm(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public asn()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public aso(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public asp()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public asq()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public asr(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public ass()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public ast(Landroid/bluetooth/BluetoothDevice;Z)V
    .locals 0

    return-void
.end method

.method public onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    return-void
.end method

.method public onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    return-void
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    return-void
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/GattProfile;->aEz:Lcom/android/settingslib/bluetooth/t;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/bluetooth/t;->ckL(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, p0, p3}, Lcom/android/settingslib/bluetooth/b;->ciG(Lcom/android/settingslib/bluetooth/f;I)V

    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V

    return-void
.end method

.method public onDescriptorRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onDescriptorRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V

    return-void
.end method

.method public onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V

    return-void
.end method

.method public onReadRemoteRssi(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onReadRemoteRssi(Landroid/bluetooth/BluetoothGatt;II)V

    return-void
.end method

.method public onReliableWriteCompleted(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/bluetooth/BluetoothGattCallback;->onReliableWriteCompleted(Landroid/bluetooth/BluetoothGatt;I)V

    return-void
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/bluetooth/BluetoothGattCallback;->onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V

    return-void
.end method
