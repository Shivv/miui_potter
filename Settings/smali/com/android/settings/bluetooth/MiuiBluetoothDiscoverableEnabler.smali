.class public Lcom/android/settings/bluetooth/MiuiBluetoothDiscoverableEnabler;
.super Lcom/android/settings/bluetooth/BluetoothDiscoverableEnabler;
.source "MiuiBluetoothDiscoverableEnabler.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# virtual methods
.method aqD(I)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/BluetoothDiscoverableEnabler;->aqD(I)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothDiscoverableEnabler;->aCN:Landroid/preference/CheckBoxPreference;

    const/16 v0, 0x17

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/MiuiBluetoothDiscoverableEnabler;->onPreferenceClick(Landroid/preference/Preference;)Z

    const/4 v0, 0x1

    return v0
.end method
