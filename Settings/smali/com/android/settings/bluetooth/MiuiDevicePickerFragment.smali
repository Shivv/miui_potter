.class public Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;
.super Lcom/android/settings/bluetooth/DevicePickerFragment;
.source "MiuiDevicePickerFragment.java"


# instance fields
.field private aDS:Lcom/android/settings/aD;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/DevicePickerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method aqo()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/bluetooth/DevicePickerFragment;->aqo()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->setHasOptionsMenu(Z)V

    return-void
.end method

.method aqq(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/DevicePickerFragment;->aqq(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V

    const v0, 0x7f0d0127

    invoke-virtual {p1, v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setLayoutResource(I)V

    return-void
.end method

.method public aqv(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/DevicePickerFragment;->aqv(Z)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->invalidateOptionsMenu()V

    return-void
.end method

.method protected aqz(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aDS:Lcom/android/settings/aD;

    invoke-virtual {v0}, Lcom/android/settings/aD;->bmR()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aDS:Lcom/android/settings/aD;

    invoke-virtual {v0}, Lcom/android/settings/aD;->bmS()V

    goto :goto_0
.end method

.method arN()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aCY:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/bluetooth/b;

    sget-object v3, Lcom/android/settingslib/bluetooth/g;->cEm:Lcom/android/settingslib/bluetooth/h;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/android/settingslib/bluetooth/h;->atm(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aCX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    new-instance v0, Lcom/android/settings/aD;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/aD;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aDS:Lcom/android/settings/aD;

    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/DevicePickerFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    const/16 v3, 0xc

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    const v3, 0x7f12032c

    invoke-interface {p1, v2, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aDS:Lcom/android/settings/aD;

    invoke-virtual {v4}, Lcom/android/settings/aD;->buX()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aqz(Z)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const/4 v2, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/DevicePickerFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjS()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjF()V

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->arN()V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aqn()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiDevicePickerFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/bluetooth/d;->cjz(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
