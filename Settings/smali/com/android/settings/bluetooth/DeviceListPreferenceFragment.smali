.class public abstract Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;
.super Lcom/android/settings/MiuiRestrictedSettingsFragment;
.source "DeviceListPreferenceFragment.java"

# interfaces
.implements Lcom/android/settingslib/bluetooth/a;


# instance fields
.field aCV:Lcom/android/settingslib/bluetooth/d;

.field aCW:Lcom/android/settingslib/bluetooth/q;

.field protected aCX:Landroid/preference/PreferenceGroup;

.field final aCY:Ljava/util/WeakHashMap;

.field protected aCZ:Lcom/android/settingslib/bluetooth/h;

.field aDa:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCY:Ljava/util/WeakHashMap;

    sget-object v0, Lcom/android/settingslib/bluetooth/g;->cEk:Lcom/android/settingslib/bluetooth/h;

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCZ:Lcom/android/settingslib/bluetooth/h;

    return-void
.end method


# virtual methods
.method aqI(Landroid/preference/PreferenceGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    return-void
.end method

.method public aqJ(Lcom/android/settingslib/bluetooth/b;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCY:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method public aqK(Lcom/android/settingslib/bluetooth/b;I)V
    .locals 0

    return-void
.end method

.method aqL()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjF()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCY:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->clear()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/Preference;->getOrder()I

    move-result v2

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method final aqM(Lcom/android/settingslib/bluetooth/h;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCZ:Lcom/android/settingslib/bluetooth/h;

    return-void
.end method

.method final aqN(I)V
    .locals 1

    invoke-static {p1}, Lcom/android/settingslib/bluetooth/g;->cki(I)Lcom/android/settingslib/bluetooth/h;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCZ:Lcom/android/settingslib/bluetooth/h;

    return-void
.end method

.method aqn()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckm()Lcom/android/settingslib/bluetooth/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/t;->ckU()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aqs(Lcom/android/settingslib/bluetooth/b;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method abstract aqo()V
.end method

.method aqp(Lcom/android/settingslib/bluetooth/b;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    if-nez v0, :cond_0

    const-string/jumbo v0, "DeviceListPreferenceFragment"

    const-string/jumbo v1, "Trying to create a device preference before the list group/category exists!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->bWH(Ljava/lang/String;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->bWz()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/b;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->setKey(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aqq(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCY:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {v0, p1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arR(Lcom/android/settingslib/bluetooth/b;)V

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arS()V

    goto :goto_0
.end method

.method aqq(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V
    .locals 0

    return-void
.end method

.method public aqr(I)V
    .locals 1

    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aqz(Z)V

    :cond_0
    return-void
.end method

.method public aqs(Lcom/android/settingslib/bluetooth/b;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCY:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCZ:Lcom/android/settingslib/bluetooth/h;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/settingslib/bluetooth/h;->atm(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aqp(Lcom/android/settingslib/bluetooth/b;)V

    :cond_2
    return-void
.end method

.method aqu(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V
    .locals 0

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arQ()V

    return-void
.end method

.method public aqv(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aqz(Z)V

    return-void
.end method

.method protected aqz(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    instance-of v0, v0, Lcom/android/settings/bluetooth/BluetoothProgressCategory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    check-cast v0, Lcom/android/settings/bluetooth/BluetoothProgressCategory;

    invoke-virtual {v0, p1}, Lcom/android/settings/bluetooth/BluetoothProgressCategory;->bSt(Z)V

    :cond_0
    return-void
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 3

    const/4 v2, 0x1

    const-string/jumbo v0, "bt_scan"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/bluetooth/d;->cjz(Z)V

    return v2

    :cond_0
    instance-of v0, p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->arP()Lcom/android/settingslib/bluetooth/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aDa:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aqu(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V

    return v2

    :cond_1
    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/bluetooth/Utils;->arq(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/q;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    if-nez v0, :cond_0

    const-string/jumbo v0, "DeviceListPreferenceFragment"

    const-string/jumbo v1, "Bluetooth is not supported on this device"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckl()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aqo()V

    const-string/jumbo v0, "bt_device_list"

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCX:Landroid/preference/PreferenceGroup;

    return-void
.end method

.method public onPause()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->bXB()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/q;->ckp(Landroid/content/Context;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->bXB()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/q;->ckp(Landroid/content/Context;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onStart()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->bXB()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckn()Lcom/android/settingslib/bluetooth/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/s;->ckt(Lcom/android/settingslib/bluetooth/a;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCV:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjS()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aqz(Z)V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiRestrictedSettingsFragment;->onStop()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->bXB()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aqL()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->aCW:Lcom/android/settingslib/bluetooth/q;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckn()Lcom/android/settingslib/bluetooth/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/s;->ckw(Lcom/android/settingslib/bluetooth/a;)V

    return-void
.end method
