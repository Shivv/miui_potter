.class final Lcom/android/settings/bluetooth/BluetoothPairingDialogFragment$1;
.super Landroid/os/Handler;
.source "BluetoothPairingDialogFragment.java"


# instance fields
.field final synthetic aFL:Lcom/android/settings/bluetooth/BluetoothPairingDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/bluetooth/BluetoothPairingDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothPairingDialogFragment$1;->aFL:Lcom/android/settings/bluetooth/BluetoothPairingDialogFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string/jumbo v0, "BTPairingDialogFragment"

    const-string/jumbo v1, "Delayed pairing pop up handler"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothPairingDialogFragment$1;->aFL:Lcom/android/settings/bluetooth/BluetoothPairingDialogFragment;

    invoke-static {v0}, Lcom/android/settings/bluetooth/BluetoothPairingDialogFragment;->asJ(Lcom/android/settings/bluetooth/BluetoothPairingDialogFragment;)Lcom/android/settings/bluetooth/BluetoothPairingDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/BluetoothPairingDialog;->dismiss()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
