.class public Lcom/android/settings/applications/RunningState;
.super Ljava/lang/Object;
.source "RunningState.java"


# static fields
.field static vR:Ljava/lang/Object;

.field static vS:Lcom/android/settings/applications/RunningState;


# instance fields
.field final mLock:Ljava/lang/Object;

.field vA:I

.field vB:I

.field final vC:Landroid/util/SparseArray;

.field final vD:Landroid/util/SparseArray;

.field final vE:Landroid/content/pm/PackageManager;

.field final vF:Ljava/util/ArrayList;

.field vG:Lcom/android/settings/applications/RunningState$OnRefreshUiListener;

.field final vH:Landroid/util/SparseArray;

.field vI:I

.field final vJ:Lcom/android/settings/applications/RunningState$ServiceProcessComparator;

.field final vK:Landroid/util/SparseArray;

.field final vL:Landroid/util/SparseArray;

.field final vM:Landroid/util/SparseArray;

.field final vN:Landroid/os/UserManager;

.field private final vO:Lcom/android/settings/applications/RunningState$UserManagerBroadcastReceiver;

.field vP:Ljava/util/ArrayList;

.field vQ:Z

.field final vg:Ljava/util/Comparator;

.field vh:Z

.field vi:J

.field vj:J

.field final vk:Ljava/util/ArrayList;

.field final vl:Landroid/app/ActivityManager;

.field final vm:Landroid/content/Context;

.field final vn:Lcom/android/settings/applications/RunningState$BackgroundHandler;

.field vo:Ljava/util/ArrayList;

.field final vp:Landroid/os/HandlerThread;

.field vq:J

.field final vr:Landroid/os/Handler;

.field vs:Z

.field final vt:Z

.field final vu:Lcom/android/settingslib/b/F;

.field final vv:Ljava/util/ArrayList;

.field vw:Ljava/util/ArrayList;

.field vx:Ljava/util/ArrayList;

.field final vy:I

.field vz:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/settings/applications/RunningState;->vR:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settingslib/b/F;

    invoke-direct {v0}, Lcom/android/settingslib/b/F;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vu:Lcom/android/settingslib/b/F;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vL:Landroid/util/SparseArray;

    new-instance v0, Lcom/android/settings/applications/RunningState$ServiceProcessComparator;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/RunningState$ServiceProcessComparator;-><init>(Lcom/android/settings/applications/RunningState;)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vJ:Lcom/android/settings/applications/RunningState$ServiceProcessComparator;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vv:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vk:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vD:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vC:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vM:Landroid/util/SparseArray;

    iput v2, p0, Lcom/android/settings/applications/RunningState;->vI:I

    new-instance v0, Lcom/android/settings/applications/RunningState$1;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/RunningState$1;-><init>(Lcom/android/settings/applications/RunningState;)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vg:Ljava/util/Comparator;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vw:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vx:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vo:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vP:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/settings/applications/RunningState$2;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/RunningState$2;-><init>(Lcom/android/settings/applications/RunningState;)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vr:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/applications/RunningState$UserManagerBroadcastReceiver;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/applications/RunningState$UserManagerBroadcastReceiver;-><init>(Lcom/android/settings/applications/RunningState;Lcom/android/settings/applications/RunningState$UserManagerBroadcastReceiver;)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vO:Lcom/android/settings/applications/RunningState$UserManagerBroadcastReceiver;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vm:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vm:Landroid/content/Context;

    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vl:Landroid/app/ActivityManager;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vm:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vE:Landroid/content/pm/PackageManager;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vm:Landroid/content/Context;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vN:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/RunningState;->vy:I

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vN:Landroid/os/UserManager;

    iget v1, p0, Lcom/android/settings/applications/RunningState;->vy:I

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->canHaveProfile()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/applications/RunningState;->vt:Z

    iput-boolean v2, p0, Lcom/android/settings/applications/RunningState;->vh:Z

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "RunningState:Background"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vp:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vp:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/settings/applications/RunningState$BackgroundHandler;

    iget-object v1, p0, Lcom/android/settings/applications/RunningState;->vp:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/applications/RunningState$BackgroundHandler;-><init>(Lcom/android/settings/applications/RunningState;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vn:Lcom/android/settings/applications/RunningState$BackgroundHandler;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vO:Lcom/android/settings/applications/RunningState$UserManagerBroadcastReceiver;

    iget-object v1, p0, Lcom/android/settings/applications/RunningState;->vm:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/RunningState$UserManagerBroadcastReceiver;->register(Landroid/content/Context;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static getInstance(Landroid/content/Context;)Lcom/android/settings/applications/RunningState;
    .locals 2

    sget-object v1, Lcom/android/settings/applications/RunningState;->vR:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/settings/applications/RunningState;->vS:Lcom/android/settings/applications/RunningState;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/applications/RunningState;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/RunningState;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/applications/RunningState;->vS:Lcom/android/settings/applications/RunningState;

    :cond_0
    sget-object v0, Lcom/android/settings/applications/RunningState;->vS:Lcom/android/settings/applications/RunningState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static sD(Landroid/content/pm/PackageManager;Ljava/lang/String;Landroid/content/pm/PackageItemInfo;)Ljava/lang/CharSequence;
    .locals 2

    if-eqz p2, :cond_1

    iget v0, p2, Landroid/content/pm/PackageItemInfo;->labelRes:I

    if-nez v0, :cond_0

    iget-object v0, p2, Landroid/content/pm/PackageItemInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p2, p0}, Landroid/content/pm/PackageItemInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-ltz v0, :cond_2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_2
    return-object p1
.end method

.method private sI(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    return v3

    :cond_0
    iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v1, 0x64

    if-lt v0, v1, :cond_1

    iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v1, 0x10e

    if-ge v0, v1, :cond_1

    iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonCode:I

    if-nez v0, :cond_1

    return v3

    :cond_1
    return v2
.end method

.method private sJ()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vL:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vv:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vk:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private sK(Landroid/content/Context;Landroid/app/ActivityManager;)Z
    .locals 31

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/applications/RunningState;->vI:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/settings/applications/RunningState;->vI:I

    const/4 v9, 0x0

    const/16 v4, 0xc8

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v4

    :goto_0
    const/4 v5, 0x0

    move v6, v4

    :goto_1
    if-ge v5, v6, :cond_2

    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-boolean v7, v4, Landroid/app/ActivityManager$RunningServiceInfo;->started:Z

    if-nez v7, :cond_1

    iget v7, v4, Landroid/app/ActivityManager$RunningServiceInfo;->clientLabel:I

    if-nez v7, :cond_1

    invoke-interface {v12, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v4, v5, -0x1

    add-int/lit8 v5, v6, -0x1

    :goto_2
    add-int/lit8 v4, v4, 0x1

    move v6, v5

    move v5, v4

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    iget v4, v4, Landroid/app/ActivityManager$RunningServiceInfo;->flags:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_60

    invoke-interface {v12, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v4, v5, -0x1

    add-int/lit8 v5, v6, -0x1

    goto :goto_2

    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v13

    if-eqz v13, :cond_3

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v4

    move v7, v4

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vM:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->clear()V

    const/4 v4, 0x0

    move v5, v4

    :goto_4
    if-ge v5, v7, :cond_4

    invoke-interface {v13, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/settings/applications/RunningState;->vM:Landroid/util/SparseArray;

    iget v10, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    new-instance v14, Lcom/android/settings/applications/RunningState$AppProcessInfo;

    invoke-direct {v14, v4}, Lcom/android/settings/applications/RunningState$AppProcessInfo;-><init>(Landroid/app/ActivityManager$RunningAppProcessInfo;)V

    invoke-virtual {v8, v10, v14}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_4

    :cond_3
    const/4 v4, 0x0

    move v7, v4

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    move v8, v4

    :goto_5
    if-ge v8, v6, :cond_6

    invoke-interface {v12, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-wide v14, v4, Landroid/app/ActivityManager$RunningServiceInfo;->restarting:J

    const-wide/16 v16, 0x0

    cmp-long v5, v14, v16

    if-nez v5, :cond_5

    iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I

    if-lez v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vM:Landroid/util/SparseArray;

    iget v10, v4, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I

    invoke-virtual {v5, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;

    if-eqz v5, :cond_5

    const/4 v10, 0x1

    iput-boolean v10, v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;->vU:Z

    iget-boolean v4, v4, Landroid/app/ActivityManager$RunningServiceInfo;->foreground:Z

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    iput-boolean v4, v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;->vT:Z

    :cond_5
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_5

    :cond_6
    const/4 v4, 0x0

    move v10, v4

    :goto_6
    if-ge v10, v6, :cond_f

    invoke-interface {v12, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-wide v14, v4, Landroid/app/ActivityManager$RunningServiceInfo;->restarting:J

    const-wide/16 v16, 0x0

    cmp-long v5, v14, v16

    if-nez v5, :cond_9

    iget v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I

    if-lez v5, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vM:Landroid/util/SparseArray;

    iget v8, v4, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I

    invoke-virtual {v5, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;

    if-eqz v5, :cond_9

    iget-boolean v8, v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;->vT:Z

    xor-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_9

    iget-object v8, v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;->vV:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v8, v8, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v14, 0x12c

    if-ge v8, v14, :cond_9

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/settings/applications/RunningState;->vM:Landroid/util/SparseArray;

    iget-object v5, v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;->vV:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonPid:I

    invoke-virtual {v14, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;

    :goto_7
    if-eqz v5, :cond_5f

    iget-boolean v14, v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;->vU:Z

    if-nez v14, :cond_7

    iget-object v14, v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;->vV:Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/settings/applications/RunningState;->sI(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z

    move-result v14

    if-eqz v14, :cond_8

    :cond_7
    const/4 v5, 0x1

    :goto_8
    if-eqz v5, :cond_9

    :goto_9
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_6

    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/settings/applications/RunningState;->vM:Landroid/util/SparseArray;

    iget-object v5, v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;->vV:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonPid:I

    invoke-virtual {v14, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$AppProcessInfo;

    goto :goto_7

    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    iget v8, v4, Landroid/app/ActivityManager$RunningServiceInfo;->uid:I

    invoke-virtual {v5, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    if-nez v5, :cond_5e

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    iget v14, v4, Landroid/app/ActivityManager$RunningServiceInfo;->uid:I

    invoke-virtual {v8, v14, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v8, v5

    :goto_a
    iget-object v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ProcessItem;

    if-nez v5, :cond_5d

    const/4 v9, 0x1

    new-instance v5, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v14, v4, Landroid/app/ActivityManager$RunningServiceInfo;->uid:I

    iget-object v15, v4, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v14, v15}, Lcom/android/settings/applications/RunningState$ProcessItem;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    iget-object v14, v4, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    invoke-virtual {v8, v14, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v8, v9

    :goto_b
    iget v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/settings/applications/RunningState;->vI:I

    if-eq v9, v14, :cond_d

    iget-wide v14, v4, Landroid/app/ActivityManager$RunningServiceInfo;->restarting:J

    const-wide/16 v16, 0x0

    cmp-long v9, v14, v16

    if-nez v9, :cond_e

    iget v9, v4, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I

    :goto_c
    iget v14, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    if-eq v9, v14, :cond_c

    const/4 v8, 0x1

    iget v14, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    if-eq v14, v9, :cond_c

    iget v14, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    if-eqz v14, :cond_a

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/settings/applications/RunningState;->vL:Landroid/util/SparseArray;

    iget v15, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    invoke-virtual {v14, v15}, Landroid/util/SparseArray;->remove(I)V

    :cond_a
    if-eqz v9, :cond_b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/settings/applications/RunningState;->vL:Landroid/util/SparseArray;

    invoke-virtual {v14, v9, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_b
    iput v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    :cond_c
    iget-object v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {v9}, Landroid/util/SparseArray;->clear()V

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/applications/RunningState;->vI:I

    iput v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    :cond_d
    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v4}, Lcom/android/settings/applications/RunningState$ProcessItem;->sR(Landroid/content/Context;Landroid/app/ActivityManager$RunningServiceInfo;)Z

    move-result v4

    or-int v9, v8, v4

    goto/16 :goto_9

    :cond_e
    const/4 v9, 0x0

    goto :goto_c

    :cond_f
    const/4 v4, 0x0

    move v8, v4

    move v6, v9

    :goto_d
    if-ge v8, v7, :cond_14

    invoke-interface {v13, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vL:Landroid/util/SparseArray;

    iget v9, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ProcessItem;

    if-nez v5, :cond_11

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    iget v9, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ProcessItem;

    if-nez v5, :cond_10

    const/4 v6, 0x1

    new-instance v5, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v9, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    iget-object v10, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v9, v10}, Lcom/android/settings/applications/RunningState$ProcessItem;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    iget v9, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    iput v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    iget v10, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-virtual {v9, v10, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_10
    iget-object v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {v9}, Landroid/util/SparseArray;->clear()V

    :cond_11
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/settings/applications/RunningState;->sI(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z

    move-result v9

    if-eqz v9, :cond_13

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/settings/applications/RunningState;->vv:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_12

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/settings/applications/RunningState;->vv:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_12
    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/applications/RunningState;->vI:I

    iput v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    const/4 v9, 0x1

    iput-boolean v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wx:Z

    invoke-virtual {v5, v11}, Lcom/android/settings/applications/RunningState$ProcessItem;->sO(Landroid/content/pm/PackageManager;)V

    :goto_e
    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/applications/RunningState;->vI:I

    iput v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wC:I

    iput-object v4, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->ws:Landroid/app/ActivityManager$RunningAppProcessInfo;

    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_d

    :cond_13
    const/4 v9, 0x0

    iput-boolean v9, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wx:Z

    goto :goto_e

    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v7

    const/4 v4, 0x0

    move v8, v6

    move v6, v4

    :goto_f
    if-ge v6, v7, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wC:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/applications/RunningState;->vI:I

    if-ne v5, v9, :cond_18

    iget-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->ws:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v9, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonPid:I

    if-eqz v9, :cond_17

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vL:Landroid/util/SparseArray;

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ProcessItem;

    if-nez v5, :cond_15

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ProcessItem;

    :cond_15
    if-eqz v5, :cond_16

    iget-object v5, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    iget v9, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    invoke-virtual {v5, v9, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_16
    :goto_10
    add-int/lit8 v4, v6, 0x1

    move v5, v7

    move v6, v8

    :goto_11
    move v7, v5

    move v8, v6

    move v6, v4

    goto :goto_f

    :cond_17
    const/4 v5, 0x0

    iput-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wv:Lcom/android/settings/applications/RunningState$ProcessItem;

    goto :goto_10

    :cond_18
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    invoke-virtual {v8, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    invoke-virtual {v4, v8}, Landroid/util/SparseArray;->remove(I)V

    add-int/lit8 v4, v7, -0x1

    move/from16 v28, v6

    move v6, v5

    move v5, v4

    move/from16 v4, v28

    goto :goto_11

    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vv:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v5, 0x0

    move v7, v8

    :goto_12
    if-ge v5, v6, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vv:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-boolean v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wx:Z

    if-eqz v8, :cond_1a

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    iget v4, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    invoke-virtual {v8, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1b

    :cond_1a
    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vv:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v4, v5, -0x1

    add-int/lit8 v5, v6, -0x1

    move v6, v7

    :goto_13
    add-int/lit8 v4, v4, 0x1

    move v7, v6

    move v6, v5

    move v5, v4

    goto :goto_12

    :cond_1b
    move v4, v5

    move v5, v6

    move v6, v7

    goto :goto_13

    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vL:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v8

    const/4 v4, 0x0

    move v6, v4

    move v5, v7

    :goto_14
    if-ge v6, v8, :cond_1d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vL:Landroid/util/SparseArray;

    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v7, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/applications/RunningState;->vI:I

    if-ne v7, v9, :cond_5c

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/settings/applications/RunningState;->vI:I

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v11, v7}, Lcom/android/settings/applications/RunningState$ProcessItem;->sQ(Landroid/content/Context;Landroid/content/pm/PackageManager;I)Z

    move-result v4

    or-int/2addr v4, v5

    :goto_15
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    goto :goto_14

    :cond_1d
    const/4 v6, 0x0

    const/4 v4, 0x0

    move v7, v5

    move-object v5, v6

    move v6, v4

    :goto_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v6, v4, :cond_25

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v8, v7

    move-object v7, v5

    :cond_1e
    :goto_17
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_24

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v10, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/settings/applications/RunningState;->vI:I

    if-ne v10, v12, :cond_21

    invoke-virtual {v5, v11}, Lcom/android/settings/applications/RunningState$ProcessItem;->sO(Landroid/content/pm/PackageManager;)V

    iget v10, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    if-nez v10, :cond_1f

    iget-object v10, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->ww:Landroid/util/SparseArray;

    invoke-virtual {v10}, Landroid/util/SparseArray;->clear()V

    :cond_1f
    iget-object v5, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_20
    :goto_18
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1e

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ServiceItem;

    iget v5, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wi:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/settings/applications/RunningState;->vI:I

    if-eq v5, v12, :cond_20

    const/4 v8, 0x1

    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    goto :goto_18

    :cond_21
    const/4 v8, 0x1

    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v10

    if-nez v10, :cond_23

    if-nez v7, :cond_22

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :cond_22
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    invoke-virtual {v10, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_23
    iget v10, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    if-eqz v10, :cond_1e

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/settings/applications/RunningState;->vL:Landroid/util/SparseArray;

    iget v5, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    invoke-virtual {v10, v5}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_17

    :cond_24
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move-object v5, v7

    move v7, v8

    goto/16 :goto_16

    :cond_25
    if-eqz v5, :cond_26

    const/4 v4, 0x0

    move v6, v4

    :goto_19
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v6, v4, :cond_26

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    invoke-virtual {v8, v4}, Landroid/util/SparseArray;->remove(I)V

    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_19

    :cond_26
    if-eqz v7, :cond_3e

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    move v6, v4

    :goto_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v6, v4, :cond_2b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vK:Landroid/util/SparseArray;

    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wz:Z

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wy:Z

    const-wide v12, 0x7fffffffffffffffL

    iput-wide v12, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wu:J

    iget-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_27
    :goto_1c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_29

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ServiceItem;

    iget-object v10, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    if-eqz v10, :cond_28

    iget-object v10, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    iget-object v10, v10, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v10, v10, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_28

    const/4 v10, 0x1

    iput-boolean v10, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wz:Z

    :cond_28
    iget-object v10, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    if-eqz v10, :cond_27

    iget-object v10, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    iget v10, v10, Landroid/app/ActivityManager$RunningServiceInfo;->clientLabel:I

    if-eqz v10, :cond_27

    const/4 v10, 0x0

    iput-boolean v10, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wy:Z

    iget-wide v12, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wu:J

    iget-object v10, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    iget-wide v14, v10, Landroid/app/ActivityManager$RunningServiceInfo;->activeSince:J

    cmp-long v10, v12, v14

    if-lez v10, :cond_27

    iget-object v5, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    iget-wide v12, v5, Landroid/app/ActivityManager$RunningServiceInfo;->activeSince:J

    iput-wide v12, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wu:J

    goto :goto_1c

    :cond_29
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1b

    :cond_2a
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto/16 :goto_1a

    :cond_2b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vJ:Lcom/android/settings/applications/RunningState$ServiceProcessComparator;

    invoke-static {v11, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    const/4 v4, 0x0

    move v6, v4

    :goto_1d
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v6, v4, :cond_35

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wk:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    invoke-virtual {v4, v12, v5}, Lcom/android/settings/applications/RunningState$ProcessItem;->sP(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    if-lez v5, :cond_2c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2c
    const/4 v8, 0x0

    const/4 v5, 0x0

    iget-object v9, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move-object v9, v8

    move v8, v5

    :goto_1e
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2f

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ServiceItem;

    iput-boolean v8, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wk:Z

    const/4 v8, 0x1

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v15, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wo:Lcom/android/settings/applications/RunningState$MergedItem;

    if-eqz v15, :cond_2e

    if-eqz v9, :cond_2d

    iget-object v15, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wo:Lcom/android/settings/applications/RunningState$MergedItem;

    :cond_2d
    iget-object v5, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wo:Lcom/android/settings/applications/RunningState$MergedItem;

    :goto_1f
    move-object v9, v5

    goto :goto_1e

    :cond_2e
    move-object v5, v9

    goto :goto_1f

    :cond_2f
    new-instance v9, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->mUserId:I

    invoke-direct {v9, v5}, Lcom/android/settings/applications/RunningState$MergedItem;-><init>(I)V

    iget-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_20
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_30

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$ServiceItem;

    iget-object v14, v9, Lcom/android/settings/applications/RunningState$MergedItem;->wE:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v9, v5, Lcom/android/settings/applications/RunningState$ServiceItem;->wo:Lcom/android/settings/applications/RunningState$MergedItem;

    goto :goto_20

    :cond_30
    iput-object v4, v9, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v4, v9, Lcom/android/settings/applications/RunningState$MergedItem;->wG:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    move v5, v10

    :goto_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v5, v4, :cond_31

    iget-object v8, v9, Lcom/android/settings/applications/RunningState$MergedItem;->wG:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_21

    :cond_31
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v4}, Lcom/android/settings/applications/RunningState$MergedItem;->sU(Landroid/content/Context;Z)Z

    iget v4, v9, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/settings/applications/RunningState;->vy:I

    if-eq v4, v5, :cond_34

    iget v4, v9, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    const/16 v5, 0x3e7

    if-ne v4, v5, :cond_32

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/settings/applications/RunningState;->vy:I

    if-eqz v4, :cond_33

    :cond_32
    :goto_22
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto/16 :goto_1d

    :cond_33
    sget-object v4, Lmiui/securityspace/XSpaceConstant;->REQUIRED_APPS:Ljava/util/ArrayList;

    iget-object v5, v9, Lcom/android/settings/applications/RunningState$MergedItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_32

    const-string/jumbo v4, "com.xiaomi.xmsf"

    iget-object v5, v9, Lcom/android/settings/applications/RunningState$MergedItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_32

    :cond_34
    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_22

    :cond_35
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vv:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v4, 0x0

    move v5, v4

    :goto_23
    if-ge v5, v6, :cond_3b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vv:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wv:Lcom/android/settings/applications/RunningState$ProcessItem;

    if-nez v8, :cond_38

    iget-object v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wD:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v8

    if-gtz v8, :cond_38

    iget-object v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    if-nez v8, :cond_36

    new-instance v8, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v9, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->mUserId:I

    invoke-direct {v8, v9}, Lcom/android/settings/applications/RunningState$MergedItem;-><init>(I)V

    iput-object v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    iput-object v4, v8, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    :cond_36
    iget-object v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v9}, Lcom/android/settings/applications/RunningState$MergedItem;->sU(Landroid/content/Context;Z)Z

    iget-object v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    iget v8, v8, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/applications/RunningState;->vy:I

    if-eq v8, v9, :cond_3a

    iget-object v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    iget v8, v8, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    const/16 v9, 0x3e7

    if-ne v8, v9, :cond_37

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/settings/applications/RunningState;->vy:I

    if-eqz v8, :cond_39

    :cond_37
    :goto_24
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_38
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_23

    :cond_39
    sget-object v8, Lmiui/securityspace/XSpaceConstant;->REQUIRED_APPS:Ljava/util/ArrayList;

    iget-object v9, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v9, v9, Lcom/android/settings/applications/RunningState$MergedItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v9, v9, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_37

    const-string/jumbo v8, "com.xiaomi.xmsf"

    iget-object v9, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v9, v9, Lcom/android/settings/applications/RunningState$MergedItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v9, v9, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_37

    :cond_3a
    iget-object v8, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    const/4 v9, 0x0

    invoke-virtual {v13, v9, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_24

    :cond_3b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vD:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v6

    const/4 v4, 0x0

    move v5, v4

    :goto_25
    if-ge v5, v6, :cond_3d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vD:Landroid/util/SparseArray;

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v8, v4, Lcom/android/settings/applications/RunningState$MergedItem;->wi:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/settings/applications/RunningState;->vI:I

    if-ne v8, v9, :cond_3c

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v8}, Lcom/android/settings/applications/RunningState$MergedItem;->sU(Landroid/content/Context;Z)Z

    :cond_3c
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_25

    :cond_3d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/settings/applications/RunningState;->vw:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/settings/applications/RunningState;->vx:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v5

    :cond_3e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vk:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vk:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vF:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v11

    const/4 v4, 0x0

    move v10, v4

    :goto_26
    if-ge v10, v11, :cond_42

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vH:Landroid/util/SparseArray;

    invoke-virtual {v4, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/settings/applications/RunningState;->vI:I

    if-eq v5, v12, :cond_41

    iget-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->ws:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v12, 0x190

    if-lt v5, v12, :cond_3f

    add-int/lit8 v5, v9, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/settings/applications/RunningState;->vk:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v6

    move v6, v5

    move v5, v8

    :goto_27
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    move v9, v6

    move v8, v5

    move v6, v4

    goto :goto_26

    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    :cond_3f
    iget-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->ws:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v12, 0xc8

    if-gt v5, v12, :cond_40

    add-int/lit8 v5, v8, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/settings/applications/RunningState;->vk:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v6

    move v6, v9

    goto :goto_27

    :cond_40
    const-string/jumbo v5, "RunningState"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "Unknown non-service process: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wr:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " #"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v4, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v6

    move v5, v8

    move v6, v9

    goto :goto_27

    :cond_41
    add-int/lit8 v4, v6, 0x1

    move v5, v8

    move v6, v9

    goto :goto_27

    :cond_42
    const-wide/16 v16, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v12, 0x0

    const/4 v11, 0x0

    const/16 v20, 0x0

    const/4 v10, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vk:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v0, v0, [I

    move-object/from16 v23, v0

    const/4 v4, 0x0

    move v5, v4

    :goto_28
    move/from16 v0, v18

    if-ge v5, v0, :cond_43

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vk:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v4, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wt:I

    aput v4, v23, v5

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_28

    :cond_43
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-interface {v4, v0}, Landroid/app/IActivityManager;->getProcessPss([I)[J
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v24

    const/4 v5, 0x0

    const/4 v4, 0x0

    move/from16 v22, v4

    move/from16 v28, v5

    move v5, v7

    move/from16 v7, v28

    :goto_29
    :try_start_2
    move-object/from16 v0, v23

    array-length v4, v0

    move/from16 v0, v22

    if-ge v0, v4, :cond_4d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vk:Ljava/util/ArrayList;

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    aget-wide v18, v24, v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/applications/RunningState;->vI:I

    move/from16 v21, v0

    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    move/from16 v3, v21

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/android/settings/applications/RunningState$ProcessItem;->sS(Landroid/content/Context;JI)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v18

    or-int v19, v5, v18

    :try_start_3
    iget v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wi:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/applications/RunningState;->vI:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v5, v0, :cond_44

    iget-wide v4, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wl:J

    add-long/2addr v4, v12

    move-wide v12, v14

    move-wide/from16 v14, v16

    move-object/from16 v28, v11

    move-wide/from16 v29, v4

    move v4, v7

    move v5, v10

    move-wide/from16 v10, v29

    move-object/from16 v7, v28

    :goto_2a
    add-int/lit8 v16, v22, 0x1

    move/from16 v22, v16

    move-wide/from16 v16, v14

    move-wide v14, v12

    move-wide v12, v10

    move-object v11, v7

    move v10, v5

    move v7, v4

    move/from16 v5, v19

    goto :goto_29

    :cond_44
    iget-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->ws:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v18, 0x190

    move/from16 v0, v18

    if-lt v5, v0, :cond_4c

    iget-wide v0, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wl:J

    move-wide/from16 v26, v0

    add-long v16, v16, v26

    if-eqz v11, :cond_46

    new-instance v5, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v0, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->mUserId:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-direct {v5, v0}, Lcom/android/settings/applications/RunningState$MergedItem;-><init>(I)V

    iput-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v0, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v4, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v4, v5, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/applications/RunningState;->vy:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v4, v0, :cond_45

    const/4 v4, 0x1

    :goto_2b
    or-int/2addr v10, v4

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    move-object v4, v5

    move v5, v10

    move-object v10, v11

    :goto_2c
    const/4 v11, 0x1

    :try_start_4
    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v11}, Lcom/android/settings/applications/RunningState$MergedItem;->sU(Landroid/content/Context;Z)Z

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/settings/applications/RunningState$MergedItem;->sV(Landroid/content/Context;)Z
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    add-int/lit8 v4, v7, 0x1

    move-object v7, v10

    move-wide v10, v12

    move-wide v12, v14

    move-wide/from16 v14, v16

    goto :goto_2a

    :cond_45
    const/4 v4, 0x0

    goto :goto_2b

    :cond_46
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vo:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v7, v5, :cond_47

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vo:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v5, v5, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    if-eq v5, v4, :cond_4b

    :cond_47
    new-instance v18, Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-direct {v0, v9}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2

    const/4 v5, 0x0

    move/from16 v21, v5

    :goto_2d
    move/from16 v0, v21

    if-ge v0, v7, :cond_49

    :try_start_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vo:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v11, v5, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/applications/RunningState;->vy:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-eq v11, v0, :cond_48

    const/4 v11, 0x1

    :goto_2e
    or-int/2addr v10, v11

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v21, 0x1

    move/from16 v21, v5

    goto :goto_2d

    :cond_48
    const/4 v11, 0x0

    goto :goto_2e

    :cond_49
    new-instance v5, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v11, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->mUserId:I

    invoke-direct {v5, v11}, Lcom/android/settings/applications/RunningState$MergedItem;-><init>(I)V

    iput-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v11, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wB:Lcom/android/settings/applications/RunningState$MergedItem;

    iput-object v4, v11, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v4, v5, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/settings/applications/RunningState;->vy:I

    if-eq v4, v11, :cond_4a

    const/4 v4, 0x1

    :goto_2f
    or-int/2addr v10, v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_3

    move-object v4, v5

    move v5, v10

    move-object/from16 v10, v18

    goto/16 :goto_2c

    :cond_4a
    const/4 v4, 0x0

    goto :goto_2f

    :cond_4b
    :try_start_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vo:Ljava/util/ArrayList;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$MergedItem;

    move v5, v10

    move-object v10, v11

    goto/16 :goto_2c

    :cond_4c
    iget-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->ws:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v18, 0xc8

    move/from16 v0, v18

    if-gt v5, v0, :cond_5b

    iget-wide v4, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wl:J
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_2

    add-long/2addr v4, v14

    move-wide/from16 v14, v16

    move/from16 v28, v10

    move-object/from16 v29, v11

    move-wide v10, v12

    move-wide v12, v4

    move/from16 v5, v28

    move v4, v7

    move-object/from16 v7, v29

    goto/16 :goto_2a

    :cond_4d
    move v4, v10

    move-object v7, v11

    move-wide/from16 v18, v16

    move-wide/from16 v16, v14

    move v11, v5

    move-wide v14, v12

    :goto_30
    if-nez v7, :cond_50

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->vo:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, v9, :cond_50

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v9}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, 0x0

    move v12, v5

    move v7, v4

    :goto_31
    if-ge v12, v9, :cond_4f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vo:Ljava/util/ArrayList;

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v5, v4, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/settings/applications/RunningState;->vy:I

    if-eq v5, v13, :cond_4e

    const/4 v5, 0x1

    :goto_32
    or-int/2addr v7, v5

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v12, 0x1

    move v12, v4

    goto :goto_31

    :catch_0
    move-exception v4

    :goto_33
    move v4, v10

    move-wide/from16 v18, v16

    move-wide/from16 v16, v14

    move-wide v14, v12

    move-object/from16 v28, v11

    move v11, v7

    move-object/from16 v7, v28

    goto :goto_30

    :cond_4e
    const/4 v5, 0x0

    goto :goto_32

    :cond_4f
    move v4, v7

    move-object v7, v10

    :cond_50
    if-eqz v7, :cond_5a

    if-nez v4, :cond_52

    move-object v5, v7

    :cond_51
    :goto_34
    const/4 v4, 0x0

    move v10, v4

    :goto_35
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vx:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v10, v4, :cond_57

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vx:Ljava/util/ArrayList;

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$MergedItem;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/settings/applications/RunningState$MergedItem;->sV(Landroid/content/Context;)Z

    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_35

    :cond_52
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/4 v4, 0x0

    move v10, v4

    :goto_36
    if-ge v10, v12, :cond_55

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v13, v4, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/applications/RunningState;->vy:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-eq v13, v0, :cond_54

    iget v13, v4, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    const/16 v20, 0x3e7

    move/from16 v0, v20

    if-ne v13, v0, :cond_53

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/settings/applications/RunningState;->vy:I

    if-eqz v13, :cond_54

    :cond_53
    :goto_37
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_36

    :cond_54
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_37

    :cond_55
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vC:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v12

    const/4 v4, 0x0

    move v10, v4

    :goto_38
    if-ge v10, v12, :cond_51

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/settings/applications/RunningState;->vC:Landroid/util/SparseArray;

    invoke-virtual {v4, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/applications/RunningState$MergedItem;

    iget v13, v4, Lcom/android/settings/applications/RunningState$MergedItem;->wi:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/settings/applications/RunningState;->vI:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ne v13, v0, :cond_56

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v13}, Lcom/android/settings/applications/RunningState$MergedItem;->sU(Landroid/content/Context;Z)Z

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/android/settings/applications/RunningState$MergedItem;->sV(Landroid/content/Context;)Z

    :cond_56
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_38

    :cond_57
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v10

    :try_start_8
    move-object/from16 v0, p0

    iput v9, v0, Lcom/android/settings/applications/RunningState;->vz:I

    move-object/from16 v0, p0

    iput v8, v0, Lcom/android/settings/applications/RunningState;->vA:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/android/settings/applications/RunningState;->vB:I

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/settings/applications/RunningState;->vi:J

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/settings/applications/RunningState;->vq:J

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/android/settings/applications/RunningState;->vj:J

    if-eqz v7, :cond_59

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/android/settings/applications/RunningState;->vo:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/settings/applications/RunningState;->vP:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/settings/applications/RunningState;->vQ:Z

    if-eqz v4, :cond_59

    const/4 v4, 0x1

    :goto_39
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/settings/applications/RunningState;->vs:Z

    if-nez v5, :cond_58

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/settings/applications/RunningState;->vs:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_58
    monitor-exit v10

    return v4

    :catchall_1
    move-exception v4

    monitor-exit v10

    throw v4

    :catch_1
    move-exception v4

    move v7, v5

    goto/16 :goto_33

    :catch_2
    move-exception v4

    move/from16 v7, v19

    goto/16 :goto_33

    :catch_3
    move-exception v4

    move-object/from16 v11, v18

    move/from16 v7, v19

    goto/16 :goto_33

    :catch_4
    move-exception v4

    move-object v11, v10

    move/from16 v7, v19

    move v10, v5

    goto/16 :goto_33

    :cond_59
    move v4, v11

    goto :goto_39

    :cond_5a
    move-object/from16 v5, v20

    goto/16 :goto_34

    :cond_5b
    move v4, v7

    move v5, v10

    move-object v7, v11

    move-wide v10, v12

    move-wide v12, v14

    move-wide/from16 v14, v16

    goto/16 :goto_2a

    :cond_5c
    move v4, v5

    goto/16 :goto_15

    :cond_5d
    move v8, v9

    goto/16 :goto_b

    :cond_5e
    move-object v8, v5

    goto/16 :goto_a

    :cond_5f
    move v5, v8

    goto/16 :goto_8

    :cond_60
    move v4, v5

    move v5, v6

    goto/16 :goto_2
.end method

.method static synthetic sL(Lcom/android/settings/applications/RunningState;Landroid/content/Context;Landroid/app/ActivityManager;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/applications/RunningState;->sK(Landroid/content/Context;Landroid/app/ActivityManager;)Z

    move-result v0

    return v0
.end method

.method static synthetic sM(Lcom/android/settings/applications/RunningState;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/RunningState;->sJ()V

    return-void
.end method


# virtual methods
.method pause()V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/android/settings/applications/RunningState;->vh:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/applications/RunningState;->vG:Lcom/android/settings/applications/RunningState$OnRefreshUiListener;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vr:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method sA()V
    .locals 3

    iget-object v1, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vn:Lcom/android/settings/applications/RunningState$BackgroundHandler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settings/applications/RunningState$BackgroundHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vn:Lcom/android/settings/applications/RunningState$BackgroundHandler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settings/applications/RunningState$BackgroundHandler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method sB()Ljava/util/ArrayList;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/applications/RunningState;->vP:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method sC()Ljava/util/ArrayList;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/applications/RunningState;->vx:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method sE(Lcom/android/settings/applications/RunningState$OnRefreshUiListener;)V
    .locals 4

    iget-object v1, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/settings/applications/RunningState;->vh:Z

    iput-object p1, p0, Lcom/android/settings/applications/RunningState;->vG:Lcom/android/settings/applications/RunningState$OnRefreshUiListener;

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vO:Lcom/android/settings/applications/RunningState$UserManagerBroadcastReceiver;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState$UserManagerBroadcastReceiver;->sN()Z

    move-result v0

    iget-object v2, p0, Lcom/android/settings/applications/RunningState;->vu:Lcom/android/settingslib/b/F;

    iget-object v3, p0, Lcom/android/settings/applications/RunningState;->vm:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/settingslib/b/F;->cfL(Landroid/content/res/Resources;)Z

    move-result v2

    if-nez v0, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/RunningState;->vs:Z

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vn:Lcom/android/settings/applications/RunningState$BackgroundHandler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/settings/applications/RunningState$BackgroundHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vn:Lcom/android/settings/applications/RunningState$BackgroundHandler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settings/applications/RunningState$BackgroundHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vn:Lcom/android/settings/applications/RunningState$BackgroundHandler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/settings/applications/RunningState$BackgroundHandler;->sendEmptyMessage(I)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vn:Lcom/android/settings/applications/RunningState$BackgroundHandler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settings/applications/RunningState$BackgroundHandler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vn:Lcom/android/settings/applications/RunningState$BackgroundHandler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/android/settings/applications/RunningState$BackgroundHandler;->sendEmptyMessage(I)Z

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->vr:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method sF()V
    .locals 4

    iget-object v1, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/android/settings/applications/RunningState;->vs:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method sG(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iput-boolean p1, p0, Lcom/android/settings/applications/RunningState;->vQ:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method sH()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lcom/android/settings/applications/RunningState;->vs:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
