.class public Lcom/android/settings/applications/PremiumSmsAccess;
.super Lcom/android/settings/notification/EmptyTextSettings;
.source "PremiumSmsAccess.java"

# interfaces
.implements Lcom/android/settings/applications/AppStateBaseBridge$Callback;
.implements Lcom/android/settingslib/b/f;
.implements Landroid/support/v7/preference/f;


# instance fields
.field private pB:Lcom/android/settingslib/b/a;

.field private pC:Lcom/android/settingslib/b/b;

.field private pD:Lcom/android/settings/applications/AppStateSmsPremBridge;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/EmptyTextSettings;-><init>()V

    return-void
.end method

.method private pa()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pC:Lcom/android/settingslib/b/b;

    sget-object v1, Lcom/android/settings/applications/AppStateSmsPremBridge;->uc:Lcom/android/settingslib/b/i;

    sget-object v2, Lcom/android/settingslib/b/a;->cAa:Ljava/util/Comparator;

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/b/b;->cfx(Lcom/android/settingslib/b/i;Ljava/util/Comparator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/applications/PremiumSmsAccess;->pb(Ljava/util/ArrayList;)V

    return-void
.end method

.method private pb(Ljava/util/ArrayList;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return-void

    :cond_0
    const v0, 0x7f120d01

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/PremiumSmsAccess;->fo(I)V

    invoke-virtual {p0}, Lcom/android/settings/applications/PremiumSmsAccess;->dju()Landroid/support/v7/preference/k;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/applications/PremiumSmsAccess;->aki()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/k;->dlN(Landroid/content/Context;)Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/support/v7/preference/PreferenceScreen;->dlo(Z)V

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    new-instance v4, Lcom/android/settings/applications/PremiumSmsAccess$PremiumSmsPreference;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    invoke-virtual {p0}, Lcom/android/settings/applications/PremiumSmsAccess;->aki()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, p0, v0, v5}, Lcom/android/settings/applications/PremiumSmsAccess$PremiumSmsPreference;-><init>(Lcom/android/settings/applications/PremiumSmsAccess;Lcom/android/settingslib/b/h;Landroid/content/Context;)V

    invoke-virtual {v4, p0}, Lcom/android/settings/applications/PremiumSmsAccess$PremiumSmsPreference;->dkJ(Landroid/support/v7/preference/f;)V

    invoke-virtual {v3, v4}, Landroid/support/v7/preference/PreferenceScreen;->im(Landroid/support/v7/preference/Preference;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/android/settings/DividerPreference;

    invoke-virtual {p0}, Lcom/android/settings/applications/PremiumSmsAccess;->aki()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/DividerPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcom/android/settings/DividerPreference;->boV(Z)V

    const v1, 0x7f120d02

    invoke-virtual {v0, v1}, Lcom/android/settings/DividerPreference;->dks(I)V

    invoke-virtual {v0, v6}, Lcom/android/settings/DividerPreference;->bSZ(Z)V

    invoke-virtual {v3, v0}, Landroid/support/v7/preference/PreferenceScreen;->im(Landroid/support/v7/preference/Preference;)Z

    :cond_2
    invoke-virtual {p0, v3}, Lcom/android/settings/applications/PremiumSmsAccess;->ajw(Landroid/support/v7/preference/PreferenceScreen;)V

    return-void
.end method


# virtual methods
.method public eH(Landroid/support/v7/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    check-cast p1, Lcom/android/settings/applications/PremiumSmsAccess$PremiumSmsPreference;

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {p1}, Lcom/android/settings/applications/PremiumSmsAccess$PremiumSmsPreference;->pd(Lcom/android/settings/applications/PremiumSmsAccess$PremiumSmsPreference;)Lcom/android/settingslib/b/h;

    move-result-object v1

    iget-object v1, v1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/applications/PremiumSmsAccess;->logSpecialPermissionChange(ILjava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pD:Lcom/android/settings/applications/AppStateSmsPremBridge;

    invoke-static {p1}, Lcom/android/settings/applications/PremiumSmsAccess$PremiumSmsPreference;->pd(Lcom/android/settings/applications/PremiumSmsAccess$PremiumSmsPreference;)Lcom/android/settingslib/b/h;

    move-result-object v2

    iget-object v2, v2, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/applications/AppStateSmsPremBridge;->se(Ljava/lang/String;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x184

    return v0
.end method

.method public in()V
    .locals 0

    return-void
.end method

.method public io()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/PremiumSmsAccess;->pa()V

    return-void
.end method

.method public ip()V
    .locals 0

    return-void
.end method

.method public iq()V
    .locals 0

    return-void
.end method

.method public ir()V
    .locals 0

    return-void
.end method

.method public is()V
    .locals 0

    return-void
.end method

.method public it(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public iu(Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/applications/PremiumSmsAccess;->pb(Ljava/util/ArrayList;)V

    return-void
.end method

.method public iv(Z)V
    .locals 0

    return-void
.end method

.method logSpecialPermissionChange(ILjava/lang/String;)V
    .locals 4

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/PremiumSmsAccess;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/applications/PremiumSmsAccess;->getContext()Landroid/content/Context;

    move-result-object v3

    new-array v1, v1, [Landroid/util/Pair;

    invoke-virtual {v2, v3, v0, p2, v1}, Lcom/android/settings/core/instrumentation/e;->ajQ(Landroid/content/Context;ILjava/lang/String;[Landroid/util/Pair;)V

    :cond_0
    return-void

    :pswitch_0
    const/16 v0, 0x30a

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x30b

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x30c

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/notification/EmptyTextSettings;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/PremiumSmsAccess;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/android/settingslib/b/a;->ceS(Landroid/app/Application;)Lcom/android/settingslib/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pB:Lcom/android/settingslib/b/a;

    iget-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pB:Lcom/android/settingslib/b/a;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/b/a;->ceZ(Lcom/android/settingslib/b/f;)Lcom/android/settingslib/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pC:Lcom/android/settingslib/b/b;

    new-instance v0, Lcom/android/settings/applications/AppStateSmsPremBridge;

    invoke-virtual {p0}, Lcom/android/settings/applications/PremiumSmsAccess;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pB:Lcom/android/settingslib/b/a;

    invoke-direct {v0, v1, v2, p0}, Lcom/android/settings/applications/AppStateSmsPremBridge;-><init>(Landroid/content/Context;Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iput-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pD:Lcom/android/settings/applications/AppStateSmsPremBridge;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pD:Lcom/android/settings/applications/AppStateSmsPremBridge;

    invoke-virtual {v0}, Lcom/android/settings/applications/AppStateSmsPremBridge;->vX()V

    iget-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pC:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfy()V

    invoke-super {p0}, Lcom/android/settings/notification/EmptyTextSettings;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pD:Lcom/android/settings/applications/AppStateSmsPremBridge;

    invoke-virtual {v0}, Lcom/android/settings/applications/AppStateSmsPremBridge;->pause()V

    iget-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pC:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->pause()V

    invoke-super {p0}, Lcom/android/settings/notification/EmptyTextSettings;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/notification/EmptyTextSettings;->onResume()V

    iget-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pC:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfv()V

    iget-object v0, p0, Lcom/android/settings/applications/PremiumSmsAccess;->pD:Lcom/android/settings/applications/AppStateSmsPremBridge;

    invoke-virtual {v0}, Lcom/android/settings/applications/AppStateSmsPremBridge;->vY()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/notification/EmptyTextSettings;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    return-void
.end method
