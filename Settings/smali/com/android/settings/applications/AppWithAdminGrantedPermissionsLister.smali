.class public abstract Lcom/android/settings/applications/AppWithAdminGrantedPermissionsLister;
.super Lcom/android/settings/applications/AppLister;
.source "AppWithAdminGrantedPermissionsLister.java"


# instance fields
.field private final FI:Lcom/android/settings/enterprise/q;

.field private final FJ:Lcom/android/settings/applications/IPackageManagerWrapper;

.field private final FK:[Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;Lcom/android/settings/applications/PackageManagerWrapper;Lcom/android/settings/applications/IPackageManagerWrapper;Lcom/android/settings/enterprise/q;Landroid/os/UserManager;)V
    .locals 0

    invoke-direct {p0, p2, p5}, Lcom/android/settings/applications/AppLister;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;Landroid/os/UserManager;)V

    iput-object p1, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsLister;->FK:[Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsLister;->FJ:Lcom/android/settings/applications/IPackageManagerWrapper;

    iput-object p4, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsLister;->FI:Lcom/android/settings/enterprise/q;

    return-void
.end method


# virtual methods
.method protected vu(Landroid/content/pm/ApplicationInfo;)Z
    .locals 4

    iget-object v0, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsLister;->FK:[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsLister;->FI:Lcom/android/settings/enterprise/q;

    iget-object v2, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsLister;->Ab:Lcom/android/settings/applications/PackageManagerWrapper;

    iget-object v3, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsLister;->FJ:Lcom/android/settings/applications/IPackageManagerWrapper;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;->vQ([Ljava/lang/String;Lcom/android/settings/enterprise/q;Lcom/android/settings/applications/PackageManagerWrapper;Lcom/android/settings/applications/IPackageManagerWrapper;Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    return v0
.end method
