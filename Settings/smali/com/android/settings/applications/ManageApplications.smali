.class public Lcom/android/settings/applications/ManageApplications;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "ManageApplications.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

.field static final xs:Z

.field public static final xt:[Lcom/android/settingslib/b/i;

.field public static final xu:[I

.field public static final xv:Ljava/util/Set;


# instance fields
.field private mRootView:Landroid/view/View;

.field public xA:I

.field private xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

.field private xC:Landroid/widget/Spinner;

.field private xD:Z

.field private xE:Landroid/view/LayoutInflater;

.field xF:Ljava/lang/CharSequence;

.field private xG:Landroid/view/View;

.field public xH:I

.field private xI:Landroid/widget/ListView;

.field private xJ:Landroid/view/View;

.field private xK:Lcom/android/settings/notification/NotificationBackend;

.field private xL:Landroid/view/Menu;

.field private xM:Lcom/android/settings/applications/ResetAppsHelper;

.field private xN:Z

.field private xO:I

.field private xP:Landroid/view/View;

.field private xQ:I

.field private xR:Ljava/lang/String;

.field public xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

.field private xx:Lcom/android/settingslib/b/a;

.field private xy:Ljava/lang/String;

.field private xz:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const v8, 0x7f1206ff

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x3

    const/4 v4, 0x0

    const-string/jumbo v0, "ManageApplications"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/applications/ManageApplications;->xs:Z

    const/16 v0, 0xd

    new-array v0, v0, [I

    sput-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const/16 v0, 0xd

    new-array v0, v0, [Lcom/android/settingslib/b/i;

    sput-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f120833

    aput v1, v0, v4

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    new-instance v1, Lcom/android/settingslib/b/k;

    sget-object v2, Lcom/android/settings/applications/AppStatePowerBridge;->oK:Lcom/android/settingslib/b/i;

    sget-object v3, Lcom/android/settingslib/b/a;->cAy:Lcom/android/settingslib/b/i;

    invoke-direct {v1, v2, v3}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    aput-object v1, v0, v4

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    aput v8, v0, v6

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    new-instance v1, Lcom/android/settingslib/b/k;

    sget-object v2, Lcom/android/settingslib/b/a;->cAx:Lcom/android/settingslib/b/i;

    sget-object v3, Lcom/android/settingslib/b/a;->cAy:Lcom/android/settingslib/b/i;

    invoke-direct {v1, v2, v3}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    aput-object v1, v0, v6

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    aput v8, v0, v7

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settingslib/b/a;->cAj:Lcom/android/settingslib/b/i;

    aput-object v1, v0, v7

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f120707

    aput v1, v0, v5

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settingslib/b/a;->cAy:Lcom/android/settingslib/b/i;

    aput-object v1, v0, v5

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f120702

    const/4 v2, 0x5

    aput v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settingslib/b/a;->czZ:Lcom/android/settingslib/b/i;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f120709

    const/4 v2, 0x4

    aput v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settingslib/b/a;->cAm:Lcom/android/settingslib/b/i;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f12070b

    const/4 v2, 0x6

    aput v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settings/applications/AppStateNotificationBridge;->yQ:Lcom/android/settingslib/b/i;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f120711

    const/4 v2, 0x7

    aput v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settingslib/b/a;->czR:Lcom/android/settingslib/b/i;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f120712

    const/16 v2, 0x8

    aput v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settingslib/b/a;->cAp:Lcom/android/settingslib/b/i;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const/16 v1, 0x9

    aput v8, v0, v1

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settings/applications/AppStateUsageBridge;->Aw:Lcom/android/settingslib/b/i;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f120710

    const/16 v2, 0xa

    aput v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settings/applications/AppStateOverlayBridge;->zw:Lcom/android/settingslib/b/i;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f120713

    const/16 v2, 0xb

    aput v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settings/applications/AppStateWriteSettingsBridge;->Ag:Lcom/android/settingslib/b/i;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xu:[I

    const v1, 0x7f120708

    const/16 v2, 0xc

    aput v1, v0, v2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    sget-object v1, Lcom/android/settings/applications/AppStateInstallAppsBridge;->AV:Lcom/android/settingslib/b/i;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    new-instance v0, Landroid/util/ArraySet;

    new-array v1, v7, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/util/ArraySet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/settings/applications/ManageApplications;->xv:Ljava/util/Set;

    new-instance v0, Lcom/android/settings/applications/ManageApplications$1;

    invoke-direct {v0}, Lcom/android/settings/applications/ManageApplications$1;-><init>()V

    sput-object v0, Lcom/android/settings/applications/ManageApplications;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const v0, 0x7f0a0419

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    return-void
.end method

.method static getCompositeFilter(IILjava/lang/String;)Lcom/android/settingslib/b/i;
    .locals 3

    new-instance v1, Lcom/android/settingslib/b/j;

    invoke-direct {v1, p2}, Lcom/android/settingslib/b/j;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/android/settingslib/b/k;

    sget-object v2, Lcom/android/settingslib/b/a;->cAw:Lcom/android/settingslib/b/i;

    invoke-direct {v0, v2, v1}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/settingslib/b/k;

    sget-object v2, Lcom/android/settingslib/b/a;->cAE:Lcom/android/settingslib/b/i;

    invoke-direct {v0, v2, v1}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x9

    if-ne p0, v0, :cond_2

    new-instance v0, Lcom/android/settingslib/b/k;

    sget-object v2, Lcom/android/settingslib/b/a;->czV:Lcom/android/settingslib/b/i;

    invoke-direct {v0, v2, v1}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    return-object v0

    :cond_2
    const/16 v0, 0xa

    if-ne p0, v0, :cond_3

    new-instance v0, Lcom/android/settingslib/b/k;

    sget-object v2, Lcom/android/settingslib/b/a;->cAq:Lcom/android/settingslib/b/i;

    invoke-direct {v0, v2, v1}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    return-object v0

    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method private tA()I
    .locals 1

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x2

    return v0

    :pswitch_0
    const/16 v0, 0x9

    return v0

    :pswitch_1
    const/4 v0, 0x0

    return v0

    :pswitch_2
    const/16 v0, 0xa

    return v0

    :pswitch_3
    const/16 v0, 0xb

    return v0

    :pswitch_4
    const/16 v0, 0xc

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private tB()Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    return v0

    :pswitch_1
    iget v1, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    const v2, 0x7f0a0419

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private tE(Ljava/lang/Class;I)V
    .locals 7

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications;->xy:Ljava/lang/String;

    iget v3, p0, Lcom/android/settings/applications/ManageApplications;->xz:I

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getMetricsCategory()I

    move-result v6

    const/4 v5, 0x1

    move-object v0, p1

    move v1, p2

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Lcom/android/settings/applications/AppInfoBase;->oM(Ljava/lang/Class;ILjava/lang/String;ILandroid/app/Fragment;II)V

    return-void
.end method

.method private tF()V
    .locals 3

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-class v0, Lcom/android/settings/applications/InstalledAppDetails;

    const v1, 0x7f120162

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications;->tE(Ljava/lang/Class;I)V

    :goto_0
    return-void

    :pswitch_1
    const-class v0, Lcom/android/settings/notification/AppNotificationSettings;

    const v1, 0x7f12014f

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications;->tE(Ljava/lang/Class;I)V

    goto :goto_0

    :pswitch_2
    const-class v0, Lcom/android/settings/applications/UsageAccessDetails;

    const v1, 0x7f12132a

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications;->tE(Ljava/lang/Class;I)V

    goto :goto_0

    :pswitch_3
    const-class v0, Lcom/android/settings/applications/AppStorageSettings;

    const v1, 0x7f1211a2

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications;->tE(Ljava/lang/Class;I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xy:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/android/settings/applications/ManageApplications;->xD:Z

    const/4 v2, 0x1

    invoke-static {p0, v0, v2, v1}, Lcom/android/settings/fuelgauge/HighPowerDetail;->Ip(Landroid/app/Fragment;Ljava/lang/String;IZ)V

    goto :goto_0

    :pswitch_5
    const-class v0, Lcom/android/settings/applications/DrawOverlayDetails;

    const v1, 0x7f120c32

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications;->tE(Ljava/lang/Class;I)V

    goto :goto_0

    :pswitch_6
    const-class v0, Lcom/android/settings/applications/WriteSettingsDetails;

    const v1, 0x7f121678

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications;->tE(Ljava/lang/Class;I)V

    goto :goto_0

    :pswitch_7
    const-class v0, Lcom/android/settings/applications/ExternalSourcesDetails;

    const v1, 0x7f12086b

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications;->tE(Ljava/lang/Class;I)V

    goto :goto_0

    :pswitch_8
    const-class v0, Lcom/android/settings/applications/AppStorageSettings;

    const v1, 0x7f1207c0

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications;->tE(Ljava/lang/Class;I)V

    goto :goto_0

    :pswitch_9
    const-class v0, Lcom/android/settings/applications/AppStorageSettings;

    const v1, 0x7f12119b

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications;->tE(Ljava/lang/Class;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method static synthetic tI(Lcom/android/settings/applications/ManageApplications;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xy:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic tJ(Lcom/android/settings/applications/ManageApplications;)Landroid/widget/Spinner;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xC:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic tK(Lcom/android/settings/applications/ManageApplications;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xE:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic tL(Lcom/android/settings/applications/ManageApplications;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xG:Landroid/view/View;

    return-object v0
.end method

.method static synthetic tM(Lcom/android/settings/applications/ManageApplications;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xI:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic tN(Lcom/android/settings/applications/ManageApplications;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xJ:Landroid/view/View;

    return-object v0
.end method

.method static synthetic tO(Lcom/android/settings/applications/ManageApplications;)Lcom/android/settings/notification/NotificationBackend;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xK:Lcom/android/settings/notification/NotificationBackend;

    return-object v0
.end method

.method static synthetic tP(Lcom/android/settings/applications/ManageApplications;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    return v0
.end method

.method static synthetic tQ(Lcom/android/settings/applications/ManageApplications;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xP:Landroid/view/View;

    return-object v0
.end method

.method private tz()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->mRootView:Landroid/view/View;

    const v2, 0x7f0a0321

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0d003c

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xP:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xP:Landroid/view/View;

    const v2, 0x7f0a018e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xC:Landroid/widget/Spinner;

    new-instance v1, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    invoke-direct {v1, p0}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;-><init>(Lcom/android/settings/applications/ManageApplications;)V

    iput-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xC:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xC:Landroid/widget/Spinner;

    invoke-virtual {v1, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xP:Landroid/view/View;

    invoke-virtual {v0, v1, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    invoke-direct {p0}, Lcom/android/settings/applications/ManageApplications;->tA()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;->tS(I)V

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserProfiles()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;->tS(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;->tS(I)V

    :cond_0
    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;->tS(I)V

    :cond_1
    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    invoke-virtual {v0, v4}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;->tS(I)V

    :cond_2
    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    iget v1, p0, Lcom/android/settings/applications/ManageApplications;->xQ:I

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications;->xR:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/applications/ManageApplications;->getCompositeFilter(IILjava/lang/String;)Lcom/android/settingslib/b/i;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-virtual {v1, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uh(Lcom/android/settingslib/b/i;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 2

    const/16 v1, 0xdd

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const/16 v0, 0x41

    return v0

    :pswitch_2
    const/16 v0, 0x85

    return v0

    :pswitch_3
    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xQ:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x347

    return v0

    :cond_0
    const/16 v0, 0xb6

    return v0

    :pswitch_4
    const/16 v0, 0x346

    return v0

    :pswitch_5
    const/16 v0, 0x3a7

    return v0

    :pswitch_6
    const/16 v0, 0x5f

    return v0

    :pswitch_7
    const/16 v0, 0xb8

    return v0

    :pswitch_8
    return v1

    :pswitch_9
    return v1

    :pswitch_a
    const/16 v0, 0x328

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xy:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ur(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Lcom/android/settings/applications/AppStateBaseBridge;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xy:Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/applications/ManageApplications;->xz:I

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/AppStateBaseBridge;->vZ(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    :cond_2
    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications;->xD:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xx:Lcom/android/settingslib/b/a;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xy:Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/applications/ManageApplications;->xz:I

    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/b/a;->cfk(Ljava/lang/String;I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ur(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Lcom/android/settings/applications/AppStateBaseBridge;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xy:Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/applications/ManageApplications;->xz:I

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/AppStateBaseBridge;->vZ(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const v5, 0x7f0a041a

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v3}, Lcom/android/settings/applications/ManageApplications;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settingslib/b/a;->ceS(Landroid/app/Application;)Lcom/android/settingslib/b/a;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xx:Lcom/android/settingslib/b/a;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v0, "classname"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-class v1, Lcom/android/settings/Settings$AllApplicationsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iput-boolean v3, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/applications/ManageApplications;->tA()I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xA:I

    if-eqz p1, :cond_2

    const-string/jumbo v0, "sortOrder"

    iget v1, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    const-string/jumbo v0, "showSystem"

    iget-boolean v1, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f120881

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xF:Ljava/lang/CharSequence;

    new-instance v0, Lcom/android/settings/applications/ResetAppsHelper;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/applications/ResetAppsHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xM:Lcom/android/settings/applications/ResetAppsHelper;

    return-void

    :cond_3
    const-class v1, Lcom/android/settings/Settings$NotificationAppListActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    instance-of v1, p0, Lcom/android/settings/applications/NotificationApps;

    if-eqz v1, :cond_5

    :cond_4
    iput v3, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    new-instance v0, Lcom/android/settings/notification/NotificationBackend;

    invoke-direct {v0}, Lcom/android/settings/notification/NotificationBackend;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xK:Lcom/android/settings/notification/NotificationBackend;

    goto :goto_0

    :cond_5
    const-class v1, Lcom/android/settings/Settings$StorageUseActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    if-eqz v2, :cond_6

    const-string/jumbo v0, "volumeUuid"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "volumeUuid"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xR:Ljava/lang/String;

    const-string/jumbo v0, "storageType"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xQ:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    :goto_1
    iput v5, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    goto :goto_0

    :cond_6
    iput v4, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    goto :goto_1

    :cond_7
    const-class v1, Lcom/android/settings/Settings$UsageAccessSettingsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    goto/16 :goto_0

    :cond_8
    const-class v1, Lcom/android/settings/Settings$HighPowerApplicationsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    iput-boolean v3, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    goto/16 :goto_0

    :cond_9
    const-class v1, Lcom/android/settings/Settings$OverlaySettingsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v0, 0x6

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    goto/16 :goto_0

    :cond_a
    const-class v1, Lcom/android/settings/Settings$WriteSettingsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v0, 0x7

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    goto/16 :goto_0

    :cond_b
    const-class v1, Lcom/android/settings/Settings$ManageExternalSourcesActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v0, 0x8

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    goto/16 :goto_0

    :cond_c
    const-class v1, Lcom/android/settings/aE;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v0, 0x9

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    iput v5, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    goto/16 :goto_0

    :cond_d
    const-class v1, Lcom/android/settings/aF;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    iput v5, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    goto/16 :goto_0

    :cond_e
    iput v4, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-nez v0, :cond_1

    const v0, 0x7f120809

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, v0, v2}, Lcom/android/settingslib/B;->crK(Landroid/app/Activity;Landroid/view/Menu;ILjava/lang/String;)Z

    iput-object p1, p0, Lcom/android/settings/applications/ManageApplications;->xL:Landroid/view/Menu;

    const v0, 0x7f0e0002

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->tG()V

    return-void

    :cond_1
    const v0, 0x7f12080e

    goto :goto_0
.end method

.method public onDestroyOptionsMenu()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xL:Landroid/view/Menu;

    return-void
.end method

.method public onDestroyView()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-virtual {v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ue()V

    :cond_0
    iput-object v1, p0, Lcom/android/settings/applications/ManageApplications;->mRootView:Landroid/view/View;

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    iput-object p1, p0, Lcom/android/settings/applications/ManageApplications;->xE:Landroid/view/LayoutInflater;

    const v0, 0x7f0d00e7

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->mRootView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->mRootView:Landroid/view/View;

    const v1, 0x7f0a0271

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xJ:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xJ:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->mRootView:Landroid/view/View;

    const v1, 0x7f0a026b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xG:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xG:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xG:Landroid/view/View;

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xG:Landroid/view/View;

    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setSaveEnabled(Z)V

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xI:Landroid/widget/ListView;

    new-instance v0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xx:Lcom/android/settingslib/b/a;

    iget v2, p0, Lcom/android/settings/applications/ManageApplications;->xA:I

    invoke-direct {v0, v1, p0, v2}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;-><init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/ManageApplications;I)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    const-string/jumbo v1, "hasEntries"

    invoke-virtual {p3, v1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ux(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;Z)Z

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    const-string/jumbo v1, "hasBridge"

    invoke-virtual {p3, v1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uw(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;Z)Z

    :cond_1
    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xQ:I

    if-ne v0, v6, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    new-instance v2, Lcom/android/settings/applications/MusicViewHolderController;

    new-instance v3, Lcom/android/settingslib/b/H;

    invoke-direct {v3, v0}, Lcom/android/settingslib/b/H;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/android/settings/applications/ManageApplications;->xR:Ljava/lang/String;

    iget v5, p0, Lcom/android/settings/applications/ManageApplications;->xz:I

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    invoke-static {v5}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v5

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/android/settings/applications/MusicViewHolderController;-><init>(Landroid/content/Context;Lcom/android/settingslib/b/H;Ljava/lang/String;Landroid/os/UserHandle;)V

    invoke-virtual {v1, v2}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ui(Lcom/android/settings/applications/FileViewHolderController;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xI:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xI:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xI:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/android/settings/applications/ManageApplications;->tB()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->mRootView:Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xI:Landroid/widget/ListView;

    invoke-static {p2, v0, v1, v7}, Lcom/android/settings/aq;->bqJ(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Z)V

    :cond_3
    instance-of v0, p2, Landroid/preference/PreferenceFrameLayout;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->mRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    iput-boolean v6, v0, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    :cond_4
    invoke-direct {p0}, Lcom/android/settings/applications/ManageApplications;->tz()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xM:Lcom/android/settings/applications/ResetAppsHelper;

    invoke-virtual {v0, p3}, Lcom/android/settings/applications/ResetAppsHelper;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-virtual {v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->tY()I

    move-result v0

    if-le v0, p3, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-virtual {v0, p3}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->tX(I)Lcom/android/settingslib/b/h;

    move-result-object v0

    iget-object v1, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xy:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xz:I

    invoke-direct {p0}, Lcom/android/settings/applications/ManageApplications;->tF()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->us(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Lcom/android/settings/applications/FileViewHolderController;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/android/settings/applications/FileViewHolderController;->wX(Landroid/app/Fragment;)V

    goto :goto_0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    invoke-virtual {v0, p3}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;->tT(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xA:I

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    iget v1, p0, Lcom/android/settings/applications/ManageApplications;->xA:I

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uj(I)V

    sget-boolean v0, Lcom/android/settings/applications/ManageApplications;->xs:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ManageApplications"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Selecting filter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/applications/ManageApplications;->xA:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    return v2

    :sswitch_0
    iput v0, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xI:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/android/settings/applications/ManageApplications;->tB()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    iget v1, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ub(I)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->tG()V

    return v8

    :sswitch_1
    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-virtual {v0, v2}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xM:Lcom/android/settings/applications/ResetAppsHelper;

    invoke-virtual {v0}, Lcom/android/settings/applications/ResetAppsHelper;->vd()V

    return v8

    :sswitch_3
    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-ne v0, v8, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    const-class v1, Lcom/android/settings/notification/ConfigureNotificationSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f12045c

    move-object v1, p0

    move-object v5, v3

    move-object v6, p0

    invoke-virtual/range {v0 .. v7}, Lcom/android/settings/bL;->Uv(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    :goto_1
    return v8

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    const-class v1, Lcom/android/settings/applications/AdvancedAppSettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f12045a

    move-object v1, p0

    move-object v5, v3

    move-object v6, p0

    invoke-virtual/range {v0 .. v7}, Lcom/android/settings/bL;->Uv(Landroid/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    goto :goto_1

    :sswitch_4
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    return v8

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    :cond_3
    :goto_2
    return v8

    :cond_4
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_4
        0x7f0a004a -> :sswitch_3
        0x7f0a01e4 -> :sswitch_1
        0x7f0a0389 -> :sswitch_2
        0x7f0a0402 -> :sswitch_1
        0x7f0a0419 -> :sswitch_0
        0x7f0a041a -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-virtual {v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->pause()V

    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->tG()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->tH()V

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->tG()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    iget v1, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ug(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uz(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xM:Lcom/android/settings/applications/ResetAppsHelper;

    invoke-virtual {v0, p1}, Lcom/android/settings/applications/ResetAppsHelper;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "sortOrder"

    iget v1, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "showSystem"

    iget-boolean v1, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "hasEntries"

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-static {v1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uu(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "hasBridge"

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications;->xw:Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;

    invoke-static {v1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ut(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStop()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xM:Lcom/android/settings/applications/ResetAppsHelper;

    invoke-virtual {v0}, Lcom/android/settings/applications/ResetAppsHelper;->stop()V

    return-void
.end method

.method public tC(Z)V
    .locals 2

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;->tV(IZ)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;->tV(IZ)V

    return-void
.end method

.method public tD(Z)V
    .locals 2

    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xv:Ljava/util/Set;

    iget v1, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xB:Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/applications/ManageApplications$FilterSpinnerAdapter;->tV(IZ)V

    :cond_0
    return-void
.end method

.method tG()V
    .locals 10

    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xL:Landroid/view/Menu;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications;->xL:Landroid/view/Menu;

    const v3, 0x7f0a004a

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/applications/ManageApplications;->xL:Landroid/view/Menu;

    const v4, 0x7f0a0419

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/applications/ManageApplications;->xL:Landroid/view/Menu;

    const v5, 0x7f0a041a

    invoke-interface {v4, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/applications/ManageApplications;->xL:Landroid/view/Menu;

    const v6, 0x7f0a0402

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iget-object v6, p0, Lcom/android/settings/applications/ManageApplications;->xL:Landroid/view/Menu;

    const v7, 0x7f0a01e4

    invoke-interface {v6, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    if-eqz v0, :cond_1

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    if-eqz v3, :cond_2

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-ne v0, v8, :cond_8

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    const v7, 0x7f0a0419

    if-eq v0, v7, :cond_7

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    if-eqz v4, :cond_3

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-ne v0, v8, :cond_a

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xO:I

    const v3, 0x7f0a041a

    if-eq v0, v3, :cond_9

    move v0, v1

    :goto_1
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    if-eqz v5, :cond_4

    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    if-nez v0, :cond_c

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-eq v0, v9, :cond_b

    move v0, v1

    :goto_2
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_4
    if-eqz v6, :cond_6

    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications;->xN:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/android/settings/applications/ManageApplications;->xH:I

    if-eq v0, v9, :cond_5

    move v2, v1

    :cond_5
    invoke-interface {v6, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_6
    return-void

    :cond_7
    move v0, v2

    goto :goto_0

    :cond_8
    move v0, v2

    goto :goto_0

    :cond_9
    move v0, v2

    goto :goto_1

    :cond_a
    move v0, v2

    goto :goto_1

    :cond_b
    move v0, v2

    goto :goto_2

    :cond_c
    move v0, v2

    goto :goto_2
.end method

.method public tH()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->tG()V

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method
