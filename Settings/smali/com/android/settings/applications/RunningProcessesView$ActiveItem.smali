.class public Lcom/android/settings/applications/RunningProcessesView$ActiveItem;
.super Ljava/lang/Object;
.source "RunningProcessesView.java"


# instance fields
.field mRootView:Landroid/view/View;

.field uO:J

.field uP:Lcom/android/settings/applications/RunningState$BaseItem;

.field uQ:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

.field uR:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method st(Landroid/content/Context;Ljava/lang/StringBuilder;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    instance-of v0, v0, Lcom/android/settings/applications/RunningState$ServiceItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uQ:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    iget-object v0, v0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uS:Landroid/widget/TextView;

    move-object v3, v0

    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    iput-boolean v2, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uR:Z

    iget-wide v4, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uO:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uO:J

    sub-long/2addr v0, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-static {p2, v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(Ljava/lang/StringBuilder;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$BaseItem;->wc:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$BaseItem;->wc:Ljava/lang/String;

    :goto_2
    iget-object v4, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    iget-object v4, v4, Lcom/android/settings/applications/RunningState$BaseItem;->wd:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    iput-object v0, v4, Lcom/android/settings/applications/RunningState$BaseItem;->wd:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uQ:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    iget-object v4, v4, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uS:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    iget-boolean v0, v0, Lcom/android/settings/applications/RunningState$BaseItem;->wa:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uR:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uR:Z

    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uQ:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    iget-object v0, v0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uT:Landroid/widget/TextView;

    const-string/jumbo v4, ""

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    const-string/jumbo v0, ""

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    instance-of v0, v0, Lcom/android/settings/applications/RunningState$MergedItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uQ:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    iget-object v0, v0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uT:Landroid/widget/TextView;

    move-object v3, v0

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    instance-of v0, v0, Lcom/android/settings/applications/RunningState$MergedItem;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    move v0, v1

    :goto_3
    if-eqz v0, :cond_8

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f121025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    const-string/jumbo v0, ""

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_3
.end method
