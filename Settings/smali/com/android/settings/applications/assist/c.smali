.class public Lcom/android/settings/applications/assist/c;
.super Lcom/android/settings/applications/defaultapps/c;
.source "DefaultVoiceInputPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;


# instance fields
.field private ns:Lcom/android/internal/app/AssistUtils;

.field private nt:Lcom/android/settings/applications/assist/m;

.field private nu:Landroid/preference/Preference;

.field private nv:Landroid/preference/PreferenceScreen;

.field private nw:Lcom/android/settings/applications/assist/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/applications/defaultapps/c;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/applications/assist/d;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/assist/d;-><init>(Lcom/android/settings/applications/assist/c;)V

    iput-object v0, p0, Lcom/android/settings/applications/assist/c;->nw:Lcom/android/settings/applications/assist/d;

    new-instance v0, Lcom/android/internal/app/AssistUtils;

    invoke-direct {v0, p1}, Lcom/android/internal/app/AssistUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/assist/c;->ns:Lcom/android/internal/app/AssistUtils;

    new-instance v0, Lcom/android/settings/applications/assist/m;

    invoke-direct {v0, p1}, Lcom/android/settings/applications/assist/m;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/assist/c;->nt:Lcom/android/settings/applications/assist/m;

    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nt:Lcom/android/settings/applications/assist/m;

    invoke-virtual {v0}, Lcom/android/settings/applications/assist/m;->nc()V

    if-eqz p2, :cond_0

    invoke-virtual {p2, p0}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    :cond_0
    return-void
.end method

.method private mJ()Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nt:Lcom/android/settings/applications/assist/m;

    invoke-static {v0}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mO(Lcom/android/settings/applications/assist/m;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private mK()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nu:Landroid/preference/Preference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nt:Lcom/android/settings/applications/assist/m;

    invoke-virtual {v0}, Lcom/android/settings/applications/assist/m;->nc()V

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/c;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nv:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/c;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nv:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/applications/assist/c;->nu:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nv:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/applications/assist/c;->nu:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method static synthetic mL(Lcom/android/settings/applications/assist/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/assist/c;->mK()V

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nu:Landroid/preference/Preference;

    invoke-super {p0, v0}, Lcom/android/settings/applications/defaultapps/c;->cz(Landroid/preference/Preference;)V

    invoke-direct {p0}, Lcom/android/settings/applications/assist/c;->mK()V

    return-void
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/applications/assist/c;->nv:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/c;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/assist/c;->nu:Landroid/preference/Preference;

    invoke-super {p0, p1}, Lcom/android/settings/applications/defaultapps/c;->i(Landroid/preference/PreferenceScreen;)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "voice_input_settings"

    return-object v0
.end method

.method protected mr()Lcom/android/settings/applications/defaultapps/a;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/android/settings/applications/assist/c;->mJ()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    return-object v4

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nt:Lcom/android/settings/applications/assist/m;

    iget-object v0, v0, Lcom/android/settings/applications/assist/m;->nX:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/assist/o;

    iget-object v3, v0, Lcom/android/settings/applications/assist/o;->key:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v1, Lcom/android/settings/applications/assist/e;

    iget-object v2, p0, Lcom/android/settings/applications/assist/c;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v3, p0, Lcom/android/settings/applications/assist/c;->mUserId:I

    invoke-direct {v1, v2, v3, v0, v5}, Lcom/android/settings/applications/assist/e;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;ILcom/android/settings/applications/assist/n;Z)V

    return-object v1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nt:Lcom/android/settings/applications/assist/m;

    iget-object v0, v0, Lcom/android/settings/applications/assist/m;->nY:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/assist/p;

    iget-object v3, v0, Lcom/android/settings/applications/assist/p;->key:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v1, Lcom/android/settings/applications/assist/e;

    iget-object v2, p0, Lcom/android/settings/applications/assist/c;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v3, p0, Lcom/android/settings/applications/assist/c;->mUserId:I

    invoke-direct {v1, v2, v3, v0, v5}, Lcom/android/settings/applications/assist/e;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;ILcom/android/settings/applications/assist/n;Z)V

    return-object v1

    :cond_4
    return-object v4
.end method

.method protected ms(Lcom/android/settings/applications/defaultapps/a;)Landroid/content/Intent;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/assist/c;->mr()Lcom/android/settings/applications/defaultapps/a;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/android/settings/applications/assist/e;

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    return-object v2

    :cond_1
    check-cast v0, Lcom/android/settings/applications/assist/e;

    invoke-virtual {v0}, Lcom/android/settings/applications/assist/e;->mR()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nw:Lcom/android/settings/applications/assist/d;

    iget-object v1, p0, Lcom/android/settings/applications/assist/c;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/assist/d;->mT(Landroid/content/ContentResolver;Z)V

    return-void
.end method

.method public onResume()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nw:Lcom/android/settings/applications/assist/d;

    iget-object v1, p0, Lcom/android/settings/applications/assist/c;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/assist/d;->mT(Landroid/content/ContentResolver;Z)V

    invoke-direct {p0}, Lcom/android/settings/applications/assist/c;->mK()V

    return-void
.end method

.method public p()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/assist/c;->nt:Lcom/android/settings/applications/assist/m;

    invoke-static {v0}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mO(Lcom/android/settings/applications/assist/m;)Landroid/content/ComponentName;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/assist/c;->ns:Lcom/android/internal/app/AssistUtils;

    iget v2, p0, Lcom/android/settings/applications/assist/c;->mUserId:I

    invoke-virtual {v1, v2}, Lcom/android/internal/app/AssistUtils;->getAssistComponentForUser(I)Landroid/content/ComponentName;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settings/applications/assist/DefaultVoiceInputPicker;->mP(Landroid/content/ComponentName;Landroid/content/ComponentName;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
