.class public Lcom/android/settings/applications/RunningProcessesView$ViewHolder;
.super Ljava/lang/Object;
.source "RunningProcessesView.java"


# instance fields
.field public uS:Landroid/widget/TextView;

.field public uT:Landroid/widget/TextView;

.field public uU:Landroid/widget/TextView;

.field public uV:Landroid/widget/ImageView;

.field public uW:Landroid/widget/TextView;

.field public uX:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uX:Landroid/view/View;

    const v0, 0x7f0a01f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uV:Landroid/widget/ImageView;

    const v0, 0x7f0a02b7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uW:Landroid/widget/TextView;

    const v0, 0x7f0a0130

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uU:Landroid/widget/TextView;

    const v0, 0x7f0a040c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uS:Landroid/widget/TextView;

    const v0, 0x7f0a04d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uT:Landroid/widget/TextView;

    invoke-virtual {p1, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public su(Lcom/android/settings/applications/RunningState;Lcom/android/settings/applications/RunningState$BaseItem;Ljava/lang/StringBuilder;)Lcom/android/settings/applications/RunningProcessesView$ActiveItem;
    .locals 10

    iget-object v3, p1, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uX:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v2, p2, Lcom/android/settings/applications/RunningState$BaseItem;->wb:Landroid/content/pm/PackageItemInfo;

    if-nez v2, :cond_0

    instance-of v2, p2, Lcom/android/settings/applications/RunningState$MergedItem;

    if-eqz v2, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    move-object v2, v0

    iget-object v2, v2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    if-eqz v2, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    move-object v2, v0

    iget-object v2, v2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    invoke-virtual {v2, v4}, Lcom/android/settings/applications/RunningState$ProcessItem;->sO(Landroid/content/pm/PackageManager;)V

    move-object v0, p2

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    move-object v2, v0

    iget-object v2, v2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v2, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->wb:Landroid/content/pm/PackageItemInfo;

    iput-object v2, p2, Lcom/android/settings/applications/RunningState$BaseItem;->wb:Landroid/content/pm/PackageItemInfo;

    move-object v0, p2

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    move-object v2, v0

    iget-object v2, v2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v2, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->we:Ljava/lang/CharSequence;

    iput-object v2, p2, Lcom/android/settings/applications/RunningState$BaseItem;->we:Ljava/lang/CharSequence;

    :cond_0
    iget-object v2, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uW:Landroid/widget/TextView;

    iget-object v5, p2, Lcom/android/settings/applications/RunningState$BaseItem;->we:Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    invoke-direct {v5}, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;-><init>()V

    iget-object v2, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uX:Landroid/view/View;

    iput-object v2, v5, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mRootView:Landroid/view/View;

    iput-object p2, v5, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    iput-object p0, v5, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uQ:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    iget-wide v6, p2, Lcom/android/settings/applications/RunningState$BaseItem;->wf:J

    iput-wide v6, v5, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uO:J

    iget-boolean v2, p2, Lcom/android/settings/applications/RunningState$BaseItem;->wa:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uU:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uX:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f1203ac

    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const/4 v2, 0x0

    iput-object v2, p2, Lcom/android/settings/applications/RunningState$BaseItem;->wd:Ljava/lang/String;

    iget-object v2, p2, Lcom/android/settings/applications/RunningState$BaseItem;->wb:Landroid/content/pm/PackageItemInfo;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uX:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v6, p2, Lcom/android/settings/applications/RunningState$BaseItem;->wb:Landroid/content/pm/PackageItemInfo;

    const-wide/32 v8, 0x927c0

    invoke-static {v2, v6, v4, v8, v9}, Lmiui/maml/util/AppIconsHelper;->getIconDrawable(Landroid/content/Context;Landroid/content/pm/PackageItemInfo;Landroid/content/pm/PackageManager;J)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget v4, p2, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    const/16 v6, 0x3e7

    if-ne v4, v6, :cond_1

    iget-object v4, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uX:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lmiui/securityspace/XSpaceUserHandle;->getXSpaceIcon(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    :cond_1
    iget-object v4, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uV:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    iget-object v2, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uV:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uX:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v5, v2, p3}, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->st(Landroid/content/Context;Ljava/lang/StringBuilder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-object v5

    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uU:Landroid/widget/TextView;

    iget-object v6, p2, Lcom/android/settings/applications/RunningState$BaseItem;->wg:Ljava/lang/String;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method
