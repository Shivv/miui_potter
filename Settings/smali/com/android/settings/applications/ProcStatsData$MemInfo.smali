.class public Lcom/android/settings/applications/ProcStatsData$MemInfo;
.super Ljava/lang/Object;
.source "ProcStatsData.java"


# instance fields
.field FA:D

.field FB:D

.field FC:D

.field FD:J

.field FE:J

.field FF:D

.field FG:[D

.field FH:D

.field public Fx:D

.field public Fy:D

.field public Fz:D


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;J)V
    .locals 11

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xe

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FG:[D

    iput-wide p3, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FD:J

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/applications/ProcStatsData$MemInfo;->zM(Landroid/content/Context;Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;J)V

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FA:D

    mul-double/2addr v0, v8

    long-to-double v2, p3

    div-double/2addr v0, v2

    iget-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FF:D

    mul-double/2addr v2, v8

    long-to-double v4, p3

    div-double/2addr v2, v4

    add-double v4, v0, v2

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FH:D

    iget-wide v4, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fy:D

    iget-wide v6, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FH:D

    div-double/2addr v4, v6

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FC:D

    iget-wide v4, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FC:D

    long-to-double v6, p3

    div-double/2addr v4, v6

    mul-double/2addr v4, v8

    iput-wide v4, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FB:D

    iget-wide v4, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FC:D

    mul-double/2addr v0, v4

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fx:D

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FC:D

    mul-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fz:D

    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    const-string/jumbo v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide v4, v1, Landroid/app/ActivityManager$MemoryInfo;->hiddenAppThreshold:J

    long-to-double v4, v4

    iget-wide v6, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fz:D

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_0

    iput-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fx:D

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fz:D

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fz:D

    double-to-long v0, v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FE:J

    :goto_0
    return-void

    :cond_0
    iget-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fx:D

    iget-wide v4, v1, Landroid/app/ActivityManager$MemoryInfo;->hiddenAppThreshold:J

    long-to-double v4, v4

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fx:D

    iget-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fz:D

    iget-wide v4, v1, Landroid/app/ActivityManager$MemoryInfo;->hiddenAppThreshold:J

    long-to-double v4, v4

    sub-double/2addr v2, v4

    iput-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fz:D

    iget-wide v0, v1, Landroid/app/ActivityManager$MemoryInfo;->hiddenAppThreshold:J

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FE:J

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;JLcom/android/settings/applications/ProcStatsData$MemInfo;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/applications/ProcStatsData$MemInfo;-><init>(Landroid/content/Context;Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;J)V

    return-void
.end method

.method private zM(Landroid/content/Context;Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;J)V
    .locals 6

    new-instance v0, Lcom/android/internal/util/MemInfoReader;

    invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getTotalSize()J

    move-result-wide v0

    long-to-double v0, v0

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fy:D

    iget-wide v0, p2, Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;->sysMemFreeWeight:D

    iget-wide v2, p2, Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;->sysMemCachedWeight:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FF:D

    iget-wide v0, p2, Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;->sysMemKernelWeight:D

    iget-wide v2, p2, Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;->sysMemNativeWeight:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FA:D

    iget-boolean v0, p2, Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;->hasSwappedOutPss:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FA:D

    iget-wide v2, p2, Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;->sysMemZRamWeight:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FA:D

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FG:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FG:[D

    iget-object v2, p2, Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;->processStateWeight:[D

    aget-wide v2, v2, v0

    aput-wide v2, v1, v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_2

    iget-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FF:D

    iget-object v1, p2, Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;->processStateWeight:[D

    aget-wide v4, v1, v0

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FF:D

    goto :goto_1

    :cond_2
    iget-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FA:D

    iget-object v1, p2, Lcom/android/internal/app/procstats/ProcessStats$TotalMemoryUseCollection;->processStateWeight:[D

    aget-wide v4, v1, v0

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FA:D

    goto :goto_1

    :cond_3
    return-void
.end method
