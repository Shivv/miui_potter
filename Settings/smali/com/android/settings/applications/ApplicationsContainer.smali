.class public Lcom/android/settings/applications/ApplicationsContainer;
.super Lcom/android/settings/BaseFragment;
.source "ApplicationsContainer.java"

# interfaces
.implements Lcom/android/settings/applications/ManageAppClickListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Lmiui/app/ActionBar$FragmentViewPagerChangeListener;


# instance fields
.field private ta:Lmiui/app/ActionBar;

.field private tb:Landroid/app/Activity;

.field private tc:Z

.field private td:Lcom/android/settingslib/b/a;

.field public te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

.field tf:Lcom/android/settingslib/b/h;

.field tg:Ljava/lang/String;

.field private th:I

.field private ti:Landroid/view/Menu;

.field private tj:Landroid/app/AlertDialog;

.field private tk:I

.field private tl:Lcom/android/settings/applications/RunningState;

.field private tm:[I

.field private final tn:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    const v0, 0x7f120700

    const v1, 0x7f120705

    const v2, 0x7f120704

    const v3, 0x7f120701

    filled-new-array {v0, v1, v2, v3}, [I

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tm:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    return-void
.end method

.method private onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, -0x1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string/jumbo v0, "sortOrder"

    iget v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    const-string/jumbo v0, "defaultListType"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_1

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    :cond_1
    const-string/jumbo v0, "resetDialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->rn()V

    :cond_2
    return-void
.end method

.method private rq(I)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    packed-switch p1, :pswitch_data_0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yJ()V

    :goto_1
    return-void

    :pswitch_0
    const v0, 0x7f120704

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningApplicationsFragment;

    goto :goto_0

    :pswitch_1
    const v0, 0x7f120701

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningApplicationsFragment;

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/applications/ApplicationsContainer;->rs()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private rr(I)V
    .locals 1

    iput p1, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer;->rv(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)V

    invoke-direct {p0, p1}, Lcom/android/settings/applications/ApplicationsContainer;->rq(I)V

    return-void
.end method

.method private rs()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tl:Lcom/android/settings/applications/RunningState;

    iget-boolean v0, v0, Lcom/android/settings/applications/RunningState;->vh:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ApplicationsContainer"

    const-string/jumbo v1, "pause RunningState"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tl:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->pause()V

    :cond_0
    return-void
.end method

.method private rt()V
    .locals 10

    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ta:Lmiui/app/ActionBar;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ta:Lmiui/app/ActionBar;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ta:Lmiui/app/ActionBar;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiui/app/ActionBar;->setFragmentViewPagerMode(Landroid/content/Context;Landroid/app/FragmentManager;)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ta:Lmiui/app/ActionBar;

    invoke-virtual {v0, p0}, Lmiui/app/ActionBar;->addOnFragmentViewPagerChangeListener(Lmiui/app/ActionBar$FragmentViewPagerChangeListener;)V

    move v3, v7

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tm:[I

    array-length v0, v0

    if-ge v3, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ta:Lmiui/app/ActionBar;

    invoke-virtual {v0}, Lmiui/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tm:[I

    aget v0, v0, v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tm:[I

    aget v0, v0, v3

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    packed-switch v3, :pswitch_data_0

    move v0, v7

    move-object v4, v8

    :goto_1
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v9, "filter_app_key"

    invoke-virtual {v5, v9, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ta:Lmiui/app/ActionBar;

    invoke-virtual/range {v0 .. v6}, Lmiui/app/ActionBar;->addFragmentTab(Ljava/lang/String;Landroid/app/ActionBar$Tab;ILjava/lang/Class;Landroid/os/Bundle;Z)I

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :pswitch_0
    const-class v4, Lcom/android/settings/applications/ManageApplicationsFragment;

    move v0, v7

    goto :goto_1

    :pswitch_1
    const-class v4, Lcom/android/settings/applications/ManageApplicationsFragment;

    move v0, v6

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x2

    const-class v4, Lcom/android/settings/applications/RunningApplicationsFragment;

    goto :goto_1

    :pswitch_3
    const/4 v0, 0x3

    const-class v4, Lcom/android/settings/applications/RunningApplicationsFragment;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ta:Lmiui/app/ActionBar;

    iget v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    invoke-virtual {v0, v1}, Lmiui/app/ActionBar;->setSelectedNavigationItem(I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private ru()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "com.android.settings"

    const-string/jumbo v2, "com.android.settings.applications.InstalledAppDetailsTop"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "package"

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer;->tg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "is_xspace_app"

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer;->tf:Lcom/android/settingslib/b/h;

    iget-boolean v2, v2, Lcom/android/settingslib/b/h;->cBl:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v1, ":android:show_fragment_title"

    const v2, 0x7f120162

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic rx(Lcom/android/settings/applications/ApplicationsContainer;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tc:Z

    return v0
.end method

.method static synthetic ry(Lcom/android/settings/applications/ApplicationsContainer;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    return v0
.end method

.method static synthetic rz(Lcom/android/settings/applications/ApplicationsContainer;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "com.miui.securitycenter"

    const-string/jumbo v2, "com.miui.appmanager.AppManagerMainActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->finish()V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v0, "notification"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/INotificationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/INotificationManager;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    invoke-static {v0}, Landroid/net/NetworkPolicyManager;->from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;

    move-result-object v4

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/applications/ApplicationsContainer$1;-><init>(Lcom/android/settings/applications/ApplicationsContainer;Landroid/content/pm/PackageManager;Landroid/app/INotificationManager;Landroid/net/NetworkPolicyManager;Landroid/os/Handler;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/b/a;->ceS(Landroid/app/Application;)Lcom/android/settingslib/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->td:Lcom/android/settingslib/b/a;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.android.settings.APPLICATION_LIST_TYPE"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/settings/applications/RunningState;->getInstance(Landroid/content/Context;)Lcom/android/settings/applications/RunningState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tl:Lcom/android/settings/applications/RunningState;

    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer;->td:Lcom/android/settingslib/b/a;

    const v1, 0x7f120700

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/ApplicationsContainer;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;-><init>(Lcom/android/settings/applications/ApplicationsContainer;Lcom/android/settingslib/b/a;Ljava/lang/CharSequence;ILcom/android/settings/applications/ManageAppClickListener;)V

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer;->td:Lcom/android/settingslib/b/a;

    const v1, 0x7f120705

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/ApplicationsContainer;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;-><init>(Lcom/android/settings/applications/ApplicationsContainer;Lcom/android/settingslib/b/a;Ljava/lang/CharSequence;ILcom/android/settings/applications/ManageAppClickListener;)V

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer;->td:Lcom/android/settingslib/b/a;

    const v1, 0x7f120704

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/ApplicationsContainer;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;-><init>(Lcom/android/settings/applications/ApplicationsContainer;Lcom/android/settingslib/b/a;Ljava/lang/CharSequence;ILcom/android/settings/applications/ManageAppClickListener;)V

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer;->td:Lcom/android/settingslib/b/a;

    const v1, 0x7f120701

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/ApplicationsContainer;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;-><init>(Lcom/android/settings/applications/ApplicationsContainer;Lcom/android/settingslib/b/a;Ljava/lang/CharSequence;ILcom/android/settings/applications/ManageAppClickListener;)V

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_1

    const v0, 0x7f1301f7

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer;->setThemeRes(I)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/applications/ApplicationsContainer;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void

    :cond_1
    const v0, 0x7f1301f6

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x5

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iput-object p1, p0, Lcom/android/settings/applications/ApplicationsContainer;->ti:Landroid/view/Menu;

    const v0, 0x7f1210ef

    invoke-interface {p1, v2, v5, v4, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const/4 v0, 0x2

    const v1, 0x7f1210f0

    invoke-interface {p1, v2, v3, v0, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const/16 v0, 0x8

    const v1, 0x7f120e10

    invoke-interface {p1, v2, v0, v5, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const/16 v0, 0x9

    const v1, 0x7f120cea

    invoke-interface {p1, v2, v0, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    sget v1, Lmiui/R$drawable;->action_button_setting_light:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return v4
.end method

.method public onDetach()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tm:[I

    array-length v0, v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rB()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDetach()V

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/applications/ApplicationsContainer;->rt()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const/4 v1, 0x4

    if-eq v3, v1, :cond_0

    const/4 v1, 0x5

    if-ne v3, v1, :cond_2

    :cond_0
    iget v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    if-ne v1, v3, :cond_1

    move v1, v2

    :goto_0
    iput v3, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    move v3, v0

    :goto_1
    if-gt v3, v2, :cond_3

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rE(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer;->rv(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    if-ne v3, v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->rn()V

    :cond_3
    :goto_2
    return v2

    :cond_4
    const/16 v0, 0x9

    if-ne v3, v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    const-class v3, Lcom/android/settings/applications/PreferredListSettings;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_5
    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    return-void
.end method

.method public onPageScrolled(IFZZ)V
    .locals 0

    return-void
.end method

.method public onPageSelected(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/applications/ApplicationsContainer;->rr(I)V

    return-void
.end method

.method public onPause()V
    .locals 2

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onPause()V

    iput-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tc:Z

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->pause()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/applications/ApplicationsContainer;->rs()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->rw()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tc:Z

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    const/4 v1, -0x1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_1
    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    invoke-direct {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer;->rq(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-boolean v0, v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tv:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer;->rv(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "sortOrder"

    iget v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const-string/jumbo v0, "defaultListType"

    iget v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "resetDialog"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onStop()V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method public rm(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    const v5, 0x7f120162

    iget-object v0, p1, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->getCount()I

    move-result v0

    if-le v0, p4, :cond_0

    iget-object v0, p1, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->to:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-virtual {v0, p4}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rJ(I)Lcom/android/settingslib/b/h;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tf:Lcom/android/settingslib/b/h;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tf:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tg:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_1

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "package"

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tg:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, ":android:show_fragment_title"

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-class v0, Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/applications/ApplicationsContainer;->bLT(Lmiui/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/applications/ApplicationsContainer;->ru()V

    goto :goto_0
.end method

.method rn()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tb:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120e13

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f120e12

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f120e11

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f1203c7

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tj:Landroid/app/AlertDialog;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_0
    return-void
.end method

.method public ro()I
    .locals 1

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->th:I

    return v0
.end method

.method public rp()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tn:Ljava/util/ArrayList;

    return-object v0
.end method

.method public rv(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->rw()V

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer;->invalidateOptionsMenu()V

    invoke-virtual {p1}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rC()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tc:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget v1, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rD(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->pause()V

    goto :goto_0
.end method

.method rw()V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ti:Landroid/view/Menu;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget v0, v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->ty:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget v0, v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->ty:I

    if-ne v0, v1, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ti:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    if-eq v0, v4, :cond_3

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ti:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget v3, p0, Lcom/android/settings/applications/ApplicationsContainer;->tk:I

    if-eq v3, v5, :cond_2

    move v2, v1

    :cond_2
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ti:Landroid/view/Menu;

    invoke-interface {v0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_1
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ti:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ti:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer;->ti:Landroid/view/Menu;

    invoke-interface {v0, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method
