.class final Lcom/android/settings/applications/InstalledAppDetailsFragment$1;
.super Landroid/os/Handler;
.source "InstalledAppDetailsFragment.java"


# instance fields
.field final synthetic Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/InstalledAppDetailsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$1;->Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$1;->Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-virtual {v0}, Lcom/android/settings/applications/InstalledAppDetailsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$1;->Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-virtual {v0}, Lcom/android/settings/applications/InstalledAppDetailsFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$1;->Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-static {v0, p1}, Lcom/android/settings/applications/InstalledAppDetailsFragment;->qi(Lcom/android/settings/applications/InstalledAppDetailsFragment;Landroid/os/Message;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$1;->Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-static {v0}, Lcom/android/settings/applications/InstalledAppDetailsFragment;->qb(Lcom/android/settings/applications/InstalledAppDetailsFragment;)Lcom/android/settingslib/b/a;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$1;->Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-static {v1}, Lcom/android/settings/applications/InstalledAppDetailsFragment;->pW(Lcom/android/settings/applications/InstalledAppDetailsFragment;)Lcom/android/settingslib/b/h;

    move-result-object v1

    iget-object v1, v1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$1;->Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    iget v2, v2, Lcom/android/settings/applications/InstalledAppDetailsFragment;->mUserId:I

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/b/a;->cfk(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$1;->Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-static {v0, p1}, Lcom/android/settings/applications/InstalledAppDetailsFragment;->qj(Lcom/android/settings/applications/InstalledAppDetailsFragment;Landroid/os/Message;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetailsFragment$1;->Gi:Lcom/android/settings/applications/InstalledAppDetailsFragment;

    invoke-static {v0, v1, v1}, Lcom/android/settings/applications/InstalledAppDetailsFragment;->ql(Lcom/android/settings/applications/InstalledAppDetailsFragment;ZZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
