.class Lcom/android/settings/applications/ManageVoice$VoiceWakePref;
.super Ljava/lang/Object;
.source "ManageVoice.java"


# instance fields
.field public Dg:Ljava/lang/String;

.field public Dh:Landroid/content/IntentFilter;

.field public Di:Ljava/lang/String;

.field public Dj:Lmiui/preference/ValuePreference;

.field final synthetic Dk:Lcom/android/settings/applications/ManageVoice;

.field public intent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lcom/android/settings/applications/ManageVoice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dk:Lcom/android/settings/applications/ManageVoice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1, p3}, Lcom/android/settings/applications/ManageVoice;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dj:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dj:Lmiui/preference/ValuePreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dj:Lmiui/preference/ValuePreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0, p2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dh:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dh:Landroid/content/IntentFilter;

    invoke-static {v0}, Lcom/android/settings/applications/DefaultAppsHelper;->pk(Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->intent:Landroid/content/Intent;

    invoke-static {p1}, Lcom/android/settings/applications/ManageVoice;->xK(Lcom/android/settings/applications/ManageVoice;)Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->intent:Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v1, :cond_1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dg:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/settings/applications/ManageVoice;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dg:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/settings/applications/ManageVoice;->xK(Lcom/android/settings/applications/ManageVoice;)Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/settings/applications/DefaultAppsHelper;->pl(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Di:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dj:Lmiui/preference/ValuePreference;

    iget-object v1, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Di:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dj:Lmiui/preference/ValuePreference;

    const v1, 0x7f120ceb

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    const-string/jumbo v0, "ManageVoice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can not resolve this intent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
