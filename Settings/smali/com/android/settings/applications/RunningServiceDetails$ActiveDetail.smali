.class Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;
.super Ljava/lang/Object;
.source "RunningServiceDetails.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field mRootView:Landroid/view/View;

.field rL:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

.field rM:Landroid/content/ComponentName;

.field rN:Landroid/app/PendingIntent;

.field rO:Landroid/widget/Button;

.field rP:Lcom/android/settings/applications/RunningState$ServiceItem;

.field rQ:Landroid/widget/Button;

.field rR:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

.field final synthetic rS:Lcom/android/settings/applications/RunningServiceDetails;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/RunningServiceDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rO:Landroid/widget/Button;

    if-ne p1, v3, :cond_6

    new-instance v3, Landroid/app/ApplicationErrorReport;

    invoke-direct {v3}, Landroid/app/ApplicationErrorReport;-><init>()V

    const/4 v4, 0x5

    iput v4, v3, Landroid/app/ApplicationErrorReport;->type:I

    iget-object v4, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rP:Lcom/android/settings/applications/RunningState$ServiceItem;

    iget-object v4, v4, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iput-object v4, v3, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rM:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Landroid/app/ApplicationErrorReport;->installerPackageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rP:Lcom/android/settings/applications/RunningState$ServiceItem;

    iget-object v4, v4, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    iput-object v4, v3, Landroid/app/ApplicationErrorReport;->processName:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v3, Landroid/app/ApplicationErrorReport;->time:J

    iget-object v4, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rP:Lcom/android/settings/applications/RunningState$ServiceItem;

    iget-object v4, v4, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    :goto_0
    iput-boolean v0, v3, Landroid/app/ApplicationErrorReport;->systemApp:Z

    new-instance v4, Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    invoke-direct {v4}, Landroid/app/ApplicationErrorReport$RunningServiceInfo;-><init>()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rL:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    iget-wide v0, v0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uO:J

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-ltz v0, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v5, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rL:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    iget-wide v6, v5, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uO:J

    sub-long/2addr v0, v6

    iput-wide v0, v4, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->durationMillis:J

    :goto_1
    new-instance v5, Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rP:Lcom/android/settings/applications/RunningState$ServiceItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rP:Lcom/android/settings/applications/RunningState$ServiceItem;

    iget-object v1, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v5, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningServiceDetails;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "service_dump.txt"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string/jumbo v0, "activity"

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v7

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/String;

    const-string/jumbo v9, "-a"

    const/4 v10, 0x0

    aput-object v9, v8, v10

    const-string/jumbo v9, "service"

    const/4 v10, 0x1

    aput-object v9, v8, v10

    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    aput-object v9, v8, v10

    invoke-static {v0, v7, v8}, Landroid/os/Debug;->dumpService(Ljava/lang/String;Ljava/io/FileDescriptor;[Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_2
    :try_start_3
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v8

    long-to-int v0, v8

    new-array v0, v0, [B

    invoke-virtual {v1, v0}, Ljava/io/FileInputStream;->read([B)I

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    iput-object v2, v4, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->serviceDetails:Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_1
    :goto_3
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    const-string/jumbo v0, "RunningServicesDetails"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Details: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v4, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->serviceDetails:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v4, v3, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.APP_ERROR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rM:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.BUG_REPORT"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {v1, v0}, Lcom/android/settings/applications/RunningServiceDetails;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_2
    move v0, v1

    goto/16 :goto_0

    :cond_3
    const-wide/16 v0, -0x1

    iput-wide v0, v4, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->durationMillis:J

    goto/16 :goto_1

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_6
    const-string/jumbo v7, "RunningServicesDetails"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Can\'t dump service: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    if-eqz v1, :cond_0

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_2

    :catch_2
    move-exception v0

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_5
    if-eqz v1, :cond_4

    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_4
    :goto_6
    throw v0

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    :goto_7
    :try_start_9
    const-string/jumbo v1, "RunningServicesDetails"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Can\'t read service dump: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v2, :cond_1

    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_3

    :catch_6
    move-exception v0

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    :goto_8
    if-eqz v2, :cond_5

    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    :cond_5
    :goto_9
    throw v0

    :catch_7
    move-exception v1

    goto :goto_9

    :cond_6
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rN:Landroid/app/PendingIntent;

    if-eqz v0, :cond_7

    :try_start_c
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningServiceDetails;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rN:Landroid/app/PendingIntent;

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x10080000

    const/high16 v4, 0x80000

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    :try_end_c
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_c .. :try_end_c} :catch_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_9
    .catch Landroid/content/ActivityNotFoundException; {:try_start_c .. :try_end_c} :catch_8

    :goto_a
    return-void

    :catch_8
    move-exception v0

    const-string/jumbo v1, "RunningServicesDetails"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    :catch_9
    move-exception v0

    const-string/jumbo v1, "RunningServicesDetails"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    :catch_a
    move-exception v0

    const-string/jumbo v1, "RunningServicesDetails"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    :cond_7
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rP:Lcom/android/settings/applications/RunningState$ServiceItem;

    if-eqz v0, :cond_8

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->qO(Z)V

    goto :goto_a

    :cond_8
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rL:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    iget-boolean v0, v0, Lcom/android/settings/applications/RunningState$BaseItem;->wa:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-object v0, v0, Lcom/android/settings/applications/RunningServiceDetails;->rv:Landroid/app/ActivityManager;

    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rL:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    iget-object v1, v1, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    iget-object v1, v1, Lcom/android/settings/applications/RunningState$BaseItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->killBackgroundProcesses(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningServiceDetails;->finish()V

    goto :goto_a

    :cond_9
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-object v0, v0, Lcom/android/settings/applications/RunningServiceDetails;->rv:Landroid/app/ActivityManager;

    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rL:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    iget-object v1, v1, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->uP:Lcom/android/settings/applications/RunningState$BaseItem;

    iget-object v1, v1, Lcom/android/settings/applications/RunningState$BaseItem;->wb:Landroid/content/pm/PackageItemInfo;

    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningServiceDetails;->finish()V

    goto :goto_a

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_8

    :catch_b
    move-exception v0

    move-object v2, v1

    goto/16 :goto_7

    :catchall_3
    move-exception v0

    goto/16 :goto_5

    :catch_c
    move-exception v0

    goto/16 :goto_4
.end method

.method qO(Z)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rP:Lcom/android/settings/applications/RunningState$ServiceItem;

    if-nez p1, :cond_0

    iget-object v1, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wm:Landroid/content/pm/ServiceInfo;

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-static {v1, v0}, Lcom/android/settings/applications/RunningServiceDetails;->qN(Lcom/android/settings/applications/RunningServiceDetails;Landroid/content/ComponentName;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {v1}, Lcom/android/settings/applications/RunningServiceDetails;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->wn:Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    new-instance v2, Landroid/os/UserHandle;

    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-object v3, v3, Lcom/android/settings/applications/RunningServiceDetails;->rz:Lcom/android/settings/applications/RunningState$MergedItem;

    iget v3, v3, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->stopServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Z

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-object v0, v0, Lcom/android/settings/applications/RunningServiceDetails;->rz:Lcom/android/settings/applications/RunningState$MergedItem;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-object v0, v0, Lcom/android/settings/applications/RunningServiceDetails;->rJ:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->sA()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningServiceDetails;->finish()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-boolean v0, v0, Lcom/android/settings/applications/RunningServiceDetails;->rF:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-object v0, v0, Lcom/android/settings/applications/RunningServiceDetails;->rz:Lcom/android/settings/applications/RunningState$MergedItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wE:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-object v0, v0, Lcom/android/settings/applications/RunningServiceDetails;->rJ:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->sA()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningServiceDetails;->finish()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->rS:Lcom/android/settings/applications/RunningServiceDetails;

    iget-object v0, v0, Lcom/android/settings/applications/RunningServiceDetails;->rJ:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->sA()V

    goto :goto_0
.end method
