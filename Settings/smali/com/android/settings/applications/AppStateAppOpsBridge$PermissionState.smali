.class public Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;
.super Ljava/lang/Object;
.source "AppStateAppOpsBridge.java"


# instance fields
.field public CX:Z

.field public final CY:Landroid/os/UserHandle;

.field public CZ:Landroid/content/pm/PackageInfo;

.field public Da:I

.field public Db:Z

.field public final packageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/os/UserHandle;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;->packageName:Ljava/lang/String;

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;->Da:I

    iput-object p2, p0, Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;->CY:Landroid/os/UserHandle;

    return-void
.end method


# virtual methods
.method public xH()Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;->Da:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;->Db:Z

    return v0

    :cond_0
    iget v1, p0, Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;->Da:I

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method
