.class final Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;
.super Landroid/os/AsyncTask;
.source "ApplicationsContainer.java"


# instance fields
.field final synthetic Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

.field final synthetic Gu:Z


# direct methods
.method constructor <init>(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    iput-boolean p2, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gu:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rS(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settingslib/b/b;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rQ(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settingslib/b/i;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v2}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rO(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/Comparator;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/b/b;->cfx(Lcom/android/settingslib/b/i;Ljava/util/Comparator;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->zS(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected zS(Ljava/util/ArrayList;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gu:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rU(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rN(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    iget-object v2, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    iget-object v2, v2, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tF:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v3}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rN(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rH(Ljava/lang/CharSequence;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rV(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->notifyDataSetChanged()V

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rT(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rF(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rT(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rG(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rV(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rT(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rF(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->Gt:Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rT(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rG(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
