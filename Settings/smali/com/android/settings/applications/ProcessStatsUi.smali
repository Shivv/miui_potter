.class public Lcom/android/settings/applications/ProcessStatsUi;
.super Lcom/android/settings/applications/ProcessStatsBase;
.source "ProcessStatsUi.java"


# static fields
.field public static final pU:[I

.field public static final pV:[I

.field public static final pW:[I

.field static final qc:Ljava/util/Comparator;

.field static final qd:Ljava/util/Comparator;


# instance fields
.field private pX:Landroid/preference/PreferenceGroup;

.field private pY:Landroid/view/MenuItem;

.field private pZ:Landroid/view/MenuItem;

.field private qa:Landroid/content/pm/PackageManager;

.field private qb:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x1

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/settings/applications/ProcessStatsUi;->pU:[I

    new-array v0, v2, [I

    const/4 v1, 0x0

    aput v2, v0, v1

    sput-object v0, Lcom/android/settings/applications/ProcessStatsUi;->pW:[I

    const/16 v0, 0xb

    const/16 v1, 0xc

    const/16 v2, 0xd

    filled-new-array {v0, v1, v2}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/applications/ProcessStatsUi;->pV:[I

    new-instance v0, Lcom/android/settings/applications/ProcessStatsUi$1;

    invoke-direct {v0}, Lcom/android/settings/applications/ProcessStatsUi$1;-><init>()V

    sput-object v0, Lcom/android/settings/applications/ProcessStatsUi;->qd:Ljava/util/Comparator;

    new-instance v0, Lcom/android/settings/applications/ProcessStatsUi$2;

    invoke-direct {v0}, Lcom/android/settings/applications/ProcessStatsUi$2;-><init>()V

    sput-object v0, Lcom/android/settings/applications/ProcessStatsUi;->qc:Ljava/util/Comparator;

    return-void

    :array_0
    .array-data 4
        0x0
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/ProcessStatsBase;-><init>()V

    return-void
.end method

.method private pj()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->pZ:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/android/settings/applications/ProcessStatsUi;->qb:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->pY:Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/android/settings/applications/ProcessStatsUi;->qb:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method


# virtual methods
.method public at(Landroid/preference/Preference;)Z
    .locals 4

    instance-of v0, p1, Lcom/android/settings/applications/ProcessStatsPreference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/android/settings/applications/ProcessStatsPreference;

    iget-object v1, p0, Lcom/android/settings/applications/ProcessStatsUi;->Bx:Lcom/android/settings/applications/ProcStatsData;

    invoke-virtual {v1}, Lcom/android/settings/applications/ProcStatsData;->zD()Lcom/android/settings/applications/ProcStatsData$MemInfo;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/applications/ProcessStatsUi;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/android/settings/bL;

    invoke-virtual {v0}, Lcom/android/settings/applications/ProcessStatsPreference;->wA()Lcom/android/settings/applications/ProcStatsPackageEntry;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, Lcom/android/settings/applications/ProcessStatsUi;->wz(Landroid/app/Activity;Lcom/android/settings/applications/ProcStatsData$MemInfo;Lcom/android/settings/applications/ProcStatsPackageEntry;Z)V

    invoke-super {p0, p1}, Lcom/android/settings/applications/ProcessStatsBase;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x17

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/applications/ProcessStatsBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ProcessStatsUi;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->qa:Landroid/content/pm/PackageManager;

    const v0, 0x7f1500ae

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsUi;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "app_list"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsUi;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->pX:Landroid/preference/PreferenceGroup;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsUi;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/settings/applications/ProcessStatsBase;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    const/4 v0, 0x1

    const v1, 0x7f1210ed

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->pY:Landroid/view/MenuItem;

    const/4 v0, 0x2

    const v1, 0x7f1210ee

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->pZ:Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/android/settings/applications/ProcessStatsUi;->pj()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/applications/ProcessStatsBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    iget-boolean v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->qb:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->qb:Z

    invoke-virtual {p0}, Lcom/android/settings/applications/ProcessStatsUi;->refreshUi()V

    invoke-direct {p0}, Lcom/android/settings/applications/ProcessStatsUi;->pj()V

    const/4 v0, 0x1

    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/applications/ProcessStatsBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public refreshUi()V
    .locals 14

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->pX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->pX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->setOrderingAsAdded(Z)V

    iget-object v2, p0, Lcom/android/settings/applications/ProcessStatsUi;->pX:Landroid/preference/PreferenceGroup;

    iget-boolean v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->qb:Z

    if-eqz v0, :cond_0

    const v0, 0x7f120a40

    :goto_0
    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->setTitle(I)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ProcessStatsUi;->getActivity()Landroid/app/Activity;

    move-result-object v11

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->Bx:Lcom/android/settings/applications/ProcStatsData;

    invoke-virtual {v0}, Lcom/android/settings/applications/ProcStatsData;->zD()Lcom/android/settings/applications/ProcStatsData$MemInfo;

    move-result-object v12

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->Bx:Lcom/android/settings/applications/ProcStatsData;

    invoke-virtual {v0}, Lcom/android/settings/applications/ProcStatsData;->zF()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-interface {v13, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ProcStatsPackageEntry;

    invoke-virtual {v0}, Lcom/android/settings/applications/ProcStatsPackageEntry;->wd()V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    const v0, 0x7f1201cd

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->qb:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/settings/applications/ProcessStatsUi;->qc:Ljava/util/Comparator;

    :goto_2
    invoke-static {v13, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-boolean v0, p0, Lcom/android/settings/applications/ProcessStatsUi;->qb:Z

    if-eqz v0, :cond_3

    iget-wide v4, v12, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fy:D

    :goto_3
    move v0, v1

    :goto_4
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/applications/ProcStatsPackageEntry;

    new-instance v1, Lcom/android/settings/applications/ProcessStatsPreference;

    invoke-virtual {p0}, Lcom/android/settings/applications/ProcessStatsUi;->bWz()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/android/settings/applications/ProcessStatsPreference;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/settings/applications/ProcessStatsUi;->qa:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v11, v3}, Lcom/android/settings/applications/ProcStatsPackageEntry;->we(Landroid/content/Context;Landroid/content/pm/PackageManager;)V

    iget-object v3, p0, Lcom/android/settings/applications/ProcessStatsUi;->qa:Landroid/content/pm/PackageManager;

    iget-wide v6, v12, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FB:D

    iget-wide v8, v12, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FC:D

    iget-boolean v10, p0, Lcom/android/settings/applications/ProcessStatsUi;->qb:Z

    xor-int/lit8 v10, v10, 0x1

    invoke-virtual/range {v1 .. v10}, Lcom/android/settings/applications/ProcessStatsPreference;->wB(Lcom/android/settings/applications/ProcStatsPackageEntry;Landroid/content/pm/PackageManager;DDDZ)V

    invoke-virtual {v1, v0}, Lcom/android/settings/applications/ProcessStatsPreference;->setOrder(I)V

    iget-object v2, p0, Lcom/android/settings/applications/ProcessStatsUi;->pX:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_2
    sget-object v0, Lcom/android/settings/applications/ProcessStatsUi;->qd:Ljava/util/Comparator;

    goto :goto_2

    :cond_3
    iget-wide v2, v12, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FA:D

    iget-wide v4, v12, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FB:D

    mul-double/2addr v4, v2

    goto :goto_3

    :cond_4
    return-void
.end method
