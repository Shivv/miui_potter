.class Lcom/android/settings/applications/IconLoader$BackgroundHandler;
.super Landroid/os/Handler;
.source "IconLoader.java"


# instance fields
.field final synthetic AS:Lcom/android/settings/applications/IconLoader;


# direct methods
.method public constructor <init>(Lcom/android/settings/applications/IconLoader;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/IconLoader$BackgroundHandler;->AS:Lcom/android/settings/applications/IconLoader;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/settings/applications/IconLoader$IconItem;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, v0, Lcom/android/settings/applications/IconLoader$IconItem;->AQ:Lcom/android/settingslib/b/h;

    iget-object v2, v0, Lcom/android/settings/applications/IconLoader$IconItem;->AR:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settingslib/b/h;->cfC(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v1, Lcom/android/settingslib/b/h;->icon:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/settings/applications/IconLoader$BackgroundHandler;->AS:Lcom/android/settings/applications/IconLoader;

    iget-object v1, v1, Lcom/android/settings/applications/IconLoader;->AP:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/applications/IconLoader$BackgroundHandler;->AS:Lcom/android/settings/applications/IconLoader;

    iget-object v0, v0, Lcom/android/settings/applications/IconLoader;->AP:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public wj(I)I
    .locals 2

    rem-int/lit8 v0, p1, 0xf

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/IconLoader$BackgroundHandler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/IconLoader$BackgroundHandler;->removeMessages(I)V

    :cond_0
    return v0
.end method
