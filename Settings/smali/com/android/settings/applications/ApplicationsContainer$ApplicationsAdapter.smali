.class public Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;
.super Landroid/widget/BaseAdapter;
.source "ApplicationsContainer.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/android/settingslib/b/f;
.implements Landroid/widget/AbsListView$RecyclerListener;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final tC:Ljava/util/ArrayList;

.field private tD:Ljava/util/ArrayList;

.field private tE:Ljava/util/Comparator;

.field tF:Ljava/lang/CharSequence;

.field private tG:Ljava/util/ArrayList;

.field private tH:Landroid/widget/Filter;

.field private final tI:I

.field private tJ:Lcom/android/settingslib/b/i;

.field private tK:I

.field private tL:Lcom/android/settings/applications/IconLoader;

.field private tM:I

.field private tN:Z

.field private final tO:Lcom/android/settingslib/b/b;

.field private final tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

.field private tQ:I


# direct methods
.method public constructor <init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/ApplicationsContainer$TabInfo;I)V
    .locals 3

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tC:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tM:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tQ:I

    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$1;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$1;-><init>(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)V

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tH:Landroid/widget/Filter;

    invoke-virtual {p1, p0}, Lcom/android/settingslib/b/a;->ceZ(Lcom/android/settingslib/b/f;)Lcom/android/settingslib/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tO:Lcom/android/settingslib/b/b;

    iput-object p2, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v0, p2, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tB:Lcom/android/settings/applications/ApplicationsContainer;

    invoke-virtual {v0}, Lcom/android/settings/applications/ApplicationsContainer;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iput p3, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tI:I

    new-instance v0, Lcom/android/settings/applications/IconLoader;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "IconLoader-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->ty:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/applications/IconLoader;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tL:Lcom/android/settings/applications/IconLoader;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tL:Lcom/android/settings/applications/IconLoader;

    invoke-virtual {v0}, Lcom/android/settings/applications/IconLoader;->start()V

    return-void
.end method

.method static synthetic rN(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tD:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic rO(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tE:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic rP(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tG:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic rQ(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settingslib/b/i;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tJ:Lcom/android/settingslib/b/i;

    return-object v0
.end method

.method static synthetic rR(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tK:I

    return v0
.end method

.method static synthetic rS(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settingslib/b/b;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tO:Lcom/android/settingslib/b/b;

    return-object v0
.end method

.method static synthetic rT(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)Lcom/android/settings/applications/ApplicationsContainer$TabInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    return-object v0
.end method

.method static synthetic rU(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tD:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic rV(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tG:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic rW(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tK:I

    return p1
.end method

.method static synthetic rX(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tM:I

    return p1
.end method

.method static synthetic rY(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tN:Z

    return p1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tG:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tG:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tH:Landroid/widget/Filter;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tG:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tG:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iget-wide v0, v0, Lcom/android/settingslib/b/h;->cBe:J

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v0, v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tt:Landroid/view/LayoutInflater;

    invoke-static {v0, p2}, Lcom/android/settings/applications/AppViewHolder;->sm(Landroid/view/LayoutInflater;Landroid/view/View;)Lcom/android/settings/applications/AppViewHolder;

    move-result-object v1

    iget-object v2, v1, Lcom/android/settings/applications/AppViewHolder;->uk:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tG:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    monitor-enter v0

    :try_start_0
    iput-object v0, v1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v3, v0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/android/settings/applications/AppViewHolder;->ul:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v3, v1, Lcom/android/settings/applications/AppViewHolder;->um:Landroid/widget/ImageView;

    iget-object v4, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tL:Lcom/android/settings/applications/IconLoader;

    iget-object v4, v1, Lcom/android/settings/applications/AppViewHolder;->um:Landroid/widget/ImageView;

    invoke-virtual {v3, v4, v0, p1}, Lcom/android/settings/applications/IconLoader;->wh(Landroid/widget/ImageView;Lcom/android/settingslib/b/h;I)V

    iget-object v3, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v3, v3, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tu:Ljava/lang/CharSequence;

    iget v4, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tQ:I

    invoke-virtual {v1, v3, v4}, Lcom/android/settings/applications/AppViewHolder;->sl(Ljava/lang/CharSequence;I)V

    iget-object v3, v1, Lcom/android/settings/applications/AppViewHolder;->un:Landroid/widget/TextView;

    iget-object v1, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tC:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tC:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public in()V
    .locals 2

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tM:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rL(Z)V

    :cond_0
    return-void
.end method

.method public ip()V
    .locals 0

    return-void
.end method

.method public iq()V
    .locals 2

    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;-><init>(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public ir()V
    .locals 0

    return-void
.end method

.method public is()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rL(Z)V

    return-void
.end method

.method public it(Ljava/lang/String;)V
    .locals 5

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tC:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tC:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/AppViewHolder;

    iget-object v3, v0, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v3, v3, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v1, v0, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    monitor-enter v1

    :try_start_0
    iget-object v3, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v3, v3, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tu:Ljava/lang/CharSequence;

    iget v4, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tQ:I

    invoke-virtual {v0, v3, v4}, Lcom/android/settings/applications/AppViewHolder;->sl(Ljava/lang/CharSequence;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    iget-object v0, v0, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget-object v1, v1, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->tB:Lcom/android/settings/applications/ApplicationsContainer;

    iget-object v1, v1, Lcom/android/settings/applications/ApplicationsContainer;->tg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tM:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rL(Z)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method public iu(Ljava/util/ArrayList;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rG(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rG(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->mContext:Landroid/content/Context;

    const v2, 0x10a0001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rF(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rG(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-object p1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tD:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tF:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tD:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rH(Ljava/lang/CharSequence;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tG:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public iv(Z)V
    .locals 0

    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tC:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public pause()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tN:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tN:Z

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tO:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->pause()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tP:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    invoke-static {v0}, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->rF(Lcom/android/settings/applications/ApplicationsContainer$TabInfo;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tK:I

    return-void
.end method

.method rH(Ljava/lang/CharSequence;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-object p2

    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settingslib/b/a;->ceW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    invoke-virtual {v0}, Lcom/android/settingslib/b/h;->cfF()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_3

    :cond_2
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    return-object v4
.end method

.method public rI()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tN:Z

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tO:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfy()V

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tL:Lcom/android/settings/applications/IconLoader;

    invoke-virtual {v0}, Lcom/android/settings/applications/IconLoader;->stop()V

    return-void
.end method

.method public rJ(I)Lcom/android/settingslib/b/h;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tG:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    return-object v0
.end method

.method public rK(I)V
    .locals 1

    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tM:I

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tM:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rL(Z)V

    return-void
.end method

.method public rL(Z)V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v0

    if-eqz v0, :cond_0

    iput v1, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tQ:I

    :goto_0
    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tI:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tJ:Lcom/android/settingslib/b/i;

    :goto_1
    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tM:I

    packed-switch v0, :pswitch_data_1

    sget-object v0, Lcom/android/settingslib/b/a;->cAa:Ljava/util/Comparator;

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tE:Ljava/util/Comparator;

    :goto_2
    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;-><init>(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;Z)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tQ:I

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/android/settingslib/b/a;->cAb:Lcom/android/settingslib/b/i;

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tJ:Lcom/android/settingslib/b/i;

    goto :goto_1

    :pswitch_1
    iget v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tQ:I

    packed-switch v0, :pswitch_data_2

    sget-object v0, Lcom/android/settingslib/b/a;->cAu:Ljava/util/Comparator;

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tE:Ljava/util/Comparator;

    goto :goto_2

    :pswitch_2
    sget-object v0, Lcom/android/settingslib/b/a;->czS:Ljava/util/Comparator;

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tE:Ljava/util/Comparator;

    goto :goto_2

    :pswitch_3
    sget-object v0, Lcom/android/settingslib/b/a;->cAd:Ljava/util/Comparator;

    iput-object v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tE:Ljava/util/Comparator;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public rM(I)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->tN:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$3;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$3;-><init>(Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ApplicationsContainer$ApplicationsAdapter;->rK(I)V

    goto :goto_0
.end method
