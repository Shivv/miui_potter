.class public Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;
.super Landroid/os/AsyncTask;
.source "SystemAppSettings.java"


# instance fields
.field private yY:Ljava/util/HashMap;

.field final synthetic yZ:Lcom/android/settings/applications/SystemAppSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/applications/SystemAppSettings;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;->yZ:Lcom/android/settings/applications/SystemAppSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, Lcom/android/settings/applications/SystemAppSettings;->uM()Ljava/util/HashMap;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;->yY:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;->yY:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-object v2, p0, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;->yZ:Lcom/android/settings/applications/SystemAppSettings;

    invoke-static {v2}, Lcom/android/settings/applications/SystemAppSettings;->uL(Lcom/android/settings/applications/SystemAppSettings;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/settings/applications/SystemAppSettings;->uO(Landroid/content/Context;Landroid/preference/PreferenceActivity$Header;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;->yY:Ljava/util/HashMap;

    invoke-static {v0}, Lcom/android/settings/applications/SystemAppSettings;->uN(Ljava/util/HashMap;)Ljava/util/HashMap;

    return-void
.end method
