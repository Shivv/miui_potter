.class final Lcom/android/settings/applications/RunningState$1;
.super Ljava/lang/Object;
.source "RunningState.java"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic GD:Lcom/android/settings/applications/RunningState;


# direct methods
.method constructor <init>(Lcom/android/settings/applications/RunningState;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/RunningState$1;->GD:Lcom/android/settings/applications/RunningState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/applications/RunningState$MergedItem;

    check-cast p2, Lcom/android/settings/applications/RunningState$MergedItem;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/applications/RunningState$1;->zT(Lcom/android/settings/applications/RunningState$MergedItem;Lcom/android/settings/applications/RunningState$MergedItem;)I

    move-result v0

    return v0
.end method

.method public zT(Lcom/android/settings/applications/RunningState$MergedItem;Lcom/android/settings/applications/RunningState$MergedItem;)I
    .locals 8

    const/16 v7, 0x190

    const/4 v3, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x1

    iget v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    iget v4, p2, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    if-eq v2, v4, :cond_3

    iget v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    iget-object v3, p0, Lcom/android/settings/applications/RunningState$1;->GD:Lcom/android/settings/applications/RunningState;

    iget v3, v3, Lcom/android/settings/applications/RunningState;->vy:I

    if-ne v2, v3, :cond_0

    return v0

    :cond_0
    iget v2, p2, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    iget-object v3, p0, Lcom/android/settings/applications/RunningState$1;->GD:Lcom/android/settings/applications/RunningState;

    iget v3, v3, Lcom/android/settings/applications/RunningState;->vy:I

    if-ne v2, v3, :cond_1

    return v1

    :cond_1
    iget v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    iget v3, p2, Lcom/android/settings/applications/RunningState$MergedItem;->mUserId:I

    if-ge v2, v3, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v4, p2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    if-ne v2, v4, :cond_6

    iget-object v1, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wj:Ljava/lang/String;

    iget-object v2, p2, Lcom/android/settings/applications/RunningState$MergedItem;->wj:Ljava/lang/String;

    if-ne v1, v2, :cond_4

    return v3

    :cond_4
    iget-object v1, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wj:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v0, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wj:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/settings/applications/RunningState$MergedItem;->wj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    :cond_5
    return v0

    :cond_6
    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    if-nez v2, :cond_7

    return v0

    :cond_7
    iget-object v2, p2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    if-nez v2, :cond_8

    return v1

    :cond_8
    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v5, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->ws:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget-object v2, p2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v6, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->ws:Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v2, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    if-lt v2, v7, :cond_9

    move v2, v1

    :goto_1
    iget v4, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    if-lt v4, v7, :cond_a

    move v4, v1

    :goto_2
    if-eq v2, v4, :cond_c

    if-eqz v2, :cond_b

    :goto_3
    return v1

    :cond_9
    move v2, v3

    goto :goto_1

    :cond_a
    move v4, v3

    goto :goto_2

    :cond_b
    move v1, v0

    goto :goto_3

    :cond_c
    iget v2, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->flags:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_d

    move v2, v1

    :goto_4
    iget v4, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->flags:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_e

    move v4, v1

    :goto_5
    if-eq v2, v4, :cond_10

    if-eqz v2, :cond_f

    :goto_6
    return v0

    :cond_d
    move v2, v3

    goto :goto_4

    :cond_e
    move v4, v3

    goto :goto_5

    :cond_f
    move v0, v1

    goto :goto_6

    :cond_10
    iget v2, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->lru:I

    iget v4, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->lru:I

    if-eq v2, v4, :cond_12

    iget v2, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->lru:I

    iget v3, v6, Landroid/app/ActivityManager$RunningAppProcessInfo;->lru:I

    if-ge v2, v3, :cond_11

    :goto_7
    return v0

    :cond_11
    move v0, v1

    goto :goto_7

    :cond_12
    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v2, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    iget-object v4, p2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v4, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    if-ne v2, v4, :cond_13

    return v3

    :cond_13
    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v2, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    if-nez v2, :cond_14

    return v1

    :cond_14
    iget-object v1, p2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v1, v1, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    if-nez v1, :cond_15

    return v0

    :cond_15
    iget-object v0, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v1, v1, Lcom/android/settings/applications/RunningState$ProcessItem;->wj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
