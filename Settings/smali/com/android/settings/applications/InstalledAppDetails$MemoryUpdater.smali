.class Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;
.super Landroid/os/AsyncTask;
.source "InstalledAppDetails.java"


# instance fields
.field final synthetic Eb:Lcom/android/settings/applications/InstalledAppDetails;


# direct methods
.method private constructor <init>(Lcom/android/settings/applications/InstalledAppDetails;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/applications/InstalledAppDetails;Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;-><init>(Lcom/android/settings/applications/InstalledAppDetails;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/android/settings/applications/ProcStatsPackageEntry;
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    invoke-virtual {v0}, Lcom/android/settings/applications/InstalledAppDetails;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v5

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    iget-object v0, v0, Lcom/android/settings/applications/InstalledAppDetails;->mPackageInfo:Landroid/content/pm/PackageInfo;

    if-nez v0, :cond_1

    return-object v5

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    iget-object v0, v0, Lcom/android/settings/applications/InstalledAppDetails;->DW:Lcom/android/settings/applications/ProcStatsData;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    new-instance v1, Lcom/android/settings/applications/ProcStatsData;

    iget-object v2, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    invoke-virtual {v2}, Lcom/android/settings/applications/InstalledAppDetails;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lcom/android/settings/applications/ProcStatsData;-><init>(Landroid/content/Context;Z)V

    iput-object v1, v0, Lcom/android/settings/applications/InstalledAppDetails;->DW:Lcom/android/settings/applications/ProcStatsData;

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    iget-object v0, v0, Lcom/android/settings/applications/InstalledAppDetails;->DW:Lcom/android/settings/applications/ProcStatsData;

    sget-object v1, Lcom/android/settings/applications/ProcessStatsBase;->Bw:[J

    aget-wide v2, v1, v3

    invoke-virtual {v0, v2, v3}, Lcom/android/settings/applications/ProcStatsData;->zC(J)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    iget-object v0, v0, Lcom/android/settings/applications/InstalledAppDetails;->DW:Lcom/android/settings/applications/ProcStatsData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ProcStatsData;->zH(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    iget-object v0, v0, Lcom/android/settings/applications/InstalledAppDetails;->DW:Lcom/android/settings/applications/ProcStatsData;

    invoke-virtual {v0}, Lcom/android/settings/applications/ProcStatsData;->zF()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ProcStatsPackageEntry;

    iget-object v1, v0, Lcom/android/settings/applications/ProcStatsPackageEntry;->AG:Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/applications/ProcStatsEntry;

    iget v1, v1, Lcom/android/settings/applications/ProcStatsEntry;->Bs:I

    iget-object v4, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    iget-object v4, v4, Lcom/android/settings/applications/InstalledAppDetails;->mPackageInfo:Landroid/content/pm/PackageInfo;

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v1, v4, :cond_4

    invoke-virtual {v0}, Lcom/android/settings/applications/ProcStatsPackageEntry;->wd()V

    return-object v0

    :cond_5
    return-object v5
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->doInBackground([Ljava/lang/Void;)Lcom/android/settings/applications/ProcStatsPackageEntry;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/settings/applications/ProcStatsPackageEntry;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->yH(Lcom/android/settings/applications/ProcStatsPackageEntry;)V

    return-void
.end method

.method protected yH(Lcom/android/settings/applications/ProcStatsPackageEntry;)V
    .locals 7

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    invoke-virtual {v0}, Lcom/android/settings/applications/InstalledAppDetails;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    iput-object p1, v0, Lcom/android/settings/applications/InstalledAppDetails;->DV:Lcom/android/settings/applications/ProcStatsPackageEntry;

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    invoke-static {v0}, Lcom/android/settings/applications/InstalledAppDetails;->yx(Lcom/android/settings/applications/InstalledAppDetails;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-wide v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->Ay:D

    iget-wide v2, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->Az:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-object v2, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    iget-object v2, v2, Lcom/android/settings/applications/InstalledAppDetails;->DW:Lcom/android/settings/applications/ProcStatsData;

    invoke-virtual {v2}, Lcom/android/settings/applications/ProcStatsData;->zD()Lcom/android/settings/applications/ProcStatsData$MemInfo;

    move-result-object v2

    iget-wide v2, v2, Lcom/android/settings/applications/ProcStatsData$MemInfo;->FB:D

    mul-double/2addr v0, v2

    iget-object v2, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    invoke-static {v2}, Lcom/android/settings/applications/InstalledAppDetails;->yx(Lcom/android/settings/applications/InstalledAppDetails;)Landroid/preference/Preference;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    invoke-virtual {v5}, Lcom/android/settings/applications/InstalledAppDetails;->getContext()Landroid/content/Context;

    move-result-object v5

    double-to-long v0, v0

    invoke-static {v5, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const v0, 0x7f120a71

    invoke-virtual {v3, v0, v4}, Lcom/android/settings/applications/InstalledAppDetails;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    invoke-static {v0}, Lcom/android/settings/applications/InstalledAppDetails;->yx(Lcom/android/settings/applications/InstalledAppDetails;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    invoke-static {v0}, Lcom/android/settings/applications/InstalledAppDetails;->yx(Lcom/android/settings/applications/InstalledAppDetails;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/InstalledAppDetails$MemoryUpdater;->Eb:Lcom/android/settings/applications/InstalledAppDetails;

    const v2, 0x7f120b98

    invoke-virtual {v1, v2}, Lcom/android/settings/applications/InstalledAppDetails;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
