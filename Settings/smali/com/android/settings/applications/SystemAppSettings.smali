.class public Lcom/android/settings/applications/SystemAppSettings;
.super Lcom/android/settings/BaseListFragment;
.source "SystemAppSettings.java"


# static fields
.field private static yX:Ljava/util/HashMap;


# instance fields
.field private yU:Landroid/app/Activity;

.field private yV:Lcom/android/settings/applications/SystemAppSettings$HeaderAdapter;

.field private yW:Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/settings/applications/SystemAppSettings;->yX:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BaseListFragment;-><init>()V

    return-void
.end method

.method private uJ(Ljava/util/List;I)I
    .locals 12

    const/16 v11, 0x3e8

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yU:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "miui.intent.action.APP_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    const/4 v0, -0x1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/android/settings/applications/SystemAppSettings;->yU:Landroid/app/Activity;

    invoke-static {v2}, Lmiui/payment/PaymentManager;->get(Landroid/content/Context;)Lmiui/payment/PaymentManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/payment/PaymentManager;->isMibiServiceDisabled()Z

    move-result v2

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-boolean v5, v0, Landroid/content/pm/ResolveInfo;->system:Z

    if-nez v5, :cond_0

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v6, "com.miui.voiceassist"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string/jumbo v5, "support_main_xiaoai"

    invoke-static {v5, v10}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    if-lt v1, v11, :cond_2

    iget v5, v0, Landroid/content/pm/ResolveInfo;->priority:I

    if-ge v5, v11, :cond_2

    if-nez v2, :cond_2

    new-instance v5, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v5}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    const v6, 0x7f121680

    invoke-virtual {p0, v6}, Lcom/android/settings/applications/SystemAppSettings;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    const-wide/32 v6, 0x7f0a029b

    iput-wide v6, v5, Landroid/preference/PreferenceActivity$Header;->id:J

    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v7, "com.xiaomi.action.VIEW_MILI_CENTER"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v6, v5, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v5, Landroid/content/Intent;

    const-string/jumbo v6, "miui.intent.action.APP_SETTINGS"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v6, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v6}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    const-string/jumbo v7, "system_app"

    iput-object v7, v6, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    iput-object v5, v6, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v7, "com.android.phone"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    if-eqz v5, :cond_3

    const-wide/16 v8, -0x3e8

    iput-wide v8, v6, Landroid/preference/PreferenceActivity$Header;->id:J

    :cond_3
    const-string/jumbo v5, "com.miui.googlebase.ui.GmsCoreSettings"

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v5, :cond_6

    const-string/jumbo v5, "1"

    const-string/jumbo v7, "ro.miui.has_gmscore"

    invoke-static {v7}, Lmiui/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const v1, 0x7f08015d

    iput v1, v6, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    :cond_4
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/android/settings/applications/SystemAppSettings;->yX:Ljava/util/HashMap;

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/android/settings/applications/SystemAppSettings;->yX:Ljava/util/HashMap;

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    iget v1, v0, Landroid/content/pm/ResolveInfo;->priority:I

    move v0, v1

    goto/16 :goto_1

    :cond_6
    move v0, v1

    goto/16 :goto_1

    :cond_7
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_8

    add-int/lit8 v2, p2, 0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    invoke-interface {p1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    move p2, v2

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yW:Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;

    if-nez v0, :cond_9

    new-instance v0, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;-><init>(Lcom/android/settings/applications/SystemAppSettings;)V

    iput-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yW:Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yW:Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;

    new-array v1, v10, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/SystemAppSettings$LoadLabelTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_9
    return p2
.end method

.method private static uK(Landroid/content/Context;Landroid/preference/PreferenceActivity$Header;)V
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_0

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v1, v0}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    :cond_0
    return-void
.end method

.method static synthetic uL(Lcom/android/settings/applications/SystemAppSettings;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yU:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic uM()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/android/settings/applications/SystemAppSettings;->yX:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic uN(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    sput-object p0, Lcom/android/settings/applications/SystemAppSettings;->yX:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic uO(Landroid/content/Context;Landroid/preference/PreferenceActivity$Header;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/settings/applications/SystemAppSettings;->uK(Landroid/content/Context;Landroid/preference/PreferenceActivity$Header;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/SystemAppSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yU:Landroid/app/Activity;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/SystemAppSettings;->uJ(Ljava/util/List;I)I

    new-instance v1, Lcom/android/settings/applications/SystemAppSettings$HeaderAdapter;

    iget-object v2, p0, Lcom/android/settings/applications/SystemAppSettings;->yU:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Lcom/android/settings/applications/SystemAppSettings$HeaderAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/android/settings/applications/SystemAppSettings;->yV:Lcom/android/settings/applications/SystemAppSettings$HeaderAdapter;

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yV:Lcom/android/settings/applications/SystemAppSettings$HeaderAdapter;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/SystemAppSettings;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    invoke-super/range {p0 .. p5}, Lcom/android/settings/BaseListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yV:Lcom/android/settings/applications/SystemAppSettings$HeaderAdapter;

    invoke-virtual {v0, p3}, Lcom/android/settings/applications/SystemAppSettings$HeaderAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v4, 0x7f0a029b

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yU:Landroid/app/Activity;

    invoke-static {v0}, Lmiui/payment/PaymentManager;->get(Landroid/content/Context;)Lmiui/payment/PaymentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/SystemAppSettings;->yU:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lmiui/payment/PaymentManager;->gotoMiliCenter(Landroid/app/Activity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/16 v4, -0x3e8

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppSettings;->yU:Landroid/app/Activity;

    const v1, 0x7f1203b3

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v1, v0, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/SystemAppSettings;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
