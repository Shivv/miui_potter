.class public Lcom/android/settings/applications/ManageVoice;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "ManageVoice.java"

# interfaces
.implements Landroid/preference/PreferenceManager$OnPreferenceTreeClickListener;


# instance fields
.field private Dd:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

.field private De:Landroid/preference/PreferenceScreen;

.field private Df:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

.field private mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic xK(Lcom/android/settings/applications/ManageVoice;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15007f

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageVoice;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageVoice;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageVoice;->De:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageVoice;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageVoice;->mPackageManager:Landroid/content/pm/PackageManager;

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageVoice;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v2, Lcom/android/settings/applications/PreferredSettings;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "system_shortcut_wake"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string/jumbo v3, "preferred_app_intent"

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice;->Df:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v0, v0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->intent:Landroid/content/Intent;

    :goto_0
    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v3, "preferred_app_intent_filter"

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice;->Df:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v0, v0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dh:Landroid/content/IntentFilter;

    :goto_1
    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v3, "preferred_app_package_name"

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice;->Df:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v0, v0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dg:Ljava/lang/String;

    :goto_2
    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "preferred_label"

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageVoice;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f121419

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/ManageVoice;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice;->Dd:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v0, v0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->intent:Landroid/content/Intent;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice;->Dd:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v0, v0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dh:Landroid/content/IntentFilter;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice;->Dd:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v0, v0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dg:Ljava/lang/String;

    goto :goto_2
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    new-instance v0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    const-string/jumbo v1, "android.intent.action.ASSIST"

    const-string/jumbo v2, "system_shortcut_wake"

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;-><init>(Lcom/android/settings/applications/ManageVoice;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageVoice;->Df:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice;->De:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/applications/ManageVoice;->Df:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v1, v1, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dj:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    new-instance v0, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    const-string/jumbo v1, "android.intent.action.VOICE_COMMAND"

    const-string/jumbo v2, "bluetooth_wake"

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;-><init>(Lcom/android/settings/applications/ManageVoice;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageVoice;->Dd:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v0, p0, Lcom/android/settings/applications/ManageVoice;->De:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/applications/ManageVoice;->Dd:Lcom/android/settings/applications/ManageVoice$VoiceWakePref;

    iget-object v1, v1, Lcom/android/settings/applications/ManageVoice$VoiceWakePref;->Dj:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    return-void
.end method
