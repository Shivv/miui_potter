.class Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;
.super Landroid/widget/BaseAdapter;
.source "PreferredDialerSettings.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zs:Ljava/util/List;

.field private final zt:Landroid/view/LayoutInflater;

.field private final zu:Landroid/content/pm/PackageManager;

.field final synthetic zv:Lcom/android/settings/applications/PreferredDialerSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/applications/PreferredDialerSettings;Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zv:Lcom/android/settings/applications/PreferredDialerSettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zu:Landroid/content/pm/PackageManager;

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zt:Landroid/view/LayoutInflater;

    new-instance v0, Lcom/android/settings/applications/PreferredDialerSettings$DisplayNameComparator;

    invoke-direct {v0}, Lcom/android/settings/applications/PreferredDialerSettings$DisplayNameComparator;-><init>()V

    invoke-static {p3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iput-object p3, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zs:Ljava/util/List;

    return-void
.end method

.method private vt(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zu:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zu:Landroid/content/pm/PackageManager;

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->getItem(I)Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zt:Landroid/view/LayoutInflater;

    const v1, 0x7f0d0163

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->getItem(I)Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationInfo;

    move-result-object v2

    const v0, 0x7f0a0378

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iget-object v1, v2, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->zv:Lcom/android/settings/applications/PreferredDialerSettings;

    invoke-static {v3}, Lcom/android/settings/applications/PreferredDialerSettings;->vq(Lcom/android/settings/applications/PreferredDialerSettings;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    const v1, 0x7f0a01f0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v3, v2, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationListAdapter;->vt(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f0a02b7

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    iget-object v2, v2, Lcom/android/settings/applications/PreferredDialerSettings$DialerApplicationInfo;->zp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    const v0, 0x7f0a0393

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-object p2
.end method
