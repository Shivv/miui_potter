.class public final Lcom/android/settings/applications/MiuiSmsDefaultDialog;
.super Landroid/app/Activity;
.source "MiuiSmsDefaultDialog.java"


# instance fields
.field private pv:I

.field private pw:Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private oQ(Ljava/lang/String;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-nez v0, :cond_0

    return v2

    :cond_0
    invoke-static {p1, p0}, Lcom/android/internal/telephony/SmsApplication;->getSmsApplicationData(Ljava/lang/String;Landroid/content/Context;)Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->pw:Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;

    iget-object v0, p0, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->pw:Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;

    if-nez v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0, v3}, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->showDialog(I)V

    invoke-static {p0, v3}, Lcom/android/internal/telephony/SmsApplication;->getDefaultSmsApplication(Landroid/content/Context;Z)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/android/internal/telephony/SmsApplication;->getSmsApplicationData(Ljava/lang/String;Landroid/content/Context;)Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;

    move-result-object v0

    iget-object v0, v0, Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;->mPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->pw:Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;

    iget-object v1, v1, Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v2

    :cond_2
    return v3
.end method

.method static synthetic oR(Lcom/android/settings/applications/MiuiSmsDefaultDialog;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->pv:I

    return v0
.end method

.method static synthetic oS(Lcom/android/settings/applications/MiuiSmsDefaultDialog;)Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->pw:Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;

    return-object v0
.end method

.method static synthetic oT(Lcom/android/settings/applications/MiuiSmsDefaultDialog;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->pv:I

    return p1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->setResult(I)V

    invoke-direct {p0, v0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->oQ(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->finish()V

    :cond_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5

    const v4, 0x7f120cff

    const v1, 0x7f120cfd

    const/4 v0, 0x0

    iput p1, p0, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->pv:I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->pw:Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;

    invoke-virtual {v2, p0}, Lcom/android/internal/telephony/SmsApplication$SmsApplicationData;->getApplicationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f120cfe

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/MiuiSmsDefaultDialog$1;

    invoke-direct {v1, p0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog$1;-><init>(Lcom/android/settings/applications/MiuiSmsDefaultDialog;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/MiuiSmsDefaultDialog$2;

    invoke-direct {v1, p0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog$2;-><init>(Lcom/android/settings/applications/MiuiSmsDefaultDialog;)V

    const v2, 0x7f120b58

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/MiuiSmsDefaultDialog$3;

    invoke-direct {v1, p0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog$3;-><init>(Lcom/android/settings/applications/MiuiSmsDefaultDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120cfc

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/MiuiSmsDefaultDialog$4;

    invoke-direct {v1, p0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog$4;-><init>(Lcom/android/settings/applications/MiuiSmsDefaultDialog;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/MiuiSmsDefaultDialog$5;

    invoke-direct {v1, p0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog$5;-><init>(Lcom/android/settings/applications/MiuiSmsDefaultDialog;)V

    const v2, 0x1040013

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/MiuiSmsDefaultDialog$6;

    invoke-direct {v1, p0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog$6;-><init>(Lcom/android/settings/applications/MiuiSmsDefaultDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/MiuiSmsDefaultDialog;->setVisible(Z)V

    return-void
.end method
