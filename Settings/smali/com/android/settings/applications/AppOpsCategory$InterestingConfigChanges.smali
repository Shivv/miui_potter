.class public Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;
.super Ljava/lang/Object;
.source "AppOpsCategory.java"


# instance fields
.field final rX:Landroid/content/res/Configuration;

.field rY:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;->rX:Landroid/content/res/Configuration;

    return-void
.end method


# virtual methods
.method qT(Landroid/content/res/Resources;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;->rX:Landroid/content/res/Configuration;

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    move-result v3

    iget v0, p0, Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;->rY:I

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    if-eq v0, v4, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    and-int/lit16 v0, v3, 0x304

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, p0, Lcom/android/settings/applications/AppOpsCategory$InterestingConfigChanges;->rY:I

    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    return v2
.end method
