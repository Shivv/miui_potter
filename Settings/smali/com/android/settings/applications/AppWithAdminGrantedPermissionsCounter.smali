.class public abstract Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;
.super Lcom/android/settings/applications/AppCounter;
.source "AppWithAdminGrantedPermissionsCounter.java"


# instance fields
.field private final Ad:Lcom/android/settings/enterprise/q;

.field private final Ae:Lcom/android/settings/applications/IPackageManagerWrapper;

.field private final Af:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Lcom/android/settings/applications/PackageManagerWrapper;Lcom/android/settings/applications/IPackageManagerWrapper;Lcom/android/settings/enterprise/q;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lcom/android/settings/applications/AppCounter;-><init>(Landroid/content/Context;Lcom/android/settings/applications/PackageManagerWrapper;)V

    iput-object p2, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;->Af:[Ljava/lang/String;

    iput-object p4, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;->Ae:Lcom/android/settings/applications/IPackageManagerWrapper;

    iput-object p5, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;->Ad:Lcom/android/settings/enterprise/q;

    return-void
.end method

.method public static vQ([Ljava/lang/String;Lcom/android/settings/enterprise/q;Lcom/android/settings/applications/PackageManagerWrapper;Lcom/android/settings/applications/IPackageManagerWrapper;Landroid/content/pm/ApplicationInfo;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget v0, p4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v2, 0x17

    if-lt v0, v2, :cond_2

    array-length v2, p0

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    iget-object v4, p4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {p1, v5, v4, v3}, Lcom/android/settings/enterprise/q;->RE(Landroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-ne v3, v6, :cond_0

    return v6

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1

    :cond_2
    iget-object v0, p4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    new-instance v2, Landroid/os/UserHandle;

    iget v3, p4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    invoke-interface {p2, v0, v2}, Lcom/android/settings/applications/PackageManagerWrapper;->uU(Ljava/lang/String;Landroid/os/UserHandle;)I

    move-result v0

    if-eq v0, v6, :cond_3

    return v1

    :cond_3
    :try_start_0
    array-length v2, p0

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, p0, v0

    iget v4, p4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-interface {p3, v3, v4}, Lcom/android/settings/applications/IPackageManagerWrapper;->po(Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_4

    return v6

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_5
    return v1
.end method


# virtual methods
.method protected vP(Landroid/content/pm/ApplicationInfo;)Z
    .locals 4

    iget-object v0, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;->Af:[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;->Ad:Lcom/android/settings/enterprise/q;

    iget-object v2, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;->AT:Lcom/android/settings/applications/PackageManagerWrapper;

    iget-object v3, p0, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;->Ae:Lcom/android/settings/applications/IPackageManagerWrapper;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/android/settings/applications/AppWithAdminGrantedPermissionsCounter;->vQ([Ljava/lang/String;Lcom/android/settings/enterprise/q;Lcom/android/settings/applications/PackageManagerWrapper;Lcom/android/settings/applications/IPackageManagerWrapper;Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    return v0
.end method
