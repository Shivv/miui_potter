.class Lcom/android/settings/applications/ProcessStatsSummary$SummaryProvider;
.super Ljava/lang/Object;
.source "ProcessStatsSummary.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final pT:Lcom/android/settings/dashboard/C;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/ProcessStatsSummary$SummaryProvider;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/applications/ProcessStatsSummary$SummaryProvider;->pT:Lcom/android/settings/dashboard/C;

    return-void
.end method


# virtual methods
.method public jt(Z)V
    .locals 7

    const/4 v6, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/android/settings/applications/ProcStatsData;

    iget-object v1, p0, Lcom/android/settings/applications/ProcessStatsSummary$SummaryProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v6}, Lcom/android/settings/applications/ProcStatsData;-><init>(Landroid/content/Context;Z)V

    sget-object v1, Lcom/android/settings/applications/ProcessStatsSummary;->Bw:[J

    aget-wide v2, v1, v6

    invoke-virtual {v0, v2, v3}, Lcom/android/settings/applications/ProcStatsData;->zC(J)V

    invoke-virtual {v0}, Lcom/android/settings/applications/ProcStatsData;->zD()Lcom/android/settings/applications/ProcStatsData$MemInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ProcessStatsSummary$SummaryProvider;->mContext:Landroid/content/Context;

    iget-wide v2, v0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fx:D

    double-to-long v2, v2

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/applications/ProcessStatsSummary$SummaryProvider;->mContext:Landroid/content/Context;

    iget-wide v4, v0, Lcom/android/settings/applications/ProcStatsData$MemInfo;->Fy:D

    double-to-long v4, v4

    invoke-static {v2, v4, v5}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/applications/ProcessStatsSummary$SummaryProvider;->pT:Lcom/android/settings/dashboard/C;

    iget-object v3, p0, Lcom/android/settings/applications/ProcessStatsSummary$SummaryProvider;->mContext:Landroid/content/Context;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    const/4 v1, 0x1

    aput-object v0, v4, v1

    const v0, 0x7f120a6a

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, p0, v0}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
