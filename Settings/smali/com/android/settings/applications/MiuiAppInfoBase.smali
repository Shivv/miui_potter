.class public abstract Lcom/android/settings/applications/MiuiAppInfoBase;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiAppInfoBase.java"

# interfaces
.implements Lcom/android/settingslib/b/f;


# static fields
.field protected static final TAG:Ljava/lang/String;


# instance fields
.field protected CA:Z

.field protected CB:Z

.field protected final CC:Landroid/content/BroadcastReceiver;

.field protected CD:Lcom/android/settingslib/b/b;

.field protected CE:Landroid/hardware/usb/IUsbManager;

.field protected Cv:Landroid/content/pm/PackageManager;

.field protected Cw:Lcom/android/settingslib/n;

.field protected Cx:Z

.field protected Cy:Landroid/app/admin/DevicePolicyManager;

.field protected Cz:Lcom/android/settings/applications/ApplicationFeatureProvider;

.field protected mAppEntry:Lcom/android/settingslib/b/h;

.field protected mPackageInfo:Landroid/content/pm/PackageInfo;

.field protected mPackageName:Ljava/lang/String;

.field protected mState:Lcom/android/settingslib/b/a;

.field protected mUserId:I

.field protected mUserManager:Landroid/os/UserManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/applications/MiuiAppInfoBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/applications/MiuiAppInfoBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/applications/MiuiAppInfoBase$1;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/MiuiAppInfoBase$1;-><init>(Lcom/android/settings/applications/MiuiAppInfoBase;)V

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CC:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method public in()V
    .locals 0

    return-void
.end method

.method public ip()V
    .locals 0

    return-void
.end method

.method public iq()V
    .locals 0

    return-void
.end method

.method public ir()V
    .locals 0

    return-void
.end method

.method public is()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->refreshUi()Z

    return-void
.end method

.method public it(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public iu(Ljava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public iv(Z)V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CA:Z

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/overlay/a;->aIp(Landroid/content/Context;)Lcom/android/settings/applications/ApplicationFeatureProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->Cz:Lcom/android/settings/applications/ApplicationFeatureProvider;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/b/a;->ceS(Landroid/app/Application;)Lcom/android/settingslib/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mState:Lcom/android/settingslib/b/a;

    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mState:Lcom/android/settingslib/b/a;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/b/a;->ceZ(Lcom/android/settingslib/b/f;)Lcom/android/settingslib/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CD:Lcom/android/settingslib/b/b;

    const-string/jumbo v0, "device_policy"

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->Cy:Landroid/app/admin/DevicePolicyManager;

    const-string/jumbo v0, "user"

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->Cv:Landroid/content/pm/PackageManager;

    const-string/jumbo v0, "usb"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/hardware/usb/IUsbManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/usb/IUsbManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CE:Landroid/hardware/usb/IUsbManager;

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->retrieveAppEntry()Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mAppEntry:Lcom/android/settingslib/b/h;

    if-nez v0, :cond_0

    invoke-virtual {p0, v2, v2}, Lcom/android/settings/applications/MiuiAppInfoBase;->xk(ZZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->xm()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->xn()V

    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CD:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfy()V

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CD:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->pause()V

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CD:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfv()V

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "no_control_apps"

    iget v2, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mUserId:I

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->Cw:Lcom/android/settingslib/n;

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "no_control_apps"

    iget v2, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mUserId:I

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->Cx:Z

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->refreshUi()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v3, v3}, Lcom/android/settings/applications/MiuiAppInfoBase;->xk(ZZ)V

    :cond_0
    return-void
.end method

.method protected abstract op(II)Landroid/app/AlertDialog;
.end method

.method protected abstract refreshUi()Z
.end method

.method protected retrieveAppEntry()Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string/jumbo v0, "package"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mPackageName:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mPackageName:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "package"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mPackageName:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mPackageName:Ljava/lang/String;

    if-nez v0, :cond_1

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mPackageName:Ljava/lang/String;

    :cond_1
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mUserId:I

    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mState:Lcom/android/settingslib/b/a;

    iget-object v2, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mPackageName:Ljava/lang/String;

    iget v3, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mUserId:I

    invoke-virtual {v0, v2, v3}, Lcom/android/settingslib/b/a;->ceV(Ljava/lang/String;I)Lcom/android/settingslib/b/h;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mAppEntry:Lcom/android/settingslib/b/h;

    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mAppEntry:Lcom/android/settingslib/b/h;

    if-eqz v0, :cond_4

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->Cv:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mAppEntry:Lcom/android/settingslib/b/h;

    iget-object v1, v1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const v2, 0x401240

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mPackageInfo:Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    iget-object v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mPackageName:Ljava/lang/String;

    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "intent"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/settings/applications/MiuiAppInfoBase;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception when retrieving package:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mAppEntry:Lcom/android/settingslib/b/h;

    iget-object v3, v3, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/android/settings/applications/MiuiAppInfoBase;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "Missing AppEntry; maybe reinstalling?"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v1, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->mPackageInfo:Landroid/content/pm/PackageInfo;

    goto :goto_2
.end method

.method protected xj(II)V
    .locals 4

    invoke-static {p1, p2}, Lcom/android/settings/applications/MiuiAppInfoBase$MyAlertDialogFragment;->xo(II)Lcom/android/settings/applications/MiuiAppInfoBase$MyAlertDialogFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "dialog "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method protected xk(ZZ)V
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v0, "chg"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bL;

    const/4 v2, -0x1

    invoke-virtual {v0, p0, v2, v1}, Lcom/android/settings/bL;->bME(Landroid/app/Fragment;ILandroid/content/Intent;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CA:Z

    return-void
.end method

.method protected xl()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finishAndRemoveTask()V

    return-void
.end method

.method protected xm()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CB:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CB:Z

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CC:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected xn()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CB:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CB:Z

    invoke-virtual {p0}, Lcom/android/settings/applications/MiuiAppInfoBase;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/MiuiAppInfoBase;->CC:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
