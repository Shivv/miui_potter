.class public Lcom/android/settings/applications/IconLoader;
.super Ljava/lang/Object;
.source "IconLoader.java"


# instance fields
.field private AM:Lcom/android/settings/applications/IconLoader$BackgroundHandler;

.field private AN:Landroid/os/HandlerThread;

.field private AO:Ljava/lang/String;

.field AP:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/applications/IconLoader$1;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/IconLoader$1;-><init>(Lcom/android/settings/applications/IconLoader;)V

    iput-object v0, p0, Lcom/android/settings/applications/IconLoader;->AP:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/settings/applications/IconLoader;->AO:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public start()V
    .locals 3

    new-instance v0, Landroid/os/HandlerThread;

    iget-object v1, p0, Lcom/android/settings/applications/IconLoader;->AO:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/settings/applications/IconLoader;->AN:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/settings/applications/IconLoader;->AN:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/settings/applications/IconLoader$BackgroundHandler;

    iget-object v1, p0, Lcom/android/settings/applications/IconLoader;->AN:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/applications/IconLoader$BackgroundHandler;-><init>(Lcom/android/settings/applications/IconLoader;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/applications/IconLoader;->AM:Lcom/android/settings/applications/IconLoader$BackgroundHandler;

    return-void
.end method

.method public stop()V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/applications/IconLoader;->AP:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/applications/IconLoader;->AM:Lcom/android/settings/applications/IconLoader$BackgroundHandler;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lcom/android/settings/applications/IconLoader$BackgroundHandler;->removeMessages(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/IconLoader;->AN:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    return-void
.end method

.method public wh(Landroid/widget/ImageView;Lcom/android/settingslib/b/h;I)V
    .locals 3

    new-instance v0, Lcom/android/settings/applications/IconLoader$IconItem;

    invoke-direct {v0, p1, p2}, Lcom/android/settings/applications/IconLoader$IconItem;-><init>(Landroid/widget/ImageView;Lcom/android/settingslib/b/h;)V

    iget-object v1, p0, Lcom/android/settings/applications/IconLoader;->AM:Lcom/android/settings/applications/IconLoader$BackgroundHandler;

    invoke-virtual {v1, p3}, Lcom/android/settings/applications/IconLoader$BackgroundHandler;->wj(I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/applications/IconLoader;->AM:Lcom/android/settings/applications/IconLoader$BackgroundHandler;

    invoke-virtual {v2}, Lcom/android/settings/applications/IconLoader$BackgroundHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    iput v1, v2, Landroid/os/Message;->what:I

    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/applications/IconLoader;->AM:Lcom/android/settings/applications/IconLoader$BackgroundHandler;

    invoke-virtual {v0, v2}, Lcom/android/settings/applications/IconLoader$BackgroundHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
