.class public Lcom/android/settings/applications/RunningApplicationsFragment;
.super Lcom/android/settings/BaseFragment;
.source "RunningApplicationsFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lcom/android/settings/applications/RunningState$OnRefreshUiListener;


# instance fields
.field Ef:J

.field final Eg:Ljava/util/HashMap;

.field private Eh:Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

.field private Ei:Landroid/app/ActivityManager;

.field private Ej:Ljava/lang/StringBuilder;

.field private Ek:Lcom/android/settings/applications/ApplicationsContainer;

.field El:Lcom/android/settings/applications/RunningState$BaseItem;

.field Em:J

.field En:J

.field Eo:J

.field Ep:I

.field Eq:I

.field Er:I

.field Es:J

.field private Et:I

.field private Eu:Landroid/widget/ListView;

.field Ev:Lcom/android/internal/util/MemInfoReader;

.field private Ew:Z

.field private Ex:Lcom/android/settings/applications/RunningState;

.field private mContext:Landroid/content/Context;

.field private mRootView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, -0x1

    const-wide/16 v2, -0x1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eg:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ej:Ljava/lang/StringBuilder;

    new-instance v0, Lcom/android/internal/util/MemInfoReader;

    invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ev:Lcom/android/internal/util/MemInfoReader;

    iput v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ep:I

    iput v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eq:I

    iput v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Er:I

    iput-wide v2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->En:J

    iput-wide v2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eo:J

    iput-wide v2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Es:J

    iput-wide v2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Em:J

    return-void
.end method

.method private yL(Lcom/android/settings/applications/RunningState$MergedItem;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "com.android.settings"

    const-string/jumbo v2, "com.android.settings.applications.RunningServiceDetailsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "uid"

    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v2, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->wq:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "user_id"

    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v2, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->mUserId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "process"

    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v2, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->wr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "background"

    iget-boolean v2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ew:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic yN(Lcom/android/settings/applications/RunningApplicationsFragment;)Ljava/lang/StringBuilder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ej:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic yO(Lcom/android/settings/applications/RunningApplicationsFragment;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic yP(Lcom/android/settings/applications/RunningApplicationsFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ew:Z

    return v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lmiui/R$style;->Theme_Light_Settings_NoTitle:I

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->setThemeRes(I)V

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "filter_app_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Et:I

    iget v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Et:I

    const/4 v1, 0x3

    if-ne v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ew:Z

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/android/settings/applications/ApplicationsContainer;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ApplicationsContainer;

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ek:Lcom/android/settings/applications/ApplicationsContainer;

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ek:Lcom/android/settings/applications/ApplicationsContainer;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getParentFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ApplicationsContainer;

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ek:Lcom/android/settings/applications/ApplicationsContainer;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ei:Landroid/app/ActivityManager;

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/applications/RunningState;->getInstance(Landroid/content/Context;)Lcom/android/settings/applications/RunningState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    const v0, 0x7f0d00e9

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eu:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eu:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eu:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eu:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    new-instance v0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;-><init>(Lcom/android/settings/applications/RunningApplicationsFragment;Lcom/android/settings/applications/RunningState;)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eh:Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eu:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eh:Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ei:Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide v0, v0, Landroid/app/ActivityManager$MemoryInfo;->secondaryServerThreshold:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ef:J

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningState$MergedItem;

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->El:Lcom/android/settings/applications/RunningState$BaseItem;

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/android/settings/MiuiSettings;

    if-eqz v1, :cond_0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "uid"

    iget-object v2, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v2, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->wq:I

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "process"

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$MergedItem;->wF:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->wr:Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "background"

    iget-boolean v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ew:Z

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, ":android:show_fragment_title"

    const v1, 0x7f120162

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getParentFragment()Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/android/settings/applications/ApplicationsContainer;

    const-class v0, Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const v5, 0x7f120e63

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/applications/RunningApplicationsFragment;->bLT(Lmiui/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yL(Lcom/android/settings/applications/RunningState$MergedItem;)V

    goto :goto_0
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eg:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ek:Lcom/android/settings/applications/ApplicationsContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ek:Lcom/android/settings/applications/ApplicationsContainer;

    iget-object v0, v0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ek:Lcom/android/settings/applications/ApplicationsContainer;

    iget-object v0, v0, Lcom/android/settings/applications/ApplicationsContainer;->te:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    iget v0, v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->ty:I

    iget v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Et:I

    if-ne v0, v1, :cond_0

    const-string/jumbo v1, "RunningApplicationsFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "call resume RunningState, tab = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yJ()V

    :cond_0
    return-void
.end method

.method public qJ(I)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yM()V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yK(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yM()V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yK(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yM()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public yJ()V
    .locals 3

    const-string/jumbo v0, "RunningApplicationsFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "resume RunningState, tab = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Et:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/applications/RunningState;->getInstance(Landroid/content/Context;)Lcom/android/settings/applications/RunningState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0, p0}, Lcom/android/settings/applications/RunningState;->sE(Lcom/android/settings/applications/RunningState$OnRefreshUiListener;)V

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eh:Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->sH()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->yK(Z)V

    :cond_1
    return-void
.end method

.method yK(Z)V
    .locals 8

    const-wide/16 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eu:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->yS()V

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->notifyDataSetChanged()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ev:Lcom/android/internal/util/MemInfoReader;

    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ev:Lcom/android/internal/util/MemInfoReader;

    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getFreeSize()J

    move-result-wide v0

    iget-object v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ev:Lcom/android/internal/util/MemInfoReader;

    invoke-virtual {v4}, Lcom/android/internal/util/MemInfoReader;->getCachedSize()J

    move-result-wide v4

    add-long/2addr v0, v4

    iget-wide v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ef:J

    sub-long/2addr v0, v4

    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    move-wide v0, v2

    :cond_1
    iget-object v2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget-object v2, v2, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ep:I

    iget-object v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget v4, v4, Lcom/android/settings/applications/RunningState;->vz:I

    if-ne v3, v4, :cond_2

    iget-wide v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->En:J

    iget-object v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget-wide v6, v3, Lcom/android/settings/applications/RunningState;->vi:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_6

    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget v3, v3, Lcom/android/settings/applications/RunningState;->vz:I

    iput v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ep:I

    iget-object v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget-wide v4, v3, Lcom/android/settings/applications/RunningState;->vi:J

    iput-wide v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->En:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Em:J

    :cond_3
    iget v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eq:I

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget v1, v1, Lcom/android/settings/applications/RunningState;->vA:I

    if-ne v0, v1, :cond_4

    iget-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eo:J

    iget-object v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget-wide v4, v3, Lcom/android/settings/applications/RunningState;->vq:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget v0, v0, Lcom/android/settings/applications/RunningState;->vA:I

    iput v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eq:I

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget-wide v0, v0, Lcom/android/settings/applications/RunningState;->vq:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eo:J

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget v0, v0, Lcom/android/settings/applications/RunningState;->vB:I

    iput v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Er:I

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget-wide v0, v0, Lcom/android/settings/applications/RunningState;->vj:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Es:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    monitor-exit v2

    return-void

    :cond_6
    :try_start_1
    iget-wide v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Em:J

    cmp-long v3, v4, v0

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_7
    iget v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Er:I

    iget-object v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget v1, v1, Lcom/android/settings/applications/RunningState;->vB:I

    if-ne v0, v1, :cond_4

    iget-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Es:J

    iget-object v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ex:Lcom/android/settings/applications/RunningState;

    iget-wide v4, v3, Lcom/android/settings/applications/RunningState;->vj:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method yM()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Eg:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    iget-object v2, v0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mRootView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->Ej:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2, v3}, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->st(Landroid/content/Context;Ljava/lang/StringBuilder;)V

    goto :goto_0

    :cond_1
    return-void
.end method
