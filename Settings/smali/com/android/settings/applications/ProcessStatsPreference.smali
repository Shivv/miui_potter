.class public Lcom/android/settings/applications/ProcessStatsPreference;
.super Lcom/android/settings/AppProgressPreference;
.source "ProcessStatsPreference.java"


# instance fields
.field private BF:Lcom/android/settings/applications/ProcStatsPackageEntry;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/AppProgressPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public wA()Lcom/android/settings/applications/ProcStatsPackageEntry;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ProcessStatsPreference;->BF:Lcom/android/settings/applications/ProcStatsPackageEntry;

    return-object v0
.end method

.method public wB(Lcom/android/settings/applications/ProcStatsPackageEntry;Landroid/content/pm/PackageManager;DDDZ)V
    .locals 7

    iput-object p1, p0, Lcom/android/settings/applications/ProcessStatsPreference;->BF:Lcom/android/settings/applications/ProcStatsPackageEntry;

    iget-object v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->AI:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->mPackage:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ProcessStatsPreference"

    const-string/jumbo v1, "PackageEntry contained no package name or uiLabel"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->AJ:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->AJ:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, p2}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    iget-wide v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->Ay:D

    iget-wide v2, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->Az:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz p9, :cond_5

    if-eqz v0, :cond_4

    iget-wide v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->Ay:D

    :goto_3
    mul-double/2addr v0, p5

    :goto_4
    invoke-virtual {p0}, Lcom/android/settings/applications/ProcessStatsPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    double-to-long v4, v0

    invoke-static {v2, v4, v5}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/settings/applications/ProcessStatsPreference;->setSummary(Ljava/lang/CharSequence;)V

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    div-double/2addr v0, p3

    double-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsPreference;->setProgress(I)V

    return-void

    :cond_1
    iget-object v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->AI:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ProcessStatsPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    iget-wide v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->Az:D

    goto :goto_3

    :cond_5
    if-eqz v0, :cond_6

    iget-wide v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->AB:J

    :goto_5
    long-to-double v0, v0

    mul-double/2addr v0, p7

    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    mul-double/2addr v0, v2

    goto :goto_4

    :cond_6
    iget-wide v0, p1, Lcom/android/settings/applications/ProcStatsPackageEntry;->AA:J

    goto :goto_5
.end method
