.class public Lcom/android/settings/applications/SystemAppUpdaterStatusController;
.super Lcom/android/settings/bA;
.source "SystemAppUpdaterStatusController.java"


# instance fields
.field private Bb:Z

.field private Bc:Landroid/content/IntentFilter;

.field private Bd:Ljava/util/Locale;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;Ljava/util/Locale;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bA;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    new-instance v0, Lcom/android/settings/applications/SystemAppUpdaterStatusController$1;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/SystemAppUpdaterStatusController$1;-><init>(Lcom/android/settings/applications/SystemAppUpdaterStatusController;)V

    iput-object v0, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->Bc:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->Bc:Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.xiaomi.market.action.APP_UPDATE_CHECKED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iput-object p3, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->Bd:Ljava/util/Locale;

    return-void
.end method


# virtual methods
.method public pause()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->Bb:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->Bb:Z

    :cond_0
    return-void
.end method

.method public wt()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->Bc:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->Bb:Z

    iget-object v0, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "updatable_system_app_count"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->wu(I)V

    return-void
.end method

.method protected wu(I)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->bRZ:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->bRZ:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->Bd:Ljava/util/Locale;

    invoke-static {v2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/applications/SystemAppUpdaterStatusController;->bRZ:Landroid/widget/TextView;

    if-lez p1, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected wv()V
    .locals 0

    return-void
.end method
