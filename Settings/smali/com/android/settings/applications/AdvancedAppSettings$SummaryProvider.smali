.class Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;
.super Ljava/lang/Object;
.source "AdvancedAppSettings.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zA:Lcom/android/settings/applications/defaultapps/l;

.field private final zB:Lcom/android/settings/applications/defaultapps/g;

.field private final zC:Lcom/android/settings/applications/defaultapps/d;

.field private final zD:Lcom/android/settings/dashboard/C;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->zD:Lcom/android/settings/dashboard/C;

    new-instance v0, Lcom/android/settings/applications/defaultapps/d;

    iget-object v1, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/applications/defaultapps/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->zC:Lcom/android/settings/applications/defaultapps/d;

    new-instance v0, Lcom/android/settings/applications/defaultapps/l;

    iget-object v1, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/applications/defaultapps/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->zA:Lcom/android/settings/applications/defaultapps/l;

    new-instance v0, Lcom/android/settings/applications/defaultapps/g;

    iget-object v1, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/applications/defaultapps/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->zB:Lcom/android/settings/applications/defaultapps/g;

    return-void
.end method

.method private vw(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p2

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->mContext:Landroid/content/Context;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const v2, 0x7f120886

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public jt(Z)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->zC:Lcom/android/settings/applications/defaultapps/d;

    invoke-virtual {v0}, Lcom/android/settings/applications/defaultapps/d;->ni()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->zA:Lcom/android/settings/applications/defaultapps/l;

    invoke-virtual {v1}, Lcom/android/settings/applications/defaultapps/l;->ni()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->vw(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->zB:Lcom/android/settings/applications/defaultapps/g;

    invoke-virtual {v1}, Lcom/android/settings/applications/defaultapps/g;->ni()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->vw(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/applications/AdvancedAppSettings$SummaryProvider;->zD:Lcom/android/settings/dashboard/C;

    invoke-virtual {v1, p0, v0}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method
