.class public Lcom/android/settings/applications/AppLaunchSettings;
.super Lcom/android/settings/applications/MiuiAppInfoWithHeader;
.source "AppLaunchSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final pb:Landroid/content/Intent;


# instance fields
.field private oV:Lcom/android/settings/applications/AppDomainsPreference;

.field private oW:Lcom/android/settings/MiuiDropDownPreference;

.field private oX:Lcom/android/settings/applications/ClearDefaultsPreference;

.field private oY:Z

.field private oZ:Z

.field private pa:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "http:"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/android/settings/applications/AppLaunchSettings;->pb:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/MiuiAppInfoWithHeader;-><init>()V

    return-void
.end method

.method private oo()V
    .locals 8

    const/4 v0, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->oZ:Z

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oW:Lcom/android/settings/MiuiDropDownPreference;

    invoke-virtual {v0, v5}, Lcom/android/settings/MiuiDropDownPreference;->setShouldDisableView(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oW:Lcom/android/settings/MiuiDropDownPreference;

    invoke-virtual {v0, v4}, Lcom/android/settings/MiuiDropDownPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oV:Lcom/android/settings/applications/AppDomainsPreference;

    invoke-virtual {v0, v5}, Lcom/android/settings/applications/AppDomainsPreference;->setShouldDisableView(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oV:Lcom/android/settings/applications/AppDomainsPreference;

    invoke-virtual {v0, v4}, Lcom/android/settings/applications/AppDomainsPreference;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->oW:Lcom/android/settings/MiuiDropDownPreference;

    new-array v2, v7, [Ljava/lang/CharSequence;

    const v3, 0x7f120134

    invoke-virtual {p0, v3}, Lcom/android/settings/applications/AppLaunchSettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f120135

    invoke-virtual {p0, v3}, Lcom/android/settings/applications/AppLaunchSettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const v3, 0x7f120136

    invoke-virtual {p0, v3}, Lcom/android/settings/applications/AppLaunchSettings;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/android/settings/MiuiDropDownPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->oW:Lcom/android/settings/MiuiDropDownPreference;

    new-array v2, v7, [Ljava/lang/CharSequence;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/android/settings/MiuiDropDownPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->oW:Lcom/android/settings/MiuiDropDownPreference;

    iget-boolean v2, p0, Lcom/android/settings/applications/AppLaunchSettings;->oY:Z

    invoke-virtual {v1, v2}, Lcom/android/settings/MiuiDropDownPreference;->setEnabled(Z)V

    iget-boolean v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->oY:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->pa:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lcom/android/settings/applications/AppLaunchSettings;->mPackageName:Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getIntentVerificationStatusAsUser(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/applications/AppLaunchSettings;->oW:Lcom/android/settings/MiuiDropDownPreference;

    if-nez v1, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/settings/MiuiDropDownPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oW:Lcom/android/settings/MiuiDropDownPreference;

    new-instance v1, Lcom/android/settings/applications/AppLaunchSettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/applications/AppLaunchSettings$1;-><init>(Lcom/android/settings/applications/AppLaunchSettings;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDropDownPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private oq(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)[Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->pa:Landroid/content/pm/PackageManager;

    invoke-static {v0, p1}, Lcom/android/settings/aq;->bqC(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/util/ArraySet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/ArraySet;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    return-object v0
.end method

.method private or(Ljava/lang/String;)Z
    .locals 6

    const/4 v2, 0x0

    sget-object v0, Lcom/android/settings/applications/AppLaunchSettings;->pb:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->pa:Landroid/content/pm/PackageManager;

    sget-object v1, Lcom/android/settings/applications/AppLaunchSettings;->pb:Landroid/content/Intent;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    const/high16 v4, 0x20000

    invoke-virtual {v0, v1, v4, v3}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v5, :cond_0

    iget-boolean v0, v0, Landroid/content/pm/ResolveInfo;->handleAllWebDataURI:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v2
.end method

.method private os(I)Z
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->oZ:Z

    if-eqz v1, :cond_0

    return v0

    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->pa:Landroid/content/pm/PackageManager;

    iget-object v3, p0, Lcom/android/settings/applications/AppLaunchSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/pm/PackageManager;->getIntentVerificationStatusAsUser(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, p1, :cond_1

    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->pa:Landroid/content/pm/PackageManager;

    iget-object v3, p0, Lcom/android/settings/applications/AppLaunchSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v3, p1, v2}, Landroid/content/pm/PackageManager;->updateIntentVerificationStatusAsUser(Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->pa:Landroid/content/pm/PackageManager;

    iget-object v3, p0, Lcom/android/settings/applications/AppLaunchSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/pm/PackageManager;->getIntentVerificationStatusAsUser(Ljava/lang/String;I)I

    move-result v1

    if-ne p1, v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    :goto_0
    return v0

    :cond_3
    const-string/jumbo v0, "AppLaunchSettings"

    const-string/jumbo v2, "Couldn\'t update intent verification status!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method static synthetic ot(Lcom/android/settings/applications/AppLaunchSettings;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/applications/AppLaunchSettings;->os(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x11

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/applications/MiuiAppInfoWithHeader;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f15006b

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/AppLaunchSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "app_launch_supported_domain_urls"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/AppLaunchSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/AppDomainsPreference;

    iput-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oV:Lcom/android/settings/applications/AppDomainsPreference;

    const-string/jumbo v0, "app_launch_clear_defaults"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/AppLaunchSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ClearDefaultsPreference;

    iput-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oX:Lcom/android/settings/applications/ClearDefaultsPreference;

    const-string/jumbo v0, "app_link_state"

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/AppLaunchSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiDropDownPreference;

    iput-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oW:Lcom/android/settings/MiuiDropDownPreference;

    invoke-virtual {p0}, Lcom/android/settings/applications/AppLaunchSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->pa:Landroid/content/pm/PackageManager;

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->mPackageName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settings/applications/AppLaunchSettings;->or(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oZ:Z

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->mAppEntry:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->privateFlags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oY:Z

    iget-boolean v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oZ:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->pa:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getIntentFilterVerifications(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->pa:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lcom/android/settings/applications/AppLaunchSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getAllIntentFilters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/applications/AppLaunchSettings;->mPackageName:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v1}, Lcom/android/settings/applications/AppLaunchSettings;->oq(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)[Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->oV:Lcom/android/settings/applications/AppDomainsPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/applications/AppDomainsPreference;->og([Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->oV:Lcom/android/settings/applications/AppDomainsPreference;

    array-length v0, v0

    new-array v0, v0, [I

    invoke-virtual {v1, v0}, Lcom/android/settings/applications/AppDomainsPreference;->Uy([I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/applications/AppLaunchSettings;->oo()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected op(II)Landroid/app/AlertDialog;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected refreshUi()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oX:Lcom/android/settings/applications/ClearDefaultsPreference;

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ClearDefaultsPreference;->ow(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/applications/AppLaunchSettings;->oX:Lcom/android/settings/applications/ClearDefaultsPreference;

    iget-object v1, p0, Lcom/android/settings/applications/AppLaunchSettings;->mAppEntry:Lcom/android/settingslib/b/h;

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ClearDefaultsPreference;->ox(Lcom/android/settingslib/b/h;)V

    const/4 v0, 0x1

    return v0
.end method
