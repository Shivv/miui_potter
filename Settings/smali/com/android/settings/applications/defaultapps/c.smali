.class public abstract Lcom/android/settings/applications/defaultapps/c;
.super Lcom/android/settings/core/e;
.source "DefaultAppPreferenceController.java"


# instance fields
.field protected final mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

.field protected mUserId:I

.field protected final mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/applications/PackageManagerWrapperImpl;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/applications/PackageManagerWrapperImpl;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/android/settings/applications/defaultapps/c;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/applications/defaultapps/c;->mUserManager:Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/applications/defaultapps/c;->mUserId:I

    return-void
.end method

.method private nj(Lcom/android/settings/applications/defaultapps/a;Landroid/preference/Preference;)V
    .locals 2

    const/4 v1, 0x0

    instance-of v0, p2, Lcom/android/settings/widget/MiuiGearPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/applications/defaultapps/c;->ms(Lcom/android/settings/applications/defaultapps/a;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast p2, Lcom/android/settings/widget/MiuiGearPreference;

    new-instance v1, Lcom/android/settings/applications/defaultapps/-$Lambda$1lZY4MGaiIfvO5-P1qnFOvnbBh0;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/applications/defaultapps/-$Lambda$1lZY4MGaiIfvO5-P1qnFOvnbBh0;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p2, v1}, Lcom/android/settings/widget/MiuiGearPreference;->aBO(Lcom/android/settings/widget/z;)V

    :goto_0
    return-void

    :cond_1
    check-cast p2, Lcom/android/settings/widget/MiuiGearPreference;

    invoke-virtual {p2, v1}, Lcom/android/settings/widget/MiuiGearPreference;->aBO(Lcom/android/settings/widget/z;)V

    goto :goto_0
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/c;->mr()Lcom/android/settings/applications/defaultapps/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/c;->ni()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/android/settings/applications/defaultapps/c;->nj(Lcom/android/settings/applications/defaultapps/a;Landroid/preference/Preference;)V

    return-void

    :cond_0
    const-string/jumbo v1, "DefaultAppPrefControl"

    const-string/jumbo v2, "No default app"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f120138

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method

.method protected abstract mr()Lcom/android/settings/applications/defaultapps/a;
.end method

.method protected ms(Lcom/android/settings/applications/defaultapps/a;)Landroid/content/Intent;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public ni()Ljava/lang/CharSequence;
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/c;->p()Z

    move-result v0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/c;->mr()Lcom/android/settings/applications/defaultapps/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/applications/defaultapps/a;->mS()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_1
    return-object v1
.end method

.method synthetic nk(Landroid/content/Intent;Lcom/android/settings/widget/MiuiGearPreference;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/c;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
