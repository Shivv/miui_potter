.class public Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;
.super Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;
.source "DefaultPhonePicker.java"


# instance fields
.field private oA:Lcom/android/settings/applications/defaultapps/k;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x314

    return v0
.end method

.method protected mB()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->oA:Lcom/android/settings/applications/defaultapps/k;

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->mUserId:I

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/defaultapps/k;->nO(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected mG(Ljava/lang/String;)Z
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->mB()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->oA:Lcom/android/settings/applications/defaultapps/k;

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->mUserId:I

    invoke-virtual {v0, v1, p1, v2}, Lcom/android/settings/applications/defaultapps/k;->nQ(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected my()Ljava/util/List;
    .locals 8

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->mUserId:I

    invoke-static {v0, v2}, Landroid/telecom/DefaultDialerManager;->getInstalledDialerApplications(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :try_start_0
    new-instance v3, Lcom/android/settings/applications/defaultapps/a;

    iget-object v4, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    iget-object v5, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->oq:Lcom/android/settings/applications/PackageManagerWrapper;

    iget v6, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->mUserId:I

    const/4 v7, 0x0

    invoke-interface {v5, v0, v7, v6}, Lcom/android/settings/applications/PackageManagerWrapper;->uR(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/android/settings/applications/defaultapps/a;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/PackageItemInfo;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method protected nN()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->oA:Lcom/android/settings/applications/defaultapps/k;

    invoke-virtual {v0}, Lcom/android/settings/applications/defaultapps/k;->nP()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/applications/defaultapps/DefaultAppPickerFragment;->onAttach(Landroid/content/Context;)V

    new-instance v1, Lcom/android/settings/applications/defaultapps/k;

    const-string/jumbo v0, "telecom"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-direct {v1, v0}, Lcom/android/settings/applications/defaultapps/k;-><init>(Landroid/telecom/TelecomManager;)V

    iput-object v1, p0, Lcom/android/settings/applications/defaultapps/DefaultPhonePicker;->oA:Lcom/android/settings/applications/defaultapps/k;

    return-void
.end method
