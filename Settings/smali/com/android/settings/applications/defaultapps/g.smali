.class public Lcom/android/settings/applications/defaultapps/g;
.super Lcom/android/settings/applications/defaultapps/c;
.source "DefaultPhonePreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/applications/defaultapps/c;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static nA(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {p1, v0}, Landroid/telecom/DefaultDialerManager;->getDefaultDialerApplication(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ny()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/g;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/settings/applications/defaultapps/g;->mUserId:I

    invoke-static {v0, v1}, Landroid/telecom/DefaultDialerManager;->getInstalledDialerApplications(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static nz(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {p1, v0}, Landroid/telecom/DefaultDialerManager;->getInstalledDialerApplications(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "default_phone_app"

    return-object v0
.end method

.method protected mr()Lcom/android/settings/applications/defaultapps/a;
    .locals 5

    :try_start_0
    new-instance v0, Lcom/android/settings/applications/defaultapps/a;

    iget-object v1, p0, Lcom/android/settings/applications/defaultapps/g;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    iget-object v2, p0, Lcom/android/settings/applications/defaultapps/g;->mPackageManager:Lcom/android/settings/applications/PackageManagerWrapper;

    invoke-interface {v2}, Lcom/android/settings/applications/PackageManagerWrapper;->uW()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/applications/defaultapps/g;->mContext:Landroid/content/Context;

    iget v4, p0, Lcom/android/settings/applications/defaultapps/g;->mUserId:I

    invoke-static {v3, v4}, Landroid/telecom/DefaultDialerManager;->getDefaultDialerApplication(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/settings/applications/defaultapps/a;-><init>(Lcom/android/settings/applications/PackageManagerWrapper;Landroid/content/pm/PackageItemInfo;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return-object v0
.end method

.method public p()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/g;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isVoiceCapable()Z

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/defaultapps/g;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "user"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    const-string/jumbo v2, "no_outgoing_calls"

    invoke-virtual {v0, v2}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/applications/defaultapps/g;->ny()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
