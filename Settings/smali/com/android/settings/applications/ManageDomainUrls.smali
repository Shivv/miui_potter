.class public Lcom/android/settings/applications/ManageDomainUrls;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "ManageDomainUrls.java"

# interfaces
.implements Lcom/android/settingslib/b/f;
.implements Landroid/support/v7/preference/f;
.implements Landroid/support/v7/preference/g;


# instance fields
.field private Cn:Lcom/android/settingslib/b/a;

.field private Co:Landroid/support/v7/preference/PreferenceGroup;

.field private Cp:Landroid/support/v7/preference/Preference;

.field private Cq:Lcom/android/settingslib/b/b;

.field private Cr:Landroid/support/v14/preference/SwitchPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private xc()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cq:Lcom/android/settingslib/b/b;

    sget-object v1, Lcom/android/settingslib/b/a;->cAn:Lcom/android/settingslib/b/i;

    sget-object v2, Lcom/android/settingslib/b/a;->cAa:Ljava/util/Comparator;

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/b/b;->cfx(Lcom/android/settingslib/b/i;Ljava/util/Comparator;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageDomainUrls;->iu(Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method private xd(Landroid/support/v7/preference/PreferenceGroup;Ljava/util/ArrayList;)V
    .locals 6

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ManageDomainUrls;->bwj(Landroid/support/v7/preference/PreferenceGroup;)V

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, "|"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/settings/applications/ManageDomainUrls;->bwo(Ljava/lang/String;)Landroid/support/v7/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->aki()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, p0, v5, v0}, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;-><init>(Lcom/android/settings/applications/ManageDomainUrls;Landroid/content/Context;Lcom/android/settingslib/b/h;)V

    invoke-virtual {v1, v4}, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;->dkr(Landroid/support/v7/preference/g;)V

    invoke-virtual {p1, v1}, Landroid/support/v7/preference/PreferenceGroup;->im(Landroid/support/v7/preference/Preference;)Z

    :goto_1
    invoke-virtual {v1, v2}, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;->setOrder(I)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;->xg()V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ManageDomainUrls;->bwz(Landroid/support/v7/preference/PreferenceGroup;)V

    return-void
.end method


# virtual methods
.method public eH(Landroid/support/v7/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cr:Landroid/support/v14/preference/SwitchPreference;

    if-ne p1, v2, :cond_1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->bwe()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "instant_apps_enabled"

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1

    :cond_1
    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x8f

    return v0
.end method

.method public in()V
    .locals 0

    return-void
.end method

.method public ip()V
    .locals 0

    return-void
.end method

.method public iq()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/ManageDomainUrls;->xc()V

    return-void
.end method

.method public ir()V
    .locals 0

    return-void
.end method

.method public is()V
    .locals 0

    return-void
.end method

.method public it(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public iu(Ljava/util/ArrayList;)V
    .locals 8

    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->getContext()Landroid/content/Context;

    move-result-object v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "enable_ephemeral_feature"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_2

    move v3, v0

    :goto_0
    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Co:Landroid/support/v7/preference/PreferenceGroup;

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Co:Landroid/support/v7/preference/PreferenceGroup;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/applications/ManageDomainUrls;->xd(Landroid/support/v7/preference/PreferenceGroup;Ljava/util/ArrayList;)V

    return-void

    :cond_2
    move v3, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/preference/PreferenceGroup;->dln()I

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Landroid/support/v7/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->aki()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/support/v7/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const v5, 0x7f1214a6

    invoke-virtual {v4, v5}, Landroid/support/v7/preference/PreferenceCategory;->setTitle(I)V

    invoke-virtual {v3, v4}, Landroid/support/v7/preference/PreferenceGroup;->im(Landroid/support/v7/preference/Preference;)Z

    new-instance v5, Landroid/support/v14/preference/SwitchPreference;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->aki()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/support/v14/preference/SwitchPreference;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cr:Landroid/support/v14/preference/SwitchPreference;

    iget-object v5, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cr:Landroid/support/v14/preference/SwitchPreference;

    const v6, 0x7f1214a5

    invoke-virtual {v5, v6}, Landroid/support/v14/preference/SwitchPreference;->setTitle(I)V

    iget-object v5, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cr:Landroid/support/v14/preference/SwitchPreference;

    const v6, 0x7f1214a4

    invoke-virtual {v5, v6}, Landroid/support/v14/preference/SwitchPreference;->dks(I)V

    iget-object v5, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cr:Landroid/support/v14/preference/SwitchPreference;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->bwe()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "instant_apps_enabled"

    invoke-static {v6, v7, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_5

    :goto_2
    invoke-virtual {v5, v0}, Landroid/support/v14/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cr:Landroid/support/v14/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/support/v14/preference/SwitchPreference;->dkJ(Landroid/support/v7/preference/f;)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cr:Landroid/support/v14/preference/SwitchPreference;

    invoke-virtual {v4, v0}, Landroid/support/v7/preference/PreferenceCategory;->im(Landroid/support/v7/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getInstantAppResolverSettingsComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_6

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_4

    new-instance v1, Landroid/support/v7/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->aki()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cp:Landroid/support/v7/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cp:Landroid/support/v7/preference/Preference;

    const v2, 0x7f120872

    invoke-virtual {v1, v2}, Landroid/support/v7/preference/Preference;->setTitle(I)V

    iget-object v1, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cp:Landroid/support/v7/preference/Preference;

    new-instance v2, Lcom/android/settings/applications/-$Lambda$n-7gd7QmmOETk3LCVsVO9yDxQpo;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/applications/-$Lambda$n-7gd7QmmOETk3LCVsVO9yDxQpo;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/preference/Preference;->dkr(Landroid/support/v7/preference/g;)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cp:Landroid/support/v7/preference/Preference;

    invoke-virtual {v4, v0}, Landroid/support/v7/preference/PreferenceCategory;->im(Landroid/support/v7/preference/Preference;)Z

    :cond_4
    new-instance v0, Landroid/support/v7/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->aki()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Co:Landroid/support/v7/preference/PreferenceGroup;

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Co:Landroid/support/v7/preference/PreferenceGroup;

    const v1, 0x7f120640

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceGroup;->setTitle(I)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Co:Landroid/support/v7/preference/PreferenceGroup;

    invoke-virtual {v3, v0}, Landroid/support/v7/preference/PreferenceGroup;->im(Landroid/support/v7/preference/Preference;)Z

    goto/16 :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move-object v0, v2

    goto :goto_3
.end method

.method public iv(Z)V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v2}, Lcom/android/settings/applications/ManageDomainUrls;->bwA(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->dju()Landroid/support/v7/preference/k;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/k;->dlN(Landroid/content/Context;)Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageDomainUrls;->ajw(Landroid/support/v7/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/android/settingslib/b/a;->ceS(Landroid/app/Application;)Lcom/android/settingslib/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cn:Lcom/android/settingslib/b/a;

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cn:Lcom/android/settingslib/b/a;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/b/a;->ceZ(Lcom/android/settingslib/b/f;)Lcom/android/settingslib/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cq:Lcom/android/settingslib/b/b;

    invoke-virtual {p0, v2}, Lcom/android/settings/applications/ManageDomainUrls;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cq:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cq:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->pause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageDomainUrls;->Cq:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfv()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    return-void
.end method

.method public xb(Landroid/support/v7/preference/Preference;)Z
    .locals 7

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;

    if-ne v0, v1, :cond_0

    check-cast p1, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;

    invoke-static {p1}, Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;->xi(Lcom/android/settings/applications/ManageDomainUrls$DomainAppPreference;)Lcom/android/settingslib/b/h;

    move-result-object v1

    const-class v0, Lcom/android/settings/applications/AppLaunchSettings;

    iget-object v2, v1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v1, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v3, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageDomainUrls;->getMetricsCategory()I

    move-result v6

    const v1, 0x7f1201b0

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Lcom/android/settings/applications/AppInfoBase;->oM(Ljava/lang/Class;ILjava/lang/String;ILandroid/app/Fragment;II)V

    return v5

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method synthetic xe(Landroid/content/Intent;Landroid/support/v7/preference/Preference;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ManageDomainUrls;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    return v0
.end method
