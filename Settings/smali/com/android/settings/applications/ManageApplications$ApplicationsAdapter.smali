.class Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;
.super Landroid/widget/BaseAdapter;
.source "ManageApplications.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/android/settingslib/b/f;
.implements Lcom/android/settings/applications/AppStateBaseBridge$Callback;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/SectionIndexer;


# static fields
.field private static final xU:[Lcom/android/settings/applications/ManageApplications$SectionInfo;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mState:Lcom/android/settingslib/b/a;

.field private final xV:Ljava/util/ArrayList;

.field private xW:Ljava/util/ArrayList;

.field private final xX:Landroid/os/Handler;

.field private xY:Lcom/android/settingslib/b/i;

.field xZ:Ljava/lang/CharSequence;

.field private ya:Ljava/util/ArrayList;

.field private final yb:Lcom/android/settings/applications/AppStateBaseBridge;

.field private yc:Lcom/android/settings/applications/FileViewHolderController;

.field private final yd:Landroid/os/Handler;

.field private ye:Landroid/widget/Filter;

.field private yf:I

.field private yg:Z

.field private yh:Z

.field private yi:Landroid/icu/text/AlphabeticIndex$ImmutableIndex;

.field private yj:I

.field private yk:I

.field private yl:I

.field private final ym:Lcom/android/settings/applications/ManageApplications;

.field private yn:Landroid/content/pm/PackageManager;

.field private yo:[I

.field private yp:Z

.field private yq:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

.field private final yr:Lcom/android/settingslib/b/b;

.field private ys:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/android/settings/applications/ManageApplications$SectionInfo;

    sput-object v0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xU:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    return-void
.end method

.method public constructor <init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/ManageApplications;I)V
    .locals 4

    const/4 v1, -0x1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xV:Ljava/util/ArrayList;

    iput v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yk:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ys:I

    iput v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yj:I

    sget-object v0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xU:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yq:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    new-instance v0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter$1;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter$1;-><init>(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ye:Landroid/widget/Filter;

    iput-object p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yd:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    invoke-virtual {v1}, Lcom/android/settingslib/b/a;->cfg()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xX:Landroid/os/Handler;

    invoke-virtual {p1, p0}, Lcom/android/settingslib/b/a;->ceZ(Lcom/android/settingslib/b/f;)Lcom/android/settingslib/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yr:Lcom/android/settingslib/b/b;

    iput-object p2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-virtual {p2}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yn:Landroid/content/pm/PackageManager;

    iput p3, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yf:I

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v0, v0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/android/settings/applications/AppStateNotificationBridge;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    invoke-static {p2}, Lcom/android/settings/applications/ManageApplications;->tO(Lcom/android/settings/applications/ManageApplications;)Lcom/android/settings/notification/NotificationBackend;

    move-result-object v3

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/android/settings/applications/AppStateNotificationBridge;-><init>(Landroid/content/Context;Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;Lcom/android/settings/notification/NotificationBackend;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v0, v0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/android/settings/applications/AppStateUsageBridge;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    invoke-direct {v0, v1, v2, p0}, Lcom/android/settings/applications/AppStateUsageBridge;-><init>(Landroid/content/Context;Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v0, v0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/android/settings/applications/AppStatePowerBridge;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    invoke-direct {v0, v1, p0}, Lcom/android/settings/applications/AppStatePowerBridge;-><init>(Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v0, v0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    new-instance v0, Lcom/android/settings/applications/AppStateOverlayBridge;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    invoke-direct {v0, v1, v2, p0}, Lcom/android/settings/applications/AppStateOverlayBridge;-><init>(Landroid/content/Context;Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v0, v0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_4

    new-instance v0, Lcom/android/settings/applications/AppStateWriteSettingsBridge;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    invoke-direct {v0, v1, v2, p0}, Lcom/android/settings/applications/AppStateWriteSettingsBridge;-><init>(Landroid/content/Context;Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v0, v0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    new-instance v0, Lcom/android/settings/applications/AppStateInstallAppsBridge;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    invoke-direct {v0, v1, v2, p0}, Lcom/android/settings/applications/AppStateInstallAppsBridge;-><init>(Landroid/content/Context;Lcom/android/settingslib/b/a;Lcom/android/settings/applications/AppStateBaseBridge$Callback;)V

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    goto :goto_0
.end method

.method private static ua(Landroid/content/pm/PackageItemInfo;Landroid/content/pm/PackageItemInfo;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    if-nez v0, :cond_3

    :cond_2
    return v1

    :cond_3
    iget-object v0, p0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v1, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private ud()V
    .locals 8

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications;->tM(Lcom/android/settings/applications/ManageApplications;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->isFastScrollEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yi:Landroid/icu/text/AlphabeticIndex$ImmutableIndex;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/LocaleList;->size()I

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Landroid/os/LocaleList;

    new-array v2, v3, [Ljava/util/Locale;

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    aput-object v4, v2, v1

    invoke-direct {v0, v2}, Landroid/os/LocaleList;-><init>([Ljava/util/Locale;)V

    :cond_0
    new-instance v4, Landroid/icu/text/AlphabeticIndex;

    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/icu/text/AlphabeticIndex;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v0}, Landroid/os/LocaleList;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_1

    new-array v6, v3, [Ljava/util/Locale;

    invoke-virtual {v0, v2}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v4, v6}, Landroid/icu/text/AlphabeticIndex;->addLabels([Ljava/util/Locale;)Landroid/icu/text/AlphabeticIndex;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-array v0, v3, [Ljava/util/Locale;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    aput-object v2, v0, v1

    invoke-virtual {v4, v0}, Landroid/icu/text/AlphabeticIndex;->addLabels([Ljava/util/Locale;)Landroid/icu/text/AlphabeticIndex;

    invoke-virtual {v4}, Landroid/icu/text/AlphabeticIndex;->buildImmutableIndex()Landroid/icu/text/AlphabeticIndex$ImmutableIndex;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yi:Landroid/icu/text/AlphabeticIndex$ImmutableIndex;

    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, -0x1

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v2, v4, [I

    iput-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yo:[I

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_4

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yi:Landroid/icu/text/AlphabeticIndex$ImmutableIndex;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string/jumbo v0, ""

    :cond_3
    invoke-virtual {v5, v0}, Landroid/icu/text/AlphabeticIndex$ImmutableIndex;->getBucketIndex(Ljava/lang/CharSequence;)I

    move-result v0

    if-eq v0, v1, :cond_6

    new-instance v1, Lcom/android/settings/applications/ManageApplications$SectionInfo;

    iget-object v5, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yi:Landroid/icu/text/AlphabeticIndex$ImmutableIndex;

    invoke-virtual {v5, v0}, Landroid/icu/text/AlphabeticIndex$ImmutableIndex;->getBucket(I)Landroid/icu/text/AlphabeticIndex$Bucket;

    move-result-object v5

    invoke-virtual {v5}, Landroid/icu/text/AlphabeticIndex$Bucket;->getLabel()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5, v2}, Lcom/android/settings/applications/ManageApplications$SectionInfo;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yo:[I

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    aput v5, v1, v2

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xU:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/settings/applications/ManageApplications$SectionInfo;

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yq:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    :goto_3
    return-void

    :cond_5
    sget-object v0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xU:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yq:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    iput-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yo:[I

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private uf(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iget-object v3, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-static {v2, v6}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ua(Landroid/content/pm/PackageItemInfo;Landroid/content/pm/PackageItemInfo;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v2, v3

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->trimToSize()V

    return-object v5
.end method

.method private uk()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications;->tN(Lcom/android/settings/applications/ManageApplications;)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications;->tL(Lcom/android/settings/applications/ManageApplications;)Landroid/view/View;

    move-result-object v3

    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yr:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfs()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v3, v0, v1}, Lcom/android/settings/aq;->brx(Landroid/view/View;Landroid/view/View;ZZ)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private ul(Lcom/android/settings/applications/AppViewHolder;)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v0, v0, Lcom/android/settings/applications/ManageApplications;->xH:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget-object v0, v0, Lcom/android/settings/applications/ManageApplications;->xF:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ys:I

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/applications/AppViewHolder;->sl(Ljava/lang/CharSequence;I)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/settings/notification/NotificationBackend$AppRow;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget-object v0, v0, Lcom/android/settings/applications/ManageApplications;->xF:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ys:I

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/applications/AppViewHolder;->sl(Ljava/lang/CharSequence;I)V

    goto :goto_0

    :cond_0
    iget-object v1, p1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    check-cast v0, Lcom/android/settings/notification/NotificationBackend$AppRow;

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/android/settings/applications/InstalledAppDetails;->xZ(Lcom/android/settings/notification/NotificationBackend$AppRow;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget-object v0, v0, Lcom/android/settings/applications/ManageApplications;->xF:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ys:I

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/applications/AppViewHolder;->sl(Ljava/lang/CharSequence;I)V

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    new-instance v2, Lcom/android/settings/applications/AppStateUsageBridge$UsageState;

    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->cBd:Ljava/lang/Object;

    check-cast v0, Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;

    invoke-direct {v2, v0}, Lcom/android/settings/applications/AppStateUsageBridge$UsageState;-><init>(Lcom/android/settings/applications/AppStateAppOpsBridge$PermissionState;)V

    invoke-virtual {v2}, Lcom/android/settings/applications/AppStateUsageBridge$UsageState;->xH()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f121210

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_3
    const v0, 0x7f12120f

    goto :goto_1

    :cond_4
    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    invoke-static {v1, v2}, Lcom/android/settings/fuelgauge/HighPowerDetail;->Im(Landroid/content/Context;Lcom/android/settingslib/b/h;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    invoke-static {v1, v2}, Lcom/android/settings/applications/DrawOverlayDetails;->vx(Landroid/content/Context;Lcom/android/settingslib/b/h;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    invoke-static {v1, v2}, Lcom/android/settings/applications/WriteSettingsDetails;->qu(Landroid/content/Context;Lcom/android/settingslib/b/h;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p1, Lcom/android/settings/applications/AppViewHolder;->uo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    invoke-static {v1, v2}, Lcom/android/settings/applications/ExternalSourcesDetails;->oH(Landroid/content/Context;Lcom/android/settingslib/b/h;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic uq(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xW:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic ur(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Lcom/android/settings/applications/AppStateBaseBridge;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    return-object v0
.end method

.method static synthetic us(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Lcom/android/settings/applications/FileViewHolderController;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    return-object v0
.end method

.method static synthetic ut(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yg:Z

    return v0
.end method

.method static synthetic uu(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yh:Z

    return v0
.end method

.method static synthetic uv(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic uw(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yg:Z

    return p1
.end method

.method static synthetic ux(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yh:Z

    return p1
.end method

.method static synthetic uy(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ud()V

    return-void
.end method

.method static synthetic uz(Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uk()V

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    invoke-interface {v1}, Lcom/android/settings/applications/FileViewHolderController;->xa()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ye:Landroid/widget/Filter;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iget-wide v0, v0, Lcom/android/settingslib/b/h;->cBe:J

    return-wide v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yq:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/android/settings/applications/ManageApplications$SectionInfo;->yv:I

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yo:[I

    aget v0, v0, p1

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yq:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications;->tK(Lcom/android/settings/applications/ManageApplications;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/settings/applications/AppViewHolder;->sm(Landroid/view/LayoutInflater;Landroid/view/View;)Lcom/android/settings/applications/AppViewHolder;

    move-result-object v1

    iget-object v2, v1, Lcom/android/settings/applications/AppViewHolder;->uk:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    invoke-interface {v0, v1}, Lcom/android/settings/applications/FileViewHolderController;->wZ(Lcom/android/settings/applications/AppViewHolder;)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xV:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xV:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    monitor-enter v0

    :try_start_0
    iput-object v0, v1, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v3, v0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v1, Lcom/android/settings/applications/AppViewHolder;->ul:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/android/settingslib/b/h;->cBj:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v3, v1, Lcom/android/settings/applications/AppViewHolder;->um:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v4}, Lcom/android/settingslib/b/h;->cfC(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0, v1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ul(Lcom/android/settings/applications/AppViewHolder;)V

    iget-object v1, v1, Lcom/android/settings/applications/AppViewHolder;->un:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p0, v1, v3}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->updateDisableView(Landroid/widget/TextView;Landroid/content/pm/ApplicationInfo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->isEnabled(I)Z

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public in()V
    .locals 2

    iget v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yk:I

    const v1, 0x7f0a041a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    :cond_0
    return-void
.end method

.method public io()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yg:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    return-void
.end method

.method public ip()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications;->tP(Lcom/android/settings/applications/ManageApplications;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    :cond_0
    return-void
.end method

.method public iq()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yh:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    return-void
.end method

.method public ir()V
    .locals 0

    return-void
.end method

.method public is()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    return-void
.end method

.method public isEnabled(I)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    invoke-interface {v0}, Lcom/android/settings/applications/FileViewHolderController;->xa()Z

    move-result v0

    if-eqz v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v0, v0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    return v2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    invoke-static {}, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->getInstance()Lcom/android/settings/fuelgauge/PowerWhitelistBackend;

    move-result-object v1

    iget-object v0, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/android/settings/fuelgauge/PowerWhitelistBackend;->Ki(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public it(Ljava/lang/String;)V
    .locals 4

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xV:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/AppViewHolder;

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    if-nez v3, :cond_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v3, v3, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v3, v3, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v1, v0, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ul(Lcom/android/settings/applications/AppViewHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    iget-object v0, v0, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    iget-object v0, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v1}, Lcom/android/settings/applications/ManageApplications;->tI(Lcom/android/settings/applications/ManageApplications;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yk:I

    const v1, 0x7f0a041a

    if-ne v0, v1, :cond_2

    invoke-virtual {p0, v2}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    return-void
.end method

.method public iu(Ljava/util/ArrayList;)V
    .locals 8

    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yf:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yf:I

    if-ne v1, v6, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uf(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    :cond_1
    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v1, v1, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    iget-object v3, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/android/settingslib/b/h;->info:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    const/16 v4, 0x3e7

    if-eq v3, v4, :cond_2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move-object p1, v2

    :cond_4
    iput-object p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xW:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xW:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xZ:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xW:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->tW(Ljava/lang/CharSequence;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ud()V

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->notifyDataSetChanged()V

    iget v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yj:I

    if-eq v0, v7, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->getCount()I

    move-result v0

    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yj:I

    if-le v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications;->tM(Lcom/android/settings/applications/ManageApplications;)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yj:I

    iget v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yl:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    iput v7, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yj:I

    :cond_5
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yr:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfs()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications;->tL(Lcom/android/settings/applications/ManageApplications;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v0}, Lcom/android/settings/applications/ManageApplications;->tN(Lcom/android/settings/applications/ManageApplications;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v1}, Lcom/android/settings/applications/ManageApplications;->tL(Lcom/android/settings/applications/ManageApplications;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1, v6, v6}, Lcom/android/settings/aq;->brx(Landroid/view/View;Landroid/view/View;ZZ)V

    :cond_6
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v0, v0, Lcom/android/settings/applications/ManageApplications;->xH:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_8

    return-void

    :cond_7
    iput-object v5, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    sget-object v0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xU:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    iput-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yq:[Lcom/android/settings/applications/ManageApplications$SectionInfo;

    iput-object v5, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yo:[I

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    invoke-virtual {v1}, Lcom/android/settingslib/b/a;->ceR()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ManageApplications;->tC(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->mState:Lcom/android/settingslib/b/a;

    invoke-virtual {v1}, Lcom/android/settingslib/b/a;->cfj()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/applications/ManageApplications;->tD(Z)V

    return-void
.end method

.method public iv(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-virtual {v0}, Lcom/android/settings/applications/ManageApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xV:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public pause()V
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yp:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yp:Z

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yr:Lcom/android/settingslib/b/b;

    invoke-virtual {v1}, Lcom/android/settingslib/b/b;->pause()V

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    invoke-virtual {v1}, Lcom/android/settings/applications/AppStateBaseBridge;->pause()V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v1}, Lcom/android/settings/applications/ManageApplications;->tM(Lcom/android/settings/applications/ManageApplications;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yj:I

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v1}, Lcom/android/settings/applications/ManageApplications;->tM(Lcom/android/settings/applications/ManageApplications;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    :goto_0
    iput v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yl:I

    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v1}, Lcom/android/settings/applications/ManageApplications;->tM(Lcom/android/settings/applications/ManageApplications;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method tW(Ljava/lang/CharSequence;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-object p2

    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settingslib/b/a;->ceW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    invoke-virtual {v0}, Lcom/android/settingslib/b/h;->cfF()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_3

    :cond_2
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    return-object v4
.end method

.method public tX(I)Lcom/android/settingslib/b/h;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/b/h;

    return-object v0
.end method

.method public tY()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ya:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public tZ()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xV:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/AppViewHolder;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/android/settings/applications/AppViewHolder;->uj:Lcom/android/settingslib/b/h;

    if-eqz v1, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    invoke-interface {v1, v0}, Lcom/android/settings/applications/FileViewHolderController;->wZ(Lcom/android/settings/applications/AppViewHolder;)V

    return-void
.end method

.method public ub(I)V
    .locals 1

    iget v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yk:I

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yk:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    return-void
.end method

.method public uc(Z)V
    .locals 4

    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yg:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ys:I

    :goto_0
    sget-object v0, Lcom/android/settings/applications/ManageApplications;->xt:[Lcom/android/settingslib/b/i;

    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yf:I

    aget-object v1, v0, v1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xY:Lcom/android/settingslib/b/i;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/android/settingslib/b/k;

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xY:Lcom/android/settingslib/b/i;

    invoke-direct {v0, v1, v2}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    :goto_1
    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    invoke-static {v1}, Lcom/android/settings/applications/ManageApplications;->tP(Lcom/android/settings/applications/ManageApplications;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/android/settings/applications/ManageApplications;->xv:Ljava/util/Set;

    iget-object v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ym:Lcom/android/settings/applications/ManageApplications;

    iget v2, v2, Lcom/android/settings/applications/ManageApplications;->xH:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lcom/android/settingslib/b/k;

    sget-object v2, Lcom/android/settingslib/b/a;->cAt:Lcom/android/settingslib/b/i;

    invoke-direct {v1, v0, v2}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    move-object v0, v1

    :cond_2
    :goto_2
    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yk:I

    packed-switch v1, :pswitch_data_0

    sget-object v1, Lcom/android/settingslib/b/a;->cAa:Ljava/util/Comparator;

    :goto_3
    new-instance v2, Lcom/android/settingslib/b/k;

    sget-object v3, Lcom/android/settingslib/b/a;->cAD:Lcom/android/settingslib/b/i;

    invoke-direct {v2, v0, v3}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xX:Landroid/os/Handler;

    new-instance v3, Lcom/android/settings/applications/-$Lambda$Zob_AJV3uQklSw32eSjd82DuTdU$2;

    invoke-direct {v3, p0, v2, v1}, Lcom/android/settings/applications/-$Lambda$Zob_AJV3uQklSw32eSjd82DuTdU$2;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_3
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ys:I

    goto :goto_0

    :cond_4
    new-instance v1, Lcom/android/settingslib/b/k;

    sget-object v2, Lcom/android/settingslib/b/a;->cAe:Lcom/android/settingslib/b/i;

    invoke-direct {v1, v0, v2}, Lcom/android/settingslib/b/k;-><init>(Lcom/android/settingslib/b/i;Lcom/android/settingslib/b/i;)V

    move-object v0, v1

    goto :goto_2

    :pswitch_0
    iget v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ys:I

    packed-switch v1, :pswitch_data_1

    sget-object v1, Lcom/android/settingslib/b/a;->cAu:Ljava/util/Comparator;

    goto :goto_3

    :pswitch_1
    sget-object v1, Lcom/android/settingslib/b/a;->czS:Ljava/util/Comparator;

    goto :goto_3

    :pswitch_2
    sget-object v1, Lcom/android/settingslib/b/a;->cAd:Ljava/util/Comparator;

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a041a
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public ue()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yr:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfy()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    invoke-virtual {v0}, Lcom/android/settings/applications/AppStateBaseBridge;->vX()V

    :cond_0
    return-void
.end method

.method public ug(I)V
    .locals 3

    sget-boolean v0, Lcom/android/settings/applications/ManageApplications;->xs:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ManageApplications"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Resume!  mResumed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yp:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yp:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yp:Z

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yr:Lcom/android/settingslib/b/b;

    invoke-virtual {v0}, Lcom/android/settingslib/b/b;->cfv()V

    iput p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yk:I

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yb:Lcom/android/settings/applications/AppStateBaseBridge;

    invoke-virtual {v0}, Lcom/android/settings/applications/AppStateBaseBridge;->vY()V

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->ub(I)V

    goto :goto_0
.end method

.method public uh(Lcom/android/settingslib/b/i;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xY:Lcom/android/settingslib/b/i;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    return-void
.end method

.method public ui(Lcom/android/settings/applications/FileViewHolderController;)V
    .locals 3

    iput-object p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->xX:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/applications/-$Lambda$Zob_AJV3uQklSw32eSjd82DuTdU;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p0}, Lcom/android/settings/applications/-$Lambda$Zob_AJV3uQklSw32eSjd82DuTdU;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public uj(I)V
    .locals 1

    iput p1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yf:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->uc(Z)V

    return-void
.end method

.method synthetic um()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yc:Lcom/android/settings/applications/FileViewHolderController;

    invoke-interface {v0}, Lcom/android/settings/applications/FileViewHolderController;->wY()V

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yd:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/applications/-$Lambda$Zob_AJV3uQklSw32eSjd82DuTdU;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p0}, Lcom/android/settings/applications/-$Lambda$Zob_AJV3uQklSw32eSjd82DuTdU;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method synthetic un()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->tZ()V

    return-void
.end method

.method synthetic uo(Lcom/android/settingslib/b/i;Ljava/util/Comparator;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yr:Lcom/android/settingslib/b/b;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/settingslib/b/b;->cfu(Lcom/android/settingslib/b/i;Ljava/util/Comparator;Z)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->yd:Landroid/os/Handler;

    new-instance v2, Lcom/android/settings/applications/-$Lambda$Zob_AJV3uQklSw32eSjd82DuTdU$1;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/applications/-$Lambda$Zob_AJV3uQklSw32eSjd82DuTdU$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method synthetic up(Ljava/util/ArrayList;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ManageApplications$ApplicationsAdapter;->iu(Ljava/util/ArrayList;)V

    return-void
.end method

.method updateDisableView(Landroid/widget/TextView;Landroid/content/pm/ApplicationInfo;)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p2, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f120ba8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p2, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v0, :cond_1

    iget v0, p2, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f1205d2

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
