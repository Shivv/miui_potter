.class Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ResolverSettings.java"


# instance fields
.field private zW:Landroid/util/SparseArray;

.field final synthetic zX:Lcom/android/settings/applications/ResolverSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/applications/ResolverSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/applications/ResolverSettings;Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;-><init>(Lcom/android/settings/applications/ResolverSettings;)V

    return-void
.end method


# virtual methods
.method public OnOrderChanged(II)V
    .locals 4

    if-eq p1, p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->getItem(I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/applications/ResolverSettings;->vJ(Lcom/android/settings/applications/ResolverSettings;Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-virtual {p0, p2}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->getItem(I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/settings/applications/ResolverSettings;->vJ(Lcom/android/settings/applications/ResolverSettings;Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-static {v2}, Lcom/android/settings/applications/ResolverSettings;->vE(Lcom/android/settings/applications/ResolverSettings;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-static {v2}, Lcom/android/settings/applications/ResolverSettings;->vE(Lcom/android/settings/applications/ResolverSettings;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-static {v3}, Lcom/android/settings/applications/ResolverSettings;->vH(Lcom/android/settings/applications/ResolverSettings;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-static {v0}, Lcom/android/settings/applications/ResolverSettings;->vL(Lcom/android/settings/applications/ResolverSettings;)V

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-static {v0}, Lcom/android/settings/applications/ResolverSettings;->vI(Lcom/android/settings/applications/ResolverSettings;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->vM(Landroid/util/SparseArray;)V

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zW:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/content/pm/ResolveInfo;
    .locals 2

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zW:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zW:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zW:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->getItem(I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->getItem(I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/pm/ResolveInfo;->hashCode()I

    move-result v0

    :goto_0
    int-to-long v0, v0

    return-wide v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-virtual {v0}, Lcom/android/settings/applications/ResolverSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d018e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/android/settings/applications/ResolverSettings$ViewHolder;

    invoke-direct {v0, p2}, Lcom/android/settings/applications/ResolverSettings$ViewHolder;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/android/settings/applications/ResolverSettings$ViewHolder;->zZ:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-static {v1}, Lcom/android/settings/applications/ResolverSettings;->vF(Lcom/android/settings/applications/ResolverSettings;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->getItem(I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/ResolverSettings$ViewHolder;

    iget-object v2, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-static {v2}, Lcom/android/settings/applications/ResolverSettings;->vK(Lcom/android/settings/applications/ResolverSettings;)Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, v0, Lcom/android/settings/applications/ResolverSettings$ViewHolder;->Aa:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v0, Lcom/android/settings/applications/ResolverSettings$ViewHolder;->zZ:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zX:Lcom/android/settings/applications/ResolverSettings;

    invoke-virtual {v4}, Lcom/android/settings/applications/ResolverSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-wide/32 v6, 0x1d4c0

    invoke-static {v4, v1, v2, v6, v7}, Lmiui/maml/util/AppIconsHelper;->getIconDrawable(Landroid/content/Context;Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;J)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, v0, Lcom/android/settings/applications/ResolverSettings$ViewHolder;->zY:Landroid/view/View;

    new-instance v1, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter$1;-><init>(Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1
    return-object p2
.end method

.method public vM(Landroid/util/SparseArray;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->zW:Landroid/util/SparseArray;

    invoke-virtual {p0}, Lcom/android/settings/applications/ResolverSettings$ResolverListAdapter;->notifyDataSetChanged()V

    return-void
.end method
