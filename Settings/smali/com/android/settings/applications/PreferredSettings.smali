.class public Lcom/android/settings/applications/PreferredSettings;
.super Lmiui/app/ListActivity;
.source "PreferredSettings.java"


# instance fields
.field private mIntent:Landroid/content/Intent;

.field private mPackageName:Ljava/lang/String;

.field private sv:Ljava/lang/String;

.field private sw:Landroid/content/IntentFilter;

.field private sx:Landroid/content/pm/ResolveInfo;

.field private sy:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/ListActivity;-><init>()V

    return-void
.end method

.method static re(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)Z
    .locals 2

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private rf(Landroid/content/pm/ResolveInfo;)V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getUserId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->setDefaultBrowserPackageNameAsUser(Ljava/lang/String;I)Z

    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;->ri(Landroid/content/pm/ResolveInfo;)V

    invoke-virtual {v0}, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method private rg(Landroid/content/pm/ResolveInfo;)V
    .locals 12

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->mIntent:Landroid/content/Intent;

    const/high16 v2, 0x20000

    invoke-virtual {v5, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    new-array v8, v7, [Landroid/content/ComponentName;

    move v3, v4

    move v2, v4

    :goto_0
    if-ge v3, v7, :cond_0

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    new-instance v9, Landroid/content/ComponentName;

    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v11, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v9, v8, v3

    iget v9, v1, Landroid/content/pm/ResolveInfo;->match:I

    if-le v9, v2, :cond_4

    iget v1, v1, Landroid/content/pm/ResolveInfo;->match:I

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->mPackageName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v5, v1}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    :cond_1
    :goto_2
    if-eqz p1, :cond_3

    new-instance v1, Landroid/content/IntentFilter;

    iget-object v3, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    invoke-direct {v1, v3}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    const-string/jumbo v3, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.category.BROWSABLE"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v6, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1, v2, v8, v3}, Landroid/content/pm/PackageManager;->addPreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    :goto_3
    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;->ri(Landroid/content/pm/ResolveInfo;)V

    invoke-virtual {v0}, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;->notifyDataSetChanged()V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    invoke-virtual {v1}, Landroid/content/IntentFilter;->countCategories()I

    move-result v1

    if-lez v1, :cond_1

    const-string/jumbo v1, "android.intent.category.HOME"

    iget-object v3, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->getCategory(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    new-instance v3, Landroid/content/ComponentName;

    const-string/jumbo v6, "com.no.such.packagename"

    const-string/jumbo v7, "com.no.such.packagename.no.such.class"

    invoke-direct {v3, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1, v2, v8, v3}, Landroid/content/pm/PackageManager;->replacePreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    const-string/jumbo v1, "com.no.such.packagename"

    invoke-virtual {v5, v1}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->mIntent:Landroid/content/Intent;

    invoke-virtual {v5, v1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object p1

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method static synthetic rh(Lcom/android/settings/applications/PreferredSettings;)Landroid/content/IntentFilter;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    const/4 v2, 0x1

    const/4 v9, 0x0

    invoke-super {p0, p1}, Lmiui/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    :goto_0
    iput v9, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lmiui/R$dimen;->preference_screen_padding_bottom:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "preferred_app_intent_filter"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    iput-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "preferred_app_package_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->mPackageName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "preferred_app_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "preferred_label"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->sy:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0300c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sv:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    if-nez v1, :cond_1

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sw:Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.intent.category.HOME"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->mIntent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->mIntent:Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.category.HOME"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    aget-object v0, v0, v9

    iput-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->sy:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiui/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->mIntent:Landroid/content/Intent;

    const/high16 v1, 0x20000

    invoke-virtual {v4, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_8

    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    const-string/jumbo v3, "PreferredSettings"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "ri index: 0 name: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " priority: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Landroid/content/pm/ResolveInfo;->priority:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " isDefault: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, v0, Landroid/content/pm/ResolveInfo;->isDefault:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, v0, Landroid/content/pm/ResolveInfo;->system:Z

    if-eqz v3, :cond_2

    iput-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->sx:Landroid/content/pm/ResolveInfo;

    :cond_2
    move v3, v2

    move v2, v1

    :goto_1
    if-ge v3, v2, :cond_8

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    const-string/jumbo v6, "PreferredSettings"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "ri index: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " name: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " priority: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v1, Landroid/content/pm/ResolveInfo;->priority:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " isDefault: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, v0, Landroid/content/pm/ResolveInfo;->isDefault:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, v0, Landroid/content/pm/ResolveInfo;->priority:I

    iget v7, v1, Landroid/content/pm/ResolveInfo;->priority:I

    if-ne v6, v7, :cond_3

    iget-boolean v6, v0, Landroid/content/pm/ResolveInfo;->isDefault:Z

    iget-boolean v7, v1, Landroid/content/pm/ResolveInfo;->isDefault:Z

    if-eq v6, v7, :cond_5

    :cond_3
    :goto_2
    if-ge v3, v2, :cond_5

    invoke-interface {v5, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_4
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v1, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_5
    iget-boolean v6, v1, Landroid/content/pm/ResolveInfo;->system:Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/settings/applications/PreferredSettings;->sx:Landroid/content/pm/ResolveInfo;

    if-nez v6, :cond_7

    iput-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sx:Landroid/content/pm/ResolveInfo;

    :cond_6
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_7
    iget-object v6, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v7, "android"

    invoke-virtual {v4, v6, v7}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_6

    iput-object v1, p0, Lcom/android/settings/applications/PreferredSettings;->sx:Landroid/content/pm/ResolveInfo;

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->mIntent:Landroid/content/Intent;

    invoke-virtual {v4, v0, v9}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;

    invoke-direct {v1, p0, p0, v5, v0}, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;-><init>(Lcom/android/settings/applications/PreferredSettings;Landroid/content/Context;Ljava/util/List;Landroid/content/pm/ResolveInfo;)V

    invoke-virtual {p0, v1}, Lcom/android/settings/applications/PreferredSettings;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    invoke-super/range {p0 .. p5}, Lmiui/app/ListActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    invoke-virtual {p0}, Lcom/android/settings/applications/PreferredSettings;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;

    invoke-virtual {v0, p3}, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;->getItem(I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/settings/applications/PreferredSettings$ResolveListAdapter;->getCurrent()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/applications/PreferredSettings;->re(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/PreferredSettings;->sy:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/applications/PreferredSettings;->sv:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/settings/applications/PreferredSettings;->rf(Landroid/content/pm/ResolveInfo;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v1}, Lcom/android/settings/applications/PreferredSettings;->rg(Landroid/content/pm/ResolveInfo;)V

    goto :goto_0
.end method
