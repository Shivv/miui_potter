.class final enum Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;
.super Ljava/lang/Enum;
.source "MiuiSecurityTrustedCredentials.java"


# static fields
.field private static final synthetic bVk:[Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

.field public static final enum bVl:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

.field public static final enum bVm:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

.field private static final synthetic bVn:[I


# instance fields
.field private final mCheckbox:Z

.field private final mLabel:I

.field private final mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v5, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    const-string/jumbo v1, "SYSTEM"

    const-string/jumbo v3, "system"

    const v4, 0x7f12129a

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;-><init>(Ljava/lang/String;ILjava/lang/String;IZ)V

    sput-object v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVl:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    new-instance v3, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    const-string/jumbo v4, "USER"

    const-string/jumbo v6, "user"

    const v7, 0x7f12129c

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;-><init>(Ljava/lang/String;ILjava/lang/String;IZ)V

    sput-object v3, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVm:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    sget-object v1, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVl:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVm:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVk:[Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->mTag:Ljava/lang/String;

    iput p4, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->mLabel:I

    iput-boolean p5, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->mCheckbox:Z

    return-void
.end method

.method private bOZ(Lcom/android/org/conscrypt/TrustedCertificateStore;Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPm()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-virtual {p1, p2}, Lcom/android/org/conscrypt/TrustedCertificateStore;->containsAlias(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    :pswitch_1
    const/4 v0, 0x0

    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private bPa(Lcom/android/org/conscrypt/TrustedCertificateStore;)Ljava/util/Set;
    .locals 2

    invoke-static {}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPm()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-virtual {p1}, Lcom/android/org/conscrypt/TrustedCertificateStore;->allSystemAliases()Ljava/util/Set;

    move-result-object v0

    return-object v0

    :pswitch_1
    invoke-virtual {p1}, Lcom/android/org/conscrypt/TrustedCertificateStore;->userAliases()Ljava/util/Set;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private bPb(Lcom/android/settings/bX;)I
    .locals 2

    invoke-static {}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPm()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-static {p1}, Lcom/android/settings/bX;->bPx(Lcom/android/settings/bX;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f121295

    return v0

    :cond_0
    const v0, 0x7f121293

    return v0

    :pswitch_1
    const v0, 0x7f121297

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private bPc(Lcom/android/settings/bX;)I
    .locals 2

    invoke-static {}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPm()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-static {p1}, Lcom/android/settings/bX;->bPx(Lcom/android/settings/bX;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f121296

    return v0

    :cond_0
    const v0, 0x7f121294

    return v0

    :pswitch_1
    const v0, 0x7f121298

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private bPd(ZLcom/android/settings/bX;)V
    .locals 1

    if-eqz p1, :cond_1

    invoke-static {p2}, Lcom/android/settings/bX;->bPB(Lcom/android/settings/bX;)Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    move-result-object v0

    iget-boolean v0, v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->mCheckbox:Z

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/android/settings/bX;->bPx(Lcom/android/settings/bX;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {p2, v0}, Lcom/android/settings/bX;->bPD(Lcom/android/settings/bX;Z)Z

    :goto_0
    invoke-static {p2}, Lcom/android/settings/bX;->bPv(Lcom/android/settings/bX;)Lcom/android/settings/bV;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/bV;->notifyDataSetChanged()V

    :goto_1
    return-void

    :cond_0
    invoke-static {p2}, Lcom/android/settings/bX;->bPv(Lcom/android/settings/bX;)Lcom/android/settings/bV;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/bV;->bPo(Lcom/android/settings/bV;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/android/settings/bX;->bPv(Lcom/android/settings/bX;)Lcom/android/settings/bV;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/bV;->bPr(Lcom/android/settings/bV;)V

    goto :goto_1
.end method

.method static synthetic bPe(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->mCheckbox:Z

    return v0
.end method

.method static synthetic bPf(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->mLabel:I

    return v0
.end method

.method static synthetic bPg(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bPh(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Lcom/android/org/conscrypt/TrustedCertificateStore;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bOZ(Lcom/android/org/conscrypt/TrustedCertificateStore;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic bPi(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Lcom/android/settings/bX;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPb(Lcom/android/settings/bX;)I

    move-result v0

    return v0
.end method

.method static synthetic bPj(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Lcom/android/settings/bX;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPc(Lcom/android/settings/bX;)I

    move-result v0

    return v0
.end method

.method static synthetic bPk(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;Lcom/android/org/conscrypt/TrustedCertificateStore;)Ljava/util/Set;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPa(Lcom/android/org/conscrypt/TrustedCertificateStore;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bPl(Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;ZLcom/android/settings/bX;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bPd(ZLcom/android/settings/bX;)V

    return-void
.end method

.method private static synthetic bPm()[I
    .locals 3

    sget-object v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVn:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVn:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->values()[Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVl:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVm:Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    sput-object v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVn:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;
    .locals 1

    const-class v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    return-object v0
.end method

.method public static values()[Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;
    .locals 1

    sget-object v0, Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;->bVk:[Lcom/android/settings/MiuiSecurityTrustedCredentials$Tab;

    return-object v0
.end method
