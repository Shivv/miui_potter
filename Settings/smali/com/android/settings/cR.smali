.class Lcom/android/settings/cR;
.super Landroid/os/HandlerThread;
.source "ApkIconLoader.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private cey:Landroid/os/Handler;

.field final synthetic cez:Lcom/android/settings/cN;


# direct methods
.method public constructor <init>(Lcom/android/settings/cN;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/cR;->cez:Lcom/android/settings/cN;

    const-string/jumbo v0, "FileIconLoader"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private bWw(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, p2, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-static {p1, v1, v0, v2, v3}, Lmiui/maml/util/AppIconsHelper;->getIconDrawable(Landroid/content/Context;Landroid/content/pm/PackageItemInfo;Landroid/content/pm/PackageManager;J)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public bWx()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/cR;->cey:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/android/settings/cR;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/settings/cR;->cey:Landroid/os/Handler;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/cR;->cey:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/android/settings/cR;->cez:Lcom/android/settings/cN;

    invoke-static {v0}, Lcom/android/settings/cN;->bWs(Lcom/android/settings/cN;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cQ;

    invoke-static {}, Lcom/android/settings/cN;->bWq()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    iget-object v3, v0, Lcom/android/settings/cQ;->cex:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/cO;

    if-eqz v1, :cond_0

    iget v3, v1, Lcom/android/settings/cO;->cev:I

    if-nez v3, :cond_0

    iput v5, v1, Lcom/android/settings/cO;->cev:I

    iget-object v3, p0, Lcom/android/settings/cR;->cez:Lcom/android/settings/cN;

    invoke-static {v3}, Lcom/android/settings/cN;->bWp(Lcom/android/settings/cN;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, Lcom/android/settings/cQ;->cex:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/android/settings/cR;->bWw(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/settings/cO;->bWu(Ljava/lang/Object;)V

    iput v6, v1, Lcom/android/settings/cO;->cev:I

    invoke-static {}, Lcom/android/settings/cN;->bWq()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    iget-object v0, v0, Lcom/android/settings/cQ;->cex:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/cR;->cez:Lcom/android/settings/cN;

    invoke-static {v0}, Lcom/android/settings/cN;->bWr(Lcom/android/settings/cN;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v5
.end method
