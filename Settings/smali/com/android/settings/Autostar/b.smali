.class Lcom/android/settings/Autostar/b;
.super Landroid/preference/Preference;
.source "Autoallow.java"


# instance fields
.field bjc:Lcom/android/settings/Autostar/Autoallow;

.field bjd:Landroid/widget/TextView;

.field bje:Landroid/widget/Button;

.field final synthetic bjf:Lcom/android/settings/Autostar/Autoallow;

.field info:Landroid/content/pm/ApplicationInfo;


# direct methods
.method public constructor <init>(Lcom/android/settings/Autostar/Autoallow;Landroid/content/Context;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Lcom/android/settings/Autostar/Autoallow;Landroid/content/pm/ApplicationInfo;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/Autostar/b;->bjf:Lcom/android/settings/Autostar/Autoallow;

    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0d0043

    invoke-virtual {p0, v0}, Lcom/android/settings/Autostar/b;->setLayoutResource(I)V

    invoke-virtual {p0, p3}, Lcom/android/settings/Autostar/b;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, p4}, Lcom/android/settings/Autostar/b;->setTitle(Ljava/lang/CharSequence;)V

    iput-object p5, p0, Lcom/android/settings/Autostar/b;->bjc:Lcom/android/settings/Autostar/Autoallow;

    iput-object p6, p0, Lcom/android/settings/Autostar/b;->info:Landroid/content/pm/ApplicationInfo;

    return-void
.end method


# virtual methods
.method public aXv()Landroid/content/pm/ApplicationInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/Autostar/b;->info:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a016e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/Autostar/b;->bje:Landroid/widget/Button;

    const v0, 0x7f0a016f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/Autostar/b;->bjd:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/Autostar/b;->bje:Landroid/widget/Button;

    const v1, 0x7f120165

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/Autostar/b;->bje:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/settings/Autostar/b;->bjf:Lcom/android/settings/Autostar/Autoallow;

    iget-object v1, v1, Lcom/android/settings/Autostar/Autoallow;->biV:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/Autostar/b;->bje:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/Autostar/b;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/miui/AppOpsUtils;->getApplicationAutoStart(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/Autostar/b;->bje:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/Autostar/b;->bjd:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/Autostar/b;->bjd:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/Autostar/b;->bje:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method
