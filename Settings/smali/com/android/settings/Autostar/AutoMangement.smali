.class public Lcom/android/settings/Autostar/AutoMangement;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "AutoMangement.java"


# instance fields
.field biH:Landroid/widget/TextView;

.field biI:Landroid/widget/FrameLayout;

.field biJ:Landroid/app/Activity;

.field private biK:Ljava/util/List;

.field biL:Landroid/view/View$OnClickListener;

.field biM:Landroid/view/View$OnClickListener;

.field biN:Landroid/app/Fragment;

.field biO:Landroid/content/pm/PackageManager;

.field biP:Landroid/preference/PreferenceGroup;

.field biQ:Ljava/util/ArrayList;

.field biR:Landroid/view/View;

.field biS:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/Autostar/c;

    invoke-direct {v0, p0}, Lcom/android/settings/Autostar/c;-><init>(Lcom/android/settings/Autostar/AutoMangement;)V

    iput-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biL:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/settings/Autostar/d;

    invoke-direct {v0, p0}, Lcom/android/settings/Autostar/d;-><init>(Lcom/android/settings/Autostar/AutoMangement;)V

    iput-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biM:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static aXo(Landroid/content/pm/ApplicationInfo;)Z
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget v0, p0, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v2
.end method

.method private aXp()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biO:Landroid/content/pm/PackageManager;

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    invoke-static {v0}, Lcom/android/settings/Autostar/AutoMangement;->aXo(Landroid/content/pm/ApplicationInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/miui/AppOpsUtils;->getApplicationAutoStart(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic aXq(Lcom/android/settings/Autostar/AutoMangement;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    return-object v0
.end method

.method static synthetic aXr(Lcom/android/settings/Autostar/AutoMangement;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/Autostar/AutoMangement;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const v0, 0x7f0d0042

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biR:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biR:Landroid/view/View;

    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biR:Landroid/view/View;

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biH:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biR:Landroid/view/View;

    const v2, 0x7f0a0429

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biS:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biR:Landroid/view/View;

    const v2, 0x7f0a010a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biI:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biM:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biR:Landroid/view/View;

    return-object v0
.end method

.method aXn()V
    .locals 8

    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biP:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biQ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/android/settings/Autostar/AutoMangement;->aXp()V

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biO:Landroid/content/pm/PackageManager;

    invoke-static {v0, v6, v1}, Lmiui/maml/util/AppIconsHelper;->getIconDrawable(Landroid/content/Context;Landroid/content/pm/PackageItemInfo;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biO:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    new-instance v0, Lcom/android/settings/Autostar/a;

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/Autostar/a;-><init>(Lcom/android/settings/Autostar/AutoMangement;Landroid/content/Context;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Lcom/android/settings/Autostar/AutoMangement;Landroid/content/pm/ApplicationInfo;)V

    iget-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biQ:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/Autostar/AutoMangement;->biP:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/Autostar/AutoMangement;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->aXn()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150024

    invoke-virtual {p0, v0}, Lcom/android/settings/Autostar/AutoMangement;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biO:Landroid/content/pm/PackageManager;

    iput-object p0, p0, Lcom/android/settings/Autostar/AutoMangement;->biN:Landroid/app/Fragment;

    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biJ:Landroid/app/Activity;

    const-string/jumbo v0, "autorun"

    invoke-virtual {p0, v0}, Lcom/android/settings/Autostar/AutoMangement;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biP:Landroid/preference/PreferenceGroup;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biQ:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->aXn()V

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biP:Landroid/preference/PreferenceGroup;

    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const v4, 0x7f100008

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onResume()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->aXn()V

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biS:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/Autostar/AutoMangement;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const v4, 0x7f100008

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biI:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biH:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biI:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/Autostar/AutoMangement;->biH:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
