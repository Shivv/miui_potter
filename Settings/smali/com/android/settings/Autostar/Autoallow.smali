.class public Lcom/android/settings/Autostar/Autoallow;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "Autoallow.java"


# instance fields
.field biV:Landroid/view/View$OnClickListener;

.field biW:Landroid/widget/TextView;

.field private biX:Ljava/util/List;

.field biY:Landroid/content/pm/PackageManager;

.field biZ:Landroid/preference/PreferenceGroup;

.field bja:Ljava/util/ArrayList;

.field bjb:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/Autostar/e;

    invoke-direct {v0, p0}, Lcom/android/settings/Autostar/e;-><init>(Lcom/android/settings/Autostar/Autoallow;)V

    iput-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biV:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static aXt(Landroid/content/pm/ApplicationInfo;)Z
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget v0, p0, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v2
.end method

.method private aXu()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biY:Landroid/content/pm/PackageManager;

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    invoke-static {v0}, Lcom/android/settings/Autostar/Autoallow;->aXt(Landroid/content/pm/ApplicationInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/Autostar/Autoallow;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/miui/AppOpsUtils;->getApplicationAutoStart(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/Autostar/Autoallow;->biX:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public Vg(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const/4 v1, 0x0

    const v0, 0x7f0d0041

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->bjb:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->bjb:Landroid/view/View;

    const v2, 0x1020004

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biW:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/Autostar/Autoallow;->biW:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->bjb:Landroid/view/View;

    return-object v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method aXs()V
    .locals 8

    invoke-virtual {p0}, Lcom/android/settings/Autostar/Autoallow;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biZ:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->bja:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/android/settings/Autostar/Autoallow;->aXu()V

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biX:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/Autostar/Autoallow;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/Autostar/Autoallow;->biY:Landroid/content/pm/PackageManager;

    invoke-static {v0, v6, v1}, Lmiui/maml/util/AppIconsHelper;->getIconDrawable(Landroid/content/Context;Landroid/content/pm/PackageItemInfo;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biY:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    new-instance v0, Lcom/android/settings/Autostar/b;

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/Autostar/b;-><init>(Lcom/android/settings/Autostar/Autoallow;Landroid/content/Context;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Lcom/android/settings/Autostar/Autoallow;Landroid/content/pm/ApplicationInfo;)V

    iget-object v1, p0, Lcom/android/settings/Autostar/Autoallow;->bja:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/settings/Autostar/Autoallow;->biZ:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/Autostar/Autoallow;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150024

    invoke-virtual {p0, v0}, Lcom/android/settings/Autostar/Autoallow;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/Autostar/Autoallow;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biY:Landroid/content/pm/PackageManager;

    const-string/jumbo v0, "autorun"

    invoke-virtual {p0, v0}, Lcom/android/settings/Autostar/Autoallow;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biZ:Landroid/preference/PreferenceGroup;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->biX:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/Autostar/Autoallow;->bja:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/settings/Autostar/Autoallow;->aXs()V

    return-void
.end method
