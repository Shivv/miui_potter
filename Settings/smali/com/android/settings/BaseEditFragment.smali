.class public Lcom/android/settings/BaseEditFragment;
.super Lcom/android/settings/BaseFragment;
.source "BaseEditFragment.java"


# instance fields
.field private bqF:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/BaseEditFragment;->bqF:Z

    return-void
.end method


# virtual methods
.method public PM(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/BaseEditFragment;->beb(Landroid/os/Bundle;)V

    return-void
.end method

.method public adN()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/BaseEditFragment;->PM(Z)V

    return-void
.end method

.method public bdX()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bdY()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/BaseEditFragment;->bqF:Z

    return v0
.end method

.method public bdZ()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->finish()V

    return-void
.end method

.method public bea(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v1, 0x102001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    iput-boolean p1, p0, Lcom/android/settings/BaseEditFragment;->bqF:Z

    return-void
.end method

.method protected beb(Landroid/os/Bundle;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/BaseEditFragment;->bqF:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSettingsPreferenceFragment;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getTargetRequestCode()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->QH(ILandroid/os/Bundle;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->finish()V

    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, ""

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->bdX()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1203c9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/df;

    invoke-direct {v1, p0}, Lcom/android/settings/df;-><init>(Lcom/android/settings/BaseEditFragment;)V

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiSettings;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettings;->bgl()V

    :cond_1
    return v3
.end method

.method public onStart()V
    .locals 4

    const/16 v0, 0x10

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onStart()V

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v1, v0, v0}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    sget v0, Lmiui/R$layout;->edit_mode_title:I

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(I)V

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    const v0, 0x1020016

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x1020019

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v3, 0x1040000

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    new-instance v3, Lcom/android/settings/dd;

    invoke-direct {v3, p0}, Lcom/android/settings/dd;-><init>(Lcom/android/settings/BaseEditFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x102001a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x104000a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    new-instance v2, Lcom/android/settings/de;

    invoke-direct {v2, p0}, Lcom/android/settings/de;-><init>(Lcom/android/settings/BaseEditFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/settings/BaseEditFragment;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/android/settings/dc;->bYm(Landroid/app/Fragment;)V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/dc;->bYn(Landroid/app/Fragment;)V

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onStop()V

    return-void
.end method
