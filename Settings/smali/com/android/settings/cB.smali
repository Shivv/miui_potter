.class public Lcom/android/settings/cB;
.super Landroid/widget/BaseAdapter;
.source "DiracHeadsetAdapter.java"


# instance fields
.field private final cbn:[Lcom/android/settings/cD;


# direct methods
.method public constructor <init>(Lcom/miui/a/a/a/h;)V
    .locals 7

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-virtual {p1}, Lcom/miui/a/a/a/h;->csu()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/android/settings/cD;

    iput-object v0, p0, Lcom/android/settings/cB;->cbn:[Lcom/android/settings/cD;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lcom/android/settings/cB;->cbn:[Lcom/android/settings/cD;

    new-instance v5, Lcom/android/settings/cD;

    invoke-direct {p0, v1}, Lcom/android/settings/cB;->bTv(I)I

    move-result v6

    invoke-direct {p0, v1}, Lcom/android/settings/cB;->bTw(I)I

    move-result v1

    invoke-direct {v5, v6, v1, v0}, Lcom/android/settings/cD;-><init>(III)V

    aput-object v5, v4, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private bTv(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const v0, 0x7f080331

    return v0

    :pswitch_2
    const v0, 0x7f08033e

    return v0

    :pswitch_3
    const v0, 0x7f080342

    return v0

    :pswitch_4
    const v0, 0x7f080332

    return v0

    :pswitch_5
    const v0, 0x7f080333

    return v0

    :pswitch_6
    const v0, 0x7f08032c

    return v0

    :pswitch_7
    const v0, 0x7f080343

    return v0

    :pswitch_8
    const v0, 0x7f080346

    return v0

    :pswitch_9
    const v0, 0x7f080347

    return v0

    :pswitch_a
    const v0, 0x7f08033c

    return v0

    :pswitch_b
    const v0, 0x7f080344

    return v0

    :pswitch_c
    const v0, 0x7f08033d

    return v0

    :pswitch_d
    const v0, 0x7f08032d

    return v0

    :pswitch_e
    const v0, 0x7f08033f

    return v0

    :pswitch_f
    const v0, 0x7f08032e

    return v0

    :pswitch_10
    const v0, 0x7f080345

    return v0

    :pswitch_11
    const v0, 0x7f080341

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private bTw(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const v0, 0x7f120ae9

    return v0

    :pswitch_2
    const v0, 0x7f120af0

    return v0

    :pswitch_3
    const v0, 0x7f120af4

    return v0

    :pswitch_4
    sget-boolean v0, Lmiui/os/Build;->IS_MI2A:Z

    if-eqz v0, :cond_0

    const v0, 0x7f120aea

    return v0

    :cond_0
    const v0, 0x7f120aeb

    return v0

    :pswitch_5
    const v0, 0x7f120aec

    return v0

    :pswitch_6
    const v0, 0x7f120af7

    return v0

    :pswitch_7
    const v0, 0x7f120af6

    return v0

    :pswitch_8
    const v0, 0x7f120af9

    return v0

    :pswitch_9
    const v0, 0x7f120afa

    return v0

    :pswitch_a
    const v0, 0x7f120aed

    return v0

    :pswitch_b
    const v0, 0x7f120af8

    return v0

    :pswitch_c
    const v0, 0x7f120aef

    return v0

    :pswitch_d
    const v0, 0x7f120ae8

    return v0

    :pswitch_e
    const v0, 0x7f120af1

    return v0

    :pswitch_f
    const v0, 0x7f120aee

    return v0

    :pswitch_10
    const v0, 0x7f120afb

    return v0

    :pswitch_11
    const v0, 0x7f120ae7

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method


# virtual methods
.method public bTt(I)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/cB;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/settings/cB;->bTu(I)I

    move-result v1

    if-ne p1, v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public bTu(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/cB;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cD;

    iget v0, v0, Lcom/android/settings/cD;->cbs:I

    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cB;->cbn:[Lcom/android/settings/cD;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/cB;->cbn:[Lcom/android/settings/cD;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v2, 0x0

    if-nez p2, :cond_0

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0105

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/android/settings/cC;

    invoke-direct {v0, p2}, Lcom/android/settings/cC;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/cB;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cD;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/cC;

    iget-object v2, v1, Lcom/android/settings/cC;->cbo:Landroid/widget/ImageView;

    iget v3, v0, Lcom/android/settings/cD;->cbq:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, v1, Lcom/android/settings/cC;->cbp:Landroid/widget/TextView;

    iget v0, v0, Lcom/android/settings/cD;->cbr:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutDirection(I)V

    return-object p2
.end method
