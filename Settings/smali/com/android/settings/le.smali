.class final Lcom/android/settings/le;
.super Ljava/lang/Object;
.source "UserCredentialsSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic crV:Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;

.field final synthetic crW:I

.field final synthetic crX:Lcom/android/settings/UserCredentialsSettings$Credential;


# direct methods
.method constructor <init>(Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;ILcom/android/settings/UserCredentialsSettings$Credential;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/le;->crV:Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;

    iput p2, p0, Lcom/android/settings/le;->crW:I

    iput-object p3, p0, Lcom/android/settings/le;->crX:Lcom/android/settings/UserCredentialsSettings$Credential;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/le;->crV:Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;

    invoke-virtual {v0}, Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "no_config_credentials"

    iget v2, p0, Lcom/android/settings/le;->crW:I

    invoke-static {v0, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/le;->crV:Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;

    invoke-virtual {v1}, Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/settingslib/w;->cqW(Landroid/content/Context;Lcom/android/settingslib/n;)V

    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/cV;

    iget-object v1, p0, Lcom/android/settings/le;->crV:Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;

    iget-object v2, p0, Lcom/android/settings/le;->crV:Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;

    invoke-virtual {v2}, Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/le;->crV:Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;

    invoke-virtual {v3}, Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/cV;-><init>(Lcom/android/settings/UserCredentialsSettings$CredentialDialogFragment;Landroid/content/Context;Landroid/app/Fragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/android/settings/UserCredentialsSettings$Credential;

    iget-object v2, p0, Lcom/android/settings/le;->crX:Lcom/android/settings/UserCredentialsSettings$Credential;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/android/settings/cV;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
