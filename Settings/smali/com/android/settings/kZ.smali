.class final Lcom/android/settings/kZ;
.super Ljava/lang/Object;
.source "MiuiSmartCoverSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic crQ:Lcom/android/settings/MiuiSmartCoverSettingsFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/kZ;->crQ:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/kZ;->crQ:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXp(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/kZ;->crQ:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    iget-object v1, p0, Lcom/android/settings/kZ;->crQ:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-static {v1}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXp(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-static {v0, v1, v2}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXr(Lcom/android/settings/MiuiSmartCoverSettingsFragment;ZI)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/kZ;->crQ:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-static {v0}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXp(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->setSmartCoverMode(Z)V

    return v3

    :cond_0
    iget-object v0, p0, Lcom/android/settings/kZ;->crQ:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-static {v0, v2, v3}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXr(Lcom/android/settings/MiuiSmartCoverSettingsFragment;ZI)V

    check-cast p1, Lcom/android/settings/cT;

    invoke-virtual {p1, v3}, Lcom/android/settings/cT;->setChecked(Z)V

    const-string/jumbo v0, "persist.sys.smallwin_type"

    iget-object v1, p0, Lcom/android/settings/kZ;->crQ:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-static {v1}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXq(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/kZ;->crQ:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-static {v2}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->bXo(Lcom/android/settings/MiuiSmartCoverSettingsFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
