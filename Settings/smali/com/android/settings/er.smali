.class final Lcom/android/settings/er;
.super Landroid/os/AsyncTask;
.source "LockPatternChecker.java"


# instance fields
.field private ciE:I

.field final synthetic ciF:Lcom/android/internal/widget/LockPatternUtils;

.field final synthetic ciG:Ljava/util/List;

.field final synthetic ciH:I

.field final synthetic ciI:Lcom/android/settings/M;


# direct methods
.method constructor <init>(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;ILcom/android/settings/M;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/er;->ciF:Lcom/android/internal/widget/LockPatternUtils;

    iput-object p2, p0, Lcom/android/settings/er;->ciG:Ljava/util/List;

    iput p3, p0, Lcom/android/settings/er;->ciH:I

    iput-object p4, p0, Lcom/android/settings/er;->ciI:Lcom/android/settings/M;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bZQ(Ljava/lang/Boolean;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/er;->ciI:Lcom/android/settings/M;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget v2, p0, Lcom/android/settings/er;->ciE:I

    invoke-interface {v0, v1, v2}, Lcom/android/settings/M;->blX(ZI)V

    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/er;->ciF:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/er;->ciG:Ljava/util/List;

    iget v2, p0, Lcom/android/settings/er;->ciH:I

    invoke-static {v0, v1, v2}, Lcom/android/settings/bn;->bFp(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    iput v3, p0, Lcom/android/settings/er;->ciE:I

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/er;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/settings/er;->bZQ(Ljava/lang/Boolean;)V

    return-void
.end method
