.class public Lcom/android/settings/view/GridPreference;
.super Landroid/preference/Preference;
.source "GridPreference.java"


# instance fields
.field private bmA:Lcom/android/settings/view/a;

.field private bmB:I

.field private bmC:Z

.field private bmx:Landroid/widget/ListAdapter;

.field private final bmy:Landroid/view/View$OnClickListener;

.field private bmz:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/view/GridPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/view/GridPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/view/GridPreference;->bmC:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/view/GridPreference;->bmB:I

    new-instance v0, Lcom/android/settings/view/c;

    invoke-direct {v0, p0}, Lcom/android/settings/view/c;-><init>(Lcom/android/settings/view/GridPreference;)V

    iput-object v0, p0, Lcom/android/settings/view/GridPreference;->bmy:Landroid/view/View$OnClickListener;

    const v0, 0x7f0d0106

    invoke-virtual {p0, v0}, Lcom/android/settings/view/GridPreference;->setLayoutResource(I)V

    return-void
.end method

.method private baa(Landroid/view/View;)Landroid/widget/GridLayout;
    .locals 1

    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridLayout;

    return-object v0
.end method

.method private bab(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x1

    const/high16 v2, -0x80000000

    sget-object v0, Landroid/widget/GridLayout;->FILL:Landroid/widget/GridLayout$Alignment;

    invoke-static {v2, v3, v0}, Landroid/widget/GridLayout;->spec(IILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v0

    sget-object v1, Landroid/widget/GridLayout;->FILL:Landroid/widget/GridLayout$Alignment;

    invoke-static {v2, v3, v1}, Landroid/widget/GridLayout;->spec(IILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v1

    new-instance v2, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private bac(Landroid/view/View;Z)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/android/settings/view/GridPreference;->baa(Landroid/view/View;)Landroid/widget/GridLayout;

    move-result-object v2

    if-eqz p2, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Landroid/widget/GridLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const v0, 0x3e99999a    # 0.3f

    goto :goto_0

    :cond_1
    return-void
.end method

.method private bad(Landroid/view/View;Z)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/settings/view/GridPreference;->baa(Landroid/view/View;)Landroid/widget/GridLayout;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/widget/GridLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private baf(Landroid/view/View;IZ)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/view/GridPreference;->baa(Landroid/view/View;)Landroid/widget/GridLayout;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/View;->setSelected(Z)V

    return-void
.end method

.method private bai(Landroid/view/View;)V
    .locals 10

    const/4 v9, 0x0

    const v8, 0x7f0d0106

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/view/GridPreference;->baa(Landroid/view/View;)Landroid/widget/GridLayout;

    move-result-object v2

    iget-object v1, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Landroid/widget/GridLayout;->getColumnCount()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {v2}, Landroid/widget/GridLayout;->removeAllViews()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    invoke-virtual {v2}, Landroid/widget/GridLayout;->getChildCount()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v5

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_3

    invoke-virtual {v2, v1}, Landroid/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget-object v7, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    invoke-interface {v7, v1, v6, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    if-ne v6, v7, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v8, v7}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "convert view must be reused!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_1
    if-ge v1, v3, :cond_4

    iget-object v5, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    invoke-interface {v5, v1, v9, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/settings/view/GridPreference;->bab(Landroid/view/View;)V

    invoke-virtual {v2, v5}, Landroid/widget/GridLayout;->addView(Landroid/view/View;)V

    iget-object v6, p0, Lcom/android/settings/view/GridPreference;->bmy:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v8, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    if-ge v1, v4, :cond_5

    sub-int/2addr v4, v1

    invoke-virtual {v2, v1, v4}, Landroid/widget/GridLayout;->removeViews(II)V

    :cond_5
    const/16 v1, 0x11

    if-eq v3, v1, :cond_6

    const/16 v1, 0x12

    if-ne v3, v1, :cond_8

    :cond_6
    const v0, 0x7f080338

    :cond_7
    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/GridLayout;->setBackgroundResource(I)V

    invoke-virtual {v2}, Landroid/widget/GridLayout;->requestLayout()V

    return-void

    :cond_8
    const/16 v1, 0xf

    if-eq v3, v1, :cond_9

    const/16 v1, 0x10

    if-ne v3, v1, :cond_a

    :cond_9
    const v0, 0x7f080337

    goto :goto_2

    :cond_a
    const/16 v1, 0xd

    if-eq v3, v1, :cond_b

    const/16 v1, 0xe

    if-ne v3, v1, :cond_c

    :cond_b
    const v0, 0x7f080336

    goto :goto_2

    :cond_c
    const/16 v1, 0xb

    if-eq v3, v1, :cond_d

    const/16 v1, 0xc

    if-ne v3, v1, :cond_e

    :cond_d
    const v0, 0x7f080335

    goto :goto_2

    :cond_e
    const/16 v1, 0x9

    if-eq v3, v1, :cond_f

    const/16 v1, 0xa

    if-ne v3, v1, :cond_10

    :cond_f
    const v0, 0x7f080334

    goto :goto_2

    :cond_10
    const/4 v1, 0x7

    if-eq v3, v1, :cond_11

    const/16 v1, 0x8

    if-ne v3, v1, :cond_12

    :cond_11
    const v0, 0x7f08033b

    goto :goto_2

    :cond_12
    const/4 v1, 0x5

    if-eq v3, v1, :cond_13

    const/4 v1, 0x6

    if-ne v3, v1, :cond_14

    :cond_13
    const v0, 0x7f08033a

    goto :goto_2

    :cond_14
    const/4 v1, 0x3

    if-eq v3, v1, :cond_15

    const/4 v1, 0x4

    if-ne v3, v1, :cond_7

    :cond_15
    const v0, 0x7f080339

    goto :goto_2
.end method

.method static synthetic baj(Lcom/android/settings/view/GridPreference;)Lcom/android/settings/view/a;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/view/GridPreference;->bmA:Lcom/android/settings/view/a;

    return-object v0
.end method


# virtual methods
.method public bae(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/view/GridPreference;->bmB:I

    invoke-virtual {p0}, Lcom/android/settings/view/GridPreference;->bah()V

    return-void
.end method

.method public bag(Lcom/android/settings/view/a;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/view/GridPreference;->bmA:Lcom/android/settings/view/a;

    return-void
.end method

.method public bah()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/view/GridPreference;->notifyChanged()V

    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07015e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p1, v0, v1, v0, v2}, Landroid/view/View;->setPaddingRelative(IIII)V

    invoke-direct {p0, p1}, Lcom/android/settings/view/GridPreference;->bai(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/android/settings/view/GridPreference;->bmC:Z

    invoke-super {p0, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/settings/view/GridPreference;->bmC:Z

    invoke-direct {p0, p1, v0}, Lcom/android/settings/view/GridPreference;->bac(Landroid/view/View;Z)V

    iget v0, p0, Lcom/android/settings/view/GridPreference;->bmB:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/view/GridPreference;->bmB:I

    iget-object v1, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-direct {p0, p1, v3}, Lcom/android/settings/view/GridPreference;->bad(Landroid/view/View;Z)V

    iget v0, p0, Lcom/android/settings/view/GridPreference;->bmB:I

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/view/GridPreference;->baf(Landroid/view/View;IZ)V

    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/view/GridPreference;->bmz:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/android/settings/view/GridPreference;->bmz:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    iput-object v2, p0, Lcom/android/settings/view/GridPreference;->bmz:Landroid/database/DataSetObserver;

    :cond_0
    iput-object p1, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    iget-object v0, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settings/view/b;

    invoke-direct {v0, p0}, Lcom/android/settings/view/b;-><init>(Lcom/android/settings/view/GridPreference;)V

    iput-object v0, p0, Lcom/android/settings/view/GridPreference;->bmz:Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/settings/view/GridPreference;->bmx:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/android/settings/view/GridPreference;->bmz:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/view/GridPreference;->bah()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/view/GridPreference;->bmC:Z

    invoke-virtual {p0}, Lcom/android/settings/view/GridPreference;->bah()V

    return-void
.end method
