.class public Lcom/android/settings/MiuiConfirmCommonPassword;
.super Landroid/app/Activity;
.source "MiuiConfirmCommonPassword.java"


# instance fields
.field private ccv:Landroid/app/ActivityManager;

.field private ccw:Ljava/lang/String;

.field private ccx:I

.field private ccy:Lcom/android/settings/bM;

.field private ccz:Landroid/app/AlertDialog;

.field private mUserId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    iput-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccy:Lcom/android/settings/bM;

    return-void
.end method

.method private bUh()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method private bUi(Ljava/util/List;)Ljava/util/List;
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "second_user_id"

    const/16 v3, -0x2710

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    invoke-static {p0, v1}, Landroid/security/FingerprintIdUtils;->getUserFingerprintIds(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    move-object p1, v0

    :cond_2
    return-object p1
.end method

.method private bUj()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccy:Lcom/android/settings/bM;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    const v1, 0x7f0a00f2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f120725

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUp()V

    iget v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccx:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccx:I

    iget v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccx:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUh()V

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUq()V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUr()V

    goto :goto_0
.end method

.method private bUk()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUp()V

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUh()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->finish()V

    return-void
.end method

.method private bUm()Z
    .locals 2

    const-string/jumbo v0, "security_core_add"

    iget-object v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private bUn(Ljava/util/List;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUm()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUi(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private bUp()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccy:Lcom/android/settings/bM;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccy:Lcom/android/settings/bM;

    invoke-virtual {v0}, Lcom/android/settings/bM;->bNf()V

    iput-object v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccy:Lcom/android/settings/bM;

    :cond_0
    return-void
.end method

.method private bUq()V
    .locals 5

    const/16 v4, -0x270f

    const/4 v2, 0x0

    new-instance v0, Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    iget v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    if-ne v1, v4, :cond_2

    iget v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    invoke-static {v0, v1}, Lcom/android/settings/bn;->bFM(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v0

    :goto_0
    if-eqz v0, :cond_5

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_3

    const-class v0, Lcom/android/settings/ConfirmLockPattern$InternalActivity;

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    invoke-static {v1, v3}, Lmiui/securityspace/CrossUserUtils;->isAirSpace(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v0, Lcom/android/settings/ConfirmSpacePatternActivity;

    :cond_0
    :goto_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUm()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string/jumbo v0, "businessId"

    const-string/jumbo v3, "security_core_add"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v0, v1

    :goto_2
    const-string/jumbo v3, "from_confirm_frp_credential"

    iget v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    if-ne v1, v4, :cond_4

    const/4 v1, 0x1

    :goto_3
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    if-ne v1, v4, :cond_1

    const/16 v2, 0x3e9

    :cond_1
    invoke-virtual {p0, v0, v2}, Lcom/android/settings/MiuiConfirmCommonPassword;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_4
    return-void

    :cond_2
    iget v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    invoke-static {v0, v1}, Lcom/android/settings/bn;->bFG(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v0

    goto :goto_0

    :cond_3
    const-class v0, Lcom/android/settings/ConfirmLockPassword$InternalActivity;

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    invoke-static {v1, v3}, Lmiui/securityspace/CrossUserUtils;->isAirSpace(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v0, Lcom/android/settings/ConfirmSpacePasswordActivity;

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    invoke-virtual {p0, v2}, Lcom/android/settings/MiuiConfirmCommonPassword;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->finish()V

    goto :goto_4

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method private bUr()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Lcom/android/settings/kE;

    invoke-direct {v0, p0}, Lcom/android/settings/kE;-><init>(Lcom/android/settings/MiuiConfirmCommonPassword;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    if-nez v2, :cond_0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0d0065

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f120723

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_0
    new-instance v0, Lcom/android/settings/bM;

    invoke-direct {v0, p0}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccy:Lcom/android/settings/bM;

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccy:Lcom/android/settings/bM;

    invoke-virtual {v0}, Lcom/android/settings/bM;->bNe()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUm()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUi(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :cond_1
    new-instance v1, Lcom/android/settings/kF;

    invoke-direct {v1, p0}, Lcom/android/settings/kF;-><init>(Lcom/android/settings/MiuiConfirmCommonPassword;)V

    iget-object v2, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccy:Lcom/android/settings/bM;

    invoke-virtual {v2, v1, v0}, Lcom/android/settings/bM;->bNg(Lcom/android/settings/bl;Ljava/util/List;)V

    return-void
.end method

.method static synthetic bUs(Lcom/android/settings/MiuiConfirmCommonPassword;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccx:I

    return v0
.end method

.method static synthetic bUt(Lcom/android/settings/MiuiConfirmCommonPassword;)Lcom/android/settings/bM;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccy:Lcom/android/settings/bM;

    return-object v0
.end method

.method static synthetic bUu(Lcom/android/settings/MiuiConfirmCommonPassword;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic bUv(Lcom/android/settings/MiuiConfirmCommonPassword;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccx:I

    return p1
.end method

.method static synthetic bUw(Lcom/android/settings/MiuiConfirmCommonPassword;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUm()Z

    move-result v0

    return v0
.end method

.method static synthetic bUx(Lcom/android/settings/MiuiConfirmCommonPassword;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUh()V

    return-void
.end method

.method static synthetic bUy(Lcom/android/settings/MiuiConfirmCommonPassword;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUp()V

    return-void
.end method

.method static synthetic bUz(Lcom/android/settings/MiuiConfirmCommonPassword;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUq()V

    return-void
.end method


# virtual methods
.method protected bUl(I)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "second_user_id"

    const/16 v3, -0x2710

    invoke-static {v1, v2, v3, v0}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    invoke-static {p0, v1}, Landroid/security/FingerprintIdUtils;->getUserFingerprintIds(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method protected bUo(I)V
    .locals 2

    iget v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUl(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUk()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUj()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUl(I)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUk()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUj()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    invoke-virtual {p0, p2}, Lcom/android/settings/MiuiConfirmCommonPassword;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/16 v4, -0x2710

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "activity"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccv:Landroid/app/ActivityManager;

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "businessId"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccw:Ljava/lang/String;

    const-string/jumbo v0, "android.app.action.CONFIRM_FRP_CREDENTIAL"

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, -0x270f

    iput v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUm()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "com.android.settings.userIdToConfirm"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    iget v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    if-eq v0, v4, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "com.android.settings.bgColor"

    const v4, 0x7f0600e0

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f030057

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f030056

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    move v0, v1

    :goto_0
    array-length v5, v3

    if-ge v0, v5, :cond_2

    aget-object v5, v3, v0

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    aget-object v0, v4, v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v1, v0, 0x1

    :cond_2
    :try_start_0
    new-instance v0, Lcom/android/settings/bM;

    invoke-direct {v0, p0}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/bM;->bNe()Ljava/util/List;

    move-result-object v0

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    :cond_3
    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUn(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccv:Landroid/app/ActivityManager;

    iget v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->mUserId:I

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->isUserRunning(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUr()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiuiConfirmCommonPassword"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_5
    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUq()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUp()V

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUh()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->bUp()V

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/MiuiConfirmCommonPassword;->ccz:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/android/settings/MiuiConfirmCommonPassword;->finish()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method
