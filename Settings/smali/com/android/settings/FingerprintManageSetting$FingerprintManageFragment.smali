.class public Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;
.super Lcom/android/settings/KeyguardSettingsPreferenceFragment;
.source "FingerprintManageSetting.java"


# instance fields
.field private bAl:Landroid/preference/PreferenceCategory;

.field private bAm:Ljava/util/List;

.field private bAn:Landroid/preference/PreferenceCategory;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    return-void
.end method

.method private bnQ(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v4, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, p1}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/android/settings/eO;

    invoke-direct {v0, p0}, Lcom/android/settings/eO;-><init>(Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;)V

    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAl:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bWB()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v0, "miui_keyguard"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v5, p1, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v4, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    if-ne v0, v1, :cond_3

    move v0, v3

    :goto_1
    invoke-virtual {v4, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    if-nez p3, :cond_0

    invoke-virtual {v4, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    invoke-virtual {v4, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    const-string/jumbo v0, "miui_keyguard"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v4}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnY(Landroid/preference/CheckBoxPreference;)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method private bnR()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    const v0, 0x7f120a3f

    invoke-direct {p0, v0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnU(I)V

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/settings/NewFingerprintInternalActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x6b

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->finish()V

    return-void
.end method

.method private bnS(Z)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAl:Landroid/preference/PreferenceCategory;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAl:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAl:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v2, v0, Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_1

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string/jumbo v2, "miui_keyguard"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, v0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnY(Landroid/preference/CheckBoxPreference;)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private bnT(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "extra_fingerprint_key"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "extra_fingerprint_title"

    invoke-virtual {v4, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-class v0, Lcom/android/settings/MiuiFingerprintDetailFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x449

    const v5, 0x7f12072c

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    return-void
.end method

.method private bnU(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnV(Ljava/lang/String;)V

    return-void
.end method

.method private bnV(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120854

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private bnW()V
    .locals 6

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/android/settings/bd;->bAb(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    new-instance v2, Lcom/android/settings/eN;

    invoke-direct {v2, p0, v1}, Lcom/android/settings/eN;-><init>(Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;Ljava/util/Map;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method private bnX()V
    .locals 7

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030057

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f030058

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f030056

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_2

    const-string/jumbo v4, "miui_keyguard"

    aget-object v5, v1, v0

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "fingerprint_apply_to_privacy_password"

    aget-object v5, v1, v0

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "com_miui_applicatinlock_use_fingerprint_state"

    aget-object v5, v1, v0

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    array-length v4, v2

    if-ge v0, v4, :cond_2

    array-length v4, v3

    if-ge v0, v4, :cond_2

    aget-object v4, v1, v0

    aget-object v5, v2, v0

    aget-object v6, v3, v0

    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-direct {p0, v4, v5, v6}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnQ(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private bnY(Landroid/preference/CheckBoxPreference;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v2, "device_policy"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/app/admin/DevicePolicyManager;->getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I

    move-result v0

    :goto_0
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const v0, 0x7f120722

    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private bnZ()V
    .locals 6

    new-instance v0, Lcom/android/settings/bM;

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/bM;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/bM;->bNe()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    invoke-direct {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnW()V

    new-instance v3, Lcom/android/settings/eM;

    invoke-direct {v3, p0}, Lcom/android/settings/eM;-><init>(Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;)V

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAn:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v4, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/settings/bd;->bzY(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v5, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    invoke-static {v2, v5}, Lcom/android/settings/bd;->bzZ(Landroid/content/Context;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5, v0, v2}, Lcom/android/settings/bd;->bAa(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    :goto_1
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAn:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const-string/jumbo v1, "add_fingerprint"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    const v1, 0x7f1200b0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v1, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAn:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    return-void

    :cond_1
    move-object v0, v2

    goto :goto_1
.end method

.method static synthetic boa(Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    return-object v0
.end method

.method static synthetic bob(Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnR()V

    return-void
.end method

.method static synthetic boc(Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnT(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x449
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    const v0, 0x7f15005a

    invoke-virtual {p0, v0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "fingerprint_list"

    invoke-virtual {p0, v0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAn:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "apply_fingerprint_to"

    invoke-virtual {p0, v0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAl:Landroid/preference/PreferenceCategory;

    invoke-direct {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnX()V

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.miui.action.MANAGE_FINGERPRINT_PAYMENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const v2, 0x7f120734

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(I)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAl:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnZ()V

    new-instance v0, Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {p0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/bn;->bFG(Lcom/android/internal/widget/LockPatternUtils;I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    invoke-direct {p0, v2}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnS(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bAm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/FingerprintManageSetting$FingerprintManageFragment;->bnS(Z)V

    goto :goto_0
.end method
