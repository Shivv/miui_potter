.class public Lcom/android/settings/deviceinfo/storage/c;
.super Lcom/android/settings/utils/k;
.source "VolumeSizesLoader.java"


# instance fields
.field private aFX:Landroid/app/usage/StorageStatsManager;

.field private aFY:Landroid/os/storage/VolumeInfo;

.field private aFZ:Lcom/android/settingslib/f/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/f/b;Landroid/app/usage/StorageStatsManager;Landroid/os/storage/VolumeInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/utils/k;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/deviceinfo/storage/c;->aFZ:Lcom/android/settingslib/f/b;

    iput-object p3, p0, Lcom/android/settings/deviceinfo/storage/c;->aFX:Landroid/app/usage/StorageStatsManager;

    iput-object p4, p0, Lcom/android/settings/deviceinfo/storage/c;->aFY:Landroid/os/storage/VolumeInfo;

    return-void
.end method

.method static getVolumeSize(Lcom/android/settingslib/f/b;Landroid/app/usage/StorageStatsManager;Landroid/os/storage/VolumeInfo;)Lcom/android/settingslib/f/a;
    .locals 5

    invoke-interface {p0, p1, p2}, Lcom/android/settingslib/f/b;->cok(Landroid/app/usage/StorageStatsManager;Landroid/os/storage/VolumeInfo;)J

    move-result-wide v0

    invoke-interface {p0, p1, p2}, Lcom/android/settingslib/f/b;->coj(Landroid/app/usage/StorageStatsManager;Landroid/os/storage/VolumeInfo;)J

    move-result-wide v2

    new-instance v4, Lcom/android/settingslib/f/a;

    invoke-direct {v4, v2, v3, v0, v1}, Lcom/android/settingslib/f/a;-><init>(JJ)V

    return-object v4
.end method


# virtual methods
.method protected atF(Lcom/android/settingslib/f/a;)V
    .locals 0

    return-void
.end method

.method public loadInBackground()Lcom/android/settingslib/f/a;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/c;->aFZ:Lcom/android/settingslib/f/b;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/c;->aFX:Landroid/app/usage/StorageStatsManager;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/storage/c;->aFY:Landroid/os/storage/VolumeInfo;

    invoke-static {v0, v1, v2}, Lcom/android/settings/deviceinfo/storage/c;->getVolumeSize(Lcom/android/settingslib/f/b;Landroid/app/usage/StorageStatsManager;Landroid/os/storage/VolumeInfo;)Lcom/android/settingslib/f/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/storage/c;->loadInBackground()Lcom/android/settingslib/f/a;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onDiscardResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/settingslib/f/a;

    invoke-virtual {p0, p1}, Lcom/android/settings/deviceinfo/storage/c;->atF(Lcom/android/settingslib/f/a;)V

    return-void
.end method
