.class public Lcom/android/settings/deviceinfo/storage/j;
.super Lcom/android/settings/utils/k;
.source "UserIconLoader.java"


# instance fields
.field private aGE:Lcom/android/settings/deviceinfo/storage/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/deviceinfo/storage/k;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/utils/k;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/storage/k;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/j;->aGE:Lcom/android/settings/deviceinfo/storage/k;

    return-void
.end method

.method public static aua(Landroid/content/Context;)Landroid/util/SparseArray;
    .locals 5

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    const-class v0, Landroid/os/UserManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/UserInfo;

    iget v4, v1, Landroid/content/pm/UserInfo;->id:I

    invoke-static {p0, v0, v1}, Lcom/android/settings/aq;->cqx(Landroid/content/Context;Landroid/os/UserManager;Landroid/content/pm/UserInfo;)Lcom/android/settingslib/e/a;

    move-result-object v1

    invoke-virtual {v2, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method protected aub(Landroid/util/SparseArray;)V
    .locals 0

    return-void
.end method

.method public loadInBackground()Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/j;->aGE:Lcom/android/settings/deviceinfo/storage/k;

    invoke-interface {v0}, Lcom/android/settings/deviceinfo/storage/k;->auc()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/storage/j;->loadInBackground()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onDiscardResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/util/SparseArray;

    invoke-virtual {p0, p1}, Lcom/android/settings/deviceinfo/storage/j;->aub(Landroid/util/SparseArray;)V

    return-void
.end method
