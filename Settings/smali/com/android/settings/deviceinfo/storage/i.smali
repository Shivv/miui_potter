.class public Lcom/android/settings/deviceinfo/storage/i;
.super Lcom/android/settings/core/e;
.source "StorageSummaryDonutPreferenceController.java"


# instance fields
.field private aGB:Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;

.field private aGC:J

.field private aGD:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public atY(JJ)V
    .locals 1

    iput-wide p1, p0, Lcom/android/settings/deviceinfo/storage/i;->aGD:J

    iput-wide p3, p0, Lcom/android/settings/deviceinfo/storage/i;->aGC:J

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/storage/i;->atZ()V

    return-void
.end method

.method public atZ()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/i;->aGB:Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/i;->aGB:Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/storage/i;->cz(Landroid/preference/Preference;)V

    :cond_0
    return-void
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->cz(Landroid/preference/Preference;)V

    check-cast p1, Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/i;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/settings/deviceinfo/storage/i;->aGD:J

    invoke-static {v0, v2, v3, v6}, Landroid/text/format/Formatter;->formatBytes(Landroid/content/res/Resources;JI)Landroid/text/format/Formatter$BytesResult;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/storage/i;->mContext:Landroid/content/Context;

    const v2, 0x7f1211a6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    iget-object v3, v0, Landroid/text/format/Formatter$BytesResult;->value:Ljava/lang/String;

    aput-object v3, v2, v6

    iget-object v0, v0, Landroid/text/format/Formatter$BytesResult;->units:Ljava/lang/String;

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/i;->mContext:Landroid/content/Context;

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/settings/deviceinfo/storage/i;->mContext:Landroid/content/Context;

    iget-wide v4, p0, Lcom/android/settings/deviceinfo/storage/i;->aGC:J

    invoke-static {v2, v4, v5}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const v2, 0x7f1211b3

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-wide v0, p0, Lcom/android/settings/deviceinfo/storage/i;->aGD:J

    iget-wide v2, p0, Lcom/android/settings/deviceinfo/storage/i;->aGC:J

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;->atx(JJ)V

    invoke-virtual {p1, v7}, Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;->setEnabled(Z)V

    return-void
.end method

.method public i(Landroid/preference/PreferenceScreen;)V
    .locals 2

    const-string/jumbo v0, "pref_summary"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;

    iput-object v0, p0, Lcom/android/settings/deviceinfo/storage/i;->aGB:Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/storage/i;->aGB:Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/storage/StorageSummaryDonutPreference;->setEnabled(Z)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "pref_summary"

    return-object v0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
