.class public Lcom/android/settings/deviceinfo/RegulatoryInfoPreferenceController;
.super Lcom/android/settings/core/e;
.source "RegulatoryInfoPreferenceController.java"


# static fields
.field private static final aKG:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.SHOW_REGULATORY_INFO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/settings/deviceinfo/RegulatoryInfoPreferenceController;->aKG:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "regulatory_info"

    return-object v0
.end method

.method public p()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/deviceinfo/RegulatoryInfoPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    sget-object v1, Lcom/android/settings/deviceinfo/RegulatoryInfoPreferenceController;->aKG:Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
