.class public Lcom/android/settings/deviceinfo/BasebandVersionPreferenceController;
.super Lcom/android/settings/core/e;
.source "BasebandVersionPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->cz(Landroid/preference/Preference;)V

    const-string/jumbo v0, "gsm.version.baseband"

    iget-object v1, p0, Lcom/android/settings/deviceinfo/BasebandVersionPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f120594

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "baseband_version"

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/BasebandVersionPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/aq;->bqv(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
