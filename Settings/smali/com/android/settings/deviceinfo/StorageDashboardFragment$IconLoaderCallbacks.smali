.class public final Lcom/android/settings/deviceinfo/StorageDashboardFragment$IconLoaderCallbacks;
.super Ljava/lang/Object;
.source "StorageDashboardFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# instance fields
.field final synthetic aIb:Lcom/android/settings/deviceinfo/StorageDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/deviceinfo/StorageDashboardFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment$IconLoaderCallbacks;->aIb:Lcom/android/settings/deviceinfo/StorageDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic avv(Lcom/android/settings/core/e;)Z
    .locals 1

    instance-of v0, p0, Lcom/android/settings/deviceinfo/storage/l;

    return v0
.end method

.method static synthetic avw(Landroid/util/SparseArray;Lcom/android/settings/core/e;)V
    .locals 0

    check-cast p1, Lcom/android/settings/deviceinfo/storage/l;

    invoke-interface {p1, p0}, Lcom/android/settings/deviceinfo/storage/l;->atB(Landroid/util/SparseArray;)V

    return-void
.end method


# virtual methods
.method public avt(Landroid/content/Loader;Landroid/util/SparseArray;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment$IconLoaderCallbacks;->aIb:Lcom/android/settings/deviceinfo/StorageDashboardFragment;

    invoke-static {v0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->avp(Lcom/android/settings/deviceinfo/StorageDashboardFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    sget-object v1, Lcom/android/settings/deviceinfo/-$Lambda$ZtmfcihAQli1ryqqdILDUNaW4Sc;->$INST$0:Lcom/android/settings/deviceinfo/-$Lambda$ZtmfcihAQli1ryqqdILDUNaW4Sc;

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v1, Lcom/android/settings/deviceinfo/-$Lambda$ZtmfcihAQli1ryqqdILDUNaW4Sc$2;

    invoke-direct {v1, p2}, Lcom/android/settings/deviceinfo/-$Lambda$ZtmfcihAQli1ryqqdILDUNaW4Sc$2;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->forEach(Ljava/util/function/Consumer;)V

    return-void
.end method

.method synthetic avu()Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment$IconLoaderCallbacks;->aIb:Lcom/android/settings/deviceinfo/StorageDashboardFragment;

    invoke-virtual {v0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/deviceinfo/storage/j;->aua(Landroid/content/Context;)Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 3

    new-instance v0, Lcom/android/settings/deviceinfo/storage/j;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment$IconLoaderCallbacks;->aIb:Lcom/android/settings/deviceinfo/StorageDashboardFragment;

    invoke-virtual {v1}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/android/settings/deviceinfo/-$Lambda$ZtmfcihAQli1ryqqdILDUNaW4Sc$1;

    invoke-direct {v2, p0}, Lcom/android/settings/deviceinfo/-$Lambda$ZtmfcihAQli1ryqqdILDUNaW4Sc$1;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1, v2}, Lcom/android/settings/deviceinfo/storage/j;-><init>(Landroid/content/Context;Lcom/android/settings/deviceinfo/storage/k;)V

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/util/SparseArray;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/deviceinfo/StorageDashboardFragment$IconLoaderCallbacks;->avt(Landroid/content/Loader;Landroid/util/SparseArray;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    return-void
.end method
