.class public Lcom/android/settings/deviceinfo/UsbModeChooserActivity;
.super Landroid/app/Activity;
.source "UsbModeChooserActivity.java"


# static fields
.field public static final aHk:[I

.field public static final aHl:[I


# instance fields
.field private aHm:Lcom/android/settings/deviceinfo/UsbBackend;

.field private aHn:I

.field private aHo:Ljava/util/ArrayList;

.field private aHp:Lmiui/app/AlertDialog;

.field private aHq:Landroid/content/BroadcastReceiver;

.field private aHr:Lcom/android/settingslib/n;

.field private aHs:Landroid/view/LayoutInflater;

.field private aHt:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x6

    filled-new-array {v1, v2, v3, v0}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHk:[I

    filled-new-array {v1, v2, v3}, [I

    move-result-object v0

    sput-object v0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHl:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHo:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHt:Ljava/util/HashMap;

    new-instance v0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$1;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$1;-><init>(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHq:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private auA(IZLandroid/widget/LinearLayout;Z)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHs:Landroid/view/LayoutInflater;

    const v1, 0x7f0d0194

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x1020016

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->getTitle(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v1, 0x1020010

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1, p1}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->updateSummary(Landroid/widget/TextView;I)V

    if-eqz p4, :cond_0

    iget-object v3, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHr:Lcom/android/settingslib/n;

    if-eqz v3, :cond_1

    invoke-direct {p0, v2, v0, v1}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auD(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)V

    :cond_0
    new-instance v0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$2;

    invoke-direct {v0, p0, p4, p1}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$2;-><init>(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;ZI)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v2

    check-cast v0, Landroid/widget/Checkable;

    invoke-interface {v0, p2}, Landroid/widget/Checkable;->setChecked(Z)V

    return-void

    :cond_1
    return-void
.end method

.method private auB()V
    .locals 3

    new-instance v0, Lmiui/app/AlertDialog$Builder;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f12136e

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auz()Landroid/widget/ListAdapter;

    move-result-object v1

    new-instance v2, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$3;

    invoke-direct {v2, p0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$3;-><init>(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)V

    invoke-virtual {v0, v1, v2}, Lmiui/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$4;

    invoke-direct {v1, p0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$4;-><init>(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)V

    invoke-virtual {v0, v1}, Lmiui/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v1, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$5;

    invoke-direct {v1, p0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$5;-><init>(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)V

    const v2, 0x7f1203c7

    invoke-virtual {v0, v2, v1}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHp:Lmiui/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHp:Lmiui/app/AlertDialog;

    invoke-virtual {v0}, Lmiui/app/AlertDialog;->show()V

    return-void
.end method

.method private auC([I)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHm:Lcom/android/settings/deviceinfo/UsbBackend;

    aget v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/android/settings/deviceinfo/UsbBackend;->ava(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHm:Lcom/android/settings/deviceinfo/UsbBackend;

    aget v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/android/settings/deviceinfo/UsbBackend;->avb(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHo:Ljava/util/ArrayList;

    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private auD(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHr:Lcom/android/settingslib/n;

    if-eqz v0, :cond_0

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    const v0, 0x7f0a038d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2}, Landroid/widget/TextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const v1, 0x7f060064

    invoke-virtual {p0, v1}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method static synthetic auE(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)Lcom/android/settings/deviceinfo/UsbBackend;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHm:Lcom/android/settings/deviceinfo/UsbBackend;

    return-object v0
.end method

.method static synthetic auF(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHn:I

    return v0
.end method

.method static synthetic auG(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)Lmiui/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHp:Lmiui/app/AlertDialog;

    return-object v0
.end method

.method static synthetic auH(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)Lcom/android/settingslib/n;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHr:Lcom/android/settingslib/n;

    return-object v0
.end method

.method static synthetic auI(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHs:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic auJ(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHt:Ljava/util/HashMap;

    return-object v0
.end method

.method static getTitle(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const v0, 0x7f121371

    return v0

    :pswitch_2
    const v0, 0x7f121377

    return v0

    :pswitch_3
    const v0, 0x7f121373

    return v0

    :pswitch_4
    const v0, 0x7f121375

    return v0

    :pswitch_5
    const v0, 0x7f12136f

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method static updateSummary(Landroid/widget/TextView;I)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const v0, 0x7f121378

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected auz()Landroid/widget/ListAdapter;
    .locals 3

    new-instance v0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHo:Ljava/util/ArrayList;

    const v2, 0x7f0d0084

    invoke-direct {v0, p0, p0, v2, v1}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity$CustomListViewAdapter;-><init>(Lcom/android/settings/deviceinfo/UsbModeChooserActivity;Landroid/content/Context;ILjava/util/List;)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHs:Landroid/view/LayoutInflater;

    const-string/jumbo v0, "no_usb_file_transfer"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {p0, v0, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHr:Lcom/android/settingslib/n;

    new-instance v0, Lcom/android/settings/deviceinfo/UsbBackend;

    invoke-direct {v0, p0}, Lcom/android/settings/deviceinfo/UsbBackend;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHm:Lcom/android/settings/deviceinfo/UsbBackend;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHm:Lcom/android/settings/deviceinfo/UsbBackend;

    invoke-virtual {v0}, Lcom/android/settings/deviceinfo/UsbBackend;->auZ()I

    move-result v3

    move v0, v1

    :goto_0
    sget-object v2, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHk:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHm:Lcom/android/settings/deviceinfo/UsbBackend;

    sget-object v4, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHk:[I

    aget v4, v4, v0

    invoke-virtual {v2, v4}, Lcom/android/settings/deviceinfo/UsbBackend;->ava(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHm:Lcom/android/settings/deviceinfo/UsbBackend;

    sget-object v4, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHk:[I

    aget v4, v4, v0

    invoke-virtual {v2, v4}, Lcom/android/settings/deviceinfo/UsbBackend;->avb(I)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHk:[I

    aget v4, v2, v0

    sget-object v2, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHk:[I

    aget v2, v2, v0

    if-ne v3, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iget-object v5, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHm:Lcom/android/settings/deviceinfo/UsbBackend;

    sget-object v6, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHk:[I

    aget v6, v6, v0

    invoke-virtual {v5, v6}, Lcom/android/settings/deviceinfo/UsbBackend;->auY(I)Z

    move-result v5

    invoke-direct {p0, v4, v2, v7, v5}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auA(IZLandroid/widget/LinearLayout;Z)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHm:Lcom/android/settings/deviceinfo/UsbBackend;

    invoke-virtual {v0}, Lcom/android/settings/deviceinfo/UsbBackend;->auZ()I

    move-result v0

    iput v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHn:I

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHr:Lcom/android/settingslib/n;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHk:[I

    :goto_2
    invoke-direct {p0, v0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auC([I)V

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->auB()V

    return-void

    :cond_3
    sget-object v0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHl:[I

    goto :goto_2
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.hardware.usb.action.USB_STATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHq:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->aHq:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/UsbModeChooserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
