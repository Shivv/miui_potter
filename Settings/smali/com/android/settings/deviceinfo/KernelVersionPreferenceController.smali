.class public Lcom/android/settings/deviceinfo/KernelVersionPreferenceController;
.super Lcom/android/settings/core/e;
.source "KernelVersionPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/e;->cz(Landroid/preference/Preference;)V

    invoke-static {}, Lcom/android/settingslib/I;->cst()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "kernel_version"

    return-object v0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
