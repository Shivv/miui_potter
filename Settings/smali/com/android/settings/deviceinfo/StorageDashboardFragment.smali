.class public Lcom/android/settings/deviceinfo/StorageDashboardFragment;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "StorageDashboardFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private aHU:Landroid/util/SparseArray;

.field private aHV:Lcom/android/settings/deviceinfo/PrivateVolumeOptionMenuController;

.field private aHW:Lcom/android/settings/deviceinfo/storage/e;

.field private aHX:Ljava/util/List;

.field private aHY:Lcom/android/settingslib/f/a;

.field private aHZ:Lcom/android/settings/deviceinfo/storage/i;

.field private aIa:Landroid/os/storage/VolumeInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/deviceinfo/StorageDashboardFragment$1;

    invoke-direct {v0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment$1;-><init>()V

    sput-object v0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method

.method private avn()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHY:Lcom/android/settingslib/f/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHU:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHY:Lcom/android/settingslib/f/a;

    iget-wide v0, v0, Lcom/android/settingslib/f/a;->cIH:J

    iget-object v2, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHY:Lcom/android/settingslib/f/a;

    iget-wide v2, v2, Lcom/android/settingslib/f/a;->cII:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHZ:Lcom/android/settings/deviceinfo/storage/i;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHY:Lcom/android/settingslib/f/a;

    iget-wide v4, v3, Lcom/android/settingslib/f/a;->cIH:J

    invoke-virtual {v2, v0, v1, v4, v5}, Lcom/android/settings/deviceinfo/storage/i;->atY(JJ)V

    iget-object v2, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHW:Lcom/android/settings/deviceinfo/storage/e;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aIa:Landroid/os/storage/VolumeInfo;

    invoke-virtual {v2, v3}, Lcom/android/settings/deviceinfo/storage/e;->setVolume(Landroid/os/storage/VolumeInfo;)V

    iget-object v2, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHW:Lcom/android/settings/deviceinfo/storage/e;

    invoke-virtual {v2, v0, v1}, Lcom/android/settings/deviceinfo/storage/e;->atJ(J)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHW:Lcom/android/settings/deviceinfo/storage/e;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHY:Lcom/android/settingslib/f/a;

    iget-wide v2, v1, Lcom/android/settingslib/f/a;->cIH:J

    invoke-virtual {v0, v2, v3}, Lcom/android/settings/deviceinfo/storage/e;->atK(J)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHX:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHX:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/e;

    instance-of v3, v0, Lcom/android/settings/deviceinfo/storage/a;

    if-eqz v3, :cond_2

    check-cast v0, Lcom/android/settings/deviceinfo/storage/a;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHY:Lcom/android/settingslib/f/a;

    iget-wide v4, v3, Lcom/android/settingslib/f/a;->cIH:J

    invoke-virtual {v0, v4, v5}, Lcom/android/settings/deviceinfo/storage/a;->atE(J)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHW:Lcom/android/settings/deviceinfo/storage/e;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHU:Landroid/util/SparseArray;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/deviceinfo/storage/e;->atI(Landroid/util/SparseArray;I)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHX:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHU:Landroid/util/SparseArray;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->avo(Ljava/util/List;Landroid/util/SparseArray;)V

    return-void
.end method

.method private avo(Ljava/util/List;Landroid/util/SparseArray;)V
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/e;

    instance-of v3, v0, Lcom/android/settings/deviceinfo/storage/h;

    if-eqz v3, :cond_0

    check-cast v0, Lcom/android/settings/deviceinfo/storage/h;

    invoke-interface {v0, p2}, Lcom/android/settings/deviceinfo/storage/h;->atA(Landroid/util/SparseArray;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic avp(Lcom/android/settings/deviceinfo/StorageDashboardFragment;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHX:Ljava/util/List;

    return-object v0
.end method

.method static synthetic avq(Lcom/android/settings/deviceinfo/StorageDashboardFragment;)Landroid/os/storage/VolumeInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aIa:Landroid/os/storage/VolumeInfo;

    return-object v0
.end method

.method static synthetic avr(Lcom/android/settings/deviceinfo/StorageDashboardFragment;Lcom/android/settingslib/f/a;)Lcom/android/settingslib/f/a;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHY:Lcom/android/settingslib/f/a;

    return-object p1
.end method

.method static synthetic avs(Lcom/android/settings/deviceinfo/StorageDashboardFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->avn()V

    return-void
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "StorageDashboardFrag"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f1500ed

    return v0
.end method

.method public avm(Landroid/content/Loader;Landroid/util/SparseArray;)V
    .locals 0

    iput-object p2, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHU:Landroid/util/SparseArray;

    invoke-direct {p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->avn()V

    return-void
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x2e9

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/android/settings/deviceinfo/storage/i;

    invoke-direct {v0, p1}, Lcom/android/settings/deviceinfo/storage/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHZ:Lcom/android/settings/deviceinfo/storage/i;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHZ:Lcom/android/settings/deviceinfo/storage/i;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class v0, Landroid/os/storage/StorageManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    new-instance v2, Lcom/android/settings/deviceinfo/storage/e;

    iget-object v3, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aIa:Landroid/os/storage/VolumeInfo;

    new-instance v4, Lcom/android/settingslib/f/g;

    invoke-direct {v4, v0}, Lcom/android/settingslib/f/g;-><init>(Landroid/os/storage/StorageManager;)V

    invoke-direct {v2, p1, p0, v3, v4}, Lcom/android/settings/deviceinfo/storage/e;-><init>(Landroid/content/Context;Landroid/app/Fragment;Landroid/os/storage/VolumeInfo;Lcom/android/settingslib/f/b;)V

    iput-object v2, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHW:Lcom/android/settings/deviceinfo/storage/e;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHW:Lcom/android/settings/deviceinfo/storage/e;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/applications/UserManagerWrapperImpl;

    const-class v0, Landroid/os/UserManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-direct {v2, v0}, Lcom/android/settings/applications/UserManagerWrapperImpl;-><init>(Landroid/os/UserManager;)V

    invoke-static {p1, v2}, Lcom/android/settings/deviceinfo/storage/a;->aty(Landroid/content/Context;Lcom/android/settings/applications/UserManagerWrapper;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHX:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHX:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v1
.end method

.method initializeOptionsMenu(Landroid/app/Activity;)V
    .locals 4

    new-instance v0, Lcom/android/settings/deviceinfo/PrivateVolumeOptionMenuController;

    iget-object v1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aIa:Landroid/os/storage/VolumeInfo;

    new-instance v2, Lcom/android/settings/applications/PackageManagerWrapperImpl;

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/settings/applications/PackageManagerWrapperImpl;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/android/settings/deviceinfo/PrivateVolumeOptionMenuController;-><init>(Landroid/content/Context;Landroid/os/storage/VolumeInfo;Lcom/android/settings/applications/PackageManagerWrapper;)V

    iput-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHV:Lcom/android/settings/deviceinfo/PrivateVolumeOptionMenuController;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aHV:Lcom/android/settings/deviceinfo/PrivateVolumeOptionMenuController;

    invoke-virtual {v0, v1}, Lcom/android/settings/core/lifecycle/c;->ajv(Lcom/android/settings/core/lifecycle/b;)Lcom/android/settings/core/lifecycle/b;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v0, Landroid/os/storage/StorageManager;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/settings/aq;->brH(Landroid/os/storage/StorageManager;Landroid/os/Bundle;)Landroid/os/storage/VolumeInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aIa:Landroid/os/storage/VolumeInfo;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aIa:Landroid/os/storage/VolumeInfo;

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->initializeOptionsMenu(Landroid/app/Activity;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Lcom/android/settings/deviceinfo/storage/f;

    new-instance v2, Lcom/android/settings/applications/UserManagerWrapperImpl;

    const-class v3, Landroid/os/UserManager;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    invoke-direct {v2, v3}, Lcom/android/settings/applications/UserManagerWrapperImpl;-><init>(Landroid/os/UserManager;)V

    iget-object v3, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aIa:Landroid/os/storage/VolumeInfo;

    iget-object v3, v3, Landroid/os/storage/VolumeInfo;->fsUuid:Ljava/lang/String;

    new-instance v4, Lcom/android/settingslib/b/H;

    invoke-direct {v4, v1}, Lcom/android/settingslib/b/H;-><init>(Landroid/content/Context;)V

    new-instance v5, Lcom/android/settings/applications/PackageManagerWrapperImpl;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/settings/applications/PackageManagerWrapperImpl;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-direct/range {v0 .. v5}, Lcom/android/settings/deviceinfo/storage/f;-><init>(Landroid/content/Context;Lcom/android/settings/applications/UserManagerWrapper;Ljava/lang/String;Lcom/android/settingslib/b/H;Lcom/android/settings/applications/PackageManagerWrapper;)V

    return-object v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/util/SparseArray;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->avm(Landroid/content/Loader;Landroid/util/SparseArray;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    new-instance v2, Lcom/android/settings/deviceinfo/StorageDashboardFragment$IconLoaderCallbacks;

    invoke-direct {v2, p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment$IconLoaderCallbacks;-><init>(Lcom/android/settings/deviceinfo/StorageDashboardFragment;)V

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    invoke-virtual {p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    new-instance v2, Lcom/android/settings/deviceinfo/StorageDashboardFragment$VolumeSizeCallbacks;

    invoke-direct {v2, p0}, Lcom/android/settings/deviceinfo/StorageDashboardFragment$VolumeSizeCallbacks;-><init>(Lcom/android/settings/deviceinfo/StorageDashboardFragment;)V

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/dashboard/MiuiDashboardFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    return-void
.end method

.method protected setVolume(Landroid/os/storage/VolumeInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/StorageDashboardFragment;->aIa:Landroid/os/storage/VolumeInfo;

    return-void
.end method
