.class public Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;
.super Ljava/lang/Object;
.source "PercentageBarChart.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final aKU:I

.field public final aKV:Landroid/graphics/Paint;

.field public final aKW:F


# direct methods
.method protected constructor <init>(IFLandroid/graphics/Paint;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;->aKU:I

    iput p2, p0, Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;->aKW:F

    iput-object p3, p0, Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;->aKV:Landroid/graphics/Paint;

    return-void
.end method


# virtual methods
.method public ayd(Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;)I
    .locals 2

    iget v0, p0, Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;->aKU:I

    iget v1, p1, Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;->aKU:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;

    invoke-virtual {p0, p1}, Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;->ayd(Lcom/android/settings/deviceinfo/PercentageBarChart$Entry;)I

    move-result v0

    return v0
.end method
