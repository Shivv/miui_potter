.class public Lcom/android/settings/LockPatternView;
.super Landroid/view/View;
.source "LockPatternView.java"


# instance fields
.field private btA:Z

.field private btB:F

.field private btC:F

.field private btD:I

.field private btE:Landroid/graphics/Paint;

.field private bta:J

.field private final btb:Landroid/graphics/Matrix;

.field private btc:I

.field private btd:Landroid/graphics/Bitmap;

.field private bte:Landroid/graphics/Bitmap;

.field private btf:I

.field private btg:I

.field private final bth:Landroid/graphics/Matrix;

.field private final bti:Landroid/graphics/Path;

.field private btj:F

.field private btk:I

.field private btl:I

.field private btm:Z

.field private btn:Z

.field private bto:F

.field private btp:F

.field private btq:F

.field private btr:Z

.field private bts:Z

.field private final btt:Landroid/graphics/Rect;

.field private btu:Lcom/android/settings/h;

.field private btv:Landroid/graphics/Paint;

.field private btw:Landroid/graphics/Paint;

.field private btx:Ljava/util/ArrayList;

.field private bty:Lcom/android/settings/LockPatternView$DisplayMode;

.field private btz:[[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/LockPatternView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v5, 0x3

    const/high16 v4, -0x40800000    # -1.0f

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v3, p0, Lcom/android/settings/LockPatternView;->btm:Z

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->btv:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    filled-new-array {v5, v5}, [I

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->btz:[[Z

    iput v4, p0, Lcom/android/settings/LockPatternView;->btp:F

    iput v4, p0, Lcom/android/settings/LockPatternView;->btq:F

    sget-object v0, Lcom/android/settings/LockPatternView$DisplayMode;->btG:Lcom/android/settings/LockPatternView$DisplayMode;

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    iput-boolean v2, p0, Lcom/android/settings/LockPatternView;->bts:Z

    iput-boolean v3, p0, Lcom/android/settings/LockPatternView;->btr:Z

    iput-boolean v2, p0, Lcom/android/settings/LockPatternView;->btn:Z

    iput-boolean v3, p0, Lcom/android/settings/LockPatternView;->btA:Z

    const v0, 0x3d4ccccd    # 0.05f

    iput v0, p0, Lcom/android/settings/LockPatternView;->btj:F

    const/16 v0, 0x40

    iput v0, p0, Lcom/android/settings/LockPatternView;->btD:I

    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/android/settings/LockPatternView;->bto:F

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->bti:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->btt:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->btb:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->bth:Landroid/graphics/Matrix;

    iput v3, p0, Lcom/android/settings/LockPatternView;->btk:I

    iput v3, p0, Lcom/android/settings/LockPatternView;->btl:I

    invoke-direct {p0, p1, p2}, Lcom/android/settings/LockPatternView;->bfY(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v2}, Lcom/android/settings/LockPatternView;->setClickable(Z)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/settings/LockPatternView;->btD:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/settings/LockPatternView;->btD:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    return-void
.end method

.method private bfI(Lcom/android/internal/widget/LockPatternView$Cell;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btz:[[Z

    invoke-virtual {p1}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p1}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v1

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bfZ()V

    return-void
.end method

.method private bfJ(FF)Lcom/android/internal/widget/LockPatternView$Cell;
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p2}, Lcom/android/settings/LockPatternView;->bfU(F)I

    move-result v0

    if-gez v0, :cond_0

    return-object v3

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/LockPatternView;->bfT(F)I

    move-result v1

    if-gez v1, :cond_1

    return-object v3

    :cond_1
    iget-object v2, p0, Lcom/android/settings/LockPatternView;->btz:[[Z

    aget-object v2, v2, v0

    aget-boolean v2, v2, v1

    if-eqz v2, :cond_2

    return-object v3

    :cond_2
    invoke-static {v0, v1}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v0

    return-object v0
.end method

.method private bfL()V
    .locals 5

    const/4 v4, 0x3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_0

    iget-object v3, p0, Lcom/android/settings/LockPatternView;->btz:[[Z

    aget-object v3, v3, v2

    aput-boolean v1, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private bfM(FF)Lcom/android/internal/widget/LockPatternView$Cell;
    .locals 10

    const/4 v9, 0x2

    const/4 v3, -0x1

    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/LockPatternView;->bfJ(FF)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v5

    if-eqz v5, :cond_7

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v5}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v4

    sub-int v6, v1, v4

    invoke-virtual {v5}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v4

    sub-int v7, v1, v4

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v4

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-ne v8, v9, :cond_0

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-eq v8, v2, :cond_0

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v8

    if-lez v6, :cond_5

    move v1, v2

    :goto_0
    add-int/2addr v1, v8

    :cond_0
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-ne v8, v9, :cond_6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-eq v6, v2, :cond_6

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v0

    if-lez v7, :cond_1

    move v3, v2

    :cond_1
    add-int/2addr v0, v3

    :goto_1
    invoke-static {v1, v0}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->btz:[[Z

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v3

    aget-object v1, v1, v3

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v3

    aget-boolean v1, v1, v3

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    invoke-direct {p0, v0}, Lcom/android/settings/LockPatternView;->bfI(Lcom/android/internal/widget/LockPatternView$Cell;)V

    :cond_3
    invoke-direct {p0, v5}, Lcom/android/settings/LockPatternView;->bfI(Lcom/android/internal/widget/LockPatternView$Cell;)V

    iget-boolean v0, p0, Lcom/android/settings/LockPatternView;->btn:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    invoke-virtual {p0, v2, v0}, Lcom/android/settings/LockPatternView;->performHapticFeedback(II)Z

    :cond_4
    return-object v5

    :cond_5
    move v1, v3

    goto :goto_0

    :cond_6
    move v0, v4

    goto :goto_1

    :cond_7
    return-object v0
.end method

.method private bfO(Landroid/graphics/Canvas;IIZ)V
    .locals 7

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v6, 0x3f800000    # 1.0f

    if-eqz p4, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/LockPatternView;->btr:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btH:Lcom/android/settings/LockPatternView$DisplayMode;

    if-eq v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bte:Landroid/graphics/Bitmap;

    :goto_0
    iget v1, p0, Lcom/android/settings/LockPatternView;->btg:I

    iget v2, p0, Lcom/android/settings/LockPatternView;->btf:I

    iget v3, p0, Lcom/android/settings/LockPatternView;->btC:F

    iget v4, p0, Lcom/android/settings/LockPatternView;->btB:F

    int-to-float v1, v1

    sub-float v1, v3, v1

    div-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v2, v2

    sub-float v2, v4, v2

    div-float/2addr v2, v5

    float-to-int v2, v2

    iget v3, p0, Lcom/android/settings/LockPatternView;->btC:F

    iget v4, p0, Lcom/android/settings/LockPatternView;->btg:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3, v6}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iget v4, p0, Lcom/android/settings/LockPatternView;->btB:F

    iget v5, p0, Lcom/android/settings/LockPatternView;->btf:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-static {v4, v6}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget-object v5, p0, Lcom/android/settings/LockPatternView;->bth:Landroid/graphics/Matrix;

    add-int/2addr v1, p2

    int-to-float v1, v1

    add-int/2addr v2, p3

    int-to-float v2, v2

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->bth:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/android/settings/LockPatternView;->btg:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v5, p0, Lcom/android/settings/LockPatternView;->btf:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v1, v2, v5}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->bth:Landroid/graphics/Matrix;

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->bth:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/android/settings/LockPatternView;->btg:I

    neg-int v2, v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/android/settings/LockPatternView;->btf:I

    neg-int v3, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->bth:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/android/settings/LockPatternView;->btv:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    :cond_1
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/android/settings/LockPatternView;->btA:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bte:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btH:Lcom/android/settings/LockPatternView$DisplayMode;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btd:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btG:Lcom/android/settings/LockPatternView$DisplayMode;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btF:Lcom/android/settings/LockPatternView$DisplayMode;

    if-ne v0, v1, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bte:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown display mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private bfQ(I)Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, -0x1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private bfR(I)F
    .locals 3

    iget v0, p0, Lcom/android/settings/LockPatternView;->mPaddingLeft:I

    int-to-float v0, v0

    int-to-float v1, p1

    iget v2, p0, Lcom/android/settings/LockPatternView;->btC:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/settings/LockPatternView;->btC:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private bfS(I)F
    .locals 3

    iget v0, p0, Lcom/android/settings/LockPatternView;->mPaddingTop:I

    int-to-float v0, v0

    int-to-float v1, p1

    iget v2, p0, Lcom/android/settings/LockPatternView;->btB:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/settings/LockPatternView;->btB:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private bfT(F)I
    .locals 6

    iget v1, p0, Lcom/android/settings/LockPatternView;->btC:F

    iget v0, p0, Lcom/android/settings/LockPatternView;->bto:F

    mul-float v2, v1, v0

    iget v0, p0, Lcom/android/settings/LockPatternView;->mPaddingLeft:I

    int-to-float v0, v0

    sub-float v3, v1, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v3, v0

    const/4 v0, 0x0

    :goto_0
    const/4 v4, 0x3

    if-ge v0, v4, :cond_1

    int-to-float v4, v0

    mul-float/2addr v4, v1

    add-float/2addr v4, v3

    cmpl-float v5, p1, v4

    if-ltz v5, :cond_0

    add-float/2addr v4, v2

    cmpg-float v4, p1, v4

    if-gtz v4, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method private bfU(F)I
    .locals 6

    iget v1, p0, Lcom/android/settings/LockPatternView;->btB:F

    iget v0, p0, Lcom/android/settings/LockPatternView;->bto:F

    mul-float v2, v1, v0

    iget v0, p0, Lcom/android/settings/LockPatternView;->mPaddingTop:I

    int-to-float v0, v0

    sub-float v3, v1, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v3, v0

    const/4 v0, 0x0

    :goto_0
    const/4 v4, 0x3

    if-ge v0, v4, :cond_1

    int-to-float v4, v0

    mul-float/2addr v4, v1

    add-float/2addr v4, v3

    cmpl-float v5, p1, v4

    if-ltz v5, :cond_0

    add-float/2addr v4, v2

    cmpg-float v4, p1, v4

    if-gtz v4, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method private bfV(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bgd()V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/LockPatternView;->bfM(FF)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/settings/LockPatternView;->btA:Z

    sget-object v2, Lcom/android/settings/LockPatternView$DisplayMode;->btG:Lcom/android/settings/LockPatternView$DisplayMode;

    iput-object v2, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bgc()V

    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->invalidate()V

    :goto_0
    iput v0, p0, Lcom/android/settings/LockPatternView;->btp:F

    iput v1, p0, Lcom/android/settings/LockPatternView;->btq:F

    return-void

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/settings/LockPatternView;->btA:Z

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bga()V

    goto :goto_0
.end method

.method private bfW(Landroid/view/MotionEvent;)V
    .locals 8

    const/4 v7, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    add-int/lit8 v0, v3, 0x1

    if-ge v2, v0, :cond_4

    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    move v1, v0

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    :goto_2
    invoke-direct {p0, v1, v0}, Lcom/android/settings/LockPatternView;->bfM(FF)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v4, :cond_0

    if-ne v5, v7, :cond_0

    iput-boolean v7, p0, Lcom/android/settings/LockPatternView;->btA:Z

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bgc()V

    :cond_0
    iget v4, p0, Lcom/android/settings/LockPatternView;->btp:F

    sub-float v4, v1, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/android/settings/LockPatternView;->btq:F

    sub-float v5, v0, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/android/settings/LockPatternView;->btC:F

    const v6, 0x3c23d70a    # 0.01f

    mul-float/2addr v5, v6

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    iput v1, p0, Lcom/android/settings/LockPatternView;->btp:F

    iput v0, p0, Lcom/android/settings/LockPatternView;->btq:F

    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->invalidate()V

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_2

    :cond_4
    return-void
.end method

.method private bfX(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/LockPatternView;->btA:Z

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bgb()V

    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->invalidate()V

    :cond_0
    return-void
.end method

.method private bfY(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v4, -0x1

    sget-object v1, Lcom/android/settings/cw;->bYC:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "square"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput v0, p0, Lcom/android/settings/LockPatternView;->btc:I

    :goto_0
    iget-object v2, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    const/16 v3, 0xd

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    const/16 v2, 0xa

    const v3, 0x3dcccccd    # 0.1f

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/android/settings/LockPatternView;->btj:F

    const/16 v2, 0xc

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    iput v2, p0, Lcom/android/settings/LockPatternView;->btD:I

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/settings/LockPatternView;->bfQ(I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/LockPatternView;->bte:Landroid/graphics/Bitmap;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    if-ne v4, v2, :cond_5

    iget-object v2, p0, Lcom/android/settings/LockPatternView;->bte:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/android/settings/LockPatternView;->btd:Landroid/graphics/Bitmap;

    :goto_1
    new-array v2, v5, [Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/android/settings/LockPatternView;->btd:Landroid/graphics/Bitmap;

    aput-object v3, v2, v0

    iget-object v3, p0, Lcom/android/settings/LockPatternView;->bte:Landroid/graphics/Bitmap;

    aput-object v3, v2, v6

    array-length v3, v2

    :goto_2
    if-ge v0, v3, :cond_6

    aget-object v4, v2, v0

    if-eqz v4, :cond_0

    iget v5, p0, Lcom/android/settings/LockPatternView;->btg:I

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/android/settings/LockPatternView;->btg:I

    iget v5, p0, Lcom/android/settings/LockPatternView;->btf:I

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p0, Lcom/android/settings/LockPatternView;->btf:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    const-string/jumbo v3, "lock_width"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput v6, p0, Lcom/android/settings/LockPatternView;->btc:I

    goto :goto_0

    :cond_2
    const-string/jumbo v3, "lock_height"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iput v5, p0, Lcom/android/settings/LockPatternView;->btc:I

    goto/16 :goto_0

    :cond_3
    const-string/jumbo v3, "fixed"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x3

    iput v2, p0, Lcom/android/settings/LockPatternView;->btc:I

    goto/16 :goto_0

    :cond_4
    iput v0, p0, Lcom/android/settings/LockPatternView;->btc:I

    goto/16 :goto_0

    :cond_5
    invoke-direct {p0, v2}, Lcom/android/settings/LockPatternView;->bfQ(I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/LockPatternView;->btd:Landroid/graphics/Bitmap;

    goto :goto_1

    :cond_6
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private bfZ()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btu:Lcom/android/settings/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btu:Lcom/android/settings/h;

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/android/settings/h;->onPatternCellAdded(Ljava/util/List;)V

    :cond_0
    const v0, 0x1108008b

    invoke-direct {p0, v0}, Lcom/android/settings/LockPatternView;->bgf(I)V

    return-void
.end method

.method private bga()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btu:Lcom/android/settings/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btu:Lcom/android/settings/h;

    invoke-interface {v0}, Lcom/android/settings/h;->onPatternCleared()V

    :cond_0
    const v0, 0x1108008a

    invoke-direct {p0, v0}, Lcom/android/settings/LockPatternView;->bgf(I)V

    return-void
.end method

.method private bgb()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btu:Lcom/android/settings/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btu:Lcom/android/settings/h;

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/android/settings/h;->onPatternDetected(Ljava/util/List;)V

    :cond_0
    const v0, 0x1108008c

    invoke-direct {p0, v0}, Lcom/android/settings/LockPatternView;->bgf(I)V

    return-void
.end method

.method private bgc()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btu:Lcom/android/settings/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btu:Lcom/android/settings/h;

    invoke-interface {v0}, Lcom/android/settings/h;->onPatternStart()V

    :cond_0
    const v0, 0x11080089

    invoke-direct {p0, v0}, Lcom/android/settings/LockPatternView;->bgf(I)V

    return-void
.end method

.method private bgd()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bfL()V

    sget-object v0, Lcom/android/settings/LockPatternView$DisplayMode;->btG:Lcom/android/settings/LockPatternView$DisplayMode;

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->invalidate()V

    return-void
.end method

.method private bge(II)I
    .locals 2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    move p2, v0

    :goto_0
    :sswitch_0
    return p2

    :sswitch_1
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method private bgf(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/LockPatternView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/settings/LockPatternView;->sendAccessibilityEvent(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/LockPatternView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public bfK()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bgd()V

    return-void
.end method

.method public bfN()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/LockPatternView;->bts:Z

    return-void
.end method

.method public bfP()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/LockPatternView;->bts:Z

    return-void
.end method

.method protected getSuggestedMinimumHeight()I
    .locals 1

    iget v0, p0, Lcom/android/settings/LockPatternView;->btg:I

    mul-int/lit8 v0, v0, 0x3

    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .locals 1

    iget v0, p0, Lcom/android/settings/LockPatternView;->btg:I

    mul-int/lit8 v0, v0, 0x3

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    iget-object v4, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget-object v6, p0, Lcom/android/settings/LockPatternView;->btz:[[Z

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btF:Lcom/android/settings/LockPatternView$DisplayMode;

    if-ne v0, v1, :cond_2

    add-int/lit8 v0, v5, 0x1

    mul-int/lit16 v0, v0, 0x2bc

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v8, p0, Lcom/android/settings/LockPatternView;->bta:J

    sub-long/2addr v2, v8

    long-to-int v1, v2

    rem-int v2, v1, v0

    div-int/lit16 v3, v2, 0x2bc

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bfL()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v7

    aget-object v7, v6, v7

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v0

    const/4 v8, 0x1

    aput-boolean v8, v7, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    if-lez v3, :cond_4

    if-ge v3, v5, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    rem-int/lit16 v0, v2, 0x2bc

    int-to-float v0, v0

    const/high16 v1, 0x442f0000    # 700.0f

    div-float v1, v0, v1

    add-int/lit8 v0, v3, -0x1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/settings/LockPatternView;->bfR(I)F

    move-result v2

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/LockPatternView;->bfS(I)F

    move-result v7

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/android/settings/LockPatternView;->bfR(I)F

    move-result v3

    sub-float/2addr v3, v2

    mul-float/2addr v3, v1

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/LockPatternView;->bfS(I)F

    move-result v0

    sub-float/2addr v0, v7

    mul-float/2addr v0, v1

    add-float v1, v2, v3

    iput v1, p0, Lcom/android/settings/LockPatternView;->btp:F

    add-float/2addr v0, v7

    iput v0, p0, Lcom/android/settings/LockPatternView;->btq:F

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->invalidate()V

    :cond_2
    iget v2, p0, Lcom/android/settings/LockPatternView;->btC:F

    iget v3, p0, Lcom/android/settings/LockPatternView;->btB:F

    iget v0, p0, Lcom/android/settings/LockPatternView;->btj:F

    mul-float/2addr v0, v2

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v7, p0, Lcom/android/settings/LockPatternView;->bti:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->rewind()V

    iget v8, p0, Lcom/android/settings/LockPatternView;->mPaddingTop:I

    iget v9, p0, Lcom/android/settings/LockPatternView;->mPaddingLeft:I

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    const/4 v0, 0x3

    if-ge v1, v0, :cond_6

    int-to-float v0, v8

    int-to-float v10, v1

    mul-float/2addr v10, v3

    add-float/2addr v10, v0

    const/4 v0, 0x0

    :goto_3
    const/4 v11, 0x3

    if-ge v0, v11, :cond_5

    int-to-float v11, v9

    int-to-float v12, v0

    mul-float/2addr v12, v2

    add-float/2addr v11, v12

    float-to-int v11, v11

    float-to-int v12, v10

    aget-object v13, v6, v1

    aget-boolean v13, v13, v0

    invoke-direct {p0, p1, v11, v12, v13}, Lcom/android/settings/LockPatternView;->bfO(Landroid/graphics/Canvas;IIZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_6
    iget-boolean v0, p0, Lcom/android/settings/LockPatternView;->btr:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    sget-object v1, Lcom/android/settings/LockPatternView$DisplayMode;->btH:Lcom/android/settings/LockPatternView$DisplayMode;

    if-ne v0, v1, :cond_c

    :cond_7
    const/4 v0, 0x1

    :goto_4
    iget-object v1, p0, Lcom/android/settings/LockPatternView;->btv:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getFlags()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_d

    const/4 v1, 0x1

    :goto_5
    iget-object v2, p0, Lcom/android/settings/LockPatternView;->btv:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    if-eqz v0, :cond_b

    const/4 v2, 0x0

    const/4 v0, 0x0

    move v3, v2

    move v2, v0

    :goto_6
    if-ge v2, v5, :cond_8

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v8

    aget-object v8, v6, v8

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v9

    aget-boolean v8, v8, v9

    if-nez v8, :cond_e

    :cond_8
    iget-boolean v0, p0, Lcom/android/settings/LockPatternView;->btA:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    sget-object v2, Lcom/android/settings/LockPatternView$DisplayMode;->btF:Lcom/android/settings/LockPatternView$DisplayMode;

    if-ne v0, v2, :cond_a

    :cond_9
    if-eqz v3, :cond_a

    iget v0, p0, Lcom/android/settings/LockPatternView;->btp:F

    iget v2, p0, Lcom/android/settings/LockPatternView;->btq:F

    invoke-virtual {v7, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_a
    iget-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    sget-object v2, Lcom/android/settings/LockPatternView$DisplayMode;->btH:Lcom/android/settings/LockPatternView$DisplayMode;

    if-eq v0, v2, :cond_10

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btw:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_b
    :goto_7
    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btv:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_4

    :cond_d
    const/4 v1, 0x0

    goto :goto_5

    :cond_e
    const/4 v3, 0x1

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/android/settings/LockPatternView;->bfR(I)F

    move-result v8

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/LockPatternView;->bfS(I)F

    move-result v0

    if-nez v2, :cond_f

    invoke-virtual {v7, v8, v0}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_f
    invoke-virtual {v7, v8, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_8

    :cond_10
    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btE:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_7
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/android/settings/LockPatternView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 6

    const v2, 0x7f07019f

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->getSuggestedMinimumWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->getSuggestedMinimumHeight()I

    move-result v4

    invoke-direct {p0, p1, v1}, Lcom/android/settings/LockPatternView;->bge(II)I

    move-result v3

    invoke-direct {p0, p2, v4}, Lcom/android/settings/LockPatternView;->bge(II)I

    move-result v1

    iget v4, p0, Lcom/android/settings/LockPatternView;->btc:I

    packed-switch v4, :pswitch_data_0

    move v0, v1

    move v1, v3

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/LockPatternView;->setMeasuredDimension(II)V

    return-void

    :pswitch_0
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    goto :goto_0

    :pswitch_1
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v3

    goto :goto_0

    :pswitch_2
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0

    :pswitch_3
    iget v1, p0, Lcom/android/settings/LockPatternView;->btl:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v0, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/android/settings/LockPatternView;->btl:I

    goto :goto_1

    :cond_2
    iget v2, p0, Lcom/android/settings/LockPatternView;->btk:I

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    check-cast p1, Lcom/android/settings/LockPatternView$SavedState;

    invoke-virtual {p1}, Lcom/android/settings/LockPatternView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    sget-object v0, Lcom/android/settings/LockPatternView$DisplayMode;->btG:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-virtual {p1}, Lcom/android/settings/LockPatternView$SavedState;->bgh()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/widget/LockPatternUtils;->stringToPattern(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/LockPatternView;->setPattern(Lcom/android/settings/LockPatternView$DisplayMode;Ljava/util/List;)V

    invoke-static {}, Lcom/android/settings/LockPatternView$DisplayMode;->values()[Lcom/android/settings/LockPatternView$DisplayMode;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/settings/LockPatternView$SavedState;->bgg()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-virtual {p1}, Lcom/android/settings/LockPatternView$SavedState;->bgj()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/LockPatternView;->bts:Z

    invoke-virtual {p1}, Lcom/android/settings/LockPatternView$SavedState;->bgi()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/LockPatternView;->btr:Z

    invoke-virtual {p1}, Lcom/android/settings/LockPatternView$SavedState;->bgk()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/LockPatternView;->btn:Z

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 8

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lcom/android/settings/LockPatternView$SavedState;

    iget-object v2, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/android/internal/widget/LockPatternUtils;->patternToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    invoke-virtual {v3}, Lcom/android/settings/LockPatternView$DisplayMode;->ordinal()I

    move-result v3

    iget-boolean v4, p0, Lcom/android/settings/LockPatternView;->bts:Z

    iget-boolean v5, p0, Lcom/android/settings/LockPatternView;->btr:Z

    iget-boolean v6, p0, Lcom/android/settings/LockPatternView;->btn:Z

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/LockPatternView$SavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;IZZZLcom/android/settings/LockPatternView$SavedState;)V

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    const/high16 v2, 0x40400000    # 3.0f

    iget v0, p0, Lcom/android/settings/LockPatternView;->mPaddingLeft:I

    sub-int v0, p1, v0

    iget v1, p0, Lcom/android/settings/LockPatternView;->mPaddingRight:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/settings/LockPatternView;->btC:F

    iget v0, p0, Lcom/android/settings/LockPatternView;->mPaddingTop:I

    sub-int v0, p2, v0

    iget v1, p0, Lcom/android/settings/LockPatternView;->mPaddingBottom:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/settings/LockPatternView;->btB:F

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/settings/LockPatternView;->bts:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return v2

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    return v2

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/settings/LockPatternView;->bfV(Landroid/view/MotionEvent;)V

    return v1

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/settings/LockPatternView;->bfX(Landroid/view/MotionEvent;)V

    return v1

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/settings/LockPatternView;->bfW(Landroid/view/MotionEvent;)V

    return v1

    :pswitch_3
    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bgd()V

    iput-boolean v2, p0, Lcom/android/settings/LockPatternView;->btA:Z

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bga()V

    return v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setBitmapBtnTouched(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/LockPatternView;->bfQ(I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/LockPatternView;->bte:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setDisplayMode(Lcom/android/settings/LockPatternView$DisplayMode;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/android/settings/LockPatternView;->bty:Lcom/android/settings/LockPatternView$DisplayMode;

    sget-object v0, Lcom/android/settings/LockPatternView$DisplayMode;->btF:Lcom/android/settings/LockPatternView$DisplayMode;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "you must have a pattern to animate if you want to set the display mode to animate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/LockPatternView;->bta:J

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/settings/LockPatternView;->bfR(I)F

    move-result v1

    iput v1, p0, Lcom/android/settings/LockPatternView;->btp:F

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/LockPatternView;->bfS(I)F

    move-result v0

    iput v0, p0, Lcom/android/settings/LockPatternView;->btq:F

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bfL()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/LockPatternView;->invalidate()V

    return-void
.end method

.method public setDistancePoints(II)V
    .locals 0

    iput p1, p0, Lcom/android/settings/LockPatternView;->btl:I

    iput p2, p0, Lcom/android/settings/LockPatternView;->btk:I

    return-void
.end method

.method public setInStealthMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/LockPatternView;->btr:Z

    return-void
.end method

.method public setOnPatternListener(Lcom/android/settings/h;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/LockPatternView;->btu:Lcom/android/settings/h;

    return-void
.end method

.method public setPattern(Lcom/android/settings/LockPatternView$DisplayMode;Ljava/util/List;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/settings/LockPatternView;->btx:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0}, Lcom/android/settings/LockPatternView;->bfL()V

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    iget-object v2, p0, Lcom/android/settings/LockPatternView;->btz:[[Z

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v0

    const/4 v3, 0x1

    aput-boolean v3, v2, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/LockPatternView;->setDisplayMode(Lcom/android/settings/LockPatternView$DisplayMode;)V

    return-void
.end method

.method public setTactileFeedbackEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/LockPatternView;->btn:Z

    return-void
.end method
