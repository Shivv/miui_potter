.class final Lcom/android/settings/kY;
.super Ljava/lang/Object;
.source "MiuiSettingsPreferenceFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic crM:Lcom/android/settings/kX;

.field final synthetic crN:I

.field final synthetic crO:Landroid/widget/ListView;

.field final synthetic crP:Landroid/graphics/drawable/Drawable;


# direct methods
.method constructor <init>(Lcom/android/settings/kX;ILandroid/widget/ListView;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/kY;->crM:Lcom/android/settings/kX;

    iput p2, p0, Lcom/android/settings/kY;->crN:I

    iput-object p3, p0, Lcom/android/settings/kY;->crO:Landroid/widget/ListView;

    iput-object p4, p0, Lcom/android/settings/kY;->crP:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lcom/android/settings/kY;->crN:I

    iget-object v1, p0, Lcom/android/settings/kY;->crO:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/kY;->crO:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/kY;->crO:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/android/settings/kY;->crP:Landroid/graphics/drawable/Drawable;

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setPressed(Z)V

    :cond_0
    return-void
.end method
