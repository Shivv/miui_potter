.class public Lcom/android/settings/development/ColorModePreference;
.super Landroid/preference/CheckBoxPreference;
.source "ColorModePreference.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# instance fields
.field private bgc:I

.field private bgd:Ljava/util/ArrayList;

.field private bge:Landroid/view/Display;

.field private bgf:Landroid/hardware/display/DisplayManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/android/settings/development/ColorModePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bgf:Landroid/hardware/display/DisplayManager;

    return-void
.end method


# virtual methods
.method public aUh()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bgd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public aUi()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bgf:Landroid/hardware/display/DisplayManager;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v0, p0, v1}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    return-void
.end method

.method public aUj()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bgf:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, p0}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    return-void
.end method

.method public aUk()V
    .locals 9

    const/4 v8, -0x1

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bgf:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bge:Landroid/view/Display;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bgd:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/settings/development/ColorModePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f030053

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    const v4, 0x7f030054

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f030052

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    move v0, v1

    :goto_0
    array-length v6, v2

    if-ge v0, v6, :cond_1

    aget v6, v2, v0

    if-eq v6, v8, :cond_0

    if-eq v0, v3, :cond_0

    new-instance v6, Lcom/android/settings/development/a;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Lcom/android/settings/development/a;-><init>(Lcom/android/settings/development/a;)V

    aget v7, v2, v0

    invoke-static {v6, v7}, Lcom/android/settings/development/a;->aUm(Lcom/android/settings/development/a;I)I

    aget-object v7, v4, v0

    invoke-static {v6, v7}, Lcom/android/settings/development/a;->aUo(Lcom/android/settings/development/a;Ljava/lang/String;)Ljava/lang/String;

    aget-object v7, v5, v0

    invoke-static {v6, v7}, Lcom/android/settings/development/a;->aUn(Lcom/android/settings/development/a;Ljava/lang/String;)Ljava/lang/String;

    iget-object v7, p0, Lcom/android/settings/development/ColorModePreference;->bgd:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bge:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getColorMode()I

    move-result v4

    iput v8, p0, Lcom/android/settings/development/ColorModePreference;->bgc:I

    move v2, v1

    :goto_1
    iget-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bgd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bgd:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/a;

    invoke-static {v0}, Lcom/android/settings/development/a;->aUl(Lcom/android/settings/development/a;)I

    move-result v0

    if-ne v0, v4, :cond_4

    iput v2, p0, Lcom/android/settings/development/ColorModePreference;->bgc:I

    :cond_2
    iget v0, p0, Lcom/android/settings/development/ColorModePreference;->bgc:I

    if-ne v0, v3, :cond_3

    move v1, v3

    :cond_3
    invoke-virtual {p0, v1}, Lcom/android/settings/development/ColorModePreference;->setChecked(Z)V

    return-void

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method

.method public onDisplayAdded(I)V
    .locals 0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/development/ColorModePreference;->aUk()V

    :cond_0
    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/development/ColorModePreference;->aUk()V

    :cond_0
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0

    return-void
.end method

.method protected persistBoolean(Z)Z
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/development/ColorModePreference;->bgd:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/development/ColorModePreference;->bgd:Ljava/util/ArrayList;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/a;

    iget-object v2, p0, Lcom/android/settings/development/ColorModePreference;->bge:Landroid/view/Display;

    invoke-static {v0}, Lcom/android/settings/development/a;->aUl(Lcom/android/settings/development/a;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/Display;->requestColorMode(I)V

    iget-object v2, p0, Lcom/android/settings/development/ColorModePreference;->bgd:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/development/ColorModePreference;->bgc:I

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
