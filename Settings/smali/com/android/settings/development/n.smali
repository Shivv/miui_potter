.class final Lcom/android/settings/development/n;
.super Ljava/lang/Object;
.source "DevelopmentSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic biA:Lcom/android/settings/development/DevelopmentSettings;

.field final synthetic biB:Ljava/lang/Boolean;


# direct methods
.method constructor <init>(Lcom/android/settings/development/DevelopmentSettings;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/development/n;->biA:Lcom/android/settings/development/DevelopmentSettings;

    iput-object p2, p0, Lcom/android/settings/development/n;->biB:Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    const-string/jumbo v0, "persist.sys.miui_optimization"

    iget-object v1, p0, Lcom/android/settings/development/n;->biB:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/development/n;->biA:Lcom/android/settings/development/DevelopmentSettings;

    invoke-static {v0}, Lcom/android/settings/development/DevelopmentSettings;->aWR(Lcom/android/settings/development/DevelopmentSettings;)Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MiuiSettings$Secure;->MIUI_OPTIMIZATION:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/development/n;->biB:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/development/n;->biA:Lcom/android/settings/development/DevelopmentSettings;

    iget-object v1, p0, Lcom/android/settings/development/n;->biB:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/development/DevelopmentSettings;->aWT(Lcom/android/settings/development/DevelopmentSettings;Z)V

    iget-object v0, p0, Lcom/android/settings/development/n;->biA:Lcom/android/settings/development/DevelopmentSettings;

    invoke-static {v0}, Lcom/android/settings/development/DevelopmentSettings;->aWO(Lcom/android/settings/development/DevelopmentSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/development/n;->biB:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
