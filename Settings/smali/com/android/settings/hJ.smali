.class final Lcom/android/settings/hJ;
.super Ljava/lang/Object;
.source "MiuiSecuritySettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic cnu:Lcom/android/settings/MiuiSecuritySettings;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiSecuritySettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hJ;->cnu:Lcom/android/settings/MiuiSecuritySettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/hJ;->cnu:Lcom/android/settings/MiuiSecuritySettings;

    invoke-static {v0}, Lcom/android/settings/MiuiSecuritySettings;->bDR(Lcom/android/settings/MiuiSecuritySettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/hJ;->cnu:Lcom/android/settings/MiuiSecuritySettings;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/settings/MiuiConfirmCommonPassword;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/android/settings/hJ;->cnu:Lcom/android/settings/MiuiSecuritySettings;

    const/16 v2, 0x3ea

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/MiuiSecuritySettings;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
