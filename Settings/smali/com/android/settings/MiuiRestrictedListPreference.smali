.class public Lcom/android/settings/MiuiRestrictedListPreference;
.super Landroid/preference/ListPreference;
.source "MiuiRestrictedListPreference.java"


# instance fields
.field private final bIF:Lcom/android/settingslib/o;

.field private final bIG:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIG:Ljava/util/List;

    const v0, 0x7f0d0190

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiRestrictedListPreference;->setWidgetLayoutResource(I)V

    new-instance v0, Lcom/android/settingslib/o;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/o;-><init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIF:Lcom/android/settingslib/o;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIG:Ljava/util/List;

    new-instance v0, Lcom/android/settingslib/o;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/o;-><init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIF:Lcom/android/settingslib/o;

    return-void
.end method


# virtual methods
.method public bzf()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIF:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->cqj()Z

    move-result v0

    return v0
.end method

.method protected bzg()Landroid/widget/ListAdapter;
    .locals 4

    new-instance v0, Lcom/android/settings/aY;

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedListPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedListPreference;->bzh()I

    move-result v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/settings/aY;-><init>(Lcom/android/settings/MiuiRestrictedListPreference;Landroid/content/Context;[Ljava/lang/CharSequence;I)V

    return-object v0
.end method

.method public bzh()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiRestrictedListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bzi(Ljava/lang/CharSequence;)Z
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIG:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/aZ;

    iget-object v0, v0, Lcom/android/settings/aZ;->bIJ:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    return v2
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIF:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a038d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedListPreference;->bzf()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedListPreference;->bzg()Landroid/widget/ListAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method public performClick(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIF:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->performClick()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    :cond_0
    return-void
.end method

.method public setDisabledByAdmin(Lcom/android/settingslib/n;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIF:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedListPreference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiRestrictedListPreference;->bzf()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiRestrictedListPreference;->bIF:Lcom/android/settingslib/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->setEnabled(Z)V

    return-void
.end method
