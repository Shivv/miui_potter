.class public Lcom/android/settings/c/a;
.super Ljava/lang/Object;
.source "FingerprintManagerWrapper.java"

# interfaces
.implements Lcom/android/settings/c/b;


# instance fields
.field private bfM:Landroid/hardware/fingerprint/FingerprintManager;


# direct methods
.method public constructor <init>(Landroid/hardware/fingerprint/FingerprintManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/settings/c/a;->bfM:Landroid/hardware/fingerprint/FingerprintManager;

    return-void
.end method


# virtual methods
.method public aTS([BLandroid/os/CancellationSignal;IILandroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/c/a;->bfM:Landroid/hardware/fingerprint/FingerprintManager;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/hardware/fingerprint/FingerprintManager;->enroll([BLandroid/os/CancellationSignal;IILandroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;)V

    return-void
.end method

.method public aTT(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/c/a;->bfM:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0, p1}, Landroid/hardware/fingerprint/FingerprintManager;->setActiveUser(I)V

    return-void
.end method
