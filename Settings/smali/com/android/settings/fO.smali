.class final Lcom/android/settings/fO;
.super Ljava/lang/Object;
.source "PrivacySettings.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic ckU:Lcom/android/settings/PrivacySettings;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/settings/PrivacySettings;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/fO;->ckU:Lcom/android/settings/PrivacySettings;

    iput-object p2, p0, Lcom/android/settings/fO;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "booleanResult"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/fO;->ckU:Lcom/android/settings/PrivacySettings;

    iget-object v1, p0, Lcom/android/settings/fO;->val$intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/android/settings/PrivacySettings;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "PrivacySettings"

    const-string/jumbo v1, "login account failed, finish"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "PrivacySettings"

    const-string/jumbo v2, "Exception when add account"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
