.class public Lcom/android/settings/C;
.super Landroid/widget/BaseAdapter;
.source "AutoDisableScreenButtonsAppListSettings.java"


# instance fields
.field bxx:Landroid/view/View$OnClickListener;

.field private bxy:Ljava/util/List;

.field final synthetic bxz:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/C;->bxz:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Lcom/android/settings/dZ;

    invoke-direct {v0, p0}, Lcom/android/settings/dZ;-><init>(Lcom/android/settings/C;)V

    iput-object v0, p0, Lcom/android/settings/C;->bxx:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private ble(I)I
    .locals 1

    if-ltz p1, :cond_0

    sget-object v0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxp:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    return v0

    :cond_1
    sget-object v0, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxp:[I

    aget v0, v0, p1

    return v0
.end method

.method private blf(I)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxp:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    sget-object v1, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxp:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    return v0
.end method

.method static synthetic bli(Lcom/android/settings/C;I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/C;->ble(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public blg(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/C;->bxy:Ljava/util/List;

    return-void
.end method

.method protected blh(Lcom/android/settings/B;)V
    .locals 4

    const/4 v2, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/C;->bxz:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-static {v1}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkP(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1201a8

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/C;->bxz:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    iget-object v1, v1, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bxo:[Ljava/lang/String;

    invoke-static {p1}, Lcom/android/settings/B;->blb(Lcom/android/settings/B;)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/settings/C;->blf(I)I

    move-result v2

    new-instance v3, Lcom/android/settings/ea;

    invoke-direct {v3, p0, p1}, Lcom/android/settings/ea;-><init>(Lcom/android/settings/C;Lcom/android/settings/B;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/C;->bxz:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkV(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;Landroid/app/Dialog;)Landroid/app/Dialog;

    iget-object v0, p0, Lcom/android/settings/C;->bxz:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-static {v0}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkQ(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/C;->bxy:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/C;->bxy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/android/settings/B;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/C;->bxy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/B;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/C;->getItem(I)Lcom/android/settings/B;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/C;->bxy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/B;

    invoke-static {v0}, Lcom/android/settings/B;->bld(Lcom/android/settings/B;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/C;->bxy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/B;

    invoke-static {v0}, Lcom/android/settings/B;->bld(Lcom/android/settings/B;)I

    move-result v1

    if-nez v1, :cond_3

    if-nez p2, :cond_1

    new-instance v2, Lcom/android/settings/D;

    invoke-direct {v2, p0, v4}, Lcom/android/settings/D;-><init>(Lcom/android/settings/C;Lcom/android/settings/D;)V

    iget-object v1, p0, Lcom/android/settings/C;->bxz:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-static {v1}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkT(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f0d003f

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x1020006

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/android/settings/D;->bxB:Landroid/widget/ImageView;

    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/D;->bxD:Landroid/widget/TextView;

    const v1, 0x1020010

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/D;->bxC:Landroid/widget/TextView;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_0
    invoke-static {v0}, Lcom/android/settings/B;->bla(Lcom/android/settings/B;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/android/settings/D;->bxC:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-object v2, p0, Lcom/android/settings/C;->bxz:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-static {v2}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkN(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Lcom/android/settings/cN;

    move-result-object v2

    iget-object v3, v1, Lcom/android/settings/D;->bxB:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/android/settings/B;->bkY()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/cN;->bWl(Landroid/widget/ImageView;Ljava/lang/String;)Z

    iget-object v1, v1, Lcom/android/settings/D;->bxD:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/settings/B;->blc(Lcom/android/settings/B;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f1201ad

    invoke-virtual {p2, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/C;->bxx:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_2
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/D;

    goto :goto_0

    :cond_2
    iget-object v2, v1, Lcom/android/settings/D;->bxC:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/settings/B;->bla(Lcom/android/settings/B;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/android/settings/D;->bxC:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    invoke-static {v0}, Lcom/android/settings/B;->bld(Lcom/android/settings/B;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    if-nez p2, :cond_4

    new-instance v2, Lcom/android/settings/D;

    invoke-direct {v2, p0, v4}, Lcom/android/settings/D;-><init>(Lcom/android/settings/C;Lcom/android/settings/D;)V

    iget-object v1, p0, Lcom/android/settings/C;->bxz:Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;

    invoke-static {v1}, Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;->bkT(Lcom/android/settings/AutoDisableScreenButtonsAppListSettings;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f0d003e

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f0a01db

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/android/settings/D;->bxA:Landroid/widget/TextView;

    invoke-virtual {p2, v5}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_3
    iget-object v1, v1, Lcom/android/settings/D;->bxA:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/settings/B;->blc(Lcom/android/settings/B;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/D;

    goto :goto_3
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
