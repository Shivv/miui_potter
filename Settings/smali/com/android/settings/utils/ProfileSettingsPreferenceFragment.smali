.class public abstract Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "ProfileSettingsPreferenceFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private ayi(Lcom/android/settingslib/drawer/h;)Z
    .locals 8

    const/4 v3, 0x1

    const/4 v1, 0x0

    move v0, v1

    move v2, v1

    :goto_0
    invoke-virtual {p1}, Lcom/android/settingslib/drawer/h;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_1

    invoke-virtual {p1, v0}, Lcom/android/settingslib/drawer/h;->getItemId(I)J

    move-result-wide v4

    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->ayh()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v7

    long-to-int v4, v4

    invoke-virtual {v7, v6, v1, v4}, Landroid/content/pm/PackageManager;->resolveActivityAsUser(Landroid/content/Intent;II)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-le v2, v3, :cond_2

    move v1, v3

    :cond_2
    return v1
.end method


# virtual methods
.method protected abstract ayh()Ljava/lang/String;
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {p0}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settingslib/drawer/h;->ceo(Landroid/os/UserManager;Landroid/content/Context;)Lcom/android/settingslib/drawer/h;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->ayi(Lcom/android/settingslib/drawer/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d01d7

    invoke-virtual {p0, v0}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->bWD(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v2, Lcom/android/settings/utils/l;

    invoke-direct {v2, p0, v1, v0}, Lcom/android/settings/utils/l;-><init>(Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;Lcom/android/settingslib/drawer/h;Landroid/widget/Spinner;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_0
    return-void
.end method
