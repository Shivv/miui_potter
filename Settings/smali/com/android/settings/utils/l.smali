.class final Lcom/android/settings/utils/l;
.super Ljava/lang/Object;
.source "ProfileSettingsPreferenceFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic aMw:Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;

.field final synthetic aMx:Lcom/android/settingslib/drawer/h;

.field final synthetic aMy:Landroid/widget/Spinner;


# direct methods
.method constructor <init>(Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;Lcom/android/settingslib/drawer/h;Landroid/widget/Spinner;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/utils/l;->aMw:Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;

    iput-object p2, p0, Lcom/android/settings/utils/l;->aMx:Lcom/android/settingslib/drawer/h;

    iput-object p3, p0, Lcom/android/settings/utils/l;->aMy:Landroid/widget/Spinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/utils/l;->aMx:Lcom/android/settingslib/drawer/h;

    invoke-virtual {v0, p3}, Lcom/android/settingslib/drawer/h;->cep(I)Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-eq v1, v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/utils/l;->aMw:Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;

    invoke-virtual {v2}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->ayh()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/utils/l;->aMw:Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;

    invoke-virtual {v2}, Lcom/android/settings/utils/ProfileSettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/app/Activity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    iget-object v0, p0, Lcom/android/settings/utils/l;->aMy:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
