.class public Lcom/android/settings/enterprise/h;
.super Lcom/android/settings/core/b;
.source "CaCertsPreferenceController.java"


# instance fields
.field private final aaP:Lcom/android/settings/enterprise/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIl(Landroid/content/Context;)Lcom/android/settings/enterprise/v;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/h;->aaP:Lcom/android/settings/enterprise/v;

    return-void
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/enterprise/h;->aaP:Lcom/android/settings/enterprise/v;

    invoke-interface {v0}, Lcom/android/settings/enterprise/v;->Rf()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/enterprise/h;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f100013

    invoke-virtual {v1, v3, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "ca_certs"

    return-object v0
.end method

.method public p()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/enterprise/h;->aaP:Lcom/android/settings/enterprise/v;

    invoke-interface {v1}, Lcom/android/settings/enterprise/v;->Rf()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/h;->ake(Z)V

    return v0
.end method
