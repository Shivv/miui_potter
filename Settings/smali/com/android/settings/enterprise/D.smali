.class public Lcom/android/settings/enterprise/D;
.super Lcom/android/settings/core/c;
.source "EnterpriseSetDefaultAppsListPreferenceController.java"


# static fields
.field private static final synthetic abv:[I


# instance fields
.field private final abo:Lcom/android/settings/applications/ApplicationFeatureProvider;

.field private abp:Ljava/util/List;

.field private final abq:Lcom/android/settings/enterprise/v;

.field private final abr:Lcom/android/settings/SettingsPreferenceFragment;

.field private final abs:Landroid/content/pm/PackageManager;

.field private final abt:Lcom/android/settings/users/UserFeatureProvider;

.field private abu:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/SettingsPreferenceFragment;Landroid/content/pm/PackageManager;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/D;->abu:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/D;->abp:Ljava/util/List;

    iput-object p3, p0, Lcom/android/settings/enterprise/D;->abs:Landroid/content/pm/PackageManager;

    iput-object p2, p0, Lcom/android/settings/enterprise/D;->abr:Lcom/android/settings/SettingsPreferenceFragment;

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIp(Landroid/content/Context;)Lcom/android/settings/applications/ApplicationFeatureProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/enterprise/D;->abo:Lcom/android/settings/applications/ApplicationFeatureProvider;

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIl(Landroid/content/Context;)Lcom/android/settings/enterprise/v;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/enterprise/D;->abq:Lcom/android/settings/enterprise/v;

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIx(Landroid/content/Context;)Lcom/android/settings/users/UserFeatureProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/D;->abt:Lcom/android/settings/users/UserFeatureProvider;

    invoke-direct {p0}, Lcom/android/settings/enterprise/D;->RL()V

    return-void
.end method

.method private RL()V
    .locals 12

    const/4 v5, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/enterprise/D;->abu:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/enterprise/D;->abp:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/enterprise/D;->abt:Lcom/android/settings/users/UserFeatureProvider;

    invoke-interface {v0}, Lcom/android/settings/users/UserFeatureProvider;->aQc()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserHandle;

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/settings/applications/EnterpriseDefaultApps;->values()[Lcom/android/settings/applications/EnterpriseDefaultApps;

    move-result-object v7

    array-length v8, v7

    move v4, v5

    move v2, v5

    :goto_0
    if-ge v4, v8, :cond_0

    aget-object v9, v7, v4

    iget-object v3, p0, Lcom/android/settings/enterprise/D;->abo:Lcom/android/settings/applications/ApplicationFeatureProvider;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v10

    invoke-virtual {v9}, Lcom/android/settings/applications/EnterpriseDefaultApps;->rZ()[Landroid/content/Intent;

    move-result-object v11

    invoke-interface {v3, v10, v11}, Lcom/android/settings/applications/ApplicationFeatureProvider;->xu(I[Landroid/content/Intent;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_1
    if-nez v2, :cond_4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/settings/enterprise/D;->abu:Ljava/util/List;

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/applications/UserAppInfo;

    iget-object v1, v1, Lcom/android/settings/applications/UserAppInfo;->DC:Landroid/content/pm/UserInfo;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/EnumMap;

    const-class v3, Lcom/android/settings/applications/EnterpriseDefaultApps;

    invoke-direct {v1, v3}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/android/settings/enterprise/D;->abp:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v3, v2

    move-object v2, v1

    :goto_2
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/applications/UserAppInfo;

    iget-object v1, v1, Lcom/android/settings/applications/UserAppInfo;->DB:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_2
    invoke-virtual {v2, v9, v11}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v2

    move v2, v3

    goto :goto_1

    :cond_3
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/enterprise/D;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/android/settings/enterprise/-$Lambda$YchHaoXW84ElXi6aTS9i9wX0_vs;

    invoke-direct {v1, p0}, Lcom/android/settings/enterprise/-$Lambda$YchHaoXW84ElXi6aTS9i9wX0_vs;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_4
    move v3, v2

    move-object v2, v1

    goto :goto_2
.end method

.method private RM(Landroid/content/Context;Ljava/util/List;)Ljava/lang/CharSequence;
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    move v1, v2

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v4, p0, Lcom/android/settings/enterprise/D;->abs:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_1

    aget-object v0, v3, v2

    return-object v0

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v6, :cond_2

    new-array v0, v6, [Ljava/lang/Object;

    aget-object v1, v3, v2

    aput-object v1, v0, v2

    aget-object v1, v3, v5

    aput-object v1, v0, v5

    const v1, 0x7f12013d

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    aget-object v1, v3, v2

    aput-object v1, v0, v2

    aget-object v1, v3, v5

    aput-object v1, v0, v5

    aget-object v1, v3, v6

    aput-object v1, v0, v6

    const v1, 0x7f12013e

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private RN(Landroid/content/Context;Landroid/support/v7/preference/PreferenceGroup;Ljava/util/EnumMap;)V
    .locals 8

    const/4 v2, 0x0

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/settings/applications/EnterpriseDefaultApps;->values()[Lcom/android/settings/applications/EnterpriseDefaultApps;

    move-result-object v3

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    invoke-virtual {p3, v5}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    new-instance v6, Landroid/support/v7/preference/Preference;

    invoke-direct {v6, p1}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {p0, p1, v5, v7}, Lcom/android/settings/enterprise/D;->RO(Landroid/content/Context;Lcom/android/settings/applications/EnterpriseDefaultApps;I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/v7/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1, v0}, Lcom/android/settings/enterprise/D;->RM(Landroid/content/Context;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v5}, Lcom/android/settings/applications/EnterpriseDefaultApps;->ordinal()I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/support/v7/preference/Preference;->setOrder(I)V

    invoke-virtual {v6, v2}, Landroid/support/v7/preference/Preference;->boV(Z)V

    invoke-virtual {p2, v6}, Landroid/support/v7/preference/PreferenceGroup;->im(Landroid/support/v7/preference/Preference;)Z

    goto :goto_1

    :cond_3
    return-void
.end method

.method private RO(Landroid/content/Context;Lcom/android/settings/applications/EnterpriseDefaultApps;I)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/android/settings/enterprise/D;->RQ()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/android/settings/applications/EnterpriseDefaultApps;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown type of default "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x7f120554

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    const v0, 0x7f120556

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_2
    const v0, 0x7f120557

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f10000f

    invoke-virtual {v0, v1, p3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_4
    const v0, 0x7f12055d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f10000e

    invoke-virtual {v0, v1, p3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f10000d

    invoke-virtual {v0, v1, p3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_6
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private static synthetic RQ()[I
    .locals 3

    sget-object v0, Lcom/android/settings/enterprise/D;->abv:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/settings/enterprise/D;->abv:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/settings/applications/EnterpriseDefaultApps;->values()[Lcom/android/settings/applications/EnterpriseDefaultApps;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/settings/applications/EnterpriseDefaultApps;->tS:Lcom/android/settings/applications/EnterpriseDefaultApps;

    invoke-virtual {v1}, Lcom/android/settings/applications/EnterpriseDefaultApps;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/settings/applications/EnterpriseDefaultApps;->tT:Lcom/android/settings/applications/EnterpriseDefaultApps;

    invoke-virtual {v1}, Lcom/android/settings/applications/EnterpriseDefaultApps;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/settings/applications/EnterpriseDefaultApps;->tU:Lcom/android/settings/applications/EnterpriseDefaultApps;

    invoke-virtual {v1}, Lcom/android/settings/applications/EnterpriseDefaultApps;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/settings/applications/EnterpriseDefaultApps;->tV:Lcom/android/settings/applications/EnterpriseDefaultApps;

    invoke-virtual {v1}, Lcom/android/settings/applications/EnterpriseDefaultApps;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/settings/applications/EnterpriseDefaultApps;->tW:Lcom/android/settings/applications/EnterpriseDefaultApps;

    invoke-virtual {v1}, Lcom/android/settings/applications/EnterpriseDefaultApps;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/settings/applications/EnterpriseDefaultApps;->tX:Lcom/android/settings/applications/EnterpriseDefaultApps;

    invoke-virtual {v1}, Lcom/android/settings/applications/EnterpriseDefaultApps;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/settings/applications/EnterpriseDefaultApps;->tY:Lcom/android/settings/applications/EnterpriseDefaultApps;

    invoke-virtual {v1}, Lcom/android/settings/applications/EnterpriseDefaultApps;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    sput-object v0, Lcom/android/settings/enterprise/D;->abv:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1

    :catch_6
    move-exception v1

    goto :goto_0
.end method

.method private updateUi()V
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/enterprise/D;->abr:Lcom/android/settings/SettingsPreferenceFragment;

    invoke-virtual {v1}, Lcom/android/settings/SettingsPreferenceFragment;->dju()Landroid/support/v7/preference/k;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/preference/k;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/android/settings/enterprise/D;->abr:Lcom/android/settings/SettingsPreferenceFragment;

    invoke-virtual {v1}, Lcom/android/settings/SettingsPreferenceFragment;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/enterprise/D;->abq:Lcom/android/settings/enterprise/v;

    invoke-interface {v1}, Lcom/android/settings/enterprise/v;->Rk()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/enterprise/D;->abu:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/android/settings/enterprise/D;->abp:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumMap;

    invoke-direct {p0, v2, v3, v0}, Lcom/android/settings/enterprise/D;->RN(Landroid/content/Context;Landroid/support/v7/preference/PreferenceGroup;Ljava/util/EnumMap;)V

    :cond_1
    return-void

    :cond_2
    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/enterprise/D;->abu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/enterprise/D;->abu:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    new-instance v4, Landroid/support/v7/preference/PreferenceCategory;

    invoke-direct {v4, v2}, Landroid/support/v7/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/support/v7/preference/PreferenceScreen;->im(Landroid/support/v7/preference/Preference;)Z

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f120a16

    invoke-virtual {v4, v0}, Landroid/support/v7/preference/PreferenceCategory;->setTitle(I)V

    :goto_1
    invoke-virtual {v4, v1}, Landroid/support/v7/preference/PreferenceCategory;->setOrder(I)V

    iget-object v0, p0, Lcom/android/settings/enterprise/D;->abp:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumMap;

    invoke-direct {p0, v2, v4, v0}, Lcom/android/settings/enterprise/D;->RN(Landroid/content/Context;Landroid/support/v7/preference/PreferenceGroup;Ljava/util/EnumMap;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    const v0, 0x7f120c91

    invoke-virtual {v4, v0}, Landroid/support/v7/preference/PreferenceCategory;->setTitle(I)V

    goto :goto_1
.end method


# virtual methods
.method synthetic RP()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/enterprise/D;->updateUi()V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
