.class public Lcom/android/settings/enterprise/EnterprisePrivacySettings;
.super Lcom/android/settings/dashboard/MiuiDashboardFragment;
.source "EnterprisePrivacySettings.java"


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/enterprise/E;

    invoke-direct {v0}, Lcom/android/settings/enterprise/E;-><init>()V

    sput-object v0, Lcom/android/settings/enterprise/EnterprisePrivacySettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/MiuiDashboardFragment;-><init>()V

    return-void
.end method

.method public static RG(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/overlay/a;->aIl(Landroid/content/Context;)Lcom/android/settings/enterprise/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/enterprise/v;->Rg()Z

    move-result v0

    return v0
.end method

.method private static RH(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z)Ljava/util/List;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/android/settings/enterprise/n;

    invoke-direct {v1, p0}, Lcom/android/settings/enterprise/n;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/enterprise/m;

    invoke-direct {v1, p0}, Lcom/android/settings/enterprise/m;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/enterprise/y;

    invoke-direct {v1, p0}, Lcom/android/settings/enterprise/y;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/android/settings/enterprise/c;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/settings/enterprise/c;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/enterprise/C;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/settings/enterprise/C;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/enterprise/e;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/settings/enterprise/e;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/enterprise/d;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/settings/enterprise/d;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/enterprise/f;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/enterprise/f;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/enterprise/p;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/enterprise/p;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/enterprise/a;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/enterprise/a;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/enterprise/B;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/enterprise/B;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/enterprise/w;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/enterprise/w;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/settings/enterprise/h;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/enterprise/h;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance v2, Lcom/android/settings/enterprise/x;

    invoke-direct {v2, p0, p1, v1, p2}, Lcom/android/settings/enterprise/x;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/util/List;Z)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/enterprise/A;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/enterprise/A;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/enterprise/g;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/enterprise/g;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method static synthetic RI(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z)Ljava/util/List;
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/android/settings/enterprise/EnterprisePrivacySettings;->RH(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected ar()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "EnterprisePrivacySettings"

    return-object v0
.end method

.method protected as()I
    .locals 1

    const v0, 0x7f150056

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x274

    return v0
.end method

.method protected getPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/enterprise/EnterprisePrivacySettings;->getLifecycle()Lcom/android/settings/core/lifecycle/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/android/settings/enterprise/EnterprisePrivacySettings;->RH(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
