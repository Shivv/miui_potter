.class public abstract Lcom/android/settings/enterprise/l;
.super Lcom/android/settings/core/b;
.source "AdminGrantedPermissionsPreferenceControllerBase.java"


# instance fields
.field private final aaX:Z

.field private final aaY:Lcom/android/settings/applications/ApplicationFeatureProvider;

.field private aaZ:Z

.field private final aba:Ljava/lang/String;

.field private final abb:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    iput-object p4, p0, Lcom/android/settings/enterprise/l;->abb:[Ljava/lang/String;

    iput-object p5, p0, Lcom/android/settings/enterprise/l;->aba:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIp(Landroid/content/Context;)Lcom/android/settings/applications/ApplicationFeatureProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/l;->aaY:Lcom/android/settings/applications/ApplicationFeatureProvider;

    iput-boolean p3, p0, Lcom/android/settings/enterprise/l;->aaX:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/enterprise/l;->aaZ:Z

    return-void
.end method

.method static synthetic Rp([Ljava/lang/Boolean;I)V
    .locals 2

    const/4 v1, 0x0

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, p0, v1

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method synthetic Ro(Landroid/preference/Preference;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iput-boolean v3, p0, Lcom/android/settings/enterprise/l;->aaZ:Z

    :goto_0
    iget-boolean v0, p0, Lcom/android/settings/enterprise/l;->aaZ:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/l;->ake(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/enterprise/l;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7f100016

    invoke-virtual {v0, v2, p2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iput-boolean v4, p0, Lcom/android/settings/enterprise/l;->aaZ:Z

    goto :goto_0
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/settings/enterprise/l;->aaY:Lcom/android/settings/applications/ApplicationFeatureProvider;

    iget-object v1, p0, Lcom/android/settings/enterprise/l;->abb:[Ljava/lang/String;

    new-instance v2, Lcom/android/settings/enterprise/-$Lambda$s4qgqJzITk0pCVy-czEKIzxmzsg$1;

    invoke-direct {v2, v3, p0, p1}, Lcom/android/settings/enterprise/-$Lambda$s4qgqJzITk0pCVy-czEKIzxmzsg$1;-><init>(BLjava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1, v3, v2}, Lcom/android/settings/applications/ApplicationFeatureProvider;->xs([Ljava/lang/String;ZLcom/android/settings/applications/ApplicationFeatureProvider$NumberOfAppsCallback;)V

    return-void
.end method

.method public fm(Landroid/preference/Preference;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/settings/enterprise/l;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return v2

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/enterprise/l;->aaZ:Z

    if-nez v0, :cond_1

    return v2

    :cond_1
    invoke-super {p0, p1}, Lcom/android/settings/core/b;->fm(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public p()Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/android/settings/enterprise/l;->aaX:Z

    if-eqz v0, :cond_0

    return v5

    :cond_0
    new-array v0, v5, [Ljava/lang/Boolean;

    const/4 v1, 0x0

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/android/settings/enterprise/l;->aaY:Lcom/android/settings/applications/ApplicationFeatureProvider;

    iget-object v2, p0, Lcom/android/settings/enterprise/l;->abb:[Ljava/lang/String;

    new-instance v3, Lcom/android/settings/enterprise/-$Lambda$s4qgqJzITk0pCVy-czEKIzxmzsg;

    invoke-direct {v3, v5, v0}, Lcom/android/settings/enterprise/-$Lambda$s4qgqJzITk0pCVy-czEKIzxmzsg;-><init>(BLjava/lang/Object;)V

    invoke-interface {v1, v2, v4, v3}, Lcom/android/settings/applications/ApplicationFeatureProvider;->xs([Ljava/lang/String;ZLcom/android/settings/applications/ApplicationFeatureProvider$NumberOfAppsCallback;)V

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/enterprise/l;->aaZ:Z

    iget-boolean v0, p0, Lcom/android/settings/enterprise/l;->aaZ:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/l;->ake(Z)V

    iget-boolean v0, p0, Lcom/android/settings/enterprise/l;->aaZ:Z

    return v0
.end method
