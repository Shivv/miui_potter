.class public Lcom/android/settings/enterprise/w;
.super Lcom/android/settings/core/b;
.source "GlobalHttpProxyPreferenceController.java"


# instance fields
.field private final abi:Lcom/android/settings/enterprise/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIl(Landroid/content/Context;)Lcom/android/settings/enterprise/v;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/w;->abi:Lcom/android/settings/enterprise/v;

    return-void
.end method


# virtual methods
.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "global_http_proxy"

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/w;->abi:Lcom/android/settings/enterprise/v;

    invoke-interface {v0}, Lcom/android/settings/enterprise/v;->Rj()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/w;->ake(Z)V

    return v0
.end method
