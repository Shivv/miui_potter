.class public Lcom/android/settings/enterprise/x;
.super Lcom/android/settings/core/b;
.source "ExposureChangesCategoryPreferenceController.java"

# interfaces
.implements Lcom/android/settings/core/a;


# instance fields
.field private final abj:Ljava/util/Set;

.field private abk:Z

.field private abl:Landroid/preference/Preference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Ljava/util/List;Z)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/enterprise/x;->abj:Ljava/util/Set;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/enterprise/x;->abl:Landroid/preference/Preference;

    iput-boolean p4, p0, Lcom/android/settings/enterprise/x;->abk:Z

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/core/b;

    invoke-virtual {v0, p0}, Lcom/android/settings/core/b;->akf(Lcom/android/settings/core/a;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private RJ()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/enterprise/x;->abj:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public RK(Ljava/lang/String;Z)V
    .locals 2

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/settings/enterprise/x;->abj:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/enterprise/x;->RJ()Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/settings/enterprise/x;->abk:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/x;->ake(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/enterprise/x;->abl:Landroid/preference/Preference;

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/enterprise/x;->abj:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/enterprise/x;->abl:Landroid/preference/Preference;

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "exposure_changes_category"

    return-object v0
.end method

.method public p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/enterprise/x;->abk:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/enterprise/x;->RJ()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/x;->ake(Z)V

    return v0
.end method
