.class public Lcom/android/settings/enterprise/n;
.super Lcom/android/settings/enterprise/k;
.source "NetworkLogsPreferenceController.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/enterprise/k;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected Rn()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/n;->aaW:Lcom/android/settings/enterprise/v;

    invoke-interface {v0}, Lcom/android/settings/enterprise/v;->Rb()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "network_logs"

    return-object v0
.end method

.method public p()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/settings/enterprise/n;->aaW:Lcom/android/settings/enterprise/v;

    invoke-interface {v1}, Lcom/android/settings/enterprise/v;->Rl()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/enterprise/n;->aaW:Lcom/android/settings/enterprise/v;

    invoke-interface {v1}, Lcom/android/settings/enterprise/v;->Rb()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
