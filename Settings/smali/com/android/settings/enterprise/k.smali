.class public abstract Lcom/android/settings/enterprise/k;
.super Lcom/android/settings/core/e;
.source "AdminActionPreferenceControllerBase.java"


# instance fields
.field protected final aaW:Lcom/android/settings/enterprise/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/core/e;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIl(Landroid/content/Context;)Lcom/android/settings/enterprise/v;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/k;->aaW:Lcom/android/settings/enterprise/v;

    return-void
.end method


# virtual methods
.method protected abstract Rn()Ljava/util/Date;
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/enterprise/k;->Rn()Ljava/util/Date;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/enterprise/k;->mContext:Landroid/content/Context;

    const v1, 0x7f1206b5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/enterprise/k;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const/16 v0, 0x11

    invoke-static {v1, v2, v3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
