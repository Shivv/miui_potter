.class public Lcom/android/settings/enterprise/c;
.super Lcom/android/settings/core/b;
.source "EnterpriseInstalledPackagesPreferenceController.java"


# instance fields
.field private final aaL:Z

.field private final aaM:Lcom/android/settings/applications/ApplicationFeatureProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;Z)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIp(Landroid/content/Context;)Lcom/android/settings/applications/ApplicationFeatureProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/c;->aaM:Lcom/android/settings/applications/ApplicationFeatureProvider;

    iput-boolean p3, p0, Lcom/android/settings/enterprise/c;->aaL:Z

    return-void
.end method

.method static synthetic QU([Ljava/lang/Boolean;I)V
    .locals 2

    const/4 v1, 0x0

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, p0, v1

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method synthetic QT(Landroid/preference/Preference;I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p2, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/c;->ake(Z)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/settings/enterprise/c;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const v0, 0x7f100016

    invoke-virtual {v2, v0, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    move v0, v1

    goto :goto_0
.end method

.method public cz(Landroid/preference/Preference;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/enterprise/c;->aaM:Lcom/android/settings/applications/ApplicationFeatureProvider;

    new-instance v1, Lcom/android/settings/enterprise/-$Lambda$s4qgqJzITk0pCVy-czEKIzxmzsg$1;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p0, p1}, Lcom/android/settings/enterprise/-$Lambda$s4qgqJzITk0pCVy-czEKIzxmzsg$1;-><init>(BLjava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x1

    invoke-interface {v0, v2, v1}, Lcom/android/settings/applications/ApplicationFeatureProvider;->xt(ZLcom/android/settings/applications/ApplicationFeatureProvider$NumberOfAppsCallback;)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "number_enterprise_installed_packages"

    return-object v0
.end method

.method public p()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/android/settings/enterprise/c;->aaL:Z

    if-eqz v0, :cond_0

    return v1

    :cond_0
    new-array v0, v1, [Ljava/lang/Boolean;

    const/4 v1, 0x0

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/android/settings/enterprise/c;->aaM:Lcom/android/settings/applications/ApplicationFeatureProvider;

    new-instance v2, Lcom/android/settings/enterprise/-$Lambda$s4qgqJzITk0pCVy-czEKIzxmzsg;

    invoke-direct {v2, v3, v0}, Lcom/android/settings/enterprise/-$Lambda$s4qgqJzITk0pCVy-czEKIzxmzsg;-><init>(BLjava/lang/Object;)V

    invoke-interface {v1, v3, v2}, Lcom/android/settings/applications/ApplicationFeatureProvider;->xt(ZLcom/android/settings/applications/ApplicationFeatureProvider$NumberOfAppsCallback;)V

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/c;->ake(Z)V

    return v0
.end method
