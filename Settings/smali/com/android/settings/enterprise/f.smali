.class public Lcom/android/settings/enterprise/f;
.super Lcom/android/settings/core/b;
.source "EnterpriseSetDefaultAppsPreferenceController.java"


# instance fields
.field private final aaN:Lcom/android/settings/applications/ApplicationFeatureProvider;

.field private final aaO:Lcom/android/settings/users/UserFeatureProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/b;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    invoke-static {p1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIp(Landroid/content/Context;)Lcom/android/settings/applications/ApplicationFeatureProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/enterprise/f;->aaN:Lcom/android/settings/applications/ApplicationFeatureProvider;

    invoke-virtual {v0, p1}, Lcom/android/settings/overlay/a;->aIx(Landroid/content/Context;)Lcom/android/settings/users/UserFeatureProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/enterprise/f;->aaO:Lcom/android/settings/users/UserFeatureProvider;

    return-void
.end method

.method private QV()I
    .locals 10

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/enterprise/f;->aaO:Lcom/android/settings/users/UserFeatureProvider;

    invoke-interface {v0}, Lcom/android/settings/users/UserFeatureProvider;->aQc()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserHandle;

    invoke-static {}, Lcom/android/settings/applications/EnterpriseDefaultApps;->values()[Lcom/android/settings/applications/EnterpriseDefaultApps;

    move-result-object v6

    array-length v7, v6

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_0

    aget-object v4, v6, v3

    iget-object v8, p0, Lcom/android/settings/enterprise/f;->aaN:Lcom/android/settings/applications/ApplicationFeatureProvider;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v9

    invoke-virtual {v4}, Lcom/android/settings/applications/EnterpriseDefaultApps;->rZ()[Landroid/content/Intent;

    move-result-object v4

    invoke-interface {v8, v9, v4}, Lcom/android/settings/applications/ApplicationFeatureProvider;->xu(I[Landroid/content/Intent;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v4, v1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v4

    goto :goto_0

    :cond_1
    return v1
.end method


# virtual methods
.method public cz(Landroid/preference/Preference;)V
    .locals 5

    invoke-direct {p0}, Lcom/android/settings/enterprise/f;->QV()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/enterprise/f;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f100015

    invoke-virtual {v1, v3, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "number_enterprise_set_default_apps"

    return-object v0
.end method

.method public p()Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/settings/enterprise/f;->QV()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/f;->ake(Z)V

    return v0
.end method
