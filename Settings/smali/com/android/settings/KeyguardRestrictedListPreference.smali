.class public Lcom/android/settings/KeyguardRestrictedListPreference;
.super Landroid/preference/ListPreference;
.source "KeyguardRestrictedListPreference.java"


# instance fields
.field private final bXC:Lcom/android/settingslib/o;

.field private final bXD:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXD:Ljava/util/List;

    const v0, 0x7f0d0190

    invoke-virtual {p0, v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->setWidgetLayoutResource(I)V

    new-instance v0, Lcom/android/settingslib/o;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/o;-><init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXC:Lcom/android/settingslib/o;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXD:Ljava/util/List;

    new-instance v0, Lcom/android/settingslib/o;

    invoke-direct {v0, p1, p0, p2}, Lcom/android/settingslib/o;-><init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXC:Lcom/android/settingslib/o;

    return-void
.end method

.method static synthetic bRB(Lcom/android/settings/KeyguardRestrictedListPreference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/KeyguardRestrictedListPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic bRC(Lcom/android/settings/KeyguardRestrictedListPreference;Ljava/lang/CharSequence;)Lcom/android/settings/cp;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRx(Ljava/lang/CharSequence;)Lcom/android/settings/cp;

    move-result-object v0

    return-object v0
.end method

.method private bRx(Ljava/lang/CharSequence;)Lcom/android/settings/cp;
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_0

    return-object v3

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXD:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cp;

    iget-object v2, v0, Lcom/android/settings/cp;->bXI:Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v0

    :cond_2
    return-object v3
.end method


# virtual methods
.method public bRA(Ljava/lang/CharSequence;)Z
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXD:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cp;

    iget-object v0, v0, Lcom/android/settings/cp;->bXH:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    return v2
.end method

.method public bRu()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public bRv(Lcom/android/settings/cp;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXD:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected bRw()Landroid/widget/ListAdapter;
    .locals 4

    new-instance v0, Lcom/android/settings/co;

    invoke-virtual {p0}, Lcom/android/settings/KeyguardRestrictedListPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/KeyguardRestrictedListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRy()I

    move-result v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/settings/co;-><init>(Lcom/android/settings/KeyguardRestrictedListPreference;Landroid/content/Context;[Ljava/lang/CharSequence;I)V

    return-object v0
.end method

.method public bRy()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/KeyguardRestrictedListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bRz()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXC:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->cqj()Z

    move-result v0

    return v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXC:Lcom/android/settingslib/o;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/o;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a038d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRz()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {p0}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRw()Landroid/widget/ListAdapter;

    move-result-object v0

    new-instance v1, Lcom/android/settings/kh;

    invoke-direct {v1, p0}, Lcom/android/settings/kh;-><init>(Lcom/android/settings/KeyguardRestrictedListPreference;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method public performClick(Landroid/preference/PreferenceScreen;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXC:Lcom/android/settingslib/o;

    invoke-virtual {v0}, Lcom/android/settingslib/o;->performClick()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/preference/ListPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRz()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/KeyguardRestrictedListPreference;->bXC:Lcom/android/settingslib/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/o;->setDisabledByAdmin(Lcom/android/settingslib/n;)Z

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->setEnabled(Z)V

    return-void
.end method
