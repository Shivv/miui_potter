.class public Lcom/android/settings/co;
.super Landroid/widget/ArrayAdapter;
.source "KeyguardRestrictedListPreference.java"


# instance fields
.field private final bXE:I

.field final synthetic bXF:Lcom/android/settings/KeyguardRestrictedListPreference;


# direct methods
.method public constructor <init>(Lcom/android/settings/KeyguardRestrictedListPreference;Landroid/content/Context;[Ljava/lang/CharSequence;I)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/co;->bXF:Lcom/android/settings/KeyguardRestrictedListPreference;

    const v0, 0x7f0d00d9

    const v1, 0x7f0a0486

    invoke-direct {p0, p2, v0, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    iput p4, p0, Lcom/android/settings/co;->bXE:I

    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, p1}, Lcom/android/settings/co;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const v1, 0x7f0a0486

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    const v2, 0x7f0a038e

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/android/settings/co;->bXF:Lcom/android/settings/KeyguardRestrictedListPreference;

    invoke-virtual {v6, v0}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRA(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v4}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    invoke-virtual {v1, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-object v5

    :cond_0
    iget v0, p0, Lcom/android/settings/co;->bXE:I

    const/4 v6, -0x1

    if-eq v0, v6, :cond_1

    iget v0, p0, Lcom/android/settings/co;->bXE:I

    if-ne p1, v0, :cond_3

    move v0, v3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    :cond_1
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1, v3}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v4

    goto :goto_1
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
