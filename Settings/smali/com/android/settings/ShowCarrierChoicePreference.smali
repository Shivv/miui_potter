.class public Lcom/android/settings/ShowCarrierChoicePreference;
.super Lcom/android/settings/MiuiListPreference;
.source "ShowCarrierChoicePreference.java"


# instance fields
.field private bDr:[Ljava/lang/String;

.field private bDs:Lcom/android/settings/aw;

.field private bDt:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/ShowCarrierChoicePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const v1, 0x7f0300f8

    const v2, 0x7f0300f7

    invoke-direct {p0, p1, p2}, Lcom/android/settings/MiuiListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/settings/aw;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/aw;-><init>(Lcom/android/settings/ShowCarrierChoicePreference;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/ShowCarrierChoicePreference;->bDs:Lcom/android/settings/aw;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-boolean v3, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-eqz v3, :cond_0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/android/settings/ShowCarrierChoicePreference;->bDr:[Ljava/lang/String;

    sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/settings/ShowCarrierChoicePreference;->setEntries(I)V

    sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0300fa

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/settings/ShowCarrierChoicePreference;->setEntryValues(I)V

    return-void

    :cond_0
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    const v0, 0x7f0300f9

    goto :goto_2
.end method

.method static synthetic btx(Lcom/android/settings/ShowCarrierChoicePreference;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ShowCarrierChoicePreference;->bDr:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bty(Lcom/android/settings/ShowCarrierChoicePreference;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/ShowCarrierChoicePreference;->bDt:I

    return p1
.end method

.method static synthetic btz(Lcom/android/settings/ShowCarrierChoicePreference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/ShowCarrierChoicePreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/android/settings/ShowCarrierChoicePreference;->bDs:Lcom/android/settings/aw;

    iget v1, p0, Lcom/android/settings/ShowCarrierChoicePreference;->bDt:I

    new-instance v2, Lcom/android/settings/fE;

    invoke-direct {v2, p0}, Lcom/android/settings/fE;-><init>(Lcom/android/settings/ShowCarrierChoicePreference;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v0, 0x1040000

    invoke-virtual {p1, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method public setValueIndex(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiListPreference;->setValueIndex(I)V

    iput p1, p0, Lcom/android/settings/ShowCarrierChoicePreference;->bDt:I

    return-void
.end method
