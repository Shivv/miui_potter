.class public Lcom/android/settings/bN;
.super Ljava/lang/Object;
.source "MiuiDeviceNameEditFragment.java"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field private bTZ:Lcom/android/settings/bO;

.field private bUa:I


# direct methods
.method public constructor <init>(Lcom/android/settings/bO;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/bN;->bTZ:Lcom/android/settings/bO;

    iput p2, p0, Lcom/android/settings/bN;->bUa:I

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/bN;->bTZ:Lcom/android/settings/bO;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez p5, :cond_0

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-interface {v2, v0}, Lcom/android/settings/bO;->ajq(Z)V

    iget v0, p0, Lcom/android/settings/bN;->bUa:I

    invoke-interface {p4}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    sub-int/2addr v0, v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_1

    const/4 v0, 0x0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bN;->bTZ:Lcom/android/settings/bO;

    invoke-interface {v0}, Lcom/android/settings/bO;->ajp()V

    const-string/jumbo v0, ""

    return-object v0
.end method
