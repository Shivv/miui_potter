.class public Lcom/android/settings/MiuiTetherSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiTetherSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/datausage/DataSaverBackend$Listener;


# instance fields
.field private bQD:Landroid/net/ConnectivityManager;

.field private bQE:Landroid/preference/Preference;

.field private bQF:Lcom/android/settings/datausage/DataSaverBackend;

.field private bQG:Z

.field private bQH:Landroid/preference/Preference;

.field private bQI:Landroid/preference/PreferenceCategory;

.field private bQJ:Lcom/android/settings/wifi/B;

.field private bQK:Landroid/preference/CheckBoxPreference;

.field private bQL:Landroid/content/IntentFilter;

.field private bQM:[Ljava/lang/String;

.field private bQN:Z

.field private bQO:[Ljava/lang/String;

.field private bQP:Landroid/preference/Preference;

.field private bQQ:Lmiui/preference/ValuePreference;

.field private bQR:Landroid/preference/CheckBoxPreference;

.field private bQS:Lmiui/preference/ValuePreference;

.field private bQT:Landroid/os/UserManager;

.field private bQU:Z

.field private bQV:Lcom/android/settings/wifi/m;

.field private bQW:Landroid/net/wifi/WifiConfiguration;

.field private bQX:Landroid/net/wifi/WifiManager;

.field private bQY:[Ljava/lang/String;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    new-instance v0, Lcom/android/settings/iQ;

    invoke-direct {v0, p0}, Lcom/android/settings/iQ;-><init>(Lcom/android/settings/MiuiTetherSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private bJd()V
    .locals 7

    const v4, 0x7f12161a

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQX:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQX:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f030120

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQO:[Ljava/lang/String;

    iput-boolean v5, p0, Lcom/android/settings/MiuiTetherSettings;->bQN:Z

    const-string/jumbo v0, "wifi_ap_ssid_and_security"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQE:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    if-nez v0, :cond_0

    const v0, 0x1108004a

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQE:Landroid/preference/Preference;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQO:[Ljava/lang/String;

    aget-object v0, v0, v5

    aput-object v0, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x10406d5

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-eqz v0, :cond_2

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    :goto_1
    iput-object v0, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->bJh()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-static {v0}, Lcom/android/settings/wifi/B;->afI(Landroid/net/wifi/WifiConfiguration;)I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQE:Landroid/preference/Preference;

    invoke-virtual {v1, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    iget-object v4, v4, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/android/settings/MiuiTetherSettings;->bQO:[Ljava/lang/String;

    aget-object v0, v4, v0

    aput-object v0, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-static {v1}, Landroid/provider/MiuiSettings$System;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private bJe()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-static {v0}, Lcom/android/settings/wifi/B;->afI(Landroid/net/wifi/WifiConfiguration;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQO:[Ljava/lang/String;

    aget-object v0, v1, v0

    const v1, 0x7f1215a7

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiTetherSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private bJf(Landroid/content/Context;)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQD:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v2

    invoke-static {p1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-boolean v3, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v3, :cond_1

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private bJg(Z)V
    .locals 6

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dc;->bsd(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQQ:Lmiui/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const v4, 0x7f10000b

    invoke-virtual {v2, v4, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQQ:Lmiui/preference/ValuePreference;

    const v1, 0x7f121259

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiTetherSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private bJi(Landroid/content/Context;)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f120674

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/iR;

    invoke-direct {v1, p0}, Lcom/android/settings/iR;-><init>(Lcom/android/settings/MiuiTetherSettings;)V

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/iS;

    invoke-direct {v1, p0}, Lcom/android/settings/iS;-><init>(Lcom/android/settings/MiuiTetherSettings;)V

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private bJj()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQP:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQK:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/MiuiTetherSettings;->bJe()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQP:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQP:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private bJk()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-static {v0, v1}, Lcom/android/settings/wifi/F;->agG(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0d027d

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a0376

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v3, Lmiui/R$style;->Theme_Light_Dialog_Alert:I

    invoke-direct {v0, v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f1215f4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1215f2

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private bJl(I)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settingslib/l;->cpP(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQM:[Ljava/lang/String;

    aget-object v1, v1, v4

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQM:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0, v4}, Lcom/android/settings/MiuiTetherSettings;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/MiuiTetherSettings;->bJm(I)V

    goto :goto_0
.end method

.method private bJm(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQD:Landroid/net/ConnectivityManager;

    new-instance v1, Lcom/android/settings/iT;

    invoke-direct {v1, p0}, Lcom/android/settings/iT;-><init>(Lcom/android/settings/MiuiTetherSettings;)V

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Landroid/net/ConnectivityManager;->startTethering(IZLandroid/net/ConnectivityManager$OnStartTetheringCallback;)V

    return-void
.end method

.method private bJn(Z)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "tether_auto_disable"

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz p1, :cond_1

    if-eq v2, v3, :cond_1

    const-string/jumbo v0, "tether_auto_disable"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQR:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/settings/MiuiTetherSettings;->bQR:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "tether_auto_disable"

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiTetherSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQR:Landroid/preference/CheckBoxPreference;

    goto :goto_1
.end method

.method static synthetic bJo(Lcom/android/settings/MiuiTetherSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQK:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic bJp(Lcom/android/settings/MiuiTetherSettings;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQN:Z

    return v0
.end method

.method static synthetic bJq(Lcom/android/settings/MiuiTetherSettings;)Landroid/net/wifi/WifiConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    return-object v0
.end method

.method static synthetic bJr(Lcom/android/settings/MiuiTetherSettings;)Landroid/net/wifi/WifiManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQX:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic bJs(Lcom/android/settings/MiuiTetherSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiTetherSettings;->bQN:Z

    return p1
.end method

.method static synthetic bJt(Lcom/android/settings/MiuiTetherSettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiTetherSettings;->bJg(Z)V

    return-void
.end method

.method static synthetic bJu(Lcom/android/settings/MiuiTetherSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiTetherSettings;->bJj()V

    return-void
.end method

.method static synthetic bJv(Lcom/android/settings/MiuiTetherSettings;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiTetherSettings;->bJl(I)V

    return-void
.end method

.method static synthetic bJw(Lcom/android/settings/MiuiTetherSettings;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiTetherSettings;->bJm(I)V

    return-void
.end method


# virtual methods
.method public QH(ILandroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string/jumbo v0, "config"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->bJh()V

    :cond_0
    return-void
.end method

.method public aq()I
    .locals 1

    const v0, 0x7f120824

    return v0
.end method

.method public bJh()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQX:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQD:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->stopTethering(I)V

    iput-boolean v6, p0, Lcom/android/settings/MiuiTetherSettings;->bQN:Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQX:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-static {v0}, Lcom/android/settings/wifi/B;->afI(Landroid/net/wifi/WifiConfiguration;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQE:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f12161a

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    iget-object v4, v4, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/android/settings/MiuiTetherSettings;->bQO:[Ljava/lang/String;

    aget-object v0, v4, v0

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/TetherSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x5a

    return v0
.end method

.method public iA(IZ)V
    .locals 0

    return-void
.end method

.method public iy(IZ)V
    .locals 0

    return-void
.end method

.method public iz(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/settings/MiuiTetherSettings;->bQG:Z

    const-string/jumbo v0, "TetherSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set enableWifiApSwitch to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQG:Z

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQK:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQG:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p1, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/MiuiTetherSettings;->QH(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQJ:Lcom/android/settings/wifi/B;

    invoke-virtual {v0}, Lcom/android/settings/wifi/B;->afJ()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQX:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQD:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->stopTethering(I)V

    iput-boolean v6, p0, Lcom/android/settings/MiuiTetherSettings;->bQN:Z

    :goto_0
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-static {v0}, Lcom/android/settings/wifi/B;->afI(Landroid/net/wifi/WifiConfiguration;)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQE:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f12161a

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    iget-object v4, v4, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/android/settings/MiuiTetherSettings;->bQO:[Ljava/lang/String;

    aget-object v0, v4, v0

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQX:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    const v0, 0x7f150089

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "enable_wifi_ap"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQK:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "wifi_ap_ssid_and_security"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    const-string/jumbo v0, "tether_share_qrcode"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQP:Landroid/preference/Preference;

    const-string/jumbo v0, "tether_device_management"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQI:Landroid/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQI:Landroid/preference/PreferenceCategory;

    const-string/jumbo v4, "show_connected_devices"

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQQ:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQQ:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    const-string/jumbo v0, "tether_data_usage_limit"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQS:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQS:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v4, p0, Lcom/android/settings/MiuiTetherSettings;->bQS:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    new-instance v0, Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/android/settings/datausage/DataSaverBackend;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQF:Lcom/android/settings/datausage/DataSaverBackend;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQF:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v0}, Lcom/android/settings/datausage/DataSaverBackend;->kq()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQG:Z

    const-string/jumbo v0, "disabled_on_data_saver"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQH:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQF:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v0, p0}, Lcom/android/settings/datausage/DataSaverBackend;->kn(Lcom/android/settings/datausage/DataSaverBackend$Listener;)V

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQT:Landroid/os/UserManager;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQT:Landroid/os/UserManager;

    const-string/jumbo v4, "no_config_tethering"

    invoke-virtual {v0, v4}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iput-boolean v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQU:Z

    new-instance v0, Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/preference/PreferenceScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    return-void

    :cond_2
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQD:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQD:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherableWifiRegexs()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQY:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQY:[Ljava/lang/String;

    array-length v0, v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/android/settings/aq;->bqE()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/settings/MiuiTetherSettings;->bJd()V

    new-instance v0, Lcom/android/settings/wifi/m;

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQF:Lcom/android/settings/datausage/DataSaverBackend;

    iget-object v3, p0, Lcom/android/settings/MiuiTetherSettings;->bQK:Landroid/preference/CheckBoxPreference;

    invoke-direct {v0, v4, v2, v3}, Lcom/android/settings/wifi/m;-><init>(Landroid/content/Context;Lcom/android/settings/datausage/DataSaverBackend;Landroid/preference/CheckBoxPreference;)V

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQV:Lcom/android/settings/wifi/m;

    invoke-direct {p0}, Lcom/android/settings/MiuiTetherSettings;->bJj()V

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQK:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->bJg(Z)V

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQL:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQL:Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQL:Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQL:Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQL:Landroid/content/IntentFilter;

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/dc;->bse()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiTetherSettings;->bJn(Z)V

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11090005

    invoke-static {v1, v2}, Lmiui/util/ResourceMapper;->resolveReference(Landroid/content/res/Resources;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQM:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQF:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v0}, Lcom/android/settings/datausage/DataSaverBackend;->kq()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->iz(Z)V

    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQK:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQP:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQI:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-direct {p0, v2}, Lcom/android/settings/MiuiTetherSettings;->bJn(Z)V

    goto :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/android/settings/wifi/B;

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v1, v0, p0, v2}, Lcom/android/settings/wifi/B;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/net/wifi/WifiConfiguration;)V

    iput-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQJ:Lcom/android/settings/wifi/B;

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQJ:Lcom/android/settings/wifi/B;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQF:Lcom/android/settings/datausage/DataSaverBackend;

    invoke-virtual {v0, p0}, Lcom/android/settings/datausage/DataSaverBackend;->ko(Lcom/android/settings/datausage/DataSaverBackend$Listener;)V

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, Lcom/android/settings/MiuiTetherSettings;->bQR:Landroid/preference/CheckBoxPreference;

    if-ne p1, v3, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "tether_auto_disable"

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v1

    :cond_1
    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/settings/MiuiTetherSettings;->bJf(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiTetherSettings;->bJi(Landroid/content/Context;)V

    return v0

    :cond_2
    invoke-direct {p0, v0}, Lcom/android/settings/MiuiTetherSettings;->bJl(I)V

    :goto_0
    return v1

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settingslib/l;->cpP(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/settings/TetherService;->bRX(Landroid/content/Context;I)V

    :cond_4
    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQD:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v0}, Landroid/net/ConnectivityManager;->stopTethering(I)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQE:Landroid/preference/Preference;

    if-ne p2, v0, :cond_2

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "config"

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->bQW:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    const-class v0, Lcom/android/settings/wifi/EditTetherFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/MiuiTetherSettings;->bWC(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :cond_1
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQP:Landroid/preference/Preference;

    if-ne p2, v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/MiuiTetherSettings;->bJk()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStart()V

    iget-boolean v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQU:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    if-eqz v0, :cond_0

    const v1, 0x7f121266

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQV:Lcom/android/settings/wifi/m;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQK:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQR:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQV:Lcom/android/settings/wifi/m;

    invoke-virtual {v0}, Lcom/android/settings/wifi/m;->acs()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings;->bQL:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_3
    return-void
.end method

.method public onStop()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStop()V

    iget-boolean v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQU:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQV:Lcom/android/settings/wifi/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQK:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQR:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQR:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings;->bQV:Lcom/android/settings/wifi/m;

    invoke-virtual {v0}, Lcom/android/settings/wifi/m;->pause()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_2
    return-void
.end method
