.class public Lcom/android/settings/MiuiWirelessSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "MiuiWirelessSettings.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;


# instance fields
.field private bSA:Landroid/os/UserManager;

.field private bSB:Z

.field private bSC:[Ljava/lang/String;

.field private bSD:Landroid/preference/CheckBoxPreference;

.field private bSi:Lcom/android/settings/an;

.field private bSj:Landroid/preference/CheckBoxPreference;

.field private bSk:Z

.field private bSl:Ljava/util/concurrent/atomic/AtomicReference;

.field private bSm:[Ljava/lang/String;

.field private bSn:Landroid/preference/CheckBoxPreference;

.field private bSo:Landroid/net/ConnectivityManager;

.field private bSp:Ljava/util/HashSet;

.field private bSq:Ljava/lang/String;

.field private bSr:Z

.field private bSs:Landroid/nfc/NfcAdapter;

.field private bSt:Lcom/android/settings/nfc/NfcEnabler;

.field private bSu:Lcom/android/settings/nfc/NfcSeRoute;

.field private bSv:Landroid/content/pm/PackageManager;

.field private bSw:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private bSx:Landroid/content/BroadcastReceiver;

.field private bSy:I

.field private bSz:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/ji;

    invoke-direct {v0}, Lcom/android/settings/ji;-><init>()V

    sput-object v0, Lcom/android/settings/MiuiWirelessSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSp:Ljava/util/HashSet;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSl:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSy:I

    new-instance v0, Lcom/android/settings/bC;

    invoke-direct {v0, p0}, Lcom/android/settings/bC;-><init>(Lcom/android/settings/MiuiWirelessSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSw:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    return-void
.end method

.method private bLA([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 10

    const/4 v1, 0x1

    const/4 v3, 0x0

    array-length v5, p3

    move v4, v3

    move v2, v3

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v6, p3, v4

    iget-object v7, p0, Lcom/android/settings/MiuiWirelessSettings;->bSm:[Ljava/lang/String;

    array-length v8, v7

    move v0, v2

    move v2, v3

    :goto_1
    if-ge v2, v8, :cond_1

    aget-object v9, v7, v2

    invoke-virtual {v6, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    move v0, v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_3

    return-void

    :cond_3
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v4

    const/16 v0, 0xd

    if-ne v4, v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12034e

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :goto_2
    return-void

    :cond_4
    const/16 v0, 0xb

    if-ne v4, v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12034f

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothPan;

    const/16 v5, 0xc

    if-ne v4, v5, :cond_9

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothPan;->isTetheringOn()Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v4, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothPan;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_6

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v3

    const v0, 0x7f120349

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiWirelessSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_6
    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f120348

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_2

    :cond_7
    if-eqz v2, :cond_8

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12034a

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f120347

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12034b

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto/16 :goto_2
.end method

.method private bLB()V
    .locals 3

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherableIfaces()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetheringErroredIfaces()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/android/settings/MiuiWirelessSettings;->bLC([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private bLC([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/MiuiWirelessSettings;->bLD([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/MiuiWirelessSettings;->bLA([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private bLD([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 12

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iget-boolean v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSB:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSr:Z

    xor-int/lit8 v1, v1, 0x1

    :goto_0
    const/4 v5, 0x0

    const/4 v2, 0x0

    array-length v6, p1

    move v4, v2

    :goto_1
    if-ge v4, v6, :cond_3

    aget-object v7, p1, v4

    iget-object v8, p0, Lcom/android/settings/MiuiWirelessSettings;->bSC:[Ljava/lang/String;

    const/4 v2, 0x0

    array-length v9, v8

    move v3, v2

    move v2, v5

    :goto_2
    if-ge v3, v9, :cond_2

    aget-object v5, v8, v3

    invoke-virtual {v7, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez v2, :cond_0

    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->getLastTetherError(Ljava/lang/String;)I

    move-result v2

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v5, v2

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    const/4 v0, 0x0

    array-length v6, p2

    move v3, v0

    :goto_3
    if-ge v3, v6, :cond_6

    aget-object v7, p2, v3

    iget-object v8, p0, Lcom/android/settings/MiuiWirelessSettings;->bSC:[Ljava/lang/String;

    const/4 v0, 0x0

    array-length v9, v8

    move v2, v0

    move v0, v4

    :goto_4
    if-ge v2, v9, :cond_5

    aget-object v4, v8, v2

    invoke-virtual {v7, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v0, 0x1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v4, v0

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    const/4 v0, 0x0

    array-length v6, p3

    move v3, v0

    :goto_5
    if-ge v3, v6, :cond_9

    aget-object v7, p3, v3

    iget-object v8, p0, Lcom/android/settings/MiuiWirelessSettings;->bSC:[Ljava/lang/String;

    const/4 v0, 0x0

    array-length v9, v8

    move v11, v0

    move v0, v2

    move v2, v11

    :goto_6
    if-ge v2, v9, :cond_8

    aget-object v10, v8, v2

    invoke-virtual {v7, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v0, 0x1

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_8
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_5

    :cond_9
    if-eqz v4, :cond_a

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121366

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :goto_7
    return-void

    :cond_a
    if-eqz v1, :cond_d

    if-eqz v5, :cond_b

    const/16 v0, 0x10

    if-ne v5, v0, :cond_c

    :cond_b
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121367

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :goto_8
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_7

    :cond_c
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121369

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_8

    :cond_d
    if-eqz v2, :cond_e

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121369

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_7

    :cond_e
    iget-boolean v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSr:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12136a

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_7

    :cond_f
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12136c

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_7
.end method

.method static synthetic bLE(Lcom/android/settings/MiuiWirelessSettings;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSk:Z

    return v0
.end method

.method static synthetic bLF(Lcom/android/settings/MiuiWirelessSettings;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSl:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic bLG(Lcom/android/settings/MiuiWirelessSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSk:Z

    return p1
.end method

.method static synthetic bLH(Lcom/android/settings/MiuiWirelessSettings;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic bLI(Lcom/android/settings/MiuiWirelessSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSr:Z

    return p1
.end method

.method static synthetic bLJ(Lcom/android/settings/MiuiWirelessSettings;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSB:Z

    return p1
.end method

.method static synthetic bLK(Lcom/android/settings/MiuiWirelessSettings;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic bLL(Lcom/android/settings/MiuiWirelessSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiWirelessSettings;->bLB()V

    return-void
.end method

.method static synthetic bLM(Lcom/android/settings/MiuiWirelessSettings;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/MiuiWirelessSettings;->bLC([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private static bLt([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v1, 0x0

    array-length v3, p0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, p0, v2

    array-length v5, p1

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_1

    aget-object v6, p1, v0

    invoke-virtual {v4, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method private bLu()Landroid/preference/ListPreference;
    .locals 7

    const/4 v1, 0x0

    const-string/jumbo v0, "se_route"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    if-nez v0, :cond_0

    const-string/jumbo v0, "getListPreference called! seRoute:null"

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    return-object v1

    :cond_0
    const-string/jumbo v1, "ro.se.type"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v1, "getRoSeType is null from SystemProperties"

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    const-string/jumbo v1, "HCE,eSE"

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getRoSeType value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, " "

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    array-length v4, v2

    :goto_0
    if-ge v1, v4, :cond_8

    aget-object v5, v2, v1

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const-string/jumbo v6, "HCE"

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const-string/jumbo v6, "ESE"

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    const-string/jumbo v6, "UICC"

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string/jumbo v6, "UICC1"

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    :cond_6
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    const-string/jumbo v6, "UICC2"

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "after format,getRoSeType value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private bLv(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "MiuiWirelessSettings"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private bLx(Z)V
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->setUsbTethering(Z)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f121369

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    return-void

    :cond_0
    return-void
.end method

.method private bLy(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSy:I

    invoke-direct {p0}, Lcom/android/settings/MiuiWirelessSettings;->bLz()V

    return-void
.end method

.method private bLz()V
    .locals 4

    const/4 v3, 0x1

    iget v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSy:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    iput-boolean v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSk:Z

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12034f

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothPan;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/bluetooth/BluetoothPan;->setBluetoothTethering(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f120347

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v3}, Lcom/android/settings/MiuiWirelessSettings;->bLx(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected aq()I
    .locals 1

    const v0, 0x7f12081f

    return v0
.end method

.method public bLw()V
    .locals 7

    const v6, 0x7f120aae

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string/jumbo v0, "onManageMobilePlanClick:"

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSo:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiWirelessSettings;->bSz:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->hasIccCard()Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v1, :cond_6

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.ACTION_CARRIER_SETUP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/settings/MiuiWirelessSettings;->bSz:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2, v1}, Landroid/telephony/TelephonyManager;->getCarrierPackageNamesForIntent(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v4, :cond_0

    const-string/jumbo v0, "MiuiWirelessSettings"

    const-string/jumbo v3, "Multiple matching carrier apps found, launching the first."

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSo:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getMobileProvisioningUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v0, "android.intent.action.MAIN"

    const-string/jumbo v2, "android.intent.category.APP_BROWSER"

    invoke-static {v0, v2}, Landroid/content/Intent;->makeMainSelectorActivity(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v1, 0x10400000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onManageMobilePlanClick: message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/android/settings/MiuiWirelessSettings;->alW(I)V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiuiWirelessSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onManageMobilePlanClick: startActivity failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSz:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSz:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    const v1, 0x7f120aaf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    goto :goto_0

    :cond_4
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-virtual {v0, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    goto :goto_0

    :cond_5
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-virtual {v0, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSz:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->hasIccCard()Z

    move-result v1

    if-nez v1, :cond_7

    const v1, 0x7f120aad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    const v1, 0x7f120aaa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public cC()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "MiuiWirelessSettings"

    return-object v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x6e

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string/jumbo v0, "exit_ecm_result"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSi:Lcom/android/settings/an;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v2, p0, Lcom/android/settings/MiuiWirelessSettings;->bSj:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/an;->bqk(ZZ)V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "mManageMobilePlanMessage"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onCreate: mManageMobilePlanMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSo:Landroid/net/ConnectivityManager;

    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSz:Landroid/telephony/TelephonyManager;

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSv:Landroid/content/pm/PackageManager;

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSA:Landroid/os/UserManager;

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f03003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/MiuiWirelessSettings;->bSp:Ljava/util/HashSet;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const v0, 0x7f150116

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->addPreferencesFromResource(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSA:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    move v4, v0

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string/jumbo v0, "toggle_airplane"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSj:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "toggle_nfc"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "android_beam_settings"

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/MiuiRestrictedPreference;

    const-string/jumbo v2, "nfc_payment"

    invoke-virtual {p0, v2}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceScreen;

    invoke-direct {p0}, Lcom/android/settings/MiuiWirelessSettings;->bLu()Landroid/preference/ListPreference;

    move-result-object v7

    new-instance v5, Lcom/android/settings/vpn2/VpnManager;

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v5, v3}, Lcom/android/settings/vpn2/VpnManager;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/android/settings/an;

    iget-object v8, p0, Lcom/android/settings/MiuiWirelessSettings;->bSj:Landroid/preference/CheckBoxPreference;

    new-instance v9, Lcom/android/settings/core/instrumentation/e;

    invoke-direct {v9}, Lcom/android/settings/core/instrumentation/e;-><init>()V

    invoke-direct {v3, v6, v8, v9}, Lcom/android/settings/an;-><init>(Landroid/content/Context;Landroid/preference/CheckBoxPreference;Lcom/android/settings/core/instrumentation/e;)V

    iput-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSi:Lcom/android/settings/an;

    const-string/jumbo v3, "support_se_route"

    const/4 v8, 0x0

    invoke-static {v3, v8}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1b

    new-instance v3, Lcom/android/settings/nfc/NfcEnabler;

    invoke-direct {v3, v6, v0, v1, v7}, Lcom/android/settings/nfc/NfcEnabler;-><init>(Landroid/content/Context;Landroid/preference/CheckBoxPreference;Lcom/android/settingslib/MiuiRestrictedPreference;Landroid/preference/ListPreference;)V

    iput-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSt:Lcom/android/settings/nfc/NfcEnabler;

    :goto_2
    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v8, "airplane_mode_toggleable_radios"

    invoke-static {v3, v8}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v4, :cond_1c

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v9, 0x11200da

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    :goto_3
    const-string/jumbo v9, "wimax_settings"

    invoke-virtual {p0, v9}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    if-eqz v3, :cond_2

    iget-object v10, p0, Lcom/android/settings/MiuiWirelessSettings;->bSA:Landroid/os/UserManager;

    const-string/jumbo v11, "no_config_mobile_networks"

    invoke-virtual {v10, v11}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1d

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    if-eqz v9, :cond_3

    invoke-virtual {v3, v9}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    :goto_4
    if-eqz v8, :cond_4

    const-string/jumbo v3, "wifi"

    invoke-virtual {v8, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_5

    :cond_4
    const-string/jumbo v3, "vpn_settings"

    invoke-virtual {p0, v3}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    const-string/jumbo v10, "toggle_airplane"

    invoke-virtual {v3, v10}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    :cond_5
    if-nez v4, :cond_6

    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSA:Landroid/os/UserManager;

    const-string/jumbo v10, "no_config_vpn"

    invoke-virtual {v3, v10}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v5}, Lcom/android/settings/vpn2/VpnManager;->OM()I

    move-result v3

    if-lez v3, :cond_7

    :cond_6
    const-string/jumbo v3, "vpn_settings"

    invoke-virtual {p0, v3}, Lcom/android/settings/MiuiWirelessSettings;->bWK(Ljava/lang/String;)V

    :cond_7
    if-eqz v8, :cond_8

    const-string/jumbo v3, "bluetooth"

    invoke-virtual {v8, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    :cond_8
    if-eqz v8, :cond_9

    const-string/jumbo v3, "nfc"

    invoke-virtual {v8, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_a

    :cond_9
    const-string/jumbo v3, "toggle_nfc"

    invoke-virtual {p0, v3}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    const-string/jumbo v5, "toggle_airplane"

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    const-string/jumbo v3, "android_beam_settings"

    invoke-virtual {p0, v3}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    const-string/jumbo v5, "toggle_airplane"

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    :cond_a
    invoke-static {v6}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSs:Landroid/nfc/NfcAdapter;

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSs:Landroid/nfc/NfcAdapter;

    if-nez v3, :cond_1f

    const-string/jumbo v3, "nfc_category"

    invoke-virtual {p0, v3}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {v8, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {v8, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {v8}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_b
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSt:Lcom/android/settings/nfc/NfcEnabler;

    move-object v0, v5

    :goto_5
    if-eqz v0, :cond_c

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_d

    :cond_c
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_d
    const-string/jumbo v0, "mobile_network_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bWK(Ljava/lang/String;)V

    const-string/jumbo v0, "manage_mobile_plan"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bWK(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    and-int/2addr v0, v1

    if-nez v0, :cond_e

    const-string/jumbo v0, "manage_mobile_plan"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_e

    const-string/jumbo v0, "manage_mobile_plan"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bWK(Ljava/lang/String;)V

    :cond_e
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSv:Landroid/content/pm/PackageManager;

    const-string/jumbo v1, "android.hardware.type.television"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string/jumbo v0, "toggle_airplane"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bWK(Ljava/lang/String;)V

    :cond_f
    const-string/jumbo v0, "proxy_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_10

    const-string/jumbo v0, "device_policy"

    invoke-virtual {v6, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getGlobalProxyAdmin()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_21

    const/4 v0, 0x1

    :goto_6
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_10
    const-string/jumbo v0, "connectivity"

    invoke-virtual {v6, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const-string/jumbo v1, "no_config_tethering"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-static {v6, v1, v2}, Lcom/android/settingslib/w;->crb(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/n;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v2, "tether_settings"

    invoke-virtual {p0, v2}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v1, "usb_tether_settings"

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "enable_bluetooth_tethering"

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherableUsbRegexs()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSC:[Ljava/lang/String;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSm:[Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSC:[Ljava/lang/String;

    array-length v0, v0

    if-eqz v0, :cond_22

    const/4 v0, 0x1

    :goto_7
    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSm:[Ljava/lang/String;

    array-length v1, v1

    if-eqz v1, :cond_23

    const/4 v1, 0x1

    :goto_8
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v5, p0, Lcom/android/settings/MiuiWirelessSettings;->bSw:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v6, 0x5

    invoke-virtual {v2, v3, v5, v6}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    :cond_11
    if-eqz v0, :cond_12

    invoke-static {}, Lcom/android/settings/aq;->bqE()Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_12
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_13
    if-eqz v1, :cond_14

    if-eqz v4, :cond_24

    :cond_14
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_9
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110a0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "isCellBroadcastAppLinkEnabled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/e/a;->aZJ(Landroid/content/Context;)V

    if-eqz v0, :cond_16

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSv:Landroid/content/pm/PackageManager;

    const-string/jumbo v2, "com.android.cellbroadcastreceiver"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_15

    const/4 v0, 0x0

    const-string/jumbo v1, "CMAS app disabled"

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    :cond_15
    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSv:Landroid/content/pm/PackageManager;

    invoke-static {v1}, Lcom/android/settings/e/a;->aZH(Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-eqz v1, :cond_16

    const/4 v0, 0x1

    const-string/jumbo v1, "CMAS app ncc broadcast enabled"

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_16
    :goto_a
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    and-int/2addr v0, v1

    if-nez v4, :cond_17

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSA:Landroid/os/UserManager;

    const-string/jumbo v1, "no_config_cell_broadcasts"

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_17
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "cell_broadcast_settings"

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v9, :cond_18

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_18
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "display"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v1

    const-string/jumbo v0, "wfd_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    const-string/jumbo v2, "support_nvdia_wifi_display"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_19

    if-eqz v0, :cond_19

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getFeatureState()I

    move-result v1

    if-nez v1, :cond_19

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_19
    return-void

    :cond_1a
    const/4 v0, 0x0

    move v4, v0

    goto/16 :goto_1

    :cond_1b
    new-instance v3, Lcom/android/settings/nfc/NfcEnabler;

    const/4 v8, 0x0

    invoke-direct {v3, v6, v0, v1, v8}, Lcom/android/settings/nfc/NfcEnabler;-><init>(Landroid/content/Context;Landroid/preference/CheckBoxPreference;Lcom/android/settingslib/MiuiRestrictedPreference;Landroid/preference/ListPreference;)V

    iput-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSt:Lcom/android/settings/nfc/NfcEnabler;

    goto/16 :goto_2

    :cond_1c
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_1d
    if-eqz v8, :cond_1e

    const-string/jumbo v10, "wimax"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    if-eqz v3, :cond_3

    :cond_1e
    const-string/jumbo v3, "toggle_airplane"

    invoke-virtual {v9, v3}, Landroid/preference/Preference;->setDependency(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_1f
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "android.hardware.nfc.hce"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    new-instance v0, Lcom/android/settings/nfc/PaymentBackend;

    invoke-direct {v0, v6}, Lcom/android/settings/nfc/PaymentBackend;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/nfc/PaymentBackend;->aLA()Ljava/util/List;

    move-result-object v0

    :goto_b
    const-string/jumbo v1, "support_se_route"

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_20

    new-instance v1, Lcom/android/settings/nfc/NfcSeRoute;

    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSs:Landroid/nfc/NfcAdapter;

    invoke-direct {v1, v6, v3, v7}, Lcom/android/settings/nfc/NfcSeRoute;-><init>(Landroid/content/Context;Landroid/nfc/NfcAdapter;Landroid/preference/ListPreference;)V

    iput-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSu:Lcom/android/settings/nfc/NfcSeRoute;

    goto/16 :goto_5

    :cond_20
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_5

    :cond_21
    const/4 v0, 0x0

    goto/16 :goto_6

    :cond_22
    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_23
    const/4 v1, 0x0

    goto/16 :goto_8

    :cond_24
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothPan;

    if-eqz v0, :cond_25

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothPan;->isTetheringOn()Z

    move-result v0

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_9

    :cond_25
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_9

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    const-string/jumbo v1, "MiuiWirelessSettings"

    const-string/jumbo v2, "CMAS app not installed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    :cond_26
    move-object v0, v5

    goto :goto_b
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onCreateDialog: dialogId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/jj;

    invoke-direct {v1, p0}, Lcom/android/settings/jj;-><init>(Lcom/android/settings/MiuiWirelessSettings;)V

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onDestroy()V

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothProfile;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSi:Lcom/android/settings/an;

    invoke-virtual {v0}, Lcom/android/settings/an;->pause()V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSt:Lcom/android/settings/nfc/NfcEnabler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSt:Lcom/android/settings/nfc/NfcEnabler;

    invoke-virtual {v0}, Lcom/android/settings/nfc/NfcEnabler;->pause()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSu:Lcom/android/settings/nfc/NfcSeRoute;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSu:Lcom/android/settings/nfc/NfcSeRoute;

    invoke-virtual {v0}, Lcom/android/settings/nfc/NfcSeRoute;->pause()V

    :cond_1
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onPreferenceTreeClick: preference="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bLv(Ljava/lang/String;)V

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSj:Landroid/preference/CheckBoxPreference;

    if-ne p2, v3, :cond_0

    const-string/jumbo v3, "ril.cdma.inecmmode"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.internal.intent.action.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS"

    invoke-direct {v0, v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiWirelessSettings;->startActivityForResult(Landroid/content/Intent;I)V

    return v1

    :cond_0
    const-string/jumbo v3, "manage_mobile_plan"

    invoke-virtual {p0, v3}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-ne p2, v3, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->bLw()V

    :cond_1
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_2
    const-string/jumbo v3, "cell_broadcast_settings"

    invoke-virtual {p0, v3}, Lcom/android/settings/MiuiWirelessSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    if-ne p2, v3, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/e/a;->aZK(Landroid/app/Activity;)V

    return v1

    :cond_3
    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    if-ne p2, v3, :cond_6

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiWirelessSettings;->bLy(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settingslib/l;->cpP(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/android/settings/TetherService;->bRX(Landroid/content/Context;I)V

    :cond_5
    invoke-direct {p0, v0}, Lcom/android/settings/MiuiWirelessSettings;->bLx(Z)V

    goto :goto_0

    :cond_6
    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    if-ne p2, v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0, v5}, Lcom/android/settings/MiuiWirelessSettings;->bLy(I)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settingslib/l;->cpP(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/android/settings/TetherService;->bRX(Landroid/content/Context;I)V

    :cond_8
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getTetheredIfaces()[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/MiuiWirelessSettings;->bSm:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/android/settings/MiuiWirelessSettings;->bLt([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->untether(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_a

    :goto_1
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothPan;

    if-eqz v0, :cond_9

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothPan;->setBluetoothTethering(Z)V

    :cond_9
    if-eqz v1, :cond_b

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12034a

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto/16 :goto_0

    :cond_a
    move v1, v2

    goto :goto_1

    :cond_b
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSn:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f12034b

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSi:Lcom/android/settings/an;

    invoke-virtual {v0}, Lcom/android/settings/an;->bqj()V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSt:Lcom/android/settings/nfc/NfcEnabler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSt:Lcom/android/settings/nfc/NfcEnabler;

    invoke-virtual {v0}, Lcom/android/settings/nfc/NfcEnabler;->aMj()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSu:Lcom/android/settings/nfc/NfcSeRoute;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSu:Lcom/android/settings/nfc/NfcSeRoute;

    invoke-virtual {v0}, Lcom/android/settings/nfc/NfcSeRoute;->aLW()V

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "mManageMobilePlanMessage"

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 4

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStart()V

    const-string/jumbo v0, "shared"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSr:Z

    new-instance v0, Lcom/android/settings/bD;

    invoke-direct {v0, p0, v2}, Lcom/android/settings/bD;-><init>(Lcom/android/settings/MiuiWirelessSettings;Lcom/android/settings/bD;)V

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSx:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/settings/MiuiWirelessSettings;->bSx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v3, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v3, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v3, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.action.MEDIA_UNSHARED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v3, "file"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v3, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/MiuiWirelessSettings;->bSx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v3, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/settings/MiuiWirelessSettings;->bSx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v0, v1}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/MiuiWirelessSettings;->bLB()V

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onStop()V

    invoke-virtual {p0}, Lcom/android/settings/MiuiWirelessSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiWirelessSettings;->bSx:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/MiuiWirelessSettings;->bSx:Landroid/content/BroadcastReceiver;

    return-void
.end method
