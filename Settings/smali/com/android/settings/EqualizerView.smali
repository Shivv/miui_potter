.class public Lcom/android/settings/EqualizerView;
.super Landroid/view/View;
.source "EqualizerView.java"


# static fields
.field public static bVV:I

.field public static bVW:I

.field public static bVX:I

.field public static bVY:I


# instance fields
.field private final bVZ:I

.field private final bWa:Landroid/graphics/Paint;

.field private final bWb:I

.field private final bWc:F

.field private bWd:I

.field private final bWe:[F

.field private bWf:I

.field private bWg:I

.field private bWh:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x14

    sput v0, Lcom/android/settings/EqualizerView;->bVW:I

    const/16 v0, 0x4e20

    sput v0, Lcom/android/settings/EqualizerView;->bVV:I

    const v0, 0xac44

    sput v0, Lcom/android/settings/EqualizerView;->bVX:I

    const/4 v0, 0x1

    sput v0, Lcom/android/settings/EqualizerView;->bVY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const v1, 0xffae00

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x7

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    iput v2, p0, Lcom/android/settings/EqualizerView;->bWg:I

    iput v2, p0, Lcom/android/settings/EqualizerView;->bWf:I

    invoke-virtual {p0, v2}, Lcom/android/settings/EqualizerView;->setWillNotDraw(Z)V

    if-eqz p2, :cond_0

    sget-object v0, Lcom/android/settings/cw;->bYN:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/EqualizerView;->bVZ:I

    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/android/settings/EqualizerView;->bWb:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/android/settings/EqualizerView;->bWc:F

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :goto_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/settings/EqualizerView;->bVZ:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void

    :cond_0
    iput v1, p0, Lcom/android/settings/EqualizerView;->bVZ:I

    iput v2, p0, Lcom/android/settings/EqualizerView;->bWb:I

    iput v3, p0, Lcom/android/settings/EqualizerView;->bWc:F

    goto :goto_0
.end method

.method private bQa(F)F
    .locals 4

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x3d3a0000    # -99.0f

    goto :goto_0
.end method

.method private bQb(F)F
    .locals 6

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget v2, Lcom/android/settings/EqualizerView;->bVW:I

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    sget v4, Lcom/android/settings/EqualizerView;->bVV:I

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sub-double/2addr v0, v2

    sub-double v2, v4, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private bQc(F)F
    .locals 3

    iget v0, p0, Lcom/android/settings/EqualizerView;->bWf:I

    iget v1, p0, Lcom/android/settings/EqualizerView;->bWg:I

    sub-int/2addr v0, v1

    if-gtz v0, :cond_0

    const-string/jumbo v0, "EqualizerView"

    const-string/jumbo v1, "rank is unint"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0

    :cond_0
    iget v0, p0, Lcom/android/settings/EqualizerView;->bWg:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v1, p0, Lcom/android/settings/EqualizerView;->bWf:I

    iget v2, p0, Lcom/android/settings/EqualizerView;->bWg:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v0, v1, v0

    return v0
.end method

.method private setPanitAlpha(F)V
    .locals 4

    const v1, 0x3d4ccccd    # 0.05f

    const v0, 0x3c23d70a    # 0.01f

    const/4 v3, 0x0

    cmpg-float v2, p1, v0

    if-gez v2, :cond_2

    move p1, v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget v0, p0, Lcom/android/settings/EqualizerView;->bWb:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/settings/EqualizerView;->bWc:F

    mul-float/2addr v1, p1

    iget v2, p0, Lcom/android/settings/EqualizerView;->bWb:I

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    :cond_1
    return-void

    :cond_2
    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    move p1, v1

    goto :goto_0
.end method


# virtual methods
.method public bPZ(II)V
    .locals 0

    iput p1, p0, Lcom/android/settings/EqualizerView;->bWg:I

    iput p2, p0, Lcom/android/settings/EqualizerView;->bWf:I

    return-void
.end method

.method public getMaxLevel()I
    .locals 2

    iget v0, p0, Lcom/android/settings/EqualizerView;->bWf:I

    sget v1, Lcom/android/settings/EqualizerView;->bVY:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public getMinLevel()I
    .locals 2

    iget v0, p0, Lcom/android/settings/EqualizerView;->bWg:I

    sget v1, Lcom/android/settings/EqualizerView;->bVY:I

    mul-int/2addr v0, v1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    const/4 v0, 0x6

    new-array v9, v0, [Lcom/android/settings/cb;

    new-instance v0, Lcom/android/settings/cb;

    invoke-direct {v0}, Lcom/android/settings/cb;-><init>()V

    const/4 v1, 0x0

    aput-object v0, v9, v1

    new-instance v0, Lcom/android/settings/cb;

    invoke-direct {v0}, Lcom/android/settings/cb;-><init>()V

    const/4 v1, 0x1

    aput-object v0, v9, v1

    new-instance v0, Lcom/android/settings/cb;

    invoke-direct {v0}, Lcom/android/settings/cb;-><init>()V

    const/4 v1, 0x2

    aput-object v0, v9, v1

    new-instance v0, Lcom/android/settings/cb;

    invoke-direct {v0}, Lcom/android/settings/cb;-><init>()V

    const/4 v1, 0x3

    aput-object v0, v9, v1

    new-instance v0, Lcom/android/settings/cb;

    invoke-direct {v0}, Lcom/android/settings/cb;-><init>()V

    const/4 v1, 0x4

    aput-object v0, v9, v1

    new-instance v0, Lcom/android/settings/cb;

    invoke-direct {v0}, Lcom/android/settings/cb;-><init>()V

    const/4 v1, 0x5

    aput-object v0, v9, v1

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    iget-object v2, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    const/high16 v3, 0x41a00000    # 20.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v10, v0

    const/4 v0, 0x0

    aget-object v0, v9, v0

    sget v1, Lcom/android/settings/EqualizerView;->bVX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x42960000    # 75.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/android/settings/cb;->bQe(FFFF)V

    const/4 v0, 0x1

    aget-object v0, v9, v0

    sget v1, Lcom/android/settings/EqualizerView;->bVX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    iget-object v3, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x432f0000    # 175.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/android/settings/cb;->bQe(FFFF)V

    const/4 v0, 0x2

    aget-object v0, v9, v0

    sget v1, Lcom/android/settings/EqualizerView;->bVX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v3, 0x3

    aget v2, v2, v3

    iget-object v3, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x43af0000    # 350.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/android/settings/cb;->bQe(FFFF)V

    const/4 v0, 0x3

    aget-object v0, v9, v0

    sget v1, Lcom/android/settings/EqualizerView;->bVX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v3, 0x4

    aget v2, v2, v3

    iget-object v3, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x44610000    # 900.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/android/settings/cb;->bQe(FFFF)V

    const/4 v0, 0x4

    aget-object v0, v9, v0

    sget v1, Lcom/android/settings/EqualizerView;->bVX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    iget-object v3, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v4, 0x4

    aget v3, v3, v4

    sub-float/2addr v2, v3

    const v3, 0x44dac000    # 1750.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/android/settings/cb;->bQe(FFFF)V

    const/4 v0, 0x5

    aget-object v0, v9, v0

    sget v1, Lcom/android/settings/EqualizerView;->bVX:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v3, 0x6

    aget v2, v2, v3

    iget-object v3, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    sub-float/2addr v2, v3

    const v3, 0x455ac000    # 3500.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/android/settings/cb;->bQe(FFFF)V

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, 0x0

    sget v0, Lcom/android/settings/EqualizerView;->bVW:I

    int-to-float v0, v0

    const v3, 0x3f933333    # 1.15f

    div-float/2addr v0, v3

    move v6, v0

    move v0, v1

    move v1, v2

    :goto_0
    sget v2, Lcom/android/settings/EqualizerView;->bVV:I

    int-to-float v2, v2

    const v3, 0x3f933333    # 1.15f

    mul-float/2addr v2, v3

    cmpg-float v2, v6, v2

    if-gez v2, :cond_4

    sget v2, Lcom/android/settings/EqualizerView;->bVX:I

    int-to-float v2, v2

    div-float v2, v6, v2

    const v3, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    new-instance v3, Lcom/android/settings/cc;

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    float-to-double v12, v2

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    double-to-float v2, v12

    invoke-direct {v3, v4, v2}, Lcom/android/settings/cc;-><init>(FF)V

    invoke-virtual {v3, v10}, Lcom/android/settings/cc;->bQk(F)Lcom/android/settings/cc;

    move-result-object v2

    const/4 v4, 0x0

    aget-object v4, v9, v4

    invoke-virtual {v4, v3}, Lcom/android/settings/cb;->bQd(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v5, v9, v5

    invoke-virtual {v5, v3}, Lcom/android/settings/cb;->bQd(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v5

    const/4 v7, 0x2

    aget-object v7, v9, v7

    invoke-virtual {v7, v3}, Lcom/android/settings/cb;->bQd(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v7

    const/4 v8, 0x3

    aget-object v8, v9, v8

    invoke-virtual {v8, v3}, Lcom/android/settings/cb;->bQd(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v8

    const/4 v11, 0x4

    aget-object v11, v9, v11

    invoke-virtual {v11, v3}, Lcom/android/settings/cb;->bQd(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v11

    const/4 v12, 0x5

    aget-object v12, v9, v12

    invoke-virtual {v12, v3}, Lcom/android/settings/cb;->bQd(Lcom/android/settings/cc;)Lcom/android/settings/cc;

    move-result-object v3

    invoke-virtual {v2}, Lcom/android/settings/cc;->bQl()F

    move-result v2

    invoke-virtual {v4}, Lcom/android/settings/cc;->bQl()F

    move-result v4

    mul-float/2addr v2, v4

    invoke-virtual {v5}, Lcom/android/settings/cc;->bQl()F

    move-result v4

    mul-float/2addr v2, v4

    invoke-virtual {v7}, Lcom/android/settings/cc;->bQl()F

    move-result v4

    mul-float/2addr v2, v4

    invoke-virtual {v8}, Lcom/android/settings/cc;->bQl()F

    move-result v4

    mul-float/2addr v2, v4

    invoke-virtual {v11}, Lcom/android/settings/cc;->bQl()F

    move-result v4

    mul-float/2addr v2, v4

    invoke-virtual {v3}, Lcom/android/settings/cc;->bQl()F

    move-result v3

    mul-float/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/android/settings/EqualizerView;->bQa(F)F

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/settings/EqualizerView;->bQc(F)F

    move-result v2

    iget v3, p0, Lcom/android/settings/EqualizerView;->bWd:I

    int-to-float v3, v3

    mul-float v7, v2, v3

    invoke-direct {p0, v6}, Lcom/android/settings/EqualizerView;->bQb(F)F

    move-result v2

    iget v3, p0, Lcom/android/settings/EqualizerView;->bWh:I

    int-to-float v3, v3

    mul-float v8, v2, v3

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/settings/EqualizerView;->bWh:I

    div-int/lit8 v2, v2, 0x5

    int-to-float v2, v2

    cmpg-float v3, v1, v2

    if-gez v3, :cond_2

    div-float v2, v1, v2

    invoke-direct {p0, v2}, Lcom/android/settings/EqualizerView;->setPanitAlpha(F)V

    :cond_0
    :goto_1
    iget v2, p0, Lcom/android/settings/EqualizerView;->mPaddingLeft:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/android/settings/EqualizerView;->mPaddingTop:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    iget v0, p0, Lcom/android/settings/EqualizerView;->mPaddingLeft:I

    int-to-float v0, v0

    add-float v3, v0, v8

    iget v0, p0, Lcom/android/settings/EqualizerView;->mPaddingTop:I

    int-to-float v0, v0

    add-float v4, v0, v7

    iget-object v5, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_1
    const v0, 0x3f933333    # 1.15f

    mul-float/2addr v0, v6

    move v6, v0

    move v1, v8

    move v0, v7

    goto/16 :goto_0

    :cond_2
    iget v3, p0, Lcom/android/settings/EqualizerView;->bWh:I

    int-to-float v3, v3

    sub-float/2addr v3, v1

    cmpl-float v4, v2, v3

    if-lez v4, :cond_3

    div-float v2, v3, v2

    invoke-direct {p0, v2}, Lcom/android/settings/EqualizerView;->setPanitAlpha(F)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget v2, p0, Lcom/android/settings/EqualizerView;->bWb:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/EqualizerView;->bWa:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/settings/EqualizerView;->bWc:F

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v11, p0, Lcom/android/settings/EqualizerView;->bWb:I

    invoke-virtual {v2, v3, v4, v5, v11}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    sub-int v0, p4, p2

    iget v1, p0, Lcom/android/settings/EqualizerView;->mPaddingLeft:I

    iget v2, p0, Lcom/android/settings/EqualizerView;->mPaddingRight:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/EqualizerView;->bWh:I

    sub-int v0, p5, p3

    iget v1, p0, Lcom/android/settings/EqualizerView;->mPaddingTop:I

    iget v2, p0, Lcom/android/settings/EqualizerView;->mPaddingBottom:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/EqualizerView;->bWd:I

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/EqualizerView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    return-void
.end method

.method public setBand(II)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    int-to-float v1, p2

    sget v2, Lcom/android/settings/EqualizerView;->bVY:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    aput v1, v0, p1

    invoke-virtual {p0}, Lcom/android/settings/EqualizerView;->postInvalidate()V

    return-void
.end method

.method public setBands([F)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/EqualizerView;->setBands([FI)V

    return-void
.end method

.method public setBands([FI)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/EqualizerView;->bWe:[F

    add-int v2, p2, v0

    aget v2, p1, v2

    sget v3, Lcom/android/settings/EqualizerView;->bVY:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/EqualizerView;->postInvalidate()V

    return-void
.end method
