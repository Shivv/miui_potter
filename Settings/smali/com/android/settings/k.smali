.class Lcom/android/settings/k;
.super Landroid/widget/BaseExpandableListAdapter;
.source "TrustedCredentialsSettings.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final buA:Lcom/android/settings/n;

.field final synthetic buB:Lcom/android/settings/TrustedCredentialsSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/TrustedCredentialsSettings$Tab;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    new-instance v0, Lcom/android/settings/n;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, p0, v1}, Lcom/android/settings/n;-><init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/TrustedCredentialsSettings$Tab;Lcom/android/settings/k;Lcom/android/settings/n;)V

    iput-object v0, p0, Lcom/android/settings/k;->buA:Lcom/android/settings/n;

    invoke-virtual {p0}, Lcom/android/settings/k;->load()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/TrustedCredentialsSettings$Tab;Lcom/android/settings/k;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/k;-><init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/TrustedCredentialsSettings$Tab;)V

    return-void
.end method

.method private bhw(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/k;->buA:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    return v0
.end method

.method private bhy(Lcom/android/settings/p;Lcom/android/settings/TrustedCredentialsSettings$Tab;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x0

    if-nez p3, :cond_1

    new-instance v1, Lcom/android/settings/l;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/l;-><init>(Lcom/android/settings/k;Lcom/android/settings/l;)V

    iget-object v0, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-virtual {v0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0d024b

    invoke-virtual {v0, v2, p4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    invoke-virtual {p3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const v0, 0x7f0a04b0

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/android/settings/l;->bhD(Lcom/android/settings/l;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f0a04b1

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/android/settings/l;->bhE(Lcom/android/settings/l;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f0a04af

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    invoke-static {v1, v0}, Lcom/android/settings/l;->bhF(Lcom/android/settings/l;Landroid/widget/Switch;)Landroid/widget/Switch;

    invoke-static {v1}, Lcom/android/settings/l;->bhC(Lcom/android/settings/l;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    :goto_0
    invoke-static {v0}, Lcom/android/settings/l;->bhA(Lcom/android/settings/l;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {p1}, Lcom/android/settings/p;->big(Lcom/android/settings/p;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v0}, Lcom/android/settings/l;->bhB(Lcom/android/settings/l;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {p1}, Lcom/android/settings/p;->bih(Lcom/android/settings/p;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lcom/android/settings/TrustedCredentialsSettings$Tab;->bhn(Lcom/android/settings/TrustedCredentialsSettings$Tab;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/android/settings/l;->bhC(Lcom/android/settings/l;)Landroid/widget/Switch;

    move-result-object v1

    invoke-static {p1}, Lcom/android/settings/p;->bif(Lcom/android/settings/p;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    invoke-static {v0}, Lcom/android/settings/l;->bhC(Lcom/android/settings/l;)Landroid/widget/Switch;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v2}, Lcom/android/settings/TrustedCredentialsSettings;->bhb(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/os/UserManager;

    move-result-object v2

    const-string/jumbo v3, "no_config_credentials"

    new-instance v4, Landroid/os/UserHandle;

    iget v5, p1, Lcom/android/settings/p;->bve:I

    invoke-direct {v4, v5}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v2, v3, v4}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setEnabled(Z)V

    invoke-static {v0}, Lcom/android/settings/l;->bhC(Lcom/android/settings/l;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/Switch;->setVisibility(I)V

    invoke-static {v0}, Lcom/android/settings/l;->bhC(Lcom/android/settings/l;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setTag(Ljava/lang/Object;)V

    :cond_0
    return-object p3

    :cond_1
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/l;

    goto :goto_0
.end method


# virtual methods
.method public bht(I)Z
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/k;->bhu(IZ)Z

    move-result v0

    return v0
.end method

.method public bhu(IZ)Z
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0, p1}, Lcom/android/settings/k;->getGroup(I)Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v2}, Lcom/android/settings/TrustedCredentialsSettings;->bhb(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/os/UserManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/UserManager;->isQuietModeEnabled(Landroid/os/UserHandle;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Lcom/android/internal/app/UnlaunchableAppActivity;->createInQuietModeDialogIntent(I)Landroid/content/Intent;

    move-result-object v0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-virtual {v1}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return v3

    :cond_1
    iget-object v2, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v2}, Lcom/android/settings/TrustedCredentialsSettings;->bhb(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/os/UserManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/UserManager;->isUserUnlocked(Landroid/os/UserHandle;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    iget-object v2, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-virtual {v2}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0, v1}, Lcom/android/settings/TrustedCredentialsSettings;->bhf(Lcom/android/settings/TrustedCredentialsSettings;I)Z

    :cond_2
    return v3

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method public bhv(I)Lcom/android/settings/m;
    .locals 3

    new-instance v0, Lcom/android/settings/m;

    iget-object v1, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, p1, v2}, Lcom/android/settings/m;-><init>(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/k;ILcom/android/settings/m;)V

    return-object v0
.end method

.method public bhx(I)Landroid/content/pm/UserInfo;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-static {v0}, Lcom/android/settings/TrustedCredentialsSettings;->bhb(Lcom/android/settings/TrustedCredentialsSettings;)Landroid/os/UserManager;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/settings/k;->bhw(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    return-object v0
.end method

.method public bhz(Lcom/android/settings/p;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/k;->buA:Lcom/android/settings/n;

    invoke-virtual {v0, p1}, Lcom/android/settings/n;->bhP(Lcom/android/settings/p;)V

    return-void
.end method

.method public getChild(II)Lcom/android/settings/p;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/k;->buA:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/settings/k;->bhw(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/p;

    return-object v0
.end method

.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/k;->getChild(II)Lcom/android/settings/p;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2

    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/k;->getChild(II)Lcom/android/settings/p;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/k;->buA:Lcom/android/settings/n;

    invoke-static {v1}, Lcom/android/settings/n;->bhS(Lcom/android/settings/n;)Lcom/android/settings/TrustedCredentialsSettings$Tab;

    move-result-object v1

    invoke-direct {p0, v0, v1, p4, p5}, Lcom/android/settings/k;->bhy(Lcom/android/settings/p;Lcom/android/settings/TrustedCredentialsSettings$Tab;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getChildrenCount(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/k;->buA:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getGroup(I)Landroid/os/UserHandle;
    .locals 2

    new-instance v0, Landroid/os/UserHandle;

    iget-object v1, p0, Lcom/android/settings/k;->buA:Lcom/android/settings/n;

    invoke-static {v1}, Lcom/android/settings/n;->bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    return-object v0
.end method

.method public bridge synthetic getGroup(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/k;->getGroup(I)Landroid/os/UserHandle;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/k;->buA:Lcom/android/settings/n;

    invoke-static {v0}, Lcom/android/settings/n;->bhR(Lcom/android/settings/n;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settings/k;->bhw(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-virtual {v0}, Lcom/android/settings/TrustedCredentialsSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-static {v0, p4}, Lcom/android/settings/aq;->bqM(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    :cond_0
    const v0, 0x1020016

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/android/settings/k;->bhx(I)Landroid/content/pm/UserInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f1203ec

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAlignment(I)V

    return-object p3

    :cond_1
    const v1, 0x7f1203eb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public load()V
    .locals 2

    new-instance v0, Lcom/android/settings/o;

    iget-object v1, p0, Lcom/android/settings/k;->buA:Lcom/android/settings/n;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v1}, Lcom/android/settings/o;-><init>(Lcom/android/settings/n;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/settings/o;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-virtual {p0, p3, p4}, Lcom/android/settings/k;->getChild(II)Lcom/android/settings/p;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/TrustedCredentialsSettings;->bhg(Lcom/android/settings/TrustedCredentialsSettings;Lcom/android/settings/p;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/p;

    iget-object v1, p0, Lcom/android/settings/k;->buB:Lcom/android/settings/TrustedCredentialsSettings;

    invoke-virtual {v1, v0}, Lcom/android/settings/TrustedCredentialsSettings;->bgR(Lcom/android/settings/p;)V

    return-void
.end method

.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 1

    invoke-virtual {p0, p3}, Lcom/android/settings/k;->bht(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
