.class public Lcom/android/settings/T;
.super Ljava/lang/Object;
.source "MiuiOptionUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bmv(Landroid/content/Context;I)I
    .locals 3

    const/4 v1, 0x0

    invoke-static {p0}, Lmiui/util/AudioManagerHelper;->isSilentEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, -0x1

    if-eq p1, v2, :cond_1

    if-eq p1, v0, :cond_1

    invoke-static {p0, v1}, Lmiui/util/AudioManagerHelper;->toggleSilent(Landroid/content/Context;I)V

    return p1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public static bmw(Landroid/content/Context;I)I
    .locals 4

    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/provider/MiuiSettings$SilenceMode;->getZenMode(Landroid/content/Context;)I

    move-result v2

    if-ne v2, v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    const/4 v3, -0x1

    if-eq p1, v3, :cond_2

    if-eq p1, v2, :cond_2

    if-eqz p1, :cond_1

    :goto_1
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/MiuiSettings$SilenceMode;->setSilenceMode(Landroid/content/Context;ILandroid/net/Uri;)V

    return p1

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    return v2
.end method
