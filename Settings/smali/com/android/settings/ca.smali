.class Lcom/android/settings/ca;
.super Landroid/os/AsyncTask;
.source "MiuiLabSettings.java"


# instance fields
.field final synthetic bVO:Lcom/android/settings/MiuiLabSettings;

.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiLabSettings;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/ca;->bVO:Lcom/android/settings/MiuiLabSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/android/settings/ca;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected bPQ(Ljava/lang/Integer;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/ca;->bVO:Lcom/android/settings/MiuiLabSettings;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/MiuiLabSettings;->bPP(Lcom/android/settings/MiuiLabSettings;I)I

    iget-object v0, p0, Lcom/android/settings/ca;->bVO:Lcom/android/settings/MiuiLabSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiLabSettings;->bPO(Lcom/android/settings/MiuiLabSettings;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/ca;->bVO:Lcom/android/settings/MiuiLabSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiLabSettings;->bPN(Lcom/android/settings/MiuiLabSettings;)Lmiui/preference/ValuePreference;

    move-result-object v0

    const v1, 0x7f120a9a

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/ca;->bVO:Lcom/android/settings/MiuiLabSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiLabSettings;->bPN(Lcom/android/settings/MiuiLabSettings;)Lmiui/preference/ValuePreference;

    move-result-object v0

    const v1, 0x7f120a99

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/ca;->bVO:Lcom/android/settings/MiuiLabSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiLabSettings;->bPN(Lcom/android/settings/MiuiLabSettings;)Lmiui/preference/ValuePreference;

    move-result-object v0

    const v1, 0x7f120a9b

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 6

    const/4 v2, 0x0

    const-string/jumbo v0, "content://com.miui.gallery.open_api"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "search_status"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/ca;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_4

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "status"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    return-object v0

    :cond_2
    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    :goto_0
    const/4 v0, -0x1

    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v0

    :cond_4
    :try_start_2
    const-string/jumbo v0, "MiuiLabSettings"

    const-string/jumbo v2, "Cursor is null when get gallery search status"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string/jumbo v2, "MiuiLabSettings"

    const-string/jumbo v3, "Exception when get gallery search status."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    if-eqz v1, :cond_5

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    return-object v0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/ca;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/settings/ca;->bPQ(Ljava/lang/Integer;)V

    return-void
.end method
